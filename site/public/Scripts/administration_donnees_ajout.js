/**
 * @brief Initialisation de la page d'administration des couches
 * @return
 */
function administration_donnees_ajout_init() {
	var parcourir;

	try {
		// parent.IHM_historique_add(window.location);
		parent.IHM_Chargement_stop();
		// parent.IHM_Domaines_hide();
		// parent.IHM_Domaines_resume();
		/*
		 * administration_carto_domaineChange();
		 * parent.domaines_event_onchange_add(parent.events_administration_carto_changedomaine);
		 * administration_carto_onfocus();
		 */
	} catch (e) {
	}
}

/**
 * @brief Gère le changement de type de couche
 * @param format
 * @return
 */
function administration_donnees_ajout_ontypechange(format) {
	var fichier, table;
	var parcourir;

	fichier = document.getElementById("file_item");
	table = document.getElementById("table_item");
	if (fichier && table) {
		switch (format.value) {
		case "1":
			fichier.style.display = "none";
			table.style.display = "";
			break;
		default:
			fichier.style.display = "";
			table.style.display = "none";
			break;
		}
	}
}
/**
 * Validation d'un lot de données
 * @return
 */
function administration_lot_ajout_valider(){
	var valid = true;
	var importCheckbox = document.getElementById("IMPORT");
	var mefFile = document.getElementById("mefFile");
	var treePanel = Ext.getCmp('ext-arbo-sdom');
	if (treePanel) {
		if (treePanel.getChecked().length == 0 || (importCheckbox.checked && mefFile.value=="") ) {
			valid = false;
			parent.Ext.Msg.alert("Ajout de couche",
					"Tous les champs sont obligatoires");
			return;
		}
	}
	if (valid) {
		var oCtrlSdom = document.getElementById("SDOM");
		parent.checkAndSetCurrentSousDomaine(treePanel, oCtrlSdom, tabSdom, function(){parent.IHM_Chargement_start();document.forms[0].submit();}, this);
	}
}

/**
 * @brief Valide l'ajout d'une couche
 * @return
 */
function administration_donnees_ajout_valider() {
	var serveur, format, fichier, table;
	var valid = true;
	var msg = "";
	var errormsg;
	
	format = document.getElementById("FORMAT");
	table = document.getElementById("TABLE");
	serveur = document.getElementById("SERVEUR");
	var treePanel = Ext.getCmp('ext-arbo-sdom');
	var importCheckbox = document.getElementById("IMPORT");
	var mefFile = document.getElementById("mefFile");
	// check table name : must not begin with a number and can not contain non
	// alphanumerical characters
	if (format.value == 1 || format.value == -3) {
		var modele = /^[a-z]+[a-z0-9_]*$/i;
		if (modele.test(table.value)) {

		} else {
			parent.Ext.Msg
					.alert(
							"Ajout de couche",
							"Le nom de table saisi doit commencer par un caractère <br>"
									+ "de l'alphabet et ne doit pas contenir de caractères<br>"
									+ "accentués ou non alphanumériques comme -()\"'. etc.");
			return;
		}

		if (in_array(table.value, tabData[serveur.value])) {
			parent.Ext.Msg.alert("Ajout de couche",
					"Une table portant ce nom existe déjà.");
			return;
		}
	}

	fichier = document.getElementById("PATH");
	

	errormsg = document.getElementById("errormsg");
	if (serveur && format && fichier && table && treePanel) {
		if ((serveur.value == "") || (format.value == "") ||(treePanel.getChecked().length == 0)
				|| ((format.value == 1) && (table.value == ""))
				|| (importCheckbox.checked &&  mefFile.value=="")
				)
			{
			valid = false;
			parent.Ext.Msg.alert("Ajout de couche",
					"Tous les champs sont obligatoires");
			return;
		}
	}

	// cas raster : envoie d'une requette ajax
	// vers les serveur de consultation pour recuperer les infos specifiques au
	// raster
	
	if (format.value == 0 && (fichier.value != "")) {
		var serveur = document.getElementById("SERVEUR");
		var url = window.location.protocol + "//" + serveurs[serveur.value];
		url += "/PRRA/buildVrtForRaster.php";
		parent.IHM_Chargement_start();
		parent.Ext.Ajax.timeout = 900000;  
		parent.Ext.Ajax
				.request( {
					url :url,
					success : function(response) {
					    response = Ext.decode(response.responseText);
					    parent.IHM_Chargement_stop();
						var raster_info = document
								.getElementById("RASTER_INFO");
						raster_info.value = response.rasterDesc;
						if (valid) {
						  var oCtrlSdom = document.getElementById("SDOM");
						  parent.checkAndSetCurrentSousDomaine(treePanel, oCtrlSdom, tabSdom, function(){parent.IHM_Chargement_start();document.forms[0].submit();}, this);
					    }
					},
					failure : function(response) {
						parent.Ext.Msg
								.alert("Intégration couche raster",
										"Les informations spécifiques à la couche raster n'ont pas pu être récupérées");
						valid = false;
					},
					scope :this,
					params : {
						file :fichier.value
					}
				});
	}else if(format.value == 0 && fichier.value == ""){
	  var oCtrlSdom = document.getElementById("SDOM");
	  parent.checkAndSetCurrentSousDomaine(treePanel, oCtrlSdom, tabSdom, function(){parent.IHM_Chargement_start();document.forms[0].submit();}, this);
	}else{
	  if (valid) {
		var oCtrlSdom = document.getElementById("SDOM");
		parent.checkAndSetCurrentSousDomaine(treePanel, oCtrlSdom, tabSdom, function(){parent.IHM_Chargement_start();document.forms[0].submit();}, this);
	  }
	}
	
	
}

/**
 * @brief Formatte le nom de la couche
 * @param obj
 * @return
 */
function administration_donnees_ajout_tolower(obj) {
	var text = obj.value.toString();

	text = text.toLowerCase();
	text = text.replace(/[àâä]/ig, "a");
	text = text.replace(/[éèêë]/ig, "e");
	text = text.replace(/[îï]/ig, "i");
	text = text.replace(/[ôö]/ig, "o");
	text = text.replace(/[ù]/ig, "u");
	text = text.replace(/[ç]/ig, "c");
	text = text.replace(/\s/g, "_");
	obj.value = text;
}

/**
 * @brief Ouvre la popup pour le choix des fichiers
 * @return
 */
function administration_donnees_ajout_ChooseFile() {
	var l_url;
	var serveur;

	serveur = document.getElementById("SERVEUR");
	l_url = window.location.protocol + "//" + serveurs[serveur.value];
	l_url += "/PRRA/Administration/Administration/Layers/ChooseFile/TSPopup.php";
	l_url += "?Contenu=TSParcourir.php";
	l_url += "&absolu=0";
	l_url += "&clair=1"
	l_url += "&Valider=" + escape("S&eacute;lectionner");
	l_url += "&rslt=TSParcourir_RenvoiResultat";
	l_url += "&rslt_param=\"" + escape("fct=parent.frames['fenetre'].administration_donnees_ajout_ChooseFile_back")	+ "\"";
	l_url += "&Root=MAP_PATH";
	l_url += "&filtres=Raster simple (ecw, tif)|ecw;tif||Tuillage raster|shp";
	parent.loadExtPopup(l_url, "CHOOSE_FILE", "Choix du fichier raster", 500, 400, false);
	// parent.OpenWindow(l_url, "Choix du fichier raster", true);
}

function administration_donnees_ajout_ChooseFile_back(value) {
	var inpt;

	inpt = document.getElementById('PATH');
	if (inpt)
		inpt.value = value;
}

/**
 * @brief envoie une requete vers le serveur carto permettant de choisir des
 *        champs
 * @param acces_serveur
 *            url d'accès au serveur carto
 * @param imode
 *            one => radio, several => checkbox
 * @param nameData
 *            nom d champ contenant la table
 * @param field
 *            champ à mettre à jour
 */
function chooseField(acces_serveur, imode, nameData, field) {

	data = document.getElementById(nameData).value;
	url = window.location.protocol
			+ "//"
			+ acces_serveur
			+ "/PRRA/Administration/Administration/Layers/LayersFieldsWebservice.php?DATA="
			+ data + "&iMode=" + imode + "&form=formLoadTh&element=" + field;
	window
			.open(
					url,
					"CHOOSE_FIELDS",
					"width=100, height=100, directories=no, location=no, resizable=yes, menubar=no, toolbar=no, status=no, scrollbars=auto");

}

/**
 * @brief valide le formlaire de création de thésaurus
 * 
 */
function administration_thesaurus_valider() {

	var formTh = document.formLoadTh;
	var TABLE = formTh.elements["TABLE"];
	var key = formTh.elements["key"];
	var fieldName = formTh.elements["fieldName"];
	var title = formTh.elements["title"];
	if (TABLE == "" || key == "" || fieldName == "" || title == "") {
		parent.Ext.Msg.alert("Administration des thesaurus ",
				"Tous les champs sont obligatoires")
	} else {
		formTh.action = window.location.protocol + '//'
				+ serveurs[formTh.SERVEUR.value]
				+ '/PRRA/generate_thesaurus.php';
		formTh.submit();
	}

}
/**
 * @brief Ouvre la popup de choix de visualisation des tables
 * @return
 */
function administration_donnees_ajout_ChooseTable() {
	var l_url;
	var serveur;

	serveur = document.getElementById("SERVEUR");
	l_url = parent.IHM_getBaseUrl();
	l_url += "Administration/Administration/Layers/ChooseFile/TSPopup.php";
	l_url += "?Contenu=TSParcourirTables.php";
	l_url += "&Valider=Enregistrer";
	l_url += "&Serveur=" + serveur.value;
	l_url += "&rslt=opener.administration_donnees_ajout_ChooseTable_back";
	// parent.OpenWindow(l_url, "liste des tables existantes", true);
	parent.loadExtPopup(l_url, "LIST_TABLE", "Liste des tables existantes", 500, 400, true);
	// window.open(l_url, "CHOOSE_TABLE", "width=100, height=100,
	// directories=no, location=no, menubar=no, toolbar=no, status=no,
	// scrollbars=auto");
}

function administration_donnees_ajout_ChooseTable_back(tablename) {
	var table;

	table = document.getElementById('TABLE');
	if (table)
		table.value = tablename;
}

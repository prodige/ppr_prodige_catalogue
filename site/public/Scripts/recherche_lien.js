/**
 * @brief 
 * @return
 */
function	recherche_lien_onload()
{
	var		params;
	var		i;
	var		param;
	var		id;
	var		fenetre;
	
	fenetre = recherche_lien_rec(window);
	params = document.location.search.substr(1).split("&");
	for (i = 0; i < params.length; i++)
	{
		param = params[i].split("=");
		if (param[0] == "id")
			id = param[1];
	}
	if (fenetre && id)
		fenetre.IHM_Fiche_Show(id);
}

/**
 * @brief 
 * @return
 */
function	recherche_lien_rec(fenetre)
{
	var		rslt;
	
	try
	{
		if (fenetre.IHM_isMain())
			rslt = fenetre;
	}
	catch(e){}
	if (!rslt && fenetre.opener && (fenetre.opener != fenetre))
		rslt = recherche_lien_rec(fenetre.opener);
	if (!rslt && fenetre.parent && (fenetre.parent != fenetre))
		rslt = recherche_lien_rec(fenetre.parent);
	return rslt;
}
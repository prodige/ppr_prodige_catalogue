/**
 * @brief Initialisation de la page
 * @return
 */
function	erreurs_init(bDeconnect)
{
	try
	{
	  parent.IHM_Chargement_stop();
	}
	catch(e)
	{}
}
/**
 * deconnexion de la page
 * @return
 */
function deconnect(){
	
	if (typeof(topWindow().deconnect)!="undefined")
	  topWindow().deconnect();
	topWindow().Ext.Msg.alert("Connexion", "Votre session a expiré. Vous devez vous reconnecter pour utiliser cette fonctionnalité.");
}

function	erreurs_redirect(url, target, close)
{
	if(target)
		target.location = url;
	if (close)
		window.close();
}

function	erreurs_goBack()
{
	if (parent && parent.name != 'Cartographie_dynamique')
		window.location = parent.IHM_historique_getLastUrl();
	if (opener)
		window.close();
}

/**
 * @brief Initialisation de la page
 * @return
 */
function	catalogue_resume_init()
{
	try
	{
	  parent.Ext.getCmp('westLayout').setTitle("Domaine / Sous-domaine");
	  parent.unsetDomaine();
	  parent.IHM_historique_add(window.location);
	  parent.IHM_Chargement_stop();
	  parent.domaines_event_onchange_add(parent.events_donnees_changedomaine);
	}
	catch(e)
	{}
}

Ext.onReady(function() {

/* new version */
	var viewport = new Ext.Viewport({
    layout:'border',
    items:[
      {
        region:'center',
        layout:'column',
        autoScroll:true,
        items:[
          {
            id:'column_1',
            columnWidth:.60,
            baseCls:'x-plain',
            items:[
              {
            	id:'panel_actualites',
            	title:"Actualités",
            	tools : [{
        			id: 'rss',
        			
        			handler: function(event, toolEl, panel, tc) {
        			  /*var url = Routing.generate('catalogue_generate_flux_rss', {
        			    categorie_rss: "NOUVEAUTE"
                })*/
        				window.open("./generateFluxRss.php?categorie_rss=NOUVEAUTE", "_blank");
                //window.open(url, "_blank");
        			},
        			qtip: 'Flux RSS des nouveautés du catalogue'
        		}],
                contentEl:'actualites',
                autoScroll:true,
                style:'padding:10px 5px 5px 10px'
              },
              {
            	id:'panel_mises_a_jour',
                title:"Mises à jour",
                tools : [{
        			id: 'rss',
        			handler: function(event, toolEl, panel, tc) {
                /*var url = Routing.generate('catalogue_generate_flux_rss', {
                  categorie_rss: "MISE_A_JOUR"
                })*/
        				window.open("./generateFluxRss.php?categorie_rss=MISE_A_JOUR", "_blank");
                //window.open(url, "_blank");
        			},
        			qtip: 'Flux RSS des mises à jour du catalogue'
        		}],
                contentEl:'mises_a_jour',
                autoScroll:true,
                style:'padding:5px 5px 10px 10px'
              }
            ]
          },
          {
            id:'column_2',
            columnWidth:0.40,
            baseCls:'x-plain',
            items:[
              {
                id:'panel_geosource',                
                html:"<iframe id='iframe_geosource' src='/"+ geonetwork_dir+"/srv/fre/find?s_E_siteId="+sideIdDefault+"&bFormSearchOnly=1' frameborder='0'></iframe>",
                autoScroll:false,
                style:'padding:10px 10px 5px 5px'
              },
              {
                id:'panel_tableau_de_bord',
                title:"Tableau de bord de la plateforme",
                contentEl:'tableau_de_bord',
                autoScroll:true,
                style:'padding:5px 10px 5px 5px'
              },
              {
                id:'panel_catalogues',
                title:"Catalogues moissonnés",
                contentEl:'catalogues',
                autoScroll:true,
                style:'padding:5px 10px 10px 5px'
              }
            ]
          }
        ]
      }
    ]
	});
	viewport.on('resize', resizePanels);
	resizePanels();resizeIframeGeosource();
});

/**
 * redimensionne les panels de la page d'accueil
 */
function resizePanels(){
  var winH = Ext.getBody().getViewSize().height-2;
  
  // colonne 1, tous les panels ont la même hauteur
  var nbPanels = 0;
  Ext.each(Ext.getCmp('column_1').items.items, function(item, index){
    nbPanels++;
  });
  Ext.each(Ext.getCmp('column_1').items.items, function(item, index){
    item.setHeight(winH/nbPanels);
  });
  
  // colonne 2
  Ext.getCmp('panel_geosource').setHeight(winH/1.5);
  Ext.getCmp('panel_tableau_de_bord').setHeight(winH/6);
  Ext.getCmp('panel_catalogues').setHeight(winH/6);
  
  setTimeout("resizeIframeGeosource()", 300); // effectue le redimensionnement de l'iframe géosource après un certain délai pour corriger un problème de synchronisation
}
/**
 * redimensionne l'iframe géosource
 */
function resizeIframeGeosource(){
  Ext.getDom('iframe_geosource').height=Ext.getCmp('panel_geosource').getInnerHeight()-1;
  Ext.getDom('iframe_geosource').width=Ext.getCmp('panel_geosource').getInnerWidth()-1;
}

function	catalogue_resume_onfocus()
{
	var		domaine;

	try
	{
		parent.IHM_Domaines_resume();
	}
	catch(e){}
}

function showHideSdom(sdom){
  
  //console.log(document.getElementById(sdom));
  
}

function switchVisibility(elementId){

  if(document.getElementById(elementId).style.display=="none"){
    document.getElementById(elementId).style.display="block";
    document.getElementById(elementId+"_img").src = "images/data_hide.gif";
  }else{
    document.getElementById(elementId).style.display="none";
    document.getElementById(elementId+"_img").src = "images/data_show.gif";
  }
  
}

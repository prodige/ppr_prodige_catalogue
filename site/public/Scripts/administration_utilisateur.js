
/**
 * @brief Initialisation de la page
 * @return
 */
function	administration_utilisateur_init()
{
	try
	{
		//parent.IHM_historique_add(window.location);
		parent.IHM_Chargement_stop();
	}
	catch(e){}
	administration_utilisateur_onfocus();
}

function	administration_utilisateur_onfocus()
{
	try
	{
		parent.IHM_Domaines_hide();
	}
	catch(e)
	{}
}

/**
 * @brief Validation du formulaire
 * @return
 */
function	administration_utilisateur_valider()
{
	var		mdp1, mdp2, mdpChange, expire;
	var		valid = false;
	var		msg = "";
	var		errormsg;
	
	mdp1 = document.getElementById("MOTDEPASSE1");
	mdp2 = document.getElementById("MOTDEPASSE2");
	mdp1hidden = document.getElementById("MOTDEPASSE1_hidden");
        mdp2hidden = document.getElementById("MOTDEPASSE2_hidden");
	mdpChange = document.getElementById("CHANGEMOTDEPASSE");
	expire = document.getElementById("EXPIRE");
	errormsg = document.getElementById("errormsg");
	if (mdp1 && mdp2 && mdpChange && expire)
	{
		if (mdpChange.value == "0")
		{
			mdp1.value = "";
			mdp2.value = "";
			if (expire.value == 0)
				valid = true;
			else
			{
				valid = false;
				msg += "Le mot de passe a expiré et doit être modifié !<br>";
			}
		}
		else
		{
			valid = true;
			if (mdp1.value.length < 6)
			{
				msg += "Le mot de passe doit comporter 6 caract&egrave;res minimum !<br>";
				valid = false;
			}
			if (valid && (mdp1.value != mdp2.value))
			{
				msg += "Les deux mots de passe ne correspondent pas !<br>";
				valid = false;
			}
			if (valid && (mdp1.value.match(/^[a-z]+$/i)))
			{
				msg += "Le mot de passe doit comporter au moins un caractère spécial ou un chiffre !<br>";
				valid = false;
			}
		}
	}
	else
		msg += "Erreur dans le formulaire !";
	if (valid)
	{
		if ( parent.IHM_Chargement_start )
		  parent.IHM_Chargement_start();
		if ( window.frames["AdminDroits"] ){
		  document.forms[0].target = "AdminDroits";
		}
                // @since prodige4 password is encoded server side
		//mdp1hidden.value = hex_md5(mdp1.value);
		//mdp2hidden.value = hex_md5(mdp2.value);
		//mdp1.value ="";
		//mdp2.value ="";
		
		document.forms[0].submit();
	}
	else
	{
		if (errormsg)
			errormsg.innerHTML = msg;
	}
}

/**
 * @brief Gère le changement de mot de passe
 * @param obj
 * @return
 */
function	administration_utilisateur_onchangepwd(obj)
{
	var		mdp1, mdp2, mdpChange;
	
	mdp1 = document.getElementById("MOTDEPASSE1");
	mdp2 = document.getElementById("MOTDEPASSE2");
	mdpChange = document.getElementById("CHANGEMOTDEPASSE");
	if (mdp1 && mdp2 && mdpChange)
	{
		if (mdpChange.value == "0")
		{
			mdpChange.value = "1";
			if (obj)
			{
				if (mdp1 == obj)
					mdp2.value = "";
				if (mdp2 == obj)
					mdp1.value = "";
			}
		}
	}
}

function	administration_utilisateur_ongetfocus(obj)
{
	if (obj)
		obj.select();
}

function detail_compte(){
 	var		idUser;
	
	idUser = document.getElementById("IDENTIFIANT");
	var url = parent.IHM_getBaseUrl();
  url += "administration_compte_utilisateur.php?Id="+idUser.value;
  if ( typeof Ext != "undefined" && Ext.getCmp("mainTabPanel") ){//affichage depuis interface de gestion des droits
    Ext.getCmp("mainTabPanel").setActiveTab(1);
  }
  else
    window.location = url;
}
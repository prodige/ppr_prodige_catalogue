
/**
 * @brief Supprime la rubrique d'aide
 * @param id; identifiant de la rubrique
 * @return
 */
function	administration_aide_delRubrique(id)
{
	var iframe = this;
	iframe.tabParams=new Array("id", id);
	iframe.onSuccess = confirm_administration_aide_delConfirmed;  
	parent.Ext.Msg.confirm("Suppression de rubrique", "La rubrique va être supprimée. Souhaitez-vous continuer ?", parent.confirm, iframe);	
}

function confirm_administration_aide_delConfirmed(str, id){
  var url = parent.IHM_getBaseUrl();
  url += "administration_aide_Action.php?action=del&Id="+id;
  window.location = url;
}

function administration_aide_ajout_valider(){
 
 	var		nom, url, tabProfil, id_aide;
	nom = document.getElementById("NOM");
  desc = document.getElementById("DESC");
  b_profil_internet = document.getElementById("profil_internet");  
  url = document.getElementById("URL");
  tabProfil = document.getElementById("PROFIL");
  id_aide = document.getElementById("ID"); 
    
  valid = true;
  if (nom && url && id_aide && tabProfil)
	{
		if ((nom.value == "") || (url.value == "") || (tabProfil.value == "") || (desc.value == ""))
		{
			valid = false;
			parent.Ext.Msg.alert("Création ou modification de la rubrique d'aide", "Tous les champs sont obligatoires");
			return;
		}
	}
	if (valid)
	{
		parent.IHM_Chargement_start();
		document.forms[0].submit();
	}
}

/**
 * @brief Formatte le nom de la couche
 * @param obj
 * @return
 */
function	administration_donnees_ajout_tolower(obj)
{
	var text = obj.value.toString();

	text = text.toLowerCase();
	text = text.replace(/[àâä]/ig, "a");
	text = text.replace(/[éèêë]/ig, "e");
	text = text.replace(/[îï]/ig, "i");
	text = text.replace(/[ôö]/ig, "o");
	text = text.replace(/[ù]/ig, "u");
	text = text.replace(/[ç]/ig, "c");
	text = text.replace(/\s/g, "_");
	obj.value = text;
}

/**
 * @brief Fonction qui ouvre la popup permettant de choisir un fichier sur le serveur
 * @param element_id
 */
function help_ChooseFile(element_id)
{
  var l_url;
  var l_rep = "/";
  l_rootdir = "../../../../ressources/";
  var l_filtres = "Tous";
  l_url = parent.IHM_getBaseUrl();
  l_url += "Administration/Administration/Layers/ChooseFile/TSPopup.php";
  l_url += "?Contenu=TSParcourir.php";
  l_url += "&absolu=0";
  l_url += "&clair=1";
  l_url += "&Valider=" + escape("S&eacute;lectionner");
  l_url += "&rslt=TSParcourir_RenvoiResultat";
  l_url += "&rslt_param=\"" + escape("fct=parent.frames['fenetre'].help_ChooseFile_back") + "\"";
  l_url += "&Repertoire="+l_rep;
  l_url += "&Rootdir="+l_rootdir;
  l_url += "&filtres="+l_filtres;
  l_url +="&element_id="+element_id;
  parent.loadExtPopup(l_url, "CHOOSE_FILE", "Choix du fichier raster", 600, 400, true);
  //window.open(l_url, "CHOOSE_FILE", "width=100, height=100, directories=no, location=no, menubar=no, toolbar=no, status=no, scrollbars=auto");
}

function help_UploadFile(element_id){
	var l_url;
	l_rootdir = PRO_CATALOGUE_ROOT + "/ressources/";
  l_url += "../../../Upload/UploadPopup.php";
  l_url += "?absolu=0";
  l_url += "&path="+l_rootdir;
  l_url += "&nbMaxPj=1";
  l_url += "&btExec=Valider";
  l_url += "&fctExec=Upload_RenvoiResultat";
  l_url +="&element_id="+element_id;
  l_url += "&fctRslt=parent.fenetre.help_ChooseFile_back";
  parent.loadExtPopup(l_url, "UPLOAD_FILES", "Télécharger des fichiers", 600, 400, true);
}

function help_ChooseFile_back(value, element_id){
	var   inpt;
  inpt = document.getElementById(element_id);
  if (inpt)
    inpt.value = value;
}

/**
 * Change l'ordre du document d'aide
 * @param element_id
 * @param position
 */
function help_move_doc(element_id,position){
	  var iframe = this;
	  var ajaxUrl = parent.IHM_getBaseUrl()+"administration_aide_Action.php";
	  var queryParams = {action:"move",Id:element_id,pos:position};
	  iframe.tabParams = null;	  
	  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
}



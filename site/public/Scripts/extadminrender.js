/*!
 * Ext JS Library 3.1.1
 * Copyright(c) 2006-2010 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */


function setDefaultPage(){
  Ext.Ajax.request({
    url : defaultPage ,
    method : 'GET',
    success: function(result){
      Ext.getCmp("contAdmin").body.dom.innerHTML = "";
      eval(result.responseText);
    },
    failure: function(result){
      Ext.MessageBox.alert('Failed', result); 
    } 
  });
}
function updateGridListSize(grid){
  if ( !grid.resizeEnable ) {
    grid.view.refresh();
    return;
  } 
  grid.resizeEnable = false;
  var cHeight = Ext.get('contAdmin').dom.lastChild.offsetHeight - Ext.get('module_details').dom.firstChild.offsetHeight-17;
  if ( Ext.get('data_profiles') ){
    cHeight -= Ext.get('data_profiles').dom.firstChild.offsetHeight;
  }
  cHeight = Math.min(grid.view.scroller.dom.firstChild.offsetHeight+50, cHeight);
  cWidth = Ext.get('module_details').getWidth();
  grid.setSize(cWidth, cHeight);
};

Ext.onReady( function() {

  Ext.QuickTips.init();

  var tabAccordions = [];
  var bAccueil = true;

  for(var idAction in tabMainMenu ) {
    if(typeof tabMainMenu[idAction] == "string" ) {
      if(!bAccueil)
        tabAccordions.push(createAccordion(idAction, tabMainMenu[idAction], idAction));
      else {
        tabAccordions.push(createAccueil(idAction, tabMainMenu[idAction], idAction));
        bAccueil = false;
      }
    }
  }
  var viewport = new Ext.Viewport(
  {
    id :'pageViewport',
    layout :'border',
    border :false,
    forceLayout : true,
    title :'Center',
    id :'centerPanel',
    items :[{
    border :false,
      // left panel :accordion:
      region:"west",
      id :'leftPanel',
      layout:'accordion',
      width : "350",
      renderTo : Ext.getBody(),
      activeOnTop : true,
      split : true,
      fill : false,
      items : tabAccordions,
      hidden: disableLeftPanel,
      disabled: disableLeftPanel
    },{
      // center panel :tabPanel:
      region:"center",
      id : "contAdmin",
      title : "Ma fiche utilisateur",
      split : true,
      autoScroll : true,
      listeners : {
        render : setDefaultPage,
        resize : function(_this, width, height){
          if ( typeof tabGridList != "undefined" ){
            Ext.each(tabGridList, function(grid){grid.resizeEnable = true});
          }
          if ( Ext.getCmp('module_details') && typeof Ext.getCmp('module_details').getActiveTab().gridList != "undefined" ){
            updateGridListSize(Ext.getCmp('module_details').getActiveTab().gridList);
          }
        }
      }
    }]
  });

  for (var i=1; i<Ext.getCmp("leftPanel").items.items.length; i++){
    Ext.getCmp("leftPanel").items.items[i].on(
      "expand", function(){
        if ( Ext.getCmp("grid_"+this.id).store.getTotalCount()==0 ){
          Ext.getCmp("grid_"+this.id).store.load({});
          Ext.getCmp("filter_"+this.id).store.load({});
        }
        else {
          var selModel = Ext.getCmp("grid_"+this.id).selModel;
          selModel.selectRecords([selModel.getSelected()], false);
        }
        Ext.getCmp("grid_"+this.id).view.refresh();
        setGridHeight(this.id);
        setFilterWidth(this.id);
      }
    );
    Ext.getCmp("leftPanel").items.items[i].on(
      "resize", function(){
        setGridHeight(this.id);
        setFilterWidth(this.id);  
      }
    );
  }
});

function setGridHeight(id)
{
  var newHeight = Ext.getCmp(id).getInnerHeight()-(Ext.getCmp("filter_"+id).isVisible() ? 22 : 0);
  if ( newHeight!=Ext.getCmp("grid_"+id).getHeight() )
    Ext.getCmp("grid_"+id).setHeight(newHeight);
    
  var newWidth = Ext.getCmp(id).getInnerWidth()-3;
  if ( newWidth!=Ext.getCmp("grid_"+id).getWidth() )
    Ext.getCmp("grid_"+id).setWidth(newWidth);
}
function setFilterWidth(id)
{
  var filter = Ext.getCmp("filter_"+id);
  var contWidth = Ext.getCmp(id).getInnerWidth()
  filter.el.setWidth(contWidth-20);
  filter.listWidth = contWidth-4;
}
function ensureVisibleRow(grid, rowIndex)
{
  /*for (var i=12; i>=0; i--){
    if ( (rowIndex+i)<grid.getStore().getTotalCount() ){
      rowIndex += i;
      break;
    }
  }*/
  grid.view.focusRow(Math.max(0, rowIndex-1));
}
function loadMainPanel(title, url)
{
  Ext.getCmp("contAdmin").setTitle(title);
  Ext.getCmp("contAdmin").body.getUpdater().setDefaultUrl(url);
  Ext.getCmp("contAdmin").body.getUpdater().refresh();
}


function createAccueil(id, title, action)
{
  var accordionPanel = {
    id:id,
    forceLayout : true,
    title:'<b>'+title+'</b>',
    defaults:{border:false, activeTab:0},
    style : {
      overflow : "auto"
    },
    autoLoad : {
      //url : 'droits.php?iPart=0&Action='+ACTION_OPEN_INITIAL,
      url : Routing.generate('catalogue_web_page', {
              iPart : 0,
              Action : ACTION_OPEN_INITIAL
            }),
      method : 'GET', 
      scripts: true,
      text : "Chargement en cours..."
    },
    listeners : {
      "expand" : setDefaultPage
    }
  };
  return accordionPanel;
}


function createAccordion(idPart, title, action)
{
  /*
  var typeRoute = "list";
  if (iPart ==0 ){
    typeRoute = "list";
  else if (iPart==1){
    typeRoute = "form";
  }
  */
  var ROUTE_DATA_LIST = 'catalogue_administration_' + action.toLowerCase() + '_list';
  var ROUTE_DATA_FORM = 'catalogue_administration_'+ action.toLowerCase() + '_form';

  var proxyDataReader = new Ext.data.HttpProxy({
    api: { read    : /*'droits.php?iPart=0&Action='+action*/
                      ( (["PROFILES"].indexOf(action) != -1)
                      ? Routing.generate(ROUTE_DATA_LIST)
                      : Routing.generate('catalogue_web_page', {
                        iPart : 0,
                        Action : action
                      }))
    }
  });

  var storeConfig = {
    autoDestroy: true,
    loadingText : "Chargement en cours...",
    reader : new Ext.data.XmlReader({
        record: "option",
        idProperty: "@value"
      }, [{name: 'text', mapping : "@title"}, {name: 'id', mapping : "@value"}, {name : 'classname', mapping: '@classname'}]
    ),
    reader2 : new Ext.data.XmlReader({
        record: "input",
        idProperty: "@Value"
      }, [{name: 'text', mapping : "@Value"}, 
          {name: 'disabled', mapping : "@DISABLED"}, 
          {name: 'action', mapping : "@onclick"}]
    ),
    proxy : proxyDataReader,
    fields: ['id', 'text', 'classname']
  };
    
  var store2 = new Ext.data.Store(storeConfig);
  var store = new Ext.data.Store(storeConfig);
  store.on("beforeload", function (thisStore, options){
    Ext.getCmp("reload_"+idPart).setDisabled(true);
    Ext.getCmp("filter_"+idPart).setVisible( false );
    setGridHeight(idPart);
  });
  store.on("load", function (thisStore, records, options){
    Ext.getCmp("filter_"+idPart).setVisible(thisStore.getTotalCount()>5);
    var recordsAction = this.reader2.readRecords(this.reader.xmlData);
    Ext.getCmp("reload_"+idPart).setDisabled(false);
    if ( recordsAction.totalRecords>0 ){
      var dataButton = recordsAction.records[0];
      if (  Ext.getCmp("add_"+idPart) ){
        if ( dataButton.get("disabled")!="" ){
          Ext.getCmp("reload_"+idPart).destroy();
          Ext.getCmp("add_"+idPart).destroy();
        }
        else {
          Ext.getCmp("add_"+idPart).setDisabled(false);
        }
      }
    }
    else {
      if ( Ext.getCmp("add_"+idPart) ){
        Ext.getCmp("add_"+idPart).destroy();
      }
      if ( Ext.getCmp("reload_"+idPart) ){
        Ext.getCmp("reload_"+idPart).destroy();
      }
    }
    if ( recordsAction.totalRecords>1 ){
      var fbar = Ext.getCmp("grid_"+idPart).fbar;
      for (var i=1; i<recordsAction.totalRecords; i++){
        var record = recordsAction.records[i];
        var idBtn = record.get("text").toLowerCase()+"_"+idPart;
        var textBtn = record.get("text");
        var urlAction = record.get("action");
        
        if ( record.get("disabled")!="" && Ext.getCmp(idBtn) ){
          Ext.getCmp(idBtn).destroy();
        }
        if ( record.get("disabled")!="" ) continue;
        
        var handler = function(b, e){
          Ext.getCmp("contAdmin").setTitle('Gestion des '+title.toLowerCase()+" : <span style='font-weight:normal'>"+b.text+"</span>");
          Ext.Updater.updateElement(Ext.getCmp("contAdmin").body
              , Routing.generate(b.urlAction, {
                  iPart: 1,
                  Action: action,
                  Id : -999
                })
              //'droits.php?url='+b.urlAction+'&iPart=1&Action='+action+'&Id=-999'
              , "", {indicatorText:"Chargement en cours...", loadScripts : true});
        };
        if ( fbar && fbar.getComponent(idBtn)==null ){
          fbar.addButton({
            xtype : "button",
            id : idBtn,
            text : textBtn, 
            urlAction : urlAction, 
            listeners : {
              "click" : handler
            }
          });
        }
      }
      fbar.addFill( );
      fbar.doLayout(true, true);
      // MTO : BUG FIX firefox do not hide reload and add buttons after doLayout
      if (navigator.appName != "Microsoft Internet Explorer") {
        var dataButton = recordsAction.records[0];
        if (  fbar.getComponent("add_"+idPart) ){
          if ( dataButton.get("disabled")!="" ){
            fbar.getComponent("add_"+idPart).destroy();
            if ( fbar.getComponent("reload_"+idPart) ) {
            	fbar.getComponent("reload_"+idPart).destroy();
            }
          }
        }
      }
    }
    setGridHeight(idPart);
  });
  
  var urlAdd = tabPageAdd[idPart];

  var grid = new Ext.grid.GridPanel({
    id : "grid_"+idPart,
    store : store,
    border : true,
    _title_ : 'Gestion des '+title.toLowerCase(),
    colModel: new Ext.grid.ColumnModel({
      defaults: { sortable: false, resizable: false},
      columns: [{id: "data_"+idPart, header: 'Liste des '+title.toLowerCase(), dataIndex: 'text'}]
    }),
    view: new Ext.ux.grid.BufferView({
      // render rows as they come into viewable area.
      scrollDelay: false,
      //cleanDelay: 200,
      
      deferEmptyText : false,
      forceFit: true,
      showPreview: true, // custom property
      enableRowBody: true, // required to create a second, full-width row to show expanded Record data
      
      // for correlated sdrs
      getRowClass: function(record, index, rowParam, store) {
        if ( idPart==ACTION_OPEN_SUBDOMAINS ){
          if ( record.get("text").indexOf(" : ")==-1 )
            return "data-error";
        }
        if( idPart==ACTION_OPEN_LAYERS ){
          if ( record.get("classname")!="" )
            return record.get("classname");
        }
        return "";
      }
    }),
    autoExpandColumn : "data_"+idPart,
    sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
    hideHeaders : true,
    buttonAlign : 'left',
    buttons : [{
      id: "reload_"+idPart, 
      text : "Recharger la liste", 
      listeners : {
        "click" : function(b, e){
          Ext.getCmp("grid_"+idPart).getStore().reload();
          Ext.getCmp("filter_"+idPart).getStore().reload();
        }
      }
    }, {
      id: "add_"+idPart, 
      text : "Ajouter", 
      disabled : true, 
      listeners : {
        "click" : function(b, e){
          Ext.getCmp("contAdmin").setTitle('Gestion des '+title.toLowerCase()+" : <span style='font-weight:normal'>Création d'une nouvelle entrée</span>");
          Ext.Updater.updateElement(Ext.getCmp("contAdmin").body
              , Routing.generate(ROUTE_DATA_FORM, {
                  iPart: 1,
                  Action: action,
                  Id : -999
               })
              //'droits.php?url='+urlAdd+'&iPart=1&Action='+action+'&Id=-999'
              , "", {indicatorText:"Chargement en cours...", loadScripts : true});
        }
      }
    }] 
  });
  
  // sélection d'une ligne de la liste
  
  grid.selModel.on("rowselect", function(selModel, rowIndex, record){
    if ( record==null ) return;
    var grid = selModel.grid;
    Ext.getCmp("contAdmin").setTitle(grid._title_+" : <span style='font-weight:normal'>"+record.get("text")+"</span>");
    var idSelected = record.get("id");
    Ext.Ajax.request({
      url : Routing.generate('catalogue_web_page', {
        iPart: 1,
        Action: action,
        Id: idSelected
      }),
      /* url : "droits.php" , 
      params : 'iPart=1&Action='+action+"&id="+idSelected,*/
      method : 'GET',
      success: function(result){
        Ext.getCmp("contAdmin").body.dom.innerHTML = "";
        eval(result.responseText);
      },
      failure: function(result){
        Ext.MessageBox.alert('Failed', result); 
      } 
    });
  });
  grid.on("render", function selectGrid(grid){
    grid.view.on("refresh", function(view){
        var record = grid.selModel.getSelected();
        if ( record )
          ensureVisibleRow(grid, grid.store.find("id", record.get("id")));
      }, 
      grid.view, 
      {delay : 800}
    );
  });
  
  var filter = new Ext.form.ComboBox({
    mode: 'local',
    id : "filter_"+idPart,
    forceSelection : true,
    style : {"zIndex" : 100},
    displayField : "text",
    store : store2,
    loadingText : "Chargement en cours...",
    emptyText : "Rechercher...",
    enableKeyEvents : true,
    plugins: new Ext.ux.plugins.CustomFilterCombo({
      anyMatch : true,
      caseSensitive : false
    })
  });
  
  var filterHandler = function(){
    var store = this.getStore();
    store.clearFilter(true);
    var grid = Ext.getCmp(this.id.replace("filter_", "grid_"));
    var rowIndex = store.find("text", this.getValue());
    grid.selModel.selectRow(rowIndex);
    ensureVisibleRow(grid, rowIndex);
    this.setValue("");
  };
  filter.on("select", filterHandler);
  filter.on("change", filterHandler); 
  
  var accordionPanel = {
    id:idPart,
    forceLayout : true,
    title:'<b>'+title+'</b>',
    defaults:{height : 300,border:false, activeTab:0},
    style : {
      overflow : "auto"
    },
    items:[
     filter,
      grid
    ]
  };
  return accordionPanel;
}

function getActiveTabId(tabPanelId){
  var tabPanel = Ext.getCmp(tabPanelId);
  if ( !tabPanel ) return "";
  var activeTab = tabPanel.getActiveTab();
  if ( !activeTab ) return "";
  return activeTab.getId();
}
function reloadPanel(panelId, idPart)
{
  var oPanel = Ext.getCmp(panelId);
  if ( !oPanel ) return;
  oPanel.removeAll(true);
  oPanel.body.getUpdater().refresh();
  if ( idPart==ACTION_OPEN_DOMAINS || idPart==ACTION_OPEN_SUBDOMAINS ){
    parent.reloadTree();
  }
}
function reloadGrid(idPart, selectedId, bSelect)
{
  var oReloadBtn = Ext.getCmp("reload_"+idPart);
  var grid = Ext.getCmp("grid_"+idPart);
  var filter = Ext.getCmp("filter_"+idPart);
  if ( !oReloadBtn ) return;
  
  if ( bSelect==undefined )
    bSelect = true;
  if ( bSelect ){
    var record = new Ext.data.Record({id:selectedId, text:""}, {name:"id"});
    var rowIndex = grid.getStore().find("id", selectedId);
    if ( rowIndex!=-1 ){// update
      grid.selModel.selectRow(rowIndex);
    }
    else {
      if ( selectedId==NEWROW ){//delete
        grid.store.remove(record);
      }
      else {//insert
        grid.store.add([record]);
        var rowIndex = grid.getStore().find("id", selectedId);
        grid.selModel.selectRow(rowIndex);      
      }
    }
    
    grid.getStore().on("load", function (store, records, options){
      var rowIndex = store.find("id", selectedId);
      if ( rowIndex!=-1 ){
        grid.selModel.selectRow(rowIndex);
      }
      else {
        loadMainPanel(grid._title_+" : <span style='font-weight:normal'>Veuillez sélectionner une entrée</span>", emptyPage);
      }
    }, grid.getStore(), {single:true});
  }
  oReloadBtn.fireEvent("click", {});
  
  if ( idPart==ACTION_OPEN_DOMAINS || idPart==ACTION_OPEN_SUBDOMAINS ){
    parent.reloadTree();
  }
}

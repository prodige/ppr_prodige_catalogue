/**
 * @brief déconnexion de l'utilisateur courant
 * 
 */
function	deconnexion_init(bReturnCatalogue)
{
	var		identification;
	var		barre_onglets;
	var		onglets;
	var		i;
	var		baseClassName = "consultation";
	var		onglet = null;
	
	try
	{
		//parent.IHM_historique_add(window.location);
		parent.IHM_Chargement_stop();
	}
	
	catch(e){}
	deconnexion_onfocus();
	parent.panier_vider();
	identification = document.getElementById("identification");
		if (identification)
			identification.className = "identification_hide";
		try
		{
			parent.IHM_Onglets_activate(true, false, false);
			if(bReturnCatalogue){
				parent.IHM_Onglets_selWithUrl('catalogueresume', 'catalogueresume.php');
			}
		}
		catch(e){}
}

/**
 * @brief permet de cacher le choix de domaine
 */
function	deconnexion_onfocus()
{
	try
	{
		parent.IHM_Domaines_hide();
	}
	catch(e)
	{}
}

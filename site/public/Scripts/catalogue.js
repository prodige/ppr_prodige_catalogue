/**
 * @brief Initialisation de la page
 * @return
 */
function	catalogue_init()
{
	try
	{
		var		re1 = new RegExp("geonetwork", "i");

		if (re1.test(window.location))
		{
			chemin = window.location.pathname;
			pos = chemin.lastIndexOf('/');
			if (pos >= 0)
				chemin = chemin.substr(pos + 1);
			switch(chemin)
			{
				case "metadata.show":
					lastChemin = parent.IHM_historique_getLastUrl();
					pos = lastChemin.indexOf('?');
					if (pos >= 0)
						lastChemin = lastChemin.substr(0, pos);
					pos = lastChemin.lastIndexOf('/');
					if (pos >= 0)
						lastChemin = lastChemin.substr(pos + 1);
					if (lastChemin != chemin)
						parent.IHM_historique_add(window.location);
					break;
					
				case "main.search":
					break;
				default:
					parent.IHM_historique_add(window.location);
					break;
			}
		}
		else
			parent.IHM_historique_add(window.location);
		parent.IHM_Chargement_stop();
		parent.domaines_event_onchange_add(parent.events_catalogue_changedomaine);
	}
	catch(e)
	{}
	//date = new Date(2000, 1, 1);
	//document.cookie = "JSESSIONID=; expires=" + date.toGMTString() + "; path=/geosource";
	catalogue_onfocus();
	try
	{
		var	sousdomaine;

		sousdomaine = parent.domaines_getCurentSousDomaine();
		if (sousdomaine)
		{
			blocksdom = document.getElementById('admin_sousdomaine');
			if (blocksdom)
				blocksdom.style.visibility = 'visible';
		}
	}
	catch(e)
	{}
	catalogue_domaineInit();
}

function	catalogue_onfocus()
{
	var		domaine;

	try
	{
		parent.IHM_Domaines_resume();
	}
	catch(e){}
}

function	catalogue_domaineInit()
{
	var		re1 = new RegExp("geosource", "i");

	if (!re1.test(window.location))
	{
		catalogue_domaineChange();
		return;
	}
}

/**
 * @brief Gestion du clic su run autre domaine
 * @return
 */
function	catalogue_domaineChange()
{
	var		domaine;
	var		sousdomaine;
	var		pos;
	var		path;
	try
	{
		domaine = parent.domaines_getCurentDomaine();
		sousdomaine = parent.domaines_getCurentSousDomaine();
		if (domaine)
			catalogue_loadData(domaine, sousdomaine);
		else
		{
			parent.IHM_Panier_hide();
			parent.IHM_Chargement_start();
			pos = window.location.pathname.lastIndexOf("/");
			if (pos > 0)
				path = parent.location.pathname.substr(0, pos + 1);
			else
				path = "/";
			window.location = path + "catalogueresume.php";
		}
	}
	catch(e){}
}

/**
 * @brief Affichage des données du catalogue pour un sous domaine/ domaine
 * @param domaine
 * @param sousdomaine
 * @return
 */
function	catalogue_loadData(domaine, sousdomaine)
{
	var		url;
	var ongletDonnees = parent.Ext.getCmp("donnees");
	parent.IHM_Onglets_sel(ongletDonnees, '', false, false); 
	if (domaine)
	{
		url = escape(domaine);
		if (sousdomaine)
			url += " " + escape(sousdomaine);
		if (url != "")
		{
			try
			{
				parent.IHM_Chargement_start();
			}
			catch(e){}
			window.location = "/"+geonetwork_dir+"/srv/main.search?extended=on&remote=off&attrset=geo&any=" + url;
		}
	}
}

function	catalogue_fullEscape(chaine)
{
	var		rslt = "";
	var		i;

	for (i = 0; i < chaine.length; i++)
		rslt += "%" + chaine.charCodeAt(i).toString(16);
	return rslt;
}

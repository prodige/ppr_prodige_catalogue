
/**
 * @brief Initialisation de la page de gestion des bases de données
 * @return
 */
function	administration_init()
{
	try
  {
    parent.IHM_historique_add(window.location);
    parent.IHM_Chargement_stop();
  }
  catch(e){}
  administration_onfocus();
}


function	administration_onfocus()
{
	try
	{
		parent.IHM_Domaines_hide();
	}
	catch(e)
	{}
}

/**
 * @brief Sélection d'une catégorie
 * @param lien
 * @return
 */
function	administration_selectCategory(lien)
{
	var		domaine;
	var		sousdomaine;
	var		url = lien.href;

	parent.IHM_Chargement_start();
	try
	{
		if ((url.indexOf("?domaine=") < 0) || (url.indexOf("&domaine=") < 0))
		{
			domaine = parent.domaines_getCurentDomaine();
			sousdomaine = parent.domaines_getCurentSousDomaine();
			if (domaine)
			{
				if (url.indexOf("?") < 0)
					url += "?";
				else
					url += "&";
				url += "domaine=" + escape(domaine);
				if (sousdomaine)
					url += "&sousdomaine=" + escape(sousdomaine);
			}
		}
	}
	catch(e)
	{}
	lien.href = url;
	//window.location = url;
}

function	administration_selectAdminCarto()
{
	parent.IHM_Chargement_start();
	window.open("AdministrationCarto/?map=POSTGIS_RA.map", "Administration_cartographie_dynamique", "directories=0, fullscreen=0, menubar=0, status=1, toolbar=0, resizable=1");
}

/**
 * @brief Fonction pour le changement de base de données de carto à catalogue ou inversement
 * @return
 */
function administration_database_changeDB()
{
  var f=document.select_database;
  f.action='';
  f.target='';
  f.submit();
}

/**
 * @brief Gère le changement de requête
 * @return
 */
function ChangeRequest()
{
  var f=document.form_sql;
  f.action='print_request.php';
  f.target='footerExec';
  f.submit();
}

/**
 * @brief Gère l'affichage de la requête par défaut sur une vue ou une table
 * @return
 */
function DefaultRequest(table_type)
{
  var f=document.form_sql;
  f.action='print_request.php?table_type='+table_type;
  f.target='footerExec';
  f.submit();
}

/**
 * @brief Exécute le sql
 * @return
 */
function Confirmation() 
{ form=document.form_sql;
//check the type of request
//if delete or update send confirmation message
  var strSql=form.request_sql.value;
  if (strSql.toLowerCase().substr(0, 6) == "update"  || strSql.toLowerCase().substr(0, 7) == "(update"){
    if (confirm('Voulez-vous vraiment modifier cet ou ces enregistrement(s)?')==false) return;
  }
  if (strSql.toLowerCase().substr(0, 6) == "delete"  || strSql.toLowerCase().substr(0, 7) == "(delete"){
    if (confirm('Voulez-vous vraiment supprimer cet ou ces enregistrement(s)?')==false) return;
  }
  form.action="iframe_sql.php";
  form.target="";
  form.submit();
  return;
}
/**
 * @brief Efface le contenu du cadre de requêtes
 * @return
 */
function InitAll()
{
  document.form_sql.request_sql.value = "";
}
/**
 * @brief Sauvegarde la requête (ouvre une popup)
 * @return
 */
function SaveRequest()
{
  form=document.form_sql;
  var request=encodeURI(form.request_sql.value);
  var request_id=form.request_id.value;
  var t = (screen.height -100 ) / 2;
  var l = (screen.width -300 ) / 2;
  form.action="administration_database_save.php";
  form.target="popup";
  window.open("administration_database_save.php?", form.target,'status=yes,scrollbars=auto,resizable=yes,height='+150+',width='+400+',top='+t+',left='+l);
  form.submit();
}
/**
 * @brief Efface une requête
 * @return
 */
function DelRequest(){
  var f=document.form_sql;
  f.action='del_request.php';
  f.target='footerExec';
  f.submit();
}
/**
 * @brief Supprime une valeur d'une liste SELECT
 * @param objSelect
 * @param bReturn
 * @return
 */
function SupprVal(objSelect, bReturn) 
{
  // supprime de la liste, l'element de la liste d'indice idSuppr
  var idSuppr = objSelect.selectedIndex;
  var nbElt = objSelect.length;
  var strValSuppr = objSelect.options[idSuppr].value;
  if( idSuppr==0 || idSuppr==nbElt || nbElt<2 )
    return "-1";
  for(var i=idSuppr; i<objSelect.length-1; i++) {
    objSelect.options[i].value = objSelect.options[i+1].value;
    objSelect.options[i].text = objSelect.options[i+1].text;
   }
  objSelect.length = nbElt - 1;
  if( bReturn )  return strValSuppr;
}
/**
 * @brief Ajoute une option à une liste SELECT
 * @param objSelect
 * @param val
 * @param txt
 * @return
 */
function AddOptionToSelect(objSelect, val, txt)
{
  objSelect.length=objSelect.length+1;
  var nbElt = objSelect.length;
  objSelect.options[nbElt-1].value = val;
  objSelect.options[nbElt-1].text = txt;
  objSelect.selectedIndex = nbElt-1;
}
/**
 * @brief Exporte au format Excel les résultats d'une requête
 * @param strSql
 * @return
 */
function ExcelExport(strSql)
{
    window.open("exportExcel.php?strSql="+strSql, "footerExec","");
}
/**
 * @brief gestion du clic sur une autre table pour afficher sa structure
 * @return
 */
function ChangeTable(){
  var f=document.form_consult;
  f.action='print_tableStructure.php?table_type=table';
  f.target='footerExec';
  f.submit();
}
/**
 * @brief gestion d uclic sur une autre vue pour afficher sa structure
 * @return
 */
function ChangeView(){
  var f=document.form_consult;
  f.action='print_tableStructure.php?table_type=view';
  f.target='footerExec';
  f.submit();
}

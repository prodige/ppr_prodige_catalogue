/**
 * @brief Supprime un élément du panier
 * @param evt
 * @return
 */
function  panier_supprimerElement(evt)
{
  
  
  /*var    element;
  var    panier;
  var    ligne;
  var    i;
  var    id;
  var    identifiant = null;
  
  if (navigator.appName == "Microsoft Internet Explorer")
  {
    evt = window.event;
    element = evt.srcElement;
  }
  else
    element = evt.target;
  while (element && (element.tagName != "TR"))
    element = element.parentNode;
  if (element && (element.tagName == "TR"))
    identifiant = panier_getElementId(element);
  panier = panier_getPanier();
  if (panier && identifiant)
  {
    for (i = 0; (i < panier.rows.length) && identifiant; i++)
    {
      ligne = panier.rows[i];
      id = panier_getElementId(ligne);
      if (id == identifiant)
      {
        identifiant = null;
        panier.deleteRow(i);
      }
    }
  }
  IHM_InfosBulle_hide(evt);*/
}

/**
 * @brief vide les éléments du panier
 * @param oBt   objet bouton Ext, permet de différencier un appel utilisateur d'un appel programmé
 */
function  panier_vider(oBt)
{
  if ( Ext.getCmp("gridPanier").getStore().getCount() > 0 ) {
    Ext.getCmp("gridPanier").getStore().removeAll();
  } else if ( oBt ) {
    Ext.Msg.alert('Panier','Le panier ne contient aucune couche de données.<br>Veuillez ajouter une (ou des) couche(s) à votre panier.');
  }
  
  /*
  var    panier;
  var    ligne;
  
  panier = panier_getPanier();
  if (panier)
  {
    for (i = 0; i < panier.rows.length; i++)
    {
      if (panier.rows[i]){
        ligne = panier.rows[i];
        id = panier_getElementId(ligne);
        panier.deleteRow(i);
      }
    }
  }
  */
}

/**
 * @brief Visualise un élément du panier
 * @param evt
 * @return
 */
function  panier_visualiserElement(evt)
{
  var    element;
  var    panier;
  var    identifiant = null;
  
  if (navigator.appName == "Microsoft Internet Explorer")
  {
    evt = window.event;
    element = evt.srcElement;
  }
  else
    element = evt.target;
  while (element && (element.tagName != "TR"))
    element = element.parentNode;
  if (element && (element.tagName == "TR"))
    identifiant = panier_getElementId(element);
  if (identifiant)
  {
    try
    {
      IHM_Fiche_Show(identifiant);
    }
    catch(e){}
  }
  IHM_InfosBulle_hide(evt);
}
/**
 * @brief Vérifie si l'élément existe déjà dans le panier
 * @param identifiant
 * @return boolean
 */
function  panier_elementExist(identifiant)
{
  var    panier;
  var    ligne;
  var    i;
  var    id;
  var    bRslt = false;
  
  panier = panier_getPanier();
  if (panier && identifiant)
  {
    for (i = 0; (i < panier.rows.length) && !bRslt; i++)
    {
      ligne = panier.rows[i];
      id = panier_getElementId(ligne);
      if (id == identifiant)
        bRslt = true;
    }
  }
  return bRslt;  
}

var lastError = [];
/**
 * @brief Ajoute un élément au panier
 * @param identifiant
 * @param titre
 * @return
 */
function  panier_ajouterElement(uuid, identifiant, titre, record, bCumulError)
{
  var    panier;
  var    ligne;
  var    td1, td2;
  var    img, inpt;
  
  if ( bCumulError == undefined ) bCumulError = false;
  
  if ( uuid==undefined && identifiant==undefined && titre==undefined && bCumulError ){
    if ( typeof lastError == "undefined" ) return;
    if ( lastError.length == 0 ) return;
    
    var strMsg = "";
    if ( lastError.length == 1 ){
      strMsg = 'La couche '+lastError[0]+' est déjà présente dans le panier.';
    }
    else {
      strMsg = 'Les couches : ';
      for (var i=0; i<lastError.length; i++){
        strMsg += "<br/> - "+lastError[i];
      }
      strMsg += '<br/> sont déjà présentes dans le panier.';
    }
    Ext.Msg.alert('Panier', strMsg);    
    lastError = [];
    return;
  }
  
  var DataRecord = Ext.data.Record.create([{
    name: 'Data',
    name: 'record',
    id : 'id'
    }
  ]);
  
  if ( Ext.getCmp("contPanier").timerCollapse ){
    clearTimeout(Ext.getCmp("contPanier").timerCollapse);
    Ext.getCmp("contPanier").timerCollapse = null;
  }
  
  if(Ext.getCmp("gridPanier").getStore().findExact("id", identifiant)>-1){
    
    if ( bCumulError ){
      lastError.push(titre);
    }
    else
     Ext.Msg.alert('Panier', 'La couche '+titre+' est déjà présente dans le panier.');
     
  }else{
    Ext.getCmp("gridPanier").getStore().add(
        new DataRecord({
          Data: titre,
          id : identifiant,
          record: record
        })
    );
    Ext.getCmp("contPanier").expand(true);
    Ext.getCmp("contPanier").timerCollapse = setTimeout('Ext.getCmp("contPanier").collapse(true);', 3000);
  }
    //windowGrid.getStore().add(r);

  /*
  if (panier_elementExist(identifiant))
  {
    alert("La couche est déjà présente dans le panier");
    return;
  }
  panier = panier_getPanier();
  if (panier && identifiant && titre)
  {
    // Suppression de la ligne vide (compatibilitÃ© HTML4.01 Transitionnal)
    if (panier.rows.length == 1)
    {
      if (panier.rows[0].cells.length == 1)
        panier.deleteRow(0);
    }
    
    ligne = panier.insertRow(-1);
    if (ligne)
    {
      td1 = ligne.insertCell(-1);
      td2 = ligne.insertCell(-1);
      if (td1)
      {
        node = document.createTextNode(titre);
        td1.appendChild(node);
        td1.className = "visualiser";
        td1.onclick = panier_visualiserElement;
        td1.onmouseout = IHM_InfosBulle_hide;
        td1.onmousemove = panier_InfosBulle_visualiser;
        inpt = document.createElement("input");
        inpt.type = "hidden";
        inpt.name = "panier_" + ligne.rowIndex;
        inpt.value = identifiant;
        td1.appendChild(inpt);
      }
      if (td2)
      {
        td2.className = "supprimer";
        td2.onclick = panier_supprimerElement;
        td2.onmouseout = IHM_InfosBulle_hide;
        td2.onmousemove = panier_InfosBulle_supprimer;
      }
    }
  }*/
}


/**
 * @brief Affiche l'info bulle visualiser
 * @param evt
 * @return
 */
function  panier_InfosBulle_visualiser(evt)
{
  IHM_InfosBulle_show(evt, "Consulter la fiche");
}

/**
 * @brief Affiche l'info bulle supprimer du panier
 * @param evt
 * @return
 */
function  panier_InfosBulle_supprimer(evt)
{
  IHM_InfosBulle_show(evt, "Supprimer du panier");
}
/**
 * @brief retourne l'identifiant de l'élément
 * @param ligne
 * @return
 */
function  panier_getElementId(ligne)
{
  var    elems;
  var    id = null;
  
  if (ligne && (ligne.cells.length == 2))
  {
    elems = ligne.cells[0].getElementsByTagName("input");
    if (elems && (elems.length == 1))
       id = elems[0].value;
  }
  return id;
}
/**
 * @brief renvoie le contenu du panier
 * @return
 */
function  panier_getPanier()
{
  var    div;
  var    tables;
  var    panier = null;
  
  div = document.getElementById("panier");
  if (div)
  {
    tables = div.getElementsByTagName("TABLE");
    if (tables && (tables.length == 1))
      panier = tables[0];
  }
  return panier;
}

/**
 * MPA: 6 mai 2006
 * @brief Fonction qui permet d'acceder au parametrage du telechargement
 */
function  panier_downloadParametrage()
{
  
 /* var bHasRecord = false;
  var l_url  = "panierDownloadFrontal_parametrage.php?LAYERIDTS=";
  //console.log(Ext.getCmp("gridPanier").getStore().data);
  for(idx = 0; idx < Ext.getCmp("gridPanier").getStore().data.length; idx++){
    bHasRecord = true;
    l_url += Ext.getCmp("gridPanier").getStore().data.items[idx].data.id;
    if (idx!=Ext.getCmp("gridPanier").getStore().data.length-1){
      l_url +="|";
    }
  }
  if(bHasRecord){
    contenu = window.frames["fenetre"];
    if (contenu)
    {
      //parent.IHM_historique_add(l_url);
      contenu.location = l_url;
    }
  }else{
    Ext.Msg.alert('Panier','Le panier ne contient aucune couche de données.<br>Veuillez ajouter une (ou des) couche(s) à votre panier.');
  }*/
  /**
   * Passage des parametres en POST
   **/
  var bHasRecord = false;
  var l_form = document.createElement("form");
  var l_form_input = "";
  l_form.setAttribute('name', 'l_form');
  l_form.setAttribute('action', 'panierDownloadFrontal_parametrage.php');
  l_form.setAttribute('method', 'post');
  l_form.setAttribute('target', 'fenetre');
  var layerIdtsValue = "";
  for(idx = 0; idx < Ext.getCmp("gridPanier").getStore().data.length; idx++){
	layerIdtsValue += Ext.getCmp("gridPanier").getStore().data.items[idx].data.id;
	if(idx != Ext.getCmp("gridPanier").getStore().data.length-1){
	  layerIdtsValue += "|";
	}
  }
    bHasRecord = true;
    l_form_input = document.createElement("input");
    l_form_input.setAttribute("type", "text");
    l_form_input.setAttribute("name", "LAYERIDTS");
    l_form_input.setAttribute("value", layerIdtsValue);
    //l_form_input.setAttribute("name", "l_form_input_"+idx);
    //l_form_input.setAttribute("value", Ext.getCmp("gridPanier").getStore().data.items[idx].data.id);
    l_form.appendChild(l_form_input);
  //}
  var container = document.getElementById("fenetre");
  container.appendChild(l_form);
  if(bHasRecord){ 
    contenu = window.frames["fenetre"];
    if(contenu){
      l_form.submit();
    }
  }else{
    Ext.Msg.alert('Panier', 'Le panier ne contient aucune couche de données.<br>Veuillez ajouter une (ou des) couche(s) à votre panier.');
  }

  /*var    panier     = null;
  var    ligne     = null;
  var   coucheIdt   = "";
  var   coucheIdts   = "";
  var   idx     = 0;
  var    l_url    = "";
  var    ongSelected  = null;
  
  if (panier_hasCouches())
  {
    panier = panier_getPanier();
    for (idx = 0; idx < panier.rows.length; idx++)
    {
      ligne = panier.rows[idx];
      coucheIdt = panier_getElementId(ligne);
      if (coucheIdts != "") coucheIdts += "|";
      coucheIdts += coucheIdt;
    }
    l_url  = "panierDownloadFrontal_parametrage.php?";
    l_url += "LAYERIDTS="+coucheIdts;
    
    ongSelected = IHM_Onglets_get(IHM_Onglets_getSel());
    IHM_Onglets_unsel(ongSelected);
    
    if (navigator.appName == "Microsoft Internet Explorer")
      contenu = window.frames("fenetre");
    else
      contenu = window.frames["fenetre"];
    if (contenu)
    {
      parent.IHM_historique_add(l_url);
      contenu.location = l_url;
    }
  }
  else{
    alert("Le panier ne contient aucune couche de données.\nAucun téléchargement n'est accessible.\n\nVeuillez ajouter une (ou des) couche(s) à votre panier.");
  }*/
}

/**
 * MPA: 6 mai 2006
 * @brief Fonction qui permet de savoir si le panier contient des couche
 */
function  panier_hasCouches()
{
  var    panier = null;
  var b_HasElement = false;
  
  panier = panier_getPanier();
  if (panier){
    if (panier.rows.length == 1) {
      //on verifie que le panier contient bien une couche
      if (panier.rows[0].cells.length == 2) b_HasElement = true;
    }
    else if (panier.rows.length > 1) b_HasElement = true;
  }
  return b_HasElement;
}

/**
 * MTO: 5 juin 2012
 * @brief Fonction qui permet de covisualiser le panier
 */
var nbRecordsCovisu = 0;
var tabRecordsCanCovisu = [];
var tabDatasCannotCovisu = [];
var tabParamsCovisu = [];
function panier_covisualiser()
{
  nbRecordsCovisu = Ext.getCmp("gridPanier").getStore().getCount();
  if ( nbRecordsCovisu > 0 ) {
    Ext.getCmp("gridPanier").getStore().each(function(){
      var type = this.get("record").json.type[0].value || this.get("record").json.type;
      // donnée de typ dataset
      if ( type == "dataset" ) {
        var oParam = {
          id:this.get("id"),
          layerTitle:this.get("Data"),
          isHarvested:this.get("record").get("isharvested")
        };
        // donnée table
        if ( this.get("record").json.spatialRepresentationType &&  this.get("record").json.spatialRepresentationType[0].value == "textTable" ) {
          tabDatasCannotCovisu.push(this.get("Data"));
          nbRecordsCovisu--;
        }
        // donnée moissonnée
        else if ( this.get("record").get("isharvested") == "y" ) {
          var urlRelation = document.location.protocol+"//"+document.location.host+"/"+geonetwork_dir+"/srv/fre/xml.relation";
          var paramsRelation = {type:"service", fast:false, uuid:this.get("record").get("uuid")};
          var iframe = {};
          iframe.tabParams = [this.get("record")];
          iframe.onSuccess = function(record, res){
            try { // code for IE
      			var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
      			xmlDoc.async = "false";
      			xmlDoc.loadXML(res);
      		}catch(error) { // code for Mozilla, Firefox, Opera, etc.
      			try {
      				var parser = new DOMParser();
      				var xmlDoc = parser.parseFromString(res,"text/xml");
      			}catch(error) {
      				alert(error.message);
      				return;
      			}
      		}  	
      		//console.log(xmlDoc);
            var tabService = Ext.DomQuery.select("relations/relation", xmlDoc);
            //console.log(tabService);
            if ( tabService.length > 0 ) {
              oParam.uuidService = "";
              for ( var i=0; i<tabService.length; i++ ) {
            	  oParam.uuidService += Ext.DomQuery.selectValue("uuid", tabService[i])+"|";
              }
              oParam.uuidData = record.get("uuid");
              tabRecordsCanCovisu.push(record);
              tabParamsCovisu.push(oParam);
              nbRecordsCovisu--;
              panier_covisualiser_confirm();
            } else {
              var tabLinks = record.get("links");
              if ( tabLinks.length > 0 ) {
                var oLink = {};
                Ext.each(tabLinks, function(link){
                  var _link = {};
                  if ( typeof(link) == 'string' ) { // cas particulier où l'ajout au panier a été effectué par une sélection (case à cocher) => les liens ne sont pas directement accessibles)
                    var tabSplit = link.split("|");
                    _link.name = tabSplit[0] || "";
                    _link.href = tabSplit[2] || "";
                    _link.protocol = tabSplit[3] || "";
                  } else {
                    _link = link;
                  }
                  if ( _link.protocol && _link.protocol != "" && new RegExp(/^OGC:WMS/i).test(_link.protocol) ) {
                    if ( _link.href && _link.href != "" && _link.name && _link.name != "" ) {
                      oLink.wms = {
                        protocol:"GetMap",
                        connection:_link.href,
                        layer:_link.name
                      }
                    }
                  }
                  if ( _link.protocol && _link.protocol != "" && new RegExp(/^OGC:WFS/i).test(_link.protocol) ) {
                    if ( _link.href && _link.href != "" && _link.name && _link.name != "" ) {
                      oLink.wfs = {
                        protocol:"GetMap",
                        connection:_link.href,
                        layer:_link.name
                      }
                    }
                  }
                }, this);
                if ( oLink.wms ) {
                  oParam.protocol = oLink.wms.protocol;
                  oParam.connection = oLink.wms.connection;
                  oParam.layer = oLink.wms.layer;
                  tabRecordsCanCovisu.push(record);
                  tabParamsCovisu.push(oParam);
                  nbRecordsCovisu--;
                  panier_covisualiser_confirm();
                } else if ( oLink.wfs ) {
                  oParam.protocol = oLink.wfs.protocol;
                  oParam.connection = oLink.wfs.connection;
                  oParam.layer = oLink.wfs.layer;
                  tabRecordsCanCovisu.push(record);
                  tabParamsCovisu.push(oParam);
                  nbRecordsCovisu--;
                  panier_covisualiser_confirm();
                } else {
                  tabDatasCannotCovisu.push(record.get("title"));
                  nbRecordsCovisu--;
                  panier_covisualiser_confirm();
                }
              } else {
                tabDatasCannotCovisu.push(record.get("title"));
                nbRecordsCovisu--;
                panier_covisualiser_confirm();
              }
            }
          }
          AjaxRequest(urlRelation, paramsRelation, iframe);
        }
        // donnée non moissonnée
        else if ( this.get("record").get("isharvested") == "n" ) {
          var urlUserRight = "Services/getUserRights.php";
          var paramsUserRight = {ID:this.get("id"),TRAITEMENTS:"NAVIGATION",OBJET_TYPE:type,OBJET_STYPE:( type == "service" && this.get("record").json.serviceType ? this.get("record").json.serviceType[0].value || this.get("record").json.serviceType : "" )};
          var iframe = {};
          iframe.tabParams = [this.get("record")];
          iframe.onSuccess = function(record, res){
            var json_res = eval("("+res+")");
            if ( json_res.NAVIGATION ) {
              tabRecordsCanCovisu.push(record);
              tabParamsCovisu.push(oParam);
              nbRecordsCovisu--;
              panier_covisualiser_confirm();
            } else {
              tabDatasCannotCovisu.push(record.get("title"));
              nbRecordsCovisu--;
              panier_covisualiser_confirm();
            }
          };
          AjaxRequest(urlUserRight, paramsUserRight, iframe);
        }
        // moissonnage non défini
        else {
          tabDatasCannotCovisu.push(this.get("Data"));
          nbRecordsCovisu--;
        }
      }
      // autre type de donnée
      else {
        tabDatasCannotCovisu.push(this.get("Data"));
        nbRecordsCovisu--;
      }
    });
    panier_covisualiser_confirm();
  } else {
    Ext.Msg.alert('Panier','Le panier ne contient aucune couche de données.<br>Veuillez ajouter une (ou des) couche(s) à votre panier.');
  }
}

/**
 * MTO: 5 juin 2012
 * @brief Fonction qui permet d'afficher les données non visualisable et de continuer
 */
function panier_covisualiser_confirm()
{
  if ( nbRecordsCovisu == 0 ) {
    if ( tabRecordsCanCovisu.length > 0 ) {
      if ( tabDatasCannotCovisu.length > 0 ) {
        var iframe = {};
        iframe.tabParams = [];
        iframe.onSuccess = panier_covisualiser_confirmed;
        Ext.Msg.confirm('Panier', 'Les données '+tabDatasCannotCovisu.join(', ')+' ne sont pas visualisables.<br/>Souhaitez-vous continuer ?', confirm, iframe);
        tabDatasCannotCovisu = [];
      } else {
        panier_covisualiser_confirmed();
      }
    } else {
      Ext.Msg.alert('Panier', 'Aucune donnée du panier n\'est covisualisable.');
    }
  }
}

/**
 * MTO: 5 juin 2012
 * @brief Fonction qui permet de préparer les données avant covisualisation
 */
function panier_covisualiser_confirmed()
{
  var d = new Date();
  var curr_time = d.getTime()	
  openWindowWithPost("covisualisation.php", "covisu"+curr_time, tabParamsCovisu);
  tabParamsCovisu = [];
}

/**
 * @brief Ouvre un nouvel onglet avec passage des paramètres en post
 * @param tabParams tableau des paramètres (chaque paramètre est un objet)
 */
function openWindowWithPost(url,name,tabParams)
{
  var newWindow = window.open(url, name);
  if (!newWindow) return false;
  var html = "";
  html += "<html><head></head><body><form id='form_covisu' method='post' action='"+url+"'>";
  if ( tabParams && tabParams.length > 0 ) {
    for ( var i=0; i < tabParams.length; i++) {
      for ( var key in tabParams[i] ) {
        html += "<input type=\"hidden\" name=\"data["+i+"]["+key+"]\" value=\""+tabParams[i][key]+"\"/>";  // TODO : effectuer un "htmlentities"
      }
    }
  }
  html += "</form><script type='text/javascript'>document.getElementById(\"form_covisu\").submit()</script></body></html>";
  newWindow.document.write(html);
  return newWindow;
}
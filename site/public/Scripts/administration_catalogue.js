
/**
 * @brief Initialisation de la page catalogue
 * @return
 */
function	administration_catalogue_init()
{
	try
	{
		parent.IHM_historique_add(window.location);
		parent.IHM_Chargement_stop();
		parent.domaines_event_onchange_add(parent.events_administration_catalogue_changedomaine);
	}
	catch(e){}
	administration_catalogue_onfocus();
	administration_catalogue_domaineInit();
}

function	administration_catalogue_onfocus()
{
	var		domaine;

	try
	{
		parent.IHM_Domaines_resume();
	}
	catch(e){}
}
/**
 * @brief Initialisation des domaines
 * @return
 */
function	administration_catalogue_domaineInit()
{
	var		re1 = new RegExp("geosource", "i");

	if (!re1.test(window.location))
		administration_catalogue_domaineChange();
}
/**
 * @brief Fonction gérant le changement de domaine
 * @return
 */
function	administration_catalogue_domaineChange()
{
	var		domaine;
	var		sousdomaine;
	var		pos;
	var		path;

	try
	{
		domaine = parent.domaines_getCurentDomaine();
		sousdomaine = parent.domaines_getCurentSousDomaine();
		if (domaine)
			administration_catalogue_loadData(domaine, sousdomaine);
		else
		{
			parent.IHM_Panier_hide();
			parent.IHM_Chargement_start();
			pos = window.location.pathname.lastIndexOf("/");
			if (pos > 0)
				path = parent.location.pathname.substr(0, pos + 1);
			else
				path = "/";
			window.location = path + "catalogueresume.php";
		}
	}
	catch(e){}
}
/**
 * @brief Affichage des données concernant le sous-domaine et le domaine
 * @param domaine
 * @param sousdomaine
 * @return
 */
function	administration_catalogue_loadData(domaine, sousdomaine)
{
	var		url;

	if (domaine)
	{
		url = administration_catalogue_fullEscape(domaine);
		if (sousdomaine)
			url += " " + administration_catalogue_fullEscape(sousdomaine) + "&sousdomaine=" + administration_catalogue_fullEscape(sousdomaine);
		if (url != "")
		{
			parent.IHM_Chargement_start();
			window.location = "/geosource/srv/fre/GS_main.search?extended=off&remote=off&attrset=geo&themekey=" + url;
		}
	}
}


function	administration_catalogue_fullEscape(chaine)
{
	var		rslt = "";
	var		i;

	for (i = 0; i < chaine.length; i++)
		rslt += "%" + chaine.charCodeAt(i).toString(16);
	return rslt;
}

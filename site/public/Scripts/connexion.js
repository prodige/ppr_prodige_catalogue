/**
 * @brief Initialise la connexion (appelé sur le onload de la page de connexion)
 * 
 */
function	connexion_init()
{
	var		identification;
	var		barre_onglets;
	var		onglets;
	var		i;
	var		baseClassName = "consultation";
	var		onglet = null;
	
	try
	{
		//parent.IHM_historique_add(window.location);
		parent.IHM_Chargement_stop();
	}
	catch(e){}
	connexion_onfocus();
	if (connexion_isValid())
	{
		
	  identification = document.getElementById("identification");
		if (identification)
			identification.className = "identification_hide";
		try
		{
		  parent.IHM_Onglets_activate(false, true, true);
			parent.IHM_historique_goBack();
		}
		catch(e){}
	}
}

/**
 * @brief Action sur le onfocus de l'onglet connexion
 */
function	connexion_onfocus()
{
	try
	{
		parent.IHM_Domaines_hide();
	}
	catch(e)
	{}
}

/**
 * @brief Validation de la connexion
 * 
 */
function	connexion_valider()
{
	parent.IHM_Chargement_start();
	document.forms[0].MOTDEPASSE.value = hex_md5(document.forms[0].MOTDEPASSE2.value);
	document.forms[0].MOTDEPASSE2.value ="";

	document.forms[0].submit();
}

/**
 * @brief Validation de la connexion
 * 
 */
function	connexion_valider_mapfile()
{
	//parent.IHM_Chargement_start();
	var f = eval("document.formIdent");
	
	MOTDEPASSE = document.getElementById("MOTDEPASSE");
	MOTDEPASSE2 = document.getElementById("MOTDEPASSE2");

	f.MOTDEPASSE.value = hex_md5(MOTDEPASSE2.value);
    f.submit();
}


/**
 * retourne vrai si l'utilisateur est connecté
 */
function	connexion_isValid()
{
	return userIsConnected;
}

/**
 * @brief Validation de la connexion
 * 
 */
function chgpwd_valider()
{	
	if(document.forms[0].CHGMOTDEPASSE2.value != document.forms[0].CHGMOTDEPASSE3.value){
		alert("Les deux mots de passe ne correspondent pas !");
		return false;
	}else if(document.forms[0].CHGMOTDEPASSE2.value.length < 6){
		alert("Le mot de passe doit comporter 6 caractères minimum !");
		return false;
	}else if(document.forms[0].CHGMOTDEPASSE2.value.match(/^[a-z]+$/i)){
		alert("Le mot de passe doit comporter au moins un caractère spécial ou un chiffre !");
		return false;
	}else{
		document.forms[0].CHGMOTDEPASSE.value = hex_md5(document.forms[0].CHGMOTDEPASSE2.value);
		document.forms[0].CHGMOTDEPASSE2.value ="";
		document.forms[0].CHGMOTDEPASSE3.value ="";
		parent.IHM_Chargement_start();
	    parent.Ext.Ajax.request({
		  url : parent.IHM_getBaseUrl()+"connexion_change_pwd.php",
	      method : 'POST',
	      params : {
	        'pwdEncrypted'  : document.forms[0].CHGMOTDEPASSE.value,
	        'oldPassword'  : document.forms[0].OLDMOTDEPASSE.value,
	        'identifiant'	: document.forms[0].IDENTIFIANT.value
	      },
		  success: function(result){
			  if(result.responseText != "ok"){
				    parent.Ext.MessageBox.alert('Failed','erreur'); 
			  }else{
				  top.location = window.location.protocol+"//"+window.location.host;
			  }
		  },
		  failure: function(result){
		    parent.Ext.MessageBox.alert('Failed','erreur lors de la mise à jour du mot de passe'); 
		  } 
	    });		
	}
} 

function passwordForgotten(){		 
	 var l_url = parent.IHM_getBaseUrl();
	  l_url += "connexion_demand_pwd.php?";
	  parent.loadExtPopup(l_url, "connexion.php", "Mot de passe oublié ?", 500, 300, true);
}


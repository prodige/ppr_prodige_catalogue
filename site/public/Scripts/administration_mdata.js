/**
 * @brief Initialisation de la page d'administration des contacts et des producteurs de métadonnées
 * @return
 */
function  administration_mdata_init()
{
  
  try
  {
    
    var str=window.location.toString();
    var last_url=parent.IHM_historique_getLastUrl().toString();
        
    if (str.lastIndexOf(last_url)==-1){
      parent.IHM_historique_add(window.location);
    }
    parent.IHM_Chargement_stop();
  }
  catch(e){}
  administration_mdata_onfocus();
}



function  administration_mdata_onfocus()
{
  try
  {
    parent.IHM_Domaines_hide();
  }
  catch(e)
  {}
}

/**
 * @brief Gère le clic sur un autre contact dans la liste
 * @return
 */
function ChangeContact(){
  var f=document.md_contact_form;
  var oSelect=f.elements["contact_name_selected"];
  var contact_name=GetSelectedValues(oSelect);
  f.action="admin_maj.php?mode=contact&name="+contact_name;
  f.target='footerExec';
  f.submit();
}

/**
 * @brief Gère le clic sur un autre producteur dans la liste
 * @return
 */
function ChangeProducteur(){
  var f=document.md_producteur_form;
  var oSelect=f.elements["producteur_name_selected"];
  var producteur_name=GetSelectedValues(oSelect);
  f.action="admin_maj.php?mode=producteur&name="+producteur_name;
  f.target='footerExec';
  f.submit();
}

/**
 * @brief Renvoie la valeur sélectionnée dans une liste SELECT
 * @param oSelect
 * @return tabValues
 */
function GetSelectedValues(oSelect)
{
  var tabValues = new Array();
  for (var i=0; i<oSelect.options.length; i++){
    if (oSelect.options[i].selected)
      tabValues.push(oSelect.options[i].value);
  }
  return tabValues;
}

/**
 * @brief Ajoute un contact
 * @return
 */
function mdata_addContact(){
  var f=document.md_contact_form;
  var contact_name=f.elements["contact_name_add"].value;
  var contact_tel=f.elements["contact_tel_add"].value;
  if (contact_name==""){
    alert ('Veuillez saisir un nom.');
    return;
  }
  if (contact_tel.length<20){
    alert ('Veuillez saisir un numéro de téléphone valide.');
    return;
  }
  f.action="admin_contacts.php?mode=add";
  f.target="";
  f.submit();
}

/**
 * @brief Ajoute un producteur
 * @return
 */
function mdata_addProducteur(){
  var f=document.md_producteur_form;
  var producteur_name=f.elements["producteur_name_add"].value;
  if (producteur_name==""){
    alert ('Veuillez saisir un nom.');
    return;
  }
  f.action="admin_producteurs.php?mode=add";
  f.target="";
  f.submit();
}

/**
 * @brief Modifie les données d'un contact
 * @return
 */
function mdata_modifyContact(){
  var f=document.md_contact_form;
  f.action="admin_contacts.php?mode=modify";
  var contact_name=f.elements["contact_name_modify"].value;
  var contact_tel=f.elements["contact_tel_modify"].value;
  if (contact_name==""){
    alert ('Veuillez saisir un nom.');
    return;
  }
  if (contact_tel.length<20){
    alert ('Veuillez saisir un numéro de téléphone valide.');
    return;
  }
  f.target="";
  f.submit();
}

/**
 * @brief Modifie les données d'un producteur
 * @return
 */
function mdata_modifyProducteur(){
  var f=document.md_producteur_form;
  f.action="admin_producteurs.php?mode=modify";
  var producteur_name=f.elements["producteur_name_modify"].value;
  if (producteur_name==""){
    alert ('Veuillez saisir un nom.');
    return;
  }
  f.target="";
  f.submit();
}

/**
 * @brief Efface un contact
 * @return
 */
function mdata_deleteContact(){
  var f=document.md_contact_form;
  f.action="admin_contacts.php?mode=delete";
  f.target="";
  f.submit();
}

/**
 * @brief Efface un producteur
 * @return
 */
function mdata_deleteProducteur(){
  var f=document.md_producteur_form;
  f.action="admin_producteurs.php?mode=delete";
  f.target="";
  f.submit();
}




function isNumeric(value){
  var expression = new RegExp("^[0-9() \t]+$"); // /^\d+$/
  var isNumeric=expression.test(value);
  return isNumeric;
}
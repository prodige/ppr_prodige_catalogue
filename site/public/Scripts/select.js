/**
 * @brief Ajoute une option é une liste SELECT
 * @param objSelect
 * @param val
 * @param txt
 * @param bMoveDefaultToEnd
 * @param bVerifExist
 * @return
 */
function AddOptionToSelect(objSelect, val, txt, bMoveDefaultToEnd, bVerifExist)
{
  if( bVerifExist ) {
    var i = 0;
    var bFound = false;
    while( !bFound && i<objSelect.length ) {
      if( objSelect.options[i].value != val ) i++; else bFound = true;
    }
    if( bFound ) return false;
  }

  if( bMoveDefaultToEnd ) {
    objSelect.length=objSelect.length+1;
    var nbElt = objSelect.length;
    objSelect.options[nbElt-2].value = val;
    objSelect.options[nbElt-2].text = txt;
    objSelect.options[nbElt-1].value = "";
    objSelect.options[nbElt-1].text = "";
    if( objSelect.selectedIndex == (nbElt-2) )
      objSelect.selectedIndex = nbElt-1;
  } else {
    objSelect.length=objSelect.length+1;
    var nbElt = objSelect.length;
    objSelect.options[nbElt-1].value = val;
    objSelect.options[nbElt-1].text = txt;
    objSelect.selectedIndex = nbElt-1;
  }
  return true;
}
/**
 * @brief Sélectionne toutes les valeurs
 * @param objSelect
 * @return
 */
function SelectAllValues(objSelect) 
{
  // selectionne tous les elements de liste
  for(var i=0; i<objSelect.length-1; i++)
    objSelect.options[i].selected = true;
  // retire le dernier element vide de la selection
  if( objSelect.length > 0 )
    objSelect.options[objSelect.length-1].selected = false;
}
/**
 * @brief Enléve une option d'une liste SELECT
 * @param objSelect
 * @return
 */
function DelOptionFromSelect(objSelect)
{
  /*
  var nbElt = objSelect.length;
  objSelect.selectedIndex = 0;
  while( nbElt>0 ) {
    objSelect.options[nbElt-1].value="";
    objSelect.options[nbElt-1].text="";
    nbElt--;
  }*/
  objSelect.selectedIndex = 0;
  objSelect.length = 1;
}
/**
 * @brief Supprime une valeur d'un objet SELECT
 * @param objSelect
 * @param bReturn
 * @return
 */
function SupprVal(objSelect, bReturn) 
{
  // supprime de la liste, l'element de la liste d'indice idSuppr
  var idSuppr = objSelect.selectedIndex;
  var nbElt = objSelect.length;
  if( idSuppr==-1 || idSuppr==nbElt-1 || nbElt<2 )
    return "-1";

  var strValSuppr = objSelect.options[idSuppr].value;

  if( idSuppr >= nbElt - 2 )
     objSelect.selectedIndex = nbElt - 1;
  else
    objSelect.selectedIndex = idSuppr;

  objSelect.options[nbElt-1].value = "";
  objSelect.options[nbElt-1].text = "";

  for(var i=idSuppr; i<objSelect.length-1; i++) {
    objSelect.options[i].value = objSelect.options[i+1].value;
    objSelect.options[i].text = objSelect.options[i+1].text;
   }
  objSelect.length = nbElt - 1;

 if( bReturn )  return strValSuppr;
}
/**
 * @brief Initialise la valeur sélectionnée
 * @param objSelect
 * @param strValue
 * @return
 */
function SetSelectValue(objSelect, strValue)
{
  // sélectionne l'idem ayant strValue comme valeur
  var nbElt = objSelect.length;
  var i = 0;
  while( i < objSelect.options.length && objSelect.options[i].value != strValue ) i++;
  if( i < objSelect.options.length )
    objSelect.options[i].selected = true;
}
/**
 * @brief Vide la liste SELECT
 * @param objSelect
 * @param bRemoveFirst
 * @return
 */
function RemoveAllFromSelect(objSelect, bRemoveFirst)
{
  objSelect.selectedIndex = -1;
  if( bRemoveFirst ) 
    objSelect.options.length = 0;
  else
    objSelect.options.length = 1;
}
function isSelectRequire(formName, objSelect, strForbidden)
{
  var tabForbidden = strForbidden.split(',');
  var bOk = true;
  if( objSelect && objSelect.selectedIndex<0 ) return false;
  if (objSelect.multiple){
    for (var opt=0; opt<objSelect.options.length; opt++){
      if (objSelect.options[opt].selected){
        var value = objSelect.options[opt].value;
        for (var i=0; i<tabForbidden.length; i++){
          bOk &= ( value != tabForbidden[i] );
        }
      }
    }
  }
  else {
    var value = objSelect.options[objSelect.selectedIndex].value;
    for (var i=0; i<tabForbidden.length; i++){
      bOk &= ( value != tabForbidden[i] );
    }
  }
  return bOk;
}

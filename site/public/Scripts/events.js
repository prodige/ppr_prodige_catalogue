/**
 * @brief Gestion de changement de domaine dans le catalogue en mode consultation
 * @param evt
 * @return
 */
function	events_catalogue_changedomaine(evt)
{
	try
	{
		if (navigator.appName == "Microsoft Internet Explorer")
			window.frames("fenetre").catalogue_domaineChange();
		else
			window.frames["fenetre"].catalogue_domaineChange();
			//window.frames["fenetre_catalogue"].catalogue_domaineChange();
	}
	catch(e)
	{
		domaines_event_onchange_remove(events_catalogue_changedomaine);
	}
}
/**
 * @brief Gestion de changement de domaine dans la carto en mode consultation
 * @param evt
 * @return
 */
function	events_consultation_changedomaine(evt)
{
	try
	{
		if (navigator.appName == "Microsoft Internet Explorer")
			window.frames("fenetre").consultation_domaineChange();
		else
			window.frames["fenetre"].consultation_domaineChange();
			//window.frames["fenetre_consultation"].consultation_domaineChange();
	}
	catch(e)
	{
		domaines_event_onchange_remove(events_consultation_changedomaine);
	}
}

/**
 * @brief Gestion de changement de domaine dans l'onglet donnees
 * @param evt
 * @return
 */
function  events_donnees_changedomaine(evt)
{
  try
  {
    if (navigator.appName == "Microsoft Internet Explorer")
      window.frames("fenetre").donnees_domaineChange();
    else
      window.frames["fenetre"].donnees_domaineChange();
      //window.frames["fenetre_consultation"].consultation_domaineChange();
  }
  catch(e)
  {
    domaines_event_onchange_remove(events_donnees_changedomaine);
  }
}

/**
 * @brief Gestion de changement de domaine dans le catalogue en mode administration
 * @param evt
 * @return
 */
function	events_administration_catalogue_changedomaine(evt)
{
	try
	{
		if (navigator.appName == "Microsoft Internet Explorer")
			window.frames("fenetre").administration_catalogue_domaineChange();
		else
			window.frames["fenetre"].administration_catalogue_domaineChange();
			//window.frames["fenetre_administration"].administration_catalogue_domaineChange();
	}
	catch(e)
	{
		domaines_event_onchange_remove(events_administration_catalogue_changedomaine);
	}
}
/**
 * @brief Gestion de changement de domaine dans la carto en mode administration
 * @param evt
 * @return
 */
function	events_administration_carto_changedomaine(evt)
{
	try
	{
		if (navigator.appName == "Microsoft Internet Explorer")
			window.frames("fenetre").administration_carto_domaineChange();
		else
			window.frames["fenetre"].administration_carto_domaineChange();
			//window.frames["fenetre_administration"].administration_carto_domaineChange();
	}
	catch(e)
	{
		domaines_event_onchange_remove(events_administration_carto_changedomaine);
	}
}

/**
 * @brief Gestion de changement de domaine pour Geonetwork
 * @param evt
 * @return
 */
function	events_geonetwork_changedomaine(evt)
{
	try
	{
		if (navigator.appName == "Microsoft Internet Explorer")
			window.frames("fenetre").search();
		else
			window.frames["fenetre"].search();
	}
	catch(e)
	{
		domaines_event_onchange_remove(events_geonetwork_changedomaine);
	}
}
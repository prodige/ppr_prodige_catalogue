
Ext.ns('Ext.ux.plugins');

/**
 * allow custom value in combobox
 * if the entered value is not found in the store the value is used as the combos value
 * 
 * @author Gustav Rek
 * @date 21.01.2010
 * @version 1
 * 
 */

Ext.ux.plugins.CustomFilterCombo = function(config) {
    Ext.apply(this, config);
    
    Ext.ux.plugins.CustomFilterCombo.superclass.constructor.call(this);
};

Ext.extend(Ext.ux.plugins.CustomFilterCombo, Object, {
    
    /**
     * @cfg {Boolean} anyMatch <tt>true</tt> to match any part not just the beginning (default=false)
     */
    anyMatch : false,
    
    /**
     * @cfg {Boolean} caseSensitive <tt>true</tt> for case sensitive comparison (default=false)
     */
    caseSensitive : false,
    
    /**
     * @cfg {Function} filterFn Filter by a function. This function will be called for each
     * Record in this Store. If the function returns <tt>true</tt> the Record is included,
     * otherwise it is filtered out (default=undefined).
     * When using this parameter anyMathch and caseSensitive are ignored!
     * @param {Ext.data.Record} record  The {@link Ext.data.Record record}
     * to test for filtering. Access field values using {@link Ext.data.Record#get}.
     * @param {Object} id The ID of the Record passed.
     * @param {String} field The field filtered in (normally the displayField of the combo).
     * Use this with {@link Ext.data.Record#get} to fetch the String to match against.
     * @param {String} value The value typed in by the user.
     */
    filterFn : undefined,
    
    init : function(combo) {
        this.combo = combo;
        
        if(Ext.isFunction(this.filterFn)) {
            combo.store.filter = this.filterBy.createDelegate(this);
        } else {
            // save this funtcion for later use, before we overwrite it
            combo.store.originalFilter = combo.store.filter;
            combo.store.filter = this.filter.createDelegate(this);
        }
    },
    
    // scope: this
    filterBy : function(field, value) {
        this.combo.store.filterBy(this.filterFn.createDelegate(this.combo, [field, value], true));        
    },
    
    // scope: this
    filter : function(field, value) {
        this.combo.store.originalFilter(field, value, this.anyMatch, this.caseSensitive);
    }
});

Ext.preg('customfiltercombo', Ext.ux.plugins.CustomFilterCombo);

Ext.lib.Ajax.isCrossDomain = function(u) {
  var match = /(?:(\w*:)\/\/)?([\w\.\-\_]*(?::\d*)?)/.exec(u);
  if (!match[1])
    return false; // No protocol, not cross-domain
  return (match[1] != location.protocol) || (match[2] != location.host);
};

Ext.override(
  Ext.data.Connection,
  {

    request : function(o) {
      if (this.fireEvent("beforerequest", this, o) !== false) {
        var p = o.params;

        if (typeof p == "function") {
          p = p.call(o.scope || window, o);
        }
        if (typeof p == "object") {
          p = Ext.urlEncode(p);
        }
        if (this.extraParams) {
          var extras = Ext.urlEncode(this.extraParams);
          p = p ? (p + '&' + extras) : extras;
        }

        var url = o.url || this.url;
        if (typeof url == 'function') {
          url = url.call(o.scope || window, o);
        }

        if (o.form) {
          var form = Ext.getDom(o.form);
          url = url || form.action;

          var enctype = form.getAttribute("enctype");
          if (o.isUpload
              || (enctype && enctype.toLowerCase() == 'multipart/form-data')) {
            return this.doFormUpload(o, p, url);
          }
          var f = Ext.lib.Ajax.serializeForm(form);
          p = p ? (p + '&' + f) : f;
        }

        var hs = o.headers;
        if (this.defaultHeaders) {
          hs = Ext.apply(hs || {}, this.defaultHeaders);
          if (!o.headers) {
            o.headers = hs;
          }
        }

        var cb = {
          success :this.handleResponse,
          failure :this.handleFailure,
          scope :this,
          argument : {
            options :o
          },
          timeout :this.timeout
        };

        var method = o.method || this.method || (p ? "POST" : "GET");

        if (method == 'GET'
            && (this.disableCaching && o.disableCaching !== false)
            || o.disableCaching === true) {
          url += (url.indexOf('?') != -1 ? '&' : '?') + '_dc='
              + (new Date().getTime());
        }

        if (typeof o.autoAbort == 'boolean') { // options
          // gets top
          // priority
          if (o.autoAbort) {
            this.abort();
          }
        } else if (this.autoAbort !== false) {
          this.abort();
        }
        if ((method == 'GET' && p) || o.xmlData || o.jsonData) {
          url += (url.indexOf('?') != -1 ? '&' : '?') + p;
          p = '';
        }
        if (o.scriptTag || this.scriptTag
            || Ext.lib.Ajax.isCrossDomain(url)) {
          this.transId = this.scriptRequest(method, url, cb, p, o);
        } else {
          this.transId = Ext.lib.Ajax.request(method, url, cb, p, o);
        }
        return this.transId;
      } else {
        Ext.callback(o.callback, o.scope, [ o, null, null ]);
        return null;
      }
    },

    scriptRequest : function(method, url, cb, data, options) {
      var transId = ++Ext.data.ScriptTagProxy.TRANS_ID;
      var trans = {
        id :transId,
        cb :options.callbackName || "stcCallback" + transId,
        scriptId :"stcScript" + transId,
        options :options
      };

      url += (url.indexOf("?") != -1 ? "&" : "?")
          + data
          + String.format("&{0}={1}", options.callbackParam
              || this.callbackParam || 'callback', trans.cb);

      var conn = this;
      window[trans.cb] = function(o) {
        conn.handleScriptResponse(o, trans);
      };

      // Set up the timeout handler
      trans.timeoutId = this.handleScriptFailure.defer(cb.timeout, this,
          [ trans ]);

      var script = document.createElement("script");
      script.setAttribute("src", url);
      script.setAttribute("type", "text/javascript");
      script.setAttribute("id", trans.scriptId);
      document.getElementsByTagName("head")[0].appendChild(script);

      return trans;
    },

    handleScriptResponse : function(o, trans) {
      this.transId = false;
      this.destroyScriptTrans(trans, true);
      var options = trans.options;

      // Attempt to parse a string parameter as XML.
      var doc;
      if (typeof o == 'string') {
        if (window.ActiveXObject) {
          doc = new ActiveXObject("Microsoft.XMLDOM");
          doc.async = "false";
          doc.loadXML(o);
        } else {
          doc = new DOMParser().parseFromString(o, "text/xml");
        }
      }

      // Create the bogus XHR
      response = {
        responseObject :o,
        responseText :(typeof o == "object") ? Ext.util.JSON.encode(o)
            : String(o),
        responseXML :doc,
        argument :options.argument
      }
      
      this.fireEvent("requestcomplete", this, response, options);
      Ext.callback(options.success, options.scope, [ response, options ]);
      Ext.callback(options.callback, options.scope, [ options, true,
          response ]);
    },

    handleScriptFailure : function(trans) {
      this.transId = false;
      this.destroyScriptTrans(trans, false);
      var options = trans.options;
      response = {
        argument :options.argument,
        status :500,
        statusText :'Server failed to respond',
        responseText :''
      };
      this.fireEvent("requestexception", this, response, options, {
        status :-1,
        statusText :'communication failure'
      });
      Ext.callback(options.failure, options.scope, [ response, options ]);
      Ext.callback(options.callback, options.scope, [ options, false,
          response ]);
    },

    // private
    destroyScriptTrans : function(trans, isLoaded) {
      document.getElementsByTagName("head")[0].removeChild(document
          .getElementById(trans.scriptId));
      clearTimeout(trans.timeoutId);
      if (isLoaded) {
        window[trans.cb] = undefined;
        try {
          delete window[trans.cb];
        } catch (e) {
        }
      } else {
        // if hasn't been loaded, wait for load to remove it
        // to prevent script error
        window[trans.cb] = function() {
          window[trans.cb] = undefined;
          try {
            delete window[trans.cb];
          } catch (e) {
          }
        };
      }},
          // private
    handleResponse : function(response){
        this.transId = false;
        var options = response.argument.options;
        response.argument = options ? options.argument : null;
        this.fireEvent("requestcomplete", this, response, options);
        if(options.success){
            options.success.call(options.scope, response, options);
        }
        if(options.callback){
            options.callback.call(options.scope, options, true, response);
        }
    },

    // private
    handleFailure : function(response, e){
        this.transId = false;
        var options = response.argument.options;
        response.argument = options ? options.argument : null;
        this.fireEvent("requestexception", this, response, options, e);
        if(options.failure){
            options.failure.call(options.scope, response, options);
        }
        if(options.callback){
            options.callback.call(options.scope, options, false, response);
        }
    }
    
});

function setResponseObject(response) {
  if(response.responseObject)
    return response;
  response.responseObject = eval('('+response.responseText+')');
  return response;
}

Ext.Element.prototype.getAttribute = (function(){
    var test = document.createElement('table'),
        isBrokenOnTable = false,
        hasGetAttribute = 'getAttribute' in test,
        unknownRe = /undefined|unknown/;

    if (hasGetAttribute) {

        try {
            test.getAttribute('ext:qtip');
        } catch (e) {
            isBrokenOnTable = true;
        }

        return function(name, ns) {
            var el = this.dom,
                value;

            if (el.getAttributeNS) {
                value  = el.getAttributeNS(ns, name) || null;
            }

            if (value == null) {
                if (ns) {
                    if (isBrokenOnTable && el.tagName.toUpperCase() == 'TABLE') {
                        try {
                            value = el.getAttribute(ns + ':' + name);
                        } catch (e) {
                            value = '';
                        }
                    } else {
                        value = el.getAttribute(ns + ':' + name);
                    }
                } else {
                    value = el.getAttribute(name) || el[name];
                }
            }
            return value || '';
        };
    } else {
        return function(name, ns) {
            var el = this.om,
                value,
                attribute;

            if (ns) {
                attribute = el[ns + ':' + name];
                value = unknownRe.test(typeof attribute) ? undefined : attribute;
            } else {
                value = el[name];
            }
            return value || '';
        };
    }
    test = null;
})();
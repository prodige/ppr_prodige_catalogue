var	consultation_win_cartodynamique = null;
var	historique = new Array();
var jsession = '';

/**
 * @brief initialisation de l'interface
 */
function	IHM_init()
{
	var		onglets;
	var		onglets_list;
	var		onglet_actif;
	//IHM_Chargement_stop();
	onglets = Ext.getCmp("tabPanel").items.items;
	//onglets = document.getElementById("onglets");
	if (onglets)
	{
		//onglets_list = onglets.getElementsByTagName("div");
		if (onglets && (onglets.length > 0) && (IHM_Onglets_getSel() == ""))
		{
			onglet_actif = onglets[0];
			IHM_Onglets_sel(onglet_actif);
		}
	}
}

function IHM_NoSelectionDomain_hide()
{
  Ext.get('selectdomain').hide();
}
function IHM_NoSelectionDomain_show()
{
  Ext.get('selectdomain').show();
}
/**
 * @brief Chargement du message d'attente
 */
function	IHM_Chargement_start()
{
	/*var		chargement;

	chargement = document.getElementById("chargement_en_cours");
	if (chargement)
		chargement.className = "chargement_en_cours";*/
  Ext.get('loading').show();
  IHM_NoSelectionDomain_hide();
  
}

/**
 * @brief Arrêt du message d'attente
 */
function	IHM_Chargement_stop()
{
  /*var		chargement;
  
	chargement = document.getElementById("chargement_en_cours");
	if (chargement)
		chargement.className = "chargement_en_cours_hide";*/
  Ext.get('loading').hide();
  //Ext.get('loading-mask').fadeOut({remove:true});
}

/**
 * @brief Changement du mode d"affichage du panier
 */
function	IHM_Panier_switch()
{
	var		panier;
	var		onglet;
	var		contenu;

	panier = document.getElementById('panier');
	if (panier)
	{
		switch(panier.className)
		{
			case "panier_ouvert":
				IHM_Panier_hide();
				onglet = IHM_Onglets_getSel();
				if (onglet)
				{
					try
					{
						if (navigator.appName == "Microsoft Internet Explorer")
							contenu = window.frames("fenetre");
						else
							contenu = window.frames["fenetre_" + onglet];
						eval("contenu." + onglet + "_onfocus();");
					}
					catch(e)
					{
						IHM_Domaines_resume();
					}
				}
				else
					IHM_Domaines_resume();
				break;
			case "panier_ferme":
				IHM_Panier_show();
				IHM_Domaines_resume();
				break;
			default:
				break;
		}
	}
}

/**
 * @brief met le panier en mode refermé
 */
function	IHM_Panier_hide()
{
	var		panier;
	Ext.getCmp("contPanier").collapse(true);
	/*panier = document.getElementById('panier');
	if (panier)
	{
		switch(panier.className)
		{
			case "panier_ouvert":
				panier.className = "panier_ferme";
				break;
			default:
				break;
		}
	}*/
	
}
/**
 * @brief met le panier en mode ouvert
 */
function	IHM_Panier_show()
{
	Ext.getCmp("contPanier").expand(true);
	return;
	var		panier;

	panier = document.getElementById('panier');
	if (panier)
	{
		switch(panier.className)
		{
			case "panier_ferme":
				panier.className = "panier_ouvert";
				break;
			default:
				break;
		}
	}
	IHM_Domaines_resume();
}
function	IHM_Domaines_clean()
{
  Ext.getCmp('westLayout').setTitle("Domaine / Sous-domaine");
}

/**
 * @brief change le mode d'affichage des domaines
 */
function	IHM_Domaines_switch()
{
	var		domaines;

	domaines = document.getElementById('domaines');
	if (domaines)
	{
		switch(domaines.className)
		{
			case "domaines_ouvert":
				IHM_Domaines_resume();
				break;
			case "domaines_resume_vide":
			case "domaines_resume":
			case "domaines_ferme":
				IHM_Domaines_show();
				IHM_Panier_hide();
				break;
			default:
				break;
		}
	}
}

/**
 * @brief cache le div des domaines
 */
function	IHM_Domaines_hide()
{
  
	window.viewport.layout.west.slideIn();
	/*var		domaines;

	domaines = document.getElementById('domaines');
	if (domaines)
	{
		switch(domaines.className)
		{
			case "domaines_resume_vide":
			case "domaines_resume":
			case "domaines_ouvert":
				domaines.className = "domaines_ferme";
				break;
			default:
				break;
		}
	}*/
}
/**
 * @brief montre le div des domaines
 */
function	IHM_Domaines_show()
{
  if ( !Ext.getCmp("westLayout").isVisible() ){
    if ( window.viewport.layout.west.timeout ){
      clearTimeout(window.viewport.layout.west.timeout);
      window.viewport.layout.west.timeout = null;
    }
  	window.viewport.layout.west.slideOut();
  	Ext.getCmp("westLayout").expand();
  	Ext.getCmp("TreeDomaine").expand();
  }
  /*var		domaines;
	
	domaines = document.getElementById('domaines');
	if (domaines)
	{
		switch(domaines.className)
		{
			case "domaines_resume_vide":
			case "domaines_resume":
			case "domaines_ferme":
				domaines.className = "domaines_ouvert";
				domaines.style.height = "390px";
				break;
			default:
				break;
		}
	}*/
  
}

function	IHM_Domaines_resume()
{
	var		domaines;
	var		resume;
	var		bottom;
	var		domaine;
	var		sousdomaine;

	domaines = document.getElementById('domaines');
	if (domaines)
	{
		switch(domaines.className)
		{
			case "domaines_ferme":
			case "domaines_ouvert":
				if (IHM_Domaines_IsSelected())
					domaines.className = "domaines_resume";
				else
					domaines.className = "domaines_resume_vide";
				resume = document.getElementById('resume');
				if (resume)
				{
					bottom = resume.offsetTop;
					if (bottom <= 0)
						bottom = 40;
					if (domaines_getCurentDomaine())
					{
						domaine = document.getElementById("domaine");
						if (domaine)
						{
							bottom += domaine.offsetHeight;
							sousdomaine = document.getElementById("sousdomaine");
							if (sousdomaine)
							{
								bottom += sousdomaine.offsetHeight;
							}
						}
					}
					domaines.style.height = bottom + "px";
				}
				break;
			default:
				break;
		}
	}
}

/**
 * @brief retourne vrai si un domaine est sélectionné
 * @return boolean
 */
function	IHM_Domaines_IsSelected()
{
	var		div_dom;
	var		div_sdom;
	var		actif = false;

	div_dom = document.getElementById("domaine");
	if (div_dom)
	{
		node = div_dom.firstChild;
		if (node && (node.nodeType == 3) && (node.data != ""))
			actif = true;
	}
	return actif;
}

/**
 * @brief Retourne le nom de classe (css) de base de l'objet
 * @param onglet
 * @return baseClassName
 */
function	IHM_baseClassName(onglet)
{
  return onglet.id;
	var		pos;
	var		baseClassName = "";
	if (onglet)
	{
		pos = onglet.className.lastIndexOf("_");
		if (pos > 0)
			baseClassName = onglet.className.substr(0, pos);
		else
			baseClassName = onglet.className;
	}
	return baseClassName;
}

/**
 * @brief Gère le over dans les css pour l'onglet onglet
 * @param onglet
 * @return
 */
function	IHM_Onglets_over(onglet)
{
	var		baseClassName = "";

	if (onglet)
	{
		baseClassName = IHM_baseClassName(onglet);
		if (onglet.className == baseClassName)
			onglet.className = baseClassName + "_over";
	}
}
/**
 * @brief Gère le out dans les css pour l'onglet onglet
 * @param onglet
 * @return
 */
function	IHM_Onglets_out(onglet)
{
	var		baseClassName = "";

	if (onglet)
	{
		baseClassName = IHM_baseClassName(onglet);
		if (onglet.className == baseClassName + "_over")
			onglet.className = baseClassName;
	}
}
/**
 * @brief Gère la sélection dans les css pour l'onglet onglet
 * @param onglet
 * @param query  url arguments
 * @return
 */
function	IHM_Onglets_sel(onglet, query)
{
   
  if(typeof(query)=="undefined"){
    if(typeof(onglet.query) == "undefined")
      query = "";
    else
      query = onglet.query;  
  }
	  
  if ( window.timerOnload ){
	topWindow().Ext.getCmp('westLayout').setTitle("Domaine / Sous-domaine");
    parent.IHM_Panier_hide();
    parent.IHM_Domaines_hide();
    parent.IHM_NoSelectionDomain_hide();
    clearTimeout(window.timerOnload);
    window.timerOnload = null;
  }
	var		fenetre;
	var		i;
	var		pos;
	var		path;
	var		baseClassName = "";

	IHM_Panier_hide();
	if (onglet)
	{
		baseClassName = IHM_baseClassName(onglet);
		if ( onglet!=Ext.getCmp("tabPanel").getActiveTab() ){
		  Ext.getCmp("tabPanel").setActiveTab(onglet.id);
		  return;
		}
		pos = window.location.pathname.lastIndexOf("/");
		if (pos > 0)
			path = window.location.pathname.substr(0, pos + 1);
		else
			path = "/";
		contenu = window.frames["fenetre"];
		if (contenu )
		{
			IHM_Chargement_start();
			
			if (query != "")
				contenu.location = path + baseClassName + ".php?" + query;
			else
				contenu.location = path + baseClassName + ".php";
		}
		else
			IHM_Chargement_stop();
	}
}

/**
 * @brief Gère les sélection avec url dans les css pour l'onglet onglet
 * @param onglet
 * @return
 */
function	IHM_Onglets_selWithUrl(nomonglet, url)
{
  
	var	onglet = Ext.getCmp(nomonglet);
	var onglets = Ext.getCmp("tabPanel").items.items;

	IHM_Panier_hide();
	if (onglet)
	{
	  //if (onglet.isVisible() )
  		Ext.getCmp("tabPanel").setActiveTab(onglet.id);
      //else {
      //  Ext.getCmp("tabPanel").setActiveTab(onglets[0].id);
      //}
	}
	var contenu = window.frames["fenetre"];
		
	if (contenu)
	{
		IHM_Chargement_start();
		contenu.location = url;
	}
	else
		IHM_Chargement_stop();
}

function	IHM_Onglets_getSel()
{
	return Ext.getCmp("tabPanel").getActiveTab().id;
}

/**
 * @brief Retourne le nom de l'onglet
 * @param name
 * @return onglet
 */
function	IHM_Onglets_get(name)
{
	var		barre_onglets;
	var		onglets;
	var		i;
	var		onglet;

	/*barre_onglets = document.getElementById("onglets");
	if (barre_onglets)
		onglets = barre_onglets.getElementsByTagName("div");*/
	onglets = Ext.getCmp("tabPanel").items.items;
	if (onglets)
	{
		for (i = 0; (i < onglets.length); i++)
		{
			if (IHM_baseClassName(onglets[i]) == name)
				onglet = onglets[i];
		}
	}
	return onglet;
}

/**
 * @brief Désélectionne un onglet
 * @param onglet
 * @return
 */
function	IHM_Onglets_unsel(onglet)
{
	var		onglets;
	var		fenetre;
	var		i;
	var		baseClassName = "";

	if (onglet)
	{
		baseClassName = IHM_baseClassName(onglet);
		if (onglet.className == baseClassName + "_sel")
		{
			onglet.className = baseClassName;
			if (navigator.appName != "Microsoft Internet Explorer")
				fenetre = document.getElementById(baseClassName);
			if (fenetre)
				fenetre.className = baseClassName + "_hide";
		}
	}
}
/**
 * @brief Active l'interface
 * @param connexion
 * @param navigation
 * @param administration
 * @return
 */
function	IHM_Onglets_activate(connexion, administration, carteperso)
{
	var		i;
	var		barre_onglets;
	var		onglets;
	if ( !Ext.getCmp("tabPanel") || typeof Ext.getCmp("tabPanel").items=="undefined" || !Ext.getCmp("tabPanel").items.items ) return;
	onglets = Ext.getCmp("tabPanel").items.items;
	/*if (barre_onglets)
	{
		onglets = barre_onglets.getElementsByTagName("div");
	}*/
	if (onglets)
	{
		for (i = 0; i < onglets.length; i++)
		{
		  switch(IHM_baseClassName(onglets[i]))
			{
				case "connexion":
					if (connexion){
						onglets[i].className = "connexion";
						Ext.getCmp("tabPanel").unhideTabStripItem("connexion");
					}
					else
					{
						IHM_Onglets_unsel(onglets[i]);
						onglets[i].className = "connexion_hide";
						Ext.getCmp("tabPanel").hideTabStripItem("connexion");
					}
					break;
				case "deconnexion":
					if (!connexion){
						onglets[i].className = "deconnexion";
					  Ext.getCmp("tabPanel").unhideTabStripItem("deconnexion");
					}
					else
					{
						IHM_Onglets_unsel(onglets[i]);
						onglets[i].className = "deconnexion_hide";
						Ext.getCmp("tabPanel").hideTabStripItem("deconnexion");
					}
					break;	
				
				case "administration":
					if (!connexion && administration){
						onglets[i].className = "administration";
						Ext.getCmp("tabPanel").unhideTabStripItem("administration");
					}
					else
					{
						IHM_Onglets_unsel(onglets[i]);
						onglets[i].className = "administration_hide";
						Ext.getCmp("tabPanel").hideTabStripItem("administration");
					}
					break;
				case "carteperso":
					if ((!connexion && carteperso && PRO_IS_CARTEPERSO_ACTIF && PRO_IS_CARTEPERSO_ACTIF=="on")||(PRO_ACTIVE_CARTE_PERSO && PRO_ACTIVE_CARTE_PERSO=="on")){
						onglets[i].className = "carteperso";
						Ext.getCmp("tabPanel").unhideTabStripItem("carteperso");
					}
					else
					{						
						IHM_Onglets_unsel(onglets[i]);
						onglets[i].className = "carteperso_hide";
						Ext.getCmp("tabPanel").hideTabStripItem("carteperso");
					}
					break;	
				default:
				  break;
			}
		}
	}
}
/**
 * @brief Affiche le contenu d'un info bulle
 * @param evt
 * @param message
 * @return
 */
function	IHM_InfosBulle_show(evt, message)
{
	var		infosbulle;
	var		texte;
	var		x, y;

	if (navigator.appName == "Microsoft Internet Explorer")
	{
		evt = window.event;
		x = evt.clientX;
		y = evt.clientY;
	}
	else
	{
		x = evt.pageX;
		y = evt.pageY;
	}
	infosbulle = document.getElementById("infosbulle");
	if (infosbulle)
	{
		infosbulle.className = "infosbulle_show";
		infosbulle.style.left = x + "px";
		infosbulle.style.top = y + "px";
		texte = infosbulle.firstChild;
		if (texte)
			texte.data = message;
		else
		{
			texte = document.createTextNode(message);
			if (texte)
				infosbulle.appendChild(texte);
		}
	}
}
/**
 * @brief Cache une info-bulle
 * @param evt
 * @return
 */
function	IHM_InfosBulle_hide(evt)
{
	var		infosbulle;
	var		texte;

	infosbulle = document.getElementById("infosbulle");
	if (infosbulle)
	{
		infosbulle.className = "infosbulle_hide";
		texte = infosbulle.firstChild;
		if (texte)
			infosbulle.removeChild(texte);
	}
}
/**
 * @brief Affiche le contenu d'une fiche de métadonnées
 * @param id
 * @return
 */
function	IHM_Fiche_Show(id)
{
	var		barre_onglets;
	var		onglets;
	var		i;
	var		baseClassName = "recherche";
	var		onglet = null;

	onglets = Ext.getCmp("tabPanel").items.items;
	/*barre_onglets = document.getElementById("onglets");
	if (barre_onglets)
		onglets = barre_onglets.getElementsByTagName("div");*/
	if (onglets)
	{
		for (i = 0; (i < onglets.length) && (!onglet); i++)
		{
			if (IHM_baseClassName(onglets[i]) == baseClassName)
				onglet = onglets[i];
		}
	}
	if (onglet)
	{
		IHM_Onglets_sel(onglet, "id=" + id);
	}
}
/**
 * @brief Ouvre une fiche de métadonnées en mode édition
 * @param id
 * @param mode
 * @return
 */
function	IHM_Fiche_Edit(id, mode)
{
	var		barre_onglets;
	var		onglets;
	var		i;
	//var		baseClassName = "recherche";
	var		baseClassName = "administration";
	var		onglet = null;
	onglets = Ext.getCmp("tabPanel").items.items;
	/*barre_onglets = document.getElementById("onglets");
	if (barre_onglets)
		onglets = barre_onglets.getElementsByTagName("div");*/
	if (onglets)
	{
		for (i = 0; (i < onglets.length) && (!onglet); i++)
		{
			if (IHM_baseClassName(onglets[i]) == baseClassName)
				onglet = onglets[i];
		}
	}
	if (onglet)
	{
		IHM_Onglets_sel(onglet, "mode=edit&type=" + mode + "&id=" + id);
	}
}

function	IHM_isMain()
{
	return true;
}
/**
 * @brief Ajoute une url dans l'historique 
 * @param url
 * @return
 */
function	IHM_historique_add(url)
{
  var entry = new Array(IHM_Onglets_getSel(), url.toString());
  if (IHM_historique_getLastUrl() != url){
	historique.push(entry);
    //console.log("entrée");console.log(entry);
  }
	//IHM_historique_dump();
}
/**
 * @brief Remplace une url dans l'historique 
 * @param url
 * @return
 */
function	IHM_historique_replace(url)
{
	historique.pop();
	IHM_historique_add(url);
//	IHM_historique_dump();
}

/**
 * @brief Dump l'historique
 * @return
 */
function	IHM_historique_dump()
{
	var		msg = "Dump : " + historique.length + " entrées\n";
	var		entry;

	for (i = 0; i < historique.length; i++)
	{
		entry = historique[i];
		msg += entry[1] + " (" + entry[0] + ")\n";
	}
	alert(msg);
}
/**
 * @brief Retourne la dernière url de l'historique
 * @return
 */
function	IHM_historique_getLastUrl()
{
	var		entry;
	var		url = "";

	if (historique.length > 0)
		entry = historique[historique.length - 1];
	if (entry)
		url = entry[1];
	return url;
}
/**
 * @brief Effectue un retour en arrière (page précédente au sein de l'appli)
 * @return
 */
function	IHM_historique_goBack()
{
	var		entry;
	historique.pop();
	entry = historique.pop();
	if (entry)
		IHM_Onglets_selWithUrl(entry[0], entry[1]);
  else{
    var onglets = Ext.getCmp("tabPanel").items.items;
    if ( onglets )
      IHM_Onglets_sel(onglets[0], "");
  }
}

/**
 * @brief Retourne l'url de base de l'application
 */
function IHM_getBaseUrl()
{
	// URL en absolue si Prodige est intégrée dans une application externe
  var PRO_INCLUDED = document.getElementById("PRO_INCLUDED");
  if ( PRO_INCLUDED && PRO_INCLUDED.value == 1 ) {
  	return window.location.protocol+"//"+window.location.host+"/PRRA/";
  }
  
  var   url = "";
  var   pos;

  try
  {
    pos = window.location.pathname.lastIndexOf("/");
    if (pos > 0)
      url = window.location.pathname.substr(0, pos) + "/";
    else
      url = "/";
  }
  catch(e){}
  
  return url;
}

function topWindow(){
  try{
	if(top.location.host!=window.location.host)
	  return window.self;
	else
	  return window.top;
  }catch(err){
	return window.self;
  }
}
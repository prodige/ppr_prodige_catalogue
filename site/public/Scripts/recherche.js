/**
 * @brief Initialisation de la page de recherche
 * @return
 */
function	recherche_init()
{
	try
	{
		var		re1 = new RegExp("geosource", "i");

		if (re1.test(window.location))
		{
			chemin = window.location.pathname;
			pos = chemin.lastIndexOf('/');
			if (pos >= 0)
				chemin = chemin.substr(pos + 1);
			switch(chemin)
			{
				case "metadata.show":
					lastChemin = parent.IHM_historique_getLastUrl();
					pos = lastChemin.indexOf('?');
					if (pos >= 0)
						lastChemin = lastChemin.substr(0, pos);
					pos = lastChemin.lastIndexOf('/');
					if (pos >= 0)
						lastChemin = lastChemin.substr(pos + 1);
					if (lastChemin != chemin)
						parent.IHM_historique_add(window.location);
					break;
				default:
					parent.IHM_historique_add(window.location);
					break;
			}
		}
		else
			parent.IHM_historique_add(window.location);
		parent.IHM_Chargement_stop();
	}
	catch(e){}
	recherche_onfocus();
}

function	recherche_onfocus()
{
	try
	{
		parent.IHM_Domaines_hide();
	}
	catch(e)
	{}
}

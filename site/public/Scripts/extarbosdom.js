var treePanel = null;

// url of parent
var urlParent = ( parent.document.location.href ? parent.document.location.href : '' );

Ext.onReady( function() {
  treePanel = new Ext.tree.TreePanel({
    renderTo:'treePanelSdom',
    id:'ext-arbo-sdom',
    root:new Ext.tree.AsyncTreeNode({
      nodeType:"async",
      loader:new Ext.tree.TreeLoader({
        url:parent.IHM_getBaseUrl()+"Services/getDomaines.php",
        baseParams:{fmeta_id:( typeof fmeta_id != 'undefined' ? fmeta_id : 0 ) }
      })
    }),
    rootVisible:false,
    border:true,
    autoScroll:true,
    enableDD:false,
    fieldLabel:"Domaine/Sous-domaine",
    height:300,
    width:"90%"
  });
});
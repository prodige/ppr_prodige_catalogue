/**
 * @brief : Update the list of the layers
 * @author : Alkante
 */
var jsonMaps_catalog = null ; //layer list from catalog
var jsonMaps_carto = null ; //layer list from carto
var tabMaps = null ; //layer list fusion from catalog and carto (keep the ones in carto that are listed in catalog)
var nbAjaxRequest = 0 ; //number of current ajax requests;
var ajaxCartoRequest = null;//ajax request for carto Maps

if (ajaxCatalogRequest){
  ajaxCatalogRequest = null;
}
else{
  var ajaxCatalogRequest = null;
}
if (loadmask){
  loadmask = null;//extjs loading mask
}
else{
  var loadmask = null;//extjs loading mask
}
if (selectedLayer){
  selectedLayer = null;
}
else{
  var selectedLayer = null;
}


var     popup_ajoutdecarte_pos;//position de la popup dans le tableau tabExtPopup

var typeModel;//type de model
var typeExtraction; //type d'extraction
var winModel=null;

function  TSAjoutDeCarte_Ouvrir(type, typeExtractionPassed)
{ 
  typeModel = type;
  typeExtraction = typeExtractionPassed;
  if(winModel == null) {
    winModel = new Ext.Window({
        title: 'Choix du modèle pour '+(type==1 ? 'la covisualisation des données' : 'l\'extraction des données'),
        closable:true,
        closeAction: 'hide',
        width:400,
        height:250,
        //border:false,
        id : 'chooseModel',
        plain:true,
        contentEl : 'CARTE_DIV'
    });
    winModel.show();
    TSAjoutDeCarteloadMaps();
  }
  else
    winModel.show();
    
  //parent.frames.carto.TSPopup_resizeExtJS(popup_ajoutdecarte_pos); 
}

/**
 * @brief Called when select type :
 * if first loading (
 * load catalog Maps
 * load carto Maps
 * fusion of the 2 json objects
 * organize in arrays according to geomtype
 * )
 * display Maps with extjs tree
 */
function  TSAjoutDeCarteloadMaps(){

  //empty div POSTGIS_DIV
  var div_layerlist = document.getElementById("CARTE_DIV");
  if (div_layerlist.firstChild){
    div_layerlist.removeChild(div_layerlist.firstChild);
  }
  
  //if not loaded, load the list of postgis prodige Maps
  if (tabMaps==null){
    //show loadmask 
    loadmask = new Ext.LoadMask(Ext.get("CARTE_DIV"), {msg: "Chargement des cartes..."});
    loadmask.show();
    
    
    //load carto Maps
    LoadModelMaps();
        
  }

}

/**
 * @brief Load cartographic maps from the catalog database
 */
function LoadCatalogMaps(layerType){
  //ajax call
  const_phpRequete = "/PRRA/Administration/CommunicationTranservale/AdministrationCarto_getCarte.php?";
  var url = const_phpRequete;
  nbAjaxRequest ++;
  
  ajaxCatalogRequest = Ext.Ajax.request({
      url: url,
      method: "GET",
      success: function (response){
        jsonMaps_catalog = eval(response.responseText);
        /* load carto Maps
        organize Maps in their respective geomtype, 
        hide loadmask and display Maps */
        ProcessMaps(layerType);
      },
      failure: function (response){
         //hide loadmask and print error
         ProcessFailures();
      },
      params : {
        "SRV_CARTO" : const_serveurCartoFront,
        "coucheType" : 1
      },
      scope: this
    });
}

/**
 * @brief Load cartographic maps from the prodige database
 */
function LoadModelMaps(){
  //ajax call
  //const_phpRequete = "/GIMS/popup/spec/ajoutDeCarte/TSAjoutDeCarte_MapModelList.php?";
  //var url = const_serveurCarto+const_phpRequete;
  nbAjaxRequest ++;
  ajaxCartoRequest = Ext.Ajax.request({
      url: Routing.generate('catalogue_geosource_tSAjoutDeCarte_MapModelList'),
      method: "GET",
      success: function (response){
      tabMaps = eval(response.responseText);
        /* load carto Maps
        organize Maps in their respective geomtype, 
        hide loadmask and display Maps */
        ProcessMaps();
      },
      failure: function (response){
         //hide loadmask and print error
         ProcessFailures();
      },
      params : {
        "SRV_CARTO" : const_serveurCarto,
        "modelType" : typeModel
      },
      scope: this
    });
}

/**
 * @brief Process the failures : stop all requests and print error
 */
function ProcessFailures(){
  //console.log('process failures');
  //hide loading mask
  loadmask.hide();
  //stop all ajax requests
  if (Ext.Ajax.isLoading()){
    Ext.Ajax.abort();
  }
  Ext.Msg.alert("Prodige", "Erreur de récupération des cartes.");
}


/**
 * @brief Load carto Maps and do the fusion with Maps from catalog :
 * Organize Maps
 */
function ProcessMaps(){
  nbAjaxRequest --;
  if (nbAjaxRequest==0){
    loadmask.hide();
    //FusionMaps();
    DisplayMaps();
  }
}

/**
 * @brief Concat the informations from json_cartoMaps (geomtype) and json_catalogMaps (all)
 * and put all objects in the array tabMaps
 */
function FusionMaps(){
  tabMaps = new Array();
  if (jsonMaps_catalog && jsonMaps_carto){
    for (i=0; i<jsonMaps_catalog.length; i++){
      //lookup layer in carto with the right table name
      map = getMapByMapName(jsonMaps_carto, jsonMaps_catalog[i].map_name);
      if(map){
        //put layer in the global object with geometry type
        tabMaps.push(jsonMaps_catalog[i]);
        //put layer in the right object according to the geometry type
      }
    }
  }
}

/**
 * @brief Lookup jsonMaps_tmp and return the object with table_name=tablename
 */
function getMapByMapName(jsonMaps_tmp, searchname){
  var layer = null;
  var i = 0;
  
  while (layer==null && i<jsonMaps_tmp.length){
    if (jsonMaps_tmp[i].map_name==searchname){
      layer=jsonMaps_tmp[i];
    }
    else{
      i++;
    }
  }
  return layer;
}


/**
 * @brief Print the layer tree for the selected type
 */
function DisplayMaps(){
  var tabMapsToPrint = tabMaps;
  //create the treepanel
  var Tree = Ext.tree;
  var tree = new Tree.TreePanel({
    animate:true,
    enableDD:false,
    loader: new Tree.TreeLoader(),
    lines: true,
    selModel: new Ext.tree.DefaultSelectionModel(),
    containerScroll: true,
    border: false,
    autoScroll: true,
    width : 400,
    height : 250,
    rootVisible : false,
    listeners:{
      "click" : function(treeNode){
        //set layer title text
        //TSAjoutDeCouche_setLAYER_TITLE(treeNode.attributes.text);
        //selectedLayer = [treeNode.attributes.map_name, treeNode.attributes.text];
        if(typeModel==1){
          document.choix_modele.mapModel.value = treeNode.attributes.map_name;
          document.choix_modele.submit();
        }else{
          //Ext.getCmp("chooseModel").hide();
          getCartographicArea(treeNode.attributes.map_name, typeExtraction);
        }
      }
    }
  });

  var root = new Tree.AsyncTreeNode({
    draggable:false,
    children: eval(Ext.util.JSON.encode(tabMapsToPrint))
  });
    
  tree.setRootNode(root);
  tree.render('CARTE_DIV');
  root.expand();
}

/**
 * @brief Initialisation de la page
 * @return
 */
function	administration_init()
{

	try
	{
		var		re1 = new RegExp("geonetwork", "i");

		if (re1.test(window.location))
		{
			chemin = window.location.pathname;
			pos = chemin.lastIndexOf('/');
			if (pos >= 0)
				chemin = chemin.substr(pos + 1);
			switch(chemin)
			{
				case "metadata.edit":
					if (window.location.search != "")
						parent.IHM_historique_add(window.location);
					break;
				case "metadata.thumbnail.form":
				case "metadata.thumbnail.set":
				case "metadata.thumbnail.unset":
				case "metadata.update":
					break;
				default:
					parent.IHM_historique_add(window.location);
					break;
			}
		}
		else
			parent.IHM_historique_add(window.location);
		parent.IHM_Chargement_stop();
		if (parent.jsession != '')
			document.cookie = parent.jsession + "; path=/geonetwork";
	}
	catch(e){}
	administration_onfocus();
}


function administration_servers_init(){
  parent.IHM_Chargement_stop();
}
function	administration_onfocus()
{
	try
	{
		parent.IHM_Domaines_hide();
	}
	catch(e)
	{}
}

function	administration_selectCategory(lien)
{
	var		domaine;
	var		sousdomaine;
	var		url = lien.href;

	parent.IHM_Chargement_start();
	try
	{
		if ((url.indexOf("?domaine=") < 0) || (url.indexOf("&domaine=") < 0))
		{
			domaine = parent.domaines_getCurentDomaine();
			sousdomaine = parent.domaines_getCurentSousDomaine();
			if (domaine)
			{
				if (url.indexOf("?") < 0)
					url += "?";
				else
					url += "&";
				url += "domaine=" + escape(domaine);
				if (sousdomaine)
					url += "&sousdomaine=" + escape(sousdomaine);
			}
		}
	}
	catch(e)
	{}
	lien.href = url;
	//window.location = url;
}

function	administration_selectAdminCarto()
{
	parent.IHM_Chargement_start();
	window.open("AdministrationCarto/?map=POSTGIS_RA.map", "Administration_cartographie_dynamique", "directories=0, fullscreen=0, menubar=0, status=1, toolbar=0, resizable=1");
}

function administration_database_changeDB()
{
  var f=document.select_database;
  f.action='';
  f.target='';
  f.submit();
}

function reBuildIndex(){
  //parent.IHM_Chargement_start();
  parent.Ext.Ajax.timeout = 300000;  // set timout
  parent.Ext.Msg.alert("Indexation de Géosource", "Attention, cette action peut prendre un certain temps, elle est exécutée en tâche de fond.");
  //var iframe = this;
  var iFrameUrl = parent.IHM_getBaseUrl();
  iFrameUrl += "administration_geosource_index.php";
  parent.frames["srv_sync"].location = iFrameUrl;
  //var queryParams = {};
  //iframe.onSuccess = function(){};
  //parent.AjaxRequest(ajaxUrl, queryParams, iframe);
  
}

/**
 * Geonetwork web service pour l'indexation (exécuté en AJAX)
 * @param service service demandé
 * @return
 */
function idxOperation(service, adminPwd)
{
  var iframe = this;
  iframe.tabParams = new Array(service, adminPwd)
  iframe.onSuccess = confirm_idxOperation;
  parent.Ext.Msg.confirm("Gestion de l'index", "Il est préférable de réaliser cette opération en dehors des périodes de forte activité. Souhaitez-vous continuer ?", parent.confirm, iframe);
}

function confirm_idxOperation(service, adminPwd){
  var url = "/"+parent.geonetwork_dir+"/j_spring_security_check";
  parent.Ext.Ajax.request({
	url : url , 
	method : 'POST',
	headers: {
	        "Content-Type": "application/x-www-form-urlencoded"
	    },
    params : {
             'username':"administrateur",
             'password':adminPwd
    },
	success: function(result){
	  var url = "/"+parent.geonetwork_dir+"/srv/fre/" + service;
	  parent.Ext.Ajax.request({
	  	  url : url , 
	  	  method : 'GET',
	  	  params : {
	  		reset:"yes"  
	  	  },
	  	  success: function(result){
	  		  var root = result.responseXML.documentElement;
	  	      var resp = root.getElementsByTagName('status')[0].firstChild.nodeValue;
	  	      if (resp == "true")
	  	    	  parent.Ext.Msg.alert("Gestion de l'index", "L'opération est en cours");
	  	      else
	  	    	  parent.Ext.Msg.alert("Gestion de l'index", "Echec de l'opération");
	  	  },
	  	  failure: function(result){
	  		  parent.Ext.Msg.alert("Gestion de l'index", "Echec de l'opération");
	  	  }
	  });
	},
	failure: function(result){
	  parent.Ext.Msg.alert("Gestion de l'index", "Echec de l'opération");
	}
  });

}

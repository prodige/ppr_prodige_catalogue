

var tabExtPopup = new Array();
var popupWindowGroup = new Ext.WindowGroup();
Ext.namespace('Ext.ux.plugins');

/**
 * Ouverture de popups
 */
function loadExtPopup(url, cible, title, width, height, bClosable){
  
   
  if (!width || width==0){
    width = 400;
  }
  if (!height || height==0){
    height = 300;
  }
  var popup_position = tabExtPopup.length;
  var err1 = "La fenêtre est déjà ouverte !";
  
  try{
    for (var i=0; i<topWindow().frames.length; i++){
      if (topWindow().frames[i].name==cible){
        throw err1;
      }
    }
    var url = url+"&target="+cible+"&popup_pos="+popup_position;
    if (tabExtPopup[popup_position]){
      tabExtPopup[popup_position].close();
    }
    /**extjs 3 **/
    tabExtPopup[popup_position] = new Ext.ux.ManagedIFrame.Window({
      title         : title,
      width         : eval(width),
      height        : eval(height),
		  constrain     : true,
      maximizable   : true,
      collapsible   : true,
      closable      : bClosable,
      shadow        : Ext.isIE,
      animCollapse  : false,
      autoScroll    : true,
      manager       : popupWindowGroup,
      hideMode      : 'nosize',
      defaultSrc    : url,
      loadMask      :  {msg:'Chargement...'},
      close         : function(){closeExtPopup(popup_position);},//'close'
      closeAction   : 'close'
    });

    tabExtPopup[popup_position].show();

    if (tabExtPopup[popup_position].getFrameWindow()){
     if (window.frames[cible]){
        //ext js does'nt destroy the window.frames[cible] when destroying tabExtPopup[popup_position]
        //so if this iframe already exists we remplace it with the new one
        window.frames[cible]=tabExtPopup[popup_position].getFrameWindow();  
     }
     tabExtPopup[popup_position].getFrameWindow().name = cible;
     
    }
    
  }
  catch(e){
    if (e == err1){
      alert(err1);
    }
    else{
      throw e;
    }
  }
}

/**
 * Ouverture de popups
 */
function loadExtWindow(url, cible, title, loadfunction, width, height){

  if (!width || width==0){
    width = 400;
  }
  if (!height || height==0){
    height = 300;
  }
  var popup_position = tabExtPopup.length ;
  var err1 = "La fenêtre est déjà ouverte !";
  try{
    for (var i=0; i<topWindow().frames.length; i++){
      if (topWindow().frames[i].name==cible){
        throw err1;
      }
    }
    var url = url+"&target="+cible+"&popup_pos="+popup_position;
 
    if (tabExtPopup[popup_position]){
      tabExtPopup[popup_position].close();
    }

    var popup_position = tabExtPopup.length ;
  
    if (tabExtPopup[popup_position]){
      tabExtPopup[popup_position].close();
    }

    tabExtPopup[popup_position] = new Ext.Window({          
        autoLoad : {
          url : url,
          scripts : true
        },
        width       : 800,
        height      : 600,
		    constrain   :true,
        resizable   : true,             
        title       : title,
        manager     : popupWindowGroup,
        close         : function(){closeExtWindow(popup_position);},//'close'
        bodyStyle   :"overflow:auto"
    });
    
    tabExtPopup[popup_position].show();

  }
  
  catch(e){
    if (e == err1){
      alert(err1);
    }
    else{
      throw e;
    }
  }
}

/**
 * Fermeture de la popup
 */
function closeExtPopup(popup_position){
 if (tabExtPopup[popup_position]!=null){
  	if ( tabExtPopup[popup_position].getFrameWindow() ) {
  	  // delete popup window name (useful when destroy() fail)
  	  tabExtPopup[popup_position].getFrameWindow().name = null;
  	}
    tabExtPopup[popup_position].destroy();
    tabExtPopup[popup_position]=null;    
  }
}

/**
 * Fermeture de la window extjs
 */
function closeExtWindow(popup_position){
  if (tabExtPopup[popup_position]!=null){
    tabExtPopup[popup_position].destroy();
    tabExtPopup[popup_position]=null;
  }
}

/**
 * Ajustement de la taille de la popup
 */
function resizeExtPopup(popup_position, width, height){
  if (tabExtPopup[popup_position]){
     //+20 en largeur pour prendre en compte l'éventuelle barre de scroll
     //+45 en hauteur pour prendre en compte la place réservée au titre de la popup + scroll si présent
    tabExtPopup[popup_position].setSize(width+20,height+45);   
    tabExtPopup[popup_position].doLayout();
  }
}

/**
 * Recentrage de la popup
 */
function moveExtPopup(popup_position){
  var nb_active_popups = 0;
  var last_active_popup = 0;
  //compte les popups affichées
  for (var i = 0 ; i < tabExtPopup.length ; i++){
    if (tabExtPopup[i]!=null){
      nb_active_popups++;
      if (i!=(tabExtPopup.length-1)){
        last_active_popup = i;
      }
    }
  }
  //si 0 alors on met la popup en haut à gauche du panel de la carte
  if (nb_active_popups == 1){
    if (tabExtPopup[popup_position]){
     // tabExtPopup[popup_position].alignTo(center_panel.getEl(), 'tl-tl');
    }
    //if bottom not visible
    if (tabExtPopup[popup_position].getEl().getBottom() > center_panel.getEl().getBottom()){
        //alert('align to '+center_panel.getEl().getBottom()); 
        tabExtPopup[popup_position].alignTo(center_panel.getEl(),'b-b', [0,0]);
      }
  }
  else{
    //sinon on aligne les popups
    if (tabExtPopup[popup_position]){
      tabExtPopup[popup_position].alignTo(tabExtPopup[last_active_popup].getEl(),'tl-tl', [6, 20]);
      //on repositionne toutes les fenêtres du groupe
      //for each window du windowgroup
      var previouswin = null;
      popupWindowGroup.each(function(win){
        if (win.id == tabExtPopup[popup_position].id){
          if (previouswin){//si pas première popup
            //si en l'ajoutant à droite de la dernière popup
            // ça dépasse du center_panel à droite, on le met sur la gauche du center panel, en dessous
            //alert(win.getEl().getRight()+' > ? '+center_panel.getEl().getRight());
            if (win.getEl().getRight() > center_panel.getEl().getRight()){
              //si hauteur dépasse le bas de la fenetre
              //on aligne sur le bas de la fenetre
              if (win.getEl().getBottom() > center_panel.getEl().getBottom()){
                //alert('align to '+center_panel.getEl().getBottom()); 
                win.alignTo(center_panel.getEl(),'b-b', [0,0]);
              }
              else{
                win.alignTo(previouswin.getEl(), 'tl-tl', [ -(center_panel.getEl().getLeft() - previouswin.getEl().getLeft()) , 20]);
              }
            }
           else{//sinon on l'ajoute normalement
              //si hauteur dépasse le bas de la fenetre
              //on aligne sur le bas de la fenetre
              //alert(win.getEl().getBottom() > center_panel.getEl().getBottom());
              if (win.getEl().getBottom() > center_panel.getEl().getBottom()){
                //alert('align to '+center_panel.getEl().getBottom()); 
                win.alignTo(center_panel.getEl(),'b-b', [0,0]);
              }
              else{
                win.alignTo(previouswin.getEl(), 'tl-tl', [6, 20]);
              }
            }
          }
          else{//si première popup, on la laisse au centre, donc on ne fait rien
            //win.alignTo(center_panel.getEl(), 'c');
            if (win.getEl().getBottom() > center_panel.getEl().getBottom()){
              //alert('align to '+center_panel.getEl().getBottom()); 
              win.alignTo(center_panel.getEl(),'b-b', [0,0]);
            }
          }
        }
        previouswin = win;
      }
      );
    }
  }
  
}

/**
 * @version 3.2
 * traite le prompt extjs et applique la fonction onSuccess définie lors de l'appel
 * @param btn nom du bouton actionné
 * @param text texte du prompt
 * @return
 */
function prompt(btn, text){
  var fn;
  if (btn == 'ok'){
    if ( typeof this.onSuccess == 'function' ) {
      fn = this.onSuccess;
      if ( !this.onSuccess.createDelegate ) {
        fn = function(){
          return this.onSuccess.apply(this, arguments)
        }
      }
    }
    fn.createDelegate(this, ( this.tabParams.concat && typeof this.tabParams.concat == 'function' ? this.tabParams.concat(text) : [text] )).call();
  }
}

/**
 * @version 3.2
 * @todo vérifier que this.tabParams et this.tabParamsNo sont bien des tableaux
 * traite le confirm extjs et applique la fonction onSuccess définie lors de l'appel
 * @param btn nom du bouton actionné
 * @return
 */
function confirm(btn){
  var fn;
  if (btn == 'yes') {
    if ( typeof this.onSuccess == 'function' ) {
      fn = this.onSuccess;
      if ( !this.onSuccess.createDelegate ) {
        fn = function(){
          return this.onSuccess.apply(this, arguments)
        }
      }
      fn.createDelegate(this, ( this.tabParams ? this.tabParams : [] )).call();
    }
  } else {
    if ( typeof this.onResponseNo == 'function' ) {
      fn = this.onResponseNo;
      if ( !this.onResponseNo.createDelegate ) {
        fn = function(){
          return this.onResponseNo.apply(this, arguments)
        }
      }
      fn.createDelegate(this, ( this.tabParamsNo ? this.tabParamsNo : [] )).call();
    }
  }
}

/**
 * @version 3.2
 * effectue un appel Ajax
 * @param ajaxUrl        URL de l'appel
 * @param queryParams    paramètres de l'appel
 * @param iframe         objet contenant les fonctions "onSuccess" ainsi que les paramètres "tabParams" associés
 * @param method         méthode de l'appel ("GET" par défaut)
 */
function AjaxRequest(ajaxUrl, queryParams, iframe, method){
  Ext.Ajax.request({
    url : ajaxUrl , 
    params : queryParams,
    method : ( method ? method : 'GET' ),
    success : function(result){
      var fn;
      if ( typeof iframe.onSuccess == 'function' ) {
        fn = iframe.onSuccess;
        if ( !iframe.onSuccess.createDelegate ) {
          fn = function(){
            return iframe.onSuccess.apply(this, arguments)
          }
        }
        fn.createDelegate(this, ( iframe.tabParams && iframe.tabParams.concat && typeof iframe.tabParams.concat == 'function' ? iframe.tabParams.concat(result.responseText) : [result.responseText] )).call();
      }
    },
    failure : function(result){
      console.log("failedAjax")
      console.log(result);
    } 
  });
}
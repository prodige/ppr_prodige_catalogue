//var GROUPING_URL_SAVE = "droits.php?url=/Administration/Grouping/Grouping_save.php&";

/**
 * @brief Initialise la taille
 */
function setSizeIFrame() { 
   var iframe = parent.frames['Module'].frameElement;
   var pere=parent;
   var principal = parent.parent.parent.parent.parent.frames['fenetre']; 
   var new_height=principal.innerHeight;
   iframe.style.height = new_height-40+'px';
                   
 } 
/**
 * @brief Initisalition de la page
 * @return
 */
function	administration_init()
{
	try
  {
    parent.IHM_historique_add(window.location);
    parent.IHM_Chargement_stop();
  }
  catch(e){}
  
	administration_onfocus();
}

function	administration_onfocus()
{
	try
	{
		parent.IHM_Domaines_hide();
	}
	catch(e)
	{}
}

/**
 * @brief Sélection d'une catégorie
 * @param lien
 * @return
 */
function	administration_selectCategory(lien)
{
	var		domaine;
	var		sousdomaine;
	var		url = lien.href;

	parent.IHM_Chargement_start();
	try
	{
		if ((url.indexOf("?domaine=") < 0) || (url.indexOf("&domaine=") < 0))
		{
			domaine = parent.domaines_getCurentDomaine();
			sousdomaine = parent.domaines_getCurentSousDomaine();
			if (domaine)
			{
				if (url.indexOf("?") < 0)
					url += "?";
				else
					url += "&";
				url += "domaine=" + escape(domaine);
				if (sousdomaine)
					url += "&sousdomaine=" + escape(sousdomaine);
			}
		}
	}
	catch(e)
	{}
	lien.href = url;
	//window.location = url;
}

/**
 * @brief Clique sur l'admin carto
 * @return
 */
function	administration_selectAdminCarto()
{
	parent.IHM_Chargement_start();
	window.open("AdministrationCarto/?map=POSTGIS_RA.map", "Administration_cartographie_dynamique", "directories=0, fullscreen=0, menubar=0, status=1, toolbar=0, resizable=1");
}

//regroupement des groupes-profil

/**
 * @brief Ajoute une groupe profil
 * 
 */
function addGroupProfil()
{
  var form=document.groupes_profil;
  var result_profil_name=form.elements['result_profil_name'];
  var result_profil_id=form.elements['result_profil_id']; 
  if (result_profil_id.value==""){
    parent.Ext.Msg.alert("Gestion des groupes", "Veuillez entrer un identifiant pour le nouveau groupe-profil.");
    return;
  }
  if (result_profil_name.value==""){
    parent.Ext.Msg.alert("Gestion des groupes", "Veuillez entrer un nom pour le nouveau groupe-profil.");
    return;
  }  else{
    //form.action = GROUPING_URL_SAVE+"iMode=1";
    form.action = Routing.generate('catalogue_administration_grouping_save', {
      iMode: '1'
    });
    form.submit();
  }
}

/**
 * @brief Gère le clic sur un autre groupe profil dans la liste select
 * @param mode
 * @return
 */
function onChangeGroupeProfil(mode)
{
  var form=document.groupes_profil;
  pk_grp_profil1=form.elements['pk_groupProfil1'];
  pk_grp_profil2=form.elements['pk_groupProfil2'];
  if (mode==1){
    var selectedIndex=pk_grp_profil1.selectedIndex;
    var value_to_del=pk_grp_profil1.options[selectedIndex].value;
    //form.action="GroupingGroupesProfil_sql.php?groupProfil=2&value_to_del="+value_to_del;
    form.action = Routing.generate('catalogue_administration_grouping_profile_sql', {
      groupProfil: 2,
      value_to_del: value_to_del
    });
  }
  if (mode==2){
    var selectedIndex=pk_grp_profil2.selectedIndex;
    var value_to_del=pk_grp_profil2.options[selectedIndex].value;
    //form.action="GroupingGroupesProfil_sql.php?groupProfil=1&value_to_del="+value_to_del;
    form.action = Routing.generate('catalogue_administration_grouping_profile_sql', {
      groupProfil: 1,
      value_to_del: value_to_del
    });
  }
  var oldTarget = form.target;
  form.target="footerExec";
  form.submit();
  form.target = oldTarget;
}

//regroupement des sous-domaines
/**
 * @brief Gère le clic sur un autre domaine dans la liste select
 */
function onChangeDom()
{
  var form=document.regrpt_ssdom;
  form.action="";
  var oldTarget = form.target;
  form.target="";
  form.submit();
  form.target = oldTarget;
}

/**
 * @brief Gère le clic sur un autre sous-domaine dans la liste select
 * @param no_ssdom
 */
function onChangeSSDom(no_ssdom)
{
  var form=document.regrpt_ssdom;
  pk_dom=form.elements["pk_dom"];
  pk_dom_selected_index=pk_dom.selectedIndex;
  pk_dom_value=pk_dom.options[pk_dom_selected_index].value;
  
  pk_ssdom1=form.elements["pk_ssdom1"];
  pk_ssdom2=form.elements["pk_ssdom2"];
  if (no_ssdom==1){
    var selected_index=pk_ssdom1.selectedIndex;
    var value_to_del=pk_ssdom1.options[selected_index].value;
    //form.action="GroupingSubDomains_sql.php?iMode=1&no_ssdom=2&pk_dom_value="+pk_dom_value+"&value_to_del="+value_to_del; 
    Routing.generate('catalogue_administration_grouping_subdomains_sql', {
      iMode: 1,
      no_ssdom: 2,
      pk_dom_value:pk_dom_value,
      value_to_del: value_to_del
    });
  }
  if (no_ssdom==2){
    var selected_index=pk_ssdom2.selectedIndex;
    var value_to_del=pk_ssdom2.options[selected_index].value; 
    //form.action="GroupingSubDomains_sql.php?iMode=1&no_ssdom=1&pk_dom_value="+pk_dom_value+"&value_to_del="+value_to_del;
    Routing.generate('catalogue_administration_grouping_subdomains_sql', {
      iMode: 1,
      no_ssdom: 1,
      pk_dom_value:pk_dom_value,
      value_to_del: value_to_del
    });
  }
  var oldTarget = form.target;
  form.target="footerExec";
  form.submit();
  form.target = oldTarget;
}
/**
 * @brief Ajoute un sous-domaine
 * @return
 */
function addSSDom()
{
  form=document.regrpt_ssdom;
  var result_ssdom_name=form.elements['result_ssdom_name'];
  var result_ssdom_id=form.elements['result_ssdom_id'];
  if (result_ssdom_id.value==""){
    parent.Ext.Msg.alert("Gestion des domaines", "Veuillez entrer un identifiant pour le nouveau sous-domaine.");
    return;
  }
  if (result_ssdom_name.value==""){
    parent.Ext.Msg.alert("Gestion des domaines", "Veuillez entrer un nom pour le nouveau sous-domaine.");
    return;
  }  else{
    //form.action = GROUPING_URL_SAVE+"iMode=2";
    form.action = Routing.generate('catalogue_administration_grouping_save', {
      iMode: '2'
    });
    form.submit();
  }
}

  

/**
 * @brief Initialisation de la page
 * @return
 */
function	administration_carto_init()
{
	try
	{
		parent.IHM_historique_add(window.location);
		parent.IHM_Chargement_stop();
		administration_carto_domaineChange();
		parent.domaines_event_onchange_add(parent.events_administration_carto_changedomaine);
		administration_carto_onfocus();
		
	}
	catch(e)
	{}
}

function	administration_carto_onfocus()
{
	var		domaine;

	try
	{
		domaine = parent.domaines_getCurentDomaine();
		if (domaine)
			parent.IHM_Domaines_resume();
		else
			parent.IHM_Domaines_show();
	}
	catch(e){}
}

/**
 * @brief Gère le clic sur un autre domaine
 * @return
 */
function	administration_carto_domaineChange()
{
	var		domaine;
	var		sousdomaine;
	try
	{
		domaine = parent.domaines_getCurentDomaine();
		sousdomaine = parent.domaines_getCurentSousDomaine();
		if (domaine){
			administration_carto_loadData(domaine, sousdomaine);
		}
		else
		{
		  parent.Ext.getCmp('westLayout').setTitle("Domaine / Sous-domaine");
			parent.IHM_Panier_hide();
			parent.IHM_Domaines_show();
			parent.IHM_Chargement_stop();
			parent.IHM_NoSelectionDomain_show();
      window.timerOnload = setTimeout("window.onload()", 2500);
		}
	}
	catch(e){}
}
/**
 * @brief Charge les données dans un sous-domaine/domaine
 * @param domaine
 * @param sousdomaine
 * @return
 */
function	administration_carto_loadData(domaine, sousdomaine)
{
  if ( document.location.pathname.indexOf("administration")==-1 ){
    
  }
  else {
  	var		url;
  	var		params;
  	var		i;
  	var		param;
  	var		curdom = null;
  	var		cursdom = null;
    if ( window.timerOnload ){
      clearTimeout(window.timerOnload);
      window.timerOnload = null;
    }
  	parent.IHM_Chargement_start();
  	//administration_carto_setResume(domaine, sousdomaine);
  	url = document.location.pathname + "?domaine=" + domaine;
  	params = document.location.search.substr(1).split("&");
  	for (i = 0; i < params.length; i++)
  	{
  		param = params[i].split("=");
  		if (param[0] == "domaine")
  			curdom = unescape(param[1]);
  		if (param[0] == "sousdomaine")
  			cursdom = unescape(param[1]);
  	}
  	if (sousdomaine)
  		url += "&sousdomaine=" + sousdomaine;
  	if ((!curdom && domaine) || (!cursdom && sousdomaine) || (cursdom && !sousdomaine) || (curdom != domaine) || (cursdom != sousdomaine)){
  	  document.location.replace(url);
  	}
    else
      parent.IHM_Chargement_stop();
  }
}

/**
 * @brief Ouvre l'interface cartographique
 * @param url
 * @return
 */
function	administration_carto_openInterface(action, login, pwd, mdtitle, PN_IS_ADM_PRODIGE)
{
	if(document.administration_carto){
	    document.administration_carto.login.value = login;
	    document.administration_carto.pass.value = pwd;
	    document.administration_carto.mdtitle.value = mdtitle;
	    //document.administration_carto.PN_IS_ADM_PRODIGE.value = PN_IS_ADM_PRODIGE;
	    document.administration_carto.action = action;
	    parent.administration_carto_win_cartodynamique = window.open("blank.html", "Cartographie_dynamique", "directories=0, fullscreen=yes, menubar=0, status=1, toolbar=0, resizable=1");
	    document.administration_carto.submit();
	}
}

/**
 * @brief Ouvre l'interface cartographique
 * @param url url de la carte
 * @param mapName nom de la carte
 * @return
 */
function  consultation_openInterface(url, mapName)
{
  //maj Logs
  var iframe = this;
  iframe.tabParams = new Array(mapName);
  
 	var ajaxUrl = parent.IHM_getBaseUrl()+"consultation_addLog.php";
  var queryParams = {'logFile' : 'ConsultationCarte', 'objectName' :mapName};
  iframe.onSuccess = function(){};
  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
  parent.consultation_win_cartodynamique = window.open(url, "Cartographie_dynamique", "directories=0, fullscreen=yes, menubar=0, status=1, toolbar=yes, resizable=1");
}

/**
 * @version 3.2 - 06/04/2012
 * @brief Supprime le service
 * @param metadata_id
 * @return
 */
function administration_carto_delService(metadata_id, service_nom, onSuccess, tabParams)
{
  var iframe = this;
  iframe.tabParams = [metadata_id, onSuccess, tabParams];
  iframe.onSuccess = confirm_administration_carto_delService;
  iframe.tabParamsNo = tabParams;
  iframe.onResponseNo = onSuccess;
	parent.Ext.Msg.confirm("Suppression du service", "Le service "+service_nom+" sera supprimé, les contextes liés à ce service sont susceptibles de ne plus fonctionner.<br>" +
			                "Souhaitez-vous continuer  ?", parent.confirm, iframe);
}

/**
 * @version 3.2 - 06/04/2012
 * @param metadata_id
 * @param deldata
 * @return
 */
function confirm_administration_carto_delService(metadata_id, onSuccess, tabParams){
  var iframe = this;
  iframe.tabParams = tabParams;
  var ajaxUrl = parent.IHM_getBaseUrl()+"administration_carto_Action.php";
  var queryParams = {action : "del", metadataId : escape(metadata_id)};
  iframe.onSuccess = onSuccess;
  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
}

/**
 * @version 3.2 - 07/03/2012
 * @brief Supprime la carte
 * @param pk_carte
 * @param pk_stockage_carte
 * @param metadata_id
 * @return
 */
function	administration_carto_delMap(pk_carte, pk_stockage_carte, metadata_id, carte_nom, onSuccess, tabParams)
{
	
	var iframe = this;
  iframe.tabParams = [pk_carte, pk_stockage_carte, metadata_id, true, onSuccess, tabParams];
  iframe.onSuccess = confirm_administration_carto_delMap;
  iframe.tabParamsNo = tabParams;
  iframe.onResponseNo = onSuccess;
	parent.Ext.Msg.confirm("Suppression de carte", "La carte "+carte_nom+" sera supprimée, les contextes liés à cette carte sont susceptibles de ne plus fonctionner.<br>" +
			                "Souhaitez-vous continuer  ?", parent.confirm, iframe);
}

/**
 * @version 3.2 - 07/03/2012
 * @param pk_carte
 * @param pk_stockage_carte
 * @param metadata_id
 * @param deldata
 * @return
 */
function confirm_administration_carto_delMap(pk_carte, pk_stockage_carte, metadata_id, deldata, onSuccess, tabParams){
  
  var iframe = this;
  iframe.tabParams = tabParams;
  var ajaxUrl = parent.IHM_getBaseUrl()+"administration_carto_Action.php";
  var queryParams = {action : "del", stockageCarteId : pk_stockage_carte, carteId : pk_carte, metadataId : escape(metadata_id), deldata : deldata};
  iframe.onSuccess = onSuccess;
  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
  
  /**
   * @deprecated v3.2 - 07/03/2012
   */
  /*
  var   params;
  var   i;
  var   param;
  var   curdom = null;
  var   cursdom = null;
  var   url;

  params = document.location.search.substr(1).split("&");
	for (i = 0; i < params.length; i++)
	{
		param = params[i].split("=");
		if (param[0] == "domaine")
			curdom = param[1];
		if (param[0] == "sousdomaine")
			cursdom = param[1];
	}
	url = parent.IHM_getBaseUrl();
	url += "administration_carto_Action.php?action=del&stockageCarteId=" + pk_stockage_carte + "&carteId=" + pk_carte + "&metadataId=" + escape(metadata_id);
	if (deldata)
		url += "&deldata=1";
	if (curdom)
	{
		url += "&domaine=" + escape(curdom);
		if (cursdom)
			url += "&sousdomaine=" + escape(cursdom);
	}
	window.location = url;
	*/
}

/**
 * @version 3.2 - 07/03/2012
 * @brief Supprime la métadonnée associée aux données MAJIC
 * @param pk_couche
 * @param metadata_id
 * @return
 */
function  administration_carto_delCoucheMajic(pk_couche, metadata_id, couche_nom, onSuccess, tabParams)
{
  var iframe = this;
  iframe.tabParams = [pk_couche, metadata_id, true, "majic", couche_nom, onSuccess, tabParams];
  iframe.onSuccess = confirm_administration_carto_delDataConfirmed;
  iframe.tabParamsNo = tabParams;
  iframe.onResponseNo = onSuccess;
  parent.Ext.Msg.confirm("Suppression de couche", "La donnée "+couche_nom+" va être supprimée. Souhaitez-vous continuer ?", parent.confirm, iframe);
  
}


/**
 * @version 3.2 - 07/03/2012
 * @brief Supprime la couche et sa métadonnée associée
 * @param pk_couche
 * @param metadata_id
 * @return
 */
function	administration_carto_delCouche(pk_couche, metadata_id, layerTable, coucheType, coucheSrv, couche_nom, onSuccess, tabParams)
{
	var iframe = this;
  iframe.tabParams = [pk_couche, metadata_id, true, layerTable, coucheType, coucheSrv, couche_nom, onSuccess, tabParams];
  iframe.onSuccess = confirm_administration_carto_delData;
  iframe.tabParamsNo = tabParams;
  iframe.onResponseNo = onSuccess;
	parent.Ext.Msg.confirm("Suppression de couche", "La donnée "+couche_nom+" va être supprimée. Souhaitez-vous continuer ?", parent.confirm, iframe);
}


/**
 * @version 3.2 - 07/03/2012
 * Suppression d'une donnée, appel de la liste des cartes impactées en ajax
 * @param pk_couche identifiant de donnée
 * @param metadata_id identifiant de métadonnée
 * @param deldata vrai si suppression de la donnée
 * @param layerTable nom de la table
 * @param coucheType type de couche (vecteur ou raster)
 * @param coucheSrv url du serveur
 * @return
 */  
function confirm_administration_carto_delData(pk_couche, metadata_id, deldata, layerTable, coucheType, coucheSrv, couche_nom, onSuccess, tabParams){
  
  var iframe = this;
  iframe.tabParams = new Array(pk_couche, metadata_id, coucheType, couche_nom, onSuccess, tabParams);
  var ajaxUrl = window.location.protocol+"//"+ coucheSrv + "/PRRA/Administration/Administration/Layers/LayerGetMaps.php";
  var queryParams = {coucheType : coucheType, layerTable : layerTable};
  iframe.onSuccess = showResultLayerMaps;
  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
}

/**
 * @version 3.2 - 07/03/2012
 * @brief Affichage de la liste des mapfiles contenant la couche
 * @param pk_couche identifiant de donnée
 * @param metadata_id identifiant de métadonnée
 * @param responseText retour Ajax
 * @return
 */  
function showResultLayerMaps(pk_couche, metadata_id, coucheType, couche_nom, onSuccess, tabParams, responseText){
  
  var iframe = this;
  iframe.tabParams = [pk_couche, metadata_id, true, coucheType, onSuccess, tabParams];
  iframe.onSuccess = confirm_administration_carto_delDataConfirmed;
  iframe.tabParamsNo = tabParams;
  iframe.onResponseNo = onSuccess;
  var tabmaps = eval(responseText);
  if(tabmaps.length == 0)
    parent.Ext.Msg.confirm("Suppression de couche", "Aucune carte ne contient la couche "+couche_nom+", souhaitez-vous poursuivre la suppression ?", parent.confirm, iframe);
  else{
    var strMaps ="";
    for (var i=0; i<tabmaps.length;i++)
      strMaps+= "<b>"+tabmaps[i]+"</b><br>"; 
    parent.Ext.Msg.confirm("Suppression de couche", "La couche "+couche_nom+" est contenue dans les cartes suivantes :<br>" +
                        strMaps+ "La couche sera supprimée des cartes. Souhaitez-vous poursuivre la suppression ?", parent.confirm, iframe);
  }
}

/**
 * @version 3.2 - 07/03/2012
 * suppression d'une donnée confirmée
 * @param pk_couche identifiant de donnée
 * @param metadata_id identifiant de métadonnée
 * @param deldata boolean permet la suppression des données associées
 * @return
 */  
function confirm_administration_carto_delDataConfirmed(pk_couche, metadata_id, deldata, mode, onSuccess, tabParams){
  
  var iframe = this;
  iframe.tabParams = tabParams;
  var ajaxUrl = parent.IHM_getBaseUrl()+"administration_carto_Action.php";
  var queryParams = {action : "del", coucheId : pk_couche, metadataId : escape(metadata_id), deldata : deldata, mode : mode};
  iframe.onSuccess = onSuccess;
  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
  
  /**
   * @deprecated v3.2 - 07/03/2012
   */
  /*
  var   params;
  var   i;
  var   param;
  var   curdom = null;
  var   cursdom = null;
  var   url;
  params = document.location.search.substr(1).split("&");
  for (i = 0; i < params.length; i++)
  {
    param = params[i].split("=");
    if (param[0] == "domaine")
      curdom = param[1];
    if (param[0] == "sousdomaine")
      cursdom = param[1];
  }
 	url = parent.IHM_getBaseUrl();
 	url += "administration_carto_Action.php?action=del&coucheId=" + pk_couche + "&metadataId=" + escape(metadata_id);
  if (deldata=="true")
    url += "&deldata=1";
  if (curdom)
  {
    url += "&domaine=" + escape(curdom);
    if (cursdom)
      url += "&sousdomaine=" + escape(cursdom);
  }
  url+="&mode="+mode;
  window.location = url;
  */
}

/**
 * @brief ajoute une couche WMS au serveur
 * @param pk_couche
 * @param type_service
 */
function administration_carto_webservice(pk_data, type_service, login, pwd, type_data){
	var		params;
	var		i;
	var		param;
	var		curdom = null;
	var		cursdom = null;
	var		url;
	var		deldata = false;

	params = document.location.search.substr(1).split("&");
	for (i = 0; i < params.length; i++)
	{
		param = params[i].split("=");
		if (param[0] == "domaine")
			curdom = param[1];
		if (param[0] == "sousdomaine")
			cursdom = param[1];
	}
	url = parent.IHM_getBaseUrl();
	if(type_data == 0) //couche
		url += "administration_carto_webservices.php?action="+type_service+"&coucheId=" + pk_data+"&login="+login+"&pass="+pwd+"&type_data="+type_data;
	if(type_data == 1)//carte
		url += "administration_carto_webservices.php?action="+type_service+"&carteId=" + pk_data+"&login="+login+"&pass="+pwd+"&type_data="+type_data;
	if (curdom){
		url += "&domaine=" + escape(curdom);
		if (cursdom)
			url += "&sousdomaine=" + escape(cursdom);
	}
	if(type_data == 0) //couche
		window.location = url;
	if(type_data == 1){
	  var iframe = this;
	  var ajaxUrl = url;
	  var queryParams = {};//{action:"valid",metadataId:metadata_id,statut:statut,mode:mode};
	  iframe.tabParams = null;
	  iframe.onSuccess = function(responseText){
	    var oRes = eval("("+responseText+")");
	    if ( oRes.success ) {
	      //alert("La carte est publié en WMS.");
	      parent.Ext.Msg.alert("Publication WMS", (oRes.enable ? "La carte a été correctement publiée en WMS" : "Le service WMS associé à la carte a été supprimé")) ;
	    } else {
	      parent.Ext.Msg.alert("Echec", oRes.msg);
	    }
	  };
	  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
	}//carte
}

/**
 * @version 3.2 - 07/03/2012
 * @brief Change le statut d'une fiche de métadonnées
 * @param metadata_id
 * @param title
 * @param statut
 * @param mode
 * @param onSuccess             fonction à exécuter après changement de statut
 * @return
 */
function	administration_carto_changeStatut(metadata_id, title, statut, mode, onSuccess)
{
	var		msg = "";
	var		fram;
	var		url;
	var		urlfram;

	try
	{
		if(mode!="majic"){
			switch(statut)
			{
				case 1:
					msg = "La fiche " + title + " va être renvoyée à la saisie";
					break;
				case 2:
					msg = "La fiche " + title + " va être proposée pour validation";
					break;
				case 3:
					msg = "La fiche " + title + " va être validée et proposée pour publication";
					break;
				case 4:
					msg = "La fiche " + title + " va être publiée";
					break;
				default:
					return;
					break;
			}
		}else{
			switch(statut)
			{
				case 1:
					msg = "La métadonnée majic va être renvoyée à la saisie";
					break;
				case 2:
					msg = "La métadonnée majic va être proposée pour validation";
					break;
				case 3:
					msg = "La métadonnée majic va être validée et proposée pour publication";
					break;
				case 4:
					msg = "La métadonnée majic va être publiée";
					break;
				default:
					return;
					break;
			}	
		}
	  var iframe = this;
	  iframe.tabParams = [metadata_id, statut, mode, onSuccess];
	  iframe.onSuccess = changeStatut;
	  parent.Ext.Msg.confirm('Changement de statut', msg, parent.confirm, iframe);
	}
	catch(e){}
}

/**
 * @version 3.2 - 07/03/2012
 * Acces à la page de modification de statut
 * @param metadata_id
 * @param statut
 * @param mode
 * @return
 */
function changeStatut(metadata_id, statut, mode, onSuccess){
  
  var iframe = this;
  var ajaxUrl = parent.IHM_getBaseUrl()+"administration_carto_Action.php";
  var queryParams = {action:"valid",metadataId:metadata_id,statut:statut,mode:mode};
  iframe.tabParams = null;
  iframe.onSuccess = function(responseText){
    var oRes = eval("("+responseText+")");
    if ( oRes.success ) {
      onSuccess();
    } else {
      parent.Ext.Msg.alert("Echec", oRes.msg);
    }
  };
  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
  
 /**
  * @deprecated v3.2 - 07/03/2012
  */
 /*
  var   curdom = null;
  var   cursdom = null;
  params = document.location.search.substr(1).split("&");
  for (i = 0; i < params.length; i++)
  {
    param = params[i].split("=");
    if (param[0] == "domaine")
      curdom = param[1];
    if (param[0] == "sousdomaine")
      cursdom = param[1];
  }
  var url = parent.IHM_getBaseUrl();
  url += "administration_carto_Action.php?action=valid";
  if (curdom)
  {
    url += "&domaine=" + escape(curdom);
    if (cursdom)
      url += "&sousdomaine=" + escape(cursdom);
  }
  url +="&metadataId=" + escape(metadata_id) + "&statut=" + statut + "&mode=" + mode;
  window.location = url;
  */
}

/**
 * @version 3.2 - 07/03/2012
 * @brief Change le statut de plusieurs fiches de métadonnées
 * @param tabMetadata           tableau d'informations de métadonnées sous la forme [{id:<id>, title:<title>, statut:<statut>}, {id:<id>, title:<title>, statut:<statut>}]
 * @param statut
 * @param mode
 * @param modePropositionActif  string true ou false
 * @param onSuccess             fonction à exécuter après changement de statut
 * @return
 */
function  administration_carto_action_changeStatut(tabMetadata, statut, mode, modePropositionActif, onSuccess)
{
  try
  {
    var tabMetadataId = [];
    var tabMetadataTitle = [];
    for ( var i=0; i<tabMetadata.length; i++ ) {
      switch ( statut ) {
        case 1 :  // action retirer
          if ( tabMetadata[i].statut != 1 ) {
            tabMetadataId.push(tabMetadata[i].id);
            tabMetadataTitle.push(tabMetadata[i].title);
          }
        break;
        case 2 : // action proposer
          if ( tabMetadata[i].statut == 1 ) {
            tabMetadataId.push(tabMetadata[i].id);
            tabMetadataTitle.push(tabMetadata[i].title);
          }
        break;
        case 3 : // action valider
          if ( !modePropositionActif && tabMetadata[i].statut == 1
            || tabMetadata[i].statut == 2 ) {
            tabMetadataId.push(tabMetadata[i].id);
            tabMetadataTitle.push(tabMetadata[i].title);
          }
        break;
        case 4 : // action publier
          if ( tabMetadata[i].statut == 3 ) {
            tabMetadataId.push(tabMetadata[i].id);
            tabMetadataTitle.push(tabMetadata[i].title);
          }
        break;
      }
    }
    if ( tabMetadataTitle.length == 0 ) {
      parent.Ext.Msg.alert("Actions sur la sélection", "Impossible de réaliser l'action demandée pour les métadonnées sélectionnées.");
    } else {
      var msg = ( tabMetadataTitle.length == 1 ? "La fiche " : "Les fiches " )+tabMetadataTitle.join(", ");
      switch ( statut ) {
        case 1 :
          msg += ( tabMetadataTitle.length == 1 ? " va être renvoyée" : " vont être renvoyées")+" à la saisie.";
        break;
        case 2 :
          msg += ( tabMetadataTitle.length == 1 ? " va être proposée" : " vont être proposées")+" pour validation.";
        break;
        case 3 :
          msg += ( tabMetadataTitle.length == 1 ? " va être validée et proposée" : " vont être validées et proposées")+" pour publication.";
        break;
        case 4 :
          msg += ( tabMetadataTitle.length == 1 ? "va être publiée" : " vont être publiées")+".";
        break;
      }
      var iframe = this;
      iframe.tabParams = new Array(tabMetadataId.join(","), statut, mode, onSuccess);
      iframe.onSuccess = changeStatutCarto;
      parent.Ext.Msg.confirm('Changement de statut', msg, parent.confirm, iframe);
    }
    
    /**
     * @deprecated v3.2 - 07/03/2012
     */
    /*
    var   msg = "";
    var   fram;
    var   url;
    var   urlfram;
    var   metadataList = "";
    
    var tabCheckbox = document.action_form.action;
    var nbFiche = 0;
    
    var strTitre = "";
    
    switch(statut)
    {
      case 1:
        if(typeof(tabCheckbox.length)!="undefined"){
          for (var i=0; i<tabCheckbox.length; i++){
            //case cochée et statut autre que en cours
            if(tabCheckbox[i].checked && document.action_form.elements["statut_"+tabCheckbox[i].value].value != 1){
              var tabInfoMetadata = tabCheckbox[i].value.split("_");
              metadataList+=tabInfoMetadata[1]+",";
              nbFiche ++;
              strTitre+= "<b>"+document.action_form.elements["titre_"+tabCheckbox[i].value].value + "</b><br>";
              
            }
          }
        }else{ //only one checkbox
          if(tabCheckbox.checked && document.action_form.elements["statut_"+tabCheckbox.value].value != 1){
            var tabInfoMetadata = tabCheckbox.value.split("_");
            metadataList+=tabInfoMetadata[1]+",";
            nbFiche ++;
            strTitre+= "<b>"+document.action_form.elements["titre_"+tabCheckbox.value].value + "</b><br>";
            
          }
        }
          
        if (nbFiche==0){
          parent.Ext.Msg.alert("Actions sur la sélection", "Impossible de réaliser l'action demandée pour les métadonnées sélectionnées");
          return;
        }else if(nbFiche==1){
          msg = "La fiche "+ strTitre+" va être renvoyée à la saisie";
        }else{
          msg = "Les fiches <br>"+ strTitre+" vont être renvoyées à la saisie";
        }
        break;
      case 2:
        if(typeof(tabCheckbox.length)!="undefined"){
          for (var i=0; i<tabCheckbox.length; i++){
            //case cochée et statut en cours
            if(tabCheckbox[i].checked && document.action_form.elements["statut_"+tabCheckbox[i].value].value == 1){
              var tabInfoMetadata = tabCheckbox[i].value.split("_");
              metadataList+=tabInfoMetadata[1]+",";
              nbFiche ++;
              strTitre+= "<b>"+document.action_form.elements["titre_"+tabCheckbox[i].value].value + "</b><br>";
            }
          }
        }else{ //only one checkbox
          if(tabCheckbox.checked && document.action_form.elements["statut_"+tabCheckbox.value].value == 1){
            var tabInfoMetadata = tabCheckbox.value.split("_");
            metadataList+=tabInfoMetadata[1]+",";
            nbFiche ++;
            strTitre+= "<b>"+document.action_form.elements["titre_"+tabCheckbox.value].value + "</b><br>";
          }
        }
        if (nbFiche==0){
          parent.Ext.Msg.alert("Actions sur la sélection", "Impossible de réaliser l'action demandée pour les métadonnées sélectionnées");
          return;
        }else if(nbFiche==1){
          msg = "La fiche "+ strTitre+" va être proposée pour validation";
        }else{
          msg = "Les fiches <br>"+ strTitre+" vont être proposées pour validation";
        }
        
        break;
      case 3:
        if(typeof(tabCheckbox.length)!="undefined"){
          for (var i=0; i<tabCheckbox.length; i++){
            //case cochée et statut attente validation ou en cours (si mode proposition désactivé)
            if(tabCheckbox[i].checked && (document.action_form.elements["statut_"+tabCheckbox[i].value].value == 2 
                || (modePropositionActif =="off" && document.action_form.elements["statut_"+tabCheckbox[i].value].value == 1) )){
              var tabInfoMetadata = tabCheckbox[i].value.split("_");
              metadataList+=tabInfoMetadata[1]+",";
              nbFiche ++;
              strTitre+= "<b>"+document.action_form.elements["titre_"+tabCheckbox[i].value].value + "</b><br>";
            }
          }
        }else{ //only one checkbox
          if(tabCheckbox.checked && (document.action_form.elements["statut_"+tabCheckbox.value].value == 2 
              || (modePropositionActif =="off" && document.action_form.elements["statut_"+tabCheckbox.value].value == 1) )){
            var tabInfoMetadata = tabCheckbox.value.split("_");
            metadataList+=tabInfoMetadata[1]+",";
            nbFiche ++;
            strTitre+= "<b>"+document.action_form.elements["titre_"+tabCheckbox.value].value + "</b><br>";
          }
        }
        if (nbFiche==0){
          parent.Ext.Msg.alert("Actions sur la sélection", "Impossible de réaliser l'action demandée pour les métadonnées sélectionnées");
          return;
        }else if(nbFiche==1){
          msg = "La fiche "+ strTitre+" va être validée et proposée pour publication";
        }else{
          msg = "Les fiches <br>"+ strTitre+" vont être validées et proposées pour publication";
        }
        
        break;
      case 4:
        if(typeof(tabCheckbox.length)!="undefined"){
          for (var i=0; i<tabCheckbox.length; i++){
            //case cochée et statut validée
            if(tabCheckbox[i].checked && document.action_form.elements["statut_"+tabCheckbox[i].value].value == 3){
              var tabInfoMetadata = tabCheckbox[i].value.split("_");
              metadataList+=tabInfoMetadata[1]+",";
              nbFiche ++;
              strTitre+= "<b>"+document.action_form.elements["titre_"+tabCheckbox[i].value].value + "</b><br>";
              
            }
          }
        }else{//only one checkbox
          if(tabCheckbox.checked && document.action_form.elements["statut_"+tabCheckbox.value].value == 3){
            var tabInfoMetadata = tabCheckbox.value.split("_");
            metadataList+=tabInfoMetadata[1]+",";
            nbFiche ++;
            strTitre+= "<b>"+document.action_form.elements["titre_"+tabCheckbox.value].value + "</b><br>";
            
          }
        }
        if (nbFiche==0){
          parent.Ext.Msg.alert("Actions sur la sélection", "Impossible de réaliser l'action demandée pour les métadonnées sélectionnées");
          return;
        }else if(nbFiche==1){
          msg = "La fiche "+ strTitre+" va être publiée";
        }else{
          msg = "Les fiches <br>"+ strTitre+" vont être publiées";
        }
        break;
      default:
        return;
        break;
    }
   
    var iframe = this;
    iframe.tabParams = new Array(metadataList, statut, mode);
    iframe.onSuccess = changeStatutCarto;
    parent.Ext.Msg.confirm('Changement de statut', msg, parent.confirm, iframe);
    */
  }
  catch(e){}
}

/**
 * @version 3.2
 * @brief effectue l'action de modification de statut d'un ensemble de métadonnées
 * @param metadataList
 * @param statut
 * @param mode
 * @param onSuccess
 * @return
 */
function changeStatutCarto(metadataList, statut, mode, onSuccess){
  
  var iframe = this;
  var ajaxUrl = parent.IHM_getBaseUrl()+"administration_carto_Action.php";
  var queryParams = {action:"valid",metadataId:metadataList,statut:statut,mode:mode};
  iframe.tabParams = null;
  iframe.onSuccess = function(responseText){
    var oRes = eval("("+responseText+")");
    if ( oRes.success ) {
      onSuccess();
    } else {
      parent.Ext.Msg.alert("Echec", oRes.msg);
    }
  };
  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
  
  /**
   * @deprecated v3.2 - 07/03/2012
   */
  /*
  var   curdom = null;
  var   cursdom = null;
  params = document.location.search.substr(1).split("&");
  for (i = 0; i < params.length; i++)
  {
    param = params[i].split("=");
    if (param[0] == "domaine")
      curdom = param[1];
    if (param[0] == "sousdomaine")
      cursdom = param[1];
  }
  var url = parent.IHM_getBaseUrl();
  url += "administration_carto_Action.php?action=valid";
  if (curdom)
  {
    url += "&domaine=" + escape(curdom);
    if (cursdom)
      url += "&sousdomaine=" + escape(cursdom);
  }
  url += "&metadataId=" + metadataList.substr(0, metadataList.length-1) + "&statut=" + statut + "&mode=" + mode;
  window.location = url;
  */
}
/**
 * @brief Ouvre geosource pour éditer la fiche de métadonnées
 * @param pk_objet
 * @param pk_objet_dep
 * @param meta_id
 * @param statut
 * @param mode
 * @return
 */
function	administration_carto_editFiche(pk_objet, pk_objet_dep, meta_id, statut, mode, bHasRightPublication)
{
	switch(statut)
	{
		case -1: // pas de fiche
		  
		  var iframe = this;
		  iframe.tabParams = new Array(pk_objet);
		  iframe.onSuccess = administration_carto_createFiche;
		  parent.Ext.Msg.confirm("Edition de métadonnée", "Aucune fiche n'est associée à la " + mode + ".\nUne nouvelle fiche va être créée.", parent.confirm, iframe);
		
			break;
		case 1: // saisie
		case 2: // attente validation
			try
			{
				parent.IHM_Fiche_Edit(meta_id, mode);
			}
			catch(e){}
			break;
		case 3: // validée
		case 4: // publiée
			if(bHasRightPublication){
			  try
	      {
	        parent.IHM_Fiche_Edit(meta_id, mode);
	      }
	      catch(e){}
			}else
			{
			  var iframe = this;
	      iframe.tabParams = new Array(pk_objet, pk_objet_dep, mode, meta_id);
	      iframe.onSuccess = administration_carto_DuplicateFiche;
	      parent.Ext.Msg.confirm("Edition de métadonnée", "La fiche doit être dupliquée pour pouvoir être éditée. Voulez-vous dupliquer cette fiche ?", parent.confirm, iframe);
			}
			break;
		default:
			break;
	}
}

/**
 * 
 * @param pk_objet
 * @return
 */
function administration_carto_createFiche(pk_objet){
  parent.IHM_Chargement_start();
  switch(mode)
  {
  case "couche":
    window.location = "/geonetwork/srv/fre/metadata.create.new?id=2&group=2&couche=" + pk_objet;
    break;
  case "carte":
    window.location = "/geonetwork/srv/fre/metadata.create.new?id=3&group=2&carte=" + pk_objet;
    break;
  }
}

/**
 * 
 * @param pk_objet
 * @param pk_objet_dep
 * @param mode
 * @param meta_id
 * @return
 */
function administration_carto_DuplicateFiche(pk_objet, pk_objet_dep, mode, meta_id){
  switch(mode)
  {
  case "couche":
    administration_carto_copyCouche(pk_objet, meta_id);
    break;
  case "carte":
    administration_carto_copyMap(pk_objet, pk_objet_dep, meta_id);
    break;
  }
}

/**
 * 
 * @param meta_id identifiant de la métadonnée dans Géosource
 * @return
 */
function administration_carto_addFicheEnfant(meta_id, domaine, sousdomaine){
  
  var url;
  url = parent.IHM_getBaseUrl();
  url += "administration_donnees_ajout.php?domaine="+domaine+
        (sousdomaine!="" ? "&sousdomaine="+sousdomaine : "")+
        "&child=y&metadataId=" + escape(meta_id);
  window.location = url;
  

}
  

/**
 * @brief Copie la carte
 * @param pk_carte
 * @param pk_stockage_carte
 * @param meta_id
 * @return
 */
function	administration_carto_copyMap(pk_carte, pk_stockage_carte, meta_id)
{
	var url;
	url = parent.IHM_getBaseUrl();
	url += "administration_carto_copy.php?action=copy&metadataId=" + escape(meta_id) + "&stockageCarteId=" + pk_stockage_carte + "&carteId=" + pk_carte + "&mode=carte";
	window.location = url;
}

/**
 * @brief Copie la couche
 * @param pk_couche
 * @param meta_id
 * @param domaine
 * @param sousdomaine
 * @return
 */
function	administration_carto_copyCouche(pk_couche, meta_id, domaine, sousdomaine)
{
	var	url;
	url = parent.IHM_getBaseUrl();
	url += "administration_carto_copy.php?action=copy&metadataId=" + escape(meta_id) + "&coucheId=" + pk_couche + "&mode=couche"
	      +"&domaine="+domaine+"&sousdomaine="+sousdomaine;
	window.location = url;
}


/**
 * @brief Copie la couche majic
 * @param pk_couche
 * @param meta_id
 * @param domaine
 * @param sousdomaine
 * @return
 */
function  administration_carto_copyMajic(pk_couche, meta_id, domaine, sousdomaine)
{
  var url;
  url = parent.IHM_getBaseUrl();
  url += "administration_carto_Action.php?action=copy&metadataId=" + escape(meta_id) + "&coucheId=" + pk_couche + "&mode=majic"
        +"&domaine="+domaine+"&sousdomaine="+sousdomaine;
  window.location = url;
}

/**
 * @brief Validation de la duplication
 */
function administration_copy_valider(mode){
	var form = document.form_copy;
	var name = form.NOM;
	var serveur = document.getElementById("SERVEUR");
	var modele = /^[a-z]+[a-z0-9_]*$/i;
  if (modele.test(name.value)){
  }
  else{
    parent.Ext.Msg.alert("Duplication", "Le nom de fichier saisi ne doit pas contenir de caractères\n" +
          "accentués ou non alphanumériques comme -()\"'. etc.");
    return;
  }
	if(in_array(name.value, tabData[serveur])){
    if(mode =="couche")
      parent.Ext.Msg.alert("Ajout de données", "Une table portant ce nom existe déjà.");
    else
      parent.Ext.Msg.alert("Ajout de carte", "Une carte portant ce nom existe déjà.");
    return;
  } 
	if(form){
		form.submit();
	}
}

/**
 * @brief Montre la fiche de métadonnées de la couche à partir de la cartographie
 * @param meta_id
 * @return
 */
function	administration_carto_showFiche(meta_id)
{
	try
	{
		parent.IHM_Fiche_Show(meta_id);
	}
	catch(e){}
}

/**
 * @brief Ajoute une nouvelle carte
 * @return
 */
function	administration_carto_ajoutCarte()
{
	try
	{
		parent.IHM_Chargement_start();
	}
	catch(e){}
	window.location = "administration_carto_ajout.php";
}

/**
 * @brief Ajoute une nouvelle couche
 * @return
 */
function	administration_carto_ajoutCouche()
{
	try
	{
		parent.IHM_Chargement_start();
	}
	catch(e){}
	window.location = "administration_donnees_ajout.php";
}

/**
 * @brief Importe les données d'une couche
 * @param pk_couche
 * @return
 */
function	administration_carto_importCouche(pk_couche, metadata_id)
{
  var   curdom = null;
  var   cursdom = null;
  params = document.location.search.substr(1).split("&");
  for (i = 0; i < params.length; i++)
  {
    param = params[i].split("=");
    if (param[0] == "domaine")
      curdom = param[1];
    if (param[0] == "sousdomaine")
      cursdom = param[1];
  }
  var url = parent.IHM_getBaseUrl();
  url += "Administration/Administration/Layers/LayersImportation.php?Id=" + pk_couche + "&metadata_id=" + metadata_id;
  if (curdom)
  {
    url += "&domaine=" + escape(curdom);
    if (cursdom)
      url += "&sousdomaine=" + escape(cursdom);
  }
  window.location = url;
}



/**
 * @brief Changer fichier d'une carte statique
 * @param pk_couche
 * @return
 */
function	administration_carto_changeStaticCarte(pk_carte, nom_fichier,fmeta_id)
{
  var url = parent.IHM_getBaseUrl();
  url += "administration_carto_changeStaticCarte.php?Id=" + pk_carte + "&file_name=" + nom_fichier+ "&fmeta_id="+fmeta_id;
  window.location = url;
}


/**
 * @brief open the map in a simplified mode to edit the default representation of the layer
 * @param action : url to open
 * @param login : login to connect paramCarto
 * @param pwd : pwd to connect paramCarto
 */
function  administration_carto_paramCarto(action, login, pwd)
{
  //open map in popup
  if(document.administration_carto){
    document.administration_carto.login.value = login;
    document.administration_carto.pass.value = pwd;
    document.administration_carto.action = action;
    parent.administration_carto_win_cartodynamique = window.open("blank.html", "Cartographie_dynamique", "directories=0, fullscreen=yes, menubar=0, status=1, toolbar=0, resizable=1");
    document.administration_carto.submit();
  }
  
	//window.open(url, "default_layer_map", "directories=0, fullscreen=0, menubar=0, status=1, toolbar=0, resizable=1");
}


/**
 * Réinitialisation de la représentation par défaut
 */
function reinitParamCarto(srvAdmin, layer_table){
	  
  var iframe = this; 
  iframe.tabParams = new Array("");
  var queryParams = {
	layerMapFile : layer_table
  }
  var ajaxUrl = window.location.protocol+"//" + srvAdmin + "/PRRA/LayerMapInit.php";
  iframe.onSuccess = reinitParamCartoSuccess;
  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
}

function reinitParamCartoSuccess(){
  parent.Ext.Msg.alert("Succès", "La représentation par défaut a été réinitialisée");	
}

/**
 * true if needle is in array (php equivalent)
 * @param needle
 * @param haystack
 * @param argStrict
 * @return
 */
function in_array (needle, haystack, argStrict) {
   
  var key = '', strict = !!argStrict; 
  if (strict) {
      for (key in haystack) {
        if (haystack[key] === needle) {
          return true;            
        }
      }
  } else {
      for (key in haystack) {
        if (haystack[key] == needle) {   
          return true;
        }
      }
  }
  return false;
}

function actionOnSelect(strMsg){
  var tabCheckbox = document.action_form.action;
  var hasOneChecked = false
  if(typeof(tabCheckbox.length)!="undefined"){
    for (var i=0; i<tabCheckbox.length; i++){
      if(tabCheckbox[i].checked){
        hasOneChecked = true;
        break;
      }
    }
  }else{//only one checkbox in html page
    if(tabCheckbox.checked)
      hasOneChecked = true;
  }
    
  if(hasOneChecked){
    if(document.getElementById("action_detail").style.display=="")
      document.getElementById("action_detail").style.display="none";
    else
      document.getElementById("action_detail").style.display=""
  }else{
    parent.Ext.Msg.alert("Actions sur la sélection", strMsg);
  }
}

/**
 * renomme un mapfile
 * @param srv : url du serveur carto admin
 * @param mapfile nom du mapfile existant
 * @param pk_stockage_carte identifiant dans stockage_carte
 * @return
 */
function map_change_name(srv, mapfile, pk_stockage_carte, urlBack){
  var iframe = this;
  iframe.onSuccess = mapfileRename;
  iframe.tabParams = new Array(srv, mapfile.substr(0, mapfile.length-4), pk_stockage_carte, urlBack);
  parent.Ext.Msg.prompt('Changement de nom', 'Vous souhaitez modifier le nom du mapfile de la carte. Les contextes utilisant cette carte sont susceptibles de ne plus fonctionner.<br>'+
                     'Le mapfile porte le nom: "'+mapfile.substr(0, mapfile.length-4)+'".<br>Entrez le nom du nouveau mapfile:', parent.prompt, iframe);
}

function mapfileRename(srv, mapfile, pk_stockage_carte, urlBack, newName){
  
  var modele = /^[a-z]+[a-z0-9_]*$/i;
  if (modele.test(newName)){
  }
  else{
    parent.Ext.Msg.alert("Changement de nom", "Le nom de fichier saisi ne doit pas contenir de caractères\n" +
          "accentués ou non alphanumériques comme -()\"'. etc.");
    return;
  }
  if (mapfile != ""){
	var ajaxUrl = window.location.protocol+"//"+srv+"/PRRA/Administration/Administration/Maps/MapChangeName.php";
	var iframe = this;
	iframe.tabParams = new Array(pk_stockage_carte, newName);
	iframe.onSuccess = successChangeName;
	iframe.onFailure = function(){parent.Ext.Msg.alert("Cartes", "Echec de l'opération")};
	var queryParams = {
	  MAPFILE : mapfile,
	  pk_stockage_carte : pk_stockage_carte,
	  NEW_MAPFILE: newName 
    }
	parent.AjaxRequest(ajaxUrl, queryParams, iframe);
	/*  
    l_url     = window.location.protocol+"//"+srv+"/PRRA/Administration/Administration/Maps/MapChangeName.php";
    l_url    += "?MAPFILE="+mapfile;
    l_url    += "&pk_stockage_carte="+pk_stockage_carte;
    l_url    += "&NEW_MAPFILE="+newName;
    l_url    += "&url_retour="+urlBack;
    parent.IHM_Chargement_start();
    window.location = l_url;*/
  }
}

function successChangeName(pk_stockage_carte, newName, responseText){
	eval("var responseJson = "+responseText);
	if(responseJson.success==true){
	  if(responseJson.mode==3){
		parent.Ext.alert("le nom du mapfile souhaité est déjà utilisé. Aucune opération réalisée.");  
	  }else{
		var iframe = this;
		iframe.onSuccess = function(){parent.Ext.Msg.alert("Cartes", "L'opération a été réalisée avec succès.")};
		iframe.onFailure = function(){parent.Ext.Msg.alert("Cartes", "Echec de l'opération")};
		var queryParams = {
		  pk_stockage_carte : pk_stockage_carte,
		  NEW_MAPFILE: newName,
		  action : "rename"
	    }
		parent.AjaxRequest(parent.IHM_getBaseUrl()+"administration_carto_Action.php", queryParams, iframe);
	 }
  }
}


/**
 * @brief Ouvre la fenêtre de création/définition d'une vue
 * @param metadata_id : identifiant de la métadonnée de vue
 * @param desc_vue : titre de la la vue
 * @param nom_vue : nom de la vue, sera utilisé cmme nom de la vue dans la base 
 * @param urlBack : url de retour en sortie du module d'administration de vue (généralement l'url de la page courante)
 * @param color_theme_id : identifiant du theme extjs de l'interface, passé au module d'administration de vue pour qu'il soit chargé dans le même thème extjs
 * @param svrAdmin : adresse du serveur d'administartion ou se trouve le module d'administarion de vue
 * @return nothing
 */
function administration_carto_adminView(metadata_id, desc_vue, nom_vue, urlBack, color_theme_id, svrAdmin, login, pwd, service)
{
  var idx = service;
  var login = login;
  var passwd = pwd;
  
  var params = {
	  idx : idx,
	  login : login,
	  passwd : passwd,
	  pk_view : metadata_id,
	  view_title : desc_vue,
	  view_name : nom_vue,
	  url_back : urlBack,
	  view_type : -4,
	  theme_id_for_ext : color_theme_id	
  }
  
  var jsonParams = parent.Ext.encode(params);
  var token = TextEncode(jsonParams);
  
  var url = window.location.protocol+"//" + svrAdmin + "/HTML_JOIN/gui/viewForm.php?token=" + token;

  window.location.href = url;
}



/**
 * @brief Ouvre la fenêtre de paramétrage de structure de cuche
 * @param metadata_id : identifiant de la métadonnée
 * @param nom_couche : nom de la couche à créer 
 * @param urlBack : url de retour en sortie du module d'administration de vue (généralement l'url de la page courante)
 * @param color_theme_id : identifiant du theme extjs de l'interface, passé au module d'administration de vue pour qu'il soit chargé dans le même thème extjs
 * @param svrAdmin : adresse du serveur d'administartion ou se trouve le module d'administarion de vue
 * @return nothing, redirect
 */
function administration_carto_creerStructure(metadata_id, nom_couche, urlBack, color_theme_id, svrAdmin)
{
  var params = {
    pk_data : metadata_id,
    data_name : nom_couche,
    url_back : urlBack,
    theme_id_for_ext : color_theme_id 
  }
  
  var jsonParams = parent.Ext.encode(params);
  var token = TextEncode(jsonParams);
  
  var url = window.location.protocol+"//" + svrAdmin + "/HTML_STRUCTURE/gui/viewForm.php?token=" + token;

  window.location.href = url;
}

/**
 * fonction appelée au chargement de la page d'admin des tables, permet de connaitre les vues utilisant les tables
 * @param srvAdmin
 * @param login
 * @param passwd
 * @param idx
 * @return
 */
function checkTables(srvAdmin, login, passwd, idx){
  if(document.getElementById("layerTables")){
	  var tabLayers = eval(document.getElementById("layerTables").value);
	  var iframe = this;
	  iframe.tabParams = new Array("");
	  var queryParams = {
		  idx : idx,
		  login : login,
		  serviceName: 'getViewsFromTablenames', 
		  passwd : passwd,
		  parameters : parent.Ext.encode({
              tablenames : tabLayers
          }) 
	  }
	  
	  var ajaxUrl = window.location.protocol+"//" + srvAdmin + "/HTML_JOIN/gui/viewServices.php";
	  iframe.onSuccess = showTablesResult;
	  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
	}
}
/**
 * gere la visiblité et le contenu des boutons supprimer et liste des vues
 * @param vide
 * @param responseText
 * @return
 */
function showTablesResult(vide, responseText){
  eval("var responseJson = "+responseText);
  var tabLayers = responseJson.data;
  for(var layerName in tabLayers){
	  if(document.getElementById("suppr_"+layerName)){
		if(tabLayers[layerName].length==0){
			document.getElementById("suppr_"+layerName).style.display="";
		}else{
		  if(document.getElementById("vues_"+layerName)){
			strViewsMsg = "Liste&nbsp;des&nbsp;vues&nbsp;contenant&nbsp;la&nbsp;table:<br>";
		    for(var i=0;i<tabLayers[layerName].length; i++){
		      strViewsMsg+= "<b>"+tabViews[tabLayers[layerName][i]]+"</b><br>";
		    }
		    document.getElementById("vues_"+layerName).style.display="";
			document.getElementById("vues_"+layerName).setAttribute('onclick',"parent.Ext.Msg.alert('Administration de tables','"+strViewsMsg+"')"); 
		  }
		}
	}
  }
}

/**
 * fonction appelée au chargement de la page d'admin des tables, permet de connaitre les vues utilisant les couches
 * @param srvAdmin
 * @param login
 * @param passwd
 * @param idx
 * @return
 */
function checkLayers(srvAdmin, login, passwd, idx){
  if(document.getElementById("layers")){
	  var tabLayers = eval(document.getElementById("layers").value);
	  var iframe = this;
	  iframe.tabParams = new Array("");
	  var queryParams = {
		  idx : idx,
		  login : login,
		  serviceName: 'getViewsFromLayernames', 
		  passwd : passwd,
		  parameters : parent.Ext.encode({
              layernames : tabLayers
          }) 
	  }
	  
	  var ajaxUrl = window.location.protocol+"//" + srvAdmin + "/HTML_JOIN/gui/viewServices.php";
	  iframe.onSuccess = showLayersResult;
	  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
	}
}
/**
 * gere la visiblité et le contenu des boutons supprimer et liste des vues
 * @param vide
 * @param responseText
 * @return
 */
function showLayersResult(vide, responseText){
  eval("var responseJson = "+responseText);
  var tabLayers = responseJson.data;
  for(var layerName in tabLayers){
    if(document.getElementById("suppr_"+layerName)){
		if(tabLayers[layerName].length==0){
			document.getElementById("suppr_"+layerName).style.display="";
		}else{
			 if(document.getElementById("vues_"+layerName)){
				strViewsMsg = "Liste&nbsp;des&nbsp;vues&nbsp;contenant&nbsp;la&nbsp;table:<br>";
				for(var i=0;i<tabLayers[layerName].length; i++){
				  strViewsMsg+= "<b>"+tabViews[tabLayers[layerName][i]]+"</b><br>";
				}
				document.getElementById("vues_"+layerName).style.display="";
				document.getElementById("vues_"+layerName).setAttribute('onclick',"parent.Ext.Msg.alert('Administration de tables','"+strViewsMsg+"')"); 
			  }
			  //rEs(document.getElementById("suppr_"+layerName));
		}
	}
  }
}

/**
 * remove Dom Element
 * @param tO element to delete
 * @return
 */
function rEs(tO) { var i, n; if (!tO) { return false; }
	if (!tO.nodeName) { if (tO.length) for (n=tO.length; n--;) rEs(tO[n]); }
	else tO.parentNode.removeChild(tO);
}

function chooseModele(displayImport){
 document.getElementById("parcourir_import").style.display=(displayImport ? "" : "none");
}

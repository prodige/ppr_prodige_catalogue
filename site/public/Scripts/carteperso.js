/**
 * @brief Initialisation de la page
 * @return
 */
function	carteperso_init(urlInit, userId)
{
	try
	{
		Ext.Ajax.request({
		    url : urlInit , 
		    //params : {"userId":userId},
		    method : 'GET',
		    success: function(result){
		      //TODO hismail
		      //parent.IHM_Chargement_stop();
		      //parent.IHM_historique_add(window.location);
		    },
		    failure: function(result){
		      Ext.MessageBox.alert('Echec', 'Echec de l\'initialisation'); 
		    } 
		  });
		
	}
	catch(e)
	{}
}
/**
 * @brief Fonction qui permet de génére le nom du .map à partir du tire de la carte
 * @param mapTitle
 */
function	cartepersoSetMap(mapTitle)
{
  var text = mapTitle.toString();

  text = text.toLowerCase();
  text = text.replace(/[àâä]/ig, "a");
  text = text.replace(/[éèêë]/ig, "e");
  text = text.replace(/[îï]/ig, "i");
  text = text.replace(/[ôö]/ig, "o");
  text = text.replace(/[ù]/ig, "u");
  text = text.replace(/[ç]/ig, "c");
  text = text.replace(/\s/g, "_");
  text = text.replace(/[^a-zA-Z0-9_]/g, "_");
  return  text;
}

function carteperso_ajouter(){
	
  var		serveur, format, fichier;
  var		valid = true;
  var		msg = ""; 
  var		errormsg;
  serveur = document.getElementById("SERVEUR");
  document.formCartePerso.MAP_TITLE.value = document.formCartePerso.MAP_TITLE.value.replace("'", "");
  document.formCartePerso.MAP.value = cartepersoSetMap(document.formCartePerso.MAP_TITLE.value);
  fichier = document.getElementById("MAP");
  var modele = /^[a-z]+[a-z0-9_]*$/i;
  if (modele.test(fichier.value)){
  }
  else{
    /*parent.*/Ext.Msg.alert("Ajout de carte", "Le nom de fichier saisi ne doit pas contenir de caractères\n" +
    				  "accentués ou non alphanumériques comme -()\"'. etc.");
    return;
  }
  if(in_array(fichier.value, tabData[serveur.value])){
    /*parent.*/Ext.Msg.alert("Ajout de carte", "Une carte portant ce nom existe déjà.");
    return;
  } 
  if (serveur && fichier)
  {
    if ((serveur.value == "") || (fichier.value == ""))
	{
	  valid = false;
	  /*parent.*/Ext.Msg.alert("Ajout de carte", "Tous les champs sont obligatoires");
      return;
	}
  }
  if (valid)
  {
	parent.administration_carto_win_cartodynamique = window.open("about:blank", "Cartographie_dynamique");
	document.forms[0].submit();
	window.location.reload();
  }	
}

/**
 * @brief ouvre la popup de choix des fichiers
 * @return
 */
function carteperso_ChooseFile(element_id){
  var 	l_url;
  var	serveur;
  var	format;

  serveur = document.getElementById("SERVEUR");
  l_url = parent.IHM_getBaseUrl();
  l_url += "Administration/Administration/Layers/ChooseFile/TSPopup.php";
  l_url += "?Contenu=TSParcourirMap.php";
  l_url += "&Valider=Enregistrer";
  l_url += "&Serveur=" + serveur.value;
  parent.loadExtPopup(l_url, "MAP_LIST", "liste des cartes existantes", 600, 400, true);	
}

/**
 * true if needle is in array (php equivalent)
 * @param needle
 * @param haystack
 * @param argStrict
 * @return
 */
function in_array (needle, haystack, argStrict) {
   
  var key = '', strict = !!argStrict; 
  if (strict) {
      for (key in haystack) {
        if (haystack[key] === needle) {
          return true;            
        }
      }
  } else {
      for (key in haystack) {
        if (haystack[key] == needle) {   
          return true;
        }
      }
  }
  return false;
}

/**
 * @brief Ouvre l'interface cartographique
 * @param url url de la carte
 * @param mapName nom de la carte
 * @return
 */
function	consultation_openInterface(url)
{
  parent.consultation_win_cartodynamique = window.open(url, "Cartographie_dynamique", "directories=0, fullscreen=yes, menubar=0, status=1, toolbar=yes, resizable=1");
}

/**
 * @brief Ouvre l'interface cartographique
 * @param url
 * @return
 */
function	carteperso_openInterface(action, login, pwd)
{
  if(document.formCartePerso){
	document.formCartePerso.login.value = login;
	document.formCartePerso.pass.value = pwd;
    document.formCartePerso.action = action;
    parent.administration_carto_win_cartodynamique = window.open("blank.html", "Cartographie_dynamique", "fullscreen=yes directories=0, menubar=0, status=1, toolbar=0, resizable=1");
    document.formCartePerso.submit();
  }
}

/**
 * @brief Supprime la carte
 * @param pk_carte
 * @param pk_stockage_carte
 * @return
 */
function carteperso_delMap(pk_carte, pk_stockage_carte) {
  /*var iframe = {};
  iframe.tabParams = new Array(pk_carte, pk_stockage_carte, true);
  iframe.onSuccess = confirm_carteperso_delMap;*/
  Ext.Msg.confirm("Suppression de carte", "La carte sera supprimée, les contextes liés à cette carte sont susceptibles de ne plus fonctionner.<br>" +
                  "Souhaitez-vous continuer  ?",
                  function(btn) {
                    if(btn == "yes") {
                      var ajaxUrl = Routing.generate('catalogue_geosource_deleteMetaData');
                      var queryParams = {action : "del", stockageCarteId : pk_stockage_carte, carteId : pk_carte, deldata : true};
                      Ext.Ajax.request({
                        url : ajaxUrl ,
                        params : queryParams,
                        method : 'GET',
                        success : function(response) {
                          var oRes = eval("("+response.responseText+")");
                          if(oRes.success) {
                            var oDivCarte = document.getElementById("carteprojet_" + pk_carte);
                            if(oDivCarte) {
                              oDivCarte.parentNode.removeChild(oDivCarte);
                            }
                          } else {
                            if(oRes.msg && oRes.msg != "") {
                              Ext.Msg.alert("Suppression de carte", oRes.msg);
                            } else {
                              Ext.Msg.alert("Suppression de carte", "Une erreur est survenue lors de la tentative de suppression de la carte personnelles");
                            }
                          }
                        },
                        failure : function(responseText) {
                          console.log("failedAjax")
                          console.log(responseText);
                        }
                      });
                    }
                  }/*parent.confirm, iframe*/);
}

/**
 * Not use in Prodige4.0
 * @param pk_carte
 * @param pk_stockage_carte
 * @param deldata
 * @return
 */
function confirm_carteperso_delMap(pk_carte, pk_stockage_carte, deldata){
  var iframe = {};
  var ajaxUrl = parent.IHM_getBaseUrl()+"administration_carto_Action.php";
  iframe.tabParams = [pk_carte];
  iframe.onSuccess = function(pk_carte, responseText){
    var oRes = eval("("+responseText+")");
    if ( oRes.success ) {
      var oDivCarte = document.getElementById("carteprojet_"+pk_carte);
      if ( oDivCarte ) {
        oDivCarte.parentNode.removeChild(oDivCarte);
      }
    } else {
      if ( oRes.msg && oRes.msg != "" ) {
        parent.Ext.Msg.alert("Suppression de carte", oRes.msg);
      } else {
        parent.Ext.Msg.alert("Suppression de carte", "Une erreur est survenue lors de la tentative de suppression de la carte personnelles");
      }
    }
  }
  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
  /**
   * @deprecated v3.2 - 16/05/2012
   */
  /*
  var   params;
  var   i;
  var   param;
  var   curdom = null;
  var   cursdom = null;
  var   url;

  url = parent.IHM_getBaseUrl();
  url += "administration_carto_Action.php?action=del&stockageCarteId=" + pk_stockage_carte + "&carteId=" + pk_carte;
  if (deldata)
	url += "&deldata=1";
  window.location = url;*/
}

/**
 * Upload le fichier de contexte vers le service de consultation de cartes dédié 
 * @return
 */
function carteperso_loadcontext(){
  var ext = document.formContext.uploadedfile.value;
  ext = ext.substring(ext.length-4,ext.length);
  ext = ext.toLowerCase();
  if(ext != 'json') {
	Ext.Msg.alert('Seuls les fichiers d\'extension json sont autorisés');
	return false; 
  }else{
	parent.consultation_win_cartodynamique = window.open("blank.html", "Cartographie_dynamique", "directories=0, fullscreen=yes, menubar=0, status=1, toolbar=yes, resizable=1");
	document.formContext.submit();
  }
	
}

/**
 * proposition de carte personnelle
 * @return
 */
function carteperso_proposer(pk_carteprojet, map_name, bAddEmail, cartp_utilisateur_email){
  var storeList = new Ext.data.JsonStore({
    	  //url: "/PRRA/Services/getAdminSdom.php",
    	  url: Routing.generate('catalogue_carteperso_getAdminsdom'),
        fields: ['pk_utilisateur','displayField','pk_sous_domaine','ssdom_nom'],
    	  autoLoad: true
    	});   
     var adminCombo = new Ext.ux.form.LovCombo({
    	 id				: 'listAdminSdom',
         name			: 'listAdminSdom',
         fieldLabel		: 'Administrateur(s) sollicité(s) pour la publication',
         typeAhead		: false,
         editable		: false,
         width			: 300,
         height			: 200,
         autoLoad		: true,
         triggerAction	: 'all',
         mode			: 'local',
         store			: storeList,
         displayField	: 'displayField',
         valueField		: 'pk_utilisateur',
         hiddenName		: 'pk_administrateur',
         hideOnSelect	: false
      });
	
	var root = new Ext.tree.AsyncTreeNode( {
	      text :'Root', // texte du noeud
	      draggable :false, // Désactiver le Drag and drop sur ce noeud
	      leaf :false,
	      id :'idRoot', // identifiant du noeud
	      loader : new Ext.tree.TreeLoader({
	        //url : "/PRRA/include/domaines.php",
	        url: Routing.generate('catalogue_geosource_getDomaines'),
	        baseParams : {load : 1},
	        requestMethod : "GET"
	      })
	 });
	var TreePanel = new Ext.tree.TreePanel(
	     {
		   root : root,
		   rootVisible :false,// La racine est visible
		   border : true,
		   autoScroll: true,
	       fieldLabel: 'Domaine/sous-domaine préconisé',
		   currentNode : null,
		   height:200
	     }	   
    );  		   
    var isSelectedSdom = false; 
    TreePanel.on('click', function(node, event) {
	      TreePanel.currentNode = node;
	      var nodeDef = node.attributes;

	      var domaine = (nodeDef.isDomain ? nodeDef.text : nodeDef.domain);
	      var sousdomaine = (nodeDef.isSubDomain ? nodeDef.text : "");
	      if(sousdomaine != ""){
	    	var sousdomaine_id = (nodeDef.id).substring(5);
	      }else{
	    	var sousdomaine_id = -1;
	      }
	      if (typeof(sousdomaine)!="undefined" &&  sousdomaine!="" ){
	    	document.getElementById("mapSdom").value = sousdomaine;
	    	document.getElementById("mapDom").value = nodeDef.domain;
	    	document.getElementById("pk_sdom").value = nodeDef.id.substr(5, nodeDef.id.length);
	    	isSelectedSdom =true;
	      }
	      else if(typeof(domaine)!="undefined" && domaine!=""){
	    	document.getElementById("mapDom").value = domaine;
	      }else{
	    	document.getElementById("mapDom").value = "";
	    	document.getElementById("mapSdom").value = "";
	      }
	      if(sousdomaine_id != -1){
	    	//Si un sous domaine a ete selectionne, on reordonne la liste des administrateurs
	    	Ext.Ajax.request({
	    	  //url: "/PRRA/Services/getAdminSdom.php?ssdom="+sousdomaine_id,
	    	  url: Routing.generate('catalogue_carteperso_getAdminsdom') + "?ssdom=" + sousdomaine_id,
	    	  success: function(result){
	    	    var jsonData = Ext.util.JSON.decode(result.responseText);
	    	    var updatedCombo = Ext.getCmp('listAdminSdom');
	    	    updatedCombo.store.loadData(jsonData);
	    	  }
	    	});
	      }
	    });
	var tabItems = [
	           		{
	                    fieldLabel: 'Nom de la carte',
	                    name: 'mapName',
	                    width:200,
	                    value : map_name,
	                    allowBlank:false
	                  },{
	                    xtype: 'textarea',
	                    width:200,
	                    fieldLabel: 'Informations complémentaires',
	                    name: 'mapComment'
	                  },
	                  {
	                      name: 'mapDom',
	                      xtype: "hidden",
	                      id : "mapDom",
	                      value : ""
	                  }, {
	                      name: 'mapSdom',
	                      xtype: "hidden",
	                      id : "mapSdom",
	                      value : ""
	                  }, {
	                      name: 'pk_sdom',
	                      xtype: "hidden",
	                      id : "pk_sdom",
	                      value : ""
	                  },TreePanel,adminCombo
	                  ];
	if(bAddEmail)
      tabItems.push({
	                    fieldLabel: 'Email',
	                    name: 'email',
	                    width:200,
	                    vtype:'email',
	                    value : cartp_utilisateur_email,
	                    allowBlank:false
	                  });
    var form = new Ext.form.FormPanel({
        labelWidth: 200,
        name : "formPropose",
        id : "formPropose",
        bodyStyle: 'padding:5px;',
        //url: 'carteperso.php?mode=Propose&pk_carteprojet='+pk_carteprojet,
        url: Routing.generate('catalogue_carteperso') + '?mode=Propose&pk_carteprojet='+pk_carteprojet,
        defaults: {
            xtype: 'textfield'
        },
        items:tabItems
    });

  var win = new Ext.Window({
		id : "windowPropose",
		width: "auto" ,
		height: "auto" ,
		constrain : true,
		title : "Intégration de la carte au catalogue",
		resizable : true,
		maximizable : true,
		items : form,
		buttons: [{
            text:'Valider',
            handler: function(){
			  if(form.getForm().isValid()){
				if(isSelectedSdom){
					if(document.getElementById("pk_administrateur").value == ""){
						Ext.Msg.alert("Proposition de carte", "Merci de solliciter au moins un administrateur");
					}else{
					form.getForm().submit({
						clientValidation: true,
						success: function(){
						  Ext.Msg.alert("Proposition de carte", "Votre demande a été transmise aux administrateurs de la plateforme");
						  win.close();
						  window.location.reload();	
					    },
						failure: function (){Ext.Msg.alert("Echec de la demande");},
						waitMsg:'Traitement de la demande...'
					 });
					}
				}else{
				  Ext.Msg.alert("Proposition de carte", "Merci de sélectionner un sous-domaine");	
				}
			  }
			}
        },{
            text: 'Annuler',
            handler: function(){
                win.hide();
            }
        }]

	  });
  win.show();	
}
/**
 * Refus d'intégration de la carte
 * @param pk_carteprojet identifiant de la carte
 * @return
 */	  
function carteperso_Refuser(pk_carteprojet){
	
	 var form = new Ext.form.FormPanel({
	        labelWidth: 200,
	        name : "formRefus",
	        id : "formRefus",
	        bodyStyle: 'padding:5px;',
	        url: Routing.generate('catalogue_carteperso') + '?mode=Refus&pk_carteprojet='+pk_carteprojet,
	        defaults: {
	            xtype: 'textfield'
	        },
	        items:[
	       		  {
	                 xtype: 'textarea',
	                 width:200,
	                 fieldLabel: 'Raisons du refus',
	                 name: 'mapComment',
	                 allowBlank:false
	               }
	              ]
	    }); 
	
	var win = new Ext.Window({
		id : "windowRefus",
		width: "auto" ,
		height: "auto" ,
	  constrain : true,
		title : "Refus d'intégration de la carte au catalogue",
		resizable : true,
		maximizable : true,
		items : form,
		buttons: [{
            text:'Valider',
            handler: function(){
			  if(form.getForm().isValid()){
				 form.getForm().submit(
				   {
					success: function() {
				          Ext.Msg.alert("Proposition de carte", "Le refus a été notifié à l'utilisateur");
				          win.close();
				          window.location.reload();
				        },
					failure:function(){ Ext.Msg.alert("Echec de la procédure");},
					waitMsg:'Traitement en cours...'
				   }
				 )
			  }
			}
        },{
            text: 'Annuler',
            handler: function(){
                win.hide();
            }
        }]

	  });
  win.show();	
}

/**
 * @return
 */	  
function carteperso_Accepter(pk_carteprojet, rubric_id, dom_id, sdom_id,fk_fiche_meta){
  var box = Ext.MessageBox.wait('Traitement en cours...', 'Intégration de la carte');
  //parent.setCurrentSousDomaine("rubric_"+rubric_id, "dom_"+dom_id,"sdom_"+sdom_id);
  Ext.Ajax.request({
  url: Routing.generate('catalogue_carteperso'),  
	params : {"mode":"Accept", "pk_carteprojet" : pk_carteprojet,"fiche_meta":fk_fiche_meta },
    method : 'GET',
    waitMsg:'Transmission...',
    success: function(result){
	  box.hide();
	  var jsonData = Ext.util.JSON.decode(result.responseText);
	  if(jsonData.success == "true"){		  
		  window.location.reload();
	  }
	  else
		  Ext.MessageBox.alert('Echec', "Echec de la procédure"); 
    },
	failure: function(result){
	  Ext.MessageBox.alert('Echec', "Echec de la procédure"); 
	} 
  });
	
}


/**
 * @return
 */	  
function carteperso_EditFicheMetadata(pk_carteprojet,fk_fiche_meta){
  var box = Ext.MessageBox.wait('Traitement en cours...', 'Intégration de la carte');
  Ext.Ajax.request({
	url: 'carteperso.php',
	params : {"mode":"EditMetadata", "pk_carteprojet" : pk_carteprojet,"fiche_meta":fk_fiche_meta},
    method : 'GET',
    waitMsg:'Transmission...',
    success: function(result){
	  box.hide();
	  var jsonData = Ext.util.JSON.decode(result.responseText);
	  if(jsonData.success == "true"){
		  connectGeonetwork(jsonData.connectUrl, jsonData.redirectUrl,jsonData.param);
	  }
	  else
		  Ext.MessageBox.alert('Echec', "Echec de la procédure"); 
    },
	failure: function(result){
	  Ext.MessageBox.alert('Echec', "Echec de la procédure"); 
	} 
  });
	
}


function editMetadata(urlEdit){
  window.open(urlEdit);
  window.location.reload();
  
}
/**
 * @return
 */	
function carteperso_EditFicheMetadataAdmin(pk_carteprojet,fk_fiche_meta){
  var box = Ext.MessageBox.wait('Traitement en cours...', 'Intégration de la carte');
  Ext.Ajax.request({
	url: 'carteperso.php',
	params : {"mode":"EditMetadata", "pk_carteprojet" : pk_carteprojet,"fiche_meta":fk_fiche_meta,"admin":1},
    method : 'GET',
    waitMsg:'Transmission...',
    success: function(result){
	  box.hide();
	  var jsonData = Ext.util.JSON.decode(result.responseText);
	  if(jsonData.success == "true"){
		  connectGeonetwork(jsonData.connectUrl, jsonData.redirectUrl,jsonData.param);
	  }
	  else
		  Ext.MessageBox.alert('Echec', "Echec de la procédure"); 
    },
	failure: function(result){
	  Ext.MessageBox.alert('Echec', "Echec de la procédure"); 
	} 
  });
	
}

Ext.onReady( function() {
  var tabItem = new Array();
  tabItem.push({contentEl:'cartelist', title:'Mes cartes'});
  document.getElementById('cartelist').style.display = '';
  tabItem.push({contentEl:'cartecreation', title:'Nouvelle carte'});
  document.getElementById('cartecreation').style.display = '';
  if(document.getElementById('contexte')){
    tabItem.push({contentEl:'contexte', title:'Charger un contexte'});
    document.getElementById('contexte').style.display = '';
  }
  if(document.getElementById('demandelist')){
    tabItem.push({contentEl:'demandelist', title:'Demandes d\'intégration'});
    document.getElementById('demandelist').style.display = '';
  }
  if ( tabItem.length > 0 ) { 
    var tabs = new Ext.TabPanel({
      renderTo:'divglobal',
      style:{
        width:'100%'
      },
      border:false,
      deferredRender:false,
      activeTab:0,
      items:tabItem
    });
  }
	
});

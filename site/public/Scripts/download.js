// URL_SVR_FRONT : url of consultation server 
// URL_SVR_CATALOGUE : url of catalogue server 
// and IDX : service_idx
// must be define before calling this function

function getCartographicArea(refMap, fieldId, sizeLimit, rectOnly) {
	// define callback params called when area is validated in the consultation frame
	var mask;
  mask = new Ext.LoadMask(Ext.getBody(), {msg:'Génération de la carte - Ajout des emprises des couches...'});
  mask.show();
  if (winModel)
  Ext.Element.data(mask.el.dom, "mask").setStyle('z-index', parseInt(winModel.el.getStyle('z-index')||0)+1);
	Ext.Ajax.request({
		method : 'POST',
		url : Routing.generate('catalogue_geosource_downloadlayers_getmapfile', {modele:refMap}),
		jsonData : {metadata : layers},
		success : function(response){
      mask.hide();
			response = Ext.decode(response.responseText);
      if (winModel!=null){
        winModel.hide();
      }
			if ( response.success ){
			  refMap = response.mapfile;
			  openMap(refMap, fieldId, sizeLimit, rectOnly);
			}
		}
	})
}
function openMap(refMap, fieldId, sizeLimit, rectOnly){
  var callerParams = 'AreaExtractorHandlerURL="' + Routing.generate('catalogue_geosource_bridge', {}, true) + ';AreaExtractorHandlerCallback="handleResponseCartographicArea";';
  //var callerParams = 'AreaExtractorHandlerURL="'+window.location.protocol+'//' + URL_SVR_CATALOGUE  + '/PRRA/bridge.php";AreaExtractorHandlerCallback="handleResponseCartographicArea";';

  if (sizeLimit || rectOnly) {
		var optionsStr = "";
		optionsStr = optionsStr + (sizeLimit ? "AreaExtractorSizeLimit=\"" + Math.floor(sizeLimit)  + "\";": "");
		optionsStr = optionsStr + (rectOnly ? "AreaExtractorRectOnly=true;" : "AreaExtractorRectOnly=false;");
		callerParams = callerParams +  optionsStr;
	}

	var url = URL_SVR_FRONT + "/" + IDX + "/" + refMap + "?callerParams=" + escape(callerParams);
	
	var autosize = false;
	var title = "Définition de la zone d\'extraction cartographique";
	var minHeight = parent.fenetre ? Math.min(parent.fenetre.document.getElementsByTagName('html')[0].offsetHeight, parent.window.document.body.clientHeight, 650) :
						Math.min(parent.window.document.body.clientHeight, 650);
	var win = new parent.Ext.ux.ManagedIFrame.Window({
      title         : title,
      id : "consultation_cartographicArea",
	  layout : "fit",
	  width: autosize ? "auto" : 800,
	  height : minHeight,
	  constrain:true,
//	  height: autosize ? "auto" : 900,
      closable      : true,
      shadow        : false,
      animCollapse  : false,
      autoScroll    : true,
      manager       : parent.Ext.WindowMgr,
      hideMode      : 'nosize',
      defaultSrc    : url,
      maximize      : true,
      modal : true,
      //loadMask      :  {msg:'Chargement...'},
      closeAction   : 'destroy'
    });
	
	// define response handler at the top level
	parent.handleResponseCartographicArea = function(geomStr) {
		var winEl = parent.Ext.getCmp("consultation_cartographicArea");
		if (winEl!=null) {
			winEl.close();
		}
		var fieldEl = Ext.fly(fieldId);	
		if (fieldEl!=null) {
			fieldEl.dom.value = geomStr;
			fieldEl.dom.onchange();
		}
	};	

	// show consultation window
	win.show();
};

function fieldUpdate(fieldId, value) {
  var fieldEl = Ext.fly(fieldId);
  if (fieldEl!=null) {
	fieldEl.dom.value = value;
  }
};

var originalContent = {};
function launchViewManager(layer_id) {

  window.launchViewProperties = window.launchViewProperties || {};

  var properties = window.launchViewProperties[layer_id];
  if (!properties)
    return;

  var layername = properties.layername;

  var params = Ext.apply({
    // define callback params called when view is completely defined in the
    // consultation frame
    url_back : encodeURIComponent(window.location.href),
    callbacks : {
    	close : "parent.closeCallback",
    	createView : "parent.createViewCallback"
    },
    viewManagerHandlerCallback : "parent.handleResponseViewManager"
  }, properties);


  if ( !properties.pk_view ){
  	window.callLaunchViewManager = window.callLaunchViewManager || {};
  	window.callLaunchViewManager[layer_id] = window.callLaunchViewManager[layer_id] || 1;
    Ext.Ajax.request({
    	url : Routing.generate("catalogue_join_temporary"),
    	method : 'POST',
    	jsonData : params,
      success : function(response){
      	response = Ext.decode(response.responseText);
        window.launchViewProperties[layer_id] = Ext.apply(window.launchViewProperties[layer_id], response.properties);
      	window.launchViewProperties[layer_id] = Ext.apply(window.launchViewProperties[layer_id], response.queryparams);
      	window.callLaunchViewManager[layer_id]++;
        if ( window.callLaunchViewManager[layer_id]<10 ) launchViewManager(layer_id);
      },
      failure : function(response){
      }
    });
    return;
  }
  var url = Routing.generate("catalogue_join") + "/" + parent.TextEncode(Ext.encode(params));

  var win = new parent.Ext.ux.ManagedIFrame.Window({
    title : "Paramétrage des jointures pour " + layername,
    id : "admin_viewManager",
    layout : "fit",
    width : 800,
    height : 660,
    constrain : true,
    closable : true,
    shadow : false,
    animCollapse : false,
    autoScroll : true,
    manager : parent.Ext.WindowMgr,
    hideMode : 'nosize',
    defaultSrc : url,
    loadMask : {
      msg : 'Chargement...'
    },
    closeAction : 'destroy'
  });

  win.show();

  var idBtn = "Download_Join_" + layer_id;

  var updateDisplayRow = function() {
    idRow = "vector_" + layer_id + "_row";
    var rowDomEl = document.getElementById('td_view_'+layer_id);
    if (rowDomEl){
      originalContent[rowDomEl.id] = originalContent[rowDomEl.id] || rowDomEl.innerHTML;
    	rowDomEl.innerHTML = "<input type='hidden' name='viewname_of_" +layer_id+ "' value='" +properties.view_name+ "'/>" +
    			"<u>Vue de jointure</u>";
    }
    
    var rowDomEl = document.getElementById('td_actions_'+layer_id);
    if (rowDomEl){
      originalContent[rowDomEl.id] = originalContent[rowDomEl.id] || rowDomEl.innerHTML;
      
      var newBtnContent = '<div id="Download_Join_'
          + layer_id
          + '"> '
          + '<input id="'
          + idBtn
          + '_update" style="margin:5px;" value="Paramétrer" type="Button"/> <br/>'
          + '<input id="'
          + idBtn
          + '_remove" style="margin:5px;" value="Supprimer" type="Button"/> <br/> '
          + '<input id="viewname_of_' +layer_id + '" value="' + properties.view_name
          + '" type="Hidden"/>' + '</div>';
      rowDomEl.innerHTML = newBtnContent;
    }
    

    var btnUpdateDomEl = document.getElementById(idBtn + '_update');
    var btnRemoveDomEl = document.getElementById(idBtn + '_remove');

    
    if (btnUpdateDomEl) {
      btnUpdateDomEl.onclick = function() {
        launchViewManager(layer_id)
      };
    }

    if (btnRemoveDomEl){
      btnRemoveDomEl.onclick = function() {
      	var title = 'Suppression de la vue de jointure';
      	Ext.Msg.confirm(title, 'Confirmez-vous la suppression de la vue de jointure ?', function(btn){
      	  if ( btn != "yes" ) return;
      	  
        Ext.Ajax.request({
          url : Routing.generate('catalogue_join_view_services'),
          method : 'POST',
          jsonData : {
            serviceName : 'removeCompositeView',
            parameters : Ext.encode({
              viewId : properties.pk_view
            })
          },
          success : function(response){
          	response = Ext.decode(response.responseText);
          	if ( response.success ){
            	Ext.Msg.alert(title, response.msg).setIcon(Ext.Msg.INFO);
              Ext.iterate(originalContent, function(elId, content){
                document.getElementById(elId).innerHTML = content;
              });
              delete window.launchViewProperties[layer_id].pk_view;
              delete window.launchViewProperties[layer_id].view_name;
          	} else {
          		Ext.Msg.alert(title, 'La suppression de la vue de jointure a échoué : '+(response.msg||'')).setIcon(Ext.Msg.ERROR);
          	}
          }
        })
      	});
      };
    }
  };

  // define response handler at the top level
  parent.createViewCallback = function(createProperties) {
  	window.launchViewProperties[layer_id];
  };

  // define response handler at the top level
  parent.closeCallback = function() {
    var winEl = Ext.getCmp("admin_viewManager");
    if (winEl != null) {
      winEl.el.removeAllListeners();
      winEl.close();
    }
    var editionMode = (document.getElementById(idBtn + '_update') != null);
    if (!editionMode)
      updateDisplayRow();

  };

};
/**
 * Encode la valeur en base64
 * @param str
 * @return
 */
function encodeValue(str){
  return encode64((str));
}

var keyStr = "ABCDEFGHIJKLMNOP" +
                "QRSTUVWXYZabcdef" +
                "ghijklmnopqrstuv" +
                "wxyz0123456789+/" +
                "=";
/**
 * base64_encode js equivalent 
 * @param input
 * @return
 */  
function encode64(input) {
  input = escape(input);
  var output = "";
  var chr1, chr2, chr3 = "";
  var enc1, enc2, enc3, enc4 = "";
  var i = 0;
  do {
     chr1 = input.charCodeAt(i++);
     chr2 = input.charCodeAt(i++);
     chr3 = input.charCodeAt(i++);
  
     enc1 = chr1 >> 2;
     enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
     enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
     enc4 = chr3 & 63;
  
     if (isNaN(chr2)) {
        enc3 = enc4 = 64;
     } else if (isNaN(chr3)) {
        enc4 = 64;
     }
  
     output = output +
        keyStr.charAt(enc1) +
        keyStr.charAt(enc2) +
        keyStr.charAt(enc3) +
        keyStr.charAt(enc4);
     chr1 = chr2 = chr3 = "";
     enc1 = enc2 = enc3 = enc4 = "";
 } while (i < input.length);
 return output;
}

Ext.onReady( function() {
  var tabItem = new Array();
  var onResizeItem = function(id){
    return;
//  	var node = document.getElementById(id);
//  	node.style.overflow = 'auto';
//  	node.style.height = "100%";
//  	this.setHeight("100%");
  }
  if ( typeof(bShowTabVector) != "undefined" &&  bShowTabVector ) {
    tabItem.push({contentEl:'downloadBlock_vector', title:'Données vecteur', onResize:function(){onResizeItem.call(this, 'downloadBlock_vector');}});
  }
  if ( typeof(bShowTabRaster) != "undefined" && bShowTabRaster ) {
    tabItem.push({contentEl:'downloadBlock_raster', title:'Données raster', onResize:function(){onResizeItem.call(this, 'downloadBlock_raster');}});
  }
  if ( typeof(bShowTabMajic) != "undefined" && bShowTabMajic ) {
    tabItem.push({contentEl:'downloadBlock_majic', title:'Données MAJIC', onResize:function(){onResizeItem.call(this, 'downloadBlock_majic');}});
  }
  if ( typeof(bShowTabTable) != "undefined" && bShowTabTable ) {
    tabItem.push({contentEl:'downloadBlock_table', title:'Données attributaires', onResize:function(){onResizeItem.call(this, 'downloadBlock_table');}});
  }
  if ( typeof(bShowTabModel) != "undefined" && bShowTabModel ) {
    tabItem.push({contentEl:'downloadBlock_model', title:'Données modèles', onResize:function(){onResizeItem.call(this, 'downloadBlock_model');}});
  }
  
  items_accordion = [];
  if ( tabItem.length > 0 ) { 
	  var tabs = new Ext.TabPanel({
	  	resizeTabs : true,
      autoScroll:true,
      height : Ext.getBody().getViewSize().height-50,
		  border:true,
		  activeTab:0,
		  defaults:{
		    width:'auto',
		    layout:'fit'
		  },
		  items:tabItem
	  });
    items_accordion.push({
      autoScroll : true,
      id:'local',
      title:'<u>Données locales</u>',
      style : {
      },
      items:tabs,
      _onResize:function(){
//        if ( Ext.getCmp('local_panel') ) {
//        	try {
//        	  Ext.getCmp('local_panel').setHeight( (parent.document.getElementById('fenetre') ? parent.document.getElementById('fenetre').offsetHeight : window.offsetHeight) - 39 - 25 - 25);
//        	}catch(e){
//        	  Ext.getCmp('local_panel').setHeight( window.offsetHeight- 39 - 25 - 25);
//        	}
//        }
      }
    });
  }
  
  if ( document.getElementById('downloadBlock_distant') ) {
    items_accordion.push({
      autoScroll:true,
      height : Ext.getBody().getViewSize().height-50,
      id:'distant',
      title:'<u>Données distantes</u>',
      autoScroll : true,
      items:[{
        id:'distant_panel',
        xtype:'panel',
        contentEl:'downloadBlock_distant'
      }],
      _onResize:function(){
//        if ( Ext.getCmp('distant_panel') ) {
//          Ext.getCmp('distant_panel').setHeight( (parent.document.getElementById('fenetre') ? parent.document.getElementById('fenetre').offsetHeight : window.offsetHeight) - 39 - 25 - 25);
//          if ( Ext.getCmp('distant_panel').getEl() ) {
//            Ext.getCmp('distant_panel').getEl().dom.children[0].children[0].style.overflow = 'auto';
//          }
//        }
      }
    });
  }
  
  var accordion = new Ext.TabPanel({
      resizeTabs : true,
  	autoScroll : true,
  	tabMargin : 10,
  //	padding : 10,
  	style : {'background-color': '#dfe8f6'},
    renderTo:'downloadBlock_tab',
    activeItem : 0,
    width:'auto',
      //height : Ext.getBody().getViewSize().height - 50,
    closable:false,
    layoutConfig:{animate:true},
    border:false,
    items:items_accordion
  });
  
  var viewport = new Ext.Viewport({
    id:'vp',
    layout :'border',
    border :false,
    forceLayout : true,
    items :[{
      region :"center",
      layout : 'fit',
  		id :"center",
  		margins :"38 0 0 0",
  		activeOnTop : true,
      split : true,
      fill : false,
  		items : [ accordion ],
  		onResize:function(){
  		  for ( var i=0; i<this.items.items.length; i++) {
  		   // this.items.items[i].fireEvent('resize');
  		  }
  		}
    }]
  });
  
  Ext.getCmp('center').fireEvent('resize');
  
});

/**
 * Affichage de la licence de téléchargement
 * @param strExecFunction
 * @param UrlLicence
 * @param verifCtrl
 * @return
 */
function affLicence(strExecFunction, UrlLicence, verifCtrl){
	if ( !eval(verifCtrl) ) return;
	
	var autosize = false;
	var minHeight = parent.fenetre ? Math.min(parent.fenetre.document.getElementsByTagName('html')[0].offsetHeight, parent.window.document.body.clientHeight, 650) :
		Math.min(parent.window.document.body.clientHeight, 650);
	var win = new parent.Ext.ux.ManagedIFrame.Window({
      title         : "Licence de téléchargement",
      id : "licenceWindow",
	  layout : "fit",
	  width: autosize ? "auto" : 550,
	  constrain:true,
//	  height: autosize ? "auto" : minHeight,
      closable      : true,
      shadow        : false,
      animCollapse  : false,
      autoScroll    : true,
      manager       : parent.Ext.WindowMgr,
      hideMode      : 'nosize',
      defaultSrc    : UrlLicence,
      //loadMask      :  {msg:'Chargement...'},
      closeAction   : 'close',
      buttons:[{
	    	  text:'Accepter',
	    	  handler:function() {
	    	    win.close();
	    	    eval(strExecFunction);
	    	  }
          },
    	  {
	    	  text:'Refuser',
	    	  handler:function() {
	  	        win.close();
	          }
    	  }
          ]
    });
	win.show();
}

/**
 * Affichage des CGU
 * @param strExecFunction
 * @param UrlLicence
 * @param verifCtrl
 * @return
 */
function affCGU(strExecFunction, verifCtrl){
    if ( !eval(verifCtrl) ) return;

    var autosize = false;
    var minHeight = parent.fenetre ? Math.min(parent.fenetre.document.getElementsByTagName('html')[0].offsetHeight, parent.window.document.body.clientHeight, 650) :
        Math.min(parent.window.document.body.clientHeight, 650);
    var cguContent = parent.document.getElementById('cgu-content').innerHTML;
    var win = new parent.Ext.ux.ManagedIFrame.Window({
        title         : "Conditions générales d'utilisation",
        id : "cguWindow",
        layout : "fit",
        width: autosize ? "auto" : 750,
        height: autosize ? "auto" : minHeight,
        constrain:true,
//	  height: autosize ? "auto" : minHeight,
        closable      : true,
        shadow        : false,
        animCollapse  : false,
        autoScroll    : true,
        manager       : parent.Ext.WindowMgr,
        hideMode      : 'nosize',
        html          : cguContent,
        //loadMask      :  {msg:'Chargement...'},
        closeAction   : 'close',
        buttons:[{
            text:'Accepter',
            handler:function() {
                win.close();
                eval(strExecFunction);
            }
        },
            {
                text:'Refuser',
                handler:function() {
                    win.close();
                }
            }
        ]
    });
    win.show();
}
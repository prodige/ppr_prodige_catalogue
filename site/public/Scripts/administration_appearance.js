
/**
 * @brief Initialise la taille
 */
function setSizeIFrame() { 
   var iframe = parent.frames['ModuleDetail'].frameElement ;
   var principal = parent.parent.parent.parent.frames['fenetre']; 
   var new_height=principal.innerHeight;
   iframe.style.height = new_height-40+'px';                
 } 



/**
 * @brief Modifie un titre
 */
function appearance_modifyTitle(){
  var f=document.modifyTitle;
  f.target="footerExec";
  f.submit();
}
/**
 * @brief Modifie l'affichage
 * Validate the appearance form, register the parameters in the database
 * and upload the carto css_parameters to the carto server
 *
 */
function appearance_modifyDisplay(){
  var f=document.modifyDisplay;
  f.target="footerExec";
  f.submit();
}
/**
 * @brief Modifie l'affichage
 * Validate the appearance form, register the parameters in the database
 * and upload the carto css_parameters to the carto server
 *
 */
function appearance_modifyRubrics(){
  var f=document.modifyRubrics;
  f.submit();
}
  
/**
 * @brief Open Window
 */
function OpenWindow(strPage, hauteur, largeur, strName) {
  if( !strName ) strName = "_blank";
  var H = (screen.height - hauteur) / 2;
  var L = (screen.width - largeur) / 2;
  window.open(strPage, strName, 
    "status=yes,scrollbars=yes,resizable=yes,height="+hauteur+",width="+largeur+",top="+H+",left="+L);
}

/**
 * @brief Ajoute une rubrique
 */
function appearance_addRubric(){
  var f=document.manage_rubrics;
  f.submit();
}  

/**
 * @brief Met à jour la rubrique ayant pour identifiant rubric_id
 * @param rubric_id
 */
function appearance_updateRubric(rubric_id){
  var f=document.manage_rubrics;
  f.elements["rubric_action"].value="update";
  f.action="RubricsDetail.php?rubric_id="+rubric_id;
  f.submit();
}  

/**
 * @brief Efface la rubrique ayant pour identifiant rubric_id
 * @param rubric_id
 */
function appearance_delRubric(rubric_id){
  var f=document.manage_rubrics;
  f.elements["rubric_action"].value="delete";
  f.action="RubricsDetail.php?rubric_id="+rubric_id;
  f.submit();
}  

/**
 * @brief Fonction qui ouvre la popup permettant de choisir un fichier sur le serveur
 * @param element_id
 */
function appearance_ChooseFile(element_id)
{
  var l_url;
  var l_rep = "/";
  var l_rootdir="";
  if (element_id=='logo' || element_id=='logo_right'){
    l_rootdir = "../../../../images/logos/";
  }
  else if(element_id=='top_image'){
    l_rootdir = "../../../../images/bandeaux/";
  }
  var l_filtres = "Tous | || Fichiers jpg|JPG||Fichiers png|PNG || Fichiers gif|GIF";
  var l_element_id=element_id;
  l_url = parent.IHM_getBaseUrl();
	l_url += "Administration/Administration/Layers/ChooseFile/TSPopup.php";
  l_url += "?Contenu=TSParcourir.php";
  l_url += "&absolu=0";
  l_url += "&clair=1"
  l_url += "&Valider=" + escape("S&eacute;lectionner");
  l_url += "&rslt=TSParcourir_RenvoiResultat";
  l_url += "&rslt_param=\"" + escape("fct=parent.frames['fenetre'].appearance_ChooseFile_back") + "\"";
  l_url += "&Repertoire="+l_rep;
  l_url += "&Rootdir="+l_rootdir;
  l_url += "&filtres="+l_filtres; 
  l_url +="&element_id="+l_element_id;
  parent.loadExtPopup(l_url, "CHOOSE_FILE", "Choix du fichier", 600, 400, true);
  //window.open(l_url, "CHOOSE_FILE", "width=100, height=100, directories=no, location=no, menubar=no, toolbar=no, status=no, scrollbars=auto");
}



function UploadFiles(element_id){
  var l_url = "";
  if (element_id=='logo' || element_id=='logo_right'){
    l_rootdir = PRO_CATALOGUE_ROOT + "/images/logos/";
  }
  else if(element_id=='top_image'){
    l_rootdir = PRO_CATALOGUE_ROOT + "/images/bandeaux/";
  }
  var l_element_id=element_id;
  l_url += "../../Upload/UploadPopup.php";
  l_url += "?absolu=0";
  l_url += "&path="+l_rootdir;
  l_url += "&nbMaxPj=1";
  l_url += "&btExec=Valider";
  l_url += "&fctExec=Upload_RenvoiResultat";
  l_url +="&element_id="+l_element_id;
  l_url += "&fctRslt=parent.frames['fenetre'].appearance_ChooseFile_back";
  parent.loadExtPopup(l_url, "UPLOAD_FILES", "Télécharger des fichiers", 600, 400, true);
}


function appearance_ChooseFile_back(value, element_id)
{
  var   inpt;
  inpt = document.getElementById(element_id);
  if (inpt)
    inpt.value = value;
}

/**
 * @brief Initialisation de la page de gestion de l'apparence
 */
function  administration_appearance_init()
{
  try
  {
    parent.IHM_historique_add(window.location);
    parent.IHM_Chargement_stop();
  }
  catch(e){}
  administration_appearance_onfocus();
  setSizeIFrame();

}


function  administration_appearance_onfocus()
{
  try
  {
    parent.IHM_Domaines_hide();
  }
  catch(e)
  {}
}




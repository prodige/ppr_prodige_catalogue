/**
 * @brief Initialisation de la page
 */
function administration_carto_ajout_init() {
	var domaine;
	var sousdomaine;
	var parcourir;

	try {
		// parent.IHM_historique_add(window.location);
		parent.IHM_Chargement_stop();
		// parent.IHM_Domaines_hide();
		// administration_carto_domaineChange();
		// parent.domaines_event_onchange_add(parent.events_administration_carto_changedomaine);
		// administration_carto_onfocus();
	} catch (e) {
	}
}

/**
 * @brief Valide l'ajout d'une carte
 */
function administration_carto_ajout_valider() {
	var serveur, format, fichier;
	var valid = true;
	var msg = "";
	var errormsg;
	format = document.getElementById("FORMAT");
	serveur = document.getElementById("SERVEUR");
	var treePanel = Ext.getCmp('ext-arbo-sdom');
	var importCheckbox = document.getElementById("IMPORT");
	var mefFile = document.getElementById("mefFile");
	if (format.value == 0) {
		fichier = document.getElementById("MAP");

		var modele = /^[a-z]+[a-z0-9_]*$/i;
		if (modele.test(fichier.value)) {
		} else {
			parent.Ext.Msg
					.alert(
							"Ajout de carte",
							"Le nom de fichier saisi ne doit pas contenir de caractères\n"
									+ "accentués ou non alphanumériques comme -()\"'. etc.");
			return;
		}
		if (in_array(fichier.value, tabData[serveur.value])) {
			parent.Ext.Msg.alert("Ajout de carte",
					"Une carte portant ce nom existe déjà.");
			return;
		}

	} else {
		fichier = document.getElementById("PATH");
	}

	if (serveur && format && fichier && treePanel) {
		if ((serveur.value == "") || (format.value == "")
				|| (fichier.value == "") || (treePanel.getChecked().length == 0)
				|| (importCheckbox.checked && mefFile.value == "")) {
			valid = false;
			parent.Ext.Msg.alert("Ajout de carte",
					"Tous les champs sont obligatoires");
			return;
		}
	}
	if (valid) {
		var oCtrlSdom = document.getElementById("SDOM");
		parent.checkAndSetCurrentSousDomaine(treePanel, oCtrlSdom, tabSdom, function(){parent.IHM_Chargement_start();document.forms[0].submit();}, this);
	}
}

/**
 * @brief Gère le changement de type de couche
 * @param format
 * @return
 */
function administration_carte_ajout_ontypechange(format) {
	var fichier, table;
	var parcourir;

	fichier = document.getElementById("file_item");
	table = document.getElementById("map_item");
	if (fichier && table) {
		switch (format.value) {
		case "0":
			fichier.style.display = "none";
			table.style.display = "";
			break;
		default:
			fichier.style.display = "";
			table.style.display = "none";
			break;
		}
	}
}

/**
 * @brief Fonction qui permet de changer le chemin de la carte
 * @param obj
 */
function administration_carto_ajout_changepath(obj) {
	var text = obj.value.toString();

	text = text.toLowerCase();
	text = text.replace(/[àâä]/ig, "a");
	text = text.replace(/[éèêë]/ig, "e");
	text = text.replace(/[îï]/ig, "i");
	text = text.replace(/[ôö]/ig, "o");
	text = text.replace(/[ù]/ig, "u");
	text = text.replace(/[ç]/ig, "c");
	text = text.replace(/\s/g, "_");
	obj.value = text;
}

/**
 * @brief ouvre la popup de choix des fichiers
 * @return
 */
function administration_carto_ajout_ChooseFile(element_id) {
	var l_url;
	var serveur;
	var format;

	format = document.getElementById("FORMAT");
	serveur = document.getElementById("SERVEUR");
	if (format.value == 0) {
		l_url = parent.IHM_getBaseUrl();
		l_url += "Administration/Administration/Layers/ChooseFile/TSPopup.php";
		l_url += "?Contenu=TSParcourirMap.php";
		l_url += "&Valider=Enregistrer";
		l_url += "&Serveur=" + serveur.value;
		// parent.OpenWindow(l_url, "liste des cartes existantes", true);
		parent.loadExtPopup(l_url, "MAP_LIST", "liste des cartes existantes", 600,
				400, true);
	} else {
		l_url = window.location.protocol + "//" + serveurs[serveur.value];
		l_url += "/PRRA/Administration/Administration/Layers/ChooseFile/TSPopup.php";
		l_url += "?Contenu=TSParcourir.php";
		l_url += "&absolu=0";
		l_url += "&clair=1"
		l_url += "&Valider=" + escape("S&eacute;lectionner");
		l_url += "&rslt=TSParcourir_RenvoiResultat";
		l_url += "&rslt_param=\""
		//+ escape("fct=parent.frames[\\'fenetre\\'].administration_carto_ajout_ChooseFile_back")
      + escape("fct=parent.fenetre.administration_carto_ajout_ChooseFile_back")
      + "\"";
		l_url += "&Root=STATIC_MAP_PATH";
		l_url += "&filtres=Tous|";
		l_url += "&element_id=PATH";
		parent.loadExtPopup(l_url, "CHOOSE_FILE", "Choix du fichier", 600, 400,
				false);
		// window.open(l_url, "CHOOSE_FILE", "width=100, height=100,
		// directories=no, location=no, menubar=no, toolbar=no, status=no,
		// scrollbars=auto");
	}

}

/**
 * @brief ouvre la popup de téléchargement des fichiers
 * @return
 */
function administration_carto_ajout_UploadFile() {
	var serveur;
	serveur = document.getElementById("SERVEUR");
	var l_url = "";
	l_url = window.location.protocol + "//" + serveurs[serveur.value];
	l_url += "/Upload/UploadPopup.php";
	l_url += "?absolu=0";
	l_url += "&nbMaxPj=1";
	l_url += "&btExec=Valider";
	l_url += "&fctExec=Upload_RenvoiResultat";
	l_url += "&element_id=PATH";
    //_url += escape("fctRslt=parent.frames[\\'fenetre\\'].administration_carto_ajout_ChooseFile_back");
	l_url += "&fctRslt=parent.fenetre.administration_carto_ajout_ChooseFile_back";
	l_url += "&Root=STATIC_MAP_PATH";
	parent.loadExtPopup(l_url, "UPLOAD_FILES", "Télécharger des fichiers", 600,
			400, false);
}

function administration_carto_ajout_ChooseFile_back(value) {
	var inpt;
	inpt = document.getElementById('PATH');
	if (inpt)
		inpt.value = value;
}

/**
 * @brief Valide l'ajout d'un service
 */
function administration_service_ajout_valider() {
	var valid = true;
	var msg = "";
	var errormsg;
	var treePanel = Ext.getCmp('ext-arbo-sdom');
	var importCheckbox = document.getElementById("IMPORT");
	var mefFile = document.getElementById("mefFile");

	if (treePanel.getChecked().length == 0 || (importCheckbox.checked && mefFile.value == "")) {
		valid = false;
		parent.Ext.Msg
				.alert("Ajout de carte", "Tous les champs sont obligatoires");
		return;
	}
	if (valid) {
		var oCtrlSdom = document.getElementById("SDOM");
		parent.checkAndSetCurrentSousDomaine(treePanel, oCtrlSdom, tabSdom, function(){parent.IHM_Chargement_start();document.forms[0].submit();}, this);
	}
}
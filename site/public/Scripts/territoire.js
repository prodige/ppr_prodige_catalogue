var combos = new Array();
var territoire_loaded = false;


function print_territoire(config, suffix){
  var table_territoire=document.getElementById("TABLE_TERRITOIRE_" + suffix);
  var check=document.getElementById("check_extract_territoire_" + suffix);
  if (check.checked){
	table_territoire.style.display="block";
	load_territoire(config, suffix);
  }
  else {
    table_territoire.style.display="none";
  }
}

/**
 * charge les territoires
 */
function load_territoire(config, suffix) {
  if ( territoire_loaded ) return; 
	Ext.Ajax.request({
	  url: config.providerUrl,
	  method: "GET",
	  success: function (response) { build_combos(response, config, suffix); } ,
	  failure: function (response) { 
	     Ext.Msg.alert("Territoire", "Erreur de récupération des territoires.");
	  },
	  params : {"queryMode": "getFields", "tablename": config.providerBaseParams.tablename, "service_idx": config.providerBaseParams.service_idx },
	  scope: this
	});
	
	territoire_loaded = true;
}

/**
 * construit les combobox
 */
function build_combos(response, config, suffix) {
  comboConfigs = Ext.decode(response.responseText);
  
  for (var i=0; i<comboConfigs.length; i++) {
    var combo = this.build_combo(comboConfigs[i], config, suffix);
    combos.push(combo);
  }
  
  for (var i=0; i<comboConfigs.length; i++) {
	  var joined = parseInt(comboConfigs[i].field_join); 
	  if (joined!=0 && joined <=comboConfigs.length) {
	    link_combos(i, joined-1, comboConfigs);  
	  }
	}
  
  for (var i=0; i<combos.length; i++) {
		combos[i].getStore().load();
	}
}

/**
 * construit un combobox
 */
function build_combo(comboConfig, config, suffix) {
  
  // récupère le tableau
  var table = document.getElementById(config.table_cb_id);
  if ( table.tBodies.length==0 ) {
		var tBody = document.createElement("tbody");
		table.appendChild( tBody );
		table = tBody;
	} else {
		table = table.tBodies.item(0);
	}
	
	// ajout une ligne à la fin
  var row = table.insertRow(-1);
  row.id = "TR_CRITERE" + comboConfig.field_order;
  
  // ajout la cellule de l'intitulé
  var cell = row.insertCell(row.cells.length);
  cell.id = "TITLE_CRITERE" + comboConfig.field_order;
  cell.setAttribute("align", "right");
  cell.innerHTML = comboConfig.field_name;
  
  // ajoute la cellule du ":"
	/*var cell = row.insertCell(row.cells.length);
  cell.innerHTML = ":";*/
  
  // ajoute la cellule du combobox
  var cell = row.insertCell(row.cells.length);
  cell.innerHTML = "<input id=\"SELECT_NAME_CRITERE" + comboConfig.field_order + "\" type=\"text\" size=\"40\"/>";
  
  // ajoute la cellule du bouton "Sélectionner"
  var cell = row.insertCell(row.cells.length);
  cell.innerHTML = "<input id=\"bt_select" + comboConfig.field_order + "\" type=\"Button\" value=\"Sélectionner\" onclick=\"select_territoire(" + comboConfig.field_order + ", " + comboConfig.field_pk + ", '" + suffix + "')\">";
  
  var cb = new Ext.form.ComboBox({
    store: build_store(comboConfig, config),
    fieldLabel: comboConfig.field_name,
    displayField: 'name',
    valueField: 'id',
    hiddenName: "SELECT_VALUE_CRITERE" + comboConfig.field_order,
    hiddenId: "SELECT_VALUE_CRITERE" + comboConfig.field_order,
    typeAhead: true,
    forceSelection: true,
    mode: 'local',
    triggerAction: 'all',
    emptyText:'Sélectionnez un territoire',
    selectOnFocus: true,
    applyTo: 'SELECT_NAME_CRITERE' + comboConfig.field_order
  });
  cb.addListener('select', 
    function(cmb, record, index) {
      document.getElementById("bt_select" + comboConfig.field_order).style.display = "";
    }, 
    this);
  
  return cb;
  
}

/**
 * ajoute les listeners
 */
function link_combos(linked_id, parent_id) {
	combos[parent_id].addListener('select', 
	  function(cmb, record, index) {
	    var params = { queryParams_filterValue : record.data.id };  
	    this.combos[linked_id].clearValue();
	    this.combos[linked_id].getStore().load({params: params});
 		}, this);
}

// Todo: Should be passed as a config object, at least the reader...
function build_store(comboConfig, config) {
       
  var store = new Ext.data.Store({
    proxy: new Ext.data.ScriptTagProxy({
      url: config.providerUrl
    }),
    baseParams: {"queryMode": "getData", "queryParams_pk": comboConfig.field_pk, "tablename": config.providerBaseParams.tablename, "service_idx": config.providerBaseParams.service_idx },
    reader: new Ext.data.JsonReader({
      idproperty: 'id',
      totalProperty: 'totalCount',
      root: 'Names'
      },
      [ { name: 'extent'},
        { name: 'id' },
        { name: 'name' }
      ])
  });
  return store;
};

/**
 * affecte les champs cachés contenant les valeurs du territoire choisi par l'utilisateur
 */
function select_territoire(field_order, field_pk, suffix ){
  if ( document.getElementById("SELECT_VALUE_CRITERE"+field_order).value == "" ) {
    Ext.Msg.alert("Territoire", "Veuillez sélectionner un territoire.");
    return;
  }
  
  var territoire_field = document.getElementById("TITLE_CRITERE"+field_order).innerHTML;
  var territoire_value = document.getElementById("SELECT_VALUE_CRITERE"+field_order).value;
  var territoire_name  = document.getElementById("SELECT_NAME_CRITERE"+field_order).value;
  
  document.getElementById("SELECT_TERRITOIRE_FIELD" + "_" + suffix).value = field_pk;
  document.getElementById("SELECT_TERRITOIRE_DATA" + "_" + suffix).value = territoire_value;
  
  document.getElementById("territoire_msg").innerHTML = territoire_field + " : " + territoire_name;
  
  document.getElementById("territoire_criteria").style.display = "none";
  document.getElementById("territoire_result").style.display = "block";
}

/**
 * réinitialise les champs cachés contenant les valeurs du territoire choisi par l'utilisateur et réaffiche les critères
 */
function change_territoire(suffix){
  document.getElementById("SELECT_TERRITOIRE_FIELD" + "_" + suffix).value = "";
  document.getElementById("SELECT_TERRITOIRE_DATA" + "_" + suffix).value = "";
  
  document.getElementById("territoire_result").style.display = "none";
  document.getElementById("territoire_criteria").style.display = "block";
}

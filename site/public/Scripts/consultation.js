/**
 * @brief Initialisation de la page
 * @return
 */
function	consultation_init()
{
	try
	{
	  parent.IHM_historique_add(window.location);
		parent.IHM_Chargement_stop();
		consultation_domaineChange();
		parent.domaines_event_onchange_add(parent.events_consultation_changedomaine);
		consultation_onfocus();
	}
	catch(e)
	{}
}

function	consultation_onfocus()
{
	var		domaine;
	
	try
	{
		domaine = parent.domaines_getCurentDomaine();
		if (domaine)
			parent.IHM_Domaines_resume();
		else
			parent.IHM_Domaines_show();
		if (parent.consultation_win_cartodynamique)
			parent.consultation_win_cartodynamique.focus();
	}
	catch(e){}
}
/**
 * @brief Gestion du changement de domaine
 * @return
 */
function	consultation_domaineChange()
{
	var		domaine;
	var		sousdomaine;
	
	try
	{
		domaine = parent.domaines_getCurentDomaine();
		sousdomaine = parent.domaines_getCurentSousDomaine();
		if (domaine)
			consultation_loadData(domaine, sousdomaine);
		else
		{
			parent.Ext.getCmp('westLayout').setTitle("Domaine / Sous-domaine");
			parent.IHM_Panier_hide();
			parent.IHM_Domaines_show();
			parent.IHM_Chargement_stop();
			parent.IHM_NoSelectionDomain_show();
      window.timerOnload = setTimeout("window.onload()", 2500);
		}
	}
	catch(e)
	{}
}
/**
 * @brief Affichage des données d'un domaine/sous domaine
 * @param domaine
 * @param sousdomaine
 * @return
 */
function	consultation_loadData(domaine, sousdomaine)
{
	var		consultation;
	var		url;
	var		params;
	var		i;
	var		param;
	var		curdom = null;
	var		cursdom = null;
	if ( window.timerOnload ){
    clearTimeout(window.timerOnload);
    window.timerOnload = null;
  }
  parent.IHM_Chargement_start();
	
	consultation = document.getElementById("consultation");
	if (consultation)
	{
		consultation.className = "consultation";
		url = document.location.pathname + "?domaine=" + domaine;
		params = document.location.search.substr(1).split("&");
		for (i = 0; i < params.length; i++)
		{
			param = params[i].split("=");
			if (param[0] == "domaine")
				curdom = unescape(param[1]);
			if (param[0] == "sousdomaine")
				cursdom = unescape(param[1]);
		}
		if (sousdomaine)
			url += "&sousdomaine=" + sousdomaine;
	  
		if ((!curdom && domaine) || (!cursdom && sousdomaine) || (cursdom && !sousdomaine) || (curdom != domaine) || (cursdom != sousdomaine))
			document.location = url;
		else {
			consultation_setResume(domaine, sousdomaine);
			parent.IHM_Chargement_stop();
		}
	}
}
/**
 * @brief Paramètre le texte résumé pour un sous-domaine
 * @param domaine
 * @param sousdomaine
 * @return
 */
function	consultation_setResume(domaine, sousdomaine)
{
	var		div_dom;
	var		div_sdom;
	var		node;
	
	div_dom = document.getElementById("domaine");
	if (domaine && div_dom)
	{
		node = div_dom.firstChild;
		if (node && (node.nodeType == 3))
			node.data = domaine;
		else
		{
			node = document.createTextNode(domaine);
			if (node)
				div_dom.appendChild(node);
		}
	}
	div_sdom = document.getElementById("sousdomaine");
	if (sousdomaine && div_sdom)
	{
		node = div_sdom.firstChild;
		if (node && (node.nodeType == 3))
			node.data = sousdomaine;
		else
		{
			node = document.createTextNode(sousdomaine);
			if (node)
				div_sdom.appendChild(node);
		}
	}
}

/**
 * @brief Ouvre l'interface cartographique
 * @param url url de la carte
 * @param mapName nom de la carte
 * @return
 */
function	consultation_openInterface(url, mapName)
{
	//maj Logs
  var iframe = this;
  iframe.tabParams = new Array(mapName);
  var ajaxUrl = parent.IHM_getBaseUrl();
  ajaxUrl += "consultation_addLog.php";
  var queryParams = {'logFile' : 'ConsultationCarte', 'objectName' :mapName.replace('"', '""')};
  iframe.onSuccess = function(){};
  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
  parent.consultation_win_cartodynamique = window.open(url, "Cartographie_dynamique", "directories=0, fullscreen=yes, menubar=0, status=1, toolbar=yes, resizable=1");
}
/**
 * @brief Ouvre l'interface cartographique pour une carte statique
 * @param url url de la carte
 * @param mapName nom de la carte
 * @return
 */
function	consultation_openCarteStatique(url, mapName)
{
  //maj Logs
  var iframe = this;
  iframe.tabParams = new Array(mapName);
  var ajaxUrl = parent.IHM_getBaseUrl();
  ajaxUrl += "consultation_addLog.php";
  var queryParams = {'logFile' : 'ConsultationCarte', 'objectName' :mapName};
  iframe.onSuccess = function(){};
  parent.AjaxRequest(ajaxUrl, queryParams, iframe);
  window.open(url, "Carte_Statique", "directories=0, fullscreen=yes, menubar=0, status=1, toolbar=0, resizable=1");
}

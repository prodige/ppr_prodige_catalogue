function TextEncode(strParam)
{
  var t = new String("test");
  if (!t.charCodeAt) return strParam;
  strParam = encodeToUTF8(strParam);
  var strEncode = "";
  for(var i=0; i<strParam.length; i++) {
    strEncode += DecToHexa(strParam.charCodeAt(i));
  }
  return strEncode;
}

function TextDecode(strParam)
{  
  var test = "";
  var t = new String("test");
  if (!t.fromCharCode && !String.fromCharCode) return strParam;
  //dÃ©codage
  var strDecode = "";
  for(var i=0; i<strParam.length; i+=2 ) {
    strDecode = new String(strDecode) + String.fromCharCode( HexaToDec(strParam.substr(i, 2) ) ) ;
  }

  return decodeFromUTF8(strDecode);
}

function DecToHexa(integer, bIter){
  var tabConv = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
  if (integer<=15 ) {
    if( bIter) return tabConv[integer]; 
    return "0"+tabConv[integer];
  }
  var quotient = Math.floor(integer/16);
  var remainder = integer % 16;
  return new String(DecToHexa(quotient,true))+new String(DecToHexa(remainder,true));
}

function HexaToDec(hexa){
  var tabConv = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
  if (hexa.length==1){
    for (var j=0; j<tabConv.length; j++){
      if (tabConv[j].toLowerCase()==hexa.toLowerCase()) return j;
    }
    return 0;
  }
  var res = 0;
  for (var p=0; p<hexa.length; p++){
    res = new Number( new Number(res) + new Number(HexaToDec(hexa.charAt(p))*Math.pow(16, hexa.length - p -1)) );
  }
  return res;
}

function encodeToUTF8(strLatin1) 
{
  var strUTF8 = "";

  for (var n = 0; n < strLatin1.length; n++) {
    var c = strLatin1.charCodeAt(n);
    if (c < 128) {
      strUTF8 += String.fromCharCode(c);
    }
    else if((c > 127) && (c < 2048)) {
      strUTF8 += String.fromCharCode((c >> 6) | 192);
      strUTF8 += String.fromCharCode((c & 63) | 128);
    }
    else {
      strUTF8 += String.fromCharCode((c >> 12) | 224);
      strUTF8 += String.fromCharCode(((c >> 6) & 63) | 128);
      strUTF8 += String.fromCharCode((c & 63) | 128);
    }
  }

  return strUTF8;
}
    
function decodeFromUTF8(strUTF8) {
  var strLatin1 = "";
  var i = 0;
  var c = c1 = c2 = 0;

  while ( i < strUTF8.length ) {
    c = strUTF8.charCodeAt(i);
    if (c < 128) {
      strLatin1 += String.fromCharCode(c);
      i++;
    }
    else if((c > 191) && (c < 224)) {
      c2 = strUTF8.charCodeAt(i+1);
      strLatin1 += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
      i += 2;
    }
    else {
      c2 = strUTF8.charCodeAt(i+1);
      c3 = strUTF8.charCodeAt(i+2);
      strLatin1 += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
      i += 3;
    }
  }

  return strLatin1;
}
function getCheckSumEncodage(strToken)
{
  var tabTrad = new Array();
  for(var i=0; i<10; i++) tabTrad[i] = i;
  tabTrad["A"] = tabTrad["a"] = 10;
  tabTrad["B"] = tabTrad["b"] = 11;
  tabTrad["C"] = tabTrad["c"] = 12;
  tabTrad["D"] = tabTrad["d"] = 13;
  tabTrad["E"] = tabTrad["e"] = 14;
  tabTrad["F"] = tabTrad["f"] = 15;
   
  var iChecksum = 0;
  for(i=0; i<strToken.length; i++) {
    iChecksum += parseInt(tabTrad[strToken.charAt(i)]);
  }
  return iChecksum;
}

function replace_html(el, html) {
	if( el ) {
		var oldEl = (typeof el === "string" ? document.getElementById(el) : el);
		var newEl = document.createElement(oldEl.nodeName);

		// Preserve any properties we care about (id and class in this example)
		newEl.id = oldEl.id;
		newEl.className = oldEl.className;

		//set the new HTML and insert back into the DOM
		newEl.innerHTML = html;
		if(oldEl.parentNode)
			oldEl.parentNode.replaceChild(newEl, oldEl);
		else
		oldEl.innerHTML = html;

		//return a reference to the new element in case we need it
		return newEl;
	}
}; 

/*!
 * Ext JS Library 3.1.1
 * Copyright(c) 2006-2010 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */

//
// This is the main layout definition.
//
function loadScriptJs(url) {
  var e = document.createElement("script");
  e.src = url;
  e.type="text/javascript";
  document.getElementsByTagName("head")[0].appendChild(e);      
}

if ( Ext.BLANK_IMAGE_URL == "http://extjs.com/s.gif" ){
  Ext.BLANK_IMAGE_URL = document.location.protocol+"//"+document.location.host+"/PRRA/images/s.gif";
}

Ext.ns('Extensive.grid');

Extensive.grid.ItemDeleter = Ext.extend(
        Ext.grid.RowSelectionModel,
        {
          width :30,
          sortable :false,
          dataIndex :0, // this is needed, otherwise there will be an error
          menuDisabled :true,
          fixed :true,
          id :'deleter',
          initEvents : function() {
            Extensive.grid.ItemDeleter.superclass.initEvents.call(this);
            this.grid.on('cellclick',
                function(grid, rowIndex, columnIndex, e) {
                  if (columnIndex == grid.getColumnModel().getIndexById(
                      'deleter')) {
                    var record = grid.getStore().getAt(rowIndex);
                    grid.getStore().remove(record);
                    grid.getView().refresh();
                  }
                });
          },

          renderer : function(v, p, record, rowIndex) {
            return '<div class="extensive-remove" style="width: 15px; height: 16px;"></div>';
          }
        });
var itemDeleter = new Extensive.grid.ItemDeleter();

Ext.ns('Panier');
Panier.Grid = Ext
    .extend(Ext.grid.GridPanel, {
      initComponent : function() {
        // hard coded - cannot be changed from outside
        var config = {
          store :new Ext.data.SimpleStore( {
            id :0,
            fields : [ {
              name :'Data',
              type :'text'
            } ],
            data : []
          }),
          columns : [ {
            id :'Data',
            header :"Données",
            sortable :false
            }, 
            itemDeleter
          ],
          selModel: itemDeleter,

          viewConfig : {
            forceFit :true
          }
        }; // eo config object

        // apply config
        Ext.apply(this, Ext.apply(this.initialConfig, config));

        // call parent
        Panier.Grid.superclass.initComponent.apply(this, arguments);
      } // eo function initComponent

    });

Ext.reg('paniergrid', Panier.Grid);

Ext.TabPanel.prototype.onStripMouseDown = function(e){
        if(e.button !== 0){
            return;
        }
        e.preventDefault();
        var t = this.findTargets(e);
        if(t.close){
            if (t.item.fireEvent('beforeclose', t.item) !== false) {
                t.item.fireEvent('close', t.item);
                this.remove(t.item);
            }
            return;
        }
        if(t.item){
            if ( t.item == this.activeTab ){
              this.activeTab.fireEvent("activate", this.activeTab);
            }
            else 
              this.setActiveTab(t.item);
        }
    }
;

function createTab(title, id, className){
  return {
            title : title,
            id : id,
            className : className,
            listeners : {
              activate :IHM_Onglets_sel
            }
          };
}
Ext.onReady( function() {
  
  // ne crée pas les objets si l'application qui utilise cette librairie crée elle-même ses propres objets
  var PRO_INCLUDED = document.getElementById("PRO_INCLUDED");
  if ( PRO_INCLUDED && PRO_INCLUDED.value == 1 ) {
  	return;
  }
  Ext.QuickTips.init();
  var Tree = Ext.tree;

  // arbre des domaines/
    var root = new Tree.AsyncTreeNode( {
      text :'Root', // texte du noeud
      draggable :false, // Désactiver le Drag and drop sur ce noeud
      leaf :false,
      id :'1', // identifiant du noeud
      loader : new Ext.tree.TreeLoader({
        url : "/PRRA/include/domaines.php",
        baseParams : {load : 1},
        requestMethod : "GET",
        listeners : {
          scope : this,
          load : function(loader, node, response){
            domaineList = eval(response.responseText);
          }
        }
      })
    // Objet JSON contenant la structure de l'arbre
    });

    var TreePanel = new Tree.TreePanel( {
      id :'TreeDomaine',
      animate :true, // animer l'arbre lors des expand / collapse
      enableDD :false, // Désactiver le Drag and drop
      loader :new Ext.tree.TreeLoader(), // Le composant qui va charger
      selModel :new Ext.tree.DefaultSelectionModel(), // Par défaut c'est le
      rootVisible :false,// La racine est visible
      border : false,
      currentNode : null,
      root : root
    });
    TreePanel.on('click', function(node, event) {
      TreePanel.currentNode = node;
      domaines_event_onchange_raise(event);
      
      var nodeDef = node.attributes;
      var domaine = (nodeDef.isDomain ? nodeDef.text : nodeDef.domain);
      var sousdomaine = (nodeDef.isSubDomain ? nodeDef.text : "");
      if (typeof(sousdomaine)!="undefined" &&  sousdomaine!="" ){
        Ext.getCmp('westLayout').setTitle(domaine+" / "+sousdomaine);
      }
      
      else if(typeof(domaine)!="undefined" && domaine!=""){
        Ext.getCmp('westLayout').setTitle(domaine);
      }else{
        Ext.getCmp('westLayout').setTitle("Domaine / Sous-domaine");
      }
    });

    //TreePanel.setRootNode(root);

    var actionPanier = new Ext.Action({
      id : "actionPanier",
      text : "Panier",
      icon : "images/basket.gif",
      handler: function(){
        var panier = Ext.getCmp("contPanier");
        if ( panier ){
          if ( panier.collapsed )
            panier.expand(true);
          else
            panier.collapse(true);
        }
      }
    });
    
    var tabButtonRight = new Array();
    tabButtonRight.push(actionPanier);
    
	if(PRO_CATALOGUE_CONTACT_ADMIN == "on"){
		 var adminContact = new Ext.Action({
            id : "adminContact",
            text : "",
            icon : "images/contact.png",
            tooltip : "Contacter l'administrateur du site",
            handler: function(){
              var l_url = parent.IHM_getBaseUrl();
           	  l_url += "contact_admin.php?";
           	  parent.loadExtPopup(l_url, "index.php", "Contacter l'administrateur", 600, 400, true);
           	}
          });
    	tabButtonRight.push('-',adminContact);
    }
	if(PRO_CATALOGUE_NB_SESSION_USER == "on"){
    	var textSessionNumber = {xtype: 'tbtext', text: '<font color="red">'+sessionNumber+'</font> visiteur(s)'};
    	tabButtonRight.push('-',textSessionNumber);
    }   
    
    var tabItems = [createTab('Accueil', 'catalogueresume', 'catalogueresume_sel'),
                    createTab('Recherche', 'recherche', 'recherche'),
                    createTab('Données', 'donnees', 'donnees'),
                    createTab('Cartes', 'consultation', 'consultation'),
                    createTab('Cartes personnelles', 'carteperso', 'carteperso_hide')];
    if ( PRO_INCLUDED_CONNEXION_AUTO!=1) {
   	  tabItems.push(createTab('Connexion', 'connexion', 'connexion_hide'));
    }
    tabItems.push(createTab('Administration', 'administration', 'administration_hide'));
    if ( PRO_INCLUDED_CONNEXION_AUTO!=1) {
      tabItems.push(createTab('Déconnexion', 'deconnexion', 'deconnexion_hide'));
    }
    tabItems.push(createTab('Aide', 'aide', 'aide_hide'));
    
    
    var TabPanel = new Ext.TabPanel({
      region :'center',
      html :'<div id="principal" class="principal"><iframe src="./catalogueresume.php" name="fenetre" id="fenetre" class="fenetre" frameborder="0" allowTransparency="true"></iframe><iframe src="vide.php" name="srv_sync" id="srv_sync" class="serveur_sync" frameborder="0"></iframe></div>',
      border :false,
      bodyStyle :'background-color:f0f0f0;',
      title :'Center',
      id :'tabPanel',
      xtype :'tabpanel',
      //activeTab :'catalogueresume',
      style : {
        position : "absolute",
          top : "0"
      },
      items : tabItems
   });

    
    window.viewport = new Ext.Viewport({
      id :'pageViewport',
      layout :'border',
      border :true,
      forceLayout : true,
      items : [{
        region :'north',
        height :(PRO_INCLUDED_CONNEXION_AUTO==1 ? 0 : 80 ),
        border :false,
        //title :'Prodige V3',
        collapsible :false,
        html : bandeauHtml
      },{
        region :'west',
        width :300,
        border :true,
        id :'westLayout',
        autoScroll :true,
        layout :'fit',
        forceLayout : true,
        header :false,
        collapsible :false,
        collapsed :true,
        collapsibleSplitTip : "Cliquer pour afficher l'arbre des domaines",
        splitTip : "Cliquer pour afficher l'arbre des domaines",
        useSplitTip : true,
        cmargins :{right : 5, top:0, left:5, bottom:0},
        split :true,
        items : [  TreePanel  ],
        title : "Domaine / Sous-domaine",
        plugins: [Ext.ux.PanelCollapsedTitle]
      },{
        region :'center',
        border :true,
        bodyStyle :'background-color:f0f0f0;',
        id :'centerPanel',
        layout : "fit",
        activeTab :'catalogueresume',
        items : [ TabPanel, 
        {
          id :'contPanier',
          layout : "fit",
          forceLayout : true,
          collapsed : true,
          listeners : {
            scope : this,
            expand : function(){
              //this.el.dom.style.height = "500px";
            }
          },
          style : {
            position : "absolute",
            width : "200",
            right : "0",
            top : 50,
            "z-index" : 100
          },
          y : 27,
          width : 360,
          height : 300,
          items : [{
            id :'gridPanier',
            xtype :'paniergrid',
            autoScroll : true
          }],
          tbar :  new Ext.Toolbar({
            buttonAlign:"right",
            items : ["->", {
              icon: 'images/tab-close-on.gif',
              handler : function(){
                var panier = Ext.getCmp("contPanier");
                if ( panier ){
                  panier.collapse(true);
                }
              },                          
              align : "right"
            }] 
          }),
          bbar : new Ext.Toolbar({
            buttonAlign:"right",
            items : [{
              text :'Télécharger',
              icon: 'images/download.gif',
              handler :panier_downloadParametrage,
              align : "right"
            }, {
              text :'Co-visualiser',
              icon: 'images/Oeil.gif',
              handler :panier_covisualiser,
              align : "right"
            }, {
              text :'Vider',
              icon: 'images/remove.png',
              handler :panier_vider,
              align : "right"
            }] 
          })
        }]
      }],
      renderTo :"pageDiv"
    });
    
    viewport.doLayout(true, true);
    //viewport.layout.center.panel.getTopToolbar().buttonAlign = "right";
    
    
    //Ext.getCmp("contPanier").getTopToolbar().buttonAlign = "right";
    // masquer les tables du mode connecté
    //Ext.getCmp("tabPanel").hideTabStripItem("consultation");
    if (PRO_INCLUDED_CONNEXION_AUTO!=1)
      Ext.getCmp("tabPanel").hideTabStripItem("deconnexion");
    Ext.getCmp("tabPanel").hideTabStripItem("administration");
  
    Ext.getCmp("gridPanier").getView().el.select('.x-grid3-header').setStyle('display', 'none');
    if(typeof(window.idMetadata)!="undefined"){
      Ext.getCmp("recherche").query = 'uuid='+window.idMetadata;
      Ext.getCmp("tabPanel").setActiveTab(Ext.getCmp("recherche").id);
      Ext.getCmp("recherche").query = '';
    }else if(typeof(window.adminTab)!="undefined"){
    	Ext.getCmp("administration").query = 'adminTab='+window.adminTab;
        Ext.getCmp("tabPanel").setActiveTab(Ext.getCmp("administration").id);
        Ext.getCmp("administration").query = '';
    }else if(typeof(window.chgpwdToken)!="undefined"){
    	Ext.getCmp("connexion").query = 'chgpwdToken='+window.chgpwdToken;
        Ext.getCmp("tabPanel").setActiveTab(Ext.getCmp("connexion").id);
        Ext.getCmp("recherche").query = '';
    }else{
      Ext.getCmp("tabPanel").setActiveTab(Ext.getCmp("catalogueresume").id);
    }
    var ptbar = new Ext.Panel({
      id : "ptoolbar_",
      renderTo : TabPanel.el,
      y : -1,
      style : { position : "absolute", right : "0px"},
      width : "auto",
      height : 20,
      hideBorders : true,
      bodyStyle : "display:none"
    });

    var tbar = new Ext.Toolbar({
      id : "toolbar_",
      renderTo : ptbar.el.id,
      buttonAlign : "right",
      style : { background : "none", border : "none"},
      width : '100%',
      height : 25,
      items: tabButtonRight
    });
    // Ext.get('loading').hide();
    // Ext.get('loading-mask').fadeOut();
    
    IHM_Onglets_activate(bConnexion, bAdministration, bCartePerso);
  });
function reloadTree() {
  Ext.getCmp('TreeDomaine').enable();
  Ext.getCmp('TreeDomaine').root.reload(function(){});
};


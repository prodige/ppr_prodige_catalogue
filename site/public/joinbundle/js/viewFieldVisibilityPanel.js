////////////////////////////////
// Field visibility Panel
////////////////////////////////
 Carmen.View.FieldVisibilityPanel = Ext.extend(Ext.TabPanel, {
	
	config : {   width : 490,
		manager : null,
		viewInfo : null,
		height : 330,
		frame : true,
		activeTab : 0,
		title : 'Paramétrage des champs visibles',
		id : 'fieldVisibilityPanel',
		labelWidth : 150,
		items : [],
		buttons : [
			{ 
				text : 'Fermer'
			}
		]
	},
	
	constructor : function(cfg) {
		Ext.apply(this.config, cfg);
		Carmen.View.FieldVisibilityPanel.superclass.constructor.apply(this, [this.config]);
	
		var btnCancel = this.buttons[0];
		btnCancel.setHandler(
			function() { 
				this.manager.hideComponent(this); 
				//this.manager.showComponent(this.manager.viewEditionPanel); 
				this.manager.showComponent(this.manager.groupViewPanel);
			}, this);
	},
    
	// private function used to generate field visibility panel 
	// per view components (refered as src, 0 -> layer, 1 table from join 1...)
    createFieldVisibilitySubPanel : function(src, srcTitle) {
		var fieldStore = new Ext.data.JsonStore({
			url : this.manager.getManageServiceURL(),
			baseParams : {
				serviceName : "getFieldVisibility",
				parameters : Ext.encode({
					viewId : this.compositeViewInfo.viewId,
					src : src
				})
			},
			fields : [
				"name", 
				"alias", 
				{ name : "visible", type : "bool" } 
			],
			idProperty : "name",
			root : "data",
			successProperty : "success",
			storeId : 'fieldVisibilityStore_' + this.viewInfo.viewId + '_' + src
		});
		fieldStore.load();

		var checkColumn = new Ext.grid.CheckColumn({
			header: 'Visible',
			dataIndex: 'visible',
			align : 'center',
			resizable : false,
			width: 60,
			sortable: false, 
			menuDisabled : true
		});
		var fieldGridPanel = new Ext.grid.EditorGridPanel({
			store: fieldStore,
			columns: [
				//{header: "Nom du champ", width: 195, sortable: true, menuDisabled : true, dataIndex : "name"},
				{header: "Alias du champ dans la vue", width: 380, sortable: true, menuDisabled : true, dataIndex : "alias"},
				checkColumn
			],
			plugins : checkColumn,
			stripeRows: true,
			clicksToEdit: 1,
			enableColumnMove  : false,
			height : 200,
			width : 490,
			frame : true,
			selModel: new Ext.grid.RowSelectionModel()
		});

		var fieldVisibilityUpdate = function() {
			var visibleFields= [];
			var store = Ext.StoreMgr.get("fieldVisibilityStore_" + this.viewInfo.viewId + '_' + src);
			for (var i=0; i< store.getTotalCount(); i++) {
			  var rec= store.getAt(i);
        if (rec.data.visible) {
          visibleFields.push(rec.data.name);
        }
			}
			
			Ext.Ajax.request({
				url: this.manager.getManageServiceURL(),
				method: 'POST',
				params: { 
					serviceName: 'setFieldVisibility',
					parameters : Ext.encode({
						viewId : this.compositeViewInfo.viewId,
						visibleFields : visibleFields,
						src : src,
						urlUpdateMetadataView : this.manager.getUpdateMetadataDateServiceURL(),
						urlUpdateMetadataField : this.manager.getUpdateMetadataCatalogueServiceURL()
					})
				},
				success: function(response) {
				   response = Ext.decode(response.responseText);
				   if (response && response.success) {
					   Ext.Msg.alert('Opération réussie', response.msg);
				   }
				   else
					   Ext.Msg.alert('Erreur', ((response && response.errmsg)  ? response.errmsg : "Erreur lors de l'appel du service"));
				},
				failure: function(response) {
				   response = Ext.decode(response.responseText);
           Ext.Msg.alert('Erreur', ((response && response.errmsg)  ? response.errmsg : "Erreur lors de l'appel du service"));
				},
				scope : this
			});
		};
		
		var fieldVisibilitySubPanel = new Ext.Panel({
			width : 400,
			title : srcTitle,
			id : 'fieldVisibilitySubPanel' + this.viewInfo.viewId + '_' + src,
			labelWidth : 150,
			items : [],
			buttons : [
				{ 
					id : 'fieldVisibilityPanelValidate' + this.viewInfo.viewId + '_' + src,
					text : 'Valider',
					handler : function() {
					  this.manager.beforeUpdateViewAction.call(this.manager, false, this.okCallback, Ext.emptyFn, false, this.scope)
					},
					scope : { manager : this.manager, okCallback : fieldVisibilityUpdate, scope : this}
				}
			]
		});
		
		fieldVisibilitySubPanel.add(fieldGridPanel);
		return fieldVisibilitySubPanel;
	},
    
    update : function(viewInfo) {
		this.viewInfo = viewInfo;
		// cleaning
		var itemCount = this.items.getCount();
		for (var i=1; i<itemCount; i++) {
			var item = this.items.itemAt(i);
			this.remove(item, true);
		}
		
		for (var i=0; i< viewInfo.joinCount+1; i++) {
			var tableFieldTabPanel = this.createFieldVisibilitySubPanel(i, 
				i==0 ? 
				"Données " + this.viewInfo.layerName :
				"Table " + this.viewInfo.joinInfos[i-1].tableName);
			this.add(tableFieldTabPanel);
		}
		this.setActiveTab(0);
	}
});
Ext.namespace('Carmen');
Ext.namespace('Carmen.View');

Carmen.ProdigeCatalogueReader = new Ext.data.JsonReader({
	fields:[
		{name: 'desc', mapping: 'text'},
		'type',
		{name: 'value', mapping: 'table_name'},
		{name: 'id', mapping: 'id'},
		'leaf', 
		'cls',
		'children']
});

function pg2prodigeType(type) {
	var mapping = { 
		"int4" : "numeric",
		"int2" : "numeric",
		"int8" : "numeric",
		"float8" : "numeric",
		"float4" : "numeric",
		"numeric" : "numeric",
		"varchar" : "text",
		"bpchar" : "text",
		"text" : "text",
		"date" : "date" 
	};
	return (type in mapping) ? mapping[type] : "text";
}


Carmen.View.Util = {
	

	// specific renderers
	joinRestrictionRenderer: function(value, metaData, record, rowIndex, colIndex, store) {
		return value ? 'Oui' : 'Non';
    },
    
    btnDelRenderer: function(value, metaData, record, rowIndex, colIndex, store) {
		metaData.css = 'silk-delete';
		return '';
	},
   
	btnEditRenderer: function(value, metaData, record, rowIndex, colIndex, store) {
		metaData.css = 'silk-cog';
		return '';
   },
   
   decodeCriteria : function(strCriteria) {
		var res = {viewField : "", tableField : "" };
		var mask = /^\s*\[([^\]]+)\]\s*[=]\s*\[([^\_]+)_([^\]]+)\]\s*$/;
		var result = strCriteria.match(mask);
		if (result != null) {
			res.viewField = result[1];
			res.tableField = result[3];
		}
		return res;
	}
	
};

//extend Ext.Panel to have a setTabTip method
Ext.Panel.prototype.setTabTip = function(msg) {
  var tabPanel = this.findParentByType(Ext.TabPanel);
  if (tabPanel) {
    var tabEl = Ext.fly(tabPanel.getTabEl(this).id);
    if (tabEl) {
      var tsTxt = tabEl.child('span.x-tab-strip-text', true);
      if (tsTxt)
        tsTxt.qtip = msg.toString();
    }
  }
}
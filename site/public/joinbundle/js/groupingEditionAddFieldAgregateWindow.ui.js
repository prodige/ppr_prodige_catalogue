/*
 * File: groupingEditionPanel.ui.js
 * Date: Wed Sep 07 2011 09:46:50 GMT+0200 (CEST)
 *
 * This file was generated by Ext Designer version 1.2.0.
 * http://www.sencha.com/products/designer/
 *
 * This file will be auto-generated each and everytime you export.
 *
 * Do NOT hand edit this file.
 */

groupingEditionAddFieldAgregateWindowUi = Ext.extend(Ext.Window, {
    height: 150,
    width: 473,
    padding: 10,
    title: 'Paramétrage de l\'agrégation',

    initComponent: function() {
        this.items = [
			{
				xtype: 'form',
				id: 'fieldAgregateAdd_formPanel',
				items: [
					{
						xtype: 'combo',
						width: 200,
						allowBlank: false,
						blankText: 'Choississez le champ a agréger',
						tpl: '<tpl for="."><div ext:qtip="{desc}" class="x-combo-list-item">{desc}</div></tpl>',
						emptyText : 'Choisissez un champ',
						valueNotFoundText: 'Choisissez un champ',
						mode : 'local',
						triggerAction : 'all',
						displayField : 'desc',
						valueField : 'value',
						editable: false,
						id: 'fieldAgregateFieldCombo',
						store: 'viewFieldNameStore',
						fieldLabel: 'Champ',
						labelSeparator: ' : ',
						labelStyle: 'width : 200px'
					},
					{
						xtype: 'combo',
						width: 200,
						allowBlank: false,
						blankText: 'Choississez la fonction d\'agrégation',
						tpl: '<tpl for="."><div ext:qtip="{functionDesc}" class="x-combo-list-item">{functionName}</div></tpl>',
						emptyText : 'Choisissez une fonction',
						valueNotFoundText: 'Choisissez une fonction',
						mode : 'local',
						triggerAction : 'all',
						displayField : 'functionName',
						valueField : 'value',
						editable: false,
						id: 'fieldAgregateAgregationCombo',
						store: 'viewAgregateNameStore',
						fieldLabel: 'Fonction de regroupement',
						labelSeparator: ' : ',
						labelStyle: 'width : 200px'
					}
				]
			}
        ];
        this.addButton({
			xtype: 'button',
			height: 22,
			width: 51,
			text: 'Annuler',
			handler : function() { this.hide(); },
			scope : this
        });
        this.addButton({
			xtype: 'button',
            height: 22,
			width: 51,
			text: 'Valider',
			handler : function() { this.hide(); },
			scope : this
        }); 
        groupingEditionAddFieldAgregateWindowUi.superclass.initComponent.call(this);
    },
    
    setCancelCallback : function(fn, scope) {
		this.buttons[0].purgeListeners();
		this.buttons[0].setHandler(fn, scope);
	},
	
	setValidateCallback : function(fn, scope) {
		this.buttons[1].purgeListeners()
		this.buttons[1].setHandler(fn, scope);
	},
	
	getFieldCombo : function() {
		return this.findById('fieldAgregateFieldCombo');
    },

	getAgregateCombo : function() {
		return this.findById('fieldAgregateAgregationCombo');
    },
    
    getForm : function() {
		return this.findById('fieldAgregateAdd_formPanel').getForm();
	}
    

});

Carmen.View.ExpressionPanel = Ext.extend(Ext.form.FormPanel, {
		// initial config used to instantiate the component
		config : {
			width : 590,
			height : 530,
			frame : true,

			labelWidth : 150,
			hideLabels: false,
			labelAlign: 'top',   
			labelPad: 8,   
			layout: 'table',
			layoutConfig: { columns:2 },
			items : [],
			buttons : [
				{ 
					text : 'Annuler'
				},
				{ 
					text : 'Effacer'
				},

				{ 
					text : 'Valider'			
				}
			]
		},
		
		expressionAreaCfg : {
			name: 'expression',
			blankText : 'Vous devez saisir une expression pour le champ',
			fieldLabel: 'Expression',
			allowBlank : false,
			editable: true,
			colspan : 2,
			emptyText: 'Saisissez la définition du champ ou construisez la au moyen de l\'interface...',
			width: 570,
			height: 150
		},
		
		// fields listbox as a grid panel...
		fieldDescStoreCfg : {
			fields : [
				{ name : "value", convert : function(v,r) { return '['  + r.fieldname + ']';} },
				"fieldname", 
				"type"
			],
			root : "data",
			idProperty : "fieldname",
			successProperty : "success"
		},
		
		fieldDescGridPanelCfg : {
			columns: [
				{header: "Champ", width: 180, sortable: false, menuDisabled : true, dataIndex : "fieldname"},
				{header: "Type", width: 65, sortable: false, menuDisabled : true, align : 'right', dataIndex : "type"}
			],
			enableColumnMove : false,
			height : 300,
			width : 280,
			frame : true,
			selModel: new Ext.grid.RowSelectionModel()
		},
		
		// operator listbox as a grid panel...
		operatorDescStoreCfg : {
			fields : [
				{ name : "operatorName", mapping : 0 },
				{ name : "value", mapping : 1 },
				{ name : "operatorDesc", mapping : 2 }
			],
			root : "data",
			//id : 0,
			successProperty : "success"
		},
		
		operatorDescGridPanelCfg : {
			columns: [
				{header: "Opérateur", width: 80, sortable: false, menuDisabled : true, dataIndex : "operatorName"}
			],
			enableColumnMove : false,
			height : 300,
			width : 115,
			frame : true,
			selModel: new Ext.grid.RowSelectionModel()
		},
		
		
		applyRec :  function (receiver, sender) {
			var res = {};
			for (p in receiver) {
				if (p in sender) {
					if (Ext.isObject(sender[p]) && Ext.isObject(receiver[p])) {
						res[p] = this.applyRec(receiver[p], sender[p]);
					}
					else if (Ext.isArray(sender[p]) && Ext.isArray(receiver[p])) {
						res[p] = [];
						for (var i=0; i<receiver[p].length; i++)
							res[p].push(receiver[p][i]);
						for (var i=0; i<sender[p].length; i++)
							res[p].push(sender[p][i]);
					}
					else	
						res[p] = sender[p];
				}
				else
					res[p] = receiver[p];
			}
			return res;
		},
		
		constructor : function(cfg) {
			// completing config object...
			if ("buttons" in cfg) {
				for (var i=0; i<cfg.buttons.length; i++) {
					this.config.buttons.push(cfg.buttons[i]);
				}
				delete cfg.buttons;
			}
			Ext.apply(this.config, cfg);
			Carmen.View.ExpressionPanel.superclass.constructor.apply(this, [this.config]);
			
			// events 
			this.addEvents(
				"cancel",
				"check",
				"clear",
				"validate");
			
			// specific store configs
			Ext.apply(this.operatorDescStoreCfg, this.config.operatorStore);
			Ext.apply(this.fieldDescStoreCfg, this.config.fieldStore);
			if ("expressionArea" in this.config)
				Ext.apply(this.expressionAreaCfg, this.config.expressionArea);
			
			// stores
			this.fieldDescStore = new Ext.data.JsonStore(this.fieldDescStoreCfg);
			this.operatorDescStore = new Ext.data.JsonStore(this.operatorDescStoreCfg);
			// and panels
			this.fieldDescGridPanel = new Ext.grid.GridPanel(
				Ext.apply(this.fieldDescGridPanelCfg, 
					{ store : this.fieldDescStore })
			);
			this.operatorDescGridPanel = new Ext.grid.GridPanel(
				Ext.apply(this.operatorDescGridPanelCfg, 
					{ store : this.operatorDescStore })
			);
			this.expressionArea = new Ext.form.TextArea(this.expressionAreaCfg);
			
			// structuring layout
			this.add(this.fieldDescGridPanel);
			this.add(this.operatorDescGridPanel);
			this.add(this.expressionArea);			
			
			// handlers...
			// button clear
			this.buttons[1].setHandler(
				function() {
					this.fireEvent("clear", this);
					this.clearExpression();
				}, this);
			// click events in gridPanels	
			var addToExpresssionArea = 	function(grid, rowIndex, colIndex, evt) {
					var store = grid.getStore();
					var rec = store.getAt(rowIndex);
					var expression = rec.data.value;
					var newExp = this.expressionArea.getValue() + expression;
					this.expressionArea.setValue(newExp);
					this.expressionArea.focus();
				};
			this.fieldDescGridPanel.on('celldblclick', addToExpresssionArea, this); 
			this.operatorDescGridPanel.on('celldblclick', addToExpresssionArea, this); 
			// button validate
			this.buttons[2].setHandler(
				function() { this.fireEvent("validate", this, this.getExpression()); }, 
				this);
			// button cancel
			this.buttons[0].setHandler(
				function() { this.fireEvent("cancel", this); }, 
				this);
				
		},
		
		initComponent : function() {
			Carmen.View.ExpressionPanel.superclass.initComponent.apply(this);
		},
		
		clearExpression : function() {
			this.setExpression("");
		},
		
		getExpression : function() {
			return this.expressionArea.getValue();
		},
		
		setExpression : function(newExp) {
			this.expressionArea.setValue(newExp);
		},
		invalidateExpression : function() {
			this.getForm().findField("expression").markInvalid();
		},
		
		getFieldStore : function() {
			return this.fieldDescStore;
		},
		
		getOperatorStore : function() {
			return this.operatorDescStore;
		}
});
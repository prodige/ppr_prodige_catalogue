'use strict';

angular.module('raw', [
  'ngRoute',
  'ngAnimate',
  'ngSanitize',
  'raw.filters',
  'raw.services',
  'raw.directives',
  'raw.controllers',
  'mgcrea.ngStrap',
  'ui',
  'colorpicker.module',
  'ngFileUpload'
])

//.constant('APP_NAME', 'Leaning Turn Demo App')


.config(['$routeProvider','$locationProvider', function ($routeProvider,$locationProvider) {

  $routeProvider.when('/:mode'      , {templateUrl: RESSOURCES_PATH + 'partials/main.html' , controller: 'RawCtrl'});

  //$routeProvider.otherwise({redirectTo: '/'});

  $locationProvider.html5Mode(true);

}]);
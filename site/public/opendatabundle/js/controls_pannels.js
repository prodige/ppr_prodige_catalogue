$(function () { $('#datetimepicker1').datetimepicker();   });


$(function() { $('#datetimepicker3').datetimepicker({format: 'HH:mm'});     });

function updateDiv(){ 
    $("#PDM_params" ).load(window.location.href + " #PDM_params" );
}

function change_input_border_color(id) {
    var current_input = document.getElementById(id); 
    var current_value = current_input.value; 
    current_input.style.borderColor  = "orange";
    
    if(current_input.id === "URL_DCAT" || current_input.id ==="API_url") {
        if( current_value.startsWith("https://") || current_value.startsWith("http://") ) {
            current_input.style.borderColor  = "#B5E655";
        } 
    } else {
        current_input.style.borderColor  = "#B5E655";
    }
    
}

function get_log_file (command_id) {
    
    var current_url = window.location.href; 
    var offset = 0; 
    var div_str = "";
    var url  = ""; 
    url = current_url + "/logfile/"+command_id; 
    if(current_url.indexOf('#') !== -1) {
        offset = current_url.indexOf('#'); 
        div_str = current_url.substring(offset); 
        current_url = current_url.replace(div_str, ''); 
        url = current_url + "/logfile/"+command_id; 
    } 

    return url; 
}


function reload_select_orga(node_id, array_orga, store_orga) {

    var selected_value = new Array();
    for (var i = 0; i < array_orga.length; i++) {
        
            for (var j = 0; j < store_orga.length; j++){
                store_orga[j] = store_orga[j].replace(" ", "");
                if(array_orga[i].id === store_orga[j]) {

                    selected_value.push(array_orga[i].id);
                    $("#"+node_id+"FormControlSelecOrganisation" ).val(selected_value); 
                } 
            }            
    }   
}

function empty_select_orga(array_orga, me_value) {

    for (var i = 0; i < array_orga.length; i++) {
            if(array_orga[i].name.includes(me_value)) {
                document.getElementById('orga_'+array_orga[i].id).hidden = false; 
            } else {
                document.getElementById('orga_'+array_orga[i].id).hidden = true; 
            }
    }     
    
}


function load_select_orga(node_id, array_orga, noeud) {
    
    var stored_orga = noeud.node_params; 
    var selected_value = new Array();
    if(stored_orga !==undefined) {
        for (var i = 0; i < array_orga.length; i++) {
            for (var j = 0; j < stored_orga.length; j++){
                for(var t = 0 ; t< stored_orga[j].value.length; t++) {
                    stored_orga[j].value[t] = stored_orga[j].value[t].replace(" ", "");
                    if(array_orga[i].id === stored_orga[j].value[t]) {
                        selected_value.push(array_orga[i].id);
                        $("#"+node_id+"FormControlSelecOrganisation" ).val(selected_value);    
                        break ; 
                    }
                }
            }
        }          
    } else {
        for (var i = 0; i < array_orga.length; i++) {
            selected_value.push(array_orga[i].id);
            $("#"+node_id+"FormControlSelecOrganisation" ).val(selected_value);                
        }
    }

}


function check() {
    var is_checked = document.getElementById("exampleCheck1").checked ;
    var my_pann = document.getElementById("hidded_pan"); 
    my_pann.hidden =true; 
    if(is_checked) {
        my_pann.hidden =false; 
    }
}

function show_params(){
    var hidded_contens = document.getElementById("hidded_pan_contents").hidden; 
    var my_pann =  document.getElementById("hidded_pan_contents"); 
    my_pann.hidden = true;
    if(hidded_contens){
        my_pann.hidden = false; 
    } 
}

function show_exec_day(){
    var freq_exec = document.getElementById("ExecFrequencePeriod").value; 
    var my_pann = document.getElementById("ExecFrequenceDayPannel"); 
    my_pann.hidden = true; 
    if(freq_exec === 'hebdomadaire') {
        my_pann.hidden = false; 
    } 
}

function show_pannel_moissonge(){
    document.getElementById("main_PDM").hidden = false; 
    document.getElementById("main_report").hidden = true;
    document.getElementById("harves_overview_result").hidden = true; 
}

function show_pannel_report(){
    document.getElementById("main_report").hidden = false;
    document.getElementById("harves_overview_result").hidden = true; 
    document.getElementById("main_PDM").hidden = true;
}

function hide_pannel_param_general(){
    var pannel_is_hidde = document.getElementById("main_PDM_general_contents").hidden;
    var main_PDM_general_contents = document.getElementById("main_PDM_general_contents"); 
    main_PDM_general_contents.hidden = true;
    if(pannel_is_hidde){
        main_PDM_general_contents.hidden = false;
    } 
}

function hide_pannel_param_API() {
    var pannel_is_hidde = document.getElementById("main_PDM_API_contents").hidden;
    var main_PDM_API_contents = document.getElementById("main_PDM_API_contents"); 
    main_PDM_API_contents.hidden = true;
    if(pannel_is_hidde){
        main_PDM_API_contents.hidden = false;
    }     
}

function hide_pannel_planifI() {
    var pannel_is_hidde = document.getElementById("main_PDM_planif_contents").hidden;
    var main_PDM_planif_contents = document.getElementById("main_PDM_planif_contents"); 
    main_PDM_planif_contents.hidden = true; 
    if(pannel_is_hidde){
        main_PDM_planif_contents.hidden = false;
    }        
}

function back_to_liste(){
    document.getElementById("PDM_params").hidden = true;    
    document.getElementById("add_or_edit_ko").hidden = true;   
}

function open_PDM_params( light_nodes, node = undefined, array_orga, store_orga){
    if(node !== undefined) {
        
        document.getElementById("node_id").value = node.pk_node_id;//
        document.getElementById("catalogue_name").value = node.node_name;
        document.getElementById("catalogue_name").style.borderColor  = "#B5E655";
        document.getElementById("API_url").value = node.api_url;
        document.getElementById("API_url").style.borderColor  = "#B5E655";
        
        document.getElementById("URL_DCAT").value = node.url_dcat;
        document.getElementById("URL_DCAT").style.borderColor  = "#B5E655";
        
        document.getElementById("affiche_last_execution").hidden=false;
        document.getElementById("affiche_filtres").hidden=false;
        document.getElementById("affiche_filtres_orga").hidden=true;
        document.getElementById("last_modif").value = node.node_last_modification;
        document.getElementById("planif_hour").value = node.node_planif_time;
        if(node.node_planif_time === undefined) {
            document.getElementById("planif_hour").value = "12:00";
        }
        load_select_orga(node.pk_node_id, array_orga, store_orga); 
        /* node.logo */
        show_associated_params_select (+node.pk_node_id); 
        /* reboucler le logo */
        for(var t = 0 ; t < light_nodes.length; t++) {
            if(light_nodes[t]===node.node_logo) {
                node.pk_node_id = light_nodes[t-1]; 
            }
        }
        var registred_logo = "logo"+node.node_logo; 
        set_active_logo("logo"+node.node_logo, registred_logo); 
                
        if(node.node_exec_frequency==="weekly") {
            document.getElementById("ExecFrequencePeriod").value = "hebdomadaire"; 
            document.getElementById("FormControlSelectExecDay").value = node.node_exec_frequency_day;
            document.getElementById("ExecFrequenceDayPannel").hidden = false; 
        } else {
            document.getElementById("ExecFrequencePeriod").value = "quotidienne"; 
            document.getElementById("ExecFrequenceDayPannel").hidden = true; 
            document.getElementById("FormControlSelectExecDay").value = "";
        }
        
    } else {
        document.getElementById("node_id").value = "";
        document.getElementById("planif_hour").value = "12:00";
        document.getElementById("catalogue_name").value = "";
        document.getElementById("catalogue_name").style.borderColor  = "orange";
        document.getElementById("API_url").value = "https://www.data.gouv.fr/api/1/datasets/";
        change_input_border_color('API_url'); 
        document.getElementById("URL_DCAT").value = "https://www.data.gouv.fr/datasets/";
        change_input_border_color('URL_DCAT'); 
        
        document.getElementById("affiche_last_execution").hidden=true; 
        document.getElementById("affiche_filtres").hidden=true;
        document.getElementById("affiche_filtres_orga").hidden=false;//
        document.getElementById("NewSelectParamsListe").hidden=false;
        document.getElementById("last_modif").value = "";
        document.getElementById("ExecFrequenceDayPannel").hidden = true; 
        document.getElementById("ExecFrequencePeriod").value = "quotidienne"; 
        
        var elements = document.querySelectorAll('div[id*="logo/"]');
        for (var i = 0; i < elements.length; i++) {
            elements[i].style.borderColor = "white"; 
                document.getElementById(elements[i].id+'label').innerText = 'logo disponible'; 
                document.getElementById(elements[i].id+'label').style.fontWeight ="normal"; 
                elements[i].style.opacity = "1";
        }         
    }
    document.getElementById("PDM_params").hidden = false;
    document.getElementById("harves_overview_result").hidden = true; 
    location.href = "#PDM_params";
}


function check_all_requireds_fields_and_save(exec = false){
    /* ---------------- 
     * test sur chaque champ : à améliorer
     * */
    
    var field_catalogue_name = document.getElementById("catalogue_name").value; 
    var field_API_url = document.getElementById("API_url").value; 
    var field_url_dcat = document.getElementById("URL_DCAT").value; 
    var field_planif_hour = document.getElementById("planif_hour").value; 
    
    var redirected_div = "add_or_edit_ko"; 
    var result = false; 
    var check_url = true;
    var node_logo  =  ""; 
    
    if(field_catalogue_name !== "" && field_API_url !== "" && field_planif_hour !==""){
        var elements = document.querySelectorAll('div[id^="logo"]');
        if(!(field_API_url.startsWith("https://")) && !(field_API_url.startsWith("http://"))) {
            check_url = false; 
            document.getElementById("API_url").style.borderColor="orange"; 
        } 
        if(!(field_url_dcat.startsWith("https://")) && !(field_url_dcat.startsWith("http://"))) {
            check_url = false; 
            document.getElementById("URL_DCAT").style.borderColor="orange"; 
        }         
        for (var i = 0; i < elements.length; i++) {
            if(elements[i].style.borderColor!=="white") {
//                console.log(elements[i]);
                node_logo  = elements[i].id.replace("logo","");
            }
        } 
        if(node_logo !== "" && check_url) {
            back_to_liste(); 
            redirected_div = "add_or_edit_ok"; 
            result = true;             
        }

    } else {
       document.getElementById("API_url").style.borderColor="orange"; 
       document.getElementById("URL_DCAT").style.borderColor="orange"; 
    }
    
    if(exec==="false") {
        location.href = "#"+redirected_div;
        if(check_url) {
            setTimeout(function() {
                window.location.reload();
                location.href = "#";
            }, 2000);        
        } 
    } else {
        redirected_div = "harves_add_or_edit_ko"; 
        if(node_logo !== "" && check_url) {
            redirected_div = "harves_add_or_edit_ok"; 
            result = true;    
            location.href = "#"+redirected_div;
        }
    }
    document.getElementById(redirected_div).hidden = false;  

    return result; 
}

function check_all_requireds_fields_save_and_exec(){
    check_all_requireds_fields_and_save(); 
}



function add_elements() {
    var wrapper = document.getElementById("hidded_pan_contents"); //Fields wrapper
    var nb_input = wrapper.getElementsByTagName("INPUT").length; 
    
    var ul_indice = (nb_input / 2)+1; 
    var ul_id = "liste"+ul_indice; 
    var id_key="param_key"+ul_indice;
    var id_value="param_value"+ul_indice; 
    wrapper.insertAdjacentHTML('beforeend','<ul class="list-inline" id='+ul_id+'><li><label>Clé :</label></li><li><input id='+id_key+' type="text" class="form-control" placeholder ="key '+ul_indice+'"/></li><li><label>Valeur :</label></li><li><input id='+id_value+'  type="text" class="form-control" placeholder ="value '+ul_indice+'"/></li></ul>');
    
    if(ul_indice===2){
        var html_button_remove = '<li><button id="remove_elements_button" type="button" class="btn btn-danger" onclick="remove_elements()">Supprimer le dernier ensemble clé valeur ajouté</button></li>';
        var wrapper_controls = document.getElementById("control_buttons"); 
        wrapper_controls.insertAdjacentHTML('beforeend',html_button_remove);         
    }
}



function remove_elements(){
    var wrapper = document.getElementById("hidded_pan_contents"); //Fields wrapper
    var nb_ul = wrapper.getElementsByTagName("UL").length; 
    if(nb_ul>1){
        var ul_to_delete = document.getElementById("liste"+nb_ul); 
        ul_to_delete.remove(); 
        if(nb_ul===2){
            var button_to_delete = document.getElementById("remove_elements_button"); 
            button_to_delete.remove(); 
        }
    } 
}


function set_active_logo(id_logo, registred_logo = "" ){
    var elements = document.querySelectorAll('div[id^="logo"]');
    var registred_logo_id = registred_logo+'label'; 
    var label_logos = ""; 
    
    for (var i = 0; i < elements.length; i++) {
        
        if(registred_logo !== "" ) {
            label_logos = +elements[i].id+'label'; 
            if(label_logos!== registred_logo_id) {
                document.getElementById(elements[i].id+'label').innerText = 'logo disponible'; 
                document.getElementById(elements[i].id+'label').style.fontWeight ="normal"; 
            }
        }   
        elements[i].style.borderColor  = "white";
        elements[i].style.opacity = "0.45";
    }    
    
    var my_pann = document.getElementById(id_logo); 
    if(my_pann !== null){
        my_pann.style.borderWidth  = "thick";
        my_pann.style.borderColor  = "#01B0F0";
        my_pann.style.opacity = "1";           
    }

    if(typeof(registred_logo) !== 'object') {
        if(registred_logo !== "") {
            document.getElementById(registred_logo+'label').innerText = 'logo actuel'; 
            document.getElementById(registred_logo+'label').style.fontWeight ="bolder"; 
        }
    }   
    
}

function get_logo(nodes) {
    var elements = document.querySelectorAll('div[id^="logo"]');
    
    var new_node_logo  = ""; 
    for (var i = 0; i < elements.length; i++) {
        if(elements[i].style.borderColor!=="white") {
            new_node_logo = elements[i].id.replace("logo","");
        }
    }     
    return new_node_logo; 
}
function get_api_params(node_id="") {
    
    var values_input = document.querySelectorAll('input[id^="param_value"]');
    var keys_input = document.querySelectorAll('input[id^="param_key"]');
    var params = new Array(); 
    var i = 0;
    var k = 0 ; 
    for (i = 0; i < values_input.length; i++) {     
        if(values_input[i].value !=="" && keys_input[i].value !== "") {
            params[k] = new Array();
            params[i].push(keys_input[i].value, values_input[i].value); 
            k = k+1; 
        }
    }    
    
    if(node_id === "") {
        var j = k  ;   
        params[j] = new Array(); 
        var organisations = $('#NewFormControlSelecOrganisation').val();
        if(typeof(organisations)!=="string" && organisations !== null) {
            organisations = organisations.toString(); 
        } else {
            organisations = "";
        }
        params[j].push("organization", organisations);
    } else {
        var j = k  ; 
        params[j] = new Array(); 
        var organisations = $('#'+node_id+'FormControlSelecOrganisation').val();
        if(typeof(organisations)!=="string" && organisations !== null && organisations !== undefined) {
            organisations = organisations.toString(); 
        } else {
            organisations = "";
        }
        params[j].push("organization", organisations);
    }
    return params; 
}


function show_associated_params_select (node_id) {    
    var all_select = document.querySelectorAll('div[id*="SelectParamsListe"]');
    for (var i = 0; i < all_select.length; i++) {
        all_select[i].hidden = true; 
    }    
    var select = document.querySelectorAll('div[id^="'+node_id+'SelectParamsListe"]');
    for (var i = 0; i < select.length; i++) {
        select[i].hidden=false; 
    }      
}

function add_api_param_value( fk_id, key_label) {
    document.getElementById(fk_id+'NewValueDiv'+key_label).hidden=false;
}

function hide_api_new_param_value(fk_id,key_label ) {
    document.getElementById(fk_id+'NewValueDiv'+key_label).hidden=true;
    document.getElementById(fk_id+'New_Value_'+key_label).value="";
}

function add_new_param_value( fk_id, key_label) {
    
    var new_value = document.getElementById(fk_id+'New_Value_'+key_label).value; 
    var params = new Array(fk_id, key_label, new_value); 
    if(new_value !== "") {
        
        that = $(this);
        $.ajax({
            url: Routing.generate('post_catalogue_opendata_harvester_nodes'), 
            type :"POST",
            dataType: "json",
            data: {
                "mode": "add_new_param_value",
                "api_params":params
            },
            async: true,
            success: function () {
                window.location.reload();
            }, 
            failure: function(){
                console.log("postfailure"); 
            }
        });        
    }
}
/* -----------------------------------------------------
 * *
 * 
 *      AJAX 
 * 
 * 
 * ------------------------------------------------------ */
function insert_or_patch_node(nodes, exec = 'false'){
    
    var result = check_all_requireds_fields_and_save(exec); 
    if(result) {
        if(exec === 'true') {
            prepare_harves(nodes); 
        }
        
        /* récupération des champs */
        var new_node_id = document.getElementById('node_id').value;                             // node_id 
        var new_node_name = document.getElementById('catalogue_name').value;                    // catalogue name     
        var new_node_logo = get_logo(nodes);                                                    // catalogue logo     
        var new_node_api_url = document.getElementById('API_url').value;                        // API url      
        var new_node_url_dcat = document.getElementById('URL_DCAT').value; 
        var new_node_exec_period_french = document.getElementById('ExecFrequencePeriod').value; //execution period (weekly / daily)
        var new_node_exec_period =""; 
        if(new_node_exec_period_french==="quotidienne") {
           new_node_exec_period = "daily" ;
        } else {
           new_node_exec_period = "weekly" ;
        }
        var new_node_exec_day = document.getElementById('FormControlSelectExecDay').value;        //execution day (can be null)        
        var new_node_exec_time = document.getElementById('planif_hour').value;                  //execution hour         
        
        var source_uuid =""; 
        for (var i = 0; i < nodes.length; i++) {
            if(nodes[i].pk_node_id === parseInt(new_node_id) ) {
                source_uuid = nodes[i].uuid_source; 
            }
        }  
        
        var params = get_api_params(new_node_id); 
        var api_params = new Array();
        var node_config = [new_node_name,new_node_logo,new_node_api_url, new_node_url_dcat, new_node_id, api_params, source_uuid,new_node_exec_period,new_node_exec_day, new_node_exec_time ]; 
        that = $(this);
        $.ajax({
            url: Routing.generate('post_catalogue_opendata_harvester_nodes'), 
            type :"POST",
            dataType: "json",
            data: {
                "node_config": node_config,
                "api_params":params, 
                "exec": exec
            },
            async: true,
            success: function (data) {
                if(exec === "true") {          
                    quit_harves(data, nodes);                                       
                }
            }, 
            failure: function(){
                console.log("postfailure"); 
            }
        });
    } 

}
// suppression d'un noeud de moissonnage et de l'ensemble de ces paramètres
function remove_node(node_id) {
    var submit = confirm("Attention, après la suppression est définitive");
    // passer les boutons "éditer" et "supprimer" en disabled 
    var edit_buttons = document.querySelectorAll('button[id*="edit_"]');
    var del_buttons = document.querySelectorAll('button[id*="del_"]');
    for (var i = 0; i < edit_buttons.length; i++) {
        edit_buttons[i].disabled = true; 
        del_buttons[i].disabled = true; 
    }   
     
    
    if(submit) {
        that = $(this);
        $.ajax({
            url: Routing.generate('delete_catalogue_opendata_harvester_nodes'), 
            type :"DELETE",
            dataType: "json",
            data: {"node_id": node_id},
            async: true,
            success: function (data) {
                window.location.reload();
            }, 
            failure: function(){
                console.log("delete failure"); 
            }
        });            
    } 
}
// suppression d'un paramètre 
function remove_url_param(pk_param_id, fk_node_id) {
    that = $(this);
    $.ajax({
        url: Routing.generate('delete_catalogue_opendata_harvester_nodes'), 
        type :"DELETE",
        dataType: "json",
        data: {
            "pk_param_id": pk_param_id,
            "fk_node_id": fk_node_id
        },
        async: true,
        success: function (data) {
            document.getElementById(fk_node_id+'FormControlSelecOrganisation'+pk_param_id).value = ""; 
            document.getElementById(fk_node_id+'SelectParamsListe'+pk_param_id).hidden = true; 
        }, 
        failure: function(){
            console.log("delete failure"); 
        }
    });             
}


function prepare_harves(nodes) {

    
    var node_id = document.getElementById('node_id').value;                             // node_id 
    var node_name = document.getElementById('catalogue_name').value;                    // catalogue name     
    var node_logo = get_logo(nodes);                                                    // catalogue logo     
    var node_api_url = document.getElementById('API_url').value;                        // API url      
    var node_url_dcat = document.getElementById('URL_DCAT').value;     
        
    document.getElementById("harves-title").innerText = node_name; 
    document.getElementById("harves-subtitle").innerText = node_api_url; 
    
    // les boutons d'ajout et d'éditions et suppression de noeuds sont désactivés
    document.getElementById("button_add_node").disabled = true; 
    
    for (var i = 0; i < nodes.length; i++) {
        document.getElementById("edit_"+nodes[i].pk_node_id).disabled = true; 
        document.getElementById("del_"+nodes[i].pk_node_id).disabled = true; 
    }  
    
    // main_load_harves
    document.getElementById("main_load_harves").hidden = false; 
    // main_PDM
    document.getElementById("PDM_params").hidden = true; 
    
    
}
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function quit_harves(data, nodes) {

        
    document.getElementById("harves_overview_http_code").innerText =""; 
    document.getElementById("harves_overview_status").innerText = ""; 
    document.getElementById("harves_overview_message").innerText =""; 
    document.getElementById("harves_overview_finish_time").innerText = ""; 
    document.getElementById("harves_overview_data_count").innerText = ""; 
    document.getElementById("harves_overview_data_count_success").innerText = ""; 
    document.getElementById("harves_overview_data_count_ignore").innerText = ""; 
    document.getElementById("harves_overview_data_count_update").innerText = ""; 
    document.getElementById("harves_overview_data_count_insert").innerText = ""; 

//    console.log(data); 
    
    return_json = isJson(data["output"]); 
    if(return_json) {
        data_object = JSON.parse(data["output"]); 
        if(data_object["success"] === "success") {

            document.getElementById("main_load_harves").hidden = true; 
            document.getElementById("harves_add_or_edit_ok").hidden = true; 
            document.getElementById("harves_add_or_edit_ko").hidden = true; 

            document.getElementById("harves_overview_result").hidden = false; 

            restore_init_stat (nodes); 
            var node_id = document.getElementById('node_id').value;                             // node_id 
            var node_name = document.getElementById('catalogue_name').value;                    // catalogue name     
            var node_logo = get_logo(nodes);                                                    // catalogue logo     
            var node_api_url = document.getElementById('API_url').value;                        // API url      
            var node_url_dcat = document.getElementById('URL_DCAT').value;   

            document.getElementById("harves-title-report").innerText = node_name; 

            document.getElementById("harves_overview_result_url_api").innerText = node_api_url; 
            document.getElementById("harves_overview_result_url_dcat").innerText = node_url_dcat; 

            document.getElementById("harves_overview_http_code").innerText = data_object["http_code"]+" "+data_object["OK"]; 
            document.getElementById("harves_overview_status").innerText = data_object["command_success"]; 
            document.getElementById("harves_overview_message").innerText = data_object["message"]; 
            document.getElementById("harves_overview_finish_time").innerText = data_object["finish_time"]; 
            document.getElementById("harves_overview_data_count").innerText = data_object["data_count"]; 
            document.getElementById("harves_overview_data_count_success").innerText = data_object["data_nb_success"]; 
            var precisions = "";
            if(data_object["data_ignored"] !==0) {
                precisions = " (fiches présentes dans le catalogue local et non modifiées depuis le dernier import)"; 
            }
            document.getElementById("harves_overview_data_count_ignore").innerText = data_object["data_ignored"] +precisions; 
            document.getElementById("harves_overview_data_count_update").innerText = data_object["nb_update"]; 
            document.getElementById("harves_overview_data_count_insert").innerText = data_object["nb_insert"]; 

            document.getElementById(node_logo).hidden = false; 

            location.href = "#harves_overview_result";

        } 
    } else {
        
        console.log("not a valid json string");
        
        document.getElementById("main_load_harves").hidden = true; 
        document.getElementById("harves_add_or_edit_ok").hidden = true; 
        document.getElementById("harves_add_or_edit_ko").hidden = true; 
        
        document.getElementById("harves_info_fail").hidden = false; 
    }
    
    
    

}


function restore_init_stat (nodes) {
    document.getElementById("button_add_node").disabled = false; 
    
    for (var i = 0; i < nodes.length; i++) {
        document.getElementById("edit_"+nodes[i].pk_node_id).disabled = false; 
        document.getElementById("del_"+nodes[i].pk_node_id).disabled = false; 
    }  
}


function close_harves_overview() {
   document.getElementById("harves_overview_result").hidden = true;  
   window.location.reload();
}

(function(){
  /** Définition des lot de données*/
  Ext.define('Standard.model.Standard', {
    extend: 'Ext.data.Model',
    fields: [
      {name: 'id',    type: 'int'},
      {name: 'name',  type: 'string' },
    ],
    proxy: {
      type: 'rest',
      idParam : 'id',
      url : Routing.generate('standard_api_qualite') + 'list',
      actionMethods : {read: 'GET' },
      reader: {
        type: 'json',
        rootProperty: 'Standard'
      }
    },
  });

  /**  Le store de Standard */
  Ext.define('Standard.store.Standard', {
    extend: 'Ext.data.Store',
    alias: 'Standard.store.Standard',
    model: 'Standard.model.Standard'
  });

  Ext.StoreMgr.lookup('store.Standard') || Ext.create('Standard.store.Standard',{storeId : 'store.Standard'});

  
})();
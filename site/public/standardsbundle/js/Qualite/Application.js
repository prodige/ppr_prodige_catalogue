(function(){
  // Pour charger le fichier
  Ext.define('Standard.Application', {
    alias: 'Standard.Application'
  });
})();

function Application(){
  var _mainPanel = null;

  function setMainPanel(mainPanel){
    _mainPanel = mainPanel;
   
  }

  // Application ExtJs
  Ext.define('Standard.MainApplication', {
    extend : 'Ext.app.Application',
    name : '_APPLICATION_',
    alias: "Standard.MainApplication",
  
    controllers: ['Standard.controllers.communCtrl'],

    launch: function() {
      if( _mainPanel ){
        var mainCtrl = this.getController('Standard.controller.mainCtrl');
       
        mainCtrl.mainPanel = _mainPanel;
        
        
      }
    }
  });

  return {
    setMainPanel: setMainPanel
  }
}
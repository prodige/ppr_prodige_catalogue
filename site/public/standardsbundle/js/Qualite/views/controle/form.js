(function(){
  var _uuid = null;
  var _isConforme = false;

  Ext.define('Standard.views.controle.form',{
    extend: 'Ext.form.Panel', 
    xtype: 'standard-views-controle-form',

    title: 'Simple Form',
    bodyPadding: 5,

    // The form will submit an AJAX request to this URL when submitted
    url: Routing.generate('standard_api_qualite') + 'analyse',
    
    // Fields will be arranged vertically, stretched to full width
    layout: 'anchor',
    defaults: {
      anchor: '100%'
    },
    initComponent : function(){
      this.callParent(arguments);

      console.log(_isConforme)
      
    },
    setConforme: function(isConforme, url){
      _isConforme = isConforme;

      //
      var button = Ext.getCmp('test_analyse');
      if(button){
        button.setDisabled(!url);
        button.url = url;
      }
      
      //
      buttonMaj = Ext.getCmp('maj_catalogue');
     
      if(buttonMaj ){
        buttonMaj.setDisabled(!_isConforme);  
      }
      console.log(_isConforme);
    },
    
    addUuid: function(uuid){
      this.add({
        name: 'uuid',                
        fieldLabel : "uuid",            
        allowBlank: true,  
        hidden: true,
        value: uuid
      });

      _uuid = uuid;
    },
    // The fields
    defaultType: 'textfield',
    // The fields
    items: [{
      name: 'standard',                
      fieldLabel : "Standard",            
      allowBlank: false,  
      xtype: "combo.standard.standard",
      listeners:{
        'select': function(){}
      }
    },{
      name: 'prefix',                
      fieldLabel : "Préfixe",            
      allowBlank: true,  
    },{
      name: 'suffix',                
      fieldLabel : "Postfixe",            
      allowBlank: true,  
    }],

   
    buttons: [{
      text: "Mettre à jour les catalogues d'attribut ",
      id: "maj_catalogue",
      disabled: true,
      handler: function(){
        $('#loading-spiner').show();
        Ext.Ajax.request({
          url: Routing.generate('standard_api_qualite')+ "update_catalogue/" + _uuid ,
          success: function(form, action) {
            $('#loading-spiner').hide();
            Ext.Msg.alert('Success', 'Catalogue mis à jour');
          },
          failure: function(form, action) {
            console.log(action)
            $('#loading-spiner').hide();
            try{
              Ext.Msg.alert('Failed', JSON.parse(action.response.responseText).msg );
            }catch(e){
              console.error(e);
              Ext.Msg.alert('Failed', "Erreur pendant l'analyse" );
            } 
          }
        });
      }
    },{
      text: "voir le rapport d'exécution",
      id: "test_analyse",
      disabled: true,
      url: null,
      handler: function(){
        if( this.url ){
          window.open(this.url, '_blank');
        }
      }
    },{
      text: "lançer l'analyse",
      formBind: true, //only enabled once the form is valid
      disabled: true,
      handler: function(){
        var form = this.up('form').getForm();
        if (form.isValid()) {
          $('#loading-spiner').show();
          form.submit({
            success: function(form, action) {
              console.log(Ext.getCmp('test_analyse'));
              Ext.Msg.alert('Success', '<a target="_blank" href="'+ action.result.url +'" > voir le rapport d\'exécution </a>');

              //
              var button = Ext.getCmp('test_analyse');
              if(button){
                button.setDisabled(false);
                button.url = action.result.url;
              }
              
              //
              buttonMaj = Ext.getCmp('maj_catalogue');
              console.log(!!buttonMaj,!!action.result.result, !!action.result.result.allTablesOk );
              if(buttonMaj && action.result.result  && action.result.result.allTablesOk){
                buttonMaj.setDisabled(false);  
              }

              $('#loading-spiner').hide();
            },
            failure: function(form, action) {
              console.log(action)
              $('#loading-spiner').hide();
              try{
                Ext.Msg.alert('Failed', JSON.parse(action.response.responseText).msg );
              }catch(e){
                console.error(e);
                Ext.Msg.alert('Failed', "Erreur pendant l'analyse" );
              } 
            }
          });
        }
      }
    }],
  });
})();
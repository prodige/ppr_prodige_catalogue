(function(){

  // Patch pour corriger un bug dans extjs qui empeche de bouger le cursor dans l'édition de champs de grille
  Ext.define('Ext.patch.EXTJS16166', {
    override: 'Ext.view.View',
    compatibility: '5.1.0',
    handleEvent: function(e) {
      var me = this,isKeyEvent = me.keyEventRe.test(e.type),
      nm = me.getNavigationModel();

      e.view = me;
      
      if (isKeyEvent) {
        e.item = nm.getItem();
        e.record = nm.getRecord();
      }

      // If the key event was fired programatically, it will not have triggered the focus
      // so the NavigationModel will not have this information.
      if (!e.item) {
        e.item = e.getTarget(me.itemSelector);
      }
      if (e.item && !e.record) {
        e.record = me.getRecord(e.item);
      }

      if (me.processUIEvent(e) !== false) {
        me.processSpecialEvent(e);
      }
      
      // We need to prevent default action on navigation keys
      // that can cause View element scroll unless the event is from an input field.
      // We MUST prevent browser's default action on SPACE which is to focus the event's target element.
      // Focusing causes the browser to attempt to scroll the element into view.
      
      if (isKeyEvent && !Ext.fly(e.target).isInputField()) {
        if (e.getKey() === e.SPACE || e.isNavKeyPress(true)) {
          e.preventDefault();
        }
      }
    }
  });

  // Chargement des stores
  var _storeStandard = Ext.StoreMgr.lookup('store.Standard') || Ext.create('Standard.store.Standard',{storeId : 'store.Standard'});

  // Chargement des xclass venant du menu
  function loadXClass(panel, callback){
    Ext.require(panel, function(){
      if(typeof callback === 'function'){
        callback(true);
      }
    })
  }

  /** */
  function initPanel(mainPanel){
    Ext.require('Standard.components.ComboStandard', function(){
      console.log("Test 59");
    });  
   
    _storeStandard.load();

    loadXClass('Standard.views.controle.form', function(){
      var item = {
        id: 'mainForm',
      }
    
      item.title = 'Contrôle de qualité';
      item.xclass = 'Standard.views.controle.form';

      mainPanel.add( item );

      var mainForm = Ext.getCmp('mainForm');

      if(mainForm){
        mainForm.addUuid(mainPanel.uuid);
        mainForm.setConforme(mainPanel.isConforme, mainPanel.repportUrl);
      }
    });

  }

  /**
   * Panel principal de l'application
   */
  Ext.define('Standard.views.mainPanel', {
    require: ['Standard.views.controle.form'],
    extend: 'Ext.panel.Panel',
    xtype: 'standard-view-mainPanel',
    id: 'mainPanel',
    //controller:  'communCtrl',

    border : true,
    
    width: '100%',

    renderTo: 'content',

    layout: 'fit',

    scrollable: false,

    listeners: {
      'render': function(){
        console.log("Main Panel ok");
        initPanel(this);
      }
    },
    initComponent : function(){
      this.callParent(arguments);
    },
    items : []
  });
})();
(function (){
  Ext.define('Standard.components.ComboControle',{
    alias: 'Standard.load.ComboControle'
  });

  // Chargement des stores
  var _storeStandard = Ext.StoreMgr.lookup('store.Standard') || Ext.create('Standard.store.Standard',{storeId : 'store.Standard'});
  
  // Combo des standards
  Ext.define('Controle.combobox.Stantard', {
    requires: ['Standard.model.Standard'],
    xtype: "combo.standard.standard",
    extend: "Ext.form.ComboBox",

    store: _storeStandard,
    forceSelection: true,
    queryMode: 'local',
    displayField: 'name',
    valueField: 'id'
  });
 
})();
function main(uuid, isConforme, repportUrl){
  'use strict'

  // PLACER ICI LES MODELS À CHARGÉE
  var Entity = [
    'Standard.model.Standard'
  ];

  // Autre fichier js important à charger
  var JsForLoad = [
   
    'Standard.views.mainPanel', 
    'Standard.Application'
  ];

  function initApplication(){
    Ext.onReady(function(){
      var mainPanel = Ext.create('Standard.views.mainPanel', {
        renderTo: 'content',
        layout: 'card',
        width: '30%',
        uuid: uuid,
        isConforme: isConforme,
        repportUrl: repportUrl
      });

      var qualiteApp = new Application();
      qualiteApp.setMainPanel(mainPanel);

      Ext.application('Standard.MainApplication' );

    }) 
  }

  // Chargement des classes principales
  function initLoader(){
    console.log("App init")
    Ext.Loader.setPath('Standard', document.location.origin+'/standardsbundle/js/Qualite');
    var requires = Entity.concat(JsForLoad);

    Ext.Loader.require(requires,  initApplication);

   ;
  }

  initLoader();

  $('#loading-spiner').hide();
};

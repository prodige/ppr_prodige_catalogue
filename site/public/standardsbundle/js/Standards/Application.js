/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('Standards.Application', {
  extend : 'Ext.app.Application',
  name : 'Standards',
  
  appFolder  : '/standardsbundle/js/Standards',
  paths : {'Standards.store' : '/standardsbundle/js/Standards/model', Standards : '/standardsbundle/js/Standards'},
  models : ['Standards.model.Fournisseur', 'Standards.model.Standard'],
  
  mainView: 'Standards.view.Main',
  
  views : ['Standards.view.Main'],
  
  stores : ['Standards.store.Standard'],
  
  launch : function(){
    Standards.getApplication().getStore('AllStandards');
  }
})
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function(){
  var _IsInitMetadatasIsInit = false;

  Ext.define('Standards.controller.Fournisseur', {
    extend : 'Ext.app.ViewController',
    alias : 'controller.fournisseur',
    
    openNewRecord : function(){
      var view = this.getView(),
          form = view.down('form'),
          grid = view.down('grid');

      var record = Ext.create('Standards.model.Fournisseur');
      form.loadRecord(record);
    },
    openRecord : function(btn){
      var view = this.getView(),
          form = view.down('form'),
          grid = view.down('grid'),
          container = btn.ownerCt,
          record = (container ? container.getWidgetRecord() : null);
      if ( record ){
        form.loadRecord(record);
      }
    },
    
    saveRecord : function(){
      var view = this.getView();
      var form = view.down('form');
      var grid = view.down('grid');
      var record = form.getRecord();

      if ( form.isValid() ){ 
        form.updateRecord(record);

        record.save({
          scope : this,
          success : function(record, operation){
            if ( operation instanceof Ext.data.operation.Create ){
              grid.getStore().add([record]);
            }
            record.commit();
            Ext.toast("Enregistrement réussi", "Gestion des fournisseurs", "t");
            this.closeEdition();
          },
          failure : function(){

          }
        })
      }
    },
    
    deleteRecord : function(btn){
      var self = this;
      var container = btn.ownerCt;
      var record = (container ? container.getWidgetRecord() : null);

      if ( record ){
        Ext.Msg.confirm("Suppression d'un fournisseur", "Confirmez-vous la suppression du fournisseur "+record.get('fournisseur_name')+" ?", 
          function(confirm){
            if ( confirm === "yes" ) {
              self.closeEdition();

              record.erase({
                success : function(record, operation){
                  record.commit();
                },
                failure : function(){
                  record.reject();
                }
              });
            }
          }
        );
      }
    },
    
    
    closeEdition : function(){
      var view = this.getView(),
          form = view.down('form'),
          grid = view.down('grid');

      form.loadRecord(null);
    },
    
    getExternalStandards : function(widget){
      widget.setDisabled(true);

      var url = this.lookupReference("fournisseur_url");

      if ( url.isValid() && !Ext.isEmpty(url = url.getValue()) ){

        var metadatas = this.lookupReference('metadatas');

        if ( !metadatas ){
          widget.setDisabled(false);
          return false;
        } 
       
        // Quand le chargement est finit
        $("#loading-spiner").show();
        if(!_IsInitMetadatasIsInit){
          _IsInitMetadatasIsInit = true;
          metadatas.getStore().on('load', function(store, records){
            metadatas.setVisible(true);
            $("#loading-spiner").hide();
            widget.setDisabled(false);
          });
        }
     

        // Chargement des metadata dans le store
        metadatas.getStore().getProxy().getReader().setRootProperty("metadata");
        
        url = Routing.generate('prodige_standards_fournisseur_search_standards') + "?" + Ext.urlEncode({'fournisseur' : url});
      
        metadatas.getStore().getProxy().setUrl(url);
        metadatas.getStore().load();
      }
    },
    onStandardErase: function(btnErase, fournisseur, external, btn){
      standard = Ext.create('Standards.model.Standard', {
        fournisseur_id : fournisseur.getId(),
        standard_name  : external.get('name'),
        standard_url   : external.get('URI')
      });
      
      btn.setDisabled(true);
      $("#loading-spiner").show();

      standard.save({
        success : function(self){
          Standards.store.Standard.getAllDataInstance().add(self);
          Standards.store.Standard.getAllDataInstance().remove(btnErase);
        },
        callback: function(){
          $("#loading-spiner").hide();
          btn.setDisabled(false);
        }
      });
      
    },
    loadExternalStandard : function(btn){
      var self = this;
      var view = this.getView();
      var form = view.down('form');
      var fournisseur = form.getRecord();
      if ( !fournisseur ) return;

      var external = btn.record;
      if ( !external ) return;
      
      var standard = null;
      if ( !external.get('exists') ){
        btn.setDisabled(true);
        standard = Ext.create('Standards.model.Standard', {
          fournisseur_id : fournisseur.getId(),
          standard_name  : external.get('name'),
          standard_url   : external.get('URI')
        });
      
        $("#loading-spiner").show();
        standard.save({
          success : function(self){
            Standards.store.Standard.getAllDataInstance().add(self);
          },
          callback: function(){
            $("#loading-spiner").hide();
            btn.setDisabled(false);
          }
        })
      }
      else if ( external.get('exists') ){
        var standard = Standards.store.Standard.getAllDataInstance().findRecord('standard_url', external.get('URI'));
        if(standard){
          standard.erase({
            success : function(btnErase){
              self.onStandardErase(btnErase, fournisseur, external, btn);
            }
          })
        }
        
      }
    },
    
    deleteStandardFromExternal : function(btn){
      var view = this.getView(),
          form = view.down('form'),
          grid = view.down('grid');

      var fournisseur = form.getRecord();
      if ( !fournisseur ) return;
      
      var external = btn.record;
      if ( !external ) return;
      
      Ext.Msg.confirm(
        "Suppression d'un standard"
      , "Confirmez-vous la suppression de ce standard et de tout son historique d'application ?"
      , function(confirm){
          if ( confirm != "yes" ) return;

          var standard = Standards.store.Standard.getAllDataInstance().findRecord('standard_url', external.get('URI'));
          if ( !standard ) return;

          standard.erase({
            success : function(self){
              Standards.store.Standard.getAllDataInstance().remove(self);
            }
          })
        }
      )
      
    }
  });
})();
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

__SESSION__ = (typeof __SESSION__!='undefined' ? __SESSION__ : Ext.create('Ext.data.Session'));


Ext.define('Standards.model.Standard', {
  extend : 'Ext.data.Model',
  mixins : ['Ext.mixin.Observable'],
  alternateClassName: ['Standards'],
  entityName : 'std.standard',
  
  proxy : {
    type : 'rest',
    url : Routing.generate('prodige_standards_standard_rest'),
    idParam : 'standard_id',
    actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
    reader: {
        type: 'json',
        rootProperty: 'standards'
    }
  },
  
  idProperty : 'standard_id',
  fields : [{name:'fournisseur_id', reference:'std.fournisseur'}, {name : 'standard_id', persist : false}, 'standard_name', 'standard_url']
});


Ext.define('Standards.store.Standard', {
  extend: 'Ext.data.Store',
  alias: 'std.store.standards',
  model: 'Standards.model.Standard',
  //sorters : [{property : 'fournisseur.fournisseur_name', direction : 'ASC'}, {property : 'standard_name', direction : 'ASC'}],
  session : __SESSION__,
  
  statics : {
    getAllDataInstance : function(){
      return Standards.getApplication().getStore('AllStandards');
    }
  }
});

Ext.define('Standards.store.AllStandards', {
  extend : 'Standards.store.Standard',
  autoLoad : true,
  session : __SESSION__
});


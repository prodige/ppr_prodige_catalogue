/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

__SESSION__ = (typeof __SESSION__!='undefined' ? __SESSION__ : Ext.create('Ext.data.Session'));

Ext.define('Standards.model.Fournisseur', {
  alternateClassName: ['Fournisseur'],
  extend : 'Ext.data.Model',
  entityName : 'std.fournisseur',
  
  proxy : {
    type : 'rest',
    url : Routing.generate('prodige_standards_fournisseur_rest'),
    idParam : 'fournisseur_id',
    actionMethods : { create: 'POST', read: 'GET', update: 'PATCH', destroy: 'DELETE' },
    reader: {
      type: 'json',
      rootProperty: 'fournisseurs'
    }
  },
  
  idProperty : 'fournisseur_id',
  fields : [
    {name : 'fournisseur_id', persist : false}
  , 'fournisseur_name'
  , 'fournisseur_url'
  ],
  
  hasMany : [{
    role : 'standards',
    name : 'standards',
    model : 'tandards.model.Standards',
    foreignKey : 'fournisseur_id',
    inverse : {role:'fournisseur', reference:'std.fournisseur', persist:false},
    persist:false,
    storeConfig : {sorters : [{property : 'standard_name', direction:'ASC'}], remoteFilter:false, session : __SESSION__}
  }],
});


Ext.define('Standards.store.Fournisseurs', {
  extend: 'Ext.data.Store',
  alias: 'std.store.fournisseurs',
  model: 'Standards.model.Fournisseur',
  sorters : [{property : 'fournisseur_name', direction : 'ASC'}]
});



Ext.define('Standards.model.FournisseurMetadata', {
  alternateClassName: ['FournisseurMetadata'],
  extend : 'Ext.data.Model',
  entityName : 'std.fournisseur.metadata',
  
  proxy : {
    type : 'ajax',
    
    reader: {
      type: 'json',
      rootProperty: 'metadata'
    }
  },
  
  idProperty : 'uuid',
  fields : [
    'uuid'
  , 'URI'
  , 'name'
  , 'abstract'
  , { name : 'exists', type : 'bool', defaultValue : false}
  , { name : 'standard_id', references : 'Standards.model.Standard'}
  ],
  
  hasMany : [{
    name : 'catalogs',
    model : 'std.fournisseur.metadata.catalog',
    associationKey: 'catalogs', 
    foreignKey : 'standard_id',
    inverse : {role:'standard', reference:'std.fournisseur.metadata', persist:false},
    persist:false
  }],
  
  isReferencedStandard : function(){
    var standards = Standards.store.Standard.getAllDataInstance();
    
    standards.un('load', this.isReferencedStandard, this);
    standards.un('datachanged', this.isReferencedStandard, this);
    standards.on('load', this.isReferencedStandard, this, {single:true});
    standards.on('datachanged', this.isReferencedStandard, this);
    
    if ( standards.isLoaded() ){
      var standard = standards.findExact('standard_url', this.get('URI'));
      if ( standard!=-1 ) standard = standards.getAt(standard);
      else standard = null;
      
      this.set('exists', !!standard);
      this.set('standard_id', (standard ? standard.get('standard_id') : null));
    }
  },
  
  onDestroy : function(){
    var standards = Standards.store.Standard.getAllDataInstance();
    standards.un('load', this.isReferencedStandard, this);
    standards.un('datachanged', this.isReferencedStandard, this);
  }
});

Ext.define('Standards.model.FournisseurMetadataCatalogue', {
  alternateClassName: ['FournisseurMetadataCatalogue'],
  extend : 'Ext.data.Model',
  entityName : 'std.fournisseur.metadata.catalog',
  
  idProperty : 'uuid',
  fields : [
    'standard_id'
  , 'uuid'
  , 'URI'
  ],
});

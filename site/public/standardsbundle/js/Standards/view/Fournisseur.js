/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




Ext.define('Standards.form.Fournisseur', {
  requires : ['Ext.ux.grid.SubTable'],
  extend : 'Ext.form.Panel',
  xtype : 'std-fournisseur-form',
  title : "Edition d'un fournisseur",
  viewModel : true,
  
  defaultType : 'textfield',
  defaults : {allowBlank : false, anchor : '100%', labelAlign : 'left', labelWidth : 150, labelSeparator : '', margin : 5},
  

  disabled : true, 
  initComponent : function(){
    this.items = [{
      reference : 'fournisseur_name',
      name : 'fournisseur_name', fieldLabel : 'Nom du fournisseur', bind : '{fournisseur_name}'
    }, {
      xtype : 'fieldcontainer',
      defaultType : this.defaultType,
      margin : 0,
      defaults : this.defaults,
      layout : {
        type : 'hbox',
        align : 'center',
        pack : 'stretchmax'
      },
      items : [{
        flex : 1,
        reference : 'fournisseur_url',
        name : 'fournisseur_url', fieldLabel : "URL d'interrogation", vtype : 'url'  , bind : '{fournisseur_url}'
      }, {
        flex : 0.15,
        xtype : 'button',
        text : 'Interroger',
        handler : 'getExternalStandards',
        disabled : true,
        bind : {
          disabled : '{!fournisseur_url}'
        }
      }]
    }, {
      title : "Liste des standards accessibles pour ce fournisseur",
      reference : 'metadatas',
      width : '100%',
      hidden : true,
      store : Ext.create('Ext.data.JsonStore', {model : 'Standards.model.FournisseurMetadata'}),
      xtype : 'grid',
      forceFit : true,
      reserveScrollbar : true,
      columnLines : true,
      plugins: {
        width : '100%',
        border : true,
        deferEmptyText : false,
        ptype: "subtable",
        association: 'catalogs',
        headerWidth: 24,
        title : "Catalogues d'attributs",
        forceFit : true,
        reserveScrollbar : true,
        columnLines : true,
        colspan : 2,
        style : 'width:100%',
        columns: [{
          text: "Catalogue d'attributs",
          dataIndex: 'uuid',
          flex : 1
        },{
          align : 'center',
          text: 'Consulter',
          dataIndex: 'URI',
          flex : 0.5,
          renderer : function(value){
            if ( value ) return new Ext.XTemplate('<a href="{0}" target="_blank">Consulter</a>').apply([value]);
            return ""
          }
        }]
      }, 
      deferEmptyText : false,
      viewConfig : {emptyText : "Aucune métadonnée standard", deferEmptyText : false},
      columns: [{
        flex : 1,
        text: 'Métadonnée Standard',
        dataIndex: 'name',
      },{
        align : 'center',
        flex : 0.5,
        text: 'Consulter',
        dataIndex: 'URI',
        renderer : function(value){
          if ( value ){
            return '<a href="'+ value +'" target="_blank">Consulter</a>';
          } 
          return ""
        }
      },{
        flex : 0.5,
        text : 'Actions',
        xtype : 'widgetcolumn',
        onWidgetAttach : function(column, widget, record){
          record.isReferencedStandard();
          var loader = widget.down('#loader');
          var deleter = widget.down('#deleter');
      
          loader.record = record;
          deleter.record = record;
          
          if ( record.get('exists') ){
            loader.setText('Actualiser');
          } else {
            loader.setText('Ajouter')
          }
          
          deleter.setVisible(record.get('exists'));
          widget.updateLayout();
          widget.setVisible(false);

          var form = widget.up('form');
          if ( !form ){
            return false;
          } 

          var recordForm = form.getRecord();
          if ( !recordForm ) {
            return false;
          }

          widget.setVisible(!recordForm.phantom);
        },
        widget : {
          xtype : 'panel',
          flex: 1,
          defaults : {xtype : 'button', text : ''},
          items : [ {
            itemId : 'loader',
             text : 'Actualiser',
            handler : 'loadExternalStandard'
          }, {
            itemId : 'deleter', 
            text : 'Supprimer',
            handler : 'deleteStandardFromExternal'
          }],
        }
      }],
      listeners : {
        beforerender : function(grid){
          var store = Standards.store.Standard.getAllDataInstance();
          if ( !store ) {return false};
        
          store.on('datachanged', grid.refreshOnStandards, grid);
        }
      },
      refreshOnStandards : function(){
        this.getView().refresh();
      }
    }];
  
    this.buttons = [{
      text : 'Enregistrer',
      handler : 'saveRecord'
    }, {
      text : 'Annuler',
      handler : 'closeEdition'
    }];
  
    
    this.callParent(arguments);
  },
  
  loadRecord : function(record){
    if ( record ) {
      this.setDisabled(false);
      this.setBind({title : record.phantom ? "Nouveau fournisseur" : "Fournisseur {fournisseur_name}"});
    } else {
      this.setDisabled(true);
      this.setBind({title : "Edition d'un fournisseur"});
    }
    
    record = record || Ext.create('Ext.data.Model');
    this.reset();
    return this.callParent([record]);
  }
});

Ext.define('Standards.grid.Fournisseur', {
  extend : 'Ext.grid.Panel',
  xtype : 'std-fournisseur-grid',
  title : 'Liste des fournisseurs de standards',
  
  width : '100%',
  minHeight : 200,
  maxHeight : 500,
  scrollable : true,

  forceFit : true,
  reserveScrollbar : true,
  viewConfig : {
    emptyText : "Aucun fournisseur"
  },
  columnLines : true,
  initComponent : function(){
    this.store = Ext.create('Standards.store.Fournisseurs', {autoLoad:true});
    this.columns =  [{
      flex : 0.8,
      dataIndex : 'fournisseur_name', header : 'Nom du fournisseur'
    }, {
      flex : 1,
      dataIndex : 'fournisseur_url' , header : "URL d'interrogation", vtype : 'url'  
    }, {
      flex : .22,
      xtype : 'widgetcolumn',
      align : 'center',
      widget : {
        xtype : 'fieldcontainer',
        
        border : false,
        defaults : {xtype : 'button', margin : '0 5'},
        items : [{
          text : '<i class="fa fa-pencil"></i>',
          handler : 'openRecord'
        }, {
          text : '<i class="fa fa-times"></i>',
          handler : 'deleteRecord'
        }]
      }
    }];

    this.tbar = [{
      text : 'Nouveau fournisseur',
      handler : 'openNewRecord'
    }];
    
    this.callParent(arguments);
  }
});


Ext.define('Standards.view.Fournisseur', {
  requires : ['Standards.controller.Fournisseur'],
  extend : 'Ext.panel.Panel',
  xtype : 'std-fournisseur-view',
  controller : 'fournisseur',
  layout : 'column',
  
  defaults : {margin : 5},
  items : [{
    xtype : 'std-fournisseur-grid',
    columnWidth : 0.5 
  }, {
    xtype : 'std-fournisseur-form',
    columnWidth : 0.5
  }],
});
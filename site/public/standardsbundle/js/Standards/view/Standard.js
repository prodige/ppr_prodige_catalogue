/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




Ext.define('Standards.form.Standard', {
  extend : 'Ext.form.Panel',
  xtype : 'std-standard-form',
  title : "Edition d'un standard",
  viewModel : true,
  
  defaultType : 'textfield',
  defaults : {allowBlank : false, anchor : '100%', labelAlign : 'left', labelWidth : 150, labelSeparator : '', margin : 5},
  

  disabled : true, 
  initComponent : function(){
    
    this.items = [{
      name : 'standard_name', fieldLabel : 'Nom du standard', bind : '{standard_name}'
    }, {
      xtype : 'fieldcontainer',
      defaultType : this.defaultType,
      margin : 0,
      defaults : this.defaults,
      layout : {
        type : 'hbox',
        align : 'center',
        pack : 'stretchmax'
      },
      items : [{
        flex : 1,
        name : 'standard_url' , fieldLabel : "URL d'interrogation", vtype : 'url'  , bind : '{standard_url}'
      }, {
        flex : 0.15,
        xtype : 'button',
        text : 'Interroger',
        handler : 'loadStandards',
        bind : {
          disabled : '{!standard_url}'
        }
      }]
    }, {
      
    }];
  
    this.buttons = [{
      text : 'Enregistrer',
      handler : 'saveRecord'
    }, {
      text : 'Annuler',
      handler : 'closeEdition'
    }];
  
    
    this.callParent(arguments);
  },
  
  loadRecord : function(record){
    if ( record ) {
      this.setDisabled(false);
      this.setBind({title : record.phantom ? "Nouveau standard" : "Standard {standard_name}"});
    } else {
      this.setDisabled(true);
      this.setBind({title : "Edition d'un standard"});
    }
    
    record = record || Ext.create('Ext.data.Model');
    this.callParent([record]);
  }
});

Ext.define('Standards.grid.Standard', {
  extend : 'Ext.grid.Panel',
  xtype : 'std-standard-grid',
  title : 'Liste des standards de standards',
  
  width : '100%',
  minHeight : 200,
  maxHeight : 500,
  scrollable : true,

  forceFit : true,
  reserveScrollbar : true,
  viewConfig : {
    emptyText : "Aucun standard"
  },
  columnLines : true,
  initComponent : function(){
    this.store = Ext.create('Standards.store.Standards', {autoLoad:true});
    this.columns =  [{
      flex : 1,
      dataIndex : 'standard_name', header : 'Nom du standard'
    }, {
      flex : 1,
      dataIndex : 'standard_url' , header : "URL d'interrogation", vtype : 'url'  
    }, {
      flex : .2,
      xtype : 'widgetcolumn',
      align : 'center',
      widget : {
        xtype : 'fieldcontainer',
        
        border : false,
        defaults : {xtype : 'button', margin : '0 5'},
        items : [{
          text : '<i class="fa fa-pencil"></i>',
          handler : 'openRecord'
        }, {
          text : '<i class="fa fa-times"></i>',
          handler : 'deleteRecord'
        }]
      }
    }];

    this.tbar = [{
      text : 'Nouveau standard',
      handler : 'openNewRecord'
    }];
    
    this.callParent(arguments);
  }
});


Ext.define('Standards.view.Standard', {
  requires : ['Standards.controller.Standard'],
  extend : 'Ext.panel.Panel',
  xtype : 'std-standard-view',
  controller : 'standard',
  layout : 'column',
  
  defaults : {margin : 5},
  items : [{
    xtype : 'std-standard-grid',
    columnWidth : 0.6 
  }, {
    xtype : 'std-standard-form',
    columnWidth : 0.4
  }],
});
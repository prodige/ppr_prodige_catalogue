/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('Standards.view.Main', {
  extend : 'Ext.Viewport',
  
  requires : ['Standards.view.Fournisseur'],
  
  
  layout : 'border',
  
  initComponent : function(){
    this.renderTo = Ext.getBody();
    this.items = [{
        region : 'north',
        html : '<center><h1>Administration du module qualité</h1><h2>Gestion des fournisseurs et des standards</h2></center>'
    }, {
        region : 'center',
        xtype : 'tabpanel',
        items : [{
            title : "Fournisseurs de standards",
            xtype : 'std-fournisseur-view'
        }]
    }];
  
    this.callParent(arguments);
  }
})
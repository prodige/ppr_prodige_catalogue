<?php 
/**
 * Fichier CSS
 * @author Alkante
 */
header("Content-type: text/css"); 
//require_once("../parametrage.php");
?>

body { 
  background-color:#ffffff;
  margin:0 0 0 0;
  font-family:<?php echo PRO_FONT_FAMILY ?>; 
  font-size:10pt;
}

H2 {
  font-size:14pt; 
  margin:10px 5px 10px 5px;
}
.titre  {
  top:5px; 
  width:100; 
  font-size:14pt; 
  color: <?php echo PRO_COLOR_TEXT ?>; 
}

.errormsg {
  top:50px; 
  width:100%; 
  z-index:50; 
  color:red; 
  font-family:<?php echo PRO_FONT_FAMILY ?>; 
  font-size:12pt; 
  font-weight:bold; 
  text-align:center;
}

.formulaire {
  top:70px; 
  left:80px; 
  border:0px solid black; 
  overflow:hidden; 
  z-index:10;
}

.formulaire TABLE {
  border-collapse:collapse; 
  color:black; 
  font-family:<?php echo PRO_FONT_FAMILY ?>; 
  font-size:10pt; 
  height:100%; 
}

.formulaire TR {
  height:35px;
}

.formulaire TH  {
  color:<?php echo PRO_COLOR_TEXT ?>; 
  text-align:right; 
  padding-right: 10px; 
  font-weight:bold;
  width:200px
}

.formulaire TD {
  color:<?php echo PRO_COLOR_TEXT ?>; 
  text-align:left;
  width: 200px
}

.formulaire SELECT {}

.formulaire .validation{
  text-align:center
}

.lock {
  background-0color:#dbdbdb; 
  color: #666666;
}

INPUT, SELECT, TEXTAREA, .formulaire  INPUT, .formulaire  SELECT  {
  border:1px solid <?php echo PRO_COLOR_TEXT?>;
  background-color:<?php echo PRO_COLOR_BACK ?>;
  font-family:<?php echo PRO_FONT_FAMILY ?>; 
  cursor:pointer
}

.cmd INPUT, .validation INPUT, .back INPUT {
  background-color:<?php echo PRO_COLOR_TEXT?>;
  border:1px solid #666666;
  color:#FFFFFF;
  font-family:<?php echo PRO_FONT_FAMILY ?>;
}
.etat {
  font-size:7pt; 
  color:<?php echo PRO_COLOR_TEXT?>; 
  display:block;
}

.liste  {
  position:relative; 
  width:100%; 
  height:100%;
  z-index:5; 
  font-family:<?php echo PRO_FONT_FAMILY ?>; 
  color:#333333;
}
.carteprojet  {
  background-color: #e5f2d3;
  padding:5px; 
  font-size:8pt; 
  margin:10px;
}
H4  {
  font-size:13px; 
  font-weight:bold; 
  color:<?php echo PRO_COLOR_TEXT_LIGHT ?>;
  margin:2px 0px 7px 0px;
  cursor:pointer;
  background-image:url("/cartepersobundle/images/puce_h3.gif");
  background-repeat: no-repeat;
  background-position: left 5px;
  padding-left: 10px;
}

.cmd  {
  position:relative; 
  float: right;
}

.cmd a {
    
	  background-color:<?php echo PRO_COLOR_TEXT?>;
	  border:1px solid #666666;
	  color:#FFFFFF;
	  font-family:<?php echo PRO_FONT_FAMILY ?>;
    text-decoration: none;
    font-size: 9pt;
    padding: 0 3px;
    
    
}



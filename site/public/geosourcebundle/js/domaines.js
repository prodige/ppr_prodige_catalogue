var			domaines_event_onchange = new Array(domaines_select);

/**
 * @brief Gestion du changement de domaine
 * @param evt
 * @return
 */
function	domaines_switch(evt)
{
	var		element;
	var		domaines;
	var		i;
	var		isOpen = false;

	if (navigator.appName == "Microsoft Internet Explorer")
	{
		evt = window.event;
		element = evt.srcElement;
	}
	else
		element = evt.target;
	if ((element.tagName == "TD") && (element.cellIndex > 0))
		element = element.parentNode.cells[0];
	domaines = domaines_getDomaines();
	if (domaines)
	{
		for (i = 0; i < domaines.length; i++)
		{
			if (domaines[i].cells.length > 0)
			{
				switch(domaines[i].cells[0].className)
				{
					case "groupe_ouvert":
						if (domaines[i].cells[0] == element)
						{
							domaines[i].cells[0].className = "groupe_ferme";
							isOpen = false;
						}
						else
						{
							domaines[i].cells[0].className = "groupe_ouvert";
							isOpen = true;
						}
						break;
					case "groupe_ferme":
						if (domaines[i].cells[0] != element)
						{
							domaines[i].cells[0].className = "groupe_ferme";
							isOpen = false;
						}
						else
						{
							domaines[i].cells[0].className = "groupe_ouvert";
							isOpen = true;
						}
						break;
					case "":
						switch(domaines[i].className)
						{
							case "sous_groupe":
							case "sous_groupe_hide":
								if (isOpen)
									domaines[i].className = "sous_groupe";
								else
									domaines[i].className = "sous_groupe_hide";
								break;
							default:
								break;
						}
						break;
					default:
						break;
				}
			}
		}
	}
}
/**
 * @brief Retourne les domaines
 * @return domaines
 */
function	domaines_getDomaines()
{
	var		div;
	var		tables;
	var		domaines = null;
	var		i;

	div = document.getElementById("domaines");
	if (div)
		domaines = div.getElementsByTagName("TR");
	return domaines;
}
/**
 * @brief Retourne les sous-domaines
 * @return sous-domaines
 */
function	domaines_getSousDomaine(ligne)
{
	var		elems;
	var		elem;
	var		sd = null;

	if (ligne && (ligne.cells.length == 3))
	{
		elem = ligne.cells[2].firstChild;
		if (elem && (elem.nodeType == 3))
			sd = elem.data;
	}
	return sd;
}
/**
* @brief Retourne le domaine d'une ligne
* @return dom
*/
function	domaines_getDomaine(ligne)
{
	var		mainline;
	var		elems;
	var		elem;
	var		dom = null;

	if (ligne)
	{
		if (ligne.cells.length == 2)
		{
			mainline = ligne;
			if (mainline && (mainline.tagName == "TR") && (mainline.cells.length == 2) && ((mainline.cells[0].className == "groupe_ouvert") || (mainline.cells[0].className == "groupe_ferme")))
				elem = mainline.cells[1].firstChild;
			if (elem && (elem.nodeType == 3))
				dom = elem.data;
		}
		if (ligne.cells.length == 3)
		{
			mainline = ligne.previousSibling;
			while (mainline && ((mainline.tagName != "TR") || (mainline.cells.length != 2) || ((mainline.cells[0].className != "groupe_ouvert") && (mainline.cells[0].className != "groupe_ferme"))))
				mainline = mainline.previousSibling;
			if (mainline && (mainline.tagName == "TR") && (mainline.cells.length == 2) && ((mainline.cells[0].className == "groupe_ouvert") || (mainline.cells[0].className == "groupe_ferme")))
				elem = mainline.cells[1].firstChild;
			if (elem && (elem.nodeType == 3))
				dom = elem.data;
		}
	}
	return dom;
}
/**
 * @brief Sélectionne un domaine
 * @param evt
 * @return
 */
function	domaines_select(evt)
{
	var		element;
	var		sd = null;
	var		dom = null;

	if (navigator.appName == "Microsoft Internet Explorer")
	{
		evt = window.event;
		element = evt.srcElement;
	}
	else
		element = evt.target;
	while (element && (element.tagName != "TR"))
		element = element.parentNode;
	if (element && (element.tagName == "TR"))
	{
		dom = domaines_getDomaine(element);
		sd = domaines_getSousDomaine(element);
	}
	domaines_setResume(dom, sd);
}
/**
 * @brief Paramètre le texte résumé d'un domaine
 * @param domaine
 * @param sousdomaine
 * @return
 */
function	domaines_setResume(domaine, sousdomaine)
{
	var		div_dom;
	var		div_sdom;
	var		node;

	div_dom = document.getElementById("domaine");
	if (domaine && div_dom)
	{
		node = div_dom.firstChild;
		if (node && (node.nodeType == 3))
			node.data = domaine;
		else
		{
			node = document.createTextNode(domaine);
			if (node)
				div_dom.appendChild(node);
		}
	}
	div_sdom = document.getElementById("sousdomaine");
	if (div_sdom)
	{
		node = div_sdom.firstChild;
		if (node && (node.nodeType == 3))
		{
			if (sousdomaine)
				node.data = sousdomaine;
			else
				div_sdom.removeChild(node);
		}
		else
		{
			if (sousdomaine)
			{
				node = document.createTextNode(sousdomaine);
				if (node)
					div_sdom.appendChild(node);
			}
		}
	}
	try
	{
		IHM_Domaines_resume();
	}
	catch(e)
	{}
}
/**
 * @brief Retourne le domaine courant
 * @return domaine
 */
function	domaines_getCurentDomaine()
{
	var		div_dom;
	var		domaine = null;
	//noeud sélectionné
	var		node = Ext.getCmp("TreeDomaine").currentNode;
	if(node!= null){
	  if (node.attributes["isDomain"]){
	    domaine = node.text;
	  }else if(node.attributes["isSubDomain"]){
	    domaine = node.attributes["domain"];
	  }
  }

  /*div_dom = document.getElementById("domaine");
	if (div_dom)
	{
		node = div_dom.firstChild;
		if (node && (node.nodeType == 3))
			domaine = node.data;
	}*/
	
	return domaine;
}
/**
 * @brief Retourne le sous-domaine courant
 * @return sousdomaine
 */
function	domaines_getCurentSousDomaine()
{
	var		div_sdom;
	var		sousdomaine = null;
	var		node;

	/*div_sdom = document.getElementById("sousdomaine");
	if (div_sdom)
	{
		node = div_sdom.firstChild;
		if (node && (node.nodeType == 3))
			sousdomaine = node.data;
	}*/
	var node = Ext.getCmp("TreeDomaine").currentNode;
	if(node!= null){
	  if (node.attributes["isSubDomain"]){
	    sousdomaine = node.text;
	  }
	}
	return sousdomaine;
}

function unsetDomaine(){
  var treepanel = Ext.getCmp('TreeDomaine');
  treepanel.getSelectionModel().select(treepanel.root.firstChild);
  treepanel.currentNode = treepanel.root.firstChild;
}


/**
 * vérifie que l'utilisateur a sélectionné au moins un sous-domaine dont il a accès
 */
function checkAndSetCurrentSousDomaine(treePanel, oCtrlSdom, tabSdom, onSuccess, scope){
  
  var iframe = new Object();
  iframe.tabParams = [treePanel, oCtrlSdom, tabSdom, onSuccess, scope];
  iframe.onSuccess = function(treePanel, oCtrlSdom, tabSdom, onSuccess, scope, responseText){
    var oSdomList = eval("("+responseText+")");
    var firstSdom = -1;
    oCtrlSdom.innerHTML = "";
    Ext.each(treePanel.getChecked(), function(node){
      var tabTmp = node.id.split("_");
      
      // ajoute chaque sous-domaine sélectionné au controle select caché contenant la liste des sous-domaines sélectionnés
      var option = document.createElement("OPTION");
      option.setAttribute("value", tabTmp[1]);
      option.selected = true;
      oCtrlSdom.appendChild(option);
      
      if ( firstSdom == -1 ) {
        Ext.each(oSdomList, function(oSdom){
          if ( oSdom.id == tabTmp[1] ) {
            firstSdom = tabTmp[1];
            return false;
          }
        });
      }
    });
    if ( firstSdom == -1 ) {
      /*
      var tabSDomRight = [];
      Ext.each(oSdomList, function(oSdom){
        tabSDomRight.push(oSdom.nom);
      });
      */
      Ext.Msg.alert("Sous-domaines",
                    "Veuillez sélectionner au moins un sous-domaine pour lequel vous avez les droits d'administration."/*+
                    " Ceux-ci sont listés ci-dessous :<br/>"+
                    "<ul><li>"+tabSDomRight.join('</li><li>')+"</li></ul>"*/);
    } else {
      
  		_onSuccess = function(){onSuccess()};
  		_onSuccess.createDelegate(scope).call();
    }
  };
  AjaxRequest(domaineService, {'mode':2}, iframe);
}

/**
 * @version 3.2
 * effectue un appel Ajax
 * @param ajaxUrl        URL de l'appel
 * @param queryParams    paramètres de l'appel
 * @param iframe         objet contenant les fonctions "onSuccess" ainsi que les paramètres "tabParams" associés
 * @param method         méthode de l'appel ("GET" par défaut)
 */
function AjaxRequest(ajaxUrl, queryParams, iframe, method){
  Ext.Ajax.request({
    url : ajaxUrl , 
    params : queryParams,
    method : ( method ? method : 'GET' ),
    success : function(result){
      var fn;
      if ( typeof iframe.onSuccess == 'function' ) {
        fn = iframe.onSuccess;
        if ( !iframe.onSuccess.createDelegate ) {
          fn = function(){
            return iframe.onSuccess.apply(this, arguments)
          }
        }
        fn.createDelegate(this, ( iframe.tabParams && iframe.tabParams.concat && typeof iframe.tabParams.concat == 'function' ? iframe.tabParams.concat(result.responseText) : [result.responseText] )).call();
      }
    },
    failure : function(result){
      console.log("failedAjax")
      console.log(result);
    } 
  });
}

/**
 * @brief Valide l'ajout d'une carte
 */
function administration_domaines_valider() {
  
  var valid = true;
  var msg = "";
  var errormsg;
  
  var treePanel = Ext.getCmp('ext-arbo-sdom');
  
  if (treePanel) {
    if (treePanel.getChecked().length == 0)
    {  
      valid = false;
      parent.Ext.Msg.alert("Gestion des sous-domaines",
          "Vous devez cocher au moins un sous-domaine");
      return;
    }
  }
  if (valid) {
    var oCtrlSdom = document.getElementById("SDOM");
    parent.checkAndSetCurrentSousDomaine(treePanel, oCtrlSdom, tabSdom, function(){document.forms[0].submit();}, this);
  }
}

/**
 * @brief Ajoute un domaine
 * @param fct
 * @return
 */
function	domaines_event_onchange_add(fct)
{
	var		i;
	var		nb;
	var		exist = false;
	nb = domaines_event_onchange.length;
	for (i = 0; (i < nb) && !exist; i++)
		if (fct == domaines_event_onchange[i])
			exist = true;
	if (!exist)
		domaines_event_onchange[nb] = fct;
}
/**
 * @brief Efface un domaine
 * @param fct
 * @return
 */
function	domaines_event_onchange_remove(fct)
{
	var		i;
	var		nb;
	var		exist = -1;
	var		debut, fin;
	nb = domaines_event_onchange.length;
	for (i = 0; (i < nb) && (exist < 0); i++)
		if (fct == domaines_event_onchange[i])
			exist = i;
	if (exist >= 0)
	{
		debut = domaines_event_onchange.slice(0, exist);
		fin = domaines_event_onchange.slice(exist + 1);
		domaines_event_onchange = debut.concat(fin);
	}
}
/**
 * 
 * @param evt
 * @return
 */
function	domaines_event_onchange_raise(evt)
{
	var		i;
	var		oldNb, newNb;
	newNb = domaines_event_onchange.length;
	oldNb = newNb;
	for (i = 0; i < newNb; i += ((oldNb == newNb) ? 1 : 0))
	{
		oldNb = newNb;
		domaines_event_onchange[i](evt);
		newNb = domaines_event_onchange.length;
	}
}
/**
 * @brief Récupère la liste des sous-domaines
 * @return
 */
function domaines_getAllRubric()
{
  var root = (arguments.length>0 ? arguments[0] : domaineList);
  var domaines = new Array();
  for (var i=0; i<root.length; i++){
    var node = root[i];
    if ( node.cls=="folder" ){
      domaines.push(node);
    }
    else if ( typeof node.children != "undefined" ){
      domaines = domaines.concat(domaines_getAllRubric(node.children));
    }
  }
  return domaines;
}
/**
 * @brief Récupère la liste des sous-domaines
 * @return
 */
function domaines_getAllDomaine()
{
  var root = (arguments.length>0 ? arguments[0] : domaineList);
  var domaines = new Array();
  for (var i=0; i<root.length; i++){
    var node = root[i];
    if ( node.isDomain ){
      domaines.push(node);
    }
    else if ( typeof node.children != "undefined" ){
      domaines = domaines.concat(domaines_getAllDomaine(node.children));
    }
  }
  return domaines;
}
/**
 * @brief Récupère la liste des sous-domaines
 * @return
 */
function domaines_getAllSousDomaine()
{
  var root = (arguments.length>0 ? arguments[0] : domaineList);
  var domaines = new Array();
  for (var i=0; i<root.length; i++){
    var node = root[i];
    if ( node.isSubDomain ){
      domaines.push(node);
    }
    else if ( typeof node.children != "undefined" ){
      domaines = domaines.concat(domaines_getAllSousDomaine(node.children));
    }
  }
  return domaines;
}
/**
 * supprime les espaces en fin et début de mot
 * @param myString
 * @return
 */
function trim (myString)
{
  return myString.replace(/^\s+/g,'').replace(/\s+$/g,'')
} 

/**
 * @brief Construit la liste des domaines / sous-domaines
 * @param ctrl
 * @param selection
 * @param ctrl_dom
 * @return
 */
function	domaines_buildOptionsList(ctrl_sdom, selection, ctrl_dom, ctrl_rubric)
{
	var		domaines;
	var		node;
	var		i;
	var		rubric;
	var		dom;
	var		sdom;
	var		optGroup;
	var		option;
	var		lastDom = "";
	var		attr;
	var		selected;
  var subDomain = domaines_getAllSousDomaine();
  
	ctrl_sdom.options.length = 0;
	
	for (i = 0; i < subDomain.length; i++)
	{
	  
		node = subDomain[i];
		sdom = node.text;
		dom = node.domain;
		rubric = node.rubric;
		selected = (selection==sdom);
		if (dom)
		{
			if (dom != lastDom)
			{
				lastDom = dom;
				optGroup = document.createElement("optGroup");
				if (optGroup)
				{
					optGroup.label = dom;
					if (navigator.appName == "Microsoft Internet Explorer")
						ctrl_sdom.insertAdjacentElement("beforeEnd", optGroup);
					else
						ctrl_sdom.appendChild(optGroup);
				}
			}
			if (sdom)
			{
				option = document.createElement("option");
				if (option && optGroup)
				{
					option.value = sdom;
					option.text = '  '+sdom;
					option.selected = selected;
					if (navigator.appName == "Microsoft Internet Explorer")
						ctrl_sdom.options.add(option);
					else
						optGroup.appendChild(option);
				}
				
			}
			
		}
	}
	
	if (ctrl_dom && (selection == ""))
	{
		sdom = "";
		if (domaines_getCurentSousDomaine())
			sdom = domaines_getCurentSousDomaine();
		if (!sdom)
		{
			dom = domaines_getCurentDomaine();
			if (!dom)
				dom = domaines_getFirstDomaine();
			if (dom)
			{
				sdom = domaines_getFirstSousDomaine(dom);
				ctrl_dom.value = dom;
			}
		}
		selection = sdom;
	}
	for (i = 0; i < ctrl_sdom.options.length; i++)
	{
		if (trim(ctrl_sdom.options[i].text) == selection)
		{
		  ctrl_sdom.selectedIndex = i;
			domaines_setDomaineByOptGroup(ctrl_sdom, ctrl_dom, ctrl_rubric);
		}
	}
	if ( ctrl_sdom.onchange )
    ctrl_sdom.onchange();
    
}
/**
 * @brief Utilisé pour afficher les domaines dans un certain ordre
 * @param ctrl
 * @param inpt
 * @return
 */
function	domaines_setDomaineByOptGroup(ctrl_sdom, ctrl_dom, ctrl_rubric)
{
  var subDomain = domaines_getAllSousDomaine();
	if (ctrl_sdom.selectedIndex >= 0)
	{
	  for( var i=0; i<subDomain.length; i++ ){
	    if ( subDomain[i].text == ctrl_sdom.options[ctrl_sdom.selectedIndex].value ){
	      if ( ctrl_dom ) 
	        ctrl_dom.value = subDomain[i].domain;
	      if ( ctrl_rubric ) 
	        ctrl_rubric.innerHTML = subDomain[i].rubric;
	    }
	  }
		try
		{
			parent.domaines_setResume(ctrl_dom.value, ctrl_sdom.options[ctrl_sdom.selectedIndex].text);
		}
		catch(e){}
	}
}
/**
 * @brief Retourne le premier domaine
 * @return domaine
 */
function	domaines_getFirstDomaine()
{
	var		domaines;
	var		node;
	var		i;
	var		dom;
	var		domaine = "";
	
	var    node = Ext.getCmp("TreeDomaine").root.firstChild;
  node = node.firstChild;
  if(node!= null){
    if(node.attributes["isDomain"]){
      domaine = node.attributes["text"];
    }
  }
  return domaine;
	
  
}
/**
 * @brief Retourne le premier sous domaine d'un domaine
 * @param domaine
 * @return sousdomaine
 */
function	domaines_getFirstSousDomaine(domaine)
{
	var		domaines;
	var		node;
	var		i;
	var		dom;
	var		sdom;
	var		sousdomaine = "";
	var nodeTable = Ext.getCmp("TreeDomaine").root.childNodes;
	for(var i=0; i<nodeTable.length; i++){
	  if(nodeTable[i].findChild("text", domaine, true)!=null){
	    var node = nodeTable[i].findChild("text", domaine, true);
	    var firstNode = node.attributes.children[0];
	    if(firstNode!= null){
	      if(firstNode.isSubDomain){
	        sousdomaine = firstNode.text;
	      }
	    }
	    break;
	  }
	}
	return sousdomaine;
	
}

/**
 * @brief soummset le formulaire wms/wfs
 */
function  administration_carto_services_valider()
{
  var   serveur, format, fichier;
  var   valid = true;
  var   msg = "";
  var   errormsg;

  title = document.getElementById("title");
  resume = document.getElementById("abstract");
  srs = document.getElementById("srs");
  extent = document.getElementById("extent");
  keywords = document.getElementById("keywords");
  layername = document.getElementById("layername");
  if (title && resume && srs && extent && keywords && layername)
  {
    if ((title.value == "") || (resume.value == "") || (srs.value == "")|| (keywords.value == "") || (layername.value == ""))
    {
      valid = false;
      alert("Tous les champs sont obligatoires");
      return false;
    }
  }
  var reg1=new RegExp("[\"]","g");
  if(resume.value.match(reg1) || title.value.match(reg1) || keywords.value.match(reg1)){
    alert("Le caractère \" n'est pas autorisé");
    return;
  }
  
  var modele = /^[a-z]+[a-z0-9_]*$/i;
  if (modele.test(layername.value)) {

  } else {
    alert( "Le nom de couche dans le service ne doit pas contenir de caractères "
                + "accentués ou non alphanumériques comme -()\"'. etc.");
    return;
  }
  if (!layername.readOnly  && in_array(layername.value, tabLayerName)) {
    alert("Une couche du service portant ce nom existe déjà.");
    return;
  }
  if (valid)
  {
    if (confirm('Attention, en ajoutant cette donnée au serveur, vous la rendez publique.')){
      //parent.IHM_Chargement_start();
      document.forms["formService"].action += (document.forms["formService"].action.indexOf('?')==-1 ? '?' : '&')+"iMode=2";
      document.forms["formService"].submit();
    }
  }
}
/**
 * @brief soummset le formulaire wms/wfs
 */
function  administration_carto_services_supprimer()
{
  if (confirm('Attention, vous allez supprimer la donnée du serveur.')){
    document.forms["formService"].action += (document.forms["formService"].action.indexOf('?')==-1 ? '?' : '&')+"iMode=4";
    document.forms["formService"].submit();
  }
}



/**
 * @brief Open Parametrage Carto to give a default representation to the layer added in the WMS.map
 * @return nothing, open Parametrage carto
 */
function openWMSDefaultRepresentation(action, login, pwd){
  if(document.administration_carto){
    document.administration_carto.login.value = login;
    document.administration_carto.pass.value = pwd;
    document.administration_carto.action = action;
    document.administration_carto.target = "blank";
    document.administration_carto.submit();
  }
  
}

/**
 * true if needle is in array (php equivalent)
 * @param needle
 * @param haystack
 * @param argStrict
 * @return
 */
function in_array (needle, haystack, argStrict) {
   
  var key = '', strict = !!argStrict; 
  if (strict) {
      for (key in haystack) {
        if (haystack[key] === needle) {
          return true;            
        }
      }
  } else {
      for (key in haystack) {
        if (haystack[key] == needle) {   
          return true;
        }
      }
  }
  return false;
}
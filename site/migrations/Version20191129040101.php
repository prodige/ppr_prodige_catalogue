<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191129040101 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Création des vues de la base CATALOGUE';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('set search_path to CATALOGUE, public');

        $this->addSql("
            CREATE VIEW catalogue.administrateurs_sous_domaine AS
             SELECT domaine.pk_domaine,
                domaine.dom_nom,
                sous_domaine.pk_sous_domaine,
                sous_domaine.ssdom_nom,
                groupe_profil.pk_groupe_profil,
                groupe_profil.grp_nom,
                utilisateur.pk_utilisateur,
                utilisateur.usr_id
               FROM ((((catalogue.domaine
                 RIGHT JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
                 LEFT JOIN catalogue.groupe_profil ON ((sous_domaine.ssdom_admin_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
                 LEFT JOIN catalogue.grp_regroupe_usr ON ((grp_regroupe_usr.grpusr_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
                 LEFT JOIN catalogue.utilisateur ON ((grp_regroupe_usr.grpusr_fk_utilisateur = utilisateur.pk_utilisateur)))
            ");

        $this->addSql("
            CREATE VIEW catalogue.cartes_sdom AS
             SELECT carte_projet.cartp_format,
                carte_projet.pk_carte_projet,
                carte_projet.cartp_description,
                carte_projet.cartp_nom,
                sous_domaine.pk_sous_domaine,
                sous_domaine.ssdom_nom,
                stockage_carte.pk_stockage_carte,
                stockage_carte.stkcard_path,
                acces_serveur.accs_adresse,
                acces_serveur.path_consultation,
                acces_serveur.path_administration,
                acces_serveur.path_carte_statique,
                acces_serveur.accs_adresse_admin,
                acces_serveur.accs_login_admin,
                acces_serveur.accs_pwd_admin,
                acces_serveur.accs_service_admin,
                fiche_metadonnees.pk_fiche_metadonnees,
                fiche_metadonnees.fmeta_id,
                    CASE
                        WHEN (operationallowed.operationid = 0) THEN 4
                        ELSE 1
                    END AS statut,
                domaine.pk_domaine,
                domaine.dom_rubric AS dom_coherence,
                domaine.dom_nom,
                carte_projet.cartp_wms
               FROM ((((((((catalogue.domaine
                 JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
                 JOIN catalogue.ssdom_visualise_carte ON ((sous_domaine.pk_sous_domaine = ssdom_visualise_carte.ssdcart_fk_sous_domaine)))
                 JOIN catalogue.carte_projet ON ((ssdom_visualise_carte.ssdcart_fk_carte_projet = carte_projet.pk_carte_projet)))
                 LEFT JOIN catalogue.fiche_metadonnees ON ((carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees)))
                 JOIN public.metadata ON (((fiche_metadonnees.fmeta_id)::bigint = metadata.id)))
                 LEFT JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
                 JOIN catalogue.stockage_carte ON ((carte_projet.cartp_fk_stockage_carte = stockage_carte.pk_stockage_carte)))
                 LEFT JOIN catalogue.acces_serveur ON ((acces_serveur.pk_acces_serveur = stockage_carte.stk_server)));
            ");

        $this->addSql("
            CREATE VIEW catalogue.cartes_sdom_pub AS
             SELECT cartes_sdom.cartp_format,
                cartes_sdom.pk_carte_projet,
                cartes_sdom.cartp_description,
                cartes_sdom.cartp_nom,
                cartes_sdom.pk_sous_domaine,
                cartes_sdom.ssdom_nom,
                cartes_sdom.pk_stockage_carte,
                cartes_sdom.stkcard_path,
                cartes_sdom.accs_adresse,
                cartes_sdom.path_consultation,
                cartes_sdom.path_administration,
                cartes_sdom.path_carte_statique,
                cartes_sdom.accs_service_admin,
                cartes_sdom.pk_fiche_metadonnees,
                cartes_sdom.fmeta_id,
                cartes_sdom.statut,
                cartes_sdom.pk_domaine,
                cartes_sdom.dom_coherence,
                cartes_sdom.dom_nom,
                cartes_sdom.accs_adresse_admin,
                cartes_sdom.cartp_wms
               FROM catalogue.cartes_sdom
              WHERE (cartes_sdom.statut = 4);
            ");

        $this->addSql("
            CREATE VIEW catalogue.couche_sdom AS
             SELECT domaine.pk_domaine,
                domaine.dom_rubric AS dom_coherence,
                domaine.dom_nom,
                sous_domaine.pk_sous_domaine,
                sous_domaine.ssdom_nom,
                fiche_metadonnees.pk_fiche_metadonnees,
                fiche_metadonnees.fmeta_id,
                    CASE
                        WHEN (operationallowed.operationid = 0) THEN 4
                        ELSE 1
                    END AS statut,
                couche_donnees.pk_couche_donnees,
                couche_donnees.couchd_nom,
                couche_donnees.couchd_description,
                couche_donnees.couchd_type_stockage,
                couche_donnees.couchd_emplacement_stockage,
                couche_donnees.couchd_wms,
                couche_donnees.couchd_wfs,
                couche_donnees.couchd_download,
                metadata.uuid,
                couche_donnees.couchd_restriction,
                couche_donnees.couchd_visualisable,
                couche_donnees.changedate
               FROM ((((((catalogue.domaine
                 JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
                 JOIN catalogue.ssdom_dispose_couche ON ((sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine)))
                 JOIN catalogue.fiche_metadonnees ON ((ssdom_dispose_couche.ssdcouch_fk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
                 JOIN catalogue.couche_donnees ON ((couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
                 JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
                 LEFT JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))));
            ");

        $this->addSql("
            CREATE VIEW catalogue.dom_sdom AS
             SELECT domaine.pk_domaine,
                domaine.dom_rubric,
                domaine.dom_nom,
                sous_domaine.pk_sous_domaine,
                sous_domaine.ssdom_nom
               FROM (catalogue.domaine
                 LEFT JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)));
            ");

        $this->addSql("
            CREATE VIEW catalogue.editeurs_sous_domaine AS
             SELECT domaine.pk_domaine,
                domaine.dom_nom,
                sous_domaine.pk_sous_domaine,
                sous_domaine.ssdom_nom,
                groupe_profil.pk_groupe_profil,
                groupe_profil.grp_nom,
                utilisateur.pk_utilisateur,
                utilisateur.usr_id
               FROM ((((catalogue.domaine
                 RIGHT JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
                 LEFT JOIN catalogue.groupe_profil ON ((sous_domaine.ssdom_editeur_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
                 LEFT JOIN catalogue.grp_regroupe_usr ON ((grp_regroupe_usr.grpusr_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
                 LEFT JOIN catalogue.utilisateur ON ((grp_regroupe_usr.grpusr_fk_utilisateur = utilisateur.pk_utilisateur)));
            ");

        $this->addSql("
            CREATE VIEW catalogue.incoherence_carte_fiche_metadonnees AS
             SELECT carte_projet.pk_carte_projet,
                to_char(carte_projet.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS \"date de création de la carte\",
                carte_projet.cartp_nom AS \"nom de la métadonnée\",
                carte_projet.cartp_description AS \"résumé de la métadonnée\",
                carte_projet.cartp_format AS \"format de la carte\",
                carte_projet.cartp_fk_stockage_carte AS \"identifiant stockage mapfile\",
                carte_projet.cartp_fk_fiche_metadonnees AS \"identifiant de la fiche métadonnée\"
               FROM catalogue.carte_projet
              WHERE ((NOT (carte_projet.cartp_fk_fiche_metadonnees IN ( SELECT fiche_metadonnees.pk_fiche_metadonnees
                       FROM catalogue.fiche_metadonnees))) OR ((carte_projet.cartp_fk_fiche_metadonnees IS NULL) AND (carte_projet.cartp_utilisateur IS NULL)));
            ");

        $this->addSql("
            CREATE VIEW catalogue.incoherence_carte_sous_domaines AS
             SELECT carte_projet.pk_carte_projet,
                to_char(carte_projet.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS \"date de création de la carte\",
                carte_projet.cartp_nom AS \"nom de la métadonnée\",
                carte_projet.cartp_description AS \"résumé de la métadonnée\",
                carte_projet.cartp_format AS \"format de la carte\",
                carte_projet.cartp_fk_stockage_carte AS \"identifiant stockage mapfile\",
                carte_projet.cartp_fk_fiche_metadonnees AS \"identifiant de la fiche métadonnée\"
               FROM catalogue.carte_projet
              WHERE ((NOT (carte_projet.pk_carte_projet IN ( SELECT ssdom_visualise_carte.ssdcart_fk_carte_projet
                       FROM catalogue.ssdom_visualise_carte
                      WHERE (ssdom_visualise_carte.ssdcart_fk_carte_projet IS NOT NULL)))) AND (carte_projet.cartp_format = ANY (ARRAY[0, 1])));
            ");

        $this->addSql("
            CREATE VIEW catalogue.incoherence_couche_fiche_metadonnees AS
             SELECT couche_donnees.pk_couche_donnees,
                to_char(couche_donnees.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS \"date de création de la couche\",
                couche_donnees.couchd_nom AS \"nom de la métadonnée\",
                couche_donnees.couchd_description AS \"résumé de la métadonnée\",
                catalogue.prodige_couche_type_stockage(couche_donnees.couchd_type_stockage) AS \"type de stockage\",
                couche_donnees.couchd_emplacement_stockage AS \"emplacement de stockage\",
                couche_donnees.couchd_fk_acces_server AS \"identifiant du serveur\",
                couche_donnees.couchd_wms AS \"publiée en wms\",
                couche_donnees.couchd_wfs AS \"publiée en wfs\"
               FROM catalogue.couche_donnees
              WHERE (NOT (couche_donnees.pk_couche_donnees IN ( SELECT fiche_metadonnees.fmeta_fk_couche_donnees
                       FROM catalogue.fiche_metadonnees
                      WHERE (fiche_metadonnees.fmeta_fk_couche_donnees IS NOT NULL))));
            ");

        $this->addSql("
            CREATE VIEW catalogue.incoherence_couche_sous_domaines AS
             SELECT couche_donnees.pk_couche_donnees,
                to_char(couche_donnees.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS \"date de création de la couche\",
                couche_donnees.couchd_nom AS \"nom de la métadonnée\",
                couche_donnees.couchd_description AS \"résumé de la métadonnée\",
                catalogue.prodige_couche_type_stockage(couche_donnees.couchd_type_stockage) AS \"type de stockage\",
                couche_donnees.couchd_emplacement_stockage AS \"emplacement de stockage\",
                couche_donnees.couchd_fk_acces_server AS \"identifiant du serveur\",
                couche_donnees.couchd_wms AS \"publiée en wms\",
                couche_donnees.couchd_wfs AS \"publiée en wfs\"
               FROM catalogue.couche_donnees
              WHERE (NOT (couche_donnees.pk_couche_donnees IN ( SELECT ssdom_dispose_couche.ssdcouch_fk_couche_donnees
                       FROM catalogue.ssdom_dispose_couche
                      WHERE (ssdom_dispose_couche.ssdcouch_fk_couche_donnees IS NOT NULL))));
            ");

        $this->addSql("
            CREATE VIEW catalogue.incoherence_fiche_metadonnees_metadata AS
             SELECT fiche_metadonnees.pk_fiche_metadonnees,
                to_char(fiche_metadonnees.ts, 'DD Mon YYYY, HH24:MI:SS'::text) AS \"date de création de la fiche\",
                fiche_metadonnees.fmeta_id AS \"identifiant géosource\",
                fiche_metadonnees.statut
               FROM catalogue.fiche_metadonnees
              WHERE (((fiche_metadonnees.fmeta_id)::text = ''::text) OR (NOT ((fiche_metadonnees.fmeta_id)::bigint IN ( SELECT metadata.id
                       FROM public.metadata))));
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_liste_carte_publiees AS
             SELECT carte_projet.pk_carte_projet,
                carte_projet.cartp_nom AS \"nom de la carte\",
                carte_projet.cartp_description AS \"résumé\",
                carte_projet.cartp_format AS format,
                stockage_carte.stkcard_path AS stockage,
                metadata.changedate,
                metadata.createdate,
                metadata.id AS \"id metadata\",
                metadata.uuid AS \"uuid metadata\"
               FROM ((((catalogue.carte_projet
                 JOIN catalogue.stockage_carte ON ((carte_projet.cartp_fk_stockage_carte = stockage_carte.pk_stockage_carte)))
                 JOIN catalogue.fiche_metadonnees ON ((carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees)))
                 JOIN public.metadata ON (((fiche_metadonnees.fmeta_id)::text = (metadata.id)::text)))
                 JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))));
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_liste_couche_publiees AS
             SELECT couche_donnees.pk_couche_donnees,
                couche_donnees.couchd_nom AS \"nom de la couche\",
                couche_donnees.couchd_description AS \"résumé\",
                couche_donnees.couchd_type_stockage AS \"couche vecteur ?\",
                couche_donnees.couchd_emplacement_stockage AS \"emplacement de stockage\",
                couche_donnees.couchd_wms AS \"Publiée en WMS ?\",
                couche_donnees.couchd_wfs AS \"Publiée en WFS ?\",
                metadata.changedate,
                metadata.createdate,
                metadata.id AS \"id metadata\",
                metadata.uuid AS \"uuid metadata\"
               FROM (((catalogue.couche_donnees
                 JOIN catalogue.fiche_metadonnees ON ((couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
                 JOIN public.metadata ON (((fiche_metadonnees.fmeta_id)::text = (metadata.id)::text)))
                 JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))));
            ");

        $this->addSql("
            CREATE VIEW catalogue.metadata_list AS
             SELECT statistique_liste_couche_publiees.\"nom de la couche\" AS nom,
                statistique_liste_couche_publiees.\"uuid metadata\" AS id
               FROM catalogue.statistique_liste_couche_publiees
            UNION
             SELECT statistique_liste_carte_publiees.\"nom de la carte\" AS nom,
                statistique_liste_carte_publiees.\"uuid metadata\" AS id
               FROM catalogue.statistique_liste_carte_publiees;
            ");

        $this->addSql("
            CREATE VIEW catalogue.metadonnees_sdom AS
             SELECT domaine.pk_domaine,
                domaine.dom_rubric AS dom_coherence,
                domaine.dom_nom,
                sous_domaine.pk_sous_domaine,
                sous_domaine.ssdom_nom,
                fiche_metadonnees.pk_fiche_metadonnees,
                fiche_metadonnees.fmeta_id,
                fiche_metadonnees.fmeta_fk_couche_donnees
               FROM (((catalogue.domaine
                 JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
                 JOIN catalogue.ssdom_dispose_couche ON ((sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine)))
                 JOIN catalogue.fiche_metadonnees ON ((ssdom_dispose_couche.ssdcouch_fk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)));
            ");

        $this->addSql("
            CREATE VIEW catalogue.metadonnees_sdom_pub AS
             SELECT domaine.pk_domaine,
                domaine.dom_rubric AS dom_coherence,
                domaine.dom_nom,
                sous_domaine.pk_sous_domaine,
                sous_domaine.ssdom_nom,
                fiche_metadonnees.pk_fiche_metadonnees,
                fiche_metadonnees.fmeta_id,
                fiche_metadonnees.fmeta_fk_couche_donnees
               FROM (((((catalogue.domaine
                 JOIN catalogue.sous_domaine ON ((domaine.pk_domaine = sous_domaine.ssdom_fk_domaine)))
                 JOIN catalogue.ssdom_dispose_couche ON ((sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine)))
                 JOIN catalogue.fiche_metadonnees ON ((ssdom_dispose_couche.ssdcouch_fk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
                 JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
                 JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))));
            ");

        $this->addSql("
            CREATE VIEW catalogue.objet AS
             SELECT couche_donnees.pk_couche_donnees AS objet_id,
                couche_donnees.couchd_nom AS objet_nom,
                '1'::text AS objet_type_id
               FROM catalogue.couche_donnees
            UNION
             SELECT carte_projet.pk_carte_projet AS objet_id,
                carte_projet.cartp_nom AS objet_nom,
                '2'::text AS objet_type_id
               FROM catalogue.carte_projet
            UNION
             SELECT meta.pk_fiche_metadonnees AS objet_id,
                couche.couchd_nom AS objet_nom,
                '3'::text AS objet_type_id
               FROM ((catalogue.fiche_metadonnees meta
                 LEFT JOIN catalogue.couche_donnees couche ON ((meta.fmeta_fk_couche_donnees = couche.pk_couche_donnees)))
                 LEFT JOIN public.metadata ON (((meta.fmeta_id)::text = (metadata.id)::text)))
              WHERE ((meta.fmeta_fk_couche_donnees IS NOT NULL) AND (metadata.istemplate = 'n'::bpchar))
            UNION
             SELECT meta.pk_fiche_metadonnees AS objet_id,
                carte.cartp_nom AS objet_nom,
                '3'::text AS objet_type_id
               FROM ((catalogue.fiche_metadonnees meta
                 LEFT JOIN catalogue.carte_projet carte ON ((meta.pk_fiche_metadonnees = carte.cartp_fk_fiche_metadonnees)))
                 LEFT JOIN public.metadata ON (((meta.fmeta_id)::text = (metadata.id)::text)))
              WHERE ((carte.cartp_fk_fiche_metadonnees IS NOT NULL) AND (metadata.istemplate = 'n'::bpchar))
            UNION
             SELECT meta.pk_fiche_metadonnees AS objet_id,
                couche.couchd_nom AS objet_nom,
                '4'::text AS objet_type_id
               FROM ((catalogue.fiche_metadonnees meta
                 LEFT JOIN catalogue.couche_donnees couche ON ((meta.fmeta_fk_couche_donnees = couche.pk_couche_donnees)))
                 LEFT JOIN public.metadata ON (((meta.fmeta_id)::text = (metadata.id)::text)))
              WHERE ((meta.fmeta_fk_couche_donnees IS NOT NULL) AND (metadata.istemplate = 'y'::bpchar))
            UNION
             SELECT meta.pk_fiche_metadonnees AS objet_id,
                carte.cartp_nom AS objet_nom,
                '4'::text AS objet_type_id
               FROM ((catalogue.fiche_metadonnees meta
                 LEFT JOIN catalogue.carte_projet carte ON ((meta.pk_fiche_metadonnees = carte.cartp_fk_fiche_metadonnees)))
                 LEFT JOIN public.metadata ON (((meta.fmeta_id)::text = (metadata.id)::text)))
              WHERE ((carte.cartp_fk_fiche_metadonnees IS NOT NULL) AND (metadata.istemplate = 'y'::bpchar))
            UNION
             SELECT groupe_profil.pk_groupe_profil AS objet_id,
                groupe_profil.grp_nom AS objet_nom,
                '5'::text AS objet_type_id
               FROM catalogue.groupe_profil;
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_dernieres_mises_a_jour AS
             SELECT statistique_liste_couche_publiees.\"nom de la couche\" AS nom,
                statistique_liste_couche_publiees.\"résumé\",
                statistique_liste_couche_publiees.changedate AS date,
                'donnée'::text AS type,
                statistique_liste_couche_publiees.\"uuid metadata\" AS id,
                statistique_liste_couche_publiees.\"couche vecteur ?\" AS data_type
               FROM catalogue.statistique_liste_couche_publiees
            UNION
             SELECT statistique_liste_carte_publiees.\"nom de la carte\" AS nom,
                statistique_liste_carte_publiees.\"résumé\",
                statistique_liste_carte_publiees.changedate AS date,
                'carte'::text AS type,
                statistique_liste_carte_publiees.\"uuid metadata\" AS id,
                statistique_liste_carte_publiees.format AS data_type
               FROM catalogue.statistique_liste_carte_publiees
              ORDER BY 3 DESC
             LIMIT 30;
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_dernieres_nouveautes AS
             SELECT statistique_liste_couche_publiees.\"nom de la couche\" AS nom,
                statistique_liste_couche_publiees.\"résumé\",
                statistique_liste_couche_publiees.changedate AS date,
                statistique_liste_couche_publiees.createdate,
                'donnée'::text AS type,
                statistique_liste_couche_publiees.\"uuid metadata\" AS id,
                statistique_liste_couche_publiees.\"couche vecteur ?\" AS data_type
               FROM catalogue.statistique_liste_couche_publiees
            UNION
             SELECT statistique_liste_carte_publiees.\"nom de la carte\" AS nom,
                statistique_liste_carte_publiees.\"résumé\",
                statistique_liste_carte_publiees.changedate AS date,
                statistique_liste_carte_publiees.createdate,
                'carte'::text AS type,
                statistique_liste_carte_publiees.\"uuid metadata\" AS id,
                statistique_liste_carte_publiees.format AS data_type
               FROM catalogue.statistique_liste_carte_publiees
              ORDER BY 4 DESC
             LIMIT 30;
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_liste_metadata_publiees AS
             SELECT statistique_liste_couche_publiees.\"nom de la couche\" AS nom,
                statistique_liste_couche_publiees.\"résumé\",
                statistique_liste_couche_publiees.changedate AS date,
                statistique_liste_couche_publiees.createdate,
                'donnée'::text AS type,
                statistique_liste_couche_publiees.\"id metadata\" AS id,
                statistique_liste_couche_publiees.\"couche vecteur ?\" AS data_type
               FROM catalogue.statistique_liste_couche_publiees
            UNION
             SELECT statistique_liste_carte_publiees.\"nom de la carte\" AS nom,
                statistique_liste_carte_publiees.\"résumé\",
                statistique_liste_carte_publiees.changedate AS date,
                statistique_liste_carte_publiees.createdate,
                'carte'::text AS type,
                statistique_liste_carte_publiees.\"id metadata\" AS id,
                statistique_liste_carte_publiees.format AS data_type
               FROM catalogue.statistique_liste_carte_publiees;
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_metadata_date_creation AS
             SELECT metadata.id,
                substr(metadata.data, (strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47), ((strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList=\"./resources/codeList.xml#CI_DateTypeCode\"codeListValue=\"creation\"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text)) - 47)) AS date
               FROM ((( SELECT metadata_1.id,
                        replace(metadata_1.data, ' '::text, ''::text) AS data
                       FROM ( SELECT metadata_2.id,
                                replace(metadata_2.data, '

            '::text, ''::text) AS data
                               FROM public.metadata metadata_2) metadata_1) metadata
                 JOIN catalogue.fiche_metadonnees ON (((metadata.id)::text = (fiche_metadonnees.fmeta_id)::text)))
                 JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
              WHERE ((strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) > 0) AND (strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList=\"./resources/codeList.xml#CI_DateTypeCode\"codeListValue=\"creation\"/></gmd:dateType>'::text) > 0) AND (length(substr(metadata.data, (strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47), ((strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList=\"./resources/codeList.xml#CI_DateTypeCode\"codeListValue=\"creation\"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text)) - 47))) < 11));
            ");


        $this->addSql("
            CREATE VIEW catalogue.statistique_metadata_date_publication AS
             SELECT metadata.id,
                substr(metadata.data, (strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47), ((strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList=\"./resources/codeList.xml#CI_DateTypeCode\"codeListValue=\"publication\"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text)) - 47)) AS date
               FROM (( SELECT metadata_1.id,
                        replace(metadata_1.data, ' '::text, ''::text) AS data
                       FROM ( SELECT metadata_2.id,
                                replace(metadata_2.data, '

            '::text, ''::text) AS data
                               FROM public.metadata metadata_2) metadata_1) metadata
                 JOIN catalogue.fiche_metadonnees ON (((metadata.id)::text = (fiche_metadonnees.fmeta_id)::text)))
              WHERE ((strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) > 0) AND (strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList=\"./resources/codeList.xml#CI_DateTypeCode\"codeListValue=\"publication\"/></gmd:dateType>'::text) > 0) AND (fiche_metadonnees.statut = 4) AND (length(substr(metadata.data, (strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47), ((strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList=\"./resources/codeList.xml#CI_DateTypeCode\"codeListValue=\"publication\"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text)) - 47))) < 11));
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_metadata_date_revision AS
             SELECT metadata.id,
                substr(metadata.data, (strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47), ((strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList=\"./resources/codeList.xml#CI_DateTypeCode\"codeListValue=\"creation\"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text)) - 47)) AS date
               FROM ((( SELECT metadata_1.id,
                        replace(metadata_1.data, ' '::text, ''::text) AS data
                       FROM ( SELECT metadata_2.id,
                                replace(metadata_2.data, '

            '::text, ''::text) AS data
                               FROM public.metadata metadata_2) metadata_1) metadata
                 JOIN catalogue.fiche_metadonnees ON (((metadata.id)::text = (fiche_metadonnees.fmeta_id)::text)))
                 JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
              WHERE ((strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) > 0) AND (strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList=\"./resources/codeList.xml#CI_DateTypeCode\"codeListValue=\"creation\"/></gmd:dateType>'::text) > 0) AND (length(substr(metadata.data, (strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text) + 47), ((strpos(metadata.data, '</gco:DateTime></gmd:date><gmd:dateType><gmd:CI_DateTypeCodecodeList=\"./resources/codeList.xml#CI_DateTypeCode\"codeListValue=\"creation\"/></gmd:dateType></gmd:CI_Date></gmd:date>'::text) - strpos(metadata.data, '<gmd:date><gmd:CI_Date><gmd:date><gco:DateTime>'::text)) - 47))) < 11));
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_liste_metadonnees_prod_date_couche AS
             SELECT metadata.id,
                (xpath('//gmd:contact/gmd:CI_ResponsibleParty/gmd:organisationName/gco:CharacterString/text()'::text, (('<?xml version=\"1.0\" encoding=\"utf-8\"?>'::text || metadata.data))::xml, ARRAY[ARRAY['gmd'::text, 'http://www.isotc211.org/2005/gmd'::text], ARRAY['gco'::text, 'http://www.isotc211.org/2005/gco'::text]]))::text AS \"Producteur\",
                statistique_metadata_date_creation.date AS \"date de création\",
                statistique_metadata_date_revision.date AS \"date de révision\",
                statistique_metadata_date_publication.date AS \"date de publication\",
                couche_donnees.couchd_nom AS \"nom de la couche\",
                couche_donnees.couchd_description AS \"Résumé\"
               FROM ((((((public.metadata
                 JOIN catalogue.fiche_metadonnees ON (((metadata.id)::text = (fiche_metadonnees.fmeta_id)::text)))
                 JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
                 JOIN catalogue.couche_donnees ON ((fiche_metadonnees.fmeta_fk_couche_donnees = couche_donnees.pk_couche_donnees)))
                 LEFT JOIN catalogue.statistique_metadata_date_creation ON ((statistique_metadata_date_creation.id = metadata.id)))
                 LEFT JOIN catalogue.statistique_metadata_date_revision ON ((statistique_metadata_date_revision.id = metadata.id)))
                 LEFT JOIN catalogue.statistique_metadata_date_publication ON ((statistique_metadata_date_publication.id = metadata.id)))
              WHERE ((NOT (metadata.data IS NULL)) AND (metadata.data <> ''::text));
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_nombre_cartes_publiees_domaine AS
             SELECT rubric_param.rubric_name AS \"nom de la rubrique\",
                domaine.pk_domaine,
                domaine.dom_nom AS \"nom du domaine\",
                count(*) AS \"Nombre de cartes publiées\"
               FROM (((((((catalogue.fiche_metadonnees
                 JOIN catalogue.carte_projet ON ((carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees)))
                 JOIN catalogue.ssdom_visualise_carte ON ((carte_projet.pk_carte_projet = ssdom_visualise_carte.ssdcart_fk_carte_projet)))
                 JOIN catalogue.sous_domaine ON ((sous_domaine.pk_sous_domaine = ssdom_visualise_carte.ssdcart_fk_sous_domaine)))
                 JOIN catalogue.domaine ON ((sous_domaine.ssdom_fk_domaine = domaine.pk_domaine)))
                 JOIN catalogue.rubric_param ON ((domaine.dom_rubric = rubric_param.rubric_id)))
                 JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
                 JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
              WHERE (fiche_metadonnees.fmeta_fk_couche_donnees IS NULL)
              GROUP BY rubric_param.rubric_name, domaine.dom_nom, domaine.pk_domaine
              ORDER BY rubric_param.rubric_name, domaine.dom_nom;
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_nombre_cartes_publiees_sousdomaine AS
             SELECT rubric_param.rubric_name AS \"nom de la rubrique\",
                domaine.dom_nom AS \"nom du domaine\",
                sous_domaine.pk_sous_domaine,
                sous_domaine.ssdom_nom AS \"nom du sous-domaine\",
                count(*) AS \"Nombre de cartes publiées\"
               FROM (((((((catalogue.fiche_metadonnees
                 JOIN catalogue.carte_projet ON ((carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees)))
                 JOIN catalogue.ssdom_visualise_carte ON ((carte_projet.pk_carte_projet = ssdom_visualise_carte.ssdcart_fk_carte_projet)))
                 JOIN catalogue.sous_domaine ON ((sous_domaine.pk_sous_domaine = ssdom_visualise_carte.ssdcart_fk_sous_domaine)))
                 JOIN catalogue.domaine ON ((sous_domaine.ssdom_fk_domaine = domaine.pk_domaine)))
                 JOIN catalogue.rubric_param ON ((domaine.dom_rubric = rubric_param.rubric_id)))
                 JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
                 JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
              WHERE (fiche_metadonnees.fmeta_fk_couche_donnees IS NULL)
              GROUP BY rubric_param.rubric_name, domaine.dom_nom, sous_domaine.ssdom_nom, sous_domaine.pk_sous_domaine
              ORDER BY rubric_param.rubric_name, domaine.dom_nom, sous_domaine.ssdom_nom;
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_nombre_couches_publiees_domaine AS
             SELECT rubric_param.rubric_name AS \"nom de la rubrique\",
                domaine.pk_domaine,
                domaine.dom_nom AS \"nom du domaine\",
                count(*) AS \"Nombre de couches publiées\"
               FROM (((((((catalogue.fiche_metadonnees
                 JOIN catalogue.couche_donnees ON ((couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
                 JOIN catalogue.ssdom_dispose_couche ON ((couche_donnees.pk_couche_donnees = ssdom_dispose_couche.ssdcouch_fk_couche_donnees)))
                 JOIN catalogue.sous_domaine ON ((sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine)))
                 JOIN catalogue.domaine ON ((sous_domaine.ssdom_fk_domaine = domaine.pk_domaine)))
                 JOIN catalogue.rubric_param ON ((domaine.dom_rubric = rubric_param.rubric_id)))
                 JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
                 JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
              GROUP BY rubric_param.rubric_name, domaine.dom_nom, domaine.pk_domaine
              ORDER BY rubric_param.rubric_name, domaine.dom_nom;
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_nombre_couches_publiees_sousdomaine AS
             SELECT rubric_param.rubric_name AS \"nom de la rubrique\",
                domaine.dom_nom AS \"nom du domaine\",
                sous_domaine.pk_sous_domaine,
                sous_domaine.ssdom_nom AS \"nom du sous-domaine\",
                count(*) AS \"Nombre de couches publiées\"
               FROM (((((((catalogue.fiche_metadonnees
                 JOIN catalogue.couche_donnees ON ((couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
                 JOIN catalogue.ssdom_dispose_couche ON ((couche_donnees.pk_couche_donnees = ssdom_dispose_couche.ssdcouch_fk_couche_donnees)))
                 JOIN catalogue.sous_domaine ON ((sous_domaine.pk_sous_domaine = ssdom_dispose_couche.ssdcouch_fk_sous_domaine)))
                 JOIN catalogue.domaine ON ((sous_domaine.ssdom_fk_domaine = domaine.pk_domaine)))
                 JOIN catalogue.rubric_param ON ((domaine.dom_rubric = rubric_param.rubric_id)))
                 JOIN public.metadata ON ((metadata.id = (fiche_metadonnees.fmeta_id)::bigint)))
                 JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
              GROUP BY rubric_param.rubric_name, domaine.dom_nom, sous_domaine.pk_sous_domaine, sous_domaine.ssdom_nom
              ORDER BY rubric_param.rubric_name, domaine.dom_nom;
            ");

        $this->addSql("
            CREATE VIEW catalogue.statistique_ogc AS
             SELECT couche_donnees.pk_couche_donnees,
                couche_donnees.couchd_nom,
                couche_donnees.couchd_type_stockage,
                couche_donnees.couchd_emplacement_stockage,
                couche_donnees.couchd_wms,
                couche_donnees.couchd_wfs,
                metadata.changedate,
                metadata.createdate,
                metadata.id,
                metadata.uuid
               FROM (((catalogue.couche_donnees
                 JOIN catalogue.fiche_metadonnees ON ((couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees)))
                 JOIN public.metadata ON (((fiche_metadonnees.fmeta_id)::text = (metadata.id)::text)))
                 JOIN public.operationallowed ON (((metadata.id = operationallowed.metadataid) AND (operationallowed.operationid = 0) AND (operationallowed.groupid = 1))))
              WHERE ((couche_donnees.couchd_wms = 1) OR (couche_donnees.couchd_wfs = 1));
            ");

        $this->addSql("
            CREATE VIEW catalogue.traitements_utilisateur AS
             SELECT utilisateur.pk_utilisateur,
                utilisateur.usr_nom,
                groupe_profil.pk_groupe_profil,
                groupe_profil.grp_nom,
                traitement.trt_id,
                traitement.trt_administre,
                traitement.trt_accede,
                traitement.trt_edite
               FROM ((((catalogue.utilisateur
                 JOIN catalogue.grp_regroupe_usr ON ((utilisateur.pk_utilisateur = grp_regroupe_usr.grpusr_fk_utilisateur)))
                 JOIN catalogue.groupe_profil ON ((grp_regroupe_usr.grpusr_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
                 JOIN catalogue.grp_autorise_trt ON ((groupe_profil.pk_groupe_profil = grp_autorise_trt.grptrt_fk_groupe_profil)))
                 JOIN catalogue.traitement ON ((grp_autorise_trt.grptrt_fk_traitement = traitement.pk_traitement)));
            ");

        $this->addSql("
            CREATE VIEW catalogue.utilisateurs_domaine AS
             SELECT domaine.pk_domaine,
                domaine.dom_nom,
                groupe_profil.pk_groupe_profil,
                groupe_profil.grp_nom,
                utilisateur.pk_utilisateur,
                utilisateur.usr_id
               FROM ((((catalogue.domaine
                 JOIN catalogue.grp_accede_dom ON ((domaine.pk_domaine = grp_accede_dom.grpdom_fk_domaine)))
                 JOIN catalogue.groupe_profil ON ((grp_accede_dom.grpdom_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
                 JOIN catalogue.grp_regroupe_usr ON ((grp_regroupe_usr.grpusr_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
                 JOIN catalogue.utilisateur ON ((grp_regroupe_usr.grpusr_fk_utilisateur = utilisateur.pk_utilisateur)));
            ");

        $this->addSql("
            CREATE VIEW catalogue.utilisateurs_sous_domaine AS
             SELECT sous_domaine.pk_sous_domaine,
                sous_domaine.ssdom_nom,
                groupe_profil.pk_groupe_profil,
                groupe_profil.grp_nom,
                utilisateur.pk_utilisateur,
                utilisateur.usr_id
               FROM ((((catalogue.sous_domaine
                 JOIN catalogue.grp_accede_ssdom ON ((sous_domaine.pk_sous_domaine = grp_accede_ssdom.grpss_fk_sous_domaine)))
                 JOIN catalogue.groupe_profil ON ((grp_accede_ssdom.grpss_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
                 JOIN catalogue.grp_regroupe_usr ON ((grp_regroupe_usr.grpusr_fk_groupe_profil = groupe_profil.pk_groupe_profil)))
                 JOIN catalogue.utilisateur ON ((grp_regroupe_usr.grpusr_fk_utilisateur = utilisateur.pk_utilisateur)));
            ");

        $this->addSql("
            CREATE VIEW catalogue.v_acces_couche AS
             SELECT cd.pk_couche_donnees AS couche_pk,
                cd.couchd_id AS couche_id,
                cd.couchd_nom AS couche_nom,
                cd.couchd_description AS couche_desc,
                cd.couchd_emplacement_stockage AS couche_table,
                cd.couchd_type_stockage AS couche_type,
                acces_serveur.accs_adresse AS couche_srv,
                acces_serveur.accs_service_admin AS couche_service_admin,
                acces_serveur.accs_adresse_admin AS couche_srv_admin,
                acces_serveur.accs_login_admin,
                acces_serveur.accs_pwd_admin
               FROM (catalogue.couche_donnees cd
                 LEFT JOIN catalogue.acces_serveur ON ((cd.couchd_fk_acces_server = acces_serveur.pk_acces_serveur)));
            ");

        $this->addSql("
            CREATE VIEW catalogue.v_desc_metadata AS
             SELECT m.id AS metadataid,
                cd.couchd_nom AS couchenom,
                cd.couchd_type_stockage AS couchetype,
                acs.accs_adresse AS couchesrv,
                acs.accs_service_admin AS couchesrv_service,
                acs.accs_adresse_tele AS couchesrv_tele,
                cd.couchd_emplacement_stockage AS couchetable,
                m.data AS metadataxml,
                fm.schema
               FROM (((public.metadata m
                 LEFT JOIN catalogue.fiche_metadonnees fm ON (((m.id)::text = (fm.fmeta_id)::text)))
                 JOIN catalogue.couche_donnees cd ON ((fm.fmeta_fk_couche_donnees = cd.pk_couche_donnees)))
                 JOIN catalogue.acces_serveur acs ON ((cd.couchd_fk_acces_server = acs.pk_acces_serveur)));
            ");

        $this->addSql("
            CREATE VIEW catalogue.v_desc_raster AS
             SELECT m.id AS metadataid,
                r.id AS layerid,
                r.name,
                r.srid,
                r.ulx,
                r.uly,
                r.lrx,
                r.lry,
                r.bands,
                r.cols,
                r.rows,
                r.resx,
                r.resy,
                r.format,
                r.vrt_path,
                r.compression,
                r.color_interp
               FROM ((public.metadata m
                 JOIN catalogue.fiche_metadonnees fm ON (((m.id)::text = (fm.fmeta_id)::text)))
                 JOIN catalogue.raster_info r ON ((fm.fmeta_fk_couche_donnees = r.id)));
            ");

        $this->addSql("
            CREATE VIEW catalogue.v_utilisateur AS
             SELECT utilisateur.pk_utilisateur,
                utilisateur.usr_id,
                utilisateur.usr_nom,
                utilisateur.usr_prenom,
                utilisateur.usr_email
               FROM catalogue.utilisateur
              WHERE ((utilisateur.date_expiration_compte IS NULL) OR ((utilisateur.date_expiration_compte IS NOT NULL) AND (utilisateur.date_expiration_compte > now())));
            ");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
        
        $this->addSql('set search_path to CATALOGUE, public');

        $this->addSql("DROP VIEW catalogue.v_utilisateur");
        $this->addSql("DROP VIEW catalogue.v_desc_raster");
        $this->addSql("DROP VIEW catalogue.v_desc_metadata");
        $this->addSql("DROP VIEW catalogue.v_acces_couche");
        $this->addSql("DROP VIEW catalogue.utilisateurs_sous_domaine");
        $this->addSql("DROP VIEW catalogue.utilisateurs_domaine");
        $this->addSql("DROP VIEW catalogue.traitements_utilisateur");
        $this->addSql("DROP VIEW catalogue.statistique_ogc");
        $this->addSql("DROP VIEW catalogue.statistique_nombre_couches_publiees_sousdomaine");
        $this->addSql("DROP VIEW catalogue.statistique_nombre_couches_publiees_domaine");
        $this->addSql("DROP VIEW catalogue.statistique_nombre_cartes_publiees_sousdomaine");
        $this->addSql("DROP VIEW catalogue.statistique_nombre_cartes_publiees_domaine");
        $this->addSql("DROP VIEW catalogue.statistique_liste_metadonnees_prod_date_couche");
        $this->addSql("DROP VIEW catalogue.statistique_metadata_date_revision");
        $this->addSql("DROP VIEW catalogue.statistique_metadata_date_publication");
        $this->addSql("DROP VIEW catalogue.statistique_metadata_date_creation");
        $this->addSql("DROP VIEW catalogue.statistique_liste_metadata_publiees");
        $this->addSql("DROP VIEW catalogue.statistique_dernieres_nouveautes");
        $this->addSql("DROP VIEW catalogue.statistique_dernieres_mises_a_jour");
        $this->addSql("DROP VIEW catalogue.objet");
        $this->addSql("DROP VIEW catalogue.metadonnees_sdom_pub");
        $this->addSql("DROP VIEW catalogue.metadonnees_sdom");
        $this->addSql("DROP VIEW catalogue.metadata_list");
        $this->addSql("DROP VIEW catalogue.statistique_liste_couche_publiees");
        $this->addSql("DROP VIEW catalogue.statistique_liste_carte_publiees");
        $this->addSql("DROP VIEW catalogue.incoherence_fiche_metadonnees_metadata");
        $this->addSql("DROP VIEW catalogue.incoherence_couche_sous_domaines");
        $this->addSql("DROP VIEW catalogue.incoherence_couche_fiche_metadonnees");
        $this->addSql("DROP VIEW catalogue.incoherence_carte_sous_domaines");
        $this->addSql("DROP VIEW catalogue.incoherence_carte_fiche_metadonnees");
        $this->addSql("DROP VIEW catalogue.editeurs_sous_domaine");
        $this->addSql("DROP VIEW catalogue.dom_sdom");
        $this->addSql("DROP VIEW catalogue.couche_sdom");
        $this->addSql("DROP VIEW catalogue.cartes_sdom_pub");
        $this->addSql("DROP VIEW catalogue.cartes_sdom");
        $this->addSql("DROP VIEW catalogue.administrateurs_sous_domaine");
    }
}

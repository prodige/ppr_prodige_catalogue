<?php


declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220726000000 extends AbstractMigration
{
   public function getDescription(): string
    {
        return 'migrate raster_info path';
    }

    public function up(Schema $schema): void
    {    // migrate raster_info path
        $this->addSql("update catalogue.raster_info set vrt_path =  replace (vrt_path, '/home/prodige', '/var/www/html/data');");
    }


    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}

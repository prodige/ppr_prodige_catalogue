<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191129040100 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Création des tables de la base CATALOGUE';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            'postgresql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('CREATE SCHEMA CATALOGUE');
        $this->addSql('CREATE SCHEMA bdterr');
        $this->addSql('set search_path to public');

        $this->addSql("drop function if exists catalogue.prodige_couche_type_stockage(i integer)");
        $this->addSql(
            "
          CREATE FUNCTION catalogue.prodige_couche_type_stockage(i integer) RETURNS character varying
              LANGUAGE plpgsql
              AS $$
          BEGIN
             IF i=0 THEN
              RETURN 'Raster';
             ELSIF i=1 THEN
              RETURN 'Vecteur';
             ELSIF i=-1 THEN
              RETURN 'Ensenble de séries de données';
             ELSIF i=-2 THEN
              RETURN 'Majic';
             ELSIF i=-3 THEN
              RETURN 'Tableau';
             ELSIF i=-4 THEN
              RETURN 'Vue';
            ELSE
               RETURN 'Type Inconnu';
          END IF;
          END;
          \$\$
          "
        );

        $this->addSql("drop function if exists public.st_aslatlontext(public.geometry)");
        $this->addSql(
            "
          CREATE FUNCTION public.st_aslatlontext(public.geometry) RETURNS text
              LANGUAGE sql IMMUTABLE STRICT
              AS \$_\$ SELECT ST_AsLatLonText($1, '') \$_\$;
          "
        );

        /*$this->addSql("
           CREATE OPERATOR FAMILY public.gist_geometry_ops USING gist;
           ");*/

        $this->addSql(
            "CREATE TABLE bdterr.bdterr_champ_type (    champtype_id integer NOT NULL,    champtype_nom character varying(255) DEFAULT NULL::character varying,    champtype_incontenutier boolean DEFAULT true)"
        );
        $this->addSql(
            "CREATE TABLE bdterr.bdterr_contenus_tiers (    contenu_id integer NOT NULL,    lot_id integer,    contenu_type integer,    contenu_nom character varying(255) DEFAULT NULL::character varying,    contenu_ordre integer,    contenu_fiche boolean,    contenu_resultats boolean,    contenu_url character varying(255) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE bdterr.bdterr_couche_champ (    champ_id integer NOT NULL,    champ_lot_id integer NOT NULL,    champ_type integer,    champ_nom character varying(150) NOT NULL,    champ_alias character varying(255) DEFAULT NULL::character varying,    champ_ordre integer,    champ_format character varying(255) DEFAULT NULL::character varying,    champ_fiche boolean,    champ_resultats boolean,    champ_identifiant boolean,    champ_label boolean)"
        );
        $this->addSql(
            "CREATE TABLE bdterr.bdterr_donnee (    donnee_id integer NOT NULL,    donnee_lot_id integer,    donnee_objet_id text,    donnee_objet_nom text,    donnee_geom text,    donnee_geom_json text,    donnee_attributs_fiche text,    donnee_attributs_resultat text,    donnee_ressources_resultat text,    donnee_ressources_fiche text,    donnee_insee text,    donnee_insee_delegue text)"
        );
        $this->addSql(
            "CREATE TABLE bdterr.bdterr_lot (    lot_id integer NOT NULL,    lot_theme_id integer,    lot_referentiel_id integer,    lot_uuid character varying(255) DEFAULT NULL::character varying,    lot_table character varying(255) DEFAULT NULL::character varying,    lot_alias character varying(255) DEFAULT NULL::character varying,    lot_carte character varying(255) DEFAULT NULL::character varying,    lot_avertissement_message character varying(255) DEFAULT NULL::character varying,    lot_statistiques boolean,    lot_visualiseur boolean,    lot_date_maj timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,    lot_statistic text,    lot_layer_name text)"
        );
        $this->addSql(
            "CREATE TABLE bdterr.bdterr_rapports (    rapport_id integer NOT NULL,    rapport_nom character varying(255) DEFAULT NULL::character varying,    rapport_gabarit_html character varying(255) DEFAULT NULL::character varying,    rapport_gabarit_odt character varying(255) DEFAULT NULL::character varying)"
        );
        $this->addSql(
            "CREATE TABLE bdterr.bdterr_rapports_profils (    id integer NOT NULL,    bterr_rapport_id integer,    prodige_profil_id integer)"
        );
        $this->addSql(
            "CREATE TABLE bdterr.bdterr_referentiel_carto (    referentiel_id integer NOT NULL,    referentiel_table text NOT NULL,    referentiel_champs text)"
        );
        $this->addSql(
            "CREATE SEQUENCE bdterr.bdterr_referentiel_carto_referentiel_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE bdterr.bdterr_referentiel_intersection (    referentiel_id integer NOT NULL,    referentiel_table character varying(255) DEFAULT NULL::character varying,    referentiel_insee character varying(255) DEFAULT NULL::character varying,    referentiel_insee_deleguee character varying(255) DEFAULT NULL::character varying,    referentiel_nom character varying(255) DEFAULT NULL::character varying)"
        );
        $this->addSql(
            "CREATE TABLE bdterr.bdterr_referentiel_recherche (    referentiel_id integer NOT NULL,    referentiel_table character varying(255) DEFAULT NULL::character varying,    referentiel_insee character varying(255) DEFAULT NULL::character varying,    referentiel_insee_delegue character varying(255) DEFAULT NULL::character varying,    referentiel_commune character varying(255) DEFAULT NULL::character varying,    referentiel_nom text,    referentiel_api_champs text)"
        );
        $this->addSql(
            "CREATE TABLE bdterr.bdterr_themes (    theme_id integer NOT NULL,    theme_parent integer,    theme_nom character varying(255) NOT NULL,    theme_alias text,    theme_ordre integer)"
        );
        $this->addSql(
            "CREATE SEQUENCE bdterr.champ_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE bdterr.champtype_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE bdterr.contenu_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE bdterr.donnee_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE bdterr.lot_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE bdterr.rapport_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE bdterr.rapports_profils_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE bdterr.referentiel_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE bdterr.referentiel_recherche_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE bdterr.theme_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_acces_serveur    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.acces_serveur (    pk_acces_serveur integer DEFAULT nextval('catalogue.seq_acces_serveur'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    accs_id character varying(128) NOT NULL,    accs_adresse character varying(128) NOT NULL,    path_consultation character varying(128) DEFAULT '/PRRA/Consultation/?map='::character varying NOT NULL,    path_administration character varying(128) DEFAULT '/PRRA/AdministrationCarto/?map='::character varying NOT NULL,    path_carte_statique character varying(128) DEFAULT '/CartesStatiques/'::character varying,    accs_adresse_admin text,    accs_adresse_tele text,    accs_login_admin text,    accs_pwd_admin text,    accs_service_admin integer,    accs_adresse_data text)"
        );
        $this->addSql(
            "COMMENT ON COLUMN catalogue.acces_serveur.path_consultation IS 'Chemin d''acces au site de consultation'"
        );
        $this->addSql(
            "COMMENT ON COLUMN catalogue.acces_serveur.path_administration IS 'Chemin d''acces au site d''administration'"
        );
        $this->addSql(
            "COMMENT ON COLUMN catalogue.acces_serveur.path_carte_statique IS 'Chemin d''acces aux cartes statiques'"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_domaine    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.domaine (    pk_domaine integer DEFAULT nextval('catalogue.seq_domaine'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    dom_id character varying(30) NOT NULL,    dom_nom character varying(128) NOT NULL,    dom_description character varying(1024),    dom_rubric integer DEFAULT 0 NOT NULL)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_groupe_profil    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.groupe_profil (    pk_groupe_profil integer DEFAULT nextval('catalogue.seq_groupe_profil'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    grp_id character varying(30) NOT NULL,    grp_nom character varying(128) NOT NULL,    grp_description character varying(1024),    grp_is_default_installation integer DEFAULT 0 NOT NULL,    grp_nom_ldap character varying(128))"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_grp_regroupe_usr    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.grp_regroupe_usr (    pk_grp_regroupe_usr integer DEFAULT nextval('catalogue.seq_grp_regroupe_usr'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    grpusr_fk_groupe_profil integer,    grpusr_fk_utilisateur integer,    grpusr_is_principal integer DEFAULT 0)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_sous_domaine    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_utilisateur    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.sous_domaine (    pk_sous_domaine integer DEFAULT nextval('catalogue.seq_sous_domaine'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    ssdom_id character varying(30) NOT NULL,    ssdom_nom character varying(128) NOT NULL,    ssdom_description character varying(1024),    ssdom_fk_domaine integer,    ssdom_admin_fk_groupe_profil integer,    ssdom_createur_fk_groupe_profil integer,    ssdom_editeur_fk_groupe_profil integer,    ssdom_service_wms integer DEFAULT 0,    ssdom_service_wms_uuid character varying(250))"
        );
        $this->addSql(
            "CREATE TABLE catalogue.utilisateur (    pk_utilisateur integer DEFAULT nextval('catalogue.seq_utilisateur'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    usr_id character varying(128) NOT NULL,    usr_nom character varying(128) NOT NULL,    usr_prenom character varying(128),    usr_email character varying(128),    usr_telephone character varying(30),    usr_telephone2 character varying(30),    usr_service character varying(128),    usr_description character varying(1024),    usr_password character varying(128),    usr_pwdexpire date DEFAULT now() NOT NULL,    usr_generic integer DEFAULT 0 NOT NULL,    usr_ldap integer DEFAULT 0,    usr_signature text,    date_expiration_compte date)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_carte_projet    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.carte_projet (    pk_carte_projet integer DEFAULT nextval('catalogue.seq_carte_projet'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    cartp_id character varying(30) NOT NULL,    cartp_nom character varying(255) NOT NULL,    cartp_description text,    cartp_format integer DEFAULT 0 NOT NULL,    cartp_fk_stockage_carte integer,    cartp_fk_fiche_metadonnees integer,    cartp_utilisateur integer,    cartep_etat integer DEFAULT 0,    cartp_administrateur integer,    cartp_utilisateur_email character varying(128),    cartp_wms integer DEFAULT 0)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_fiche_metadonnees    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.fiche_metadonnees (    pk_fiche_metadonnees integer DEFAULT nextval('catalogue.seq_fiche_metadonnees'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    fmeta_id character varying(128) NOT NULL,    fmeta_description character varying(1024),    fmeta_fk_couche_donnees integer,    statut integer DEFAULT 1 NOT NULL,    schema character varying(128))"
        );
        $this->addSql(
            "COMMENT ON COLUMN catalogue.fiche_metadonnees.statut IS 'Statut de publication de la fiche (1 - projet, 2 - attente validation, 3- validee, 4 - publiee)'"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_ssdom_visualise_carte    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_stockage_carte    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.ssdom_visualise_carte (    pk_ssdom_visualise_carte integer DEFAULT nextval('catalogue.seq_ssdom_visualise_carte'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    ssdcart_fk_carte_projet integer,    ssdcart_fk_sous_domaine integer)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.stockage_carte (    pk_stockage_carte integer DEFAULT nextval('catalogue.seq_stockage_carte'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    stkcard_id character varying(30) NOT NULL,    stkcard_path character varying(1024) NOT NULL,    stk_server integer)"
        );
        $this->addSql(
            "CREATE TABLE public.metadata (    id integer NOT NULL,    uuid character varying(250) NOT NULL,    schemaid character varying(32) NOT NULL,    istemplate character(1) DEFAULT 'n'::bpchar NOT NULL,    isharvested character(1) DEFAULT 'n'::bpchar NOT NULL,    createdate character varying(30) NOT NULL,    changedate character varying(30) NOT NULL,    data text NOT NULL,    source character varying(250) NOT NULL,    title character varying(255),    root character varying(255),    harvestuuid character varying(250) DEFAULT NULL::character varying,    owner integer NOT NULL,    groupowner integer,    harvesturi character varying(512) DEFAULT NULL::character varying,    rating integer DEFAULT 0 NOT NULL,    popularity integer DEFAULT 0 NOT NULL,    displayorder integer,    doctype character varying(255),    extra character varying(255))"
        );
        $this->addSql(
            "CREATE TABLE public.operationallowed (    groupid integer NOT NULL,    metadataid integer NOT NULL,    operationid integer NOT NULL)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_competence    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.competence (    pk_competence_id integer DEFAULT nextval('catalogue.seq_competence'::regclass) NOT NULL,    competence_nom character varying)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_competence_accede_carte    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.competence_accede_carte (    pk_competence_accede_carte integer DEFAULT nextval('catalogue.seq_competence_accede_carte'::regclass) NOT NULL,    competencecarte_fk_competence_id integer,    competencecarte_fk_carte_projet integer)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_competence_accede_couche    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.competence_accede_couche (    pk_competence_accede_couche integer DEFAULT nextval('catalogue.seq_competence_accede_couche'::regclass) NOT NULL,    competencecouche_fk_competence_id integer,    competencecouche_fk_couche_donnees integer)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_couche_donnees    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.couche_donnees (    pk_couche_donnees integer DEFAULT nextval('catalogue.seq_couche_donnees'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    couchd_id character varying(128) NOT NULL,    couchd_nom character varying(255) NOT NULL,    couchd_description character varying(2048),    couchd_type_stockage integer,    couchd_emplacement_stockage character varying(1024),    couchd_fk_acces_server integer,    couchd_wms integer DEFAULT 0,    couchd_wfs integer DEFAULT 0,    couchd_download integer DEFAULT 0 NOT NULL,    couchd_restriction integer DEFAULT 0 NOT NULL,    couchd_restriction_exclusif integer DEFAULT 0 NOT NULL,    couchd_zonageid_fk integer,    couchd_restriction_buffer double precision,    couchd_visualisable integer DEFAULT 1 NOT NULL,    couchd_extraction_attributaire integer DEFAULT 0 NOT NULL,    couchd_extraction_attributaire_champ character varying DEFAULT ''::character varying,    changedate timestamp with time zone DEFAULT now() NOT NULL,    couchd_alert_msg_id integer,    couchd_wfs_uuid character varying(250) DEFAULT NULL::character varying,    couchd_wms_sdom integer[])"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_ssdom_dispose_couche    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.ssdom_dispose_couche (    pk_ssdom_dispose_couche integer DEFAULT nextval('catalogue.seq_ssdom_dispose_couche'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    ssdcouch_fk_couche_donnees integer,    ssdcouch_fk_sous_domaine integer)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.execution_report (    id integer NOT NULL,    scheduled_command_id integer,    date_time timestamp(0) without time zone NOT NULL,    file character varying(255) NOT NULL,    status character varying(255) DEFAULT NULL::character varying)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.execution_report_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_grp_accede_competence    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.grp_accede_competence (    pk_grp_accede_competence integer DEFAULT nextval('catalogue.seq_grp_accede_competence'::regclass) NOT NULL,    fk_grp_id integer NOT NULL,    fk_competence_id integer NOT NULL)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_grp_accede_dom    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.grp_accede_dom (    pk_grp_accede_dom integer DEFAULT nextval('catalogue.seq_grp_accede_dom'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    grpdom_fk_domaine integer,    grpdom_fk_groupe_profil integer)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_grp_accede_ssdom    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.grp_accede_ssdom (    pk_grp_accede_ssdom integer DEFAULT nextval('catalogue.seq_grp_accede_ssdom'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    grpss_fk_sous_domaine integer,    grpss_fk_groupe_profil integer)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_grp_autorise_trt    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.grp_autorise_trt (    pk_grp_autorise_trt integer DEFAULT nextval('catalogue.seq_grp_autorise_trt'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    grptrt_fk_traitement integer,    grptrt_fk_groupe_profil integer)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_grp_trt_objet    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.grp_trt_objet (    pk_grp_trt_objet integer DEFAULT nextval('catalogue.seq_grp_trt_objet'::regclass) NOT NULL,    grptrtobj_fk_grp_id integer,    grptrtobj_fk_trt_id integer,    grptrtobj_fk_objet_id integer,    grptrtobj_fk_obj_type_id integer,    grp_trt_objet_status integer DEFAULT 0)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.harves_opendata_node (    pk_node_id integer NOT NULL,    node_name text,    node_logo text,    api_url text,    node_last_modification text,    url_dcat text,    node_log_file text,    command_id integer DEFAULT 0 NOT NULL)"
        );
        $this->addSql("COMMENT ON TABLE catalogue.harves_opendata_node IS 'moissonnage DCAT'");
        $this->addSql(
            "CREATE TABLE catalogue.harves_opendata_node_params (    pk_params_id integer NOT NULL,    fk_node_id integer NOT NULL,    key text,    value text)"
        );
        $this->addSql(
            "COMMENT ON TABLE catalogue.harves_opendata_node_params IS 'paramètres nœuds de moissonnage DCAT'"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.\"harves_opendata_node_params_FK_node_id_seq\"    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "ALTER SEQUENCE catalogue.\"harves_opendata_node_params_FK_node_id_seq\" OWNED BY catalogue.harves_opendata_node_params.fk_node_id"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.harves_opendata_node_params_pk_params_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "ALTER SEQUENCE catalogue.harves_opendata_node_params_pk_params_id_seq OWNED BY catalogue.harves_opendata_node_params.pk_params_id"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.harves_opendata_node_pk_node_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "ALTER SEQUENCE catalogue.harves_opendata_node_pk_node_id_seq OWNED BY catalogue.harves_opendata_node.pk_node_id"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_objet_type    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.objet_type (    pk_objet_type_id integer DEFAULT nextval('catalogue.seq_objet_type'::regclass) NOT NULL,    objet_type_nom character varying(128) NOT NULL)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_perimetre    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.perimetre (    pk_perimetre_id integer DEFAULT nextval('catalogue.seq_perimetre'::regclass) NOT NULL,    perimetre_code character varying,    perimetre_nom character varying,    perimetre_zonage_id integer)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.prodige_carto_colors_prodige_carto_color_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.prodige_carto_colors (    prodige_carto_color_id integer DEFAULT nextval('catalogue.prodige_carto_colors_prodige_carto_color_id_seq'::regclass) NOT NULL,    color_theme character varying(10),    color_white character varying(6),    color_light character varying(6),    color_dark character varying(6),    color_light_grey character varying(6),    color_tables character varying(6),    color_grey character varying(6),    color_theme_extjs text)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.prodige_colors_prodige_color_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.prodige_colors (    prodige_color_id integer DEFAULT nextval('catalogue.prodige_colors_prodige_color_id_seq'::regclass) NOT NULL,    color_theme character varying(10),    color_back character varying(7),    color_tables character varying(7),    color_text character varying(7),    color_text_light character varying(7),    volet_url character varying(30),    css_file text)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.prodige_database_requests_prodige_database_request_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.prodige_database_requests (    prodige_database_request_id integer DEFAULT nextval('catalogue.prodige_database_requests_prodige_database_request_id_seq'::regclass) NOT NULL,    request_description character varying(30),    sql_request text,    viewname character varying(128))"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_prodige_external_access    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.prodige_external_access (    pk_prodige_external_access integer DEFAULT nextval('catalogue.seq_prodige_external_access'::regclass) NOT NULL,    prodige_extaccess_referer text NOT NULL,    prodige_extaccess_fk_utilisateur integer NOT NULL)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.prodige_fonts_font_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.prodige_fonts (    font_id integer DEFAULT nextval('catalogue.prodige_fonts_font_id_seq'::regclass) NOT NULL,    font1 character varying(20),    font2 character varying(20),    font3 character varying(20))"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.prodige_geosource_colors_prodige_geosource_color_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.prodige_geosource_colors (    prodige_geosource_color_id integer DEFAULT nextval('catalogue.prodige_geosource_colors_prodige_geosource_color_id_seq'::regclass) NOT NULL,    geosource_color_theme character varying(10),    color_commontext character varying(6),    color_a character varying(6),    color_a_hover character varying(6),    color_left_menu_context character varying(6),    color_border character varying(6),    color_background_content character varying(6),    color_left_menu_text character varying(6))"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_prodige_help    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.prodige_help (    pk_prodige_help_id integer DEFAULT nextval('catalogue.seq_prodige_help'::regclass) NOT NULL,    prodige_help_title text NOT NULL,    prodige_help_desc text,    prodige_help_url text,    prodige_help_row_number integer)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_prodige_help_group    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.prodige_help_group (    hlpgrp_fk_help_id integer NOT NULL,    hlpgrp_fk_groupe_profil integer NOT NULL,    pk_prodige_help_group integer DEFAULT nextval('catalogue.seq_prodige_help_group'::regclass) NOT NULL)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.prodige_param_pk_prodige_param_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.prodige_param (    pk_prodige_param integer DEFAULT nextval('catalogue.prodige_param_pk_prodige_param_seq'::regclass) NOT NULL,    prodige_title character varying(255),    prodige_subtitle character varying(255),    prodige_catalog_intro text,    logo_url character varying(255),    top_image_url character varying(255),    font_id integer,    prodige_color_id integer,    prodige_geosource_color_id integer,    prodige_carto_color_id integer,    logo_right_url character varying(255))"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.prodige_server_server_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.prodige_session_user (    pk_session_user character varying(128) NOT NULL,    date_connexion timestamp without time zone NOT NULL)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_prodige_settings    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.prodige_settings (    pk_prodige_settings_id integer DEFAULT nextval('catalogue.seq_prodige_settings'::regclass) NOT NULL,    prodige_settings_constant text NOT NULL,    prodige_settings_title text,    prodige_settings_value text,    prodige_settings_desc text,    prodige_settings_type text DEFAULT 'textfield'::text NOT NULL,    prodige_settings_param text)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.raster_info (    id bigint NOT NULL,    name character varying,    srid integer,    ulx double precision NOT NULL,    uly double precision NOT NULL,    lrx double precision NOT NULL,    lry double precision NOT NULL,    bands integer DEFAULT 1,    cols bigint DEFAULT 0 NOT NULL,    rows bigint DEFAULT 0 NOT NULL,    resx double precision DEFAULT 1.0 NOT NULL,    resy double precision DEFAULT 1 NOT NULL,    format character varying DEFAULT 'GTIFF'::character varying NOT NULL,    vrt_path character varying NOT NULL,    compression character varying DEFAULT 'NONE'::character varying,    color_interp integer DEFAULT 1)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_rawgraph_couche_config    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.rawgraph_couche_config (    id integer DEFAULT nextval('catalogue.seq_rawgraph_couche_config'::regclass) NOT NULL,    titre character varying(255) NOT NULL,    resume text NOT NULL,    serie_donnees_uuid character varying(255) NOT NULL,    couche character varying(255) NOT NULL,    colonnes text NOT NULL,    uuid character varying(255) NOT NULL,    config json)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.rubric_param_rubric_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.rubric_param (    rubric_id integer DEFAULT nextval('catalogue.rubric_param_rubric_id_seq'::regclass) NOT NULL,    rubric_name character varying)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.scheduled_command (    id integer NOT NULL,    name text,    command text,    arguments text,    cron_expression text,    last_execution timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,    last_return_code integer DEFAULT 0,    log_file text,    priority integer DEFAULT 0,    execute_immediately boolean DEFAULT false,    disabled boolean DEFAULT false,    locked boolean DEFAULT false,    is_periodic boolean NOT NULL,    year integer,    frequency text)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.scheduled_command_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_commune    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_departement    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_dico    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_dico_majic2    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_export    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_incoherence_perso    START WITH 0    INCREMENT BY 1    MINVALUE 0    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_mapfile    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_mcd_field    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_mcd_layer    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_menu    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_profil    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_ssdom_dispose_metadata    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_statistique    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_tache_import_donnees    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_traitement    START WITH 200    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_trt_autorise_objet    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_trt_objet    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_usr_accede_perimetre    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_usr_accede_perimetre_edition    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_usr_alerte_perimetre_edition    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_utilisateur_carte    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_utilisateur_mapfile    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.seq_zonage    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE catalogue.ssdom_dispose_metadata (    pk_ssdom_dispose_metadata integer DEFAULT nextval('catalogue.seq_ssdom_dispose_metadata'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    uuid character varying(250) NOT NULL,    ssdcouch_fk_sous_domaine integer)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.standards_conformite (    metadata_id integer NOT NULL,    standard_id integer NOT NULL,    conformite_prefix character varying(100),    conformite_suffix character varying(100),    conformite_success boolean DEFAULT true,    conformite_report_url text)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.standards_fournisseur (    fournisseur_id integer NOT NULL,    fournisseur_name text NOT NULL,    fournisseur_url text NOT NULL)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.standards_fournisseur_fournisseur_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "ALTER SEQUENCE catalogue.standards_fournisseur_fournisseur_id_seq OWNED BY catalogue.standards_fournisseur.fournisseur_id"
        );
        $this->addSql(
            "CREATE TABLE catalogue.standards_standard (    standard_id integer NOT NULL,    fournisseur_id integer NOT NULL,    standard_name text NOT NULL,    standard_url text NOT NULL,    standard_database text,    standard_schema text,    fmeta_id integer)"
        );
        $this->addSql(
            "CREATE SEQUENCE catalogue.standards_standard_standard_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "ALTER SEQUENCE catalogue.standards_standard_standard_id_seq OWNED BY catalogue.standards_standard.standard_id"
        );
        $this->addSql(
            "CREATE TABLE catalogue.tache_import_donnees (    pk_tache_import_donnees integer DEFAULT nextval('catalogue.seq_tache_import_donnees'::regclass) NOT NULL,    uuid character varying(250) NOT NULL,    pk_couche_donnees integer,    user_id integer NOT NULL,    import_date_request timestamp without time zone DEFAULT now() NOT NULL,    import_date_execution timestamp without time zone,    import_type character varying(20) DEFAULT 'import_differe'::character varying NOT NULL,    import_frequency integer,    import_table character varying(1024) NOT NULL,    import_data_type character varying(250) DEFAULT 'VECTOR'::character varying NOT NULL,    import_data_source text NOT NULL,    import_extra_params text)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.test_requests (    test_id integer NOT NULL,    test_description character varying(30))"
        );
        $this->addSql(
            "CREATE TABLE catalogue.traitement (    pk_traitement integer DEFAULT nextval('catalogue.seq_traitement'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    trt_id character varying(30) NOT NULL,    trt_nom character varying(128) NOT NULL,    trt_description character varying(1024),    trt_administre boolean NOT NULL,    trt_accede boolean NOT NULL,    trt_edite boolean DEFAULT false)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.trt_autorise_objet (    pk_trt_autorise_objet integer DEFAULT nextval('catalogue.seq_trt_autorise_objet'::regclass) NOT NULL,    trtautoobjet_fk_trt_id integer,    trtautoobjet_fk_obj_type_id integer)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.trt_objet (    pk_trt_objet integer DEFAULT nextval('catalogue.trt_objet'::regclass) NOT NULL,    trt_id character varying(255) NOT NULL,    trt_nom character varying(255) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.usr_accede_perimetre (    pk_usr_accede_perimetre integer DEFAULT nextval('catalogue.seq_usr_accede_perimetre'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    usrperim_fk_utilisateur integer,    usrperim_fk_perimetre integer)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.usr_accede_perimetre_edition (    pk_usr_accede_perimetre_edition integer DEFAULT nextval('catalogue.seq_usr_accede_perimetre_edition'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    usrperim_fk_utilisateur integer,    usrperim_fk_perimetre integer)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.usr_alerte_perimetre_edition (    pk_usr_alerte_perimetre_edition integer DEFAULT nextval('catalogue.seq_usr_alerte_perimetre_edition'::regclass) NOT NULL,    ts timestamp without time zone DEFAULT now(),    usralert_fk_utilisateur integer,    usralert_fk_couchedonnees integer,    usralert_fk_perimetre integer)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.utilisateur_carte (    id bigint DEFAULT nextval('catalogue.seq_utilisateur_carte'::regclass) NOT NULL,    fk_utilisateur integer NOT NULL,    fk_stockage_carte integer NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE catalogue.zonage (    pk_zonage_id integer DEFAULT nextval('catalogue.seq_zonage'::regclass) NOT NULL,    zonage_nom character varying,    zonage_minimal integer DEFAULT 0,    zonage_field_id character varying,    zonage_field_name character varying,    zonage_fk_couche_donnees integer,    zonage_field_liaison character varying)"
        );
        $this->addSql(
            "CREATE TABLE public.address (    id integer NOT NULL,    address character varying(128),    city character varying(128),    state character varying(32),    zip character varying(16),    country character varying(128))"
        );
        $this->addSql(
            "CREATE TABLE public.categories (    id integer NOT NULL,    name character varying(255) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.categoriesdes (    iddes integer NOT NULL,    langid character varying(5) NOT NULL,    label character varying(255) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.cswservercapabilitiesinfo (    idfield integer NOT NULL,    langid character varying(5) NOT NULL,    field character varying(32) NOT NULL,    label text)"
        );
        $this->addSql(
            "CREATE TABLE public.customelementset (    xpath character varying(1000) NOT NULL,    xpathhashcode integer NOT NULL)"
        );
        $this->addSql("CREATE TABLE public.email (    user_id integer NOT NULL,    email character varying(128))");
        $this->addSql(
            "CREATE TABLE public.files (    _id integer NOT NULL,    _content text NOT NULL,    _mimetype character varying(255) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.group_category (    group_id integer NOT NULL,    category_id integer NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.groups (    id integer NOT NULL,    name character varying(32) NOT NULL,    description character varying(255),    email character varying(32),    referrer integer,    logo character varying(255),    website character varying(255),    defaultcategory_id integer,    enablecategoriesrestriction character(1) DEFAULT 'n'::bpchar)"
        );
        $this->addSql(
            "CREATE TABLE public.groupsdes (    iddes integer NOT NULL,    langid character varying(5) NOT NULL,    label character varying(96) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.harvesterdata (    harvesteruuid character varying(255) NOT NULL,    key character varying(255) NOT NULL,    value character varying(255) NOT NULL,    keyvalue character varying(255) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.harvestersettings (    id integer NOT NULL,    parentid integer,    name character varying(64) NOT NULL,    value text)"
        );
        $this->addSql(
            "CREATE TABLE public.harvesthistory (    id integer NOT NULL,    harvestdate character varying(30),    harvesteruuid character varying(250),    harvestername character varying(128),    harvestertype character varying(128),    deleted character(1) DEFAULT 'n'::bpchar NOT NULL,    info text,    params text,    elapsedtime integer)"
        );
        $this->addSql(
            "CREATE SEQUENCE public.hibernate_sequence    START WITH 40000    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE public.inspireatomfeed (    id integer NOT NULL,    atom text,    atomdatasetid character varying(255),    atomdatasetns character varying(255),    atomurl character varying(255),    authoremail character varying(255),    authorname character varying(255),    lang character varying(3),    metadataid integer NOT NULL,    rights character varying(255),    subtitle character varying(255),    title character varying(255))"
        );
        $this->addSql(
            "CREATE TABLE public.inspireatomfeed_entrylist (    inspireatomfeed_id integer NOT NULL,    crs character varying(255),    id integer NOT NULL,    lang character varying(3),    title character varying(255),    type character varying(255),    url character varying(255))"
        );
        $this->addSql(
            "CREATE TABLE public.isolanguages (    id integer NOT NULL,    code character varying(3) NOT NULL,    shortcode character varying(2))"
        );
        $this->addSql(
            "CREATE TABLE public.isolanguagesdes (    iddes integer NOT NULL,    langid character varying(5) NOT NULL,    label character varying(96) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.languages (    id character varying(5) NOT NULL,    name character varying(32) NOT NULL,    isinspire character(1) DEFAULT 'n'::bpchar,    isdefault character(1) DEFAULT 'n'::bpchar)"
        );
        $this->addSql(
            "CREATE TABLE public.mapservers (    id integer NOT NULL,    configurl character varying(255) NOT NULL,    description character varying(255),    name character varying(32) NOT NULL,    namespace character varying(255),    namespaceprefix character varying(255),    password character varying(128),    stylerurl character varying(255),    username character varying(128),    wcsurl character varying(255),    wfsurl character varying(255),    wmsurl character varying(255),    pushstyleinworkspace character(1))"
        );
        $this->addSql(
            "CREATE TABLE public.metadatacateg (    metadataid integer NOT NULL,    categoryid integer NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.metadatafiledownloads (    id integer NOT NULL,    downloaddate character varying(255) NOT NULL,    filename character varying(255) NOT NULL,    fileuploadid integer NOT NULL,    metadataid integer NOT NULL,    requestercomments character varying(255),    requestermail character varying(255),    requestername character varying(255),    requesterorg character varying(255),    username character varying(255))"
        );
        $this->addSql(
            "CREATE TABLE public.metadatafileuploads (    id integer NOT NULL,    deleteddate character varying(255),    filename character varying(255) NOT NULL,    filesize double precision NOT NULL,    metadataid integer NOT NULL,    uploaddate character varying(255) NOT NULL,    username character varying(255) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.metadataidentifiertemplate (    id integer NOT NULL,    name character varying(32) NOT NULL,    isprovided character(1) DEFAULT 'n'::bpchar NOT NULL,    template character varying(255) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.metadatanotifications (    metadataid integer NOT NULL,    notifierid integer NOT NULL,    notified character(1) DEFAULT 'n'::bpchar NOT NULL,    metadatauuid character varying(250) NOT NULL,    action integer NOT NULL,    errormsg text)"
        );
        $this->addSql(
            "CREATE TABLE public.metadatanotifiers (    id integer NOT NULL,    name character varying(32) NOT NULL,    url character varying(255) NOT NULL,    enabled character(1) DEFAULT 'n'::bpchar NOT NULL,    username character varying(32),    password character varying(32))"
        );
        $this->addSql(
            "CREATE TABLE public.metadatarating (    metadataid integer NOT NULL,    ipaddress character varying(32) NOT NULL,    rating integer NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.metadatastatus (    metadataid integer NOT NULL,    statusid integer DEFAULT 0 NOT NULL,    userid integer NOT NULL,    changedate character varying(30) NOT NULL,    changemessage character varying(2048) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.operations (    id integer NOT NULL,    name character varying(32) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.operationsdes (    iddes integer NOT NULL,    langid character varying(5) NOT NULL,    label character varying(96) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.params (    id integer NOT NULL,    requestid integer,    querytype integer,    termfield character varying(128),    termtext character varying(128),    similarity double precision,    lowertext character varying(128),    uppertext character varying(128),    inclusive character(1))"
        );
        $this->addSql(
            "CREATE TABLE public.regions (    id integer NOT NULL,    north double precision NOT NULL,    south double precision NOT NULL,    west double precision NOT NULL,    east double precision NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.regionsdes (    iddes integer NOT NULL,    langid character varying(5) NOT NULL,    label character varying(96) NOT NULL)"
        );
        $this->addSql("CREATE TABLE public.relations (    id integer NOT NULL,    relatedid integer NOT NULL)");
        $this->addSql(
            "CREATE TABLE public.requests (    id integer NOT NULL,    requestdate character varying(30),    ip character varying(128),    query text,    hits integer,    lang character varying(16),    sortby character varying(128),    spatialfilter text,    type text,    service character varying(128),    autogenerated boolean,    simple boolean)"
        );
        $this->addSql(
            "CREATE TABLE public.schematron (    id integer NOT NULL,    displaypriority integer NOT NULL,    filename character varying(255) NOT NULL,    schemaname character varying(255) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.schematroncriteria (    id integer NOT NULL,    type character varying(255) NOT NULL,    uitype character varying(255),    uivalue character varying(255),    value character varying(255) NOT NULL,    group_name character varying(255) NOT NULL,    group_schematronid integer NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.schematroncriteriagroup (    name character varying(255) NOT NULL,    schematronid integer NOT NULL,    requirement character varying(255) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.schematrondes (    iddes integer NOT NULL,    label character varying(96) NOT NULL,    langid character varying(5) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.selections (    id integer NOT NULL,    name character varying(255) NOT NULL,    iswatchable character(1) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.selectionsdes (    iddes integer NOT NULL,    label character varying(255) NOT NULL,    langid character varying(5) NOT NULL)"
        );
        $this->addSql(
            "CREATE SEQUENCE public.serviceparameter_id_seq    START WITH 1    INCREMENT BY 1    NO MINVALUE    NO MAXVALUE    CACHE 1"
        );
        $this->addSql(
            "CREATE TABLE public.serviceparameters (    service integer,    name character varying(64) NOT NULL,    value character varying(1048) NOT NULL,    occur character varying(1) DEFAULT '+'::character varying,    id integer NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.services (    id integer NOT NULL,    name character varying(64) NOT NULL,    class character varying(1048) NOT NULL,    description character varying(1048),    explicitquery character varying(255))"
        );
        $this->addSql(
            "CREATE TABLE public.settings (    name character varying(512) NOT NULL,    value text,    datatype integer,    \"position\" integer,    internal character varying(1))"
        );
        $this->addSql(
            "CREATE TABLE public.sources (    uuid character varying(250) NOT NULL,    name character varying(250),    islocal character(1) DEFAULT 'y'::bpchar)"
        );
        $this->addSql(
            "CREATE TABLE public.sourcesdes (    iddes character varying(255) NOT NULL,    label character varying(96) NOT NULL,    langid character varying(5) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.spatialindex (    fid integer NOT NULL,    id character varying(250),    the_geom public.geometry,    CONSTRAINT enforce_dims_the_geom CHECK ((public.st_ndims(the_geom) = 2)),    CONSTRAINT enforce_geotype_the_geom CHECK (((public.geometrytype(the_geom) = 'MULTIPOLYGON'::text) OR (the_geom IS NULL))),    CONSTRAINT enforce_srid_the_geom CHECK ((public.st_srid(the_geom) = 4326)))"
        );
        $this->addSql(
            "CREATE TABLE public.statusvalues (    id integer NOT NULL,    name character varying(32) NOT NULL,    reserved character(1) DEFAULT 'n'::bpchar NOT NULL,    displayorder integer)"
        );
        $this->addSql(
            "CREATE TABLE public.statusvaluesdes (    iddes integer NOT NULL,    langid character varying(5) NOT NULL,    label character varying(96) NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.thesaurus (    id character varying(250) NOT NULL,    activated character varying(1))"
        );
        $this->addSql("CREATE TABLE public.useraddress (    userid integer NOT NULL,    addressid integer NOT NULL)");
        $this->addSql(
            "CREATE TABLE public.usergroups (    userid integer NOT NULL,    groupid integer NOT NULL,    profile integer NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.users (    id integer NOT NULL,    username character varying(256) NOT NULL,    password character varying(120) NOT NULL,    surname character varying(32),    name character varying(32),    profile integer NOT NULL,    organisation character varying(128),    kind character varying(16),    security character varying(128) DEFAULT ''::character varying,    authtype character varying(32),    lastlogindate character varying(255),    nodeid character varying(255),    isenabled character(1) DEFAULT 'y'::bpchar NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.usersavedselections (    metadatauuid character varying(255) NOT NULL,    selectionid integer NOT NULL,    userid integer NOT NULL)"
        );
        $this->addSql(
            "CREATE TABLE public.validation (    metadataid integer NOT NULL,    valtype character varying(40) NOT NULL,    status integer,    tested integer,    failed integer,    valdate character varying(30),    required boolean)"
        );

        $this->addSql(
            "ALTER TABLE ONLY catalogue.harves_opendata_node ALTER COLUMN pk_node_id SET DEFAULT nextval('catalogue.harves_opendata_node_pk_node_id_seq'::regclass)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.harves_opendata_node_params ALTER COLUMN pk_params_id SET DEFAULT nextval('catalogue.harves_opendata_node_params_pk_params_id_seq'::regclass)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.harves_opendata_node_params ALTER COLUMN fk_node_id SET DEFAULT nextval('catalogue.\"harves_opendata_node_params_FK_node_id_seq\"'::regclass)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.standards_fournisseur ALTER COLUMN fournisseur_id SET DEFAULT nextval('catalogue.standards_fournisseur_fournisseur_id_seq'::regclass)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.standards_standard ALTER COLUMN standard_id SET DEFAULT nextval('catalogue.standards_standard_standard_id_seq'::regclass)"
        );
        $this->addSql(
            "ALTER TABLE ONLY bdterr.bdterr_champ_type ADD CONSTRAINT bdterr_champ_type_pkey PRIMARY KEY (champtype_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY bdterr.bdterr_contenus_tiers ADD CONSTRAINT bdterr_contenus_tiers_pkey PRIMARY KEY (contenu_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY bdterr.bdterr_couche_champ ADD CONSTRAINT bdterr_couche_champ_pkey PRIMARY KEY (champ_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY bdterr.bdterr_donnee ADD CONSTRAINT bdterr_donnee_pkey PRIMARY KEY (donnee_id)"
        );
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_lot ADD CONSTRAINT bdterr_lot_pkey PRIMARY KEY (lot_id)");
        $this->addSql(
            "ALTER TABLE ONLY bdterr.bdterr_rapports ADD CONSTRAINT bdterr_rapports_pkey PRIMARY KEY (rapport_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY bdterr.bdterr_rapports_profils ADD CONSTRAINT bdterr_rapports_profils_pkey PRIMARY KEY (id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY bdterr.bdterr_referentiel_carto ADD CONSTRAINT bdterr_referentiel_carto_pkey PRIMARY KEY (referentiel_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY bdterr.bdterr_referentiel_intersection ADD CONSTRAINT bdterr_referentiel_intersection_pkey PRIMARY KEY (referentiel_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY bdterr.bdterr_referentiel_recherche ADD CONSTRAINT bdterr_referentiel_recherche_pkey PRIMARY KEY (referentiel_id)"
        );
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_themes ADD CONSTRAINT bdterr_themes_pkey PRIMARY KEY (theme_id)");
        $this->addSql(
            "ALTER TABLE ONLY catalogue.prodige_carto_colors ADD CONSTRAINT carto_colors_pkey PRIMARY KEY (prodige_carto_color_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.prodige_colors ADD CONSTRAINT colors_pkey PRIMARY KEY (prodige_color_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.competence_accede_carte ADD CONSTRAINT competence_accede_carte_pk PRIMARY KEY (pk_competence_accede_carte)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.competence_accede_couche ADD CONSTRAINT competence_accede_couche_pk PRIMARY KEY (pk_competence_accede_couche)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.competence ADD CONSTRAINT competence_pk PRIMARY KEY (pk_competence_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.execution_report ADD CONSTRAINT execution_report_pkey PRIMARY KEY (id)"
        );
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_fonts ADD CONSTRAINT fonts_pkey PRIMARY KEY (font_id)");
        $this->addSql(
            "ALTER TABLE ONLY catalogue.prodige_geosource_colors ADD CONSTRAINT geosource_colors_pkey PRIMARY KEY (prodige_geosource_color_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.grp_accede_competence ADD CONSTRAINT grp_accede_competence_pkey PRIMARY KEY (pk_grp_accede_competence)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.grp_trt_objet ADD CONSTRAINT grp_trt_objet_pkey PRIMARY KEY (pk_grp_trt_objet)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.harves_opendata_node_params ADD CONSTRAINT harves_opendata_node_params_pkey PRIMARY KEY (pk_params_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.harves_opendata_node ADD CONSTRAINT harves_opendata_node_pkey PRIMARY KEY (pk_node_id)"
        );
        $this->addSql("ALTER TABLE ONLY catalogue.perimetre ADD CONSTRAINT perimetre_pk PRIMARY KEY (pk_perimetre_id)");
        $this->addSql(
            "ALTER TABLE ONLY catalogue.acces_serveur ADD CONSTRAINT pk_acces_serveur PRIMARY KEY (pk_acces_serveur)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.couche_donnees ADD CONSTRAINT pk_couche_donnees PRIMARY KEY (pk_couche_donnees)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.fiche_metadonnees ADD CONSTRAINT pk_fiche_metadonnees PRIMARY KEY (pk_fiche_metadonnees)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.grp_autorise_trt ADD CONSTRAINT pk_grp_autorise_trt PRIMARY KEY (pk_grp_autorise_trt)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.objet_type ADD CONSTRAINT pk_objet_type_id_pkey PRIMARY KEY (pk_objet_type_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.carte_projet ADD CONSTRAINT pk_pk_carte_projet PRIMARY KEY (pk_carte_projet)"
        );
        $this->addSql("ALTER TABLE ONLY catalogue.domaine ADD CONSTRAINT pk_pk_domaine PRIMARY KEY (pk_domaine)");
        $this->addSql(
            "ALTER TABLE ONLY catalogue.groupe_profil ADD CONSTRAINT pk_pk_groupe_profil PRIMARY KEY (pk_groupe_profil)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.grp_accede_dom ADD CONSTRAINT pk_pk_grp_accede_dom PRIMARY KEY (pk_grp_accede_dom)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.grp_accede_ssdom ADD CONSTRAINT pk_pk_grp_accede_ssdom PRIMARY KEY (pk_grp_accede_ssdom)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.grp_regroupe_usr ADD CONSTRAINT pk_pk_grp_regroupe_usr PRIMARY KEY (pk_grp_regroupe_usr)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.sous_domaine ADD CONSTRAINT pk_pk_sous_domaine PRIMARY KEY (pk_sous_domaine)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.stockage_carte ADD CONSTRAINT pk_pk_stockage_carte PRIMARY KEY (pk_stockage_carte)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.traitement ADD CONSTRAINT pk_pk_traitement PRIMARY KEY (pk_traitement)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.utilisateur ADD CONSTRAINT pk_pk_utilisateur PRIMARY KEY (pk_utilisateur)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.prodige_external_access ADD CONSTRAINT pk_prodige_external_access PRIMARY KEY (pk_prodige_external_access)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.prodige_help ADD CONSTRAINT pk_prodige_help PRIMARY KEY (pk_prodige_help_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.prodige_help_group ADD CONSTRAINT pk_prodige_help_group PRIMARY KEY (pk_prodige_help_group)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.prodige_settings ADD CONSTRAINT pk_prodige_settings PRIMARY KEY (pk_prodige_settings_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.rawgraph_couche_config ADD CONSTRAINT pk_rawgraph_couche_config PRIMARY KEY (id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.ssdom_dispose_couche ADD CONSTRAINT pk_ssdom_dispose_couche PRIMARY KEY (pk_ssdom_dispose_couche)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.ssdom_dispose_metadata ADD CONSTRAINT pk_ssdom_dispose_metadata PRIMARY KEY (pk_ssdom_dispose_metadata)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.ssdom_visualise_carte ADD CONSTRAINT pk_ssdom_visualise_carte PRIMARY KEY (pk_ssdom_visualise_carte)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.standards_conformite ADD CONSTRAINT pk_standards_conformite PRIMARY KEY (metadata_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.standards_fournisseur ADD CONSTRAINT pk_standards_fournisseur PRIMARY KEY (fournisseur_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.standards_standard ADD CONSTRAINT pk_standards_standard PRIMARY KEY (standard_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.tache_import_donnees ADD CONSTRAINT pk_tache_import_donnees PRIMARY KEY (pk_tache_import_donnees)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.trt_objet ADD CONSTRAINT pk_trt_objet_pkey PRIMARY KEY (pk_trt_objet)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.prodige_param ADD CONSTRAINT pkey_prodige_param PRIMARY KEY (pk_prodige_param)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.prodige_session_user ADD CONSTRAINT prodige_session_user_pkey PRIMARY KEY (pk_session_user)"
        );
        $this->addSql("ALTER TABLE ONLY catalogue.raster_info ADD CONSTRAINT raster_info_pkey PRIMARY KEY (id)");
        $this->addSql(
            "ALTER TABLE ONLY catalogue.prodige_database_requests ADD CONSTRAINT request_pk PRIMARY KEY (prodige_database_request_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.rubric_param ADD CONSTRAINT rubric_param_pkey PRIMARY KEY (rubric_id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.scheduled_command ADD CONSTRAINT scheduled_command_pkey PRIMARY KEY (id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.ssdom_visualise_carte ADD CONSTRAINT ssdom_visualise_carte_ssdcart_fk_carte_projet_key UNIQUE (ssdcart_fk_carte_projet, ssdcart_fk_sous_domaine)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.trt_autorise_objet ADD CONSTRAINT trt_autorise_objet_pkey PRIMARY KEY (pk_trt_autorise_objet)"
        );
        $this->addSql("ALTER TABLE ONLY catalogue.acces_serveur ADD CONSTRAINT un_accs_id UNIQUE (accs_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.carte_projet ADD CONSTRAINT un_cartp_id UNIQUE (cartp_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.couche_donnees ADD CONSTRAINT un_couchd_id UNIQUE (couchd_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.domaine ADD CONSTRAINT un_dom_id UNIQUE (dom_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.domaine ADD CONSTRAINT un_dom_nom UNIQUE (dom_nom)");
        $this->addSql(
            "ALTER TABLE ONLY catalogue.grp_accede_dom ADD CONSTRAINT un_grp_accede_dom_fks UNIQUE (grpdom_fk_domaine, grpdom_fk_groupe_profil)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.grp_accede_ssdom ADD CONSTRAINT un_grp_accede_ssdom_fks UNIQUE (grpss_fk_sous_domaine, grpss_fk_groupe_profil)"
        );
        $this->addSql("ALTER TABLE ONLY catalogue.groupe_profil ADD CONSTRAINT un_grp_id UNIQUE (grp_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.groupe_profil ADD CONSTRAINT un_grp_nom UNIQUE (grp_nom)");
        $this->addSql(
            "ALTER TABLE ONLY catalogue.grp_regroupe_usr ADD CONSTRAINT un_grp_regroupe_usr_fks UNIQUE (grpusr_fk_groupe_profil, grpusr_fk_utilisateur)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.ssdom_dispose_couche ADD CONSTRAINT un_ssdom_dispose_couche_fks UNIQUE (ssdcouch_fk_couche_donnees, ssdcouch_fk_sous_domaine)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.ssdom_dispose_metadata ADD CONSTRAINT un_ssdom_dispose_metadata_fks UNIQUE (uuid, ssdcouch_fk_sous_domaine)"
        );
        $this->addSql("ALTER TABLE ONLY catalogue.sous_domaine ADD CONSTRAINT un_ssdom_id UNIQUE (ssdom_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.sous_domaine ADD CONSTRAINT un_ssdom_nom UNIQUE (ssdom_nom)");
        $this->addSql("ALTER TABLE ONLY catalogue.stockage_carte ADD CONSTRAINT un_stkcard_id UNIQUE (stkcard_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.traitement ADD CONSTRAINT un_trt_id UNIQUE (trt_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.traitement ADD CONSTRAINT un_trt_nom UNIQUE (trt_nom)");
        $this->addSql("ALTER TABLE ONLY catalogue.utilisateur ADD CONSTRAINT un_usr_id UNIQUE (usr_id)");
        $this->addSql(
            "ALTER TABLE ONLY catalogue.usr_accede_perimetre_edition ADD CONSTRAINT usr_accede_perimetre_edit_pkey PRIMARY KEY (pk_usr_accede_perimetre_edition)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.usr_alerte_perimetre_edition ADD CONSTRAINT usr_alerte_perim_edit_pkey PRIMARY KEY (pk_usr_alerte_perimetre_edition)"
        );
        $this->addSql(
            "ALTER TABLE ONLY catalogue.utilisateur_carte ADD CONSTRAINT utilisateur_carte_pkey PRIMARY KEY (id)"
        );
        $this->addSql("ALTER TABLE ONLY catalogue.zonage ADD CONSTRAINT zonage_pk PRIMARY KEY (pk_zonage_id)");
        $this->addSql("ALTER TABLE ONLY public.address ADD CONSTRAINT address_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY public.categories ADD CONSTRAINT categories_name_key UNIQUE (name)");
        $this->addSql("ALTER TABLE ONLY public.categories ADD CONSTRAINT categories_pkey PRIMARY KEY (id)");
        $this->addSql(
            "ALTER TABLE ONLY public.categoriesdes ADD CONSTRAINT categoriesdes_pkey PRIMARY KEY (iddes, langid)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.cswservercapabilitiesinfo ADD CONSTRAINT cswservercapabilitiesinfo_pkey PRIMARY KEY (idfield)"
        );
        $this->addSql("ALTER TABLE ONLY public.email ADD CONSTRAINT email_pkey PRIMARY KEY (user_id)");
        $this->addSql("ALTER TABLE ONLY public.files ADD CONSTRAINT files_pkey PRIMARY KEY (_id)");
        $this->addSql("ALTER TABLE ONLY public.groups ADD CONSTRAINT groups_name_key UNIQUE (name)");
        $this->addSql("ALTER TABLE ONLY public.groups ADD CONSTRAINT groups_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY public.groupsdes ADD CONSTRAINT groupsdes_pkey PRIMARY KEY (iddes, langid)");
        $this->addSql(
            "ALTER TABLE ONLY public.harvesterdata ADD CONSTRAINT harvesterdata_pkey PRIMARY KEY (harvesteruuid, key)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.harvestersettings ADD CONSTRAINT harvestersettings_pkey PRIMARY KEY (id)"
        );
        $this->addSql("ALTER TABLE ONLY public.harvesthistory ADD CONSTRAINT harvesthistory_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY public.inspireatomfeed ADD CONSTRAINT inspireatomfeed_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY public.isolanguages ADD CONSTRAINT isolanguages_code_key UNIQUE (code)");
        $this->addSql("ALTER TABLE ONLY public.isolanguages ADD CONSTRAINT isolanguages_pkey PRIMARY KEY (id)");
        $this->addSql(
            "ALTER TABLE ONLY public.isolanguagesdes ADD CONSTRAINT isolanguagesdes_pkey PRIMARY KEY (iddes, langid)"
        );
        $this->addSql("ALTER TABLE ONLY public.languages ADD CONSTRAINT languages_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY public.mapservers ADD CONSTRAINT mapservers_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY public.metadata ADD CONSTRAINT metadata_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY public.metadata ADD CONSTRAINT metadata_uuid_key UNIQUE (uuid)");
        $this->addSql(
            "ALTER TABLE ONLY public.metadatacateg ADD CONSTRAINT metadatacateg_pkey PRIMARY KEY (metadataid, categoryid)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.metadatafiledownloads ADD CONSTRAINT metadatafiledownloads_pkey PRIMARY KEY (id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.metadatafileuploads ADD CONSTRAINT metadatafileuploads_pkey PRIMARY KEY (id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.metadataidentifiertemplate ADD CONSTRAINT metadataidentifiertemplate_pkey PRIMARY KEY (id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.metadatanotifications ADD CONSTRAINT metadatanotifications_pkey PRIMARY KEY (metadataid, notifierid)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.metadatanotifiers ADD CONSTRAINT metadatanotifiers_pkey PRIMARY KEY (id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.metadatarating ADD CONSTRAINT metadatarating_pkey PRIMARY KEY (metadataid, ipaddress)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.metadatastatus ADD CONSTRAINT metadatastatus_pkey PRIMARY KEY (metadataid, statusid, userid, changedate)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.operationallowed ADD CONSTRAINT operationallowed_pkey PRIMARY KEY (groupid, metadataid, operationid)"
        );
        $this->addSql("ALTER TABLE ONLY public.operations ADD CONSTRAINT operations_pkey PRIMARY KEY (id)");
        $this->addSql(
            "ALTER TABLE ONLY public.operationsdes ADD CONSTRAINT operationsdes_pkey PRIMARY KEY (iddes, langid)"
        );
        $this->addSql("ALTER TABLE ONLY public.params ADD CONSTRAINT params_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY public.regions ADD CONSTRAINT regions_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY public.regionsdes ADD CONSTRAINT regionsdes_pkey PRIMARY KEY (iddes, langid)");
        $this->addSql("ALTER TABLE ONLY public.relations ADD CONSTRAINT relations_pkey PRIMARY KEY (id, relatedid)");
        $this->addSql("ALTER TABLE ONLY public.requests ADD CONSTRAINT requests_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY public.schematron ADD CONSTRAINT schematron_pkey PRIMARY KEY (id)");
        $this->addSql(
            "ALTER TABLE ONLY public.schematroncriteria ADD CONSTRAINT schematroncriteria_pkey PRIMARY KEY (id)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.schematroncriteriagroup ADD CONSTRAINT schematroncriteriagroup_pkey PRIMARY KEY (name, schematronid)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.schematrondes ADD CONSTRAINT schematrondes_pkey PRIMARY KEY (iddes, langid)"
        );
        $this->addSql("ALTER TABLE ONLY public.selections ADD CONSTRAINT selections_pkey PRIMARY KEY (id)");
        $this->addSql(
            "ALTER TABLE ONLY public.selectionsdes ADD CONSTRAINT selectionsdes_pkey PRIMARY KEY (iddes, langid)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.serviceparameters ADD CONSTRAINT serviceparameters_pkey PRIMARY KEY (id)"
        );
        $this->addSql("ALTER TABLE ONLY public.services ADD CONSTRAINT services_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY public.settings ADD CONSTRAINT settings_pkey PRIMARY KEY (name)");
        $this->addSql("ALTER TABLE ONLY public.sources ADD CONSTRAINT sources_pkey PRIMARY KEY (uuid)");
        $this->addSql("ALTER TABLE ONLY public.sourcesdes ADD CONSTRAINT sourcesdes_pkey PRIMARY KEY (iddes, langid)");
        $this->addSql("ALTER TABLE ONLY public.spatialindex ADD CONSTRAINT spatialindex_pkey PRIMARY KEY (fid)");
        $this->addSql("ALTER TABLE ONLY public.statusvalues ADD CONSTRAINT statusvalues_pkey PRIMARY KEY (id)");
        $this->addSql(
            "ALTER TABLE ONLY public.statusvaluesdes ADD CONSTRAINT statusvaluesdes_pkey PRIMARY KEY (iddes, langid)"
        );
        $this->addSql("ALTER TABLE ONLY public.thesaurus ADD CONSTRAINT thesaurus_pkey PRIMARY KEY (id)");
        $this->addSql(
            "ALTER TABLE ONLY public.useraddress ADD CONSTRAINT uk_8te6nqcuovmv45ej1oej73hg3 UNIQUE (addressid)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.schematron ADD CONSTRAINT uk_k7c29i3x0x6p5hbvb0qsdmuek UNIQUE (schemaname, filename)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.schematron ADD CONSTRAINT uk_mlb74anygnag9es7yexj3qj0d UNIQUE (schemaname, filename)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.useraddress ADD CONSTRAINT useraddress_pkey PRIMARY KEY (userid, addressid)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.usergroups ADD CONSTRAINT usergroups_pkey PRIMARY KEY (userid, groupid, profile)"
        );
        $this->addSql("ALTER TABLE ONLY public.users ADD CONSTRAINT users_pkey PRIMARY KEY (id)");
        $this->addSql("ALTER TABLE ONLY public.users ADD CONSTRAINT users_username_key UNIQUE (username)");
        $this->addSql(
            "ALTER TABLE ONLY public.usersavedselections ADD CONSTRAINT usersavedselections_pkey PRIMARY KEY (metadatauuid, selectionid, userid)"
        );
        $this->addSql(
            "ALTER TABLE ONLY public.validation ADD CONSTRAINT validation_pkey PRIMARY KEY (metadataid, valtype)"
        );

        $this->addSql("CREATE INDEX idx_30055129141f7bf0 ON bdterr.bdterr_donnee USING btree (donnee_lot_id)");
        $this->addSql("CREATE INDEX idx_4674d3dfa9ee0a03 ON bdterr.bdterr_lot USING btree (lot_referentiel_id)");
        $this->addSql("CREATE INDEX idx_4674d3dffbc885b3 ON bdterr.bdterr_lot USING btree (lot_theme_id)");
        $this->addSql("CREATE INDEX idx_4819e6af8ae06949 ON bdterr.bdterr_contenus_tiers USING btree (contenu_type)");
        $this->addSql("CREATE INDEX idx_4819e6afa8cba5f7 ON bdterr.bdterr_contenus_tiers USING btree (lot_id)");
        $this->addSql(
            "CREATE INDEX idx_51faf077d1eae64c ON bdterr.bdterr_rapports_profils USING btree (bterr_rapport_id)"
        );
        $this->addSql("CREATE INDEX idx_52a0ca003dc30acd ON bdterr.bdterr_couche_champ USING btree (champ_lot_id)");
        $this->addSql("CREATE INDEX idx_52a0ca00d43aa457 ON bdterr.bdterr_couche_champ USING btree (champ_type)");
        $this->addSql("CREATE INDEX idx_a06003abedb65433 ON bdterr.bdterr_themes USING btree (theme_parent)");
        $this->addSql(
            "CREATE UNIQUE INDEX competence_accede_carte_pkey ON catalogue.competence_accede_carte USING btree (pk_competence_accede_carte)"
        );
        $this->addSql(
            "CREATE UNIQUE INDEX competence_accede_couche_pkey ON catalogue.competence_accede_couche USING btree (pk_competence_accede_couche)"
        );
        $this->addSql("CREATE UNIQUE INDEX competence_pkey ON catalogue.competence USING btree (pk_competence_id)");
        $this->addSql(
            "CREATE INDEX competencecarte_fk_carte_projet_fkey ON catalogue.competence_accede_carte USING btree (competencecarte_fk_carte_projet)"
        );
        $this->addSql(
            "CREATE INDEX competencecarte_fk_competence_id_fkey ON catalogue.competence_accede_carte USING btree (competencecarte_fk_competence_id)"
        );
        $this->addSql(
            "CREATE INDEX competencecouche_fk_competence_id_fkey ON catalogue.competence_accede_couche USING btree (competencecouche_fk_competence_id)"
        );
        $this->addSql(
            "CREATE INDEX competencecouche_fk_couche_donnees_fkey ON catalogue.competence_accede_couche USING btree (competencecouche_fk_couche_donnees)"
        );
        $this->addSql(
            "CREATE INDEX couche_donnees_zonage_fkey ON catalogue.couche_donnees USING btree (couchd_zonageid_fk)"
        );
        $this->addSql(
            "CREATE INDEX fk_competence_id_fkey_fkey ON catalogue.grp_accede_competence USING btree (fk_competence_id)"
        );
        $this->addSql("CREATE INDEX fk_grp_id_fkey ON catalogue.grp_accede_competence USING btree (fk_grp_id)");
        $this->addSql(
            "CREATE INDEX fk_hlpgrp_fk_groupe_profil_fkey ON catalogue.prodige_help_group USING btree (hlpgrp_fk_groupe_profil)"
        );
        $this->addSql(
            "CREATE INDEX fk_hlpgrp_fk_help_id_fkey ON catalogue.prodige_help_group USING btree (hlpgrp_fk_help_id)"
        );
        $this->addSql(
            "CREATE INDEX fkey_usr_accede_perim_edit_usrperim_fk_perimetre_fkey ON catalogue.usr_accede_perimetre_edition USING btree (usrperim_fk_perimetre)"
        );
        $this->addSql(
            "CREATE INDEX fkey_usr_accede_perim_edit_usrperim_fk_utilisateur_fkey ON catalogue.usr_accede_perimetre_edition USING btree (usrperim_fk_utilisateur)"
        );
        $this->addSql(
            "CREATE INDEX fkey_usr_accede_perim_usrperim_fk_perimetre_fkey ON catalogue.usr_accede_perimetre USING btree (usrperim_fk_perimetre)"
        );
        $this->addSql(
            "CREATE INDEX fkey_usr_accede_perim_usrperim_fk_utilisateur_fkey ON catalogue.usr_accede_perimetre USING btree (usrperim_fk_utilisateur)"
        );
        $this->addSql(
            "CREATE INDEX fkey_usr_alerte_perim_edit_fk_perimetre_fkey ON catalogue.usr_alerte_perimetre_edition USING btree (usralert_fk_perimetre)"
        );
        $this->addSql(
            "CREATE INDEX fkey_usr_alerte_perim_edit_usrperim_fk_couchedonnees_fkey ON catalogue.usr_alerte_perimetre_edition USING btree (usralert_fk_couchedonnees)"
        );
        $this->addSql(
            "CREATE INDEX fkey_usr_alerte_perim_edit_usrperim_fk_utilisateur_fkey ON catalogue.usr_alerte_perimetre_edition USING btree (usralert_fk_utilisateur)"
        );
        $this->addSql(
            "CREATE INDEX grptrtobj_fk_grp_id_fkey ON catalogue.grp_trt_objet USING btree (grptrtobj_fk_grp_id)"
        );
        $this->addSql(
            "CREATE INDEX grptrtobj_fk_obj_type_id_fkey ON catalogue.grp_trt_objet USING btree (grptrtobj_fk_obj_type_id)"
        );
        $this->addSql(
            "CREATE INDEX grptrtobj_fk_objet_id_fkey ON catalogue.grp_trt_objet USING btree (grptrtobj_fk_objet_id)"
        );
        $this->addSql(
            "CREATE INDEX grptrtobj_fk_trt_id_fkey ON catalogue.grp_trt_objet USING btree (grptrtobj_fk_trt_id)"
        );
        $this->addSql(
            "CREATE INDEX idx_8e1b7bbcad93e023 ON catalogue.execution_report USING btree (scheduled_command_id)"
        );
        $this->addSql(
            "CREATE INDEX idx_utilisateur_carte_fk_stockage_carte ON catalogue.utilisateur_carte USING btree (fk_stockage_carte)"
        );
        $this->addSql(
            "CREATE INDEX idx_utilisateur_carte_fk_utilisateur ON catalogue.utilisateur_carte USING btree (fk_utilisateur)"
        );
        $this->addSql("CREATE INDEX perimetre_fk_zonage_fkey ON catalogue.perimetre USING btree (perimetre_zonage_id)");
        $this->addSql("CREATE UNIQUE INDEX perimetre_pkey ON catalogue.perimetre USING btree (pk_perimetre_id)");
        $this->addSql(
            "CREATE UNIQUE INDEX pk_objet_type_id_fkey ON catalogue.objet_type USING btree (pk_objet_type_id)"
        );
        $this->addSql(
            "CREATE INDEX pk_usr_accede_perimetre_edit_pkey ON catalogue.usr_accede_perimetre_edition USING btree (pk_usr_accede_perimetre_edition)"
        );
        $this->addSql(
            "CREATE INDEX pk_usr_accede_perimetre_pkey ON catalogue.usr_accede_perimetre USING btree (pk_usr_accede_perimetre)"
        );
        $this->addSql(
            "CREATE INDEX pk_usr_alerte_perim_edit_pkey ON catalogue.usr_alerte_perimetre_edition USING btree (pk_usr_alerte_perimetre_edition)"
        );
        $this->addSql(
            "CREATE INDEX prodige_extaccess_fk_utilisateur_fkey ON catalogue.prodige_external_access USING btree (prodige_extaccess_fk_utilisateur)"
        );
        $this->addSql(
            "CREATE INDEX trtautoobjet_fk_obj_type_id_fkey ON catalogue.trt_autorise_objet USING btree (trtautoobjet_fk_obj_type_id)"
        );
        $this->addSql(
            "CREATE INDEX trtautoobjet_fk_trt_id_fkey ON catalogue.trt_autorise_objet USING btree (trtautoobjet_fk_trt_id)"
        );
        $this->addSql("CREATE UNIQUE INDEX zonage_pkey ON catalogue.zonage USING btree (pk_zonage_id)");
        $this->addSql(
            "CREATE INDEX zonaget_fk_couche_donnees_fkey ON catalogue.zonage USING btree (zonage_fk_couche_donnees)"
        );
        $this->addSql("CREATE INDEX harvesthistoryndx1 ON public.harvesthistory USING btree (harvestdate)");
        $this->addSql("CREATE INDEX metadatandx1 ON public.metadata USING btree (source)");
        $this->addSql("CREATE INDEX paramsndx1 ON public.params USING btree (requestid)");
        $this->addSql("CREATE INDEX paramsndx2 ON public.params USING btree (querytype)");
        $this->addSql("CREATE INDEX paramsndx3 ON public.params USING btree (termfield)");
        $this->addSql("CREATE INDEX paramsndx4 ON public.params USING btree (termtext)");
        $this->addSql("CREATE INDEX requestsndx1 ON public.requests USING btree (requestdate)");
        $this->addSql("CREATE INDEX requestsndx2 ON public.requests USING btree (ip)");
        $this->addSql("CREATE INDEX requestsndx3 ON public.requests USING btree (hits)");
        $this->addSql("CREATE INDEX requestsndx4 ON public.requests USING btree (lang)");
        $this->addSql("CREATE INDEX spatialindexndx1 ON public.spatialindex USING btree (id)");
        $this->addSql("CREATE INDEX spatialindexndx2 ON public.spatialindex USING gist (the_geom)");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            'postgresql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'postgresql\'.'
        );

        $this->addSql('set search_path to public');
        $this->addSql('DROP SCHEMA bdterr cascade');
        $this->addSql('DROP SCHEMA catalogue cascade');

        $this->addSql("drop table public.customelementset cascade");
        $this->addSql("drop table public.files cascade");
        $this->addSql("drop table public.harvesterdata cascade");
        $this->addSql("drop table public.harvesthistory cascade");
        $this->addSql("drop table public.categoriesdes cascade");
        $this->addSql("drop table public.cswservercapabilitiesinfo cascade");
        $this->addSql("drop table public.mapservers cascade");
        $this->addSql("drop table public.metadatafiledownloads cascade");
        $this->addSql("drop table public.metadatafileuploads cascade");
        $this->addSql("drop table public.metadataidentifiertemplate cascade");
        $this->addSql("drop table public.relations cascade");
        $this->addSql("drop table public.requests cascade");
        $this->addSql("drop table public.params cascade");
        $this->addSql("drop table public.settings cascade");
        $this->addSql("drop table public.spatialindex cascade");
        $this->addSql("drop table public.thesaurus cascade");
        $this->addSql("drop table public.statusvaluesdes cascade");
        $this->addSql("drop table public.email cascade");
        $this->addSql("drop table public.sources cascade");
        $this->addSql("drop table public.sourcesdes cascade");
        $this->addSql("drop table public.schematroncriteriagroup cascade");
        $this->addSql("drop table public.schematroncriteria cascade");
        $this->addSql("drop table public.inspireatomfeed cascade");
        $this->addSql("drop table public.inspireatomfeed_entrylist cascade");
        $this->addSql("drop table public.metadatanotifiers cascade");
        $this->addSql("drop table public.metadatanotifications cascade");
        $this->addSql("drop table public.usersavedselections cascade");
        $this->addSql("drop table public.selections cascade");
        $this->addSql("drop table public.selectionsdes cascade");
        $this->addSql("drop table public.group_category cascade");
        $this->addSql("drop table public.schematron cascade");
        $this->addSql("drop table public.schematrondes cascade");
        $this->addSql("drop table public.groupsdes cascade");
        $this->addSql("drop table public.harvestersettings cascade");
        $this->addSql("drop table public.isolanguages cascade");
        $this->addSql("drop table public.isolanguagesdes cascade");
        $this->addSql("drop table public.categories cascade");
        $this->addSql("drop table public.metadatacateg cascade");
        $this->addSql("drop table public.metadatarating cascade");
        $this->addSql("drop table public.statusvalues cascade");
        $this->addSql("drop table public.metadatastatus cascade");
        $this->addSql("drop table public.operationallowed cascade");
        $this->addSql("drop table public.operations cascade");
        $this->addSql("drop table public.operationsdes cascade");
        $this->addSql("drop table public.regions cascade");
        $this->addSql("drop table public.languages cascade");
        $this->addSql("drop table public.regionsdes cascade");
        $this->addSql("drop table public.services cascade");
        $this->addSql("drop table public.serviceparameters cascade");
        $this->addSql("drop table public.address cascade");
        $this->addSql("drop table public.useraddress cascade");
        $this->addSql("drop table public.groups cascade");
        $this->addSql("drop table public.users cascade");
        $this->addSql("drop table public.usergroups cascade");
        $this->addSql("drop table public.metadata cascade");
        $this->addSql("drop table public.validation cascade");
        $this->addSql("drop SEQUENCE if exists public.hibernate_sequence");
        $this->addSql("drop SEQUENCE if exists public.serviceparameter_id_seq");
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211109103752 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migrate metadata in 4.4 PRODIGE version';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        //get URL
        $tabInfo = $this->connection->fetchAllAssociative("select accs_adresse from catalogue.acces_serveur");
        $catalogue_url = "https://".str_replace("carto", "catalogue", $tabInfo[0]["accs_adresse"]);
        
        $tabMetadata = $this->connection->fetchAllAssociative("select metadata.uuid, id, data from catalogue.cartes_sdom, metadata where metadata.id = cartes_sdom.fmeta_id::bigint ");
        
        foreach($tabMetadata as $ind => $tabInfo){
            
            $id = $tabInfo["id"];
            $data = $tabInfo["data"];
            $version = "1.0";
            $encoding = "UTF-8";
            $metadata_doc = new \DOMDocument($version, $encoding);
            $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
            $metadata_data = $entete . $data;

            // $metadata_data = str_replace("&", "&amp;", $metadata_data);
            if (@$metadata_doc->loadXML($metadata_data)) {
                $this->migrateMaps($metadata_doc);
                if (isset($metadata_doc)) {
                    $new_metadata_data = $metadata_doc->saveXML();
                  //  echo $new_metadata_data;
                    $new_metadata_data = str_replace($entete, "", $new_metadata_data);
                    $info = array("id" => $id, "data" =>$new_metadata_data);
                    $this->addSql("update metadata set data=:data where id=:id", $info );
            
                }
            }
        }

        $tabMetadata = $this->connection->fetchAllAssociative("select metadata.uuid, id, data, dom_nom, pk_sous_domaine from catalogue.couche_sdom, metadata where metadata.id = couche_sdom.fmeta_id::bigint ");
        
        foreach($tabMetadata as $ind => $tabInfo){
            
            $id = $tabInfo["id"];
            $data = $tabInfo["data"];
            $version = "1.0";
            $encoding = "UTF-8";
            $metadata_doc = new \DOMDocument($version, $encoding);
            $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
            $metadata_data = $entete . $data;

            // $metadata_data = str_replace("&", "&amp;", $metadata_data);
            if (@$metadata_doc->loadXML($metadata_data)) {
                $this->migrateDatas($metadata_doc, $id, $catalogue_url);
                if (isset($metadata_doc)) {
                    $new_metadata_data = $metadata_doc->saveXML();
                  //  echo $new_metadata_data;
                    $new_metadata_data = str_replace($entete, "", $new_metadata_data);
                    $info = array("id" => $id, "data" =>$new_metadata_data);
                    $this->addSql("update metadata set data=:data where id=:id", $info );
            
                }
            }
        }

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    



    protected function migrateDatas(&$metadata_doc,$id, $catalogue_url){
        
        $xpath = new \DOMXpath($metadata_doc);
        $xmlFragment = "<gmd:Link 
        xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" >
        <gmd:onLine>
            <gmd:CI_OnlineResource>
                <gmd:linkage>
                    <gmd:URL>".$catalogue_url."/geosource/panierDownloadFrontalParametrage?LAYERIDTS=".$id."</gmd:URL>
                </gmd:linkage>
                <gmd:protocol>
                    <gco:CharacterString>WWW:DOWNLOAD-1.0-http--download</gco:CharacterString>
                </gmd:protocol>
                <gmd:name>
                    <gco:CharacterString>Accès au téléchargement des données</gco:CharacterString>
                </gmd:name>
            </gmd:CI_OnlineResource>
        </gmd:onLine>
        <gmd:onLine>
            <gmd:CI_OnlineResource>
                <gmd:linkage>
                    <gmd:URL>".$catalogue_url."/geosource/consultationWMS?IDT=".$id."</gmd:URL>
                </gmd:linkage>
                <gmd:protocol>
                    <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                </gmd:protocol>
                <gmd:name>
                    <gco:CharacterString>Accès à la visualisation des données</gco:CharacterString>
                </gmd:name>
                <gmd:applicationProfile>
                    <gco:CharacterString>MAP</gco:CharacterString>
                </gmd:applicationProfile>
            </gmd:CI_OnlineResource>
        </gmd:onLine>
        </gmd:Link>";
        
        $appProfile = new \DOMDocument();
        $appProfile->loadXML($xmlFragment);
        $nodeToImport = $appProfile->getElementsByTagName("onLine")->item(0);
        $nodeToImport2 = $appProfile->getElementsByTagName("onLine")->item(1);

        //remove existant node
        $elements = @$xpath->query("//gmd:onLine/gmd:CI_OnlineResource/gmd:linkage/gmd:URL");
        if ($elements && $elements->length > 0) {
            foreach($elements as $element){
                if(strpos($element->nodeValue, "panierDownloadFrontalParametrage")!==false){
                    $element->parentNode->parentNode->parentNode->parentNode->removeChild($element->parentNode->parentNode->parentNode);
                }   
            }
        }
        //add view link
        $elements = @$xpath->query("//gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine");
        if ($elements && $elements->length > 0) {
            foreach($elements as $element){
                $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                $element->parentNode->appendChild($newNodeDesc);
                $newNodeDesc2 = $metadata_doc->importNode($nodeToImport2, true);
                $element->parentNode->appendChild($newNodeDesc2);
                break;
            }
        }
        //change name
        $elements = @$xpath->query("//gmd:onLine/gmd:CI_OnlineResource/gmd:name/gco:CharacterString");
        if ($elements && $elements->length > 0) {
            foreach($elements as $element){
                if( $element->nodeValue==="Téléchargement direct des données"){
                    $element->nodeValue = "Accès au lien ATOM de téléchargement";
                }       
            }
        }
        //remove description
        $elements = @$xpath->query("//gmd:onLine/gmd:CI_OnlineResource/gmd:description/gco:CharacterString");
        if ($elements && $elements->length > 0) {
            foreach($elements as $element){
                if( $element->nodeValue==="Téléchargement direct des données"){
                    $element->parentNode->parentNode->removeChild($element->parentNode);
                }       
            }
        }

        
    }

    protected function migrateMaps(&$metadata_doc){

        $xpath = new \DOMXpath($metadata_doc);
        $xmlFragment = "<gmd:CI_OnlineResource 
        xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" >
        <gmd:applicationProfile><gco:CharacterString>MAP</gco:CharacterString></gmd:applicationProfile>
        </gmd:CI_OnlineResource>";
      
        $appProfile = new \DOMDocument();
        $appProfile->loadXML($xmlFragment);
        $nodeToImport = $appProfile->getElementsByTagName("applicationProfile")->item(0);

        $elements = @$xpath->query("//srv:containsOperations/srv:SV_OperationMetadata/srv:operationName/gco:CharacterString");
        if ($elements && $elements->length > 0) {
            foreach($elements as $element){
                if($element->nodeValue==="Accès au fichier contexte OWS de la carte"){
                    $element->parentNode->parentNode->parentNode->parentNode->removeChild($element->parentNode->parentNode->parentNode);
                }   
            }
        }

        $elements = @$xpath->query("//gmd:onLine/gmd:CI_OnlineResource/gmd:name/gco:CharacterString");
        if ($elements && $elements->length > 0) {
            foreach($elements as $element){
                if($element->nodeValue==="Accès au fichier contexte OWS de la carte"){
                    $element->parentNode->parentNode->parentNode->parentNode->removeChild($element->parentNode->parentNode->parentNode);
                }
            }
        }

        $elements = @$xpath->query("//gmd:onLine/gmd:CI_OnlineResource/gmd:name/gco:CharacterString");
        if ($elements && $elements->length > 0) {
            foreach($elements as $element){
                if($element->nodeValue==="Accès à la carte"){
                    $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                    $element->parentNode->parentNode->appendChild($newNodeDesc);
                }
            }
        }

        $elements = @$xpath->query("//gmd:onLine/gmd:CI_OnlineResource/gmd:name/gco:CharacterString");
        if ($elements && $elements->length > 0) {
            foreach($elements as $element){
                if( $element->nodeValue==="Accès au fichier PDF de la carte"){
                    $element->parentNode->parentNode->parentNode->parentNode->removeChild($element->parentNode->parentNode->parentNode);
                }       
            }
        }

        
    }
}

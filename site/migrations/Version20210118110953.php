<?php

namespace DoctrineMigrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210118110953 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE catalogue.couche_donnees add column couchd_cgu_display integer default 0");
        $this->addSql("ALTER TABLE catalogue.couche_donnees add column couchd_cgu_message text");

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE catalogue.couche_donnees drop column couchd_cgu_display");
        $this->addSql("ALTER TABLE catalogue.couche_donnees drop column couchd_cgu_message");

    }
}

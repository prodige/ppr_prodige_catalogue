<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220414155300 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migrate metadata keywords';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("delete from catalogue.prodige_settings where prodige_settings_constant='PRO_ACTIVE_CARTE_PERSO'");
          
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }


}

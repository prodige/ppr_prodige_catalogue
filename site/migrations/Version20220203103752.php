<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220203103752 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migrate metadata keywords';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("update public.metadata set data=replace(data, '/srv/fre/xml.keyword.get?thesaurus=external.theme', '/srv/api/registries/vocabularies/keyword?skipdescriptivekeywords=true&amp;thesaurus=external.theme') where isharvested='n'");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }


}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200325113000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version tampon : Patch 4.2.7';
    }

    public function up(Schema $schema): void
    {
        #  Name: utilisateur_contexte; Type: TABLE; Schema: catalogue;
        $this->addSql("ALTER TABLE catalogue.groupe_profil ADD grp_template_mail TEXT;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE catalogue.groupe_profil DROP column grp_template_mail;");
    }
}

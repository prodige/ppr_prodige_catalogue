<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200529080000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version tampon : Patch 4.2.6';
    }

    public function up(Schema $schema): void
    {
        //Ajout champs gestion type affichage (valuues : bt / pacoc / both)
        $this->addSql("ALTER TABLE bdterr.bdterr_lot ADD COLUMN lot_display_type text DEFAULT 'bt';");
         
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE bdterr.bdterr_lot drop COLUMN lot_display_type;");
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191129040102 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout des données initiales de la base CATALOGUE';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('set search_path to public');

        $this->addSql("INSERT INTO bdterr.bdterr_themes (theme_id, theme_parent, theme_nom, theme_alias, theme_ordre) VALUES (0, NULL, 'root', NULL, 0)");

        $this->addSql("SELECT pg_catalog.setval('bdterr.bdterr_referentiel_carto_referentiel_id_seq', 1, false)");

        $this->addSql("INSERT INTO bdterr.bdterr_referentiel_recherche (referentiel_id, referentiel_table, referentiel_insee, referentiel_insee_delegue, referentiel_commune, referentiel_nom, referentiel_api_champs) VALUES (1, '', '', '', '', '', '')");

        $this->addSql("SELECT pg_catalog.setval('bdterr.champ_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.champtype_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.contenu_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.donnee_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.lot_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.rapport_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.rapports_profils_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.referentiel_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.referentiel_recherche_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('bdterr.theme_id_seq', 1, false)");

        $this->addSql("INSERT INTO catalogue.acces_serveur (pk_acces_serveur, ts, accs_id, accs_adresse, path_consultation, path_administration, path_carte_statique, accs_adresse_admin, accs_adresse_tele, accs_login_admin, accs_pwd_admin, accs_service_admin, accs_adresse_data) VALUES (1, '2007-02-28 11:07:04.150759', 'Serveur cartographique régional', 'carto.prodige.internal', '/', '/HTML_PRIVATE/index.php?map=', '/CartesStatiques/', 'admincarto.prodige.internal', 'telecarto.prodige.internal', 'cartes', 'cartes', 1, 'datacarto.prodige.internal')");

        $this->addSql("INSERT INTO catalogue.couche_donnees (pk_couche_donnees, ts, couchd_id, couchd_nom, couchd_description, couchd_type_stockage, couchd_emplacement_stockage, couchd_fk_acces_server, couchd_wms, couchd_wfs, couchd_download, couchd_restriction, couchd_restriction_exclusif, couchd_zonageid_fk, couchd_restriction_buffer, couchd_visualisable, couchd_extraction_attributaire, couchd_extraction_attributaire_champ, changedate, couchd_alert_msg_id, couchd_wfs_uuid, couchd_wms_sdom) VALUES (269, '2009-03-06 17:38:25.8314', '1_269', 'departements', 'departements', 1, 'departements', 1, 0, 0, 0, 0, 0, NULL, NULL, 1, 0, '', '2017-04-26 11:00:16.037012+02', NULL, NULL, NULL)");

        $this->addSql("INSERT INTO catalogue.fiche_metadonnees (pk_fiche_metadonnees, ts, fmeta_id, fmeta_description, fmeta_fk_couche_donnees, statut, schema) VALUES (330, '2009-03-06 17:38:41.802036', '9', NULL, 269, 4, NULL)");
        $this->addSql("INSERT INTO catalogue.fiche_metadonnees (pk_fiche_metadonnees, ts, fmeta_id, fmeta_description, fmeta_fk_couche_donnees, statut, schema) VALUES (331, '2009-03-06 17:54:02.250734', '10', NULL, NULL, 4, NULL)");

        $this->addSql("INSERT INTO catalogue.stockage_carte (pk_stockage_carte, ts, stkcard_id, stkcard_path, stk_server) VALUES (174, '2009-03-06 17:53:49.462071', '1_174_limites_administratives', 'limites_administratives.map', 1)");

        $this->addSql("INSERT INTO catalogue.utilisateur (pk_utilisateur, ts, usr_id, usr_nom, usr_prenom, usr_email, usr_telephone, usr_telephone2, usr_service, usr_description, usr_password, usr_pwdexpire, usr_generic, usr_ldap, usr_signature, date_expiration_compte) VALUES (1, '2007-03-02 10:45:52.52875', 'admin@prodige-opensource.org', 'PRODIGE_ADM', 'ADM', 'admin@prodige-opensource.org', '', '', 'Service', '', 'ac6f71ff549832d6139ac2acc57e946b', '2024-07-01', 0, 0, NULL, NULL)");
        $this->addSql("INSERT INTO catalogue.utilisateur (pk_utilisateur, ts, usr_id, usr_nom, usr_prenom, usr_email, usr_telephone, usr_telephone2, usr_service, usr_description, usr_password, usr_pwdexpire, usr_generic, usr_ldap, usr_signature, date_expiration_compte) VALUES (40, '2012-04-11 12:19:46.123204', 'vinternet', 'Internet', 'Internet', 'NEANT', NULL, NULL, NULL, NULL, 'dcaa6e60155776107c638af755498759', '2024-07-01', 1, 0, NULL, NULL)");

        $this->addSql("INSERT INTO catalogue.carte_projet (pk_carte_projet, ts, cartp_id, cartp_nom, cartp_description, cartp_format, cartp_fk_stockage_carte, cartp_fk_fiche_metadonnees, cartp_utilisateur, cartep_etat, cartp_administrateur, cartp_utilisateur_email, cartp_wms) VALUES (174, '2009-03-06 17:53:49.462071', '1_174_limites_administratives', 'limites_administratives', 'limites_administratives', 0, 174, 331, NULL, 0, NULL, NULL, 0)");

        $this->addSql("INSERT INTO catalogue.rubric_param (rubric_id, rubric_name) VALUES (14, 'PRODIGE')");

        $this->addSql("INSERT INTO catalogue.domaine (pk_domaine, ts, dom_id, dom_nom, dom_description, dom_rubric) VALUES (48, '2009-03-06 14:40:21.466063', 'Demonstration', 'Demonstration', 'D&amp;eacute;monstration', 14)");

        $this->addSql("SELECT pg_catalog.setval('catalogue.execution_report_id_seq', 1, false)");

        $this->addSql("INSERT INTO catalogue.groupe_profil (pk_groupe_profil, ts, grp_id, grp_nom, grp_description, grp_is_default_installation, grp_nom_ldap) VALUES (200, '2007-02-26 16:58:27.117093', 'ADM-PRODIGE', 'Administrateur Prodige', 'Administrateur principal de l''application', 1, NULL)");
        $this->addSql("INSERT INTO catalogue.groupe_profil (pk_groupe_profil, ts, grp_id, grp_nom, grp_description, grp_is_default_installation, grp_nom_ldap) VALUES (219, '2012-04-11 12:19:46.123956', 'Internet', 'Internet', 'Internet', 1, NULL)");

        $this->addSql("INSERT INTO catalogue.grp_accede_dom (pk_grp_accede_dom, ts, grpdom_fk_domaine, grpdom_fk_groupe_profil) VALUES (102, '2009-03-06 15:00:07.863653', 48, 200)");

        $this->addSql("INSERT INTO catalogue.sous_domaine (pk_sous_domaine, ts, ssdom_id, ssdom_nom, ssdom_description, ssdom_fk_domaine, ssdom_admin_fk_groupe_profil, ssdom_createur_fk_groupe_profil, ssdom_editeur_fk_groupe_profil, ssdom_service_wms, ssdom_service_wms_uuid) VALUES (217, '2009-03-06 14:57:44.783952', 'France', 'France', 'France', 48, 200, 200, NULL, 0, NULL)");

        $this->addSql("INSERT INTO catalogue.grp_accede_ssdom (pk_grp_accede_ssdom, ts, grpss_fk_sous_domaine, grpss_fk_groupe_profil) VALUES (7, '2009-03-06 15:01:05.886418', 217, 200)");

        $this->addSql("INSERT INTO catalogue.traitement (pk_traitement, ts, trt_id, trt_nom, trt_description, trt_administre, trt_accede, trt_edite) VALUES (1, '2006-05-15 10:32:20.274767', 'TELECHARGEMENT', 'Acc&egrave;s &agrave; l''outil de t&eacute;l&eacute;chargement', 'Permet le t&eacute;l&eacute;chargement de donn&eacute;es g&eacute;ographiques', false, true, false)");
        $this->addSql("INSERT INTO catalogue.traitement (pk_traitement, ts, trt_id, trt_nom, trt_description, trt_administre, trt_accede, trt_edite) VALUES (4, '2006-05-15 10:32:20.285331', 'CMS', 'Acc&egrave;s &agrave; l''outil de cr&eacute;ation / modification / suppression des m&eacute;tadonn&eacute;es', 'Permet l''administration des fiches de m&eacute;tadonn&eacute;es', true, false, false)");
        $this->addSql("INSERT INTO catalogue.traitement (pk_traitement, ts, trt_id, trt_nom, trt_description, trt_administre, trt_accede, trt_edite) VALUES (2, '2006-05-15 10:32:20.279317', 'NAVIGATION', 'Acc&egrave;s &agrave; l''outil de navigation cartographique', 'Permet de consulter les cartes', false, true, false)");
        $this->addSql("INSERT INTO catalogue.traitement (pk_traitement, ts, trt_id, trt_nom, trt_description, trt_administre, trt_accede, trt_edite) VALUES (3, '2006-05-15 10:32:20.282272', 'ADMINISTRATION', 'Acc&egrave;s &agrave; l''outil d''administration', 'Gestion des droits PRODIGE.', false, false, false)");
        $this->addSql("INSERT INTO catalogue.traitement (pk_traitement, ts, trt_id, trt_nom, trt_description, trt_administre, trt_accede, trt_edite) VALUES (7, '2006-05-15 10:32:20.293369', 'PARAMETRAGE', 'Acc&egrave;s &agrave; l''outil de param&eacute;trage des cartes projets.', 'Permet d''administrer les cartes', true, false, false)");
        $this->addSql("INSERT INTO catalogue.traitement (pk_traitement, ts, trt_id, trt_nom, trt_description, trt_administre, trt_accede, trt_edite) VALUES (8, '2010-07-29 17:46:20.53123', 'MAJIC', 'Acc&egrave;s &agrave; l''outil d''administration des donn&eacute;es MAJIC.', 'Permet d''administrer les donn&eacute;es MAJIC', true, false, false)");
        $this->addSql("INSERT INTO catalogue.traitement (pk_traitement, ts, trt_id, trt_nom, trt_description, trt_administre, trt_accede, trt_edite) VALUES (9, '2010-11-23 17:46:20.53123', 'CARTEPERSO', 'Acc&egrave;s &agrave; l''outil Carte Personnelle', 'Permet de cr&eacute;er des cartes personnelles', false, false, false)");
        $this->addSql("INSERT INTO catalogue.traitement (pk_traitement, ts, trt_id, trt_nom, trt_description, trt_administre, trt_accede, trt_edite) VALUES (10, '2017-04-26 11:03:07.476403', 'PUBLIPOSTAGE', 'Acc&egrave;s &agrave; l''outil de publipostage', 'Permet d''envoyer des mailing &agrave; partir d''informations attributaires', false, true, false)");
        $this->addSql("INSERT INTO catalogue.traitement (pk_traitement, ts, trt_id, trt_nom, trt_description, trt_administre, trt_accede, trt_edite) VALUES (11, '2017-04-26 11:03:07.478022', 'EDITION EN LIGNE', 'Acc&egrave;s &agrave; l''outil d''édition de donnée', 'Permet de modifier en ligne des couches de type point, ligne et polygone', true, true, true)");
        $this->addSql("INSERT INTO catalogue.traitement (pk_traitement, ts, trt_id, trt_nom, trt_description, trt_administre, trt_accede, trt_edite) VALUES (12, '2017-04-26 11:03:07.485083', 'EDITION EN LIGNE (AJOUT)', 'Acc&egrave;s &agrave; l''outil d''édition de donnée (fonction d''ajout)', 'Permet de créer en ligne des objets de type point, ligne et polygone', true, true, true)");
        $this->addSql("INSERT INTO catalogue.traitement (pk_traitement, ts, trt_id, trt_nom, trt_description, trt_administre, trt_accede, trt_edite) VALUES (13, '2017-04-27 10:55:19.459418', 'EDITION EN LIGNE (MODIF)', 'l''outil de modification de donnée (fonction de modification)', 'Permet de modifier en ligne des objets', true, true, true)");

        $this->addSql("INSERT INTO catalogue.grp_autorise_trt (pk_grp_autorise_trt, ts, grptrt_fk_traitement, grptrt_fk_groupe_profil) VALUES (9, '2007-02-26 17:09:47.800647', 3, 200)");
        $this->addSql("INSERT INTO catalogue.grp_autorise_trt (pk_grp_autorise_trt, ts, grptrt_fk_traitement, grptrt_fk_groupe_profil) VALUES (10, '2007-02-26 17:09:48.392514', 2, 200)");
        $this->addSql("INSERT INTO catalogue.grp_autorise_trt (pk_grp_autorise_trt, ts, grptrt_fk_traitement, grptrt_fk_groupe_profil) VALUES (12, '2007-02-26 17:17:51.040256', 7, 200)");
        $this->addSql("INSERT INTO catalogue.grp_autorise_trt (pk_grp_autorise_trt, ts, grptrt_fk_traitement, grptrt_fk_groupe_profil) VALUES (13, '2007-02-26 17:17:52.077436', 4, 200)");
        $this->addSql("INSERT INTO catalogue.grp_autorise_trt (pk_grp_autorise_trt, ts, grptrt_fk_traitement, grptrt_fk_groupe_profil) VALUES (14, '2007-02-26 17:17:57.586974', 1, 200)");

        $this->addSql("INSERT INTO catalogue.grp_regroupe_usr (pk_grp_regroupe_usr, ts, grpusr_fk_groupe_profil, grpusr_fk_utilisateur, grpusr_is_principal) VALUES (1, '2007-03-02 11:20:54.78394', 200, 1, 0)");
        $this->addSql("INSERT INTO catalogue.grp_regroupe_usr (pk_grp_regroupe_usr, ts, grpusr_fk_groupe_profil, grpusr_fk_utilisateur, grpusr_is_principal) VALUES (59, '2012-04-11 12:19:46.124562', 219, 40, 0)");

        $this->addSql("INSERT INTO catalogue.objet_type (pk_objet_type_id, objet_type_nom) VALUES (1, 'couche')");
        $this->addSql("INSERT INTO catalogue.objet_type (pk_objet_type_id, objet_type_nom) VALUES (2, 'carte')");
        $this->addSql("INSERT INTO catalogue.objet_type (pk_objet_type_id, objet_type_nom) VALUES (4, 'modéle')");
        $this->addSql("INSERT INTO catalogue.objet_type (pk_objet_type_id, objet_type_nom) VALUES (5, 'groupe')");

        $this->addSql("INSERT INTO catalogue.trt_objet (pk_trt_objet, trt_id, trt_nom) VALUES (1, 'TELECHARGEMENT', 'Acc&egrave;s &agrave; l''outil de t&eacute;l&eacute;chargement  ')");
        $this->addSql("INSERT INTO catalogue.trt_objet (pk_trt_objet, trt_id, trt_nom) VALUES (2, 'CONSULTATION', 'Acc&egrave;s &agrave; l''outil de consultation')");
        $this->addSql("INSERT INTO catalogue.trt_objet (pk_trt_objet, trt_id, trt_nom) VALUES (7, 'ADMINISTRATION', 'Acc&egrave;s &agrave; l''outil d''administration')");
        $this->addSql("INSERT INTO catalogue.trt_objet (pk_trt_objet, trt_id, trt_nom) VALUES (8, 'SUPPRESSION', 'Acc&egrave;s &agrave; l''outil de supression')");
        $this->addSql("INSERT INTO catalogue.trt_objet (pk_trt_objet, trt_id, trt_nom) VALUES (9, 'EDITION', 'Acc&egrave;s &agrave; l''outil d''édition de donnée')");
        $this->addSql("INSERT INTO catalogue.trt_objet (pk_trt_objet, trt_id, trt_nom) VALUES (10, 'EDITION (AJOUT)', 'Acc&egrave;s &agrave; l''outil d''édition de donnée (fonction d''ajout)')");
        $this->addSql("INSERT INTO catalogue.trt_objet (pk_trt_objet, trt_id, trt_nom) VALUES (11, 'EDITION EN LIGNE (MODIF)', 'Acc&egrave;s &agrave; l''outil modification de donnée (fonction de modification)')");

        $this->addSql("SELECT pg_catalog.setval('catalogue.\"harves_opendata_node_params_FK_node_id_seq\"', 1, false)");

        $this->addSql("SELECT pg_catalog.setval('catalogue.harves_opendata_node_params_pk_params_id_seq', 1, false)");

        $this->addSql("SELECT pg_catalog.setval('catalogue.harves_opendata_node_pk_node_id_seq', 1, false)");

        $this->addSql("INSERT INTO catalogue.prodige_carto_colors (prodige_carto_color_id, color_theme, color_white, color_light, color_dark, color_light_grey, color_tables, color_grey, color_theme_extjs) VALUES (1, 'Bleu', 'e8e8ff', 'c8d2dc', '114f88', '87a6c3', '003399', '56779a', 'blue')");
        $this->addSql("INSERT INTO catalogue.prodige_carto_colors (prodige_carto_color_id, color_theme, color_white, color_light, color_dark, color_light_grey, color_tables, color_grey, color_theme_extjs) VALUES (2, 'Vert', 'edffe8', 'cbdcc8', '198811', '87c387', '07461e', '5b9a56', 'olive')");
        $this->addSql("INSERT INTO catalogue.prodige_carto_colors (prodige_carto_color_id, color_theme, color_white, color_light, color_dark, color_light_grey, color_tables, color_grey, color_theme_extjs) VALUES (3, 'Rouge', 'ffefeb', 'dccfc8', '883011', 'c38787', 'b50000', '9a5656', 'peppermint')");
        $this->addSql("INSERT INTO catalogue.prodige_carto_colors (prodige_carto_color_id, color_theme, color_white, color_light, color_dark, color_light_grey, color_tables, color_grey, color_theme_extjs) VALUES (4, 'Orange', 'fff3e8', 'dcd4c8', '885711', 'c3a887', 'e17a00', '9a7e56', 'orange')");
        $this->addSql("INSERT INTO catalogue.prodige_carto_colors (prodige_carto_color_id, color_theme, color_white, color_light, color_dark, color_light_grey, color_tables, color_grey, color_theme_extjs) VALUES (5, 'Violet', 'ffebff', 'dbc8dc', '701188', 'bb87c3', '3a046d', '9a5696', 'purple')");
        $this->addSql("INSERT INTO catalogue.prodige_carto_colors (prodige_carto_color_id, color_theme, color_white, color_light, color_dark, color_light_grey, color_tables, color_grey, color_theme_extjs) VALUES (6, 'Marron', 'fff7eb', 'dcd1c8', '884911', 'c3a387', '422f05', '9a7e56', 'brown')");
        $this->addSql("INSERT INTO catalogue.prodige_carto_colors (prodige_carto_color_id, color_theme, color_white, color_light, color_dark, color_light_grey, color_tables, color_grey, color_theme_extjs) VALUES (7, 'Gris', 'f1f1f1', 'd7d7d7', '878787', 'c3c0be', '000000', '969593', 'grey')");

        $this->addSql("SELECT pg_catalog.setval('catalogue.prodige_carto_colors_prodige_carto_color_id_seq', 7, true)");

        $this->addSql("INSERT INTO catalogue.prodige_colors (prodige_color_id, color_theme, color_back, color_tables, color_text, color_text_light, volet_url, css_file) VALUES (1, 'Bleu', '#f5f9ff', '#003399', '#15428b', '#333333', 'volet_bleu.png', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_colors (prodige_color_id, color_theme, color_back, color_tables, color_text, color_text_light, volet_url, css_file) VALUES (2, 'Vert', '#f8ffeb', '#759341', '#324b06', '#333333', 'volet_vert.png', 'CSS/olive/css/xtheme-olive.css')");
        $this->addSql("INSERT INTO catalogue.prodige_colors (prodige_color_id, color_theme, color_back, color_tables, color_text, color_text_light, volet_url, css_file) VALUES (3, 'Rouge', '#fff2f2', '#FF5353', '#a40e0e', '#333333', 'volet_rouge.png', 'CSS/xtheme-peppermint/xtheme-peppermint.css')");
        $this->addSql("INSERT INTO catalogue.prodige_colors (prodige_color_id, color_theme, color_back, color_tables, color_text, color_text_light, volet_url, css_file) VALUES (4, 'Violet', '#fbfaff', '#84d0ef', '#260496', '#333333', 'volet_violet.png', 'CSS/PurpleTheme/css/xtheme-purple.css')");
        $this->addSql("INSERT INTO catalogue.prodige_colors (prodige_color_id, color_theme, color_back, color_tables, color_text, color_text_light, volet_url, css_file) VALUES (5, 'Marron', '#ffffee', '#422f05', '#913006', '#333333', 'volet_marron.png', 'CSS/xtheme-light-brown/css/xtheme-light-brown.css')");
        $this->addSql("INSERT INTO catalogue.prodige_colors (prodige_color_id, color_theme, color_back, color_tables, color_text, color_text_light, volet_url, css_file) VALUES (6, 'Gris', '#f1f1f1', '#000000', '#636060', '#333333', 'volet_gris.png', 'CSS/DarkGrayTheme/css/xtheme-darkgray.css')");
        $this->addSql("INSERT INTO catalogue.prodige_colors (prodige_color_id, color_theme, color_back, color_tables, color_text, color_text_light, volet_url, css_file) VALUES (7, 'Orange', '#fff9ee', '#e17a00', '#e58700', '#333333', 'volet_orange.png', 'CSS/xtheme-light-orange/css/xtheme-light-orange.css')");

        $this->addSql("SELECT pg_catalog.setval('catalogue.prodige_colors_prodige_color_id_seq', 7, true)");

        $this->addSql("SELECT pg_catalog.setval('catalogue.prodige_database_requests_prodige_database_request_id_seq', 47, true)");

        $this->addSql("INSERT INTO catalogue.prodige_fonts (font_id, font1, font2, font3) VALUES (1, 'Arial', 'Helvetica', 'sans-serif')");
        $this->addSql("INSERT INTO catalogue.prodige_fonts (font_id, font1, font2, font3) VALUES (2, 'Bookman Old Style', 'Helvetica', 'sans-serif')");
        $this->addSql("INSERT INTO catalogue.prodige_fonts (font_id, font1, font2, font3) VALUES (3, 'Comic Sans MS', 'monospace', 'sans-serif')");
        $this->addSql("INSERT INTO catalogue.prodige_fonts (font_id, font1, font2, font3) VALUES (4, 'Garamond', 'Helvetica', 'sans-serif')");
        $this->addSql("INSERT INTO catalogue.prodige_fonts (font_id, font1, font2, font3) VALUES (5, 'Impact', 'Charcoal', 'sans-serif')");
        $this->addSql("INSERT INTO catalogue.prodige_fonts (font_id, font1, font2, font3) VALUES (6, 'Palatino Linotype', 'Book Antiqua', 'Palatino')");
        $this->addSql("INSERT INTO catalogue.prodige_fonts (font_id, font1, font2, font3) VALUES (7, 'Symbol', 'Helvetica', 'sans-serif')");
        $this->addSql("INSERT INTO catalogue.prodige_fonts (font_id, font1, font2, font3) VALUES (8, 'Tahoma', 'Geneva', 'sans-serif')");
        $this->addSql("INSERT INTO catalogue.prodige_fonts (font_id, font1, font2, font3) VALUES (9, 'Times New Roman', 'Times', 'serif')");
        $this->addSql("INSERT INTO catalogue.prodige_fonts (font_id, font1, font2, font3) VALUES (10, 'Trebuchet MS', 'Helvetica', 'sans-serif')");

        $this->addSql("SELECT pg_catalog.setval('catalogue.prodige_fonts_font_id_seq', 10, true)");

        $this->addSql("INSERT INTO catalogue.prodige_geosource_colors (prodige_geosource_color_id, geosource_color_theme, color_commontext, color_a, color_a_hover, color_left_menu_context, color_border, color_background_content, color_left_menu_text) VALUES (1, 'Bleu', '0263b2', '064377', '025090', 'b9d3f5', '266397', 'eff4fa', '15428b')");
        $this->addSql("INSERT INTO catalogue.prodige_geosource_colors (prodige_geosource_color_id, geosource_color_theme, color_commontext, color_a, color_a_hover, color_left_menu_context, color_border, color_background_content, color_left_menu_text) VALUES (2, 'Vert', '105f2f', '105f2f', '07461e', '92c95d', '07461e', 'e8f9d7', '000000')");
        $this->addSql("INSERT INTO catalogue.prodige_geosource_colors (prodige_geosource_color_id, geosource_color_theme, color_commontext, color_a, color_a_hover, color_left_menu_context, color_border, color_background_content, color_left_menu_text) VALUES (3, 'Rouge', '822626', '822626', 'b50000', 'ffa8a8', 'b50000', 'ffeeee', '000000')");
        $this->addSql("INSERT INTO catalogue.prodige_geosource_colors (prodige_geosource_color_id, geosource_color_theme, color_commontext, color_a, color_a_hover, color_left_menu_context, color_border, color_background_content, color_left_menu_text) VALUES (4, 'Orange', '915003', '915003', 'e17a00', 'ffcc90', 'e17a00', 'fef0df', 'a03e00')");
        $this->addSql("INSERT INTO catalogue.prodige_geosource_colors (prodige_geosource_color_id, geosource_color_theme, color_commontext, color_a, color_a_hover, color_left_menu_context, color_border, color_background_content, color_left_menu_text) VALUES (5, 'Violet', '490b82', '490b82', '3a046d', 'd2a2ff', '3a046d', 'f2edff', '000000')");
        $this->addSql("INSERT INTO catalogue.prodige_geosource_colors (prodige_geosource_color_id, geosource_color_theme, color_commontext, color_a, color_a_hover, color_left_menu_context, color_border, color_background_content, color_left_menu_text) VALUES (6, 'Marron', '523d11', '523d11', '422f05', 'e3b98c', '422f05', 'f2e9e0', 'a2753f')");
        $this->addSql("INSERT INTO catalogue.prodige_geosource_colors (prodige_geosource_color_id, geosource_color_theme, color_commontext, color_a, color_a_hover, color_left_menu_context, color_border, color_background_content, color_left_menu_text) VALUES (7, 'Gris', '424242', '424242', '000000', '888888', '000000', 'ececec', 'ffffff')");

        $this->addSql("SELECT pg_catalog.setval('catalogue.prodige_geosource_colors_prodige_geosource_color_id_seq', 7, true)");

        $this->addSql("INSERT INTO catalogue.prodige_help (pk_prodige_help_id, prodige_help_title, prodige_help_desc, prodige_help_url, prodige_help_row_number) VALUES (4, 'Manuel de l&#039;outil de consultation de cartes', 'Ce manuel pr&eacute;sente les fonctions du module de consultation de cartes.', './ressources/Manuel Outil de consultation cartographique.pdf', 0)");
        $this->addSql("INSERT INTO catalogue.prodige_help (pk_prodige_help_id, prodige_help_title, prodige_help_desc, prodige_help_url, prodige_help_row_number) VALUES (5, 'Manuel de l&#039;outil d&#039;administration de cartes', 'Ce manuel pr&eacute;sente les fonctions du module d&#039;administration de cartes.', './ressources/Manuel Outil d&#039;administration cartographique.pdf', 1)");
        $this->addSql("INSERT INTO catalogue.prodige_help (pk_prodige_help_id, prodige_help_title, prodige_help_desc, prodige_help_url, prodige_help_row_number) VALUES (2, 'Manuel de l&#039;outil de gestion des m&eacute;tadonn&eacute;es', 'Ce manuel pr&eacute;sente les fonctions d&#039;administration et de consultation du catalogue.', './ressources/Manuel Outil de gestion des metadonnees.pdf', 2)");

        $this->addSql("INSERT INTO catalogue.prodige_help_group (hlpgrp_fk_help_id, hlpgrp_fk_groupe_profil, pk_prodige_help_group) VALUES (4, 200, 1)");
        $this->addSql("INSERT INTO catalogue.prodige_help_group (hlpgrp_fk_help_id, hlpgrp_fk_groupe_profil, pk_prodige_help_group) VALUES (4, 219, 2)");
        $this->addSql("INSERT INTO catalogue.prodige_help_group (hlpgrp_fk_help_id, hlpgrp_fk_groupe_profil, pk_prodige_help_group) VALUES (5, 200, 3)");
        $this->addSql("INSERT INTO catalogue.prodige_help_group (hlpgrp_fk_help_id, hlpgrp_fk_groupe_profil, pk_prodige_help_group) VALUES (2, 200, 4)");

        $this->addSql("INSERT INTO catalogue.prodige_param (pk_prodige_param, prodige_title, prodige_subtitle, prodige_catalog_intro, logo_url, top_image_url, font_id, prodige_color_id, prodige_geosource_color_id, prodige_carto_color_id, logo_right_url) VALUES (1, 'PRODIGE', 'Plate-forme mutualisée pour le partage d''information géographique', 'PRODIGE', 'Logo_Prodige_V0.jpg', 'bandeau_bleu.png', 1, 1, 1, 1, NULL)");

        $this->addSql("SELECT pg_catalog.setval('catalogue.prodige_param_pk_prodige_param_seq', 1, true)");

        $this->addSql("SELECT pg_catalog.setval('catalogue.prodige_server_server_id_seq', 2, true)");

        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (4, 'PRO_TIMEOUT_TELE_DIRECT', NULL, '18000', 'Timeout du service de téléchargement (en ms)', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (5, 'PRO_IS_MAJIC_ACTIF', NULL, 'off', 'Activation du module MAJIC', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (8, 'PRO_IS_CARTEPERSO_ACTIF', NULL, 'off', 'Activation du module carte personnelle', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (9, 'PRO_IMPORT_EPSG', 'Projection principale de la plateforme', '2154', 'Projection principale de la plateforme (code EPSG)', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (6, 'PRO_RASTER_INFO_ECW_SIZE_LIMIT', NULL, '500000000', 'Taille maximale des fichiers Raster ECW téléchargés (en octet)', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (10, 'PRO_IS_REQ_JOINTURES_ACTIF', NULL, 'off', 'Activation des requêtes et jointures', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (7, 'PRO_RASTER_INFO_GTIFF_SIZE_LIMIT', 'NULL', '100000000000', 'Taille maximale des fichiers Raster GeoTIFF téléchargés (en octet)', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (23, 'PRO_CATALOGUE_EMAIL_ADMIN', NULL, 'admin@prodige-opensource.org', 'Adresse mail d''administration', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (22, 'PRO_CATALOGUE_NOM_EMAIL_ADMIN', NULL, 'Administrateur Prodige', 'Nom de l''expéditeur des mails d''administration', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (21, 'PRO_CATALOGUE_EMAIL_AUTO', NULL, 'no_reply@prodige.fr', 'Adresse des mails automatiques', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (20, 'PRO_CATALOGUE_NOM_EMAIL_AUTO', NULL, 'No reply', 'Nom de l''expéditeur des mails automatiques', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (19, 'PRO_CATALOGUE_NB_SESSION_USER', NULL, 'off', 'Afficher le nombre de visiteur', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (18, 'PRO_CATALOGUE_CONTACT_ADMIN', NULL, 'off', 'Possibilité de contacter un admininstrateur', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (17, 'PRO_GEONETWORK_LIST_CATALOGUE', NULL, '', 'Liste des catalogues', 'multiselectfield', 'uuid,name;public.sources')");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (16, 'PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL', NULL, '', 'Url du fichier de licence', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (15, 'PRO_CATALOGUE_TELECHARGEMENT_LICENCE', NULL, 'off', 'Affichage d''une licence dans l''interface de téléchargement', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (24, 'PRO_ACTIVE_CARTE_PERSO', NULL, 'off', 'Active le module cartes personnelles pour l''internaute', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (25, 'PRO_WMS_METADATA_ID', NULL, '40020', 'Identifiant de la métadonnée de service WMS', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (26, 'PRO_WFS_METADATA_ID', NULL, '40021', 'Identifiant de la métadonnée de service WFS', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (27, 'PRO_DOWNLOAD_METADATA_ID', NULL, '40022', 'Identifiant de la métadonnée de service de téléchargement libre', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (28, 'PRO_PUBLIPOSTAGE', NULL, 'off', 'Module publipostage activé', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (29, 'PRO_EDITION', NULL, 'off', 'Module édition en ligne activé', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (30, 'PRO_MODULE_STANDARDS', 'Module qualité activé', 'off', 'Module qualité activé', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (31, 'PRO_MODULE_BASE_TERRITORIALE', 'Module Base territoriale activé', 'off', 'Module Base territoriale activé', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (32, 'PRO_HARVES_DCAT_ORGANIZATIONS', NULL, 'https://www.data.gouv.fr/api/1/organizations/', 'Définit l''url d''API fournissant la liste des organisations disponibles', 'textfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (33, 'PRO_IS_OPENDATA_ACTIF', NULL, 'off', 'Module opendata activé', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (34, 'PRO_IS_RAWGRAPH_ACTIF', NULL, 'off', 'Module graphe activé', 'checkboxfield', NULL)");
        $this->addSql("INSERT INTO catalogue.prodige_settings (pk_prodige_settings_id, prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES (35, 'PRO_NONGEO_METADATA_ID', NULL, '40787', 'Identifiant de la métadonnée de série de données non géographiques modèle', 'textfield', NULL)");

        $this->addSql("SELECT pg_catalog.setval('catalogue.rubric_param_rubric_id_seq', 13, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.scheduled_command_id_seq', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_acces_serveur', 1, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_carte_projet', 171, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_commune', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_competence', 2, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_competence_accede_carte', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_competence_accede_couche', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_couche_donnees', 266, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_departement', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_dico', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_dico_majic2', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_domaine', 39, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_export', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_fiche_metadonnees', 326, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_groupe_profil', 219, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_grp_accede_competence', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_grp_accede_dom', 100, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_grp_accede_ssdom', 6, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_grp_autorise_trt', 116, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_grp_regroupe_usr', 59, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_grp_trt_objet', 50, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_incoherence_perso', 0, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_mapfile', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_mcd_field', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_mcd_layer', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_menu', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_objet_type', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_perimetre', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_prodige_external_access', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_prodige_help', 5, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_prodige_help_group', 4, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_prodige_settings', 35, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_profil', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_rawgraph_couche_config', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_sous_domaine', 215, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_ssdom_dispose_couche', 365, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_ssdom_dispose_metadata', 2, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_ssdom_visualise_carte', 200, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_statistique', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_stockage_carte', 171, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_tache_import_donnees', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_traitement', 200, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_trt_autorise_objet', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_trt_objet', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_usr_accede_perimetre', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_usr_accede_perimetre_edition', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_usr_alerte_perimetre_edition', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_utilisateur', 40, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_utilisateur_carte', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_utilisateur_mapfile', 1, false)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.seq_zonage', 1, false)");

        $this->addSql("INSERT INTO catalogue.ssdom_dispose_couche (pk_ssdom_dispose_couche, ts, ssdcouch_fk_couche_donnees, ssdcouch_fk_sous_domaine) VALUES (368, '2009-03-06 17:38:41.802036', 269, 217)");

        $this->addSql("INSERT INTO catalogue.ssdom_dispose_metadata (pk_ssdom_dispose_metadata, ts, uuid, ssdcouch_fk_sous_domaine) VALUES (1, '2017-04-27 10:55:19.445171', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 217)");
        $this->addSql("INSERT INTO catalogue.ssdom_dispose_metadata (pk_ssdom_dispose_metadata, ts, uuid, ssdcouch_fk_sous_domaine) VALUES (2, '2017-04-27 10:55:19.445171', '617af7f0-0a6f-11de-ad6b-00104b7907b4', 217)");

        $this->addSql("INSERT INTO catalogue.ssdom_visualise_carte (pk_ssdom_visualise_carte, ts, ssdcart_fk_carte_projet, ssdcart_fk_sous_domaine) VALUES (202, '2009-03-06 17:54:02.315995', 174, 217)");

        $this->addSql("INSERT INTO catalogue.standards_fournisseur (fournisseur_id, fournisseur_name, fournisseur_url) VALUES (1, 'Prodige', 'prodige.org')");

        $this->addSql("INSERT INTO public.categories (id, name) VALUES (1, 'maps')");
        $this->addSql("INSERT INTO public.categories (id, name) VALUES (2, 'datasets')");
        $this->addSql("INSERT INTO public.categories (id, name) VALUES (3, 'interactiveResources')");
        $this->addSql("INSERT INTO public.categories (id, name) VALUES (4, 'applications')");
        $this->addSql("INSERT INTO public.categories (id, name) VALUES (5, 'caseStudies')");
        $this->addSql("INSERT INTO public.categories (id, name) VALUES (6, 'proceedings')");
        $this->addSql("INSERT INTO public.categories (id, name) VALUES (7, 'photo')");
        $this->addSql("INSERT INTO public.categories (id, name) VALUES (8, 'audioVideo')");
        $this->addSql("INSERT INTO public.categories (id, name) VALUES (9, 'directories')");
        $this->addSql("INSERT INTO public.categories (id, name) VALUES (10, 'otherResources')");
        $this->addSql("INSERT INTO public.categories (id, name) VALUES (11, 'z3950Servers')");
        $this->addSql("INSERT INTO public.categories (id, name) VALUES (12, 'registers')");

        $this->addSql("INSERT INTO public.users (id, username, password, surname, name, profile, organisation, kind, security, authtype, lastlogindate, nodeid, isenabled) VALUES (40102, 'admin@prodige-opensource.org', '', 'ADM', 'PRODIGE_ADM', 0, '', NULL, '', 'LDAP', '2017-04-27T11:50:14', 'srv', 'y')");
        $this->addSql("INSERT INTO public.users (id, username, password, surname, name, profile, organisation, kind, security, authtype, lastlogindate, nodeid, isenabled) VALUES (1, 'administrateur', '1cb5662aa3cc6ce81f14426f0e04451a9f4288eebc847d9813bde81568c437dabb3b0e1e0dee13c0', 'admin', 'admin', 0, NULL, NULL, '', NULL, '2017-04-26T11:00:48', 'srv', 'y')");
        $this->addSql("INSERT INTO public.users (id, username, password, surname, name, profile, organisation, kind, security, authtype, lastlogindate, nodeid, isenabled) VALUES (40, 'Internet', '', 'Internet', 'Internet', 4, NULL, NULL, '', 'LDAP', '', 'srv', 'y')");
        $this->addSql("INSERT INTO public.users (id, username, password, surname, name, profile, organisation, kind, security, authtype, lastlogindate, nodeid, isenabled) VALUES (40784, 'admincli', '', '', 'admincli', 0, '', NULL, '', 'LDAP', NULL, 'srv', 'y')");

        $this->addSql("INSERT INTO public.groups (id, name, description, email, referrer, logo, website, defaultcategory_id, enablecategoriesrestriction) VALUES (0, 'Intranet', '', '', 1, NULL, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.groups (id, name, description, email, referrer, logo, website, defaultcategory_id, enablecategoriesrestriction) VALUES (1, 'All (Internet)', '', '', 1, NULL, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.groups (id, name, description, email, referrer, logo, website, defaultcategory_id, enablecategoriesrestriction) VALUES (-1, 'GUEST', 'self-registered users', NULL, NULL, NULL, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.groups (id, name, description, email, referrer, logo, website, defaultcategory_id, enablecategoriesrestriction) VALUES (217, 'France', 'France', NULL, NULL, NULL, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.groups (id, name, description, email, referrer, logo, website, defaultcategory_id, enablecategoriesrestriction) VALUES (40785, 'Alkante', NULL, NULL, NULL, NULL, NULL, NULL, 'n')");

        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (1, 'c4e06459-add2-47e3-86a7-14addeb70e93', 'iso19139', 'y', 'n', '2010-03-04T15:46:22', '2010-03-04T15:46:22', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:fra=\"http://www.cnig.gouv.fr/2005/fra\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
  <gmd:fileIdentifier>
    <gco:CharacterString>c4e06459-add2-47e3-86a7-14addeb70e93</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>

  <gco:CharacterString>fre</gco:CharacterString></gmd:language>
  <gmd:characterSet>
    <gmd:MD_CharacterSetCode codeList=\"./resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\"/>
  </gmd:characterSet>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode\" codeListValue=\"series\"/>
  </gmd:hierarchyLevel><gmd:contact>
    <gmd:CI_ResponsibleParty>
      <gmd:individualName gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:individualName>
      <gmd:organisationName gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:organisationName>
      <gmd:positionName gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:positionName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone>
              <gmd:voice gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:voice>
              <gmd:facsimile gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:facsimile>
            </gmd:CI_Telephone>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:deliveryPoint gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:deliveryPoint>
              <gmd:city gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:city>
              <gmd:administrativeArea gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:administrativeArea>
              <gmd:postalCode gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:postalCode>
              <gmd:country gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:country>
              <gmd:electronicMailAddress gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode\" codeListValue=\"originator\"/>
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gmd:contact>
  <gmd:dateStamp>
    <gco:DateTime>2010-06-18T16:01:29</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:metadataStandardVersion>
    <gco:CharacterString>1.0</gco:CharacterString>
  </gmd:metadataStandardVersion>
  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>
    <gmd:MD_DataIdentification>
      <gmd:citation>
        <gmd:CI_Citation>
          <gmd:title gco:nilReason=\"missing\">
            <gco:CharacterString>métadonnée d''ensemble de séries de données</gco:CharacterString>
          </gmd:title><gmd:date>
            <gmd:CI_Date>
              <gmd:date>
                <gco:DateTime/>
              </gmd:date>
              <gmd:dateType>
                <gmd:CI_DateTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode\" codeListValue=\"publication\"/>
              </gmd:dateType>
            </gmd:CI_Date>
          </gmd:date>


        </gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:abstract>
      <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty>
          <gmd:individualName gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:individualName>
          <gmd:organisationName gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:organisationName>
          <gmd:contactInfo>
            <gmd:CI_Contact>
              <gmd:phone>
                <gmd:CI_Telephone>
                  <gmd:voice gco:nilReason=\"missing\">
                    <gco:CharacterString/>
                  </gmd:voice>
                </gmd:CI_Telephone>
              </gmd:phone>
              <gmd:address>
                <gmd:CI_Address>
                  <gmd:electronicMailAddress gco:nilReason=\"missing\">
                    <gco:CharacterString/>
                  </gmd:electronicMailAddress>
                </gmd:CI_Address>
              </gmd:address>
            </gmd:CI_Contact>
          </gmd:contactInfo>
          <gmd:role>
            <gmd:CI_RoleCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode\" codeListValue=\"distributor\"/>
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:keyword>
          <gmd:keyword gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"theme\"/>
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords xlink:href=\"https://catalogue.prodige.internal/geonetwork/srv/fr/xml.keyword.get?thesaurus=external.place.regions&amp;id=http://geonetwork-opensource.org/regions%2368&amp;multiple=false\" xlink:title=\"\" xlink:role=\"pointOfContact\">
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>France</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"place\"/>
          </gmd:type>
          <gmd:thesaurusName>
            <gmd:CI_Citation>
              <gmd:title>
                <gco:CharacterString>external.place.regions</gco:CharacterString>
              </gmd:title>
              <gmd:date gco:nilReason=\"unknown\"/>
            </gmd:CI_Citation>
          </gmd:thesaurusName>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints>
          <gmd:useLimitation gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:useLimitation>
          <gmd:accessConstraints>
            <gmd:MD_RestrictionCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"restricted\"/>
          </gmd:accessConstraints>
          <gmd:useConstraints>
            <gmd:MD_RestrictionCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"copyright\"/>
          </gmd:useConstraints>
          <gmd:otherConstraints gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:otherConstraints>
        </gmd:MD_LegalConstraints>
      </gmd:resourceConstraints>
      <gmd:resourceConstraints>
        <gmd:MD_SecurityConstraints>
          <gmd:classification>
            <gmd:MD_ClassificationCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_ClassificationCode\" codeListValue=\"\"/>
          </gmd:classification>
          <gmd:userNote gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:userNote>
        </gmd:MD_SecurityConstraints>
      </gmd:resourceConstraints>
      <gmd:spatialRepresentationType>
        <gmd:MD_SpatialRepresentationTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_SpatialRepresentationTypeCode\" codeListValue=\"vector\"/>
      </gmd:spatialRepresentationType>
      <gmd:spatialResolution>
        <gmd:MD_Resolution>
          <gmd:equivalentScale>
            <gmd:MD_RepresentativeFraction>
              <gmd:denominator>
                <gco:Integer/>
              </gmd:denominator>
            </gmd:MD_RepresentativeFraction>
          </gmd:equivalentScale>
        </gmd:MD_Resolution>
      </gmd:spatialResolution>
      <gmd:spatialResolution>
        <gmd:MD_Resolution>
          <gmd:distance/>
        </gmd:MD_Resolution>
      </gmd:spatialResolution>
      <gmd:language>
        <gco:CharacterString>fre</gco:CharacterString>
      </gmd:language>
      <gmd:characterSet>
        <gmd:MD_CharacterSetCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\"/>
      </gmd:characterSet>
      <gmd:topicCategory>
        <gmd:MD_TopicCategoryCode/>
      </gmd:topicCategory>
      <gmd:extent>
        <gmd:EX_Extent>
          <gmd:description gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:description>
          <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
              <gmd:westBoundLongitude>
                <gco:Decimal>-4.31</gco:Decimal>
              </gmd:westBoundLongitude>
              <gmd:eastBoundLongitude>
                <gco:Decimal>8.40</gco:Decimal>
              </gmd:eastBoundLongitude>
              <gmd:southBoundLatitude>
                <gco:Decimal>41.24</gco:Decimal>
              </gmd:southBoundLatitude>
              <gmd:northBoundLatitude>
                <gco:Decimal>51</gco:Decimal>
              </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
          </gmd:geographicElement>

        </gmd:EX_Extent>
      </gmd:extent><gmd:supplementalInformation gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:supplementalInformation>


    </gmd:MD_DataIdentification>
  </gmd:identificationInfo>
  <gmd:dataQualityInfo>
    <gmd:DQ_DataQuality>
      <gmd:scope>
        <gmd:DQ_Scope>
          <gmd:level>
            <gmd:MD_ScopeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode\" codeListValue=\"series\"/>
          </gmd:level>
        </gmd:DQ_Scope>
      </gmd:scope>
      <gmd:lineage>
        <gmd:LI_Lineage>
          <gmd:statement gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:statement>
          <gmd:source>
            <gmd:LI_Source>
              <gmd:description gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:description>
            </gmd:LI_Source>
          </gmd:source>
        </gmd:LI_Lineage>
      </gmd:lineage>
    </gmd:DQ_DataQuality>
  </gmd:dataQualityInfo>

</gmd:MD_Metadata>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, 'gmd:MD_Metadata', NULL, 1, 1, NULL, 0, 0, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (40085, '9ba5f3e9-c0ef-4160-81c0-b633b6067601', 'iso19139', 'y', 'n', '2012-04-06T10:42:35', '2012-04-06T10:42:35', '<gmd:CI_ResponsibleParty xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:geonet=\"http://www.fao.org/geonetwork\">
  <gmd:individualName>
    <gco:CharacterString>Nom</gco:CharacterString>
  </gmd:individualName>
  <gmd:organisationName>
    <gco:CharacterString>Organisation</gco:CharacterString>
  </gmd:organisationName>
  <gmd:positionName>
    <gco:CharacterString>Fonction</gco:CharacterString>
  </gmd:positionName>
  <gmd:contactInfo>
    <gmd:CI_Contact>
      <gmd:address>
        <gmd:CI_Address>
          <gmd:electronicMailAddress>
            <gco:CharacterString>Email</gco:CharacterString>
          </gmd:electronicMailAddress>
        </gmd:CI_Address>
      </gmd:address>
    </gmd:CI_Contact>
  </gmd:contactInfo>
  <gmd:role>
    <gmd:CI_RoleCode codeList=\"./resources/codeList.xml#CI_RoleCode\" codeListValue=\"pointOfContact\" />
  </gmd:role>
</gmd:CI_ResponsibleParty>', '7fc45be3-9aba-4198-920c-b8737112d522', 'PointOfContact', 'gmd:CI_ResponsibleParty', NULL, 1, 1, NULL, 0, 1, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (2, '493039f0-eef0-11da-a2ce-000c2929140b', 'iso19139', 'y', 'n', '2006-05-29T10:51:14', '2006-07-27T09:02:02', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:fra=\"http://www.cnig.gouv.fr/2005/fra\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
  <gmd:fileIdentifier>
    <gco:CharacterString>493039f0-eef0-11da-a2ce-000c2929140b</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>

  <gco:CharacterString>fre</gco:CharacterString></gmd:language>
  <gmd:characterSet>
    <gmd:MD_CharacterSetCode codeList=\"./resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\"/>
  </gmd:characterSet>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode\" codeListValue=\"dataset\"/>
  </gmd:hierarchyLevel><gmd:contact>
    <gmd:CI_ResponsibleParty>
      <gmd:individualName gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:individualName>
      <gmd:organisationName gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:organisationName>
      <gmd:positionName gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:positionName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone>
              <gmd:voice gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:voice>
              <gmd:facsimile gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:facsimile>
            </gmd:CI_Telephone>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:deliveryPoint gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:deliveryPoint>
              <gmd:city gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:city>
              <gmd:administrativeArea gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:administrativeArea>
              <gmd:postalCode gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:postalCode>
              <gmd:country gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:country>
              <gmd:electronicMailAddress gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode\" codeListValue=\"originator\"/>
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gmd:contact>
  <gmd:dateStamp>
    <gco:DateTime>2010-06-18T15:55:36</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:metadataStandardVersion>
    <gco:CharacterString>1.0</gco:CharacterString>
  </gmd:metadataStandardVersion>
  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>
    <gmd:MD_DataIdentification>
      <gmd:citation>
        <gmd:CI_Citation>
          <gmd:title gco:nilReason=\"missing\">
            <gco:CharacterString>métadonnée de série de données standard</gco:CharacterString>
          </gmd:title><gmd:date>
            <gmd:CI_Date>
              <gmd:date>
                <gco:DateTime/>
              </gmd:date>
              <gmd:dateType>
                <gmd:CI_DateTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode\" codeListValue=\"publication\"/>
              </gmd:dateType>
            </gmd:CI_Date>
          </gmd:date>


        </gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:abstract>
      <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty>
          <gmd:individualName gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:individualName>
          <gmd:organisationName gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:organisationName>
          <gmd:contactInfo>
            <gmd:CI_Contact>
              <gmd:phone>
                <gmd:CI_Telephone>
                  <gmd:voice gco:nilReason=\"missing\">
                    <gco:CharacterString/>
                  </gmd:voice>
                </gmd:CI_Telephone>
              </gmd:phone>
              <gmd:address>
                <gmd:CI_Address>
                  <gmd:electronicMailAddress gco:nilReason=\"missing\">
                    <gco:CharacterString/>
                  </gmd:electronicMailAddress>
                </gmd:CI_Address>
              </gmd:address>
            </gmd:CI_Contact>
          </gmd:contactInfo>
          <gmd:role>
            <gmd:CI_RoleCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode\" codeListValue=\"distributor\"/>
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:keyword>
          <gmd:keyword gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"theme\"/>
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords xlink:href=\"https://catalogue.prodige.internal/geonetwork/srv/fr/xml.keyword.get?thesaurus=external.place.regions&amp;id=http://geonetwork-opensource.org/regions%2368&amp;multiple=false\" xlink:title=\"\" xlink:role=\"pointOfContact\">
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>France</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"place\"/>
          </gmd:type>
          <gmd:thesaurusName>
            <gmd:CI_Citation>
              <gmd:title>
                <gco:CharacterString>external.place.regions</gco:CharacterString>
              </gmd:title>
              <gmd:date gco:nilReason=\"unknown\"/>
            </gmd:CI_Citation>
          </gmd:thesaurusName>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints>
          <gmd:useLimitation gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:useLimitation>
          <gmd:accessConstraints>
            <gmd:MD_RestrictionCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"restricted\"/>
          </gmd:accessConstraints>
          <gmd:useConstraints>
            <gmd:MD_RestrictionCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"copyright\"/>
          </gmd:useConstraints>
          <gmd:otherConstraints gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:otherConstraints>
        </gmd:MD_LegalConstraints>
      </gmd:resourceConstraints>
      <gmd:resourceConstraints>
        <gmd:MD_SecurityConstraints>
          <gmd:classification>
            <gmd:MD_ClassificationCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_ClassificationCode\" codeListValue=\"\"/>
          </gmd:classification>
          <gmd:userNote gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:userNote>
        </gmd:MD_SecurityConstraints>
      </gmd:resourceConstraints>
      <gmd:spatialRepresentationType>
        <gmd:MD_SpatialRepresentationTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_SpatialRepresentationTypeCode\" codeListValue=\"vector\"/>
      </gmd:spatialRepresentationType>
      <gmd:spatialResolution>
        <gmd:MD_Resolution>
          <gmd:equivalentScale>
            <gmd:MD_RepresentativeFraction>
              <gmd:denominator>
                <gco:Integer/>
              </gmd:denominator>
            </gmd:MD_RepresentativeFraction>
          </gmd:equivalentScale>
        </gmd:MD_Resolution>
      </gmd:spatialResolution>
      <gmd:spatialResolution>
        <gmd:MD_Resolution>
          <gmd:distance/>
        </gmd:MD_Resolution>
      </gmd:spatialResolution>
      <gmd:language>
        <gco:CharacterString>fre</gco:CharacterString>
      </gmd:language>
      <gmd:characterSet>
        <gmd:MD_CharacterSetCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\"/>
      </gmd:characterSet>
      <gmd:topicCategory>
        <gmd:MD_TopicCategoryCode/>
      </gmd:topicCategory>
      <gmd:extent>
        <gmd:EX_Extent>
          <gmd:description gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:description>
          <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
              <gmd:westBoundLongitude>
                <gco:Decimal>-4.31</gco:Decimal>
              </gmd:westBoundLongitude>
              <gmd:eastBoundLongitude>
                <gco:Decimal>8.40</gco:Decimal>
              </gmd:eastBoundLongitude>
              <gmd:southBoundLatitude>
                <gco:Decimal>41.24</gco:Decimal>
              </gmd:southBoundLatitude>
              <gmd:northBoundLatitude>
                <gco:Decimal>51</gco:Decimal>
              </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
          </gmd:geographicElement>

        </gmd:EX_Extent>
      </gmd:extent><gmd:supplementalInformation gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:supplementalInformation>


    </gmd:MD_DataIdentification>
  </gmd:identificationInfo>
  <gmd:dataQualityInfo>
    <gmd:DQ_DataQuality>
      <gmd:scope>
        <gmd:DQ_Scope>
          <gmd:level>
            <gmd:MD_ScopeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode\" codeListValue=\"dataset\"/>
          </gmd:level>
        </gmd:DQ_Scope>
      </gmd:scope>
      <gmd:lineage>
        <gmd:LI_Lineage>
          <gmd:statement gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:statement>
          <gmd:source>
            <gmd:LI_Source>
              <gmd:description gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:description>
            </gmd:LI_Source>
          </gmd:source>
        </gmd:LI_Lineage>
      </gmd:lineage>
    </gmd:DQ_DataQuality>
  </gmd:dataQualityInfo>

</gmd:MD_Metadata>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, NULL, NULL, 1, 1, NULL, 0, 1, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (11, 'c4e06459-add2-47e3-86a7-14addeb70e92', 'iso19110', 'y', 'n', '2010-03-04T15:46:22', '2010-03-04T15:46:22', '<gfc:FC_FeatureCatalogue xmlns:gfc=\"http://www.isotc211.org/2005/gfc\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xsi:schemaLocation=\"http://www.isotc211.org/2005/gfc ../../../../web/geonetwork/xml/schemas/iso19110/schema.xsd\">
  <gfc:name>
    <gco:CharacterString>Template de catalogue d''attributs</gco:CharacterString>
  </gfc:name>
  <gfc:scope>
    <gco:CharacterString>http://url.of.your.document</gco:CharacterString>
  </gfc:scope>
  <gfc:fieldOfApplication>
    <gco:CharacterString>Dictionnaire de données pour ...</gco:CharacterString>
  </gfc:fieldOfApplication>
  <gfc:versionNumber>
    <gco:CharacterString/>
  </gfc:versionNumber>
  <gfc:versionDate>
    <gco:Date>2008-04-17</gco:Date>
  </gfc:versionDate>
  <gfc:producer>
    <gmd:CI_ResponsibleParty>
      <gmd:individualName>
        <gco:CharacterString/>
      </gmd:individualName>
      <gmd:organisationName>
        <gco:CharacterString/>
      </gmd:organisationName>
      <gmd:positionName>
        <gco:CharacterString/>
      </gmd:positionName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone>
              <gmd:voice>
                <gco:CharacterString/>
              </gmd:voice>
              <gmd:facsimile>
                <gco:CharacterString/>
              </gmd:facsimile>
            </gmd:CI_Telephone>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:deliveryPoint>
                <gco:CharacterString/>
              </gmd:deliveryPoint>
              <gmd:city>
                <gco:CharacterString/>
              </gmd:city>
              <gmd:administrativeArea>
                <gco:CharacterString/>
              </gmd:administrativeArea>
              <gmd:postalCode>
                <gco:CharacterString/>
              </gmd:postalCode>
              <gmd:country>
                <gco:CharacterString/>
              </gmd:country>
              <gmd:electronicMailAddress>
                <gco:CharacterString/>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeListValue=\"principalInvestigator\" codeList=\"CI_RoleCode\"/>
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gfc:producer>
  <gfc:featureType>
    <gfc:FC_FeatureType>
      <!--id=\"Table attributaire associée\"-->
      <gfc:typeName>
        <gco:LocalName>Nom de la propriété</gco:LocalName>
      </gfc:typeName>
      <gfc:definition>
        <gco:CharacterString>Definition de la table d''attributs</gco:CharacterString>
      </gfc:definition>
      <gfc:isAbstract>
        <gco:Boolean>false</gco:Boolean>
      </gfc:isAbstract>
      <gfc:aliases>
        <gco:LocalName/>
      </gfc:aliases>
      <gfc:featureCatalogue/>
      <gfc:carrierOfCharacteristics>
        <gfc:FC_FeatureAttribute>
          <!--id=\"Table attributaire associée.AREAé\"-->
          <gfc:memberName>
            <gco:LocalName>superficie en m²</gco:LocalName>
          </gfc:memberName>
          <gfc:definition>
            <gco:CharacterString>superficie en m²</gco:CharacterString>
          </gfc:definition>
          <gfc:cardinality>
            <gco:Multiplicity>
              <gco:range>
                <gco:MultiplicityRange>
                  <gco:lower>
                    <gco:Integer>0</gco:Integer>
                  </gco:lower>
                  <gco:upper>
                    <gco:UnlimitedInteger isInfinite=\"false\">1</gco:UnlimitedInteger>
                  </gco:upper>
                </gco:MultiplicityRange>
              </gco:range>
            </gco:Multiplicity>
          </gfc:cardinality>
          <gfc:featureType/>
          <gfc:valueMeasurementUnit>
            <gml:UnitDefinition xmlns:gml=\"http://www.opengis.net/gml\" gml:id=\"unknown\">
              <gml:description/>
              <gml:identifier codeSpace=\"unknown\"/>
            </gml:UnitDefinition>
          </gfc:valueMeasurementUnit>
          <gfc:listedValue>
            <gfc:FC_ListedValue>
              <gfc:label>
                <gco:CharacterString/>
              </gfc:label>
            </gfc:FC_ListedValue>
          </gfc:listedValue>
          <gfc:valueType>
            <gco:TypeName>
              <gco:aName>
                <gco:CharacterString>Numérique</gco:CharacterString>
              </gco:aName>
            </gco:TypeName>
          </gfc:valueType>
        </gfc:FC_FeatureAttribute>
      </gfc:carrierOfCharacteristics>
      <gfc:carrierOfCharacteristics>
        <gfc:FC_FeatureAttribute>
          <!--id=\"Table attributaire associée.PERIMETER\"-->
          <gfc:memberName>
            <gco:LocalName>périmètre en mètres</gco:LocalName>
          </gfc:memberName>
          <gfc:definition>
            <gco:CharacterString>définition périmètre</gco:CharacterString>
          </gfc:definition>
          <gfc:cardinality>
            <gco:Multiplicity>
              <gco:range>
                <gco:MultiplicityRange>
                  <gco:lower>
                    <gco:Integer>0</gco:Integer>
                  </gco:lower>
                  <gco:upper>
                    <gco:UnlimitedInteger isInfinite=\"true\" xsi:nil=\"true\"/>
                  </gco:upper>
                </gco:MultiplicityRange>
              </gco:range>
            </gco:Multiplicity>
          </gfc:cardinality>
          <gfc:featureType/>
          <gfc:valueMeasurementUnit>
            <gml:UnitDefinition xmlns:gml=\"http://www.opengis.net/gml\" gml:id=\"null\">
              <gml:identifier codeSpace=\"unknown\"/>
            </gml:UnitDefinition>
          </gfc:valueMeasurementUnit>
          <gfc:valueType>
            <gco:TypeName>
              <gco:aName>
                <gco:CharacterString>Numérique</gco:CharacterString>
              </gco:aName>
            </gco:TypeName>
          </gfc:valueType>
        </gfc:FC_FeatureAttribute>
      </gfc:carrierOfCharacteristics>
      <gfc:carrierOfCharacteristics>
        <gfc:FC_FeatureAttribute>
          <!--id=\"Table attributaire associée.CODE_00\"-->
          <gfc:memberName>
            <gco:LocalName>code selon nomenclature CLC niveau 3</gco:LocalName>
          </gfc:memberName>
          <gfc:definition>
            <gco:CharacterString>code selon nomenclature CLC niveau 3</gco:CharacterString>
          </gfc:definition>
          <gfc:cardinality>
            <gco:Multiplicity>
              <gco:range>
                <gco:MultiplicityRange>
                  <gco:lower>
                    <gco:Integer>0</gco:Integer>
                  </gco:lower>
                  <gco:upper>
                    <gco:UnlimitedInteger isInfinite=\"true\" xsi:nil=\"true\"/>
                  </gco:upper>
                </gco:MultiplicityRange>
              </gco:range>
            </gco:Multiplicity>
          </gfc:cardinality>
          <gfc:featureType/>
          <gfc:valueMeasurementUnit>
            <gml:UnitDefinition xmlns:gml=\"http://www.opengis.net/gml\" gml:id=\"unknown_1\">
              <gml:identifier codeSpace=\"unknown\"/>
            </gml:UnitDefinition>
          </gfc:valueMeasurementUnit>
          <gfc:listedValue>
            <gfc:FC_ListedValue>
              <gfc:label>
                <gco:CharacterString>1.1.1.</gco:CharacterString>
              </gfc:label>
              <gfc:code>
                <gco:CharacterString/>
              </gfc:code>
              <gfc:definition>
                <gco:CharacterString/>
              </gfc:definition>
            </gfc:FC_ListedValue>
          </gfc:listedValue>
          <gfc:listedValue>
            <gfc:FC_ListedValue>
              <gfc:label>
                <gco:CharacterString>1.1.2.</gco:CharacterString>
              </gfc:label>
              <gfc:code>
                <gco:CharacterString/>
              </gfc:code>
              <gfc:definition>
                <gco:CharacterString/>
              </gfc:definition>
            </gfc:FC_ListedValue>
          </gfc:listedValue>
          <gfc:valueType>
            <gco:TypeName>
              <gco:aName>
                <gco:CharacterString>Chaine de caractères</gco:CharacterString>
              </gco:aName>
            </gco:TypeName>
          </gfc:valueType>
        </gfc:FC_FeatureAttribute>
      </gfc:carrierOfCharacteristics>
    </gfc:FC_FeatureType>
  </gfc:featureType>
</gfc:FC_FeatureCatalogue>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, 'gfc:FC_FeatureCatalogue', NULL, 1, 1, NULL, 0, 0, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (14, '274971da-ef09-4e3b-9a5e-b236fd670874', 'iso19110', 'n', 'n', '2013-06-26T17:40:19', '2013-06-26T17:40:19', '<gfc:FC_FeatureCatalogue xmlns:gfc=\"http://www.isotc211.org/2005/gfc\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" uuid=\"274971da-ef09-4e3b-9a5e-b236fd670874\" xsi:schemaLocation=\"http://www.isotc211.org/2005/gfc ../../../../web/geonetwork/xml/schemas/iso19110/schema.xsd\">
  <gfc:name>
    <gco:CharacterString>Template de catalogue d''attributs</gco:CharacterString>
  </gfc:name>
  <gfc:scope>
    <gco:CharacterString>http://url.of.your.document</gco:CharacterString>
  </gfc:scope>
  <gfc:fieldOfApplication>
    <gco:CharacterString>Dictionnaire de données pour ...</gco:CharacterString>
  </gfc:fieldOfApplication>
  <gfc:versionNumber>
    <gco:CharacterString/>
  </gfc:versionNumber>
  <gfc:versionDate>
    <gco:DateTime xmlns:gmx=\"http://www.isotc211.org/2005/gmx\">2013-06-26T17:40:19</gco:DateTime>
  </gfc:versionDate>
  <gfc:producer>
    <gmd:CI_ResponsibleParty>
      <gmd:individualName>
        <gco:CharacterString/>
      </gmd:individualName>
      <gmd:organisationName>
        <gco:CharacterString/>
      </gmd:organisationName>
      <gmd:positionName>
        <gco:CharacterString/>
      </gmd:positionName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone>
              <gmd:voice>
                <gco:CharacterString/>
              </gmd:voice>
              <gmd:facsimile>
                <gco:CharacterString/>
              </gmd:facsimile>
            </gmd:CI_Telephone>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:deliveryPoint>
                <gco:CharacterString/>
              </gmd:deliveryPoint>
              <gmd:city>
                <gco:CharacterString/>
              </gmd:city>
              <gmd:administrativeArea>
                <gco:CharacterString/>
              </gmd:administrativeArea>
              <gmd:postalCode>
                <gco:CharacterString/>
              </gmd:postalCode>
              <gmd:country>
                <gco:CharacterString/>
              </gmd:country>
              <gmd:electronicMailAddress>
                <gco:CharacterString/>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeListValue=\"principalInvestigator\" codeList=\"CI_RoleCode\"/>
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gfc:producer>
  <gfc:featureType>
    <gfc:FC_FeatureType>
      <!--id=\"Table attributaire associée\"-->
      <gfc:typeName>
        <gco:LocalName>Table</gco:LocalName>
      </gfc:typeName>
      <gfc:definition>
        <gco:CharacterString>Definition de la table d''attributs</gco:CharacterString>
      </gfc:definition>
      <gfc:isAbstract>
        <gco:Boolean>false</gco:Boolean>
      </gfc:isAbstract>
      <gfc:aliases>
        <gco:LocalName/>
      </gfc:aliases>
      <gfc:featureCatalogue/>



    <gfc:carrierOfCharacteristics><gfc:FC_FeatureAttribute><gfc:memberName><gco:LocalName xmlns:gco=\"http://www.isotc211.org/2005/gco\">id_geofla</gco:LocalName></gfc:memberName><gfc:definition><gco:CharacterString xmlns:gco=\"http://www.isotc211.org/2005/gco\"/></gfc:definition><gfc:cardinality><gco:Multiplicity xmlns:gco=\"http://www.isotc211.org/2005/gco\"><gco:range><gco:MultiplicityRange><gco:lower><gco:Integer>0</gco:Integer></gco:lower><gco:upper><gco:UnlimitedInteger isInfinite=\"true\" xsi:nil=\"true\"/></gco:upper></gco:MultiplicityRange></gco:range></gco:Multiplicity></gfc:cardinality><gfc:featureType/><gfc:valueMeasurementUnit><gml:UnitDefinition xmlns:gml=\"http://www.opengis.net/gml\" gml:id=\"null\"><gml:identifier codeSpace=\"unknown\"/></gml:UnitDefinition></gfc:valueMeasurementUnit><gfc:valueType><gco:TypeName xmlns:gco=\"http://www.isotc211.org/2005/gco\"><gco:aName><gco:CharacterString>Numérique</gco:CharacterString></gco:aName></gco:TypeName></gfc:valueType></gfc:FC_FeatureAttribute></gfc:carrierOfCharacteristics><gfc:carrierOfCharacteristics><gfc:FC_FeatureAttribute><gfc:memberName><gco:LocalName xmlns:gco=\"http://www.isotc211.org/2005/gco\">nature</gco:LocalName></gfc:memberName><gfc:definition><gco:CharacterString xmlns:gco=\"http://www.isotc211.org/2005/gco\"/></gfc:definition><gfc:cardinality><gco:Multiplicity xmlns:gco=\"http://www.isotc211.org/2005/gco\"><gco:range><gco:MultiplicityRange><gco:lower><gco:Integer>0</gco:Integer></gco:lower><gco:upper><gco:UnlimitedInteger isInfinite=\"true\" xsi:nil=\"true\"/></gco:upper></gco:MultiplicityRange></gco:range></gco:Multiplicity></gfc:cardinality><gfc:featureType/><gfc:valueMeasurementUnit><gml:UnitDefinition xmlns:gml=\"http://www.opengis.net/gml\" gml:id=\"null\"><gml:identifier codeSpace=\"unknown\"/></gml:UnitDefinition></gfc:valueMeasurementUnit><gfc:valueType><gco:TypeName xmlns:gco=\"http://www.isotc211.org/2005/gco\"><gco:aName><gco:CharacterString>Chaine de caractères</gco:CharacterString></gco:aName></gco:TypeName></gfc:valueType></gfc:FC_FeatureAttribute></gfc:carrierOfCharacteristics></gfc:FC_FeatureType>
  </gfc:featureType>
</gfc:FC_FeatureCatalogue>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, 'gfc:FC_FeatureCatalogue', NULL, 1, 1, NULL, 0, 1, NULL, '', NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (40089, '88cacc48-04c5-42a6-b5ed-d1d375061544', 'iso19139', 'y', 'n', '2010-03-04T15:46:22', '2010-03-04T15:46:22', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:fra=\"http://www.cnig.gouv.fr/2005/fra\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xsi:schemaLocation=\"http://www.isotc211.org/2005/fra ../../../../web/geonetwork/xml/schemas/iso19139fra/schema.xsd\">

  <gmd:fileIdentifier>

    <gco:CharacterString>88cacc48-04c5-42a6-b5ed-d1d375061544</gco:CharacterString>

  </gmd:fileIdentifier>

  <gmd:language>



  <gco:CharacterString>fre</gco:CharacterString></gmd:language>

  <gmd:characterSet>

    <gmd:MD_CharacterSetCode codeList=\"./resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\"/>

  </gmd:characterSet>

  <gmd:hierarchyLevel>

    <gmd:MD_ScopeCode codeList=\"./resources/codeList.xml#MD_ScopeCode\" codeListValue=\"dataset\"/>

  </gmd:hierarchyLevel><gmd:contact>

    <gmd:CI_ResponsibleParty>

      <gmd:individualName>

        <gco:CharacterString/>

      </gmd:individualName>

      <gmd:organisationName>

        <gco:CharacterString/>

      </gmd:organisationName>

      <gmd:positionName>

        <gco:CharacterString/>

      </gmd:positionName>

      <gmd:contactInfo>

        <gmd:CI_Contact>

          <gmd:phone>

            <gmd:CI_Telephone>

              <gmd:voice>

                <gco:CharacterString/>

              </gmd:voice>

              <gmd:facsimile>

                <gco:CharacterString/>

              </gmd:facsimile>

            </gmd:CI_Telephone>

          </gmd:phone>

          <gmd:address>

            <gmd:CI_Address>

              <gmd:deliveryPoint>

                <gco:CharacterString/>

              </gmd:deliveryPoint>

              <gmd:city>

                <gco:CharacterString/>

              </gmd:city>

              <gmd:administrativeArea>

                <gco:CharacterString/>

              </gmd:administrativeArea>

              <gmd:postalCode>

                <gco:CharacterString/>

              </gmd:postalCode>

              <gmd:country>

                <gco:CharacterString/>

              </gmd:country>

              <gmd:electronicMailAddress>

                <gco:CharacterString/>

              </gmd:electronicMailAddress>

            </gmd:CI_Address>

          </gmd:address>

        </gmd:CI_Contact>

      </gmd:contactInfo>

      <gmd:role>

        <gmd:CI_RoleCode codeList=\"./resources/codeList.xml#CI_RoleCode\" codeListValue=\"originator\"/>

      </gmd:role>

    </gmd:CI_ResponsibleParty>

  </gmd:contact>

  <gmd:dateStamp>

    <gco:DateTime>2006-07-27T09:02:02</gco:DateTime>

  </gmd:dateStamp>

  <gmd:metadataStandardName>

    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>

  </gmd:metadataStandardName>

  <gmd:metadataStandardVersion>

    <gco:CharacterString>1.0</gco:CharacterString>

  </gmd:metadataStandardVersion>

  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>

    <gmd:MD_DataIdentification>

      <gmd:citation>

        <gmd:CI_Citation>

          <gmd:title>

            <gco:CharacterString>métadonnées de données MAJIC</gco:CharacterString>

          </gmd:title><gmd:date>

            <gmd:CI_Date>

              <gmd:date>

                <gco:DateTime/>

              </gmd:date>

              <gmd:dateType>

                <gmd:CI_DateTypeCode codeList=\"./resources/codeList.xml#CI_DateTypeCode\" codeListValue=\"publication\"/>

              </gmd:dateType>

            </gmd:CI_Date>

          </gmd:date>





        </gmd:CI_Citation>

      </gmd:citation>

      <gmd:abstract>

        <gco:CharacterString/>

      </gmd:abstract>

      <gmd:pointOfContact>

        <gmd:CI_ResponsibleParty>

          <gmd:individualName>

            <gco:CharacterString/>

          </gmd:individualName>

          <gmd:organisationName>

            <gco:CharacterString/>

          </gmd:organisationName>

          <gmd:contactInfo>

            <gmd:CI_Contact>

              <gmd:phone>

                <gmd:CI_Telephone>

                  <gmd:voice>

                    <gco:CharacterString/>

                  </gmd:voice>

                </gmd:CI_Telephone>

              </gmd:phone>

              <gmd:address>

                <gmd:CI_Address>

                  <gmd:electronicMailAddress>

                    <gco:CharacterString/>

                  </gmd:electronicMailAddress>

                </gmd:CI_Address>

              </gmd:address>

            </gmd:CI_Contact>

          </gmd:contactInfo>

          <gmd:role>

            <gmd:CI_RoleCode codeList=\"./resources/codeList.xml#CI_RoleCode\" codeListValue=\"distributor\"/>

          </gmd:role>

        </gmd:CI_ResponsibleParty>

      </gmd:pointOfContact>

      <gmd:descriptiveKeywords>

        <gmd:MD_Keywords>

          <gmd:keyword>

            <gco:CharacterString/>

          </gmd:keyword>

          <gmd:keyword>

            <gco:CharacterString/>

          </gmd:keyword>

          <gmd:type>

            <gmd:MD_KeywordTypeCode codeList=\"./resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"theme\"/>

          </gmd:type>

        </gmd:MD_Keywords>

      </gmd:descriptiveKeywords>

      <gmd:descriptiveKeywords>

        <gmd:MD_Keywords>

          <gmd:keyword gco:nilReason=\"missing\">

            <gco:CharacterString/>

          </gmd:keyword>

          <gmd:type>

            <gmd:MD_KeywordTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"place\"/>

          </gmd:type>

        </gmd:MD_Keywords>

      </gmd:descriptiveKeywords>

      <gmd:resourceConstraints>

        <gmd:MD_LegalConstraints>

          <gmd:useLimitation>

            <gco:CharacterString/>

          </gmd:useLimitation>

          <gmd:accessConstraints>

            <gmd:MD_RestrictionCode codeList=\"./resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"restricted\"/>

          </gmd:accessConstraints>

          <gmd:useConstraints>

            <gmd:MD_RestrictionCode codeList=\"./resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"copyright\"/>

          </gmd:useConstraints>

          <gmd:otherConstraints>

            <gco:CharacterString/>

          </gmd:otherConstraints>

        </gmd:MD_LegalConstraints>

      </gmd:resourceConstraints>

      <gmd:resourceConstraints>

        <gmd:MD_SecurityConstraints>

          <gmd:classification>

            <gmd:MD_ClassificationCode codeList=\"./resources/codeList.xml#MD_ClassificationCode\" codeListValue=\"\"/>

          </gmd:classification>

          <gmd:userNote>

            <gco:CharacterString/>

          </gmd:userNote>

        </gmd:MD_SecurityConstraints>

      </gmd:resourceConstraints>

      <gmd:spatialRepresentationType>

        <gmd:MD_SpatialRepresentationTypeCode codeList=\"./resources/codeList.xml#MD_SpatialRepresentationTypeCode\" codeListValue=\"textTable\"/>

      </gmd:spatialRepresentationType>

      <gmd:spatialResolution>

        <gmd:MD_Resolution>

          <gmd:equivalentScale>

            <gmd:MD_RepresentativeFraction>

              <gmd:denominator>

                <gco:Integer/>

              </gmd:denominator>

            </gmd:MD_RepresentativeFraction>

          </gmd:equivalentScale>

        </gmd:MD_Resolution>

      </gmd:spatialResolution>

      <gmd:spatialResolution>

        <gmd:MD_Resolution>

          <gmd:distance/>

        </gmd:MD_Resolution>

      </gmd:spatialResolution>

      <gmd:language>

        <gco:CharacterString>fre</gco:CharacterString>

      </gmd:language>

      <gmd:characterSet>

        <gmd:MD_CharacterSetCode codeList=\"./resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\"/>

      </gmd:characterSet>

      <gmd:topicCategory>

        <gmd:MD_TopicCategoryCode/>

      </gmd:topicCategory>





      <gmd:extent>

        <gmd:EX_Extent>

          <gmd:description gco:nilReason=\"missing\">

            <gco:CharacterString/>

          </gmd:description>

          <gmd:geographicElement>

            <gmd:EX_GeographicBoundingBox>

              <gmd:westBoundLongitude>

                <gco:Decimal>0</gco:Decimal>

              </gmd:westBoundLongitude>

              <gmd:eastBoundLongitude>

                <gco:Decimal>0</gco:Decimal>

              </gmd:eastBoundLongitude>

              <gmd:southBoundLatitude>

                <gco:Decimal>0</gco:Decimal>

              </gmd:southBoundLatitude>

              <gmd:northBoundLatitude>

                <gco:Decimal>0</gco:Decimal>

              </gmd:northBoundLatitude>

            </gmd:EX_GeographicBoundingBox>

          </gmd:geographicElement>



        </gmd:EX_Extent>

      </gmd:extent><gmd:supplementalInformation>

        <gco:CharacterString/>

      </gmd:supplementalInformation>



    </gmd:MD_DataIdentification>

  </gmd:identificationInfo>

  <gmd:dataQualityInfo>

    <gmd:DQ_DataQuality>

      <gmd:scope>

        <gmd:DQ_Scope>

          <gmd:level>

            <gmd:MD_ScopeCode codeList=\"./resources/codeList.xml#MD_ScopeCode\" codeListValue=\"dataset\"/>

          </gmd:level>

        </gmd:DQ_Scope>

      </gmd:scope>

      <gmd:lineage>

        <gmd:LI_Lineage>

          <gmd:statement>

            <gco:CharacterString/>

          </gmd:statement>

          <gmd:source>

            <gmd:LI_Source>

              <gmd:description>

                <gco:CharacterString/>

              </gmd:description>

            </gmd:LI_Source>

          </gmd:source>

        </gmd:LI_Lineage>

      </gmd:lineage>

    </gmd:DQ_DataQuality>

  </gmd:dataQualityInfo>



</gmd:MD_Metadata>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, 'gmd:MD_Metadata', NULL, 1, 1, NULL, 0, 0, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (3, '3f52a630-f3cc-11da-916a-000c2929140b', 'iso19139', 'y', 'n', '2006-06-04T15:15:52', '2006-06-04T15:15:58', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:gfc=\"http://www.isotc211.org/2005/gfc\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:srv=\"http://www.isotc211.org/2005/srv\">
  <gmd:fileIdentifier>
    <gco:CharacterString>3f52a630-f3cc-11da-916a-000c2929140b</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>

  <gco:CharacterString>fre</gco:CharacterString></gmd:language>
  <gmd:characterSet>
    <gmd:MD_CharacterSetCode codeList=\"./resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\"/>
  </gmd:characterSet>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode\" codeListValue=\"service\"/>
  </gmd:hierarchyLevel><gmd:hierarchyLevelName><gco:CharacterString>service</gco:CharacterString></gmd:hierarchyLevelName><gmd:contact>
    <gmd:CI_ResponsibleParty>
      <gmd:individualName gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:individualName>
      <gmd:organisationName gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:organisationName>
      <gmd:positionName gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:positionName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone>
              <gmd:voice gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:voice>
              <gmd:facsimile gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:facsimile>
            </gmd:CI_Telephone>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:deliveryPoint gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:deliveryPoint>
              <gmd:city gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:city>
              <gmd:administrativeArea gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:administrativeArea>
              <gmd:postalCode gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:postalCode>
              <gmd:country gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:country>
              <gmd:electronicMailAddress gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode\" codeListValue=\"pointOfContact\"/>
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gmd:contact>
  <gmd:dateStamp>
    <gco:DateTime>2010-06-18T15:59:32</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:metadataStandardVersion>
    <gco:CharacterString>1.0</gco:CharacterString>
  </gmd:metadataStandardVersion>
  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>
    <srv:SV_ServiceIdentification>
      <gmd:citation xmlns:gmd=\"http://www.isotc211.org/2005/gmd\">
        <gmd:CI_Citation>
          <gmd:title gco:nilReason=\"missing\">
            <gco:CharacterString>métadonnées de cartes</gco:CharacterString>
          </gmd:title><gmd:date>
            <gmd:CI_Date>
              <gmd:date>
                <gco:DateTime/>
              </gmd:date>
              <gmd:dateType>
                <gmd:CI_DateTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode\" codeListValue=\"publication\"/>
              </gmd:dateType>
            </gmd:CI_Date>
          </gmd:date>


        </gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:abstract>
      <gmd:pointOfContact xmlns:gmd=\"http://www.isotc211.org/2005/gmd\">
        <gmd:CI_ResponsibleParty>
          <gmd:individualName gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:individualName>
          <gmd:organisationName gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:organisationName>
          <gmd:contactInfo>
            <gmd:CI_Contact>
              <gmd:phone>
                <gmd:CI_Telephone>
                  <gmd:voice gco:nilReason=\"missing\">
                    <gco:CharacterString/>
                  </gmd:voice>
                </gmd:CI_Telephone>
              </gmd:phone>
              <gmd:address>
                <gmd:CI_Address>
                  <gmd:electronicMailAddress gco:nilReason=\"missing\">
                    <gco:CharacterString/>
                  </gmd:electronicMailAddress>
                </gmd:CI_Address>
              </gmd:address>
            </gmd:CI_Contact>
          </gmd:contactInfo>
          <gmd:role>
            <gmd:CI_RoleCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode\" codeListValue=\"pointOfContact\"/>
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>
      <gmd:descriptiveKeywords xmlns:gmd=\"http://www.isotc211.org/2005/gmd\">
        <gmd:MD_Keywords>
          <gmd:keyword gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:keyword>
          <gmd:keyword gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"theme\"/>
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"https://catalogue.prodige.internal/geonetwork/srv/fr/xml.keyword.get?thesaurus=external.place.regions&amp;id=http://geonetwork-opensource.org/regions%2368&amp;multiple=false\" xlink:title=\"\" xlink:role=\"pointOfContact\">
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>France</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"place\"/>
          </gmd:type>
          <gmd:thesaurusName>
            <gmd:CI_Citation>
              <gmd:title>
                <gco:CharacterString>external.place.regions</gco:CharacterString>
              </gmd:title>
              <gmd:date gco:nilReason=\"unknown\"/>
            </gmd:CI_Citation>
          </gmd:thesaurusName>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>infoMapAccessService</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode\" codeListValue=\"theme\"/>
          </gmd:type>
          <gmd:thesaurusName>
            <gmd:CI_Citation>
              <gmd:title>
                <gco:CharacterString>INSPIRE Service taxonomy</gco:CharacterString>
              </gmd:title>
              <gmd:date>
                <gmd:CI_Date>
                  <gmd:date>
                    <gco:Date>2012-04-11</gco:Date>
                  </gmd:date>
                  <gmd:dateType>
                    <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_DateTypeCode\" codeListValue=\"publication\"/>
                  </gmd:dateType>
                </gmd:CI_Date>
              </gmd:date>
            </gmd:CI_Citation>
          </gmd:thesaurusName>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords><gmd:resourceConstraints xmlns:gmd=\"http://www.isotc211.org/2005/gmd\">
        <gmd:MD_LegalConstraints>
          <gmd:accessConstraints>
            <gmd:MD_RestrictionCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"\"/>
          </gmd:accessConstraints>
          <gmd:useConstraints>
            <gmd:MD_RestrictionCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"\"/>
          </gmd:useConstraints>
          <gmd:otherConstraints gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:otherConstraints>
        </gmd:MD_LegalConstraints>
      </gmd:resourceConstraints>
      <gmd:resourceConstraints xmlns:gmd=\"http://www.isotc211.org/2005/gmd\">
        <gmd:MD_SecurityConstraints>
          <gmd:classification>
            <gmd:MD_ClassificationCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_ClassificationCode\" codeListValue=\"\"/>
          </gmd:classification>
          <gmd:userNote gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:userNote>
        </gmd:MD_SecurityConstraints>
      </gmd:resourceConstraints>






      <srv:serviceType><gco:LocalName>invoke</gco:LocalName></srv:serviceType><srv:extent>
        <gmd:EX_Extent xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\">
          <gmd:description gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:description>
          <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
              <gmd:westBoundLongitude>
                <gco:Decimal>-5.79028</gco:Decimal>
              </gmd:westBoundLongitude>
              <gmd:eastBoundLongitude>
                <gco:Decimal>9.56222</gco:Decimal>
              </gmd:eastBoundLongitude>
              <gmd:southBoundLatitude>
                <gco:Decimal>41.36493</gco:Decimal>
              </gmd:southBoundLatitude>
              <gmd:northBoundLatitude>
                <gco:Decimal>51.09111</gco:Decimal>
              </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
          </gmd:geographicElement>

        </gmd:EX_Extent>
      </srv:extent>



               <srv:couplingType>
				         <srv:SV_CouplingType codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType\" codeListValue=\"tight\"/>
				       </srv:couplingType>
				       <srv:containsOperations>
				         <srv:SV_OperationMetadata>
				           <srv:operationName>
				             <gco:CharacterString>Accès à la carte</gco:CharacterString>
				           </srv:operationName>
				           <srv:DCP>
				             <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
				           </srv:DCP>
				           <srv:connectPoint>
				             <gmd:CI_OnlineResource>
				               <gmd:linkage>
				                 <gmd:URL>https://catalogue.prodige.internal/geosource/consultation?id=3</gmd:URL>
				               </gmd:linkage>
				               <gmd:protocol>
				                 <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
				               </gmd:protocol>
				             </gmd:CI_OnlineResource>
				           </srv:connectPoint>
				         </srv:SV_OperationMetadata>
				       </srv:containsOperations>
             </srv:SV_ServiceIdentification>
  </gmd:identificationInfo>

  <gmd:dataQualityInfo>
    <gmd:DQ_DataQuality>
      <gmd:scope>
        <gmd:DQ_Scope>
          <gmd:level>
            <gmd:MD_ScopeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode\" codeListValue=\"service\"/>
          </gmd:level>
        </gmd:DQ_Scope>
      </gmd:scope>
      <gmd:lineage>
        <gmd:LI_Lineage>
          <gmd:statement gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:statement>

        </gmd:LI_Lineage>
      </gmd:lineage>
    </gmd:DQ_DataQuality>
  </gmd:dataQualityInfo>

</gmd:MD_Metadata>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, NULL, NULL, 1, 1, NULL, 0, 0, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (40088, '88cacc48-04c5-42a6-b5ed-d1d375214856', 'iso19139', 'y', 'n', '2010-03-04T15:46:22', '2010-03-04T15:46:22', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:fra=\"http://www.cnig.gouv.fr/2005/fra\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xsi:schemaLocation=\"http://www.isotc211.org/2005/fra ../../../../web/geonetwork/xml/schemas/iso19139fra/schema.xsd\">

  <gmd:fileIdentifier>

    <gco:CharacterString>88cacc48-04c5-42a6-b5ed-d1d375214856</gco:CharacterString>

  </gmd:fileIdentifier>

  <gmd:language>



  <gco:CharacterString>fre</gco:CharacterString></gmd:language>

  <gmd:characterSet>

    <gmd:MD_CharacterSetCode codeList=\"./resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\"/>

  </gmd:characterSet>

  <gmd:hierarchyLevel>

    <gmd:MD_ScopeCode codeList=\"./resources/codeList.xml#MD_ScopeCode\" codeListValue=\"dataset\"/>

  </gmd:hierarchyLevel><gmd:contact>

    <gmd:CI_ResponsibleParty>

      <gmd:individualName>

        <gco:CharacterString/>

      </gmd:individualName>

      <gmd:organisationName>

        <gco:CharacterString/>

      </gmd:organisationName>

      <gmd:positionName>

        <gco:CharacterString/>

      </gmd:positionName>

      <gmd:contactInfo>

        <gmd:CI_Contact>

          <gmd:phone>

            <gmd:CI_Telephone>

              <gmd:voice>

                <gco:CharacterString/>

              </gmd:voice>

              <gmd:facsimile>

                <gco:CharacterString/>

              </gmd:facsimile>

            </gmd:CI_Telephone>

          </gmd:phone>

          <gmd:address>

            <gmd:CI_Address>

              <gmd:deliveryPoint>

                <gco:CharacterString/>

              </gmd:deliveryPoint>

              <gmd:city>

                <gco:CharacterString/>

              </gmd:city>

              <gmd:administrativeArea>

                <gco:CharacterString/>

              </gmd:administrativeArea>

              <gmd:postalCode>

                <gco:CharacterString/>

              </gmd:postalCode>

              <gmd:country>

                <gco:CharacterString/>

              </gmd:country>

              <gmd:electronicMailAddress>

                <gco:CharacterString/>

              </gmd:electronicMailAddress>

            </gmd:CI_Address>

          </gmd:address>

        </gmd:CI_Contact>

      </gmd:contactInfo>

      <gmd:role>

        <gmd:CI_RoleCode codeList=\"./resources/codeList.xml#CI_RoleCode\" codeListValue=\"originator\"/>

      </gmd:role>

    </gmd:CI_ResponsibleParty>

  </gmd:contact>

  <gmd:dateStamp>

    <gco:DateTime>2006-07-27T09:02:02</gco:DateTime>

  </gmd:dateStamp>

  <gmd:metadataStandardName>

    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>

  </gmd:metadataStandardName>

  <gmd:metadataStandardVersion>

    <gco:CharacterString>1.0</gco:CharacterString>

  </gmd:metadataStandardVersion>

  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>

    <gmd:MD_DataIdentification>

      <gmd:citation>

        <gmd:CI_Citation>

          <gmd:title>

            <gco:CharacterString>métadonnée de série de données tabulaire</gco:CharacterString>

          </gmd:title><gmd:date>

            <gmd:CI_Date>

              <gmd:date>

                <gco:DateTime/>

              </gmd:date>

              <gmd:dateType>

                <gmd:CI_DateTypeCode codeList=\"./resources/codeList.xml#CI_DateTypeCode\" codeListValue=\"publication\"/>

              </gmd:dateType>

            </gmd:CI_Date>

          </gmd:date>





        </gmd:CI_Citation>

      </gmd:citation>

      <gmd:abstract>

        <gco:CharacterString/>

      </gmd:abstract>

      <gmd:pointOfContact>

        <gmd:CI_ResponsibleParty>

          <gmd:individualName>

            <gco:CharacterString/>

          </gmd:individualName>

          <gmd:organisationName>

            <gco:CharacterString/>

          </gmd:organisationName>

          <gmd:contactInfo>

            <gmd:CI_Contact>

              <gmd:phone>

                <gmd:CI_Telephone>

                  <gmd:voice>

                    <gco:CharacterString/>

                  </gmd:voice>

                </gmd:CI_Telephone>

              </gmd:phone>

              <gmd:address>

                <gmd:CI_Address>

                  <gmd:electronicMailAddress>

                    <gco:CharacterString/>

                  </gmd:electronicMailAddress>

                </gmd:CI_Address>

              </gmd:address>

            </gmd:CI_Contact>

          </gmd:contactInfo>

          <gmd:role>

            <gmd:CI_RoleCode codeList=\"./resources/codeList.xml#CI_RoleCode\" codeListValue=\"distributor\"/>

          </gmd:role>

        </gmd:CI_ResponsibleParty>

      </gmd:pointOfContact>

      <gmd:descriptiveKeywords>

        <gmd:MD_Keywords>

          <gmd:keyword>

            <gco:CharacterString/>

          </gmd:keyword>

          <gmd:keyword>

            <gco:CharacterString/>

          </gmd:keyword>

          <gmd:type>

            <gmd:MD_KeywordTypeCode codeList=\"./resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"theme\"/>

          </gmd:type>

        </gmd:MD_Keywords>

      </gmd:descriptiveKeywords>

      <gmd:descriptiveKeywords>

        <gmd:MD_Keywords>

          <gmd:keyword gco:nilReason=\"missing\">

            <gco:CharacterString/>

          </gmd:keyword>

          <gmd:type>

            <gmd:MD_KeywordTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"place\"/>

          </gmd:type>

        </gmd:MD_Keywords>

      </gmd:descriptiveKeywords>

      <gmd:resourceConstraints>

        <gmd:MD_LegalConstraints>

          <gmd:useLimitation>

            <gco:CharacterString/>

          </gmd:useLimitation>

          <gmd:accessConstraints>

            <gmd:MD_RestrictionCode codeList=\"./resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"restricted\"/>

          </gmd:accessConstraints>

          <gmd:useConstraints>

            <gmd:MD_RestrictionCode codeList=\"./resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"copyright\"/>

          </gmd:useConstraints>

          <gmd:otherConstraints>

            <gco:CharacterString/>

          </gmd:otherConstraints>

        </gmd:MD_LegalConstraints>

      </gmd:resourceConstraints>

      <gmd:resourceConstraints>

        <gmd:MD_SecurityConstraints>

          <gmd:classification>

            <gmd:MD_ClassificationCode codeList=\"./resources/codeList.xml#MD_ClassificationCode\" codeListValue=\"\"/>

          </gmd:classification>

          <gmd:userNote>

            <gco:CharacterString/>

          </gmd:userNote>

        </gmd:MD_SecurityConstraints>

      </gmd:resourceConstraints>

      <gmd:spatialRepresentationType>

        <gmd:MD_SpatialRepresentationTypeCode codeList=\"./resources/codeList.xml#MD_SpatialRepresentationTypeCode\" codeListValue=\"textTable\"/>

      </gmd:spatialRepresentationType>

      <gmd:spatialResolution>

        <gmd:MD_Resolution>

          <gmd:equivalentScale>

            <gmd:MD_RepresentativeFraction>

              <gmd:denominator>

                <gco:Integer/>

              </gmd:denominator>

            </gmd:MD_RepresentativeFraction>

          </gmd:equivalentScale>

        </gmd:MD_Resolution>

      </gmd:spatialResolution>

      <gmd:spatialResolution>

        <gmd:MD_Resolution>

          <gmd:distance/>

        </gmd:MD_Resolution>

      </gmd:spatialResolution>

      <gmd:language>

        <gco:CharacterString>fre</gco:CharacterString>

      </gmd:language>

      <gmd:characterSet>

        <gmd:MD_CharacterSetCode codeList=\"./resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\"/>

      </gmd:characterSet>

      <gmd:topicCategory>

        <gmd:MD_TopicCategoryCode/>

      </gmd:topicCategory>





      <gmd:extent>

        <gmd:EX_Extent>

          <gmd:description gco:nilReason=\"missing\">

            <gco:CharacterString/>

          </gmd:description>

          <gmd:geographicElement>

            <gmd:EX_GeographicBoundingBox>

              <gmd:westBoundLongitude>

                <gco:Decimal>0</gco:Decimal>

              </gmd:westBoundLongitude>

              <gmd:eastBoundLongitude>

                <gco:Decimal>0</gco:Decimal>

              </gmd:eastBoundLongitude>

              <gmd:southBoundLatitude>

                <gco:Decimal>0</gco:Decimal>

              </gmd:southBoundLatitude>

              <gmd:northBoundLatitude>

                <gco:Decimal>0</gco:Decimal>

              </gmd:northBoundLatitude>

            </gmd:EX_GeographicBoundingBox>

          </gmd:geographicElement>



        </gmd:EX_Extent>

      </gmd:extent><gmd:supplementalInformation>

        <gco:CharacterString/>

      </gmd:supplementalInformation>



    </gmd:MD_DataIdentification>

  </gmd:identificationInfo>

  <gmd:dataQualityInfo>

    <gmd:DQ_DataQuality>

      <gmd:scope>

        <gmd:DQ_Scope>

          <gmd:level>

            <gmd:MD_ScopeCode codeList=\"./resources/codeList.xml#MD_ScopeCode\" codeListValue=\"dataset\"/>

          </gmd:level>

        </gmd:DQ_Scope>

      </gmd:scope>

      <gmd:lineage>

        <gmd:LI_Lineage>

          <gmd:statement>

            <gco:CharacterString/>

          </gmd:statement>

          <gmd:source>

            <gmd:LI_Source>

              <gmd:description>

                <gco:CharacterString/>

              </gmd:description>

            </gmd:LI_Source>

          </gmd:source>

        </gmd:LI_Lineage>

      </gmd:lineage>

    </gmd:DQ_DataQuality>

  </gmd:dataQualityInfo>



</gmd:MD_Metadata>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, 'gmd:MD_Metadata', NULL, 1, 1, NULL, 0, 0, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (12, '7d19ee05-1684-4e01-8e18-901688427dc5', 'iso19139', 's', 'n', '2012-04-11T12:19:46', '2012-04-11T12:19:46', '<gmd:CI_ResponsibleParty xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:geonet=\"http://www.fao.org/geonetwork\">
    <gmd:individualName>
      <gco:CharacterString>-- Contact --</gco:CharacterString>
    </gmd:individualName>
    <gmd:organisationName>
      <gco:CharacterString>-- Organisation --</gco:CharacterString>
    </gmd:organisationName>
    <gmd:contactInfo>
      <gmd:CI_Contact>
        <gmd:phone>
          <gmd:CI_Telephone>
            <gmd:voice>
              <gco:CharacterString/>
            </gmd:voice>
            <gmd:facsimile>
              <gco:CharacterString/>
            </gmd:facsimile>
          </gmd:CI_Telephone>
        </gmd:phone>
        <gmd:address>
          <gmd:CI_Address>
            <gmd:deliveryPoint>
              <gco:CharacterString/>
            </gmd:deliveryPoint>
            <gmd:city>
              <gco:CharacterString/>
            </gmd:city>
            <gmd:administrativeArea>
              <gco:CharacterString/>
            </gmd:administrativeArea>
            <gmd:postalCode>
              <gco:CharacterString/>
            </gmd:postalCode>
            <gmd:country>
              <gco:CharacterString/>
            </gmd:country>
            <gmd:electronicMailAddress>
              <gco:CharacterString/>
            </gmd:electronicMailAddress>
          </gmd:CI_Address>
        </gmd:address>
      </gmd:CI_Contact>
    </gmd:contactInfo>
    <gmd:role/>
  </gmd:CI_ResponsibleParty>
', '7fc45be3-9aba-4198-920c-b8737112d522', 'PointOfContact', 'gmd:CI_ResponsibleParty', NULL, 1, 1, NULL, 0, 0, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (40087, '88cacc48-04c5-42a6-vf45-d1d375214856', 'iso19139', 'y', 'n', '2010-03-04T15:46:22', '2010-03-04T15:46:22', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:fra=\"http://www.cnig.gouv.fr/2005/fra\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xsi:schemaLocation=\"http://www.isotc211.org/2005/fra ../../../../web/geonetwork/xml/schemas/iso19139fra/schema.xsd\">

  <gmd:fileIdentifier>

    <gco:CharacterString>88cacc48-04c5-42a6-vf45-d1d375214856</gco:CharacterString>

  </gmd:fileIdentifier>

  <gmd:language>



  <gco:CharacterString>fre</gco:CharacterString></gmd:language>

  <gmd:characterSet>

    <gmd:MD_CharacterSetCode codeList=\"./resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\"/>

  </gmd:characterSet>

  <gmd:hierarchyLevel>

    <gmd:MD_ScopeCode codeList=\"./resources/codeList.xml#MD_ScopeCode\" codeListValue=\"dataset\"/>

  </gmd:hierarchyLevel><gmd:contact>

    <gmd:CI_ResponsibleParty>

      <gmd:individualName>

        <gco:CharacterString/>

      </gmd:individualName>

      <gmd:organisationName>

        <gco:CharacterString/>

      </gmd:organisationName>

      <gmd:positionName>

        <gco:CharacterString/>

      </gmd:positionName>

      <gmd:contactInfo>

        <gmd:CI_Contact>

          <gmd:phone>

            <gmd:CI_Telephone>

              <gmd:voice>

                <gco:CharacterString/>

              </gmd:voice>

              <gmd:facsimile>

                <gco:CharacterString/>

              </gmd:facsimile>

            </gmd:CI_Telephone>

          </gmd:phone>

          <gmd:address>

            <gmd:CI_Address>

              <gmd:deliveryPoint>

                <gco:CharacterString/>

              </gmd:deliveryPoint>

              <gmd:city>

                <gco:CharacterString/>

              </gmd:city>

              <gmd:administrativeArea>

                <gco:CharacterString/>

              </gmd:administrativeArea>

              <gmd:postalCode>

                <gco:CharacterString/>

              </gmd:postalCode>

              <gmd:country>

                <gco:CharacterString/>

              </gmd:country>

              <gmd:electronicMailAddress>

                <gco:CharacterString/>

              </gmd:electronicMailAddress>

            </gmd:CI_Address>

          </gmd:address>

        </gmd:CI_Contact>

      </gmd:contactInfo>

      <gmd:role>

        <gmd:CI_RoleCode codeList=\"./resources/codeList.xml#CI_RoleCode\" codeListValue=\"originator\"/>

      </gmd:role>

    </gmd:CI_ResponsibleParty>

  </gmd:contact>

  <gmd:dateStamp>

    <gco:DateTime>2006-07-27T09:02:02</gco:DateTime>

  </gmd:dateStamp>

  <gmd:metadataStandardName>

    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>

  </gmd:metadataStandardName>

  <gmd:metadataStandardVersion>

    <gco:CharacterString>1.0</gco:CharacterString>

  </gmd:metadataStandardVersion>

  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>

    <gmd:MD_DataIdentification>

      <gmd:citation>

        <gmd:CI_Citation>

          <gmd:title>

            <gco:CharacterString>métadonnée de série de données par jointure</gco:CharacterString>

          </gmd:title><gmd:date>

            <gmd:CI_Date>

              <gmd:date>

                <gco:DateTime/>

              </gmd:date>

              <gmd:dateType>

                <gmd:CI_DateTypeCode codeList=\"./resources/codeList.xml#CI_DateTypeCode\" codeListValue=\"publication\"/>

              </gmd:dateType>

            </gmd:CI_Date>

          </gmd:date>





        </gmd:CI_Citation>

      </gmd:citation>

      <gmd:abstract>

        <gco:CharacterString/>

      </gmd:abstract>

      <gmd:pointOfContact>

        <gmd:CI_ResponsibleParty>

          <gmd:individualName>

            <gco:CharacterString/>

          </gmd:individualName>

          <gmd:organisationName>

            <gco:CharacterString/>

          </gmd:organisationName>

          <gmd:contactInfo>

            <gmd:CI_Contact>

              <gmd:phone>

                <gmd:CI_Telephone>

                  <gmd:voice>

                    <gco:CharacterString/>

                  </gmd:voice>

                </gmd:CI_Telephone>

              </gmd:phone>

              <gmd:address>

                <gmd:CI_Address>

                  <gmd:electronicMailAddress>

                    <gco:CharacterString/>

                  </gmd:electronicMailAddress>

                </gmd:CI_Address>

              </gmd:address>

            </gmd:CI_Contact>

          </gmd:contactInfo>

          <gmd:role>

            <gmd:CI_RoleCode codeList=\"./resources/codeList.xml#CI_RoleCode\" codeListValue=\"distributor\"/>

          </gmd:role>

        </gmd:CI_ResponsibleParty>

      </gmd:pointOfContact>

      <gmd:descriptiveKeywords>

        <gmd:MD_Keywords>

          <gmd:keyword>

            <gco:CharacterString/>

          </gmd:keyword>

          <gmd:keyword>

            <gco:CharacterString/>

          </gmd:keyword>

          <gmd:type>

            <gmd:MD_KeywordTypeCode codeList=\"./resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"theme\"/>

          </gmd:type>

        </gmd:MD_Keywords>

      </gmd:descriptiveKeywords>

<gmd:descriptiveKeywords>

        <gmd:MD_Keywords>

          <gmd:keyword gco:nilReason=\"missing\">

            <gco:CharacterString/>

          </gmd:keyword>

          <gmd:type>

            <gmd:MD_KeywordTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"place\"/>

          </gmd:type>

        </gmd:MD_Keywords>

      </gmd:descriptiveKeywords>

      <gmd:resourceConstraints>

        <gmd:MD_LegalConstraints>

          <gmd:useLimitation>

            <gco:CharacterString/>

          </gmd:useLimitation>

          <gmd:accessConstraints>

            <gmd:MD_RestrictionCode codeList=\"./resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"restricted\"/>

          </gmd:accessConstraints>

          <gmd:useConstraints>

            <gmd:MD_RestrictionCode codeList=\"./resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"copyright\"/>

          </gmd:useConstraints>

          <gmd:otherConstraints>

            <gco:CharacterString/>

          </gmd:otherConstraints>

        </gmd:MD_LegalConstraints>

      </gmd:resourceConstraints>

      <gmd:resourceConstraints>

        <gmd:MD_SecurityConstraints>

          <gmd:classification>

            <gmd:MD_ClassificationCode codeList=\"./resources/codeList.xml#MD_ClassificationCode\" codeListValue=\"\"/>

          </gmd:classification>

          <gmd:userNote>

            <gco:CharacterString/>

          </gmd:userNote>

        </gmd:MD_SecurityConstraints>

      </gmd:resourceConstraints>

      <gmd:spatialRepresentationType>

        <gmd:MD_SpatialRepresentationTypeCode codeList=\"./resources/codeList.xml#MD_SpatialRepresentationTypeCode\" codeListValue=\"vector\"/>

      </gmd:spatialRepresentationType>

      <gmd:spatialResolution>

        <gmd:MD_Resolution>

          <gmd:equivalentScale>

            <gmd:MD_RepresentativeFraction>

              <gmd:denominator>

                <gco:Integer/>

              </gmd:denominator>

            </gmd:MD_RepresentativeFraction>

          </gmd:equivalentScale>

        </gmd:MD_Resolution>

      </gmd:spatialResolution>

      <gmd:spatialResolution>

        <gmd:MD_Resolution>

          <gmd:distance/>

        </gmd:MD_Resolution>

      </gmd:spatialResolution>

      <gmd:language>

        <gco:CharacterString>fre</gco:CharacterString>

      </gmd:language>

      <gmd:characterSet>

        <gmd:MD_CharacterSetCode codeList=\"./resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\"/>

      </gmd:characterSet>

      <gmd:topicCategory>

        <gmd:MD_TopicCategoryCode/>

      </gmd:topicCategory>





      <gmd:extent>

        <gmd:EX_Extent>

          <gmd:description gco:nilReason=\"missing\">

            <gco:CharacterString/>

          </gmd:description>

          <gmd:geographicElement>

            <gmd:EX_GeographicBoundingBox>

              <gmd:westBoundLongitude>

                <gco:Decimal>0</gco:Decimal>

              </gmd:westBoundLongitude>

              <gmd:eastBoundLongitude>

                <gco:Decimal>0</gco:Decimal>

              </gmd:eastBoundLongitude>

              <gmd:southBoundLatitude>

                <gco:Decimal>0</gco:Decimal>

              </gmd:southBoundLatitude>

              <gmd:northBoundLatitude>

                <gco:Decimal>0</gco:Decimal>

              </gmd:northBoundLatitude>

            </gmd:EX_GeographicBoundingBox>

          </gmd:geographicElement>



        </gmd:EX_Extent>

      </gmd:extent><gmd:supplementalInformation>

        <gco:CharacterString/>

      </gmd:supplementalInformation>



    </gmd:MD_DataIdentification>

  </gmd:identificationInfo>

  <gmd:dataQualityInfo>

    <gmd:DQ_DataQuality>

      <gmd:scope>

        <gmd:DQ_Scope>

          <gmd:level>

            <gmd:MD_ScopeCode codeList=\"./resources/codeList.xml#MD_ScopeCode\" codeListValue=\"dataset\"/>

          </gmd:level>

        </gmd:DQ_Scope>

      </gmd:scope>

      <gmd:lineage>

        <gmd:LI_Lineage>

          <gmd:statement>

            <gco:CharacterString/>

          </gmd:statement>

          <gmd:source>

            <gmd:LI_Source>

              <gmd:description>

                <gco:CharacterString/>

              </gmd:description>

            </gmd:LI_Source>

          </gmd:source>

        </gmd:LI_Lineage>

      </gmd:lineage>

    </gmd:DQ_DataQuality>

  </gmd:dataQualityInfo>



</gmd:MD_Metadata>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, 'gmd:MD_Metadata', NULL, 1, 1, NULL, 0, 0, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (40086, 'c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1', 'iso19139', 'y', 'n', '2012-04-11T12:19:46', '2017-04-27T12:01:53', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:srv=\"http://www.isotc211.org/2005/srv\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:gfc=\"http://www.isotc211.org/2005/gfc\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:geonet=\"http://www.fao.org/geonetwork\">
  <gmd:fileIdentifier>
    <gco:CharacterString>c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>
    <gco:CharacterString>fre</gco:CharacterString>
  </gmd:language>
  <gmd:characterSet>
    <gmd:MD_CharacterSetCode codeListValue=\"utf8\" codeList=\"MD_CharacterSetCode\"/>
  </gmd:characterSet>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeListValue=\"service\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_ScopeCode\"/>
  </gmd:hierarchyLevel>
  <gmd:hierarchyLevelName>
    <gco:CharacterString>Service</gco:CharacterString>
  </gmd:hierarchyLevelName>
  <gmd:contact>
    <gmd:CI_ResponsibleParty>
      <gmd:organisationName>
        <gco:CharacterString>-- Contact sur la métadonnée --</gco:CharacterString>
      </gmd:organisationName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone/>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:electronicMailAddress>
                <gco:CharacterString>-- Adresse email --</gco:CharacterString>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"pointOfContact\"/>
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gmd:contact>
  <gmd:dateStamp>
    <gco:DateTime>2012-04-11T12:19:46</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:metadataStandardVersion>
    <gco:CharacterString>1.0</gco:CharacterString>
  </gmd:metadataStandardVersion>
  <gmd:referenceSystemInfo>
    <gmd:MD_ReferenceSystem>
      <gmd:referenceSystemIdentifier>
        <gmd:RS_Identifier>
          <gmd:code>
            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>
          </gmd:code>
          <gmd:codeSpace>
            <gco:CharacterString>EPSG</gco:CharacterString>
          </gmd:codeSpace>
          <gmd:version>
            <gco:CharacterString>7.4</gco:CharacterString>
          </gmd:version>
        </gmd:RS_Identifier>
      </gmd:referenceSystemIdentifier>
    </gmd:MD_ReferenceSystem>
  </gmd:referenceSystemInfo>
  <gmd:identificationInfo>
    <srv:SV_ServiceIdentification>
      <gmd:citation>
        <gmd:CI_Citation>
          <gmd:title>
            <gco:CharacterString>Modèle pour la saisie d''un service</gco:CharacterString>
          </gmd:title>
          <gmd:date>
            <gmd:CI_Date>
              <gmd:date>
                <gco:Date>2010-01-01</gco:Date>
              </gmd:date>
              <gmd:dateType>
                <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_DateTypeCode\" codeListValue=\"creation\"/>
              </gmd:dateType>
            </gmd:CI_Date>
          </gmd:date>
        </gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract>
        <gco:CharacterString>-- Description du service  --</gco:CharacterString>
      </gmd:abstract>
      <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty>
          <gmd:organisationName>
            <gco:CharacterString>-- Organisation responsable du service --</gco:CharacterString>
          </gmd:organisationName>
          <gmd:contactInfo>
            <gmd:CI_Contact>
              <gmd:address>
                <gmd:CI_Address>
                  <gmd:electronicMailAddress>
                    <gco:CharacterString>-- Adresse email --</gco:CharacterString>
                  </gmd:electronicMailAddress>
                </gmd:CI_Address>
              </gmd:address>
            </gmd:CI_Contact>
          </gmd:contactInfo>
          <gmd:role>
            <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"owner\"/>
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints/>
      </gmd:resourceConstraints>
      <gmd:resourceConstraints>
        <gmd:MD_Constraints/>
      </gmd:resourceConstraints>
      <srv:serviceType>
        <gco:LocalName>W3C:HTML:LINK</gco:LocalName>
      </srv:serviceType>
      <srv:serviceTypeVersion>
        <gco:CharacterString>-- Version du service --</gco:CharacterString>
      </srv:serviceTypeVersion>
      <srv:restrictions>
        <gmd:MD_Constraints/>
      </srv:restrictions>
      <srv:extent>
        <gmd:EX_Extent>
          <gmd:description>
            <gco:CharacterString>-- Description de l&amp;amp;apos;étendue géographique (eg France métropolitaine) --</gco:CharacterString>
          </gmd:description>
          <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
              <gmd:westBoundLongitude>
                <gco:Decimal>-5.449218749023903</gco:Decimal>
              </gmd:westBoundLongitude>
              <gmd:eastBoundLongitude>
                <gco:Decimal>11.777343747890368</gco:Decimal>
              </gmd:eastBoundLongitude>
              <gmd:southBoundLatitude>
                <gco:Decimal>40.1116886595881</gco:Decimal>
              </gmd:southBoundLatitude>
              <gmd:northBoundLatitude>
                <gco:Decimal>51.34433865388288</gco:Decimal>
              </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
          </gmd:geographicElement>
        </gmd:EX_Extent>
      </srv:extent>
      <srv:coupledResource/>
      <srv:couplingType>
        <srv:SV_CouplingType codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType\" codeListValue=\"tight\"/>
      </srv:couplingType>
      <srv:containsOperations>
        <srv:SV_OperationMetadata>
          <srv:operationName gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </srv:operationName>
          <srv:DCP>
            <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
          </srv:DCP>
          <srv:connectPoint>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>-- Adresse du service --</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
              </gmd:protocol>
            </gmd:CI_OnlineResource>
          </srv:connectPoint>
        </srv:SV_OperationMetadata>
      </srv:containsOperations>
      <srv:operatesOn uuidref=\"-- Identifiant de la métadonnée de couche associée --\"/>
    </srv:SV_ServiceIdentification>
  </gmd:identificationInfo>
  <gmd:distributionInfo>
    <gmd:MD_Distribution>
      <gmd:transferOptions>
        <gmd:MD_DigitalTransferOptions>
          <gmd:onLine>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>-- Adresse du service --</gmd:URL>
              </gmd:linkage>
            </gmd:CI_OnlineResource>
          </gmd:onLine>
        </gmd:MD_DigitalTransferOptions>
      </gmd:transferOptions>
    </gmd:MD_Distribution>
  </gmd:distributionInfo>
  <gmd:dataQualityInfo>
    <gmd:DQ_DataQuality>
      <gmd:scope>
        <gmd:DQ_Scope>
          <gmd:level>
            <gmd:MD_ScopeCode codeListValue=\"service\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_ScopeCode\"/>
          </gmd:level>
        </gmd:DQ_Scope>
      </gmd:scope>
      <gmd:lineage>
        <gmd:LI_Lineage/>
      </gmd:lineage>
    </gmd:DQ_DataQuality>
  </gmd:dataQualityInfo>
</gmd:MD_Metadata>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, 'gmd:MD_Metadata', NULL, 1, 1, NULL, 0, 0, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (40021, '83aa3f50-c5ed-4048-930f-91ee4b983e5c', 'iso19139', 'n', 'n', '2017-04-26T11:00:46', '2017-04-26T11:00:46', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gfc=\"http://www.isotc211.org/2005/gfc\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:srv=\"http://www.isotc211.org/2005/srv\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:geonet=\"http://www.fao.org/geonetwork\">
  <gmd:fileIdentifier>
    <gco:CharacterString>83aa3f50-c5ed-4048-930f-91ee4b983e5c</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>
    <gco:CharacterString>fre</gco:CharacterString>
  </gmd:language>
  <gmd:characterSet>
    <gmd:MD_CharacterSetCode codeListValue=\"utf8\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_CharacterSetCode\"/>
  </gmd:characterSet>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeListValue=\"service\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode\"/>
  </gmd:hierarchyLevel>
  <gmd:hierarchyLevelName>
    <gco:CharacterString>Service</gco:CharacterString>
  </gmd:hierarchyLevelName>
  <gmd:contact>
    <gmd:CI_ResponsibleParty>
      <gmd:organisationName>
        <gco:CharacterString>-- Contact sur la métadonnée --</gco:CharacterString>
      </gmd:organisationName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone/>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:electronicMailAddress>
                <gco:CharacterString>-- Adresse email --</gco:CharacterString>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"pointOfContact\"/>
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gmd:contact>
  <gmd:dateStamp>
    <gco:DateTime>2017-04-26T11:00:46</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:metadataStandardVersion>
    <gco:CharacterString>1.0</gco:CharacterString>
  </gmd:metadataStandardVersion>
  <gmd:referenceSystemInfo>
    <gmd:MD_ReferenceSystem>
      <gmd:referenceSystemIdentifier>
        <gmd:RS_Identifier>
          <gmd:code>
            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>
          </gmd:code>
          <gmd:codeSpace>
            <gco:CharacterString>EPSG</gco:CharacterString>
          </gmd:codeSpace>
          <gmd:version>
            <gco:CharacterString>7.4</gco:CharacterString>
          </gmd:version>
        </gmd:RS_Identifier>
      </gmd:referenceSystemIdentifier>
    </gmd:MD_ReferenceSystem>
  </gmd:referenceSystemInfo>
  <gmd:identificationInfo>
    <srv:SV_ServiceIdentification>
      <gmd:citation>
        <gmd:CI_Citation>
          <gmd:title>
            <gco:CharacterString>Service WFS</gco:CharacterString>
          </gmd:title>
          <gmd:date>
            <gmd:CI_Date>
              <gmd:date>
                <gco:Date>2010-01-01</gco:Date>
              </gmd:date>
              <gmd:dateType>
                <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode\" codeListValue=\"creation\"/>
              </gmd:dateType>
            </gmd:CI_Date>
          </gmd:date>
        </gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract>
        <gco:CharacterString>-- Description du service WFS --</gco:CharacterString>
      </gmd:abstract>
      <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty>
          <gmd:organisationName>
            <gco:CharacterString>-- Organisation responsable du service --</gco:CharacterString>
          </gmd:organisationName>
          <gmd:contactInfo>
            <gmd:CI_Contact>
              <gmd:address>
                <gmd:CI_Address>
                  <gmd:electronicMailAddress>
                    <gco:CharacterString>-- Adresse email  --</gco:CharacterString>
                  </gmd:electronicMailAddress>
                </gmd:CI_Address>
              </gmd:address>
            </gmd:CI_Contact>
          </gmd:contactInfo>
          <gmd:role>
            <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"owner\"/>
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>OGC:WFS</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode\" codeListValue=\"theme\"/>
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints/>
      </gmd:resourceConstraints>
      <gmd:resourceConstraints>
        <gmd:MD_Constraints/>
      </gmd:resourceConstraints>
      <srv:serviceType>
        <gco:LocalName>download</gco:LocalName>
      </srv:serviceType>
      <srv:serviceTypeVersion>
        <gco:CharacterString>-- Version du service WFS --</gco:CharacterString>
      </srv:serviceTypeVersion>
      <srv:restrictions>
        <gmd:MD_Constraints/>
      </srv:restrictions>
      <srv:extent>
        <gmd:EX_Extent>
          <gmd:description>
            <gco:CharacterString>-- Description de l''étendue géographique (eg France métropolitaine) --</gco:CharacterString>
          </gmd:description>
          <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
              <gmd:westBoundLongitude>
                <gco:Decimal>-5.449218749023903</gco:Decimal>
              </gmd:westBoundLongitude>
              <gmd:eastBoundLongitude>
                <gco:Decimal>11.777343747890368</gco:Decimal>
              </gmd:eastBoundLongitude>
              <gmd:southBoundLatitude>
                <gco:Decimal>40.1116886595881</gco:Decimal>
              </gmd:southBoundLatitude>
              <gmd:northBoundLatitude>
                <gco:Decimal>51.34433865388288</gco:Decimal>
              </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
          </gmd:geographicElement>
        </gmd:EX_Extent>
      </srv:extent>
      <srv:couplingType>
        <srv:SV_CouplingType codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType\" codeListValue=\"tight\"/>
      </srv:couplingType>
      <srv:containsOperations>
        <srv:SV_OperationMetadata>
          <srv:operationName>
            <gco:CharacterString>GetCapabilities</gco:CharacterString>
          </srv:operationName>
          <srv:DCP>
            <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
          </srv:DCP>
          <srv:connectPoint>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://carto.prodige.internal/cgi-bin/mapservwfs?</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>OGC:WFS-1.0.0-http-get-capabilities</gco:CharacterString>
              </gmd:protocol>
            </gmd:CI_OnlineResource>
          </srv:connectPoint>
        </srv:SV_OperationMetadata>
      </srv:containsOperations>
      <srv:containsOperations>
        <srv:SV_OperationMetadata>
          <srv:operationName>
            <gco:CharacterString>DescribeFeatureType</gco:CharacterString>
          </srv:operationName>
          <srv:DCP>
            <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
          </srv:DCP>
          <srv:connectPoint>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://carto.prodige.internal/cgi-bin/mapservwfs?</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>OGC:WFS-1.0.0-http-get-capabilities</gco:CharacterString>
              </gmd:protocol>
            </gmd:CI_OnlineResource>
          </srv:connectPoint>
        </srv:SV_OperationMetadata>
      </srv:containsOperations>
      <srv:containsOperations>
        <srv:SV_OperationMetadata>
          <srv:operationName>
            <gco:CharacterString>GetFeature</gco:CharacterString>
          </srv:operationName>
          <srv:DCP>
            <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
          </srv:DCP>
          <srv:connectPoint>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://carto.prodige.internal/cgi-bin/mapservwfs?</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>OGC:WFS-1.0.0-http-get-capabilities</gco:CharacterString>
              </gmd:protocol>
            </gmd:CI_OnlineResource>
          </srv:connectPoint>
        </srv:SV_OperationMetadata>
      </srv:containsOperations>
    </srv:SV_ServiceIdentification>
  </gmd:identificationInfo>
  <gmd:distributionInfo>
    <gmd:MD_Distribution>
      <gmd:transferOptions>
        <gmd:MD_DigitalTransferOptions>
          <gmd:onLine>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://carto.prodige.internal/cgi-bin/mapservwfs?</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>OGC:WFS-1.0.0-http-get-capabilities</gco:CharacterString>
              </gmd:protocol>
            </gmd:CI_OnlineResource>
          </gmd:onLine>
        </gmd:MD_DigitalTransferOptions>
      </gmd:transferOptions>
    </gmd:MD_Distribution>
  </gmd:distributionInfo>
  <gmd:dataQualityInfo>
    <gmd:DQ_DataQuality>
      <gmd:scope>
        <gmd:DQ_Scope>
          <gmd:level>
            <gmd:MD_ScopeCode codeListValue=\"service\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode\"/>
          </gmd:level>
        </gmd:DQ_Scope>
      </gmd:scope>
      <gmd:lineage>
        <gmd:LI_Lineage/>
      </gmd:lineage>
    </gmd:DQ_DataQuality>
  </gmd:dataQualityInfo>
</gmd:MD_Metadata>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, NULL, NULL, 1, 1, NULL, 0, 0, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (40022, '80a7ceef-0bd5-4238-a631-f72fb3330ded', 'iso19139', 'n', 'n', '2017-04-26T11:00:49', '2017-04-26T11:00:49', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:srv=\"http://www.isotc211.org/2005/srv\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:gml=\"http://www.opengis.net/gml\" xsi:schemaLocation=\"http://www.isotc211.org/2005/srv http://schemas.opengis.net/iso/19139/20060504/srv/srv.xsd\">
  <gmd:fileIdentifier>
    <gco:CharacterString>80a7ceef-0bd5-4238-a631-f72fb3330ded</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>
    <gmd:LanguageCode codeList=\"http://www.loc.gov/standards/iso639-2/\" codeListValue=\"fre\"/>
  </gmd:language>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeListValue=\"service\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode\"/>
  </gmd:hierarchyLevel>
  <gmd:hierarchyLevelName>
    <gco:CharacterString>Service de téléchargement simple \"Atom-based\"</gco:CharacterString>
  </gmd:hierarchyLevelName>
  <gmd:contact>
    <gmd:CI_ResponsibleParty>
      <gmd:organisationName>
        <gco:CharacterString>-- Contact sur la métadonnée --</gco:CharacterString>
      </gmd:organisationName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone/>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:electronicMailAddress>
                <gco:CharacterString>-- Adresse email --</gco:CharacterString>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"pointOfContact\"/>
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gmd:contact>
  <gmd:dateStamp>
    <gco:DateTime>2017-04-26T11:00:49</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>ISO 19119</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:metadataStandardVersion>
    <gco:CharacterString>2005/PDAM1</gco:CharacterString>
  </gmd:metadataStandardVersion>
  <gmd:referenceSystemInfo>
    <gmd:MD_ReferenceSystem>
      <gmd:referenceSystemIdentifier>
        <gmd:RS_Identifier>
          <gmd:code>
            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>
          </gmd:code>
          <gmd:codeSpace>
            <gco:CharacterString>EPSG</gco:CharacterString>
          </gmd:codeSpace>
          <gmd:version>
            <gco:CharacterString>7.4</gco:CharacterString>
          </gmd:version>
        </gmd:RS_Identifier>
      </gmd:referenceSystemIdentifier>
    </gmd:MD_ReferenceSystem>
  </gmd:referenceSystemInfo>
  <gmd:identificationInfo>
    <srv:SV_ServiceIdentification>
      <gmd:citation>
        <gmd:CI_Citation>
          <gmd:title>
            <gco:CharacterString>Service de téléchargement simple de la plateforme</gco:CharacterString>
          </gmd:title>
          <gmd:date>
            <gmd:CI_Date>
              <gmd:date>
                <gco:Date>2014-05-26</gco:Date>
              </gmd:date>
              <gmd:dateType>
                <gmd:CI_DateTypeCode codeListValue=\"creation\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode\"/>
              </gmd:dateType>
            </gmd:CI_Date>
          </gmd:date>
        </gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract>
        <gco:CharacterString>Service de téléchargement simple</gco:CharacterString>
      </gmd:abstract>
      <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty>
          <gmd:organisationName>
            <gco:CharacterString>-- Organisation responsable du service --</gco:CharacterString>
          </gmd:organisationName>
          <gmd:contactInfo>
            <gmd:CI_Contact>
              <gmd:address>
                <gmd:CI_Address>
                  <gmd:electronicMailAddress>
                    <gco:CharacterString>-- Adresse email  --</gco:CharacterString>
                  </gmd:electronicMailAddress>
                </gmd:CI_Address>
              </gmd:address>
            </gmd:CI_Contact>
          </gmd:contactInfo>
          <gmd:role>
            <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"owner\"/>
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>Service d’accès au produit</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>InfoProductAccessService</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeListValue=\"stratum\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode\"/>
          </gmd:type>
          <gmd:thesaurusName>
            <gmd:CI_Citation>
              <gmd:title>
                <gco:CharacterString>RÈGLEMENT (CE) No 1205/2008, partie D.4</gco:CharacterString>
              </gmd:title>
              <gmd:date>
                <gmd:CI_Date>
                  <gmd:date>
                    <gco:Date>2008-12-03</gco:Date>
                  </gmd:date>
                  <gmd:dateType>
                    <gmd:CI_DateTypeCode codeListValue=\"publication\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode\"/>
                  </gmd:dateType>
                </gmd:CI_Date>
              </gmd:date>
            </gmd:CI_Citation>
          </gmd:thesaurusName>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>NIVEAU DE DIFFUSION</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>GRAND PUBLIC</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode\" codeListValue=\"theme\"/>
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints>
          <gmd:useLimitation>
            <gco:CharacterString>Aucune condition ne s applique</gco:CharacterString>
          </gmd:useLimitation>
          <gmd:accessConstraints>
            <gmd:MD_RestrictionCode codeListValue=\"otherRestrictions\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_RestrictionCode\"/>
          </gmd:accessConstraints>
          <gmd:otherConstraints>
            <gco:CharacterString>Pas de limitation d''accès public.</gco:CharacterString>
          </gmd:otherConstraints>
        </gmd:MD_LegalConstraints>
      </gmd:resourceConstraints>
      <srv:serviceType>
        <gco:LocalName>download</gco:LocalName>
      </srv:serviceType>
      <srv:extent>
        <gmd:EX_Extent>
          <gmd:description>
            <gco:CharacterString>FRANCE METROPOLITAINE</gco:CharacterString>
          </gmd:description>
          <gmd:geographicElement>
            <gmd:EX_GeographicDescription>
              <gmd:geographicIdentifier>
                <gmd:MD_Identifier>
                  <gmd:code>
                    <gco:CharacterString>http://id.insee.fr/geo/pays/99100</gco:CharacterString>
                  </gmd:code>
                </gmd:MD_Identifier>
              </gmd:geographicIdentifier>
            </gmd:EX_GeographicDescription>
          </gmd:geographicElement>
          <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
              <gmd:westBoundLongitude>
                <gco:Decimal>-5.14</gco:Decimal>
              </gmd:westBoundLongitude>
              <gmd:eastBoundLongitude>
                <gco:Decimal>9.56</gco:Decimal>
              </gmd:eastBoundLongitude>
              <gmd:southBoundLatitude>
                <gco:Decimal>41.33</gco:Decimal>
              </gmd:southBoundLatitude>
              <gmd:northBoundLatitude>
                <gco:Decimal>51.09</gco:Decimal>
              </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
          </gmd:geographicElement>
        </gmd:EX_Extent>
      </srv:extent>
      <srv:couplingType>
        <srv:SV_CouplingType codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType\" codeListValue=\"tight\"/>
      </srv:couplingType>
      <srv:containsOperations>
        <srv:SV_OperationMetadata>
          <srv:operationName>
            <gco:CharacterString>GetResource</gco:CharacterString>
          </srv:operationName>
          <srv:DCP>
            <srv:DCPList codeListValue=\"WebServices\" codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\"/>
          </srv:DCP>
          <srv:connectPoint>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://catalogue.prodige.internal/rss/atomfeed/topatom</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>WWW:DOWNLOAD-1.0-http--download</gco:CharacterString>
              </gmd:protocol>
            </gmd:CI_OnlineResource>
          </srv:connectPoint>
        </srv:SV_OperationMetadata>
      </srv:containsOperations>
      <srv:containsOperations>
        <srv:SV_OperationMetadata>
          <srv:operationName>
            <gco:CharacterString>GetServiceDescription</gco:CharacterString>
          </srv:operationName>
          <srv:DCP>
            <srv:DCPList codeListValue=\"WebServices\" codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\"/>
          </srv:DCP>
          <srv:connectPoint>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://catalogue.prodige.internal/rss/atomfeed/topatom</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>WWW:DOWNLOAD-1.0-http--download</gco:CharacterString>
              </gmd:protocol>
            </gmd:CI_OnlineResource>
          </srv:connectPoint>
        </srv:SV_OperationMetadata>
      </srv:containsOperations>
      <srv:containsOperations>
        <srv:SV_OperationMetadata>
          <srv:operationName>
            <gco:CharacterString>GetResourceDescription</gco:CharacterString>
          </srv:operationName>
          <srv:DCP>
            <srv:DCPList codeListValue=\"WebServices\" codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\"/>
          </srv:DCP>
          <srv:connectPoint>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://catalogue.prodige.internal/rss/atomfeed/topatom</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>WWW:DOWNLOAD-1.0-http--download</gco:CharacterString>
              </gmd:protocol>
            </gmd:CI_OnlineResource>
          </srv:connectPoint>
        </srv:SV_OperationMetadata>
      </srv:containsOperations>
    </srv:SV_ServiceIdentification>
  </gmd:identificationInfo>
  <gmd:distributionInfo>
    <gmd:MD_Distribution>
      <gmd:distributionFormat>
        <gmd:MD_Format>
          <gmd:name>
            <gco:CharacterString>Format variable selon le jeu de données</gco:CharacterString>
          </gmd:name>
          <gmd:version>
            <gco:CharacterString>Inconnue</gco:CharacterString>
          </gmd:version>
        </gmd:MD_Format>
      </gmd:distributionFormat>
      <gmd:transferOptions>
        <gmd:MD_DigitalTransferOptions>
          <gmd:unitsOfDistribution>
            <gco:CharacterString>Accès au service</gco:CharacterString>
          </gmd:unitsOfDistribution>
          <gmd:onLine>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://catalogue.prodige.internal/rss/atomfeed/topatom</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>WWW:LINK-1.0-http--atom</gco:CharacterString>
              </gmd:protocol>
              <gmd:name>
                <gco:CharacterString>Document de description du service de téléchargement</gco:CharacterString>
              </gmd:name>
            </gmd:CI_OnlineResource>
          </gmd:onLine>
        </gmd:MD_DigitalTransferOptions>
      </gmd:transferOptions>
    </gmd:MD_Distribution>
  </gmd:distributionInfo>
  <gmd:dataQualityInfo>
    <gmd:DQ_DataQuality>
      <gmd:scope>
        <gmd:DQ_Scope>
          <gmd:level>
            <gmd:MD_ScopeCode codeListValue=\"service\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode\"/>
          </gmd:level>
        </gmd:DQ_Scope>
      </gmd:scope>
      <gmd:report>
        <gmd:DQ_DomainConsistency>
          <gmd:result>
            <gmd:DQ_ConformanceResult>
              <gmd:specification>
                <gmd:CI_Citation>
                  <gmd:title>
                    <gco:CharacterString>Règlement (UE) No 1088/2010</gco:CharacterString>
                  </gmd:title>
                  <gmd:date>
                    <gmd:CI_Date>
                      <gmd:date>
                        <gco:Date>2010-11-23</gco:Date>
                      </gmd:date>
                      <gmd:dateType>
                        <gmd:CI_DateTypeCode codeListValue=\"publication\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode\"/>
                      </gmd:dateType>
                    </gmd:CI_Date>
                  </gmd:date>
                </gmd:CI_Citation>
              </gmd:specification>
              <gmd:explanation>
                <gco:CharacterString>See the referenced specification</gco:CharacterString>
              </gmd:explanation>
              <gmd:pass gco:nilReason=\"unknown\"/>
            </gmd:DQ_ConformanceResult>
          </gmd:result>
        </gmd:DQ_DomainConsistency>
      </gmd:report>
    </gmd:DQ_DataQuality>
  </gmd:dataQualityInfo>
</gmd:MD_Metadata>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, NULL, NULL, 1, 1, NULL, 0, 0, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (40020, '1c661f3e-33b5-4410-aa0d-c85f21a8ae7d', 'iso19139', 'n', 'n', '2017-04-26T11:00:41', '2017-04-26T11:00:41', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gfc=\"http://www.isotc211.org/2005/gfc\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:srv=\"http://www.isotc211.org/2005/srv\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:geonet=\"http://www.fao.org/geonetwork\">
  <gmd:fileIdentifier>
    <gco:CharacterString>1c661f3e-33b5-4410-aa0d-c85f21a8ae7d</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>
    <gco:CharacterString>fre</gco:CharacterString>
  </gmd:language>
  <gmd:characterSet>
    <gmd:MD_CharacterSetCode codeListValue=\"utf8\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_CharacterSetCode\"/>
  </gmd:characterSet>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeListValue=\"service\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode\"/>
  </gmd:hierarchyLevel>
  <gmd:hierarchyLevelName>
    <gco:CharacterString>Service</gco:CharacterString>
  </gmd:hierarchyLevelName>
  <gmd:contact>
    <gmd:CI_ResponsibleParty>
      <gmd:organisationName>
        <gco:CharacterString>-- Contact sur la métadonnée --</gco:CharacterString>
      </gmd:organisationName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone/>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:electronicMailAddress>
                <gco:CharacterString>-- Adresse email --</gco:CharacterString>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"pointOfContact\"/>
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gmd:contact>
  <gmd:dateStamp>
    <gco:DateTime>2017-04-26T11:00:42</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:metadataStandardVersion>
    <gco:CharacterString>1.0</gco:CharacterString>
  </gmd:metadataStandardVersion>
  <gmd:referenceSystemInfo>
    <gmd:MD_ReferenceSystem>
      <gmd:referenceSystemIdentifier>
        <gmd:RS_Identifier>
          <gmd:code>
            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>
          </gmd:code>
          <gmd:codeSpace>
            <gco:CharacterString>EPSG</gco:CharacterString>
          </gmd:codeSpace>
          <gmd:version>
            <gco:CharacterString>7.4</gco:CharacterString>
          </gmd:version>
        </gmd:RS_Identifier>
      </gmd:referenceSystemIdentifier>
    </gmd:MD_ReferenceSystem>
  </gmd:referenceSystemInfo>
  <gmd:identificationInfo>
    <srv:SV_ServiceIdentification>
      <gmd:citation>
        <gmd:CI_Citation>
          <gmd:title>
            <gco:CharacterString>Service WMS</gco:CharacterString>
          </gmd:title>
          <gmd:date>
            <gmd:CI_Date>
              <gmd:date>
                <gco:Date>2010-01-01</gco:Date>
              </gmd:date>
              <gmd:dateType>
                <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode\" codeListValue=\"creation\"/>
              </gmd:dateType>
            </gmd:CI_Date>
          </gmd:date>
        </gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract>
        <gco:CharacterString>-- Description du service WMS --</gco:CharacterString>
      </gmd:abstract>
      <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty>
          <gmd:organisationName>
            <gco:CharacterString>-- Organisation responsable du service --</gco:CharacterString>
          </gmd:organisationName>
          <gmd:contactInfo>
            <gmd:CI_Contact>
              <gmd:address>
                <gmd:CI_Address>
                  <gmd:electronicMailAddress>
                    <gco:CharacterString>-- Adresse email  --</gco:CharacterString>
                  </gmd:electronicMailAddress>
                </gmd:CI_Address>
              </gmd:address>
            </gmd:CI_Contact>
          </gmd:contactInfo>
          <gmd:role>
            <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"owner\"/>
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>OGC:WMS</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode\" codeListValue=\"theme\"/>
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints/>
      </gmd:resourceConstraints>
      <gmd:resourceConstraints>
        <gmd:MD_Constraints/>
      </gmd:resourceConstraints>
      <srv:serviceType>
        <gco:LocalName>view</gco:LocalName>
      </srv:serviceType>
      <srv:serviceTypeVersion>
        <gco:CharacterString>-- Version du service WMS --</gco:CharacterString>
      </srv:serviceTypeVersion>
      <srv:restrictions>
        <gmd:MD_Constraints/>
      </srv:restrictions>
      <srv:extent>
        <gmd:EX_Extent>
          <gmd:description>
            <gco:CharacterString>-- Description de l''étendue géographique (eg France métropolitaine) --</gco:CharacterString>
          </gmd:description>
          <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
              <gmd:westBoundLongitude>
                <gco:Decimal>-5.449218749023903</gco:Decimal>
              </gmd:westBoundLongitude>
              <gmd:eastBoundLongitude>
                <gco:Decimal>11.777343747890368</gco:Decimal>
              </gmd:eastBoundLongitude>
              <gmd:southBoundLatitude>
                <gco:Decimal>40.1116886595881</gco:Decimal>
              </gmd:southBoundLatitude>
              <gmd:northBoundLatitude>
                <gco:Decimal>51.34433865388288</gco:Decimal>
              </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
          </gmd:geographicElement>
        </gmd:EX_Extent>
      </srv:extent>
      <srv:couplingType>
        <srv:SV_CouplingType codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType\" codeListValue=\"tight\"/>
      </srv:couplingType>
      <srv:containsOperations>
        <srv:SV_OperationMetadata>
          <srv:operationName>
            <gco:CharacterString>GetCapabilities</gco:CharacterString>
          </srv:operationName>
          <srv:DCP>
            <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
          </srv:DCP>
          <srv:connectPoint>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://carto.prodige.internal/cgi-bin/mapserv?</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>
              </gmd:protocol>
            </gmd:CI_OnlineResource>
          </srv:connectPoint>
        </srv:SV_OperationMetadata>
      </srv:containsOperations>
      <srv:containsOperations>
        <srv:SV_OperationMetadata>
          <srv:operationName>
            <gco:CharacterString>GetMap</gco:CharacterString>
          </srv:operationName>
          <srv:DCP>
            <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
          </srv:DCP>
          <srv:connectPoint>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://carto.prodige.internal/cgi-bin/mapserv?</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>
              </gmd:protocol>
            </gmd:CI_OnlineResource>
          </srv:connectPoint>
        </srv:SV_OperationMetadata>
      </srv:containsOperations>
      <srv:containsOperations>
        <srv:SV_OperationMetadata>
          <srv:operationName>
            <gco:CharacterString>GetFeatureInfo</gco:CharacterString>
          </srv:operationName>
          <srv:DCP>
            <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
          </srv:DCP>
          <srv:connectPoint>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://carto.prodige.internal/cgi-bin/mapserv?</gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>
              </gmd:protocol>
            </gmd:CI_OnlineResource>
          </srv:connectPoint>
        </srv:SV_OperationMetadata>
      </srv:containsOperations>
      <srv:coupledResource>
        <srv:SV_CoupledResource>
          <srv:operationName>
            <gco:CharacterString>GETMAP</gco:CharacterString>
          </srv:operationName>
          <srv:identifier>
            <gco:CharacterString>7a8db1d7-f5cb-455d-a911-78f93418a0ce</gco:CharacterString>
          </srv:identifier>
          <gco:ScopedName>TESTBF</gco:ScopedName>
        </srv:SV_CoupledResource>
      </srv:coupledResource>
      <srv:operatesOn xmlns:xlink=\"http://www.isotc211.org/2005/xlink\" uuidref=\"7a8db1d7-f5cb-455d-a911-78f93418a0ce\" xlink:href=\"https://catalogue.prodige.internal:443/geonetwork/srv/fre/csw?service=CSW&amp;amp;request=GetRecordById&amp;amp;version=2.0.2&amp;amp;outputSchema=http://www.isotc211.org/2005/gmd&amp;amp;elementSetName=full&amp;amp;id=7a8db1d7-f5cb-455d-a911-78f93418a0ce#MD_DataIdentification\"/>
    </srv:SV_ServiceIdentification>
  </gmd:identificationInfo>
  <gmd:distributionInfo>
    <gmd:MD_Distribution>
      <gmd:transferOptions>
        <gmd:MD_DigitalTransferOptions>
          <gmd:onLine>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL>https://carto.prodige.internal/cgi-bin/mapserv?</gmd:URL>
              </gmd:linkage>
            </gmd:CI_OnlineResource>
          </gmd:onLine>
        </gmd:MD_DigitalTransferOptions>
      </gmd:transferOptions>
    </gmd:MD_Distribution>
  </gmd:distributionInfo>
  <gmd:dataQualityInfo>
    <gmd:DQ_DataQuality>
      <gmd:scope>
        <gmd:DQ_Scope>
          <gmd:level>
            <gmd:MD_ScopeCode codeListValue=\"service\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode\"/>
          </gmd:level>
        </gmd:DQ_Scope>
      </gmd:scope>
      <gmd:lineage>
        <gmd:LI_Lineage/>
      </gmd:lineage>
    </gmd:DQ_DataQuality>
  </gmd:dataQualityInfo>
</gmd:MD_Metadata>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, NULL, NULL, 1, 1, NULL, 0, 0, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (9, '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 'iso19139', 'n', 'n', '2009-03-06T17:38:35', '2012-04-11T13:48:56', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:fra=\"http://www.cnig.gouv.fr/2005/fra\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:geonet=\"http://www.fao.org/geonetwork\" xsi:schemaLocation=\"http://www.isotc211.org/2005/gmd http://www.isotc211.org/2005/gmd/gmd.xsd\">
  <gmd:fileIdentifier>
    <gco:CharacterString>3c892ef0-0a6d-11de-ad6b-00104b7907b4</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>
    <gco:CharacterString>fre</gco:CharacterString>
  </gmd:language>
  <gmd:characterSet>
    <gmd:MD_CharacterSetCode codeList=\"./resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\" />
  </gmd:characterSet>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode\" codeListValue=\"dataset\" />
  </gmd:hierarchyLevel>
  <gmd:contact>
    <gmd:CI_ResponsibleParty>
      <gmd:individualName>
        <gco:CharacterString>-- Contact --</gco:CharacterString>
      </gmd:individualName>
      <gmd:organisationName>
        <gco:CharacterString>-- Organisation --</gco:CharacterString>
      </gmd:organisationName>
      <gmd:positionName gco:nilReason=\"missing\">
        <gco:CharacterString />
      </gmd:positionName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone>
              <gmd:voice>
                <gco:CharacterString>33 (0) 2 32 00 00 00</gco:CharacterString>
              </gmd:voice>
              <gmd:facsimile gco:nilReason=\"missing\">
                <gco:CharacterString />
              </gmd:facsimile>
            </gmd:CI_Telephone>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:deliveryPoint gco:nilReason=\"missing\">
                <gco:CharacterString />
              </gmd:deliveryPoint>
              <gmd:city gco:nilReason=\"missing\">
                <gco:CharacterString />
              </gmd:city>
              <gmd:administrativeArea gco:nilReason=\"missing\">
                <gco:CharacterString />
              </gmd:administrativeArea>
              <gmd:postalCode gco:nilReason=\"missing\">
                <gco:CharacterString />
              </gmd:postalCode>
              <gmd:country gco:nilReason=\"missing\">
                <gco:CharacterString />
              </gmd:country>
              <gmd:electronicMailAddress>
                <gco:CharacterString>contact@contact.fr</gco:CharacterString>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode\" codeListValue=\"originator\" />
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gmd:contact>
  <gmd:dateStamp>
    <gco:DateTime>2012-04-11T13:48:56</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:metadataStandardVersion>
    <gco:CharacterString>1.0</gco:CharacterString>
  </gmd:metadataStandardVersion>
  <gmd:referenceSystemInfo>
    <gmd:MD_ReferenceSystem>
      <gmd:referenceSystemIdentifier>
        <gmd:RS_Identifier>
          <gmd:code>
            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>
          </gmd:code>
          <gmd:codeSpace>
            <gco:CharacterString>EPSG</gco:CharacterString>
          </gmd:codeSpace>
          <gmd:version>
            <gco:CharacterString>7.4</gco:CharacterString>
          </gmd:version>
        </gmd:RS_Identifier>
      </gmd:referenceSystemIdentifier>
    </gmd:MD_ReferenceSystem>
  </gmd:referenceSystemInfo>
  <gmd:identificationInfo>
    <gmd:MD_DataIdentification>
      <gmd:citation>
        <gmd:CI_Citation>
          <gmd:title>
            <gco:CharacterString>departements</gco:CharacterString>
          </gmd:title>
          <gmd:date>
            <gmd:CI_Date>
              <gmd:date>
                <gco:DateTime>2009-03-06T00:00:00</gco:DateTime>
              </gmd:date>
              <gmd:dateType>
                <gmd:CI_DateTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode\" codeListValue=\"publication\" />
              </gmd:dateType>
            </gmd:CI_Date>
          </gmd:date>
          <gmd:citedResponsibleParty>
            <gmd:CI_ResponsibleParty>
              <gmd:individualName>
                <gco:CharacterString>DUPONT Patrick</gco:CharacterString>
              </gmd:individualName>
              <gmd:organisationName>
                <gco:CharacterString>DIREN</gco:CharacterString>
              </gmd:organisationName>
              <gmd:role>
                <gmd:CI_RoleCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode\" codeListValue=\"user\" />
              </gmd:role>
            </gmd:CI_ResponsibleParty>
          </gmd:citedResponsibleParty>
          <gmd:identifier>
            <gmd:MD_Identifier>
              <gmd:code>
                <gco:CharacterString>https://catalogue.prodige.internal/geonetwork/srv/3c892ef0-0a6d-11de-ad6b-00104b7907b4</gco:CharacterString>
              </gmd:code>
            </gmd:MD_Identifier>
          </gmd:identifier>
        </gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract>
        <gco:CharacterString>departements</gco:CharacterString>
      </gmd:abstract>
      <gmd:language>
        <gco:CharacterString>fre</gco:CharacterString>
      </gmd:language>
      <gmd:characterSet>
        <gmd:MD_CharacterSetCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\" />
      </gmd:characterSet>
      <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty>
          <gmd:individualName>
            <gco:CharacterString>-- Contact --</gco:CharacterString>
          </gmd:individualName>
          <gmd:organisationName>
            <gco:CharacterString>-- Organisation --</gco:CharacterString>
          </gmd:organisationName>
          <gmd:contactInfo>
            <gmd:CI_Contact>
              <gmd:phone>
                <gmd:CI_Telephone>
                  <gmd:voice>
                    <gco:CharacterString>33 (0) 2 32 00 00 00</gco:CharacterString>
                  </gmd:voice>
                </gmd:CI_Telephone>
              </gmd:phone>
              <gmd:address>
                <gmd:CI_Address>
                  <gmd:electronicMailAddress>
                    <gco:CharacterString>contact@contact.fr</gco:CharacterString>
                  </gmd:electronicMailAddress>
                </gmd:CI_Address>
              </gmd:address>
            </gmd:CI_Contact>
          </gmd:contactInfo>
          <gmd:role>
            <gmd:CI_RoleCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_RoleCode\" codeListValue=\"distributor\" />
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>France</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"place\" />
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>Dénominations géographiques</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>Répartition de la population — démographie</gco:CharacterString>
          </gmd:keyword>
          <gmd:keyword>
            <gco:CharacterString>Unités administratives</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"theme\" />
          </gmd:type>
          <gmd:thesaurusName>
            <gmd:CI_Citation>
              <gmd:title>
                <gco:CharacterString>GEMET - INSPIRE themes, version 1.0</gco:CharacterString>
              </gmd:title>
              <gmd:date>
                <gmd:CI_Date>
                  <gmd:date>
                    <gco:Date>2008-06-01</gco:Date>
                  </gmd:date>
                  <gmd:dateType>
                    <gmd:CI_DateTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#CI_DateTypeCode\" codeListValue=\"publication\" />
                  </gmd:dateType>
                </gmd:CI_Date>
              </gmd:date>
            </gmd:CI_Citation>
          </gmd:thesaurusName>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:extent>
        <gmd:EX_Extent>
          <gmd:description>
            <gco:CharacterString>France</gco:CharacterString>
          </gmd:description>
          <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
              <gmd:westBoundLongitude>
                <gco:Decimal>-4.31</gco:Decimal>
              </gmd:westBoundLongitude>
              <gmd:eastBoundLongitude>
                <gco:Decimal>8.40</gco:Decimal>
              </gmd:eastBoundLongitude>
              <gmd:southBoundLatitude>
                <gco:Decimal>41.24</gco:Decimal>
              </gmd:southBoundLatitude>
              <gmd:northBoundLatitude>
                <gco:Decimal>51</gco:Decimal>
              </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
          </gmd:geographicElement>
        </gmd:EX_Extent>
      </gmd:extent>
      <gmd:descriptiveKeywords xlink:href=\"https://catalogue.prodige.internal/geonetwork/srv/fre/xml.keyword.get?thesaurus=external.theme.prodige&amp;id=https://catalogue.prodige.internal/geonetwork//thesaurus/theme/domaine%23sousdomaine_217&amp;multiple=true&amp;lang=fre\" xlink:show=\"replace\" />
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints>
          <gmd:useLimitation>
            <gco:CharacterString>(Non) Usage et diffusion libre</gco:CharacterString>
          </gmd:useLimitation>
          <gmd:accessConstraints>
            <gmd:MD_RestrictionCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"restricted\" />
          </gmd:accessConstraints>
          <gmd:useConstraints>
            <gmd:MD_RestrictionCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_RestrictionCode\" codeListValue=\"copyright\" />
          </gmd:useConstraints>
          <gmd:otherConstraints gco:nilReason=\"missing\">
            <gco:CharacterString />
          </gmd:otherConstraints>
        </gmd:MD_LegalConstraints>
      </gmd:resourceConstraints>
      <gmd:resourceConstraints>
        <gmd:MD_SecurityConstraints>
          <gmd:classification>
            <gmd:MD_ClassificationCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_ClassificationCode\" codeListValue=\"unclassified\" />
          </gmd:classification>
          <gmd:userNote gco:nilReason=\"missing\">
            <gco:CharacterString />
          </gmd:userNote>
        </gmd:MD_SecurityConstraints>
      </gmd:resourceConstraints>
      <gmd:spatialRepresentationType>
        <gmd:MD_SpatialRepresentationTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_SpatialRepresentationTypeCode\" codeListValue=\"vector\" />
      </gmd:spatialRepresentationType>
      <gmd:spatialResolution>
        <gmd:MD_Resolution>
          <gmd:equivalentScale>
            <gmd:MD_RepresentativeFraction>
              <gmd:denominator>
                <gco:Integer>10000000</gco:Integer>
              </gmd:denominator>
            </gmd:MD_RepresentativeFraction>
          </gmd:equivalentScale>
        </gmd:MD_Resolution>
      </gmd:spatialResolution>
      <gmd:spatialResolution>
        <gmd:MD_Resolution>
          <gmd:distance />
        </gmd:MD_Resolution>
      </gmd:spatialResolution>
      <gmd:topicCategory>
        <gmd:MD_TopicCategoryCode>boundaries</gmd:MD_TopicCategoryCode>
      </gmd:topicCategory>
      <gmd:supplementalInformation gco:nilReason=\"missing\">
        <gco:CharacterString />
      </gmd:supplementalInformation>
    </gmd:MD_DataIdentification>
  </gmd:identificationInfo>
  <gmd:dataQualityInfo>
    <gmd:DQ_DataQuality>
      <gmd:scope>
        <gmd:DQ_Scope>
          <gmd:level>
            <gmd:MD_ScopeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_ScopeCode\" codeListValue=\"dataset\" />
          </gmd:level>
        </gmd:DQ_Scope>
      </gmd:scope>
      <gmd:lineage>
        <gmd:LI_Lineage>
          <gmd:statement>
            <gco:CharacterString>departements</gco:CharacterString>
          </gmd:statement>
        </gmd:LI_Lineage>
      </gmd:lineage>
    </gmd:DQ_DataQuality>
  </gmd:dataQualityInfo>
</gmd:MD_Metadata>', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, 'gmd:MD_Metadata', NULL, 1, 217, NULL, 0, 34, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (10, '617af7f0-0a6f-11de-ad6b-00104b7907b4', 'iso19139', 'n', 'n', '2009-03-06T17:53:56', '2012-04-11T13:53:40', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gfc=\"http://www.isotc211.org/2005/gfc\" xmlns:srv=\"http://www.isotc211.org/2005/srv\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:gml=\"http://www.opengis.net/gml\" xsi:schemaLocation=\" http://www.isotc211.org/2005/srv http://schemas.opengis.net/iso/19139/20060504/srv/srv.xsd\">
  <gmd:fileIdentifier>
    <gco:CharacterString>617af7f0-0a6f-11de-ad6b-00104b7907b4</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>

  <gco:CharacterString>fre</gco:CharacterString></gmd:language>
  <gmd:characterSet>
    <gmd:MD_CharacterSetCode codeList=\"./resources/codeList.xml#MD_CharacterSetCode\" codeListValue=\"utf8\"/>
  </gmd:characterSet>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_ScopeCode\" codeListValue=\"service\"/>
  </gmd:hierarchyLevel>
  <gmd:hierarchyLevelName>
    <gco:CharacterString>service</gco:CharacterString>
  </gmd:hierarchyLevelName>
  <gmd:contact>
    <gmd:CI_ResponsibleParty>
      <gmd:individualName>
        <gco:CharacterString>-- Contact --</gco:CharacterString>
      </gmd:individualName>
      <gmd:organisationName>
        <gco:CharacterString>-- Organisation --</gco:CharacterString>
      </gmd:organisationName>
      <gmd:positionName gco:nilReason=\"missing\">
        <gco:CharacterString/>
      </gmd:positionName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone>
              <gmd:voice gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:voice>
              <gmd:facsimile gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:facsimile>
            </gmd:CI_Telephone>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:deliveryPoint gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:deliveryPoint>
              <gmd:city gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:city>
              <gmd:administrativeArea gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:administrativeArea>
              <gmd:postalCode gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:postalCode>
              <gmd:country gco:nilReason=\"missing\">
                <gco:CharacterString/>
              </gmd:country>
              <gmd:electronicMailAddress>
                <gco:CharacterString>contact@contact.fr</gco:CharacterString>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"pointOfContact\"/>
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gmd:contact>
  <gmd:dateStamp>
    <gco:DateTime>2012-04-11T13:53:40</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:metadataStandardVersion>
    <gco:CharacterString>1.0</gco:CharacterString>
  </gmd:metadataStandardVersion>
  <gmd:referenceSystemInfo><gmd:MD_ReferenceSystem><gmd:referenceSystemIdentifier><gmd:RS_Identifier><gmd:code><gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString></gmd:code><gmd:codeSpace><gco:CharacterString>EPSG</gco:CharacterString></gmd:codeSpace><gmd:version><gco:CharacterString>7.4</gco:CharacterString></gmd:version></gmd:RS_Identifier></gmd:referenceSystemIdentifier></gmd:MD_ReferenceSystem></gmd:referenceSystemInfo><gmd:identificationInfo>
    <srv:SV_ServiceIdentification>
      <gmd:citation>
        <gmd:CI_Citation>
          <gmd:title>
            <gco:CharacterString>limites_administratives</gco:CharacterString>
          </gmd:title>
          <gmd:date>
            <gmd:CI_Date>
              <gmd:date>
                <gco:DateTime>2009-03-06T00:00:00</gco:DateTime>
              </gmd:date>
              <gmd:dateType>
                <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_DateTypeCode\" codeListValue=\"publication\"/>
              </gmd:dateType>
            </gmd:CI_Date>
          </gmd:date>

        <gmd:identifier>
                  				<gmd:MD_Identifier>
                     				<gmd:code>
                        				<gco:CharacterString>https://catalogue.prodige.internal/geonetwork/srv/617af7f0-0a6f-11de-ad6b-00104b7907b4</gco:CharacterString>
                    				</gmd:code>
                  				</gmd:MD_Identifier>
								</gmd:identifier></gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract>
        <gco:CharacterString>limites_administratives</gco:CharacterString>
      </gmd:abstract>
      <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty>
          <gmd:individualName>
            <gco:CharacterString>-- Contact --</gco:CharacterString>
          </gmd:individualName>
          <gmd:organisationName>
            <gco:CharacterString>-- Organisation --</gco:CharacterString>
          </gmd:organisationName>
          <gmd:contactInfo>
            <gmd:CI_Contact>
              <gmd:phone>
                <gmd:CI_Telephone>
                  <gmd:voice gco:nilReason=\"missing\">
                    <gco:CharacterString/>
                  </gmd:voice>
                </gmd:CI_Telephone>
              </gmd:phone>
              <gmd:address>
                <gmd:CI_Address>
                  <gmd:electronicMailAddress>
                    <gco:CharacterString>contact@contact.fr</gco:CharacterString>
                  </gmd:electronicMailAddress>
                </gmd:CI_Address>
              </gmd:address>
            </gmd:CI_Contact>
          </gmd:contactInfo>
          <gmd:role>
            <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"pointOfContact\"/>
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>

      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>France</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode\" codeListValue=\"place\"/>
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>infoMapAccessService</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode\" codeListValue=\"theme\"/>
          </gmd:type>
          <gmd:thesaurusName>
            <gmd:CI_Citation>
              <gmd:title>
                <gco:CharacterString>INSPIRE Service taxonomy</gco:CharacterString>
              </gmd:title>
              <gmd:date>
                <gmd:CI_Date>
                  <gmd:date>
                    <gco:Date>2012-04-11</gco:Date>
                  </gmd:date>
                  <gmd:dateType>
                    <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#CI_DateTypeCode\" codeListValue=\"publication\"/>
                  </gmd:dateType>
                </gmd:CI_Date>
              </gmd:date>
            </gmd:CI_Citation>
          </gmd:thesaurusName>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:descriptiveKeywords xlink:href=\"https://catalogue.prodige.internal/geonetwork/srv/fre/xml.keyword.get?thesaurus=external.theme.prodige&amp;id=https://catalogue.prodige.internal/geonetwork//thesaurus/theme/domaine%23sousdomaine_217&amp;multiple=true&amp;lang=fre\" xlink:show=\"replace\"/><gmd:resourceConstraints>
        <gmd:MD_LegalConstraints>
          <gmd:accessConstraints>
            <gmd:MD_RestrictionCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_RestrictionCode\" codeListValue=\"\"/>
          </gmd:accessConstraints>
          <gmd:useConstraints>
            <gmd:MD_RestrictionCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_RestrictionCode\" codeListValue=\"\"/>
          </gmd:useConstraints>
          <gmd:otherConstraints gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:otherConstraints>
        </gmd:MD_LegalConstraints>
      </gmd:resourceConstraints>
      <gmd:resourceConstraints>
        <gmd:MD_SecurityConstraints>
          <gmd:classification>
            <gmd:MD_ClassificationCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_ClassificationCode\" codeListValue=\"unclassified\"/>
          </gmd:classification>
          <gmd:userNote gco:nilReason=\"missing\">
            <gco:CharacterString/>
          </gmd:userNote>
        </gmd:MD_SecurityConstraints>
      </gmd:resourceConstraints>
      <srv:serviceType>
        <gco:LocalName>invoke</gco:LocalName>
      </srv:serviceType>
      <srv:extent>
        <gmd:EX_Extent>
          <gmd:description>
            <gco:CharacterString>FRANCE</gco:CharacterString>
          </gmd:description>
          <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
              <gmd:westBoundLongitude>
                <gco:Decimal>-4.31</gco:Decimal>
              </gmd:westBoundLongitude>
              <gmd:eastBoundLongitude>
                <gco:Decimal>8.40</gco:Decimal>
              </gmd:eastBoundLongitude>
              <gmd:southBoundLatitude>
                <gco:Decimal>41.24</gco:Decimal>
              </gmd:southBoundLatitude>
              <gmd:northBoundLatitude>
                <gco:Decimal>51</gco:Decimal>
              </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
          </gmd:geographicElement>
        </gmd:EX_Extent>
      </srv:extent>
      <srv:couplingType>
        <srv:SV_CouplingType codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#SV_CouplingType\" codeListValue=\"tight\"/>
      </srv:couplingType>
      <srv:containsOperations>
        <srv:SV_OperationMetadata>
          <srv:operationName>
            <gco:CharacterString>Accès à la carte</gco:CharacterString>
          </srv:operationName>
          <srv:DCP>
            <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
          </srv:DCP>

        </srv:SV_OperationMetadata>
      </srv:containsOperations>
    <srv:containsOperations>
                    <srv:SV_OperationMetadata>
                      <srv:operationName>
                        <gco:CharacterString>Accès à la carte</gco:CharacterString>
                      </srv:operationName>
                      <srv:DCP>
                        <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/>
                      </srv:DCP>
                      <srv:connectPoint>
                        <gmd:CI_OnlineResource>
                          <gmd:linkage>
                            <gmd:URL>https://catalogue.prodige.internal/geosource/consultation?id=10</gmd:URL>
                          </gmd:linkage>
                          <gmd:protocol>
                            <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                          </gmd:protocol>
                        </gmd:CI_OnlineResource>
                      </srv:connectPoint>
                    </srv:SV_OperationMetadata>
                  </srv:containsOperations><srv:containsOperations><srv:SV_OperationMetadata><srv:operationName><gco:CharacterString>Accès au fichier contexte OWS de la carte</gco:CharacterString></srv:operationName><srv:DCP><srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\"/></srv:DCP><srv:connectPoint><gmd:CI_OnlineResource><gmd:linkage><gmd:URL>https://carto.prodige.internal/frontcarto/context/getOws/10</gmd:URL></gmd:linkage><gmd:protocol><gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString></gmd:protocol></gmd:CI_OnlineResource></srv:connectPoint></srv:SV_OperationMetadata></srv:containsOperations></srv:SV_ServiceIdentification>
  </gmd:identificationInfo>
  <gmd:distributionInfo>
                   <gmd:MD_Distribution>
                    <gmd:transferOptions>
                      <gmd:MD_DigitalTransferOptions>
                        <gmd:onLine>
                          <gmd:CI_OnlineResource>
                            <gmd:linkage>
                              <gmd:URL>https://catalogue.prodige.internal/geosource/consultation?id=10</gmd:URL>
                            </gmd:linkage>
                          </gmd:CI_OnlineResource>
                        </gmd:onLine>
                      <gmd:onLine><gmd:CI_OnlineResource><gmd:linkage><gmd:URL>https://carto.prodige.internal/frontcarto/context/getOws/10</gmd:URL></gmd:linkage><gmd:protocol><gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString></gmd:protocol><gmd:name><gco:CharacterString>Accès au fichier contexte OWS de la carte</gco:CharacterString></gmd:name><gmd:description><gco:CharacterString>Accès au fichier contexte OWS de la carte</gco:CharacterString></gmd:description></gmd:CI_OnlineResource></gmd:onLine></gmd:MD_DigitalTransferOptions>
                    </gmd:transferOptions>
                  </gmd:MD_Distribution>
                  </gmd:distributionInfo><gmd:dataQualityInfo>
    <gmd:DQ_DataQuality>
      <gmd:scope>
        <gmd:DQ_Scope>
          <gmd:level>
            <gmd:MD_ScopeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/Codelist/ML_gmxCodelists.xml#MD_ScopeCode\" codeListValue=\"service\"/>
          </gmd:level>
          <gmd:levelDescription>
            <gmd:MD_ScopeDescription>
              <gmd:other>
                <gco:CharacterString>Carte interactive</gco:CharacterString>
              </gmd:other>
            </gmd:MD_ScopeDescription>
          </gmd:levelDescription>
        </gmd:DQ_Scope>
      </gmd:scope>
      <gmd:lineage>
        <gmd:LI_Lineage>
          <gmd:statement>
            <gco:CharacterString>Carte interactive représentant les départements</gco:CharacterString>
          </gmd:statement>
        </gmd:LI_Lineage>
      </gmd:lineage>
    </gmd:DQ_DataQuality>
  </gmd:dataQualityInfo>
</gmd:MD_Metadata>
', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, 'gmd:MD_Metadata', NULL, 1, 217, NULL, 0, 21, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (40786, '8c018199-b0f9-41f9-83b0-b2b076e5bba1', 'iso19139', 'y', 'n', '2019-05-24T17:25:18', '2019-05-24T17:25:18', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:gfc=\"http://www.isotc211.org/2005/gfc\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:srv=\"http://www.isotc211.org/2005/srv\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.isotc211.org/2005/gmd http://www.isotc211.org/2005/gmd/gmd.xsd http://www.isotc211.org/2005/gmx http://www.isotc211.org/2005/gmx/gmx.xsd http://www.isotc211.org/2005/srv http://schemas.opengis.net/iso/19139/20060504/srv/srv.xsd\">
  <gmd:fileIdentifier>
    <gco:CharacterString>8c018199-b0f9-41f9-83b0-b2b076e5bba1</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>
    <gco:CharacterString>fre</gco:CharacterString>
  </gmd:language>
  <gmd:characterSet>
    <gmd:MD_CharacterSetCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_CharacterSetCode\" codeListValue=\"utf8\" />
  </gmd:characterSet>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode\" codeListValue=\"service\" />
  </gmd:hierarchyLevel>
  <gmd:hierarchyLevelName>
    <gco:CharacterString>service</gco:CharacterString>
  </gmd:hierarchyLevelName>
  <gmd:contact>
    <gmd:CI_ResponsibleParty>
      <gmd:individualName gco:nilReason=\"missing\">
        <gco:CharacterString />
      </gmd:individualName>
      <gmd:organisationName>
        <gco:CharacterString>A remplir</gco:CharacterString>
      </gmd:organisationName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:phone>
            <gmd:CI_Telephone>
              <gmd:voice gco:nilReason=\"missing\">
                <gco:CharacterString />
              </gmd:voice>
            </gmd:CI_Telephone>
          </gmd:phone>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:electronicMailAddress>
                <gco:CharacterString>remplir@</gco:CharacterString>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"pointOfContact\" />
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gmd:contact>
  <gmd:dateStamp>
    <gco:DateTime>2018-11-15T10:54:31</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:metadataStandardVersion>
    <gco:CharacterString>1.0</gco:CharacterString>
  </gmd:metadataStandardVersion>
  <gmd:referenceSystemInfo>
    <gmd:MD_ReferenceSystem>
      <gmd:referenceSystemIdentifier>
        <gmd:RS_Identifier>
          <gmd:code>
            <gco:CharacterString>RGF93 / Lambert-93 (EPSG:2154)</gco:CharacterString>
          </gmd:code>
          <gmd:codeSpace>
            <gco:CharacterString>EPSG</gco:CharacterString>
          </gmd:codeSpace>
          <gmd:version>
            <gco:CharacterString>7.9</gco:CharacterString>
          </gmd:version>
        </gmd:RS_Identifier>
      </gmd:referenceSystemIdentifier>
    </gmd:MD_ReferenceSystem>
  </gmd:referenceSystemInfo>
  <gmd:identificationInfo>
    <srv:SV_ServiceIdentification>
      <srv:containsOperations>
        <srv:SV_OperationMetadata>
          <srv:operationName>
            <gco:CharacterString>Accès au graphe</gco:CharacterString>
          </srv:operationName>
          <srv:DCP>
            <srv:DCPList codeList=\"http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList\" codeListValue=\"WebServices\" />
          </srv:DCP>
          <srv:connectPoint>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL />
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
              </gmd:protocol>
            </gmd:CI_OnlineResource>
          </srv:connectPoint>
        </srv:SV_OperationMetadata>
      </srv:containsOperations>
      <gmd:citation>
        <gmd:CI_Citation>
          <gmd:title>
            <gco:CharacterString>Modèle de métadonnées de graphe</gco:CharacterString>
          </gmd:title>
          <gmd:date>
            <gmd:CI_Date>
              <gmd:date>
                <gco:DateTime>2018-11-15T09:56:00</gco:DateTime>
              </gmd:date>
              <gmd:dateType>
                <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode\" codeListValue=\"publication\" />
              </gmd:dateType>
            </gmd:CI_Date>
          </gmd:date>
          <gmd:identifier>
            <gmd:MD_Identifier>
              <gmd:code>
                <gco:CharacterString />
              </gmd:code>
            </gmd:MD_Identifier>
          </gmd:identifier>
        </gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract>
        <gco:CharacterString>A remplir</gco:CharacterString>
      </gmd:abstract>
      <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty>
          <gmd:individualName gco:nilReason=\"missing\">
            <gco:CharacterString />
          </gmd:individualName>
          <gmd:organisationName>
            <gco:CharacterString>A remplir</gco:CharacterString>
          </gmd:organisationName>
          <gmd:contactInfo>
            <gmd:CI_Contact>
              <gmd:phone>
                <gmd:CI_Telephone>
                  <gmd:voice gco:nilReason=\"missing\">
                    <gco:CharacterString />
                  </gmd:voice>
                </gmd:CI_Telephone>
              </gmd:phone>
              <gmd:address>
                <gmd:CI_Address>
                  <gmd:electronicMailAddress>
                    <gco:CharacterString>remplir@</gco:CharacterString>
                  </gmd:electronicMailAddress>
                </gmd:CI_Address>
              </gmd:address>
            </gmd:CI_Contact>
          </gmd:contactInfo>
          <gmd:role>
            <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"pointOfContact\" />
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>infoChartAccessService</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://www.isotc211.org/2005/resources/codeList.xml#MD_KeywordTypeCode\" codeListValue=\"theme\" />
          </gmd:type>
          <gmd:thesaurusName>
            <gmd:CI_Citation>
              <gmd:title>
                <gco:CharacterString>INSPIRE Service taxonomy</gco:CharacterString>
              </gmd:title>
              <gmd:date>
                <gmd:CI_Date>
                  <gmd:date>
                    <gco:Date>2010-04-22</gco:Date>
                  </gmd:date>
                  <gmd:dateType>
                    <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode\" codeListValue=\"publication\" />
                  </gmd:dateType>
                </gmd:CI_Date>
              </gmd:date>
              <gmd:identifier>
                <gmd:MD_Identifier>
                  <gmd:code>
                    <gmx:Anchor xlink:href=\"https://www.application-prodige-mig.fr/geonetwork/srv/fre/thesaurus.download?ref=external.theme.inspire-service-taxonomy\">geonetwork.thesaurus.external.theme.inspire-service-taxonomy</gmx:Anchor>
                  </gmd:code>
                </gmd:MD_Identifier>
              </gmd:identifier>
            </gmd:CI_Citation>
          </gmd:thesaurusName>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints>
          <gmd:useLimitation>
            <gco:CharacterString>Utilisation libre sous réserve de mentionner la source (a minima le nom du producteur) et la date de sa dernière mise à jour</gco:CharacterString>
          </gmd:useLimitation>
          <gmd:accessConstraints>
            <gmd:MD_RestrictionCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_RestrictionCode\" codeListValue=\"otherRestrictions\" />
          </gmd:accessConstraints>
          <gmd:otherConstraints>
            <gco:CharacterString>Pas de restriction d’accès public selon INSPIRE</gco:CharacterString>
          </gmd:otherConstraints>
        </gmd:MD_LegalConstraints>
      </gmd:resourceConstraints>
      <srv:serviceType>
        <gco:LocalName>invoke</gco:LocalName>
      </srv:serviceType>
      <srv:extent>
        <gmd:EX_Extent xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">
          <gmd:description>
            <gco:CharacterString>FRANCE METROPOLITAINE</gco:CharacterString>
          </gmd:description>
          <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
              <gmd:westBoundLongitude>
                <gco:Decimal>-5</gco:Decimal>
              </gmd:westBoundLongitude>
              <gmd:eastBoundLongitude>
                <gco:Decimal>10</gco:Decimal>
              </gmd:eastBoundLongitude>
              <gmd:southBoundLatitude>
                <gco:Decimal>42</gco:Decimal>
              </gmd:southBoundLatitude>
              <gmd:northBoundLatitude>
                <gco:Decimal>51</gco:Decimal>
              </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
          </gmd:geographicElement>
        </gmd:EX_Extent>
      </srv:extent>
    </srv:SV_ServiceIdentification>
  </gmd:identificationInfo>
  <gmd:distributionInfo>
    <gmd:MD_Distribution>
      <gmd:distributionFormat>
        <gmd:MD_Format>
          <gmd:name>
            <gco:CharacterString>UTF-8</gco:CharacterString>
          </gmd:name>
          <gmd:version gco:nilReason=\"missing\">
            <gco:CharacterString />
          </gmd:version>
        </gmd:MD_Format>
      </gmd:distributionFormat>
      <gmd:distributor />
    </gmd:MD_Distribution>
  </gmd:distributionInfo>
</gmd:MD_Metadata>', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, 'gmd:MD_Metadata', NULL, 40784, NULL, NULL, 0, 0, 0, NULL, NULL)");
        $this->addSql("INSERT INTO public.metadata (id, uuid, schemaid, istemplate, isharvested, createdate, changedate, data, source, title, root, harvestuuid, owner, groupowner, harvesturi, rating, popularity, displayorder, doctype, extra) VALUES (40787, '0f9a5c54-0a56-436e-b33f-a24cf270097b', 'iso19139', 'y', 'n', '2019-05-24T17:25:25', '2019-05-24T17:25:25', '<gmd:MD_Metadata xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:srv=\"http://www.isotc211.org/2005/srv\" xmlns:gfc=\"http://www.isotc211.org/2005/gfc\" xmlns:gco=\"http://www.isotc211.org/2005/gco\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:gmx=\"http://www.isotc211.org/2005/gmx\" xmlns:gts=\"http://www.isotc211.org/2005/gts\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.isotc211.org/2005/gmd http://www.isotc211.org/2005/gmd/gmd.xsd http://www.isotc211.org/2005/gmx http://www.isotc211.org/2005/gmx/gmx.xsd http://www.isotc211.org/2005/srv http://schemas.opengis.net/iso/19139/20060504/srv/srv.xsd\">
  <gmd:fileIdentifier>
    <gco:CharacterString>0f9a5c54-0a56-436e-b33f-a24cf270097b</gco:CharacterString>
  </gmd:fileIdentifier>
  <gmd:language>
    <gmd:LanguageCode codeList=\"http://www.loc.gov/standards/iso639-2/\" codeListValue=\"fre\" />
  </gmd:language>
  <gmd:characterSet>
    <gmd:MD_CharacterSetCode codeListValue=\"utf8\" codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_CharacterSetCode\" />
  </gmd:characterSet>
  <gmd:hierarchyLevel>
    <gmd:MD_ScopeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode\" codeListValue=\"nonGeographicDataset\" />
  </gmd:hierarchyLevel>
  <gmd:hierarchyLevelName>
    <gco:CharacterString>Série de données non géolocalisables</gco:CharacterString>
  </gmd:hierarchyLevelName>
  <gmd:contact>
    <gmd:CI_ResponsibleParty>
      <gmd:organisationName>
        <gco:CharacterString>-- Nom du point de contact des métadonnées --</gco:CharacterString>
      </gmd:organisationName>
      <gmd:contactInfo>
        <gmd:CI_Contact>
          <gmd:address>
            <gmd:CI_Address>
              <gmd:electronicMailAddress>
                <gco:CharacterString>-- Adresse email --</gco:CharacterString>
              </gmd:electronicMailAddress>
            </gmd:CI_Address>
          </gmd:address>
        </gmd:CI_Contact>
      </gmd:contactInfo>
      <gmd:role>
        <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"pointOfContact\" />
      </gmd:role>
    </gmd:CI_ResponsibleParty>
  </gmd:contact>
  <gmd:dateStamp>
    <gco:DateTime>2017-12-11T12:07:56</gco:DateTime>
  </gmd:dateStamp>
  <gmd:metadataStandardName>
    <gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>
  </gmd:metadataStandardName>
  <gmd:metadataStandardVersion>
    <gco:CharacterString>1.0</gco:CharacterString>
  </gmd:metadataStandardVersion>
  <gmd:identificationInfo>
    <gmd:MD_DataIdentification>
      <gmd:citation>
        <gmd:CI_Citation>
          <gmd:title>
            <gco:CharacterString>Modèle pour la saisie d''une série de données non géolocalisables</gco:CharacterString>
          </gmd:title>
          <gmd:date>
            <gmd:CI_Date>
              <gmd:date>
                <gco:Date>2011-01-01</gco:Date>
              </gmd:date>
              <gmd:dateType>
                <gmd:CI_DateTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode\" codeListValue=\"creation\" />
              </gmd:dateType>
            </gmd:CI_Date>
          </gmd:date>
        </gmd:CI_Citation>
      </gmd:citation>
      <gmd:abstract>
        <gco:CharacterString>-- Court résumé explicatif du contenu de la ressource --</gco:CharacterString>
      </gmd:abstract>
      <gmd:pointOfContact>
        <gmd:CI_ResponsibleParty>
          <gmd:organisationName>
            <gco:CharacterString>-- Nom de l''organisation responsable de la série de données --</gco:CharacterString>
          </gmd:organisationName>
          <gmd:contactInfo>
            <gmd:CI_Contact>
              <gmd:address>
                <gmd:CI_Address>
                  <gmd:electronicMailAddress>
                    <gco:CharacterString>-- Adresse email --</gco:CharacterString>
                  </gmd:electronicMailAddress>
                </gmd:CI_Address>
              </gmd:address>
            </gmd:CI_Contact>
          </gmd:contactInfo>
          <gmd:role>
            <gmd:CI_RoleCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode\" codeListValue=\"publisher\" />
          </gmd:role>
        </gmd:CI_ResponsibleParty>
      </gmd:pointOfContact>
      <gmd:descriptiveKeywords>
        <gmd:MD_Keywords>
          <gmd:keyword>
            <gco:CharacterString>-- Mots-clé --</gco:CharacterString>
          </gmd:keyword>
          <gmd:type>
            <gmd:MD_KeywordTypeCode codeList=\"http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_KeywordTypeCode\" codeListValue=\"theme\" />
          </gmd:type>
        </gmd:MD_Keywords>
      </gmd:descriptiveKeywords>
      <gmd:language>
        <gco:CharacterString>fre</gco:CharacterString>
      </gmd:language>
      <gmd:extent>
        <gmd:EX_Extent xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">
          <gmd:geographicElement>
            <gmd:EX_GeographicBoundingBox>
              <gmd:westBoundLongitude>
                <gco:Decimal>-5</gco:Decimal>
              </gmd:westBoundLongitude>
              <gmd:eastBoundLongitude>
                <gco:Decimal>10</gco:Decimal>
              </gmd:eastBoundLongitude>
              <gmd:southBoundLatitude>
                <gco:Decimal>42</gco:Decimal>
              </gmd:southBoundLatitude>
              <gmd:northBoundLatitude>
                <gco:Decimal>51</gco:Decimal>
              </gmd:northBoundLatitude>
            </gmd:EX_GeographicBoundingBox>
          </gmd:geographicElement>
        </gmd:EX_Extent>
      </gmd:extent>
    </gmd:MD_DataIdentification>
  </gmd:identificationInfo>
  <gmd:distributionInfo>
    <gmd:MD_Distribution>
      <gmd:distributionFormat>
        <gmd:MD_Format>
          <gmd:name gco:nilReason=\"missing\">
            <gco:CharacterString />
          </gmd:name>
          <gmd:version gco:nilReason=\"missing\">
            <gco:CharacterString />
          </gmd:version>
        </gmd:MD_Format>
      </gmd:distributionFormat>
      <gmd:transferOptions>
        <gmd:MD_DigitalTransferOptions />
      </gmd:transferOptions>
    </gmd:MD_Distribution>
  </gmd:distributionInfo>
</gmd:MD_Metadata>', '7fc45be3-9aba-4198-920c-b8737112d522', NULL, 'gmd:MD_Metadata', NULL, 40784, NULL, NULL, 0, 0, 0, NULL, NULL)");

        $this->addSql("SELECT pg_catalog.setval('catalogue.standards_fournisseur_fournisseur_id_seq', 1, true)");
        $this->addSql("SELECT pg_catalog.setval('catalogue.standards_standard_standard_id_seq', 1, false)");

        $this->addSql("INSERT INTO catalogue.trt_autorise_objet (pk_trt_autorise_objet, trtautoobjet_fk_trt_id, trtautoobjet_fk_obj_type_id) VALUES (1, 1, 1)");
        $this->addSql("INSERT INTO catalogue.trt_autorise_objet (pk_trt_autorise_objet, trtautoobjet_fk_trt_id, trtautoobjet_fk_obj_type_id) VALUES (2, 2, 1)");
        $this->addSql("INSERT INTO catalogue.trt_autorise_objet (pk_trt_autorise_objet, trtautoobjet_fk_trt_id, trtautoobjet_fk_obj_type_id) VALUES (3, 2, 2)");
        $this->addSql("INSERT INTO catalogue.trt_autorise_objet (pk_trt_autorise_objet, trtautoobjet_fk_trt_id, trtautoobjet_fk_obj_type_id) VALUES (13, 7, 5)");
        $this->addSql("INSERT INTO catalogue.trt_autorise_objet (pk_trt_autorise_objet, trtautoobjet_fk_trt_id, trtautoobjet_fk_obj_type_id) VALUES (15, 9, 1)");
        $this->addSql("INSERT INTO catalogue.trt_autorise_objet (pk_trt_autorise_objet, trtautoobjet_fk_trt_id, trtautoobjet_fk_obj_type_id) VALUES (16, 10, 1)");
        $this->addSql("INSERT INTO catalogue.trt_autorise_objet (pk_trt_autorise_objet, trtautoobjet_fk_trt_id, trtautoobjet_fk_obj_type_id) VALUES (17, 11, 1)");

        $this->addSql("INSERT INTO public.address (id, address, city, state, zip, country) VALUES (2, NULL, NULL, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.address (id, address, city, state, zip, country) VALUES (1, NULL, NULL, NULL, NULL, NULL)");
        $this->addSql("INSERT INTO public.address (id, address, city, state, zip, country) VALUES (3, NULL, NULL, NULL, NULL, NULL)");

        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('ara', 'العربية', 'n', 'n')");
        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('cat', 'Català', 'n', 'n')");
        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('chi', '中文', 'n', 'n')");
        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('dut', 'Nederlands', 'y', 'n')");
        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('eng', 'English', 'y', 'y')");
        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('fin', 'Suomi', 'y', 'n')");
        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('fre', 'Français', 'y', 'n')");
        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('ger', 'Deutsch', 'y', 'n')");
        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('nor', 'Norsk', 'n', 'n')");
        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('por', 'Português', 'y', 'n')");
        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('rus', 'русский язык', 'n', 'n')");
        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('spa', 'Español', 'y', 'n')");
        $this->addSql("INSERT INTO public.languages (id, name, isinspire, isdefault) VALUES ('vie', 'Tiếng Việt', 'n', 'n')");

        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (1, 'chi', 'Maps & graphics')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (2, 'chi', 'Datasets')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (3, 'chi', 'Interactive resources')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (4, 'chi', 'Applications')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (5, 'chi', 'Case studies, best practices')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (6, 'chi', 'Conference proceedings')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (7, 'chi', 'Photo')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (8, 'chi', 'Audio/Video')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (9, 'chi', 'Directories')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (10, 'chi', 'Other information resources')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (1, 'eng', 'Maps & graphics')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (2, 'eng', 'Datasets')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (3, 'eng', 'Interactive resources')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (4, 'eng', 'Applications')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (5, 'eng', 'Case studies, best practices')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (6, 'eng', 'Conference proceedings')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (7, 'eng', 'Photo')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (8, 'eng', 'Audio/Video')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (9, 'eng', 'Directories')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (10, 'eng', 'Other information resources')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (2, 'fre', 'Jeux de données')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (1, 'fre', 'Cartes & graphiques')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (7, 'fre', 'Photographies')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (10, 'fre', 'Autres ressources')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (5, 'fre', 'Etude de cas, meilleures pratiques')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (8, 'fre', 'Vidéo/Audio')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (9, 'fre', 'Répertoires')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (4, 'fre', 'Applications')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (3, 'fre', 'Ressources interactives')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (6, 'fre', 'Conférences')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (11, 'fre', 'Serveurs Z3950')");
        $this->addSql("INSERT INTO public.categoriesdes (iddes, langid, label) VALUES (12, 'fre', 'Annuaires')");

        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (13, 'chi', 'title', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (14, 'chi', 'abstract', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (15, 'chi', 'fees', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (16, 'chi', 'accessConstraints', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (9, 'dut', 'title', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (10, 'dut', 'abstract', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (11, 'dut', 'fees', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (12, 'dut', 'accessConstraints', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (1, 'eng', 'title', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (2, 'eng', 'abstract', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (3, 'eng', 'fees', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (4, 'eng', 'accessConstraints', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (21, 'fre', 'title', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (22, 'fre', 'abstract', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (23, 'fre', 'fees', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (24, 'fre', 'accessConstraints', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (17, 'ger', 'title', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (18, 'ger', 'abstract', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (19, 'ger', 'fees', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (20, 'ger', 'accessConstraints', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (29, 'rus', 'title', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (30, 'rus', 'abstract', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (31, 'rus', 'fees', '')");
        $this->addSql("INSERT INTO public.cswservercapabilitiesinfo (idfield, langid, field, label) VALUES (32, 'rus', 'accessConstraints', '')");

        $this->addSql("INSERT INTO public.email (user_id, email) VALUES (1, NULL)");
        $this->addSql("INSERT INTO public.email (user_id, email) VALUES (40102, 'admin@prodige-opensource.org')");
        $this->addSql("INSERT INTO public.email (user_id, email) VALUES (40784, 'noreply@nomail.com')");

        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (0, 'chi', 'Intranet')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (1, 'chi', 'All (Internet)')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (0, 'eng', 'Intranet')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (1, 'eng', 'All (Internet)')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (-1, 'eng', 'GUEST')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (0, 'fre', 'Intranet')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (1, 'fre', 'All (Internet)')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (-1, 'fre', 'GUEST')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'ara', 'France')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'cat', 'France')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'chi', 'France')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'dut', 'France')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'eng', 'France')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'fin', 'France')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'fre', 'France')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'ger', 'France')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'nor', 'France')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'por', 'France')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'rus', 'France')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'spa', 'France')");
        $this->addSql("INSERT INTO public.groupsdes (iddes, langid, label) VALUES (217, 'vie', 'France')");

        $this->addSql("INSERT INTO public.harvestersettings (id, parentid, name, value) VALUES (1, NULL, 'harvesting', NULL)");

        $this->addSql("SELECT pg_catalog.setval('public.hibernate_sequence', 40787, true)");

        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (1, 'aar', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (2, 'abk', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (3, 'ace', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (4, 'ach', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (5, 'ada', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (6, 'ady', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (7, 'afa', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (8, 'afh', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (9, 'afr', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (10, 'ain', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (11, 'aka', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (12, 'akk', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (13, 'alb', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (14, 'ale', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (15, 'alg', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (16, 'alt', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (17, 'amh', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (18, 'ang', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (19, 'anp', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (20, 'apa', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (22, 'arc', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (23, 'arg', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (24, 'arm', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (25, 'arn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (26, 'arp', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (27, 'art', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (28, 'arw', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (29, 'asm', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (30, 'ast', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (31, 'ath', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (32, 'aus', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (33, 'ava', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (34, 'ave', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (35, 'awa', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (36, 'aym', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (37, 'aze', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (38, 'bad', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (39, 'bai', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (40, 'bak', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (41, 'bal', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (42, 'bam', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (43, 'ban', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (44, 'baq', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (45, 'bas', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (46, 'bat', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (47, 'bej', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (48, 'bel', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (49, 'bem', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (50, 'ben', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (51, 'ber', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (52, 'bho', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (53, 'bih', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (54, 'bik', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (55, 'bin', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (56, 'bis', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (57, 'bla', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (58, 'bnt', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (59, 'bos', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (60, 'bra', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (61, 'bre', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (62, 'btk', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (63, 'bua', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (64, 'bug', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (65, 'bul', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (66, 'bur', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (67, 'byn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (68, 'cad', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (69, 'cai', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (70, 'car', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (72, 'cau', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (73, 'ceb', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (74, 'cel', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (75, 'cha', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (76, 'chb', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (77, 'che', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (78, 'chg', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (80, 'chk', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (81, 'chm', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (82, 'chn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (83, 'cho', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (84, 'chp', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (85, 'chr', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (86, 'chu', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (87, 'chv', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (88, 'chy', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (89, 'cmc', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (90, 'cop', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (91, 'cor', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (92, 'cos', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (93, 'cpe', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (94, 'cpf', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (95, 'cpp', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (96, 'cre', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (97, 'crh', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (98, 'crp', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (99, 'csb', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (100, 'cus', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (101, 'cze', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (102, 'dak', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (103, 'dan', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (104, 'dar', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (105, 'day', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (106, 'del', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (107, 'den', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (108, 'dgr', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (109, 'din', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (110, 'div', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (111, 'doi', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (112, 'dra', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (113, 'dsb', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (114, 'dua', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (115, 'dum', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (117, 'dyu', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (118, 'dzo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (119, 'efi', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (120, 'egy', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (121, 'eka', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (122, 'elx', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (124, 'enm', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (125, 'epo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (126, 'est', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (127, 'ewe', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (128, 'ewo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (129, 'fan', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (130, 'fao', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (131, 'fat', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (132, 'fij', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (133, 'fil', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (135, 'fiu', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (136, 'fon', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (138, 'frm', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (139, 'fro', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (140, 'frr', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (141, 'frs', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (142, 'fry', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (143, 'ful', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (144, 'fur', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (145, 'gaa', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (146, 'gay', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (147, 'gba', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (148, 'gem', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (149, 'geo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (150, 'deu', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (151, 'gez', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (152, 'gil', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (153, 'gla', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (154, 'gle', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (155, 'glg', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (156, 'glv', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (157, 'gmh', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (158, 'goh', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (159, 'gon', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (160, 'gor', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (161, 'got', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (162, 'grb', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (163, 'grc', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (164, 'gre', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (165, 'grn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (166, 'gsw', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (167, 'guj', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (168, 'gwi', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (169, 'hai', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (170, 'hat', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (171, 'hau', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (172, 'haw', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (173, 'heb', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (174, 'her', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (175, 'hil', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (176, 'him', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (177, 'hin', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (178, 'hit', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (179, 'hmn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (180, 'hmo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (181, 'hsb', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (182, 'hun', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (183, 'hup', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (184, 'iba', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (185, 'ibo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (186, 'ice', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (187, 'ido', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (188, 'iii', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (189, 'ijo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (190, 'iku', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (191, 'ile', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (192, 'ilo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (193, 'ina', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (194, 'inc', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (195, 'ind', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (196, 'ine', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (197, 'inh', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (198, 'ipk', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (199, 'ira', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (200, 'iro', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (201, 'ita', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (202, 'jav', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (203, 'jbo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (204, 'jpn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (205, 'jpr', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (206, 'jrb', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (207, 'kaa', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (208, 'kab', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (209, 'kac', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (210, 'kal', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (211, 'kam', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (212, 'kan', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (213, 'kar', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (214, 'kas', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (215, 'kau', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (216, 'kaw', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (217, 'kaz', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (218, 'kbd', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (219, 'kha', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (220, 'khi', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (221, 'khm', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (222, 'kho', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (223, 'kik', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (224, 'kin', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (225, 'kir', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (226, 'kmb', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (227, 'kok', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (71, 'cat', 'ca')");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (79, 'chi', 'ch')");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (116, 'dut', 'nl')");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (123, 'eng', 'en')");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (228, 'kom', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (229, 'kon', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (230, 'kor', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (231, 'kos', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (232, 'kpe', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (233, 'krc', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (234, 'krl', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (235, 'kro', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (236, 'kru', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (237, 'kua', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (238, 'kum', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (239, 'kur', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (240, 'kut', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (241, 'lad', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (242, 'lah', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (243, 'lam', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (244, 'lao', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (245, 'lat', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (246, 'lav', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (247, 'lez', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (248, 'lim', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (249, 'lin', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (250, 'lit', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (251, 'lol', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (252, 'loz', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (253, 'ltz', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (254, 'lua', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (255, 'lub', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (256, 'lug', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (257, 'lui', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (258, 'lun', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (259, 'luo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (260, 'lus', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (261, 'mac', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (262, 'mad', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (263, 'mag', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (264, 'mah', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (265, 'mai', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (266, 'mak', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (267, 'mal', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (268, 'man', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (269, 'mao', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (270, 'map', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (271, 'mar', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (272, 'mas', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (273, 'may', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (274, 'mdf', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (275, 'mdr', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (276, 'men', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (277, 'mga', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (278, 'mic', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (279, 'min', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (280, 'mis', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (281, 'mkh', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (282, 'mlg', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (283, 'mlt', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (284, 'mnc', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (285, 'mni', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (286, 'mno', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (287, 'moh', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (288, 'mol', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (289, 'mon', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (290, 'mos', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (291, 'mul', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (292, 'mun', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (293, 'mus', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (294, 'mwl', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (295, 'mwr', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (296, 'myn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (297, 'myv', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (298, 'nah', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (299, 'nai', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (300, 'nap', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (301, 'nau', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (302, 'nav', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (303, 'nbl', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (304, 'nde', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (305, 'ndo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (306, 'nds', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (307, 'nep', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (308, 'new', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (309, 'nia', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (310, 'nic', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (311, 'niu', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (312, 'nno', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (313, 'nob', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (314, 'nog', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (315, 'non', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (317, 'nso', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (318, 'nub', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (319, 'nwc', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (320, 'nya', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (321, 'nym', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (322, 'nyn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (323, 'nyo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (324, 'nzi', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (325, 'oci', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (326, 'oji', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (327, 'ori', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (328, 'orm', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (329, 'osa', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (330, 'oss', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (331, 'ota', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (332, 'oto', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (333, 'paa', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (334, 'pag', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (335, 'pal', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (336, 'pam', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (337, 'pan', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (338, 'pap', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (339, 'pau', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (340, 'peo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (341, 'per', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (342, 'phi', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (343, 'phn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (344, 'pli', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (345, 'pol', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (346, 'pon', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (348, 'pra', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (349, 'pro', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (350, 'pus', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (351, 'qaa', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (352, 'que', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (353, 'raj', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (354, 'rap', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (355, 'rar', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (356, 'roa', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (357, 'roh', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (358, 'rom', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (359, 'rum', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (360, 'run', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (361, 'rup', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (363, 'sad', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (364, 'sag', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (365, 'sah', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (366, 'sai', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (367, 'sal', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (368, 'sam', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (369, 'san', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (370, 'sas', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (371, 'sat', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (372, 'scc', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (373, 'scn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (374, 'sco', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (375, 'scr', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (376, 'sel', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (377, 'sem', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (378, 'sga', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (379, 'sgn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (380, 'shn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (381, 'sid', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (382, 'sin', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (383, 'sio', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (384, 'sit', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (385, 'sla', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (386, 'slo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (387, 'slv', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (388, 'sma', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (389, 'sme', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (390, 'smi', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (391, 'smj', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (392, 'smn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (393, 'smo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (394, 'sms', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (395, 'sna', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (396, 'snd', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (397, 'snk', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (398, 'sog', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (399, 'som', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (400, 'son', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (401, 'sot', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (403, 'srd', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (404, 'srn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (405, 'srr', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (406, 'ssa', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (407, 'ssw', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (408, 'suk', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (409, 'sun', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (410, 'sus', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (411, 'sux', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (412, 'swa', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (413, 'swe', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (414, 'syr', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (415, 'tah', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (416, 'tai', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (417, 'tam', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (418, 'tat', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (419, 'tel', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (420, 'tem', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (421, 'ter', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (422, 'tet', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (423, 'tgk', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (424, 'tgl', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (425, 'tha', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (426, 'tib', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (427, 'tig', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (428, 'tir', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (429, 'tiv', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (430, 'tkl', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (431, 'tlh', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (432, 'tli', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (433, 'tmh', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (434, 'tog', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (435, 'ton', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (436, 'tpi', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (437, 'tsi', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (438, 'tsn', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (439, 'tso', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (440, 'tuk', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (441, 'tum', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (442, 'tup', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (443, 'tur', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (444, 'tut', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (445, 'tvl', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (446, 'twi', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (447, 'tyv', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (448, 'udm', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (449, 'uga', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (450, 'uig', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (451, 'ukr', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (452, 'umb', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (453, 'und', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (347, 'por', 'po')");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (362, 'rus', 'ru')");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (402, 'spa', 'sp')");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (454, 'urd', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (455, 'uzb', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (456, 'vai', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (457, 'ven', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (459, 'vol', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (460, 'vot', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (461, 'wak', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (462, 'wal', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (463, 'war', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (464, 'was', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (465, 'wel', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (466, 'wen', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (467, 'wln', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (468, 'wol', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (469, 'xal', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (470, 'xho', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (471, 'yao', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (472, 'yap', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (473, 'yid', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (474, 'yor', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (475, 'ypk', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (476, 'zap', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (477, 'zen', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (478, 'zha', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (479, 'znd', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (480, 'zul', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (481, 'zun', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (482, 'zxx', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (483, 'nqo', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (484, 'zza', NULL)");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (21, 'ara', 'ar')");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (134, 'fin', 'fi')");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (137, 'fre', 'fr')");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (316, 'nor', 'no')");
        $this->addSql("INSERT INTO public.isolanguages (id, code, shortcode) VALUES (458, 'vie', 'vi')");

        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (1, 'eng', 'Afar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (2, 'eng', 'Abkhazian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (3, 'eng', 'Achinese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (4, 'eng', 'Acoli')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (5, 'eng', 'Adangme')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (6, 'eng', 'Adyghe; Adygei')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (7, 'eng', 'Afro-Asiatic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (8, 'eng', 'Afrihili')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (9, 'eng', 'Afrikaans')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (10, 'eng', 'Ainu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (11, 'eng', 'Akan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (12, 'eng', 'Akkadian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (13, 'eng', 'Albanian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (14, 'eng', 'Aleut')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (15, 'eng', 'Algonquian languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (16, 'eng', 'Southern Altai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (17, 'eng', 'Amharic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (18, 'eng', 'English, Old (ca.450-1100)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (19, 'eng', 'Angika')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (20, 'eng', 'Apache languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (21, 'eng', 'Arabic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (22, 'eng', 'Aramaic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (23, 'eng', 'Aragonese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (24, 'eng', 'Armenian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (25, 'eng', 'Mapudungun; Mapuche')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (26, 'eng', 'Arapaho')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (27, 'eng', 'Artificial (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (28, 'eng', 'Arawak')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (29, 'eng', 'Assamese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (30, 'eng', 'Asturian; Bable')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (31, 'eng', 'Athapascan languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (32, 'eng', 'Australian languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (33, 'eng', 'Avaric')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (34, 'eng', 'Avestan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (35, 'eng', 'Awadhi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (36, 'eng', 'Aymara')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (37, 'eng', 'Azerbaijani')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (38, 'eng', 'Banda languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (1, 'fre', 'Afar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (2, 'fre', 'Abkhaze')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (3, 'fre', 'Aceh')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (4, 'fre', 'Acoli')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (5, 'fre', 'Adangme')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (6, 'fre', 'Adyghé')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (7, 'fre', 'Afro-asiatiques, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (8, 'fre', 'Afrihili')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (9, 'fre', 'Afrikaans')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (10, 'fre', 'Aïnou')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (11, 'fre', 'Akan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (12, 'fre', 'Akkadien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (13, 'fre', 'Albanais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (14, 'fre', 'Aléoute')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (15, 'fre', 'Algonquines, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (16, 'fre', 'Altai du Sud')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (17, 'fre', 'Amharique')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (18, 'fre', 'Anglo-saxon (ca.450-1100)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (19, 'fre', 'Angika')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (20, 'fre', 'Apache')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (21, 'fre', 'Arabe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (22, 'fre', 'Araméen')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (23, 'fre', 'Aragonais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (24, 'fre', 'Arménien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (25, 'fre', 'Mapudungun; mapuche; mapuce')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (26, 'fre', 'Arapaho')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (27, 'fre', 'Artificielles, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (28, 'fre', 'Arawak')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (29, 'fre', 'Assamais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (30, 'fre', 'Asturien; bable')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (31, 'fre', 'Athapascanes, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (32, 'fre', 'Australiennes, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (33, 'fre', 'Avar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (34, 'fre', 'Avestique')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (35, 'fre', 'Awadhi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (36, 'fre', 'Aymara')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (37, 'fre', 'Azéri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (38, 'fre', 'Banda, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (44, 'eng', 'Basque')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (45, 'eng', 'Basa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (46, 'eng', 'Baltic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (47, 'eng', 'Beja')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (48, 'eng', 'Belarusian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (49, 'eng', 'Bemba')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (50, 'eng', 'Bengali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (51, 'eng', 'Berber (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (52, 'eng', 'Bhojpuri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (53, 'eng', 'Bihari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (54, 'eng', 'Bikol')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (55, 'eng', 'Bini; Edo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (56, 'eng', 'Bislama')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (57, 'eng', 'Siksika')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (58, 'eng', 'Bantu (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (59, 'eng', 'Bosnian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (60, 'eng', 'Braj')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (61, 'eng', 'Breton')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (62, 'eng', 'Batak languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (63, 'eng', 'Buriat')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (64, 'eng', 'Buginese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (65, 'eng', 'Bulgarian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (66, 'eng', 'Burmese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (67, 'eng', 'Blin; Bilin')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (68, 'eng', 'Caddo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (69, 'eng', 'Central American Indian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (70, 'eng', 'Galibi Carib')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (71, 'eng', 'Catalan; Valencian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (72, 'eng', 'Caucasian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (73, 'eng', 'Cebuano')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (74, 'eng', 'Celtic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (75, 'eng', 'Chamorro')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (76, 'eng', 'Chibcha')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (77, 'eng', 'Chechen')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (78, 'eng', 'Chagatai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (79, 'eng', 'Chinese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (80, 'eng', 'Chuukese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (43, 'fre', 'Balinais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (44, 'fre', 'Basque')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (45, 'fre', 'Basa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (46, 'fre', 'Baltiques, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (47, 'fre', 'Bedja')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (48, 'fre', 'Biélorusse')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (49, 'fre', 'Bemba')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (50, 'fre', 'Bengali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (51, 'fre', 'Berbères, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (52, 'fre', 'Bhojpuri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (53, 'fre', 'Bihari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (54, 'fre', 'Bikol')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (55, 'fre', 'Bini; edo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (56, 'fre', 'Bichlamar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (57, 'fre', 'Blackfoot')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (58, 'fre', 'Bantoues, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (59, 'fre', 'Bosniaque')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (60, 'fre', 'Braj')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (61, 'fre', 'Breton')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (62, 'fre', 'Batak, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (63, 'fre', 'Bouriate')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (64, 'fre', 'Bugi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (65, 'fre', 'Bulgare')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (66, 'fre', 'Birman')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (67, 'fre', 'Blin; bilen')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (68, 'fre', 'Caddo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (69, 'fre', 'Indiennes d''Amérique centrale, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (70, 'fre', 'Karib; galibi; carib')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (71, 'fre', 'Catalan; valencien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (72, 'fre', 'Caucasiennes, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (73, 'fre', 'Cebuano')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (74, 'fre', 'Celtiques, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (75, 'fre', 'Chamorro')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (76, 'fre', 'Chibcha')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (77, 'fre', 'Tchétchène')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (78, 'fre', 'Djaghataï')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (79, 'fre', 'Chinois')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (80, 'fre', 'Chuuk')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (81, 'fre', 'Mari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (87, 'eng', 'Chuvash')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (88, 'eng', 'Cheyenne')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (89, 'eng', 'Chamic languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (90, 'eng', 'Coptic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (91, 'eng', 'Cornish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (92, 'eng', 'Corsican')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (93, 'eng', 'Creoles and pidgins, English based (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (94, 'eng', 'Creoles and pidgins, French-based (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (95, 'eng', 'Creoles and pidgins, Portuguese-based (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (96, 'eng', 'Cree')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (97, 'eng', 'Crimean Tatar; Crimean Turkish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (98, 'eng', 'Creoles and pidgins (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (99, 'eng', 'Kashubian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (100, 'eng', 'Cushitic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (101, 'eng', 'Czech')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (102, 'eng', 'Dakota')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (103, 'eng', 'Danish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (104, 'eng', 'Dargwa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (105, 'eng', 'Land Dayak languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (106, 'eng', 'Delaware')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (107, 'eng', 'Slave (Athapascan)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (108, 'eng', 'Dogrib')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (109, 'eng', 'Dinka')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (110, 'eng', 'Divehi; Dhivehi; Maldivian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (111, 'eng', 'Dogri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (112, 'eng', 'Dravidian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (113, 'eng', 'Lower Sorbian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (114, 'eng', 'Duala')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (115, 'eng', 'Dutch, Middle (ca.1050-1350)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (116, 'eng', 'Dutch; Flemish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (117, 'eng', 'Dyula')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (118, 'eng', 'Dzongkha')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (119, 'eng', 'Efik')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (120, 'eng', 'Egyptian (Ancient)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (121, 'eng', 'Ekajuk')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (122, 'eng', 'Elamite')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (86, 'fre', 'Slavon d''église; vieux slave; slavon liturgique; vieux bulgare')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (87, 'fre', 'Tchouvache')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (88, 'fre', 'Cheyenne')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (89, 'fre', 'Chames, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (90, 'fre', 'Copte')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (91, 'fre', 'Cornique')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (92, 'fre', 'Corse')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (93, 'fre', 'Créoles et pidgins anglais, autres')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (94, 'fre', 'Créoles et pidgins français, autres')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (95, 'fre', 'Créoles et pidgins portugais, autres')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (96, 'fre', 'Cree')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (97, 'fre', 'Tatar de Crimé')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (98, 'fre', 'Créoles et pidgins divers')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (99, 'fre', 'Kachoube')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (100, 'fre', 'Couchitiques, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (101, 'fre', 'Tchèque')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (102, 'fre', 'Dakota')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (103, 'fre', 'Danois')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (104, 'fre', 'Dargwa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (105, 'fre', 'Dayak, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (106, 'fre', 'Delaware')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (107, 'fre', 'Esclave (athapascan)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (108, 'fre', 'Dogrib')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (109, 'fre', 'Dinka')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (110, 'fre', 'Maldivien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (111, 'fre', 'Dogri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (112, 'fre', 'Dravidiennes, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (113, 'fre', 'Bas-sorabe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (114, 'fre', 'Douala')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (115, 'fre', 'Néerlandais moyen (ca. 1050-1350)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (116, 'fre', 'Néerlandais; flamand')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (117, 'fre', 'Dioula')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (118, 'fre', 'Dzongkha')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (119, 'fre', 'Efik')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (126, 'eng', 'Estonian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (127, 'eng', 'Ewe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (128, 'eng', 'Ewondo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (129, 'eng', 'Fang')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (130, 'eng', 'Faroese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (131, 'eng', 'Fanti')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (132, 'eng', 'Fijian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (133, 'eng', 'Filipino; Pilipino')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (134, 'eng', 'Finnish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (135, 'eng', 'Finno-Ugrian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (136, 'eng', 'Fon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (137, 'eng', 'French')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (138, 'eng', 'French, Middle (ca.1400-1600)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (139, 'eng', 'French, Old (842-ca.1400)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (140, 'eng', 'Northern Frisian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (141, 'eng', 'Eastern Frisian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (142, 'eng', 'Western Frisian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (143, 'eng', 'Fulah')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (144, 'eng', 'Friulian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (145, 'eng', 'Ga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (146, 'eng', 'Gayo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (147, 'eng', 'Gbaya')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (148, 'eng', 'Germanic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (149, 'eng', 'Georgian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (150, 'eng', 'German')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (151, 'eng', 'Geez')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (152, 'eng', 'Gilbertese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (153, 'eng', 'Gaelic; Scottish Gaelic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (154, 'eng', 'Irish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (155, 'eng', 'Galician')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (156, 'eng', 'Manx')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (157, 'eng', 'German, Middle High (ca.1050-1500)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (158, 'eng', 'German, Old High (ca.750-1050)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (159, 'eng', 'Gondi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (160, 'eng', 'Gorontalo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (161, 'eng', 'Gothic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (162, 'eng', 'Grebo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (163, 'eng', 'Greek, Ancient (to 1453)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (125, 'fre', 'Espéranto')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (126, 'fre', 'Estonien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (127, 'fre', 'Éwé')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (128, 'fre', 'Éwondo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (129, 'fre', 'Fang')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (130, 'fre', 'Féroïen')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (131, 'fre', 'Fanti')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (132, 'fre', 'Fidjien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (133, 'fre', 'Filipino; pilipino')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (134, 'fre', 'Finnois')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (135, 'fre', 'Finno-ougriennes, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (136, 'fre', 'Fon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (137, 'fre', 'Français')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (138, 'fre', 'Français moyen (1400-1600)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (139, 'fre', 'Français ancien (842-ca.1400)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (140, 'fre', 'Frison septentrional')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (141, 'fre', 'Frison oriental')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (142, 'fre', 'Frison occidental')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (143, 'fre', 'Peul')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (144, 'fre', 'Frioulan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (145, 'fre', 'Ga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (146, 'fre', 'Gayo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (147, 'fre', 'Gbaya')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (148, 'fre', 'Germaniques, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (149, 'fre', 'Géorgien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (150, 'fre', 'Allemand')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (151, 'fre', 'Guèze')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (152, 'fre', 'Kiribati')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (153, 'fre', 'Gaélique; gaélique écossais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (154, 'fre', 'Irlandais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (155, 'fre', 'Galicien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (156, 'fre', 'Manx; mannois')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (157, 'fre', 'Allemand, moyen haut (ca. 1050-1500)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (158, 'fre', 'Allemand, vieux haut (ca. 750-1050)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (159, 'fre', 'Gond')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (167, 'eng', 'Gujarati')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (168, 'eng', 'Gwich''in')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (169, 'eng', 'Haida')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (170, 'eng', 'Haitian; Haitian Creole')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (171, 'eng', 'Hausa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (172, 'eng', 'Hawaiian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (173, 'eng', 'Hebrew')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (174, 'eng', 'Herero')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (175, 'eng', 'Hiligaynon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (176, 'eng', 'Himachali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (177, 'eng', 'Hindi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (178, 'eng', 'Hittite')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (179, 'eng', 'Hmong')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (180, 'eng', 'Hiri Motu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (181, 'eng', 'Upper Sorbian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (182, 'eng', 'Hungarian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (183, 'eng', 'Hupa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (184, 'eng', 'Iban')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (185, 'eng', 'Igbo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (186, 'eng', 'Icelandic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (187, 'eng', 'Ido')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (188, 'eng', 'Sichuan Yi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (189, 'eng', 'Ijo languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (190, 'eng', 'Inuktitut')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (191, 'eng', 'Interlingue')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (192, 'eng', 'Iloko')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (193, 'eng', 'Interlingua (International Auxiliary Language Association)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (194, 'eng', 'Indic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (195, 'eng', 'Indonesian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (196, 'eng', 'Indo-European (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (197, 'eng', 'Ingush')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (198, 'eng', 'Inupiaq')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (199, 'eng', 'Iranian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (200, 'eng', 'Iroquoian languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (201, 'eng', 'Italian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (202, 'eng', 'Javanese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (203, 'eng', 'Lojban')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (204, 'eng', 'Japanese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (167, 'fre', 'Goudjrati')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (168, 'fre', 'Gwich''in')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (169, 'fre', 'Haida')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (170, 'fre', 'Haïtien; créole haïtien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (171, 'fre', 'Haoussa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (172, 'fre', 'Hawaïen')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (173, 'fre', 'Hébreu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (174, 'fre', 'Herero')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (175, 'fre', 'Hiligaynon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (176, 'fre', 'Himachali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (177, 'fre', 'Hindi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (178, 'fre', 'Hittite')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (179, 'fre', 'Hmong')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (180, 'fre', 'Hiri motu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (181, 'fre', 'Haut-sorabe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (182, 'fre', 'Hongrois')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (183, 'fre', 'Hupa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (184, 'fre', 'Iban')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (185, 'fre', 'Igbo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (186, 'fre', 'Islandais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (187, 'fre', 'Ido')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (188, 'fre', 'Yi de Sichuan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (189, 'fre', 'Ijo, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (190, 'fre', 'Inuktitut')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (191, 'fre', 'Interlingue')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (192, 'fre', 'Ilocano')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (193, 'fre', 'Interlingua (langue auxiliaire internationale)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (194, 'fre', 'Indo-aryennes, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (195, 'fre', 'Indonésien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (196, 'fre', 'Indo-européennes, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (197, 'fre', 'Ingouche')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (198, 'fre', 'Inupiaq')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (199, 'fre', 'Iraniennes, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (200, 'fre', 'Iroquoises, langues (famille)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (201, 'fre', 'Italien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (202, 'fre', 'Javanais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (203, 'fre', 'Lojban')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (204, 'fre', 'Japonais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (211, 'chi', 'Kamba')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (210, 'eng', 'Kalaallisut; Greenlandic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (211, 'eng', 'Kamba')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (212, 'eng', 'Kannada')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (213, 'eng', 'Karen languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (214, 'eng', 'Kashmiri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (215, 'eng', 'Kanuri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (216, 'eng', 'Kawi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (217, 'eng', 'Kazakh')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (218, 'eng', 'Kabardian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (219, 'eng', 'Khasi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (220, 'eng', 'Khoisan (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (221, 'eng', 'Central Khmer')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (222, 'eng', 'Khotanese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (223, 'eng', 'Kikuyu; Gikuyu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (224, 'eng', 'Kinyarwanda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (225, 'eng', 'Kirghiz; Kyrgyz')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (226, 'eng', 'Kimbundu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (227, 'eng', 'Konkani')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (228, 'eng', 'Komi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (229, 'eng', 'Kongo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (230, 'eng', 'Korean')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (231, 'eng', 'Kosraean')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (232, 'eng', 'Kpelle')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (233, 'eng', 'Karachay-Balkar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (234, 'eng', 'Karelian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (235, 'eng', 'Kru languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (236, 'eng', 'Kurukh')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (237, 'eng', 'Kuanyama; Kwanyama')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (238, 'eng', 'Kumyk')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (239, 'eng', 'Kurdish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (240, 'eng', 'Kutenai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (241, 'eng', 'Ladino')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (242, 'eng', 'Lahnda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (243, 'eng', 'Lamba')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (244, 'eng', 'Lao')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (245, 'eng', 'Latin')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (246, 'eng', 'Latvian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (247, 'eng', 'Lezghian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (248, 'eng', 'Limburgan; Limburger; Limburgish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (209, 'fre', 'Kachin; jingpho')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (210, 'fre', 'Groenlandais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (211, 'fre', 'Kamba')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (212, 'fre', 'Kannada')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (213, 'fre', 'Karen, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (214, 'fre', 'Kashmiri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (215, 'fre', 'Kanouri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (216, 'fre', 'Kawi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (217, 'fre', 'Kazakh')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (218, 'fre', 'Kabardien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (219, 'fre', 'Khasi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (220, 'fre', 'Khoisan, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (221, 'fre', 'Khmer central')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (222, 'fre', 'Khotanais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (223, 'fre', 'Kikuyu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (224, 'fre', 'Rwanda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (225, 'fre', 'Kirghiz')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (226, 'fre', 'Kimbundu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (227, 'fre', 'Konkani')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (228, 'fre', 'Kom')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (229, 'fre', 'Kongo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (230, 'fre', 'Coréen')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (231, 'fre', 'Kosrae')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (232, 'fre', 'Kpellé')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (233, 'fre', 'Karatchai balkar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (234, 'fre', 'Carélien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (235, 'fre', 'Krou, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (236, 'fre', 'Kurukh')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (237, 'fre', 'Kuanyama; kwanyama')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (238, 'fre', 'Koumyk')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (239, 'fre', 'Kurde')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (240, 'fre', 'Kutenai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (241, 'fre', 'Judéo-espagnol')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (242, 'fre', 'Lahnda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (243, 'fre', 'Lamba')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (244, 'fre', 'Lao')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (245, 'fre', 'Latin')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (246, 'fre', 'Letton')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (247, 'fre', 'Lezghien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (253, 'eng', 'Luxembourgish; Letzeburgesch')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (254, 'eng', 'Luba-Lulua')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (255, 'eng', 'Luba-Katanga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (256, 'eng', 'Ganda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (257, 'eng', 'Luiseno')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (258, 'eng', 'Lunda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (259, 'eng', 'Luo (Kenya and Tanzania)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (260, 'eng', 'Lushai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (261, 'eng', 'Macedonian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (262, 'eng', 'Madurese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (263, 'eng', 'Magahi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (264, 'eng', 'Marshallese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (265, 'eng', 'Maithili')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (266, 'eng', 'Makasar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (267, 'eng', 'Malayalam')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (268, 'eng', 'Mandingo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (269, 'eng', 'Maori')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (270, 'eng', 'Austronesian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (271, 'eng', 'Marathi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (272, 'eng', 'Masai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (273, 'eng', 'Malay')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (274, 'eng', 'Moksha')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (275, 'eng', 'Mandar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (276, 'eng', 'Mende')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (277, 'eng', 'Irish, Middle (900-1200)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (278, 'eng', 'Mi''kmaq; Micmac')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (279, 'eng', 'Minangkabau')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (280, 'eng', 'Miscellaneous languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (281, 'eng', 'Mon-Khmer (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (282, 'eng', 'Malagasy')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (283, 'eng', 'Maltese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (284, 'eng', 'Manchu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (285, 'eng', 'Manipuri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (286, 'eng', 'Manobo languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (287, 'eng', 'Mohawk')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (288, 'eng', 'Moldavian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (289, 'eng', 'Mongolian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (290, 'eng', 'Mossi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (253, 'fre', 'Luxembourgeois')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (254, 'fre', 'Luba-lulua')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (255, 'fre', 'Luba-katanga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (256, 'fre', 'Ganda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (257, 'fre', 'Luiseno')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (258, 'fre', 'Lunda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (259, 'fre', 'Luo (Kenya et Tanzanie)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (260, 'fre', 'Lushai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (261, 'fre', 'Macédonien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (262, 'fre', 'Madourais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (263, 'fre', 'Magahi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (264, 'fre', 'Marshall')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (265, 'fre', 'Maithili')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (266, 'fre', 'Makassar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (267, 'fre', 'Malayalam')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (268, 'fre', 'Mandingue')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (269, 'fre', 'Maori')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (270, 'fre', 'Malayo-polynésiennes, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (271, 'fre', 'Marathe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (272, 'fre', 'Massaï')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (273, 'fre', 'Malais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (274, 'fre', 'Moksa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (275, 'fre', 'Mandar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (276, 'fre', 'Mendé')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (277, 'fre', 'Irlandais moyen (900-1200)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (278, 'fre', 'Mi''kmaq; micmac')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (279, 'fre', 'Minangkabau')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (280, 'fre', 'Diverses, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (281, 'fre', 'Môn-khmer, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (282, 'fre', 'Malgache')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (283, 'fre', 'Maltais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (284, 'fre', 'Mandchou')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (285, 'fre', 'Manipuri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (286, 'fre', 'Manobo, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (287, 'fre', 'Mohawk')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (288, 'fre', 'Moldave')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (289, 'fre', 'Mongol')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (290, 'fre', 'Moré')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (291, 'fre', 'Multilingue')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (295, 'chi', 'Marwari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (296, 'eng', 'Mayan languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (297, 'eng', 'Erzya')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (298, 'eng', 'Nahuatl languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (299, 'eng', 'North American Indian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (300, 'eng', 'Neapolitan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (301, 'eng', 'Nauru')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (302, 'eng', 'Navajo; Navaho')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (303, 'eng', 'Ndebele, South; South Ndebele')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (304, 'eng', 'Ndebele, North; North Ndebele')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (305, 'eng', 'Ndonga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (306, 'eng', 'Low German; Low Saxon; German, Low; Saxon, Low')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (307, 'eng', 'Nepali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (308, 'eng', 'Nepal Bhasa; Newari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (309, 'eng', 'Nias')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (310, 'eng', 'Niger-Kordofanian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (311, 'eng', 'Niuean')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (312, 'eng', 'Norwegian Nynorsk; Nynorsk, Norwegian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (313, 'eng', 'Bokmål, Norwegian; Norwegian Bokmål')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (314, 'eng', 'Nogai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (315, 'eng', 'Norse, Old')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (316, 'eng', 'Norwegian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (317, 'eng', 'Pedi; Sepedi; Northern Sotho')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (318, 'eng', 'Nubian languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (319, 'eng', 'Classical Newari; Old Newari; Classical Nepal Bhasa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (320, 'eng', 'Chichewa; Chewa; Nyanja')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (321, 'eng', 'Nyamwezi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (322, 'eng', 'Nyankole')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (323, 'eng', 'Nyoro')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (324, 'eng', 'Nzima')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (325, 'eng', 'Occitan (post 1500); Provençal')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (326, 'eng', 'Ojibwa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (327, 'eng', 'Oriya')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (328, 'eng', 'Oromo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (329, 'eng', 'Osage')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (295, 'fre', 'Marvari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (296, 'fre', 'Maya, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (297, 'fre', 'Erza')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (298, 'fre', 'Nahuatl, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (299, 'fre', 'Indiennes d''Amérique du Nord, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (300, 'fre', 'Napolitain')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (301, 'fre', 'Nauruan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (302, 'fre', 'Navaho')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (303, 'fre', 'Ndébélé du Sud')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (304, 'fre', 'Ndébélé du Nord')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (305, 'fre', 'Ndonga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (306, 'fre', 'Bas allemand; bas saxon; allemand, bas; saxon, bas')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (307, 'fre', 'Népalais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (308, 'fre', 'Nepal bhasa; newari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (309, 'fre', 'Nias')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (310, 'fre', 'Nigéro-congolaises, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (311, 'fre', 'Niué')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (312, 'fre', 'Norvégien nynorsk; nynorsk, norvégien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (313, 'fre', 'Norvégien bokmål')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (314, 'fre', 'Nogaï; nogay')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (315, 'fre', 'Norrois, vieux')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (316, 'fre', 'Norvégien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (317, 'fre', 'Pedi; sepedi; sotho du Nord')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (318, 'fre', 'Nubiennes, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (319, 'fre', 'Newari classique')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (320, 'fre', 'Chichewa; chewa; nyanja')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (321, 'fre', 'Nyamwezi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (322, 'fre', 'Nyankolé')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (323, 'fre', 'Nyoro')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (324, 'fre', 'Nzema')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (325, 'fre', 'Occitan (après 1500); provençal')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (326, 'fre', 'Ojibwa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (327, 'fre', 'Oriya')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (328, 'fre', 'Galla')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (329, 'fre', 'Osage')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (330, 'fre', 'Ossète')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (333, 'eng', 'Papuan (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (334, 'eng', 'Pangasinan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (335, 'eng', 'Pahlavi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (336, 'eng', 'Pampanga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (337, 'eng', 'Panjabi; Punjabi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (338, 'eng', 'Papiamento')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (339, 'eng', 'Palauan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (340, 'eng', 'Persian, Old (ca.600-400 B.C.)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (341, 'eng', 'Persian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (342, 'eng', 'Philippine (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (343, 'eng', 'Phoenician')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (344, 'eng', 'Pali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (345, 'eng', 'Polish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (346, 'eng', 'Pohnpeian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (347, 'eng', 'Portuguese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (348, 'eng', 'Prakrit languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (349, 'eng', 'Provençal, Old (to 1500)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (350, 'eng', 'Pushto')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (351, 'eng', 'Reserved for local use')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (352, 'eng', 'Quechua')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (353, 'eng', 'Rajasthani')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (354, 'eng', 'Rapanui')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (355, 'eng', 'Rarotongan; Cook Islands Maori')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (356, 'eng', 'Romance (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (357, 'eng', 'Romansh')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (358, 'eng', 'Romany')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (359, 'eng', 'Romanian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (360, 'eng', 'Rundi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (361, 'eng', 'Aromanian; Arumanian; Macedo-Romanian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (362, 'eng', 'Russian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (363, 'eng', 'Sandawe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (364, 'eng', 'Sango')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (365, 'eng', 'Yakut')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (366, 'eng', 'South American Indian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (367, 'eng', 'Salishan languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (368, 'eng', 'Samaritan Aramaic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (369, 'eng', 'Sanskrit')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (333, 'fre', 'Papoues, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (334, 'fre', 'Pangasinan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (335, 'fre', 'Pahlavi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (336, 'fre', 'Pampangan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (337, 'fre', 'Pendjabi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (338, 'fre', 'Papiamento')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (339, 'fre', 'Palau')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (340, 'fre', 'Perse, vieux (ca. 600-400 av. J.-C.)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (341, 'fre', 'Persan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (342, 'fre', 'Philippines, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (343, 'fre', 'Phénicien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (344, 'fre', 'Pali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (345, 'fre', 'Polonais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (346, 'fre', 'Pohnpei')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (347, 'fre', 'Portugais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (348, 'fre', 'Prâkrit')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (349, 'fre', 'Provençal ancien (jusqu''à 1500)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (350, 'fre', 'Pachto')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (351, 'fre', 'Réservée à l''usage local')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (352, 'fre', 'Quechua')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (353, 'fre', 'Rajasthani')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (354, 'fre', 'Rapanui')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (355, 'fre', 'Rarotonga; maori des îles Cook')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (356, 'fre', 'Romanes, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (357, 'fre', 'Romanche')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (358, 'fre', 'Tsigane')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (359, 'fre', 'Roumain')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (360, 'fre', 'Rundi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (361, 'fre', 'Aroumain; macédo-roumain')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (362, 'fre', 'Russe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (363, 'fre', 'Sandawe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (364, 'fre', 'Sango')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (365, 'fre', 'Iakoute')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (366, 'fre', 'Indiennes d''Amérique du Sud, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (367, 'fre', 'Salish, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (368, 'fre', 'Samaritain')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (374, 'eng', 'Scots')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (375, 'eng', 'Croatian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (376, 'eng', 'Selkup')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (377, 'eng', 'Semitic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (378, 'eng', 'Irish, Old (to 900)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (379, 'eng', 'Sign Languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (380, 'eng', 'Shan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (381, 'eng', 'Sidamo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (382, 'eng', 'Sinhala; Sinhalese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (383, 'eng', 'Siouan languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (384, 'eng', 'Sino-Tibetan (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (385, 'eng', 'Slavic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (386, 'eng', 'Slovak')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (387, 'eng', 'Slovenian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (388, 'eng', 'Southern Sami')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (389, 'eng', 'Northern Sami')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (390, 'eng', 'Sami languages (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (391, 'eng', 'Lule Sami')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (392, 'eng', 'Inari Sami')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (393, 'eng', 'Samoan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (394, 'eng', 'Skolt Sami')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (395, 'eng', 'Shona')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (396, 'eng', 'Sindhi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (397, 'eng', 'Soninke')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (398, 'eng', 'Sogdian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (399, 'eng', 'Somali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (400, 'eng', 'Songhai languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (401, 'eng', 'Sotho, Southern')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (402, 'eng', 'Spanish; Castilian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (403, 'eng', 'Sardinian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (404, 'eng', 'Sranan Tongo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (405, 'eng', 'Serer')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (406, 'eng', 'Nilo-Saharan (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (407, 'eng', 'Swati')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (408, 'eng', 'Sukuma')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (409, 'eng', 'Sundanese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (410, 'eng', 'Susu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (411, 'eng', 'Sumerian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (374, 'fre', 'Écossais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (375, 'fre', 'Croate')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (376, 'fre', 'Selkoupe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (377, 'fre', 'Sémitiques, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (378, 'fre', 'Irlandais ancien (jusqu''à 900)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (379, 'fre', 'Langues des signes')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (380, 'fre', 'Chan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (381, 'fre', 'Sidamo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (382, 'fre', 'Singhalais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (383, 'fre', 'Sioux, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (384, 'fre', 'Sino-tibétaines, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (385, 'fre', 'Slaves, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (386, 'fre', 'Slovaque')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (387, 'fre', 'Slovène')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (388, 'fre', 'Sami du Sud')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (389, 'fre', 'Sami du Nord')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (390, 'fre', 'Sami, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (391, 'fre', 'Sami de Lule')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (392, 'fre', 'Sami d''Inari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (393, 'fre', 'Samoan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (394, 'fre', 'Sami skolt')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (395, 'fre', 'Shona')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (396, 'fre', 'Sindhi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (397, 'fre', 'Soninké')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (398, 'fre', 'Sogdien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (399, 'fre', 'Somali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (400, 'fre', 'Songhai, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (401, 'fre', 'Sotho du Sud')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (402, 'fre', 'Espagnol; castillan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (403, 'fre', 'Sarde')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (404, 'fre', 'Sranan tongo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (405, 'fre', 'Sérère')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (406, 'fre', 'Nilo-sahariennes, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (407, 'fre', 'Swati')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (408, 'fre', 'Sukuma')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (409, 'fre', 'Soundanais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (410, 'fre', 'Soussou')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (417, 'eng', 'Tamil')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (418, 'eng', 'Tatar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (419, 'eng', 'Telugu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (420, 'eng', 'Timne')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (421, 'eng', 'Tereno')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (422, 'eng', 'Tetum')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (423, 'eng', 'Tajik')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (424, 'eng', 'Tagalog')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (425, 'eng', 'Thai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (426, 'eng', 'Tibetan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (427, 'eng', 'Tigre')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (428, 'eng', 'Tigrinya')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (429, 'eng', 'Tiv')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (430, 'eng', 'Tokelau')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (431, 'eng', 'Klingon; tlhIngan-Hol')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (432, 'eng', 'Tlingit')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (433, 'eng', 'Tamashek')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (434, 'eng', 'Tonga (Nyasa)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (435, 'eng', 'Tonga (Tonga Islands)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (436, 'eng', 'Tok Pisin')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (437, 'eng', 'Tsimshian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (438, 'eng', 'Tswana')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (439, 'eng', 'Tsonga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (440, 'eng', 'Turkmen')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (441, 'eng', 'Tumbuka')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (442, 'eng', 'Tupi languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (443, 'eng', 'Turkish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (444, 'eng', 'Altaic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (445, 'eng', 'Tuvalu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (446, 'eng', 'Twi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (447, 'eng', 'Tuvinian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (448, 'eng', 'Udmurt')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (449, 'eng', 'Ugaritic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (450, 'eng', 'Uighur; Uyghur')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (451, 'eng', 'Ukrainian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (452, 'eng', 'Umbundu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (453, 'eng', 'Undetermined')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (454, 'eng', 'Urdu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (455, 'eng', 'Uzbek')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (456, 'eng', 'Vai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (416, 'fre', 'Thaïes, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (417, 'fre', 'Tamoul')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (418, 'fre', 'Tatar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (419, 'fre', 'Télougou')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (420, 'fre', 'Temne')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (421, 'fre', 'Tereno')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (422, 'fre', 'Tetum')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (423, 'fre', 'Tadjik')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (424, 'fre', 'Tagalog')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (425, 'fre', 'Thaï')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (426, 'fre', 'Tibétain')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (427, 'fre', 'Tigré')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (428, 'fre', 'Tigrigna')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (429, 'fre', 'Tiv')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (430, 'fre', 'Tokelau')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (431, 'fre', 'Klingon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (432, 'fre', 'Tlingit')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (433, 'fre', 'Tamacheq')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (434, 'fre', 'Tonga (Nyasa)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (435, 'fre', 'Tongan (Îles Tonga)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (436, 'fre', 'Tok pisin')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (437, 'fre', 'Tsimshian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (438, 'fre', 'Tswana')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (439, 'fre', 'Tsonga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (440, 'fre', 'Turkmène')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (441, 'fre', 'Tumbuka')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (442, 'fre', 'Tupi, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (443, 'fre', 'Turc')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (444, 'fre', 'Altaïques, autres langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (445, 'fre', 'Tuvalu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (446, 'fre', 'Twi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (447, 'fre', 'Touva')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (448, 'fre', 'Oudmourte')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (449, 'fre', 'Ougaritique')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (450, 'fre', 'Ouïgour')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (451, 'fre', 'Ukrainien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (452, 'fre', 'Umbundu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (453, 'fre', 'Indéterminée')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (454, 'fre', 'Ourdou')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (1, 'chi', 'Afar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (2, 'chi', 'Abkhazian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (3, 'chi', 'Achinese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (4, 'chi', 'Acoli')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (5, 'chi', 'Adangme')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (6, 'chi', 'Adyghe; Adygei')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (7, 'chi', 'Afro-Asiatic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (8, 'chi', 'Afrihili')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (9, 'chi', 'Afrikaans')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (10, 'chi', 'Ainu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (11, 'chi', 'Akan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (12, 'chi', 'Akkadian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (13, 'chi', 'Albanian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (14, 'chi', 'Aleut')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (15, 'chi', 'Algonquian languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (16, 'chi', 'Southern Altai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (17, 'chi', 'Amharic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (18, 'chi', 'English, Old (ca.450-1100)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (19, 'chi', 'Angika')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (20, 'chi', 'Apache languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (21, 'chi', 'Arabic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (22, 'chi', 'Aramaic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (23, 'chi', 'Aragonese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (24, 'chi', 'Armenian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (25, 'chi', 'Mapudungun; Mapuche')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (26, 'chi', 'Arapaho')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (27, 'chi', 'Artificial (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (28, 'chi', 'Arawak')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (29, 'chi', 'Assamese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (30, 'chi', 'Asturian; Bable')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (31, 'chi', 'Athapascan languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (32, 'chi', 'Australian languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (33, 'chi', 'Avaric')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (34, 'chi', 'Avestan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (35, 'chi', 'Awadhi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (36, 'chi', 'Aymara')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (37, 'chi', 'Azerbaijani')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (38, 'chi', 'Banda languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (39, 'chi', 'Bamileke languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (40, 'chi', 'Bashkir')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (41, 'chi', 'Baluchi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (42, 'chi', 'Bambara')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (43, 'chi', 'Balinese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (44, 'chi', 'Basque')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (45, 'chi', 'Basa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (46, 'chi', 'Baltic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (47, 'chi', 'Beja')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (48, 'chi', 'Belarusian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (49, 'chi', 'Bemba')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (50, 'chi', 'Bengali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (51, 'chi', 'Berber (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (52, 'chi', 'Bhojpuri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (53, 'chi', 'Bihari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (54, 'chi', 'Bikol')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (55, 'chi', 'Bini; Edo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (56, 'chi', 'Bislama')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (57, 'chi', 'Siksika')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (58, 'chi', 'Bantu (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (59, 'chi', 'Bosnian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (60, 'chi', 'Braj')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (61, 'chi', 'Breton')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (62, 'chi', 'Batak languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (63, 'chi', 'Buriat')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (64, 'chi', 'Buginese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (65, 'chi', 'Bulgarian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (66, 'chi', 'Burmese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (67, 'chi', 'Blin; Bilin')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (68, 'chi', 'Caddo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (69, 'chi', 'Central American Indian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (70, 'chi', 'Galibi Carib')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (71, 'chi', 'Catalan; Valencian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (72, 'chi', 'Caucasian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (73, 'chi', 'Cebuano')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (461, 'eng', 'Wakashan languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (462, 'eng', 'Walamo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (463, 'eng', 'Waray')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (464, 'eng', 'Washo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (465, 'eng', 'Welsh')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (466, 'eng', 'Sorbian languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (467, 'eng', 'Walloon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (468, 'eng', 'Wolof')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (469, 'eng', 'Kalmyk; Oirat')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (470, 'eng', 'Xhosa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (471, 'eng', 'Yao')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (472, 'eng', 'Yapese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (473, 'eng', 'Yiddish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (474, 'eng', 'Yoruba')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (475, 'eng', 'Yupik languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (476, 'eng', 'Zapotec')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (477, 'eng', 'Zenaga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (478, 'eng', 'Zhuang; Chuang')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (479, 'eng', 'Zande languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (480, 'eng', 'Zulu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (481, 'eng', 'Zuni')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (482, 'eng', 'No linguistic content')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (461, 'fre', 'Wakashennes, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (462, 'fre', 'Walamo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (463, 'fre', 'Waray')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (464, 'fre', 'Washo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (465, 'fre', 'Gallois')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (466, 'fre', 'Sorabes, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (467, 'fre', 'Wallon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (468, 'fre', 'Wolof')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (469, 'fre', 'Kalmouk; oïrat')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (470, 'fre', 'Xhosa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (471, 'fre', 'Yao')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (472, 'fre', 'Yapois')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (473, 'fre', 'Yiddish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (474, 'fre', 'Yoruba')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (475, 'fre', 'Yupik, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (476, 'fre', 'Zapotèque')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (477, 'fre', 'Zenaga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (478, 'fre', 'Zhuang; chuang')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (479, 'fre', 'Zandé, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (480, 'fre', 'Zoulou')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (481, 'fre', 'Zuni')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (482, 'fre', 'Pas de contenu linguistique')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (483, 'fre', 'N''ko')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (74, 'chi', 'Celtic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (75, 'chi', 'Chamorro')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (76, 'chi', 'Chibcha')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (77, 'chi', 'Chechen')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (78, 'chi', 'Chagatai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (79, 'chi', 'Chinese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (80, 'chi', 'Chuukese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (81, 'chi', 'Mari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (82, 'chi', 'Chinook jargon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (83, 'chi', 'Choctaw')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (84, 'chi', 'Chipewyan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (85, 'chi', 'Cherokee')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (86, 'chi', 'Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (87, 'chi', 'Chuvash')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (88, 'chi', 'Cheyenne')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (89, 'chi', 'Chamic languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (90, 'chi', 'Coptic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (91, 'chi', 'Cornish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (92, 'chi', 'Corsican')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (93, 'chi', 'Creoles and pidgins, English based (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (94, 'chi', 'Creoles and pidgins, French-based (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (95, 'chi', 'Creoles and pidgins, Portuguese-based (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (96, 'chi', 'Cree')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (97, 'chi', 'Crimean Tatar; Crimean Turkish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (98, 'chi', 'Creoles and pidgins (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (99, 'chi', 'Kashubian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (100, 'chi', 'Cushitic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (101, 'chi', 'Czech')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (102, 'chi', 'Dakota')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (103, 'chi', 'Danish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (104, 'chi', 'Dargwa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (105, 'chi', 'Land Dayak languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (106, 'chi', 'Delaware')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (107, 'chi', 'Slave (Athapascan)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (108, 'chi', 'Dogrib')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (109, 'chi', 'Dinka')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (110, 'chi', 'Divehi; Dhivehi; Maldivian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (111, 'chi', 'Dogri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (112, 'chi', 'Dravidian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (113, 'chi', 'Lower Sorbian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (114, 'chi', 'Duala')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (115, 'chi', 'Dutch, Middle (ca.1050-1350)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (116, 'chi', 'Dutch; Flemish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (117, 'chi', 'Dyula')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (118, 'chi', 'Dzongkha')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (119, 'chi', 'Efik')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (120, 'chi', 'Egyptian (Ancient)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (121, 'chi', 'Ekajuk')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (122, 'chi', 'Elamite')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (123, 'chi', 'English')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (124, 'chi', 'English, Middle (1100-1500)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (125, 'chi', 'Esperanto')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (126, 'chi', 'Estonian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (127, 'chi', 'Ewe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (128, 'chi', 'Ewondo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (129, 'chi', 'Fang')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (130, 'chi', 'Faroese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (131, 'chi', 'Fanti')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (132, 'chi', 'Fijian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (133, 'chi', 'Filipino; Pilipino')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (134, 'chi', 'Finnish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (135, 'chi', 'Finno-Ugrian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (136, 'chi', 'Fon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (137, 'chi', 'French')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (138, 'chi', 'French, Middle (ca.1400-1600)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (139, 'chi', 'French, Old (842-ca.1400)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (140, 'chi', 'Northern Frisian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (141, 'chi', 'Eastern Frisian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (142, 'chi', 'Western Frisian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (143, 'chi', 'Fulah')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (144, 'chi', 'Friulian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (145, 'chi', 'Ga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (146, 'chi', 'Gayo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (147, 'chi', 'Gbaya')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (148, 'chi', 'Germanic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (149, 'chi', 'Georgian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (150, 'chi', 'German')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (151, 'chi', 'Geez')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (152, 'chi', 'Gilbertese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (153, 'chi', 'Gaelic; Scottish Gaelic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (154, 'chi', 'Irish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (155, 'chi', 'Galician')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (156, 'chi', 'Manx')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (157, 'chi', 'German, Middle High (ca.1050-1500)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (158, 'chi', 'German, Old High (ca.750-1050)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (159, 'chi', 'Gondi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (160, 'chi', 'Gorontalo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (161, 'chi', 'Gothic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (162, 'chi', 'Grebo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (163, 'chi', 'Greek, Ancient (to 1453)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (164, 'chi', 'Greek, Modern (1453-)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (165, 'chi', 'Guarani')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (166, 'chi', 'Swiss German; Alemannic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (167, 'chi', 'Gujarati')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (168, 'chi', 'Gwich''in')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (169, 'chi', 'Haida')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (170, 'chi', 'Haitian; Haitian Creole')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (171, 'chi', 'Hausa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (172, 'chi', 'Hawaiian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (173, 'chi', 'Hebrew')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (174, 'chi', 'Herero')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (175, 'chi', 'Hiligaynon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (176, 'chi', 'Himachali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (177, 'chi', 'Hindi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (178, 'chi', 'Hittite')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (179, 'chi', 'Hmong')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (180, 'chi', 'Hiri Motu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (181, 'chi', 'Upper Sorbian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (182, 'chi', 'Hungarian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (183, 'chi', 'Hupa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (184, 'chi', 'Iban')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (185, 'chi', 'Igbo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (186, 'chi', 'Icelandic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (187, 'chi', 'Ido')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (188, 'chi', 'Sichuan Yi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (189, 'chi', 'Ijo languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (190, 'chi', 'Inuktitut')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (191, 'chi', 'Interlingue')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (192, 'chi', 'Iloko')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (193, 'chi', 'Interlingua (International Auxiliary Language Association)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (194, 'chi', 'Indic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (195, 'chi', 'Indonesian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (196, 'chi', 'Indo-European (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (197, 'chi', 'Ingush')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (198, 'chi', 'Inupiaq')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (199, 'chi', 'Iranian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (200, 'chi', 'Iroquoian languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (201, 'chi', 'Italian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (202, 'chi', 'Javanese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (203, 'chi', 'Lojban')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (204, 'chi', 'Japanese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (205, 'chi', 'Judeo-Persian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (206, 'chi', 'Judeo-Arabic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (207, 'chi', 'Kara-Kalpak')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (208, 'chi', 'Kabyle')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (209, 'chi', 'Kachin; Jingpho')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (210, 'chi', 'Kalaallisut; Greenlandic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (212, 'chi', 'Kannada')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (213, 'chi', 'Karen languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (214, 'chi', 'Kashmiri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (215, 'chi', 'Kanuri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (216, 'chi', 'Kawi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (217, 'chi', 'Kazakh')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (218, 'chi', 'Kabardian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (219, 'chi', 'Khasi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (220, 'chi', 'Khoisan (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (221, 'chi', 'Central Khmer')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (222, 'chi', 'Khotanese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (223, 'chi', 'Kikuyu; Gikuyu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (224, 'chi', 'Kinyarwanda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (225, 'chi', 'Kirghiz; Kyrgyz')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (226, 'chi', 'Kimbundu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (227, 'chi', 'Konkani')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (228, 'chi', 'Komi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (229, 'chi', 'Kongo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (230, 'chi', 'Korean')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (231, 'chi', 'Kosraean')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (232, 'chi', 'Kpelle')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (233, 'chi', 'Karachay-Balkar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (234, 'chi', 'Karelian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (235, 'chi', 'Kru languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (236, 'chi', 'Kurukh')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (237, 'chi', 'Kuanyama; Kwanyama')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (238, 'chi', 'Kumyk')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (239, 'chi', 'Kurdish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (240, 'chi', 'Kutenai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (241, 'chi', 'Ladino')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (242, 'chi', 'Lahnda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (243, 'chi', 'Lamba')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (244, 'chi', 'Lao')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (245, 'chi', 'Latin')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (246, 'chi', 'Latvian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (247, 'chi', 'Lezghian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (248, 'chi', 'Limburgan; Limburger; Limburgish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (249, 'chi', 'Lingala')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (250, 'chi', 'Lithuanian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (251, 'chi', 'Mongo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (252, 'chi', 'Lozi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (253, 'chi', 'Luxembourgish; Letzeburgesch')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (254, 'chi', 'Luba-Lulua')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (255, 'chi', 'Luba-Katanga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (256, 'chi', 'Ganda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (257, 'chi', 'Luiseno')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (258, 'chi', 'Lunda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (259, 'chi', 'Luo (Kenya and Tanzania)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (260, 'chi', 'Lushai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (261, 'chi', 'Macedonian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (262, 'chi', 'Madurese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (263, 'chi', 'Magahi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (264, 'chi', 'Marshallese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (265, 'chi', 'Maithili')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (266, 'chi', 'Makasar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (267, 'chi', 'Malayalam')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (268, 'chi', 'Mandingo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (269, 'chi', 'Maori')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (270, 'chi', 'Austronesian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (271, 'chi', 'Marathi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (272, 'chi', 'Masai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (273, 'chi', 'Malay')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (274, 'chi', 'Moksha')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (275, 'chi', 'Mandar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (276, 'chi', 'Mende')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (277, 'chi', 'Irish, Middle (900-1200)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (278, 'chi', 'Mi''kmaq; Micmac')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (279, 'chi', 'Minangkabau')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (280, 'chi', 'Miscellaneous languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (281, 'chi', 'Mon-Khmer (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (282, 'chi', 'Malagasy')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (283, 'chi', 'Maltese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (284, 'chi', 'Manchu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (285, 'chi', 'Manipuri')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (286, 'chi', 'Manobo languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (287, 'chi', 'Mohawk')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (288, 'chi', 'Moldavian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (289, 'chi', 'Mongolian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (290, 'chi', 'Mossi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (291, 'chi', 'Multiple languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (292, 'chi', 'Munda languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (293, 'chi', 'Creek')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (294, 'chi', 'Mirandese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (296, 'chi', 'Mayan languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (297, 'chi', 'Erzya')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (298, 'chi', 'Nahuatl languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (299, 'chi', 'North American Indian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (300, 'chi', 'Neapolitan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (301, 'chi', 'Nauru')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (302, 'chi', 'Navajo; Navaho')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (303, 'chi', 'Ndebele, South; South Ndebele')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (304, 'chi', 'Ndebele, North; North Ndebele')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (305, 'chi', 'Ndonga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (306, 'chi', 'Low German; Low Saxon; German, Low; Saxon, Low')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (307, 'chi', 'Nepali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (308, 'chi', 'Nepal Bhasa; Newari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (309, 'chi', 'Nias')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (310, 'chi', 'Niger-Kordofanian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (311, 'chi', 'Niuean')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (312, 'chi', 'Norwegian Nynorsk; Nynorsk, Norwegian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (313, 'chi', 'Bokmål, Norwegian; Norwegian Bokmål')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (314, 'chi', 'Nogai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (315, 'chi', 'Norse, Old')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (316, 'chi', 'Norwegian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (317, 'chi', 'Pedi; Sepedi; Northern Sotho')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (318, 'chi', 'Nubian languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (319, 'chi', 'Classical Newari; Old Newari; Classical Nepal Bhasa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (320, 'chi', 'Chichewa; Chewa; Nyanja')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (321, 'chi', 'Nyamwezi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (322, 'chi', 'Nyankole')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (323, 'chi', 'Nyoro')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (324, 'chi', 'Nzima')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (325, 'chi', 'Occitan (post 1500); Provençal')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (326, 'chi', 'Ojibwa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (327, 'chi', 'Oriya')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (328, 'chi', 'Oromo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (329, 'chi', 'Osage')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (330, 'chi', 'Ossetian; Ossetic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (331, 'chi', 'Turkish, Ottoman (1500-1928)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (332, 'chi', 'Otomian languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (333, 'chi', 'Papuan (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (334, 'chi', 'Pangasinan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (335, 'chi', 'Pahlavi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (336, 'chi', 'Pampanga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (337, 'chi', 'Panjabi; Punjabi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (338, 'chi', 'Papiamento')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (339, 'chi', 'Palauan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (340, 'chi', 'Persian, Old (ca.600-400 B.C.)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (341, 'chi', 'Persian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (342, 'chi', 'Philippine (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (343, 'chi', 'Phoenician')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (344, 'chi', 'Pali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (345, 'chi', 'Polish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (346, 'chi', 'Pohnpeian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (347, 'chi', 'Portuguese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (348, 'chi', 'Prakrit languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (349, 'chi', 'Provençal, Old (to 1500)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (350, 'chi', 'Pushto')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (351, 'chi', 'Reserved for local use')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (352, 'chi', 'Quechua')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (353, 'chi', 'Rajasthani')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (354, 'chi', 'Rapanui')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (355, 'chi', 'Rarotongan; Cook Islands Maori')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (356, 'chi', 'Romance (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (357, 'chi', 'Romansh')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (358, 'chi', 'Romany')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (359, 'chi', 'Romanian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (360, 'chi', 'Rundi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (361, 'chi', 'Aromanian; Arumanian; Macedo-Romanian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (362, 'chi', 'Russian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (363, 'chi', 'Sandawe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (364, 'chi', 'Sango')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (365, 'chi', 'Yakut')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (366, 'chi', 'South American Indian (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (367, 'chi', 'Salishan languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (368, 'chi', 'Samaritan Aramaic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (369, 'chi', 'Sanskrit')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (370, 'chi', 'Sasak')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (371, 'chi', 'Santali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (372, 'chi', 'Serbian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (373, 'chi', 'Sicilian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (374, 'chi', 'Scots')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (375, 'chi', 'Croatian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (376, 'chi', 'Selkup')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (377, 'chi', 'Semitic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (378, 'chi', 'Irish, Old (to 900)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (379, 'chi', 'Sign Languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (380, 'chi', 'Shan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (381, 'chi', 'Sidamo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (382, 'chi', 'Sinhala; Sinhalese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (383, 'chi', 'Siouan languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (384, 'chi', 'Sino-Tibetan (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (385, 'chi', 'Slavic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (386, 'chi', 'Slovak')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (387, 'chi', 'Slovenian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (388, 'chi', 'Southern Sami')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (389, 'chi', 'Northern Sami')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (390, 'chi', 'Sami languages (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (391, 'chi', 'Lule Sami')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (392, 'chi', 'Inari Sami')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (393, 'chi', 'Samoan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (394, 'chi', 'Skolt Sami')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (395, 'chi', 'Shona')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (396, 'chi', 'Sindhi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (397, 'chi', 'Soninke')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (398, 'chi', 'Sogdian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (399, 'chi', 'Somali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (400, 'chi', 'Songhai languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (401, 'chi', 'Sotho, Southern')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (402, 'chi', 'Spanish; Castilian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (403, 'chi', 'Sardinian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (404, 'chi', 'Sranan Tongo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (405, 'chi', 'Serer')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (406, 'chi', 'Nilo-Saharan (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (407, 'chi', 'Swati')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (408, 'chi', 'Sukuma')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (409, 'chi', 'Sundanese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (410, 'chi', 'Susu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (411, 'chi', 'Sumerian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (412, 'chi', 'Swahili')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (413, 'chi', 'Swedish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (414, 'chi', 'Syriac')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (415, 'chi', 'Tahitian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (416, 'chi', 'Tai (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (417, 'chi', 'Tamil')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (418, 'chi', 'Tatar')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (419, 'chi', 'Telugu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (420, 'chi', 'Timne')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (421, 'chi', 'Tereno')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (422, 'chi', 'Tetum')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (423, 'chi', 'Tajik')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (424, 'chi', 'Tagalog')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (425, 'chi', 'Thai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (426, 'chi', 'Tibetan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (427, 'chi', 'Tigre')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (428, 'chi', 'Tigrinya')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (429, 'chi', 'Tiv')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (430, 'chi', 'Tokelau')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (431, 'chi', 'Klingon; tlhIngan-Hol')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (432, 'chi', 'Tlingit')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (433, 'chi', 'Tamashek')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (434, 'chi', 'Tonga (Nyasa)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (435, 'chi', 'Tonga (Tonga Islands)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (436, 'chi', 'Tok Pisin')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (437, 'chi', 'Tsimshian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (438, 'chi', 'Tswana')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (439, 'chi', 'Tsonga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (440, 'chi', 'Turkmen')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (441, 'chi', 'Tumbuka')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (442, 'chi', 'Tupi languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (443, 'chi', 'Turkish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (444, 'chi', 'Altaic (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (445, 'chi', 'Tuvalu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (446, 'chi', 'Twi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (447, 'chi', 'Tuvinian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (448, 'chi', 'Udmurt')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (449, 'chi', 'Ugaritic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (450, 'chi', 'Uighur; Uyghur')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (451, 'chi', 'Ukrainian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (452, 'chi', 'Umbundu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (453, 'chi', 'Undetermined')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (454, 'chi', 'Urdu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (455, 'chi', 'Uzbek')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (456, 'chi', 'Vai')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (457, 'chi', 'Venda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (458, 'chi', 'Vietnamese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (459, 'chi', 'Volapük')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (460, 'chi', 'Votic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (461, 'chi', 'Wakashan languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (462, 'chi', 'Walamo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (463, 'chi', 'Waray')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (464, 'chi', 'Washo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (465, 'chi', 'Welsh')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (466, 'chi', 'Sorbian languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (467, 'chi', 'Walloon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (468, 'chi', 'Wolof')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (469, 'chi', 'Kalmyk; Oirat')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (470, 'chi', 'Xhosa')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (471, 'chi', 'Yao')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (472, 'chi', 'Yapese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (473, 'chi', 'Yiddish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (474, 'chi', 'Yoruba')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (475, 'chi', 'Yupik languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (476, 'chi', 'Zapotec')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (477, 'chi', 'Zenaga')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (478, 'chi', 'Zhuang; Chuang')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (479, 'chi', 'Zande languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (480, 'chi', 'Zulu')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (481, 'chi', 'Zuni')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (482, 'chi', 'No linguistic content')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (483, 'chi', 'N''Ko')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (484, 'chi', 'Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (39, 'eng', 'Bamileke languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (40, 'eng', 'Bashkir')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (41, 'eng', 'Baluchi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (42, 'eng', 'Bambara')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (43, 'eng', 'Balinese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (81, 'eng', 'Mari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (82, 'eng', 'Chinook jargon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (83, 'eng', 'Choctaw')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (84, 'eng', 'Chipewyan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (85, 'eng', 'Cherokee')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (86, 'eng', 'Church Slavic; Old Slavonic; Church Slavonic; Old Bulgarian; Old Church Slavonic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (125, 'eng', 'Esperanto')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (123, 'eng', 'English')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (124, 'eng', 'English, Middle (1100-1500)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (164, 'eng', 'Greek, Modern (1453-)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (165, 'eng', 'Guarani')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (166, 'eng', 'Swiss German; Alemannic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (205, 'eng', 'Judeo-Persian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (206, 'eng', 'Judeo-Arabic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (207, 'eng', 'Kara-Kalpak')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (208, 'eng', 'Kabyle')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (209, 'eng', 'Kachin; Jingpho')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (249, 'eng', 'Lingala')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (250, 'eng', 'Lithuanian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (251, 'eng', 'Mongo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (252, 'eng', 'Lozi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (291, 'eng', 'Multiple languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (292, 'eng', 'Munda languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (293, 'eng', 'Creek')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (294, 'eng', 'Mirandese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (295, 'eng', 'Marwari')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (330, 'eng', 'Ossetian; Ossetic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (331, 'eng', 'Turkish, Ottoman (1500-1928)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (332, 'eng', 'Otomian languages')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (370, 'eng', 'Sasak')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (371, 'eng', 'Santali')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (372, 'eng', 'Serbian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (373, 'eng', 'Sicilian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (412, 'eng', 'Swahili')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (413, 'eng', 'Swedish')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (414, 'eng', 'Syriac')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (415, 'eng', 'Tahitian')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (416, 'eng', 'Tai (Other)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (457, 'eng', 'Venda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (458, 'eng', 'Vietnamese')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (459, 'eng', 'Volapük')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (460, 'eng', 'Votic')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (483, 'eng', 'N''Ko')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (484, 'eng', 'Zaza; Dimili; Dimli; Kirdki; Kirmanjki; Zazaki')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (39, 'fre', 'Bamilékés, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (40, 'fre', 'Bachkir')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (41, 'fre', 'Baloutchi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (42, 'fre', 'Bambara')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (82, 'fre', 'Chinook, jargon')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (83, 'fre', 'Choctaw')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (84, 'fre', 'Chipewyan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (85, 'fre', 'Cherokee')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (120, 'fre', 'Égyptien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (121, 'fre', 'Ekajuk')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (122, 'fre', 'Élamite')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (123, 'fre', 'Anglais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (124, 'fre', 'Anglais moyen (1100-1500)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (160, 'fre', 'Gorontalo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (161, 'fre', 'Gothique')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (162, 'fre', 'Grebo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (163, 'fre', 'Grec ancien (jusqu''à 1453)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (164, 'fre', 'Grec moderne (après 1453)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (165, 'fre', 'Guarani')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (166, 'fre', 'Alémanique')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (205, 'fre', 'Judéo-persan')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (206, 'fre', 'Judéo-arabe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (207, 'fre', 'Karakalpak')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (208, 'fre', 'Kabyle')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (248, 'fre', 'Limbourgeois')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (249, 'fre', 'Lingala')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (250, 'fre', 'Lituanien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (251, 'fre', 'Mongo')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (252, 'fre', 'Lozi')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (292, 'fre', 'Mounda, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (293, 'fre', 'Muskogee')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (294, 'fre', 'Mirandais')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (331, 'fre', 'Turc ottoman (1500-1928)')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (332, 'fre', 'Otomangue, langues')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (369, 'fre', 'Sanskrit')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (370, 'fre', 'Sasak')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (371, 'fre', 'Santal')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (372, 'fre', 'Serbe')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (373, 'fre', 'Sicilien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (411, 'fre', 'Sumérien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (412, 'fre', 'Swahili')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (413, 'fre', 'Suédois')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (414, 'fre', 'Syriaque')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (415, 'fre', 'Tahitien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (455, 'fre', 'Ouszbek')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (456, 'fre', 'Vaï')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (457, 'fre', 'Venda')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (458, 'fre', 'Vietnamien')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (459, 'fre', 'Volapük')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (460, 'fre', 'Vote')");
        $this->addSql("INSERT INTO public.isolanguagesdes (iddes, langid, label) VALUES (484, 'fre', 'Zaza; dimili; dimli; kirdki; kirmanjki; zazaki')");

        $this->addSql("INSERT INTO public.statusvalues (id, name, reserved, displayorder) VALUES (0, 'unknown', 'y', 0)");
        $this->addSql("INSERT INTO public.statusvalues (id, name, reserved, displayorder) VALUES (1, 'draft', 'y', 1)");
        $this->addSql("INSERT INTO public.statusvalues (id, name, reserved, displayorder) VALUES (2, 'approved', 'y', 3)");
        $this->addSql("INSERT INTO public.statusvalues (id, name, reserved, displayorder) VALUES (3, 'retired', 'y', 5)");
        $this->addSql("INSERT INTO public.statusvalues (id, name, reserved, displayorder) VALUES (4, 'submitted', 'y', 2)");
        $this->addSql("INSERT INTO public.statusvalues (id, name, reserved, displayorder) VALUES (5, 'rejected', 'y', 4)");

        $this->addSql("INSERT INTO public.operations (id, name) VALUES (0, 'view')");
        $this->addSql("INSERT INTO public.operations (id, name) VALUES (1, 'download')");
        $this->addSql("INSERT INTO public.operations (id, name) VALUES (3, 'notify')");
        $this->addSql("INSERT INTO public.operations (id, name) VALUES (5, 'dynamic')");
        $this->addSql("INSERT INTO public.operations (id, name) VALUES (6, 'featured')");
        $this->addSql("INSERT INTO public.operations (id, name) VALUES (2, 'editing')");

        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 12, 0)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 14, 0)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 10, 5)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 10, 0)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 10, 1)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 9, 5)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 9, 0)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 9, 1)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 40020, 5)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 40020, 0)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 40020, 1)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 40021, 5)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 40021, 0)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 40021, 1)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 40022, 5)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 40022, 0)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (1, 40022, 1)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (217, 9, 2)");
        $this->addSql("INSERT INTO public.operationallowed (groupid, metadataid, operationid) VALUES (217, 10, 2)");

        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (0, 'chi', 'Publish')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (1, 'chi', 'Download')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (3, 'chi', 'Notify')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (5, 'chi', 'Interactive Map')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (6, 'chi', 'Featured')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (0, 'eng', 'Publish')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (1, 'eng', 'Download')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (3, 'eng', 'Notify')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (5, 'eng', 'Interactive Map')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (6, 'eng', 'Featured')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (2, 'eng', 'Editing')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (0, 'fre', 'Publish')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (1, 'fre', 'Télécharger')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (3, 'fre', 'Notifier')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (5, 'fre', 'Interactive Map')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (6, 'fre', 'Epingler')");
        $this->addSql("INSERT INTO public.operationsdes (iddes, langid, label) VALUES (2, 'fre', 'Editer')");

        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40006, '2015-03-30T13:19:42', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (1, '2012-04-11T12:22:00', '192.168.0.216', '+(_op0:1 _op2:1) +_isTemplate:n', 2, 'fr', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (4, '2012-04-11T13:30:03', '192.168.0.216', '+(_op0:1 _op2:1) +_isTemplate:n', 2, 'fr', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (11, '2012-04-11T13:55:39', '192.168.0.209', '+(_op0:1 _op2:1) +_isTemplate:n', 2, 'fr', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (65, '2013-06-26T17:40:35', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n', 2, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (66, '2013-06-26T17:40:39', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (127, '2013-07-01T12:00:19', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +type:service +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', 'null ASC', NULL, '', 'mef.export', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (14, '2012-04-11T13:55:59', '192.168.0.209', '+(_op0:1 _op2:1) +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fr', 'null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (15, '2012-04-11T13:56:19', '192.168.0.209', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:service +serviceType:invoke +_isTemplate:n', 1, 'fr', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (16, '2012-04-11T13:56:44', '192.168.0.209', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +serviceType:invoke +type:service +_isTemplate:n', 1, 'fr', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (17, '2012-04-11T13:57:33', '192.168.0.209', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +(type:dataset type:series) +_isTemplate:n', 1, 'fr', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (18, '2012-04-11T13:57:45', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fr', 'null ASC', NULL, '', 'mef.export', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (19, '2012-04-11T13:57:45', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +type:service +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fr', 'null ASC', NULL, '', 'mef.export', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (20, '2012-04-11T13:59:25', '192.168.0.209', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +type:dataset +_isTemplate:n', 1, 'fr', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (22, '2012-04-11T13:59:44', '192.168.0.209', '+(_op0:1 _op2:1) +keyword:Unités administratives +_isTemplate:n', 1, 'fr', 'null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (24, '2012-04-11T14:26:54', '192.168.0.209', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +(type:dataset type:series) +_isTemplate:n', 1, 'fr', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (27, '2013-06-26T17:34:47', '193.251.20.2', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +(type:dataset type:series type:service type:nonGeographicDataset) +_isTemplate:n', 4, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (28, '2013-06-26T17:37:04', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (29, '2013-06-26T17:37:05', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (30, '2013-06-26T17:37:05', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +hasfeaturecat:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (31, '2013-06-26T17:37:07', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', 'null ASC', NULL, '', 'mef.export', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (32, '2013-06-26T17:37:07', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +type:service +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', 'null ASC', NULL, '', 'mef.export', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (33, '2013-06-26T17:38:12', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (34, '2013-06-26T17:38:12', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (35, '2013-06-26T17:38:12', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +hasfeaturecat:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (36, '2013-06-26T17:38:13', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', 'null ASC', NULL, '', 'mef.export', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (37, '2013-06-26T17:38:13', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +type:service +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', 'null ASC', NULL, '', 'mef.export', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (130, '2013-07-01T16:17:09', '193.251.20.2', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +(type:dataset type:series type:service type:nonGeographicDataset) +_isTemplate:n', 4, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (38, '2013-06-26T17:39:32', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (39, '2013-06-26T17:39:32', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40, '2013-06-26T17:39:32', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (41, '2013-06-26T17:39:32', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40010, '2015-03-30T13:20:14', '172.16.64.216', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +(type:dataset type:series type:service type:nonGeographicDataset) +_isTemplate:n', 5, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (116, '2013-07-01T11:29:24', '193.251.20.2', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (2, '2012-04-11T12:22:01', '192.168.0.216', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +(type:dataset type:series type:service type:nonGeographicDataset) +_isTemplate:n', 2, 'fr', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (42, '2013-06-26T17:39:44', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n', 2, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (43, '2013-06-26T17:39:50', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n', 2, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (44, '2013-06-26T17:39:54', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (85, '2013-06-26T17:42:05', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (94, '2013-06-26T17:42:30', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n', 2, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (95, '2013-06-26T17:42:36', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (100, '2013-06-26T17:42:46', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (111, '2013-06-26T17:45:02', '193.251.20.2', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n', 2, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (83, '2013-06-26T17:41:20', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (84, '2013-06-26T17:42:01', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n', 2, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (112, '2013-06-26T17:45:42', '193.251.20.2', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (113, '2013-07-01T10:48:45', '193.251.20.2', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (115, '2013-07-01T11:20:09', '192.168.1.254', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (120, '2013-07-01T11:31:30', '193.251.20.2', '+(_op0:1 _op2:1) +_isTemplate:n', 5, 'fre', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (121, '2013-07-01T11:54:42', '193.251.20.2', '+(_op0:1 _op2:1) +_isTemplate:n', 5, 'fre', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (128, '2013-07-01T16:16:22', '193.251.20.2', '+(_op0:1 _op2:1) +_isTemplate:n', 5, 'fre', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (129, '2013-07-01T16:17:07', '193.251.20.2', '+(_op0:1 _op2:1) +_isTemplate:n', 5, 'fre', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (105, '2013-06-26T17:43:02', '193.251.20.2', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +(type:dataset type:series type:serv) +_isTemplate:n', 2, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (110, '2013-06-26T17:44:36', '193.251.20.2', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n', 2, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (114, '2013-07-01T10:53:23', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:Demonstration +type:map +_isTemplate:n', 1, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (117, '2013-07-01T11:30:56', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:Demonstration +type:dataset +_isTemplate:n', 2, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (119, '2013-07-01T11:31:14', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:Demonstration +type:dataset +_isTemplate:n', 1, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (118, '2013-07-01T11:31:03', '193.251.20.2', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (122, '2013-07-01T11:54:43', '193.251.20.2', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +(type:dataset type:series type:service type:nonGeographicDataset) +_isTemplate:n', 4, 'fre', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (123, '2013-07-01T12:00:18', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (124, '2013-07-01T12:00:18', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (125, '2013-07-01T12:00:19', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +hasfeaturecat:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (126, '2013-07-01T12:00:19', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', 'null ASC', NULL, '', 'mef.export', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (3, '2012-04-11T13:29:58', '192.168.0.216', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +(type:dataset type:series) +_isTemplate:n', 1, 'fr', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (5, '2012-04-11T13:32:23', '192.168.0.209', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +type:service +serviceType:invoke +_isTemplate:n', 1, 'fr', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (6, '2012-04-11T13:32:35', '192.168.0.209', '+(_op0:1 _op2:1) +parentUuid:617af7f0-0a6f-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fr', 'null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (7, '2012-04-11T13:32:35', '192.168.0.209', '+(_op0:1 _op2:1) +operatesOn:617af7f0-0a6f-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fr', 'null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (8, '2012-04-11T13:35:16', '192.168.0.209', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:Demonstration +type:dataset +_isTemplate:n', 1, 'fr', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (9, '2012-04-11T13:49:11', '192.168.0.209', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:-1 _op2:-1 _owner:2) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:Demonstration +serviceType:invoke +type:service +_isTemplate:n', 1, 'fr', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (12, '2012-04-11T13:55:56', '192.168.0.209', '+(_op0:1 _op2:1) +_source:7fc45be3-9aba-4198-920c-b8737112d522 +keyword:France +type:dataset +_isTemplate:n', 1, 'fr', '_title ASC,null ASC', NULL, '', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (13, '2012-04-11T13:55:59', '192.168.0.209', '+(_op0:1 _op2:1) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fr', 'null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (49, '2013-06-26T17:40:03', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (50, '2013-06-26T17:40:03', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (51, '2013-06-26T17:40:03', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +hasfeaturecat:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'xml.relation', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (52, '2013-06-26T17:40:03', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', 'null ASC', NULL, '', 'mef.export', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (53, '2013-06-26T17:40:03', '127.0.0.1', '+(_op0:1 _op2:1 _op0:0 _op2:0) +type:service +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', 'null ASC', NULL, '', 'mef.export', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (21, '2012-04-11T13:59:37', '192.168.0.209', '+(_op0:1 _op2:1) +_isTemplate:n', 2, 'fr', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (45, '2013-06-26T17:39:56', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (46, '2013-06-26T17:39:56', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (47, '2013-06-26T17:39:57', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (48, '2013-06-26T17:39:57', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (54, '2013-06-26T17:40:20', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +parentUuid:274971da-ef09-4e3b-9a5e-b236fd670874 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (55, '2013-06-26T17:40:20', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +operatesOn:274971da-ef09-4e3b-9a5e-b236fd670874 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (56, '2013-06-26T17:40:20', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:274971da-ef09-4e3b-9a5e-b236fd670874 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (57, '2013-06-26T17:40:20', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (58, '2013-06-26T17:40:20', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (59, '2013-06-26T17:40:20', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +_uuid:null +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (60, '2013-06-26T17:40:20', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (61, '2013-06-26T17:40:21', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (62, '2013-06-26T17:40:21', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (63, '2013-06-26T17:40:21', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (64, '2013-06-26T17:40:21', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (67, '2013-06-26T17:40:44', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (68, '2013-06-26T17:40:44', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (69, '2013-06-26T17:40:44', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (70, '2013-06-26T17:40:44', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (72, '2013-06-26T17:40:55', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (73, '2013-06-26T17:40:55', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (74, '2013-06-26T17:40:55', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (77, '2013-06-26T17:41:12', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +_uuid:null +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (78, '2013-06-26T17:41:12', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (98, '2013-06-26T17:42:41', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (99, '2013-06-26T17:42:41', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (103, '2013-06-26T17:42:49', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (104, '2013-06-26T17:42:49', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40047, '2017-04-27T11:39:17', '172.16.64.209', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (71, '2013-06-26T17:40:55', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (75, '2013-06-26T17:41:12', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (76, '2013-06-26T17:41:12', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (79, '2013-06-26T17:41:13', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (80, '2013-06-26T17:41:13', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (81, '2013-06-26T17:41:13', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +_uuid:null +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (82, '2013-06-26T17:41:13', '127.0.0.1', '+(_op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _owner:2) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (86, '2013-06-26T17:42:16', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (87, '2013-06-26T17:42:17', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (88, '2013-06-26T17:42:17', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (89, '2013-06-26T17:42:17', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (90, '2013-06-26T17:42:17', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:27879905-936b-41ba-87d2-7f843cd5d1eb +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (91, '2013-06-26T17:42:17', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:27879905-936b-41ba-87d2-7f843cd5d1eb +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (92, '2013-06-26T17:42:17', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 1, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (93, '2013-06-26T17:42:17', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:27879905-936b-41ba-87d2-7f843cd5d1eb +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (96, '2013-06-26T17:42:41', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (97, '2013-06-26T17:42:41', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 1, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (101, '2013-06-26T17:42:49', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (102, '2013-06-26T17:42:49', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 1, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (106, '2013-06-26T17:43:09', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +parentUuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (107, '2013-06-26T17:43:09', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +operatesOn:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 1, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (108, '2013-06-26T17:43:09', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_uuid:null +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (109, '2013-06-26T17:43:09', '127.0.0.1', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +hasfeaturecat:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, '', 'metadata.show', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (10, '2012-04-11T13:55:04', '192.168.0.209', '+(_op0:3 _op2:3 _op0:2 _op2:2 _op0:1 _op2:1 _op0:0 _op2:0 _op0:-1 _op2:-1 _op0:4 _op2:4 _owner:1 _dummy:0) +_isTemplate:s', 2, 'fr', 'null ASC', NULL, 'all', 'xml.search', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (23, '2012-04-11T13:59:51', '192.168.0.209', '+(_op0:1 _op2:1) +(type:dataset type:series type:service type:nonGeographicDataset) +_isTemplate:n', 2, 'fr', '_title ASC,null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (25, '2013-06-26T17:33:56', '193.251.20.2', '+(_op0:1 _op2:1) +_isTemplate:n', 4, 'fre', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (26, '2013-06-26T17:34:45', '193.251.20.2', '+(_op0:1 _op2:1) +_isTemplate:n', 4, 'fre', 'null ASC', NULL, 'all', 'q', NULL, NULL)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40019, '2017-04-26T10:51:18', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40023, '2017-04-26T11:04:02', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40029, '2017-04-27T11:39:01', '172.16.64.209', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40027, '2017-04-27T11:39:01', '172.16.64.209', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40028, '2017-04-27T11:39:01', '172.16.64.209', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', '_popularity DESC,null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40039, '2017-04-27T11:39:17', '172.16.64.209', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40043, '2017-04-27T11:39:17', '172.16.64.209', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40051, '2017-04-27T11:39:17', '172.16.64.209', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', '_popularity DESC,null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40090, '2017-04-27T11:44:53', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40103, '2017-04-27T11:50:17', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40152, '2017-04-27T11:59:12', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40218, '2017-04-27T11:59:19', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hassource:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40231, '2017-04-27T11:59:19', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hasfeaturecat:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40256, '2017-04-27T12:00:57', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40308, '2017-04-27T12:01:21', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:y', 9, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40571, '2017-04-27T12:05:04', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +operatesOn:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40637, '2017-04-27T12:05:04', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +agg_associated:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40055, '2017-04-27T11:39:24', '172.16.64.209', '+(_op0:1 _op2:1) +parentUuid:1c661f3e-33b5-4410-aa0d-c85f21a8ae7d +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40060, '2017-04-27T11:39:24', '172.16.64.209', '+(_op0:1 _op2:1) +operatesOn:1c661f3e-33b5-4410-aa0d-c85f21a8ae7d +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40067, '2017-04-27T11:39:24', '172.16.64.209', '+(_op0:1 _op2:1) +_uuid:7a8db1d7-f5cb-455d-a911-78f93418a0ce +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40065, '2017-04-27T11:39:24', '172.16.64.209', '+(_op0:1 _op2:1) +hasfeaturecat:1c661f3e-33b5-4410-aa0d-c85f21a8ae7d +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40075, '2017-04-27T11:39:24', '172.16.64.209', '+(_op0:1 _op2:1) +agg_associated:1c661f3e-33b5-4410-aa0d-c85f21a8ae7d +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40080, '2017-04-27T11:39:24', '172.16.64.209', '+(_op0:1 _op2:1) +hasfeaturecat:1c661f3e-33b5-4410-aa0d-c85f21a8ae7d +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40349, '2017-04-27T12:01:48', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +parentUuid:c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40362, '2017-04-27T12:01:48', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +agg_associated:c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40375, '2017-04-27T12:01:48', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +operatesOn:c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40388, '2017-04-27T12:01:48', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_uuid:-- Identifiant de la métadonnée de couche associée -- +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40401, '2017-04-27T12:01:48', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hassource:c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40414, '2017-04-27T12:01:48', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hasfeaturecat:c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40495, '2017-04-27T12:02:28', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40094, '2017-04-27T11:44:53', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40116, '2017-04-27T11:50:17', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40179, '2017-04-27T11:59:19', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +parentUuid:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40205, '2017-04-27T11:59:19', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +operatesOn:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40282, '2017-04-27T12:01:10', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)', 16, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40296, '2017-04-27T12:01:11', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40427, '2017-04-27T12:01:53', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)', 16, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40441, '2017-04-27T12:02:02', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_id:40085 +(_isTemplate:y _isTemplate:n _isTemplate:s)', 1, 'fre', 'null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40456, '2017-04-27T12:02:07', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)', 16, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40470, '2017-04-27T12:02:27', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40482, '2017-04-27T12:02:27', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', '_popularity DESC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40494, '2017-04-27T12:02:28', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40098, '2017-04-27T11:44:53', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', '_popularity DESC,null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40115, '2017-04-27T11:50:17', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', '_popularity DESC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40192, '2017-04-27T11:59:19', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +agg_associated:3c892ef0-0a6d-11de-ad6b-00104b7907b4 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40244, '2017-04-27T12:00:08', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40268, '2017-04-27T12:01:05', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)', 16, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40320, '2017-04-27T12:01:41', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)', 16, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40334, '2017-04-27T12:01:46', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_id:40086 +(_isTemplate:y _isTemplate:n _isTemplate:s)', 1, 'fre', 'null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40558, '2017-04-27T12:05:04', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +parentUuid:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40139, '2017-04-27T11:51:39', '172.16.64.209', '+(+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n) +ConstantScore(type_facet:typedataset)^0.0', 1, 'fre', 'null ASC', NULL, 'MATCH_ALL_DOCS', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40140, '2017-04-27T11:51:53', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40164, '2017-04-27T11:59:12', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_id:9 +(_isTemplate:y _isTemplate:n _isTemplate:s)', 1, 'fre', 'null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40518, '2017-04-27T12:04:53', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)', 16, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40532, '2017-04-27T12:04:55', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40544, '2017-04-27T12:05:01', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n) +_uuid:9ba5f3e9-c0ef-4160-81c0-b633b6067601', 1, 'fre', 'null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40580, '2017-04-27T12:05:04', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +agg_associated:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40579, '2017-04-27T12:05:04', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hasfeaturecat:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40610, '2017-04-27T12:05:04', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hasfeaturecat:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40623, '2017-04-27T12:05:04', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +parentUuid:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40640, '2017-04-27T12:05:04', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +operatesOn:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40636, '2017-04-27T12:05:04', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hasfeaturecat:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40674, '2017-04-27T12:05:04', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +hasfeaturecat:9ba5f3e9-c0ef-4160-81c0-b633b6067601 +_isTemplate:n', 0, 'fre', '_title ASC,null ASC', NULL, 'TERM', 'Api', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40688, '2017-04-27T12:05:05', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40689, '2017-04-27T12:05:05', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', '_popularity DESC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40712, '2017-04-27T12:05:05', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40724, '2017-04-27T12:05:17', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +(_isTemplate:y _isTemplate:n _isTemplate:s)', 16, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40738, '2017-04-27T12:05:18', '172.16.64.209', '+(_op0:-1 _op2:-1 _op0:0 _op2:0 _op0:1 _op2:1 _op0:217 _op2:217 _owner:40102 _dummy:0) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, false)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40750, '2019-05-24T11:18:39', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40751, '2019-05-24T11:18:39', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40752, '2019-05-24T11:18:39', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', '_popularity DESC,null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40762, '2019-05-24T12:01:59', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'qi', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40774, '2019-05-24T12:01:59', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', 'null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40763, '2019-05-24T12:01:59', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', '_popularity DESC,null ASC', NULL, 'TERM', 'q', false, true)");
        $this->addSql("INSERT INTO public.requests (id, requestdate, ip, query, hits, lang, sortby, spatialfilter, type, service, autogenerated, simple) VALUES (40764, '2019-05-24T12:01:59', '172.16.64.216', '+(_op0:1 _op2:1) +_isTemplate:n', 6, 'fre', '_changeDate DESC,null ASC', NULL, 'TERM', 'q', false, true)");

        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (1, 2, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (2, 3, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (3, 3, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (4, 5, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (5, 5, 1, 'serviceType', 'invoke', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (6, 6, 1, 'parentUuid', '617af7f0-0a6f-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (7, 7, 1, 'operatesOn', '617af7f0-0a6f-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (8, 8, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (9, 8, 1, 'keyword', 'Demonstration', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (10, 9, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (11, 9, 1, 'keyword', 'Demonstration', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (12, 9, 1, 'serviceType', 'invoke', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (13, 12, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (14, 12, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (15, 13, 1, 'parentUuid', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (16, 14, 1, 'operatesOn', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (17, 15, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (18, 15, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (19, 15, 1, 'serviceType', 'invoke', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (20, 16, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (21, 16, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (22, 16, 1, 'serviceType', 'invoke', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (23, 17, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (24, 17, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (25, 18, 1, 'parentUuid', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (26, 19, 1, 'operatesOn', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (27, 20, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (28, 22, 1, 'keyword', 'Unités administratives', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (29, 24, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (30, 24, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (31, 27, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (32, 28, 1, 'parentUuid', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (33, 29, 1, 'operatesOn', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (34, 30, 1, 'hasfeaturecat', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (35, 31, 1, 'parentUuid', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (36, 32, 1, 'operatesOn', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (37, 33, 1, 'parentUuid', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (38, 34, 1, 'operatesOn', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (39, 35, 1, 'hasfeaturecat', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40, 36, 1, 'parentUuid', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (41, 37, 1, 'operatesOn', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (42, 42, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (43, 42, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (44, 43, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (45, 43, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (46, 44, 1, 'hasfeaturecat', '7a8db1d7-f5cb-455d-a911-78f93418a0ce', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (47, 49, 1, 'parentUuid', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (48, 50, 1, 'operatesOn', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (49, 51, 1, 'hasfeaturecat', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (50, 52, 1, 'parentUuid', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (51, 53, 1, 'operatesOn', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (52, 65, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (53, 65, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (54, 66, 1, 'hasfeaturecat', '7a8db1d7-f5cb-455d-a911-78f93418a0ce', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (55, 83, 1, 'hasfeaturecat', '7a8db1d7-f5cb-455d-a911-78f93418a0ce', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (56, 84, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (57, 84, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (58, 85, 1, 'hasfeaturecat', '7a8db1d7-f5cb-455d-a911-78f93418a0ce', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (59, 94, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (60, 94, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (61, 95, 1, 'hasfeaturecat', '7a8db1d7-f5cb-455d-a911-78f93418a0ce', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (62, 100, 1, 'hasfeaturecat', '7a8db1d7-f5cb-455d-a911-78f93418a0ce', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (63, 105, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (64, 105, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (65, 110, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (66, 110, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (67, 111, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (68, 111, 1, 'keyword', 'France', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (69, 114, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (70, 114, 1, 'keyword', 'Demonstration', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (71, 117, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (72, 117, 1, 'keyword', 'Demonstration', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (73, 118, 1, 'hasfeaturecat', '7a8db1d7-f5cb-455d-a911-78f93418a0ce', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (74, 119, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (75, 119, 1, 'keyword', 'Demonstration', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (76, 122, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (77, 123, 1, 'parentUuid', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (78, 124, 1, 'operatesOn', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (79, 125, 1, 'hasfeaturecat', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (80, 126, 1, 'parentUuid', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (81, 127, 1, 'operatesOn', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (82, 130, 1, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 1.40000000000000007e-45, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40007, 40006, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40008, 40006, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40009, 40006, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40011, 40010, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40012, 40010, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40013, 40010, 0, '_source', '7fc45be3-9aba-4198-920c-b8737112d522', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40014, 40010, 0, 'type', 'dataset', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40015, 40010, 0, 'type', 'series', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40016, 40010, 0, 'type', 'service', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40017, 40010, 0, 'type', 'nonGeographicDataset', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40018, 40010, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40020, 40019, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40021, 40019, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40022, 40019, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40024, 40023, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40025, 40023, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40026, 40023, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40031, 40029, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40035, 40027, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40030, 40028, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40032, 40029, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40033, 40029, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40034, 40028, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40038, 40028, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40036, 40027, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40037, 40027, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40040, 40039, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40041, 40039, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40042, 40039, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40044, 40043, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40045, 40043, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40046, 40043, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40048, 40047, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40049, 40047, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40050, 40047, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40052, 40051, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40053, 40051, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40054, 40051, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40056, 40055, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40057, 40055, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40058, 40055, 0, 'parentUuid', '1c661f3e-33b5-4410-aa0d-c85f21a8ae7d', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40059, 40055, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40061, 40060, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40062, 40060, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40063, 40060, 0, 'operatesOn', '1c661f3e-33b5-4410-aa0d-c85f21a8ae7d', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40064, 40060, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40068, 40067, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40069, 40067, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40070, 40067, 0, '_uuid', '7a8db1d7-f5cb-455d-a911-78f93418a0ce', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40066, 40065, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40072, 40065, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40073, 40065, 0, 'hasfeaturecat', '1c661f3e-33b5-4410-aa0d-c85f21a8ae7d', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40074, 40065, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40071, 40067, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40076, 40075, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40077, 40075, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40078, 40075, 0, 'agg_associated', '1c661f3e-33b5-4410-aa0d-c85f21a8ae7d', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40079, 40075, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40081, 40080, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40082, 40080, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40083, 40080, 0, 'hasfeaturecat', '1c661f3e-33b5-4410-aa0d-c85f21a8ae7d', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40084, 40080, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40091, 40090, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40092, 40090, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40093, 40090, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40095, 40094, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40096, 40094, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40097, 40094, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40099, 40098, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40100, 40098, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40101, 40098, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40104, 40103, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40105, 40103, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40106, 40103, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40107, 40103, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40108, 40103, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40109, 40103, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40110, 40103, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40111, 40103, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40112, 40103, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40113, 40103, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40114, 40103, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40117, 40115, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40118, 40115, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40119, 40115, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40120, 40115, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40121, 40115, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40122, 40115, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40123, 40115, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40124, 40115, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40125, 40115, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40126, 40115, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40127, 40115, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40128, 40116, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40129, 40116, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40130, 40116, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40131, 40116, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40132, 40116, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40133, 40116, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40134, 40116, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40135, 40116, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40136, 40116, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40137, 40116, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40138, 40116, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40141, 40140, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40142, 40140, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40143, 40140, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40144, 40140, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40145, 40140, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40146, 40140, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40147, 40140, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40148, 40140, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40149, 40140, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40150, 40140, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40151, 40140, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40153, 40152, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40154, 40152, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40155, 40152, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40156, 40152, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40157, 40152, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40158, 40152, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40159, 40152, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40160, 40152, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40161, 40152, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40162, 40152, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40163, 40152, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40165, 40164, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40166, 40164, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40167, 40164, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40168, 40164, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40169, 40164, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40170, 40164, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40171, 40164, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40172, 40164, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40173, 40164, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40174, 40164, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40175, 40164, 0, '_id', '9', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40176, 40164, 0, '_isTemplate', 'y', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40177, 40164, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40178, 40164, 0, '_isTemplate', 's', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40180, 40179, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40181, 40179, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40182, 40179, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40183, 40179, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40184, 40179, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40185, 40179, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40186, 40179, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40187, 40179, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40188, 40179, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40189, 40179, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40190, 40179, 0, 'parentUuid', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40191, 40179, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40193, 40192, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40194, 40192, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40195, 40192, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40196, 40192, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40197, 40192, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40198, 40192, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40199, 40192, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40200, 40192, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40201, 40192, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40202, 40192, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40203, 40192, 0, 'agg_associated', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40204, 40192, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40206, 40205, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40207, 40205, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40208, 40205, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40209, 40205, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40210, 40205, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40211, 40205, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40212, 40205, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40213, 40205, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40214, 40205, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40215, 40205, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40216, 40205, 0, 'operatesOn', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40217, 40205, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40219, 40218, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40220, 40218, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40221, 40218, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40222, 40218, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40223, 40218, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40224, 40218, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40225, 40218, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40226, 40218, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40227, 40218, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40228, 40218, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40229, 40218, 0, 'hassource', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40230, 40218, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40232, 40231, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40233, 40231, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40234, 40231, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40235, 40231, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40236, 40231, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40237, 40231, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40238, 40231, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40239, 40231, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40240, 40231, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40241, 40231, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40242, 40231, 0, 'hasfeaturecat', '3c892ef0-0a6d-11de-ad6b-00104b7907b4', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40243, 40231, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40245, 40244, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40246, 40244, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40247, 40244, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40248, 40244, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40249, 40244, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40250, 40244, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40251, 40244, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40252, 40244, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40253, 40244, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40254, 40244, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40255, 40244, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40257, 40256, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40258, 40256, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40259, 40256, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40260, 40256, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40261, 40256, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40262, 40256, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40263, 40256, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40264, 40256, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40265, 40256, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40266, 40256, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40267, 40256, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40269, 40268, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40270, 40268, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40271, 40268, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40272, 40268, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40273, 40268, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40274, 40268, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40275, 40268, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40276, 40268, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40277, 40268, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40278, 40268, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40279, 40268, 0, '_isTemplate', 'y', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40280, 40268, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40281, 40268, 0, '_isTemplate', 's', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40283, 40282, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40284, 40282, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40285, 40282, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40286, 40282, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40287, 40282, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40288, 40282, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40289, 40282, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40290, 40282, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40291, 40282, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40292, 40282, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40293, 40282, 0, '_isTemplate', 'y', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40294, 40282, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40295, 40282, 0, '_isTemplate', 's', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40297, 40296, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40298, 40296, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40299, 40296, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40300, 40296, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40301, 40296, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40302, 40296, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40303, 40296, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40304, 40296, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40305, 40296, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40306, 40296, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40307, 40296, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40309, 40308, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40310, 40308, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40311, 40308, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40312, 40308, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40313, 40308, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40314, 40308, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40315, 40308, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40316, 40308, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40317, 40308, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40318, 40308, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40319, 40308, 0, '_isTemplate', 'y', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40321, 40320, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40322, 40320, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40323, 40320, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40324, 40320, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40325, 40320, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40326, 40320, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40327, 40320, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40328, 40320, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40329, 40320, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40330, 40320, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40331, 40320, 0, '_isTemplate', 'y', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40332, 40320, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40333, 40320, 0, '_isTemplate', 's', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40335, 40334, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40336, 40334, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40337, 40334, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40338, 40334, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40339, 40334, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40340, 40334, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40341, 40334, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40342, 40334, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40343, 40334, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40344, 40334, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40345, 40334, 0, '_id', '40086', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40346, 40334, 0, '_isTemplate', 'y', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40347, 40334, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40348, 40334, 0, '_isTemplate', 's', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40350, 40349, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40351, 40349, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40352, 40349, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40363, 40362, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40364, 40362, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40365, 40362, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40366, 40362, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40367, 40362, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40368, 40362, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40369, 40362, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40370, 40362, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40371, 40362, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40372, 40362, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40373, 40362, 0, 'agg_associated', 'c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40374, 40362, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40376, 40375, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40377, 40375, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40378, 40375, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40379, 40375, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40380, 40375, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40381, 40375, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40382, 40375, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40383, 40375, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40384, 40375, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40385, 40375, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40386, 40375, 0, 'operatesOn', 'c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40387, 40375, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40389, 40388, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40390, 40388, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40391, 40388, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40392, 40388, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40393, 40388, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40394, 40388, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40395, 40388, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40396, 40388, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40397, 40388, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40398, 40388, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40399, 40388, 0, '_uuid', '-- Identifiant de la métadonnée de couche associée --', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40400, 40388, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40402, 40401, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40403, 40401, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40404, 40401, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40405, 40401, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40406, 40401, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40407, 40401, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40408, 40401, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40409, 40401, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40410, 40401, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40411, 40401, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40412, 40401, 0, 'hassource', 'c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40413, 40401, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40415, 40414, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40416, 40414, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40417, 40414, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40418, 40414, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40419, 40414, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40420, 40414, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40421, 40414, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40422, 40414, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40423, 40414, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40424, 40414, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40425, 40414, 0, 'hasfeaturecat', 'c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40426, 40414, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40353, 40349, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40354, 40349, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40355, 40349, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40356, 40349, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40357, 40349, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40358, 40349, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40359, 40349, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40360, 40349, 0, 'parentUuid', 'c0bd4c2c-21b1-4e39-99b3-4d3c7e7183a1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40361, 40349, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40428, 40427, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40429, 40427, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40430, 40427, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40431, 40427, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40432, 40427, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40433, 40427, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40434, 40427, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40435, 40427, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40436, 40427, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40437, 40427, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40438, 40427, 0, '_isTemplate', 'y', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40439, 40427, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40440, 40427, 0, '_isTemplate', 's', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40442, 40441, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40443, 40441, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40444, 40441, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40445, 40441, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40446, 40441, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40447, 40441, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40448, 40441, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40449, 40441, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40450, 40441, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40451, 40441, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40452, 40441, 0, '_id', '40085', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40453, 40441, 0, '_isTemplate', 'y', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40454, 40441, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40455, 40441, 0, '_isTemplate', 's', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40457, 40456, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40458, 40456, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40459, 40456, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40460, 40456, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40461, 40456, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40462, 40456, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40463, 40456, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40464, 40456, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40465, 40456, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40466, 40456, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40467, 40456, 0, '_isTemplate', 'y', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40468, 40456, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40469, 40456, 0, '_isTemplate', 's', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40471, 40470, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40472, 40470, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40473, 40470, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40474, 40470, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40475, 40470, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40476, 40470, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40477, 40470, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40478, 40470, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40479, 40470, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40480, 40470, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40481, 40470, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40483, 40482, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40484, 40482, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40485, 40482, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40486, 40482, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40487, 40482, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40488, 40482, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40489, 40482, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40490, 40482, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40491, 40482, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40492, 40482, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40493, 40482, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40496, 40495, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40499, 40495, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40502, 40495, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40497, 40494, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40498, 40494, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40500, 40494, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40501, 40494, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40503, 40494, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40506, 40494, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40508, 40494, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40510, 40494, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40512, 40494, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40504, 40495, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40505, 40495, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40507, 40495, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40509, 40495, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40511, 40495, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40514, 40495, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40515, 40495, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40517, 40495, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40513, 40494, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40516, 40494, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40519, 40518, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40520, 40518, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40521, 40518, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40522, 40518, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40523, 40518, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40524, 40518, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40525, 40518, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40526, 40518, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40527, 40518, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40528, 40518, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40529, 40518, 0, '_isTemplate', 'y', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40530, 40518, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40531, 40518, 0, '_isTemplate', 's', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40533, 40532, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40534, 40532, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40535, 40532, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40536, 40532, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40537, 40532, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40538, 40532, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40539, 40532, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40540, 40532, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40541, 40532, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40542, 40532, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40543, 40532, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40545, 40544, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40546, 40544, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40547, 40544, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40548, 40544, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40549, 40544, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40550, 40544, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40551, 40544, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40552, 40544, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40553, 40544, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40554, 40544, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40555, 40544, 0, '_isTemplate', 'y', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40556, 40544, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40557, 40544, 0, '_uuid', '9ba5f3e9-c0ef-4160-81c0-b633b6067601', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40559, 40558, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40560, 40558, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40561, 40558, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40562, 40558, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40563, 40558, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40564, 40558, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40565, 40558, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40566, 40558, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40567, 40558, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40568, 40558, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40569, 40558, 0, 'parentUuid', '9ba5f3e9-c0ef-4160-81c0-b633b6067601', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40570, 40558, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40572, 40571, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40573, 40571, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40574, 40571, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40575, 40571, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40576, 40571, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40577, 40571, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40578, 40571, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40604, 40571, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40605, 40571, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40581, 40580, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40582, 40580, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40584, 40580, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40606, 40571, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40607, 40571, 0, 'operatesOn', '9ba5f3e9-c0ef-4160-81c0-b633b6067601', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40608, 40571, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40583, 40579, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40592, 40579, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40594, 40579, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40585, 40580, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40586, 40580, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40587, 40580, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40596, 40579, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40597, 40579, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40598, 40579, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40588, 40580, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40589, 40580, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40590, 40580, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40599, 40579, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40600, 40579, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40601, 40579, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40591, 40580, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40593, 40580, 0, 'agg_associated', '9ba5f3e9-c0ef-4160-81c0-b633b6067601', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40595, 40580, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40602, 40579, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40603, 40579, 0, 'hasfeaturecat', '9ba5f3e9-c0ef-4160-81c0-b633b6067601', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40609, 40579, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40611, 40610, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40612, 40610, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40613, 40610, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40614, 40610, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40615, 40610, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40616, 40610, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40617, 40610, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40618, 40610, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40619, 40610, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40620, 40610, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40621, 40610, 0, 'hasfeaturecat', '9ba5f3e9-c0ef-4160-81c0-b633b6067601', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40622, 40610, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40624, 40623, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40625, 40623, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40626, 40623, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40641, 40640, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40642, 40640, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40643, 40640, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40644, 40640, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40645, 40640, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40646, 40640, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40647, 40640, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40648, 40640, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40649, 40640, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40650, 40640, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40651, 40640, 0, 'operatesOn', '9ba5f3e9-c0ef-4160-81c0-b633b6067601', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40652, 40640, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40627, 40623, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40628, 40623, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40629, 40623, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40630, 40623, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40631, 40623, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40632, 40623, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40633, 40623, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40634, 40623, 0, 'parentUuid', '9ba5f3e9-c0ef-4160-81c0-b633b6067601', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40635, 40623, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40675, 40674, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40676, 40674, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40677, 40674, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40639, 40636, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40654, 40636, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40655, 40636, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40656, 40636, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40658, 40636, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40660, 40636, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40662, 40636, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40669, 40636, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40670, 40636, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40678, 40674, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40679, 40674, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40680, 40674, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40671, 40636, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40672, 40636, 0, 'hasfeaturecat', '9ba5f3e9-c0ef-4160-81c0-b633b6067601', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40673, 40636, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40681, 40674, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40682, 40674, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40683, 40674, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40684, 40674, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40685, 40674, 0, 'hasfeaturecat', '9ba5f3e9-c0ef-4160-81c0-b633b6067601', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40686, 40674, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40691, 40688, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40692, 40688, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40694, 40688, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40696, 40688, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40698, 40688, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40700, 40688, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40702, 40688, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40704, 40688, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40706, 40688, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40708, 40688, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40710, 40688, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40690, 40689, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40693, 40689, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40695, 40689, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40697, 40689, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40699, 40689, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40701, 40689, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40703, 40689, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40705, 40689, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40707, 40689, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40709, 40689, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40711, 40689, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40638, 40637, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40653, 40637, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40657, 40637, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40659, 40637, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40661, 40637, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40663, 40637, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40664, 40637, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40665, 40637, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40666, 40637, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40667, 40637, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40668, 40637, 0, 'agg_associated', '9ba5f3e9-c0ef-4160-81c0-b633b6067601', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40687, 40637, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40713, 40712, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40714, 40712, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40715, 40712, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40716, 40712, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40717, 40712, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40718, 40712, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40719, 40712, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40720, 40712, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40721, 40712, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40722, 40712, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40723, 40712, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40725, 40724, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40726, 40724, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40727, 40724, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40728, 40724, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40729, 40724, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40730, 40724, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40731, 40724, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40732, 40724, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40733, 40724, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40734, 40724, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40735, 40724, 0, '_isTemplate', 'y', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40736, 40724, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40737, 40724, 0, '_isTemplate', 's', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40739, 40738, 0, '_op0', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40740, 40738, 0, '_op2', '-1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40741, 40738, 0, '_op0', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40742, 40738, 0, '_op2', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40743, 40738, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40744, 40738, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40745, 40738, 0, '_op0', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40746, 40738, 0, '_op2', '217', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40747, 40738, 0, '_owner', '40102', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40748, 40738, 0, '_dummy', '0', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40749, 40738, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40756, 40751, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40753, 40752, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40759, 40750, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40757, 40751, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40760, 40750, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40754, 40752, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40758, 40751, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40755, 40752, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40761, 40750, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40769, 40762, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40775, 40774, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40765, 40764, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40766, 40763, 0, '_op0', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40770, 40762, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40773, 40762, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40776, 40774, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40777, 40774, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40771, 40764, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40767, 40763, 0, '_op2', '1', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40768, 40763, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");
        $this->addSql("INSERT INTO public.params (id, requestid, querytype, termfield, termtext, similarity, lowertext, uppertext, inclusive) VALUES (40772, 40764, 0, '_isTemplate', 'n', 0, NULL, NULL, 'n')");

        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (2, 38.4719800000000021, 29.4061100000000017, 60.504170000000002, 74.9157399999999996)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (338, 38.2000000000000028, -34.6000000000000014, -17.3000000000000007, 51.1000000000000014)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (3, 42.6603399999999979, 39.6450000000000031, 19.2885400000000011, 21.053329999999999)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (4, 37.0898600000000016, 18.9763899999999985, -8.66722000000000037, 11.9864800000000002)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1220, 65, -65, -180, 180)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (5, -14.2543100000000003, -14.3755600000000001, -170.823229999999995, -170.561869999999999)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (6, 42.6559700000000035, 42.4363900000000029, 1.42138999999999993, 1.78171999999999997)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (7, -4.38898999999999972, -18.0163900000000012, 11.7312499999999993, 24.0844400000000007)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (258, 18.2729800000000004, 18.164439999999999, -63.1677800000000005, -62.9727099999999993)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (30, -60.5033299999999983, -90, -180, 180)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (8, 17.7242999999999995, 16.9897199999999984, -61.8911099999999976, -61.6669499999999999)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (9, -21.7805199999999992, -55.0516700000000014, -73.5823000000000036, -53.6500100000000018)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1, 41.2970499999999987, 38.841149999999999, 43.4541600000000017, 46.6205399999999983)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1008, 90, 48, -180, 180)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (22, 12.6277799999999996, 12.4111100000000008, -70.0596599999999938, -69.8748599999999982)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (337, 83.5, 10.8000000000000007, 31, 179.900000000000006)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1016, -50, -78.1136000000000053, -68.5052000000000021, -68.3546000000000049)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1012, 36, -6.13210000000000033, -16.434429999999999, -16)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1010, 90, 36, 55, -27.3408100000000012)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1009, 78.1700000000000017, 34.9500000000000028, 58.2012900000000002, -84.3583299999999952)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1015, -6, -50, -20, 30)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1014, 5, -60, -69.6307999999999936, -20)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1011, 36, 5, -97.8097000000000065, -40)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (10, -10.1356999999999999, -54.7538899999999984, 112.907210000000006, 158.960370000000012)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (11, 49.0187499999999972, 46.4074900000000028, 9.53356999999999921, 17.1663899999999998)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (52, 41.8970600000000033, 38.3891500000000008, 44.7788600000000017, 50.3749899999999968)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (12, 26.9291699999999992, 20.9152799999999992, -78.9788999999999959, -72.7388899999999978)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (13, 26.2888899999999985, 25.5719400000000014, 50.4533300000000011, 50.7963900000000024)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (16, 26.6261399999999995, 20.7448200000000007, 88.0438699999999983, 92.6693400000000054)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (14, 13.3370800000000003, 13.0505499999999994, -59.6594499999999996, -59.4270899999999997)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (57, 56.1674900000000008, 51.2518499999999975, 23.1654000000000018, 32.7400599999999997)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (255, 51.5012499999999989, 49.5088799999999978, 2.54166999999999987, 6.39820000000000011)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (23, 18.4898999999999987, 15.8898499999999991, -89.216399999999993, -87.7795899999999989)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (53, 12.3966600000000007, 6.21872000000000025, 0.776669999999999972, 3.85499999999999998)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (17, 32.3795100000000033, 32.2605500000000021, -64.8230599999999981, -64.6768100000000032)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (18, 28.3249999999999993, 26.7036100000000012, 88.7519400000000047, 92.1142200000000031)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (19, -9.6791999999999998, -22.9011099999999992, -69.6561899999999952, -57.5211200000000034)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (80, 45.2659499999999966, 42.5658299999999983, 15.7405899999999992, 19.6197899999999983)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (20, -17.7820900000000002, -26.8755600000000001, 19.9961100000000016, 29.373619999999999)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (31, -54.3836099999999973, -54.4627800000000022, 3.34236000000000022, 3.48417000000000021)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (21, 5.27271000000000001, -33.7411200000000022, -74.0045899999999932, -34.7929200000000023)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (24, -7.23346999999999962, -7.43625000000000025, 72.3579000000000008, 72.4942900000000066)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (239, 18.5048600000000008, 18.383890000000001, -64.6984800000000035, -64.3245200000000068)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (26, 5.05304999999999982, 4.01818999999999971, 114.095079999999996, 115.360259999999997)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (27, 44.2247199999999978, 41.2430499999999967, 22.3652799999999985, 28.6051399999999987)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (233, 15.0827799999999996, 9.3956900000000001, -5.52083000000000013, 2.39792000000000005)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (29, -2.30155999999999983, -4.4480599999999999, 28.9849999999999994, 30.8531900000000014)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (115, 14.7086199999999998, 10.4227399999999992, 102.346500000000006, 107.636380000000003)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (32, 13.0850000000000009, 1.65416999999999992, 8.50235999999999947, 16.2070100000000004)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (33, 83.1138799999999947, 41.6755500000000012, -141.002990000000011, -52.6173599999999979)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (35, 17.1923600000000008, 14.8111099999999993, -25.3605599999999995, -22.6661099999999998)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (317, 29.3500000000000014, 8.10999999999999943, -88.8100000000000023, -56.8299999999999983)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (36, 19.3541600000000003, 19.2650000000000006, -81.4008400000000023, -81.0930599999999941)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (312, 25.1600000000000001, -19.3599999999999994, -9.11999999999999922, 46.7100000000000009)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (37, 11.0008300000000006, 2.22126000000000001, 14.4188899999999993, 27.4597200000000008)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (316, 36.9099999999999966, 3.20999999999999996, -120.170000000000002, -74.8299999999999983)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (39, 23.4505499999999998, 7.45854000000000017, 13.4619400000000002, 24.0027499999999989)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (40, -17.5052799999999991, -55.902230000000003, -109.446110000000004, -66.4206300000000027)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (41, 53.5537399999999977, 18.1688800000000015, 73.6200500000000062, 134.768460000000005)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (214, 25.2836099999999995, 21.9277699999999989, 118.278599999999997, 122.000399999999999)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (42, -10.3840800000000009, -10.5109700000000004, 105.629000000000005, 105.751900000000006)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (43, -12.1304200000000009, -12.1994399999999992, 96.8174900000000065, 96.8648500000000041)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (44, 12.5902799999999999, -4.23686999999999969, -81.7201500000000038, -66.8704500000000053)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (45, -11.3669499999999992, -12.3830600000000004, 43.2140199999999979, 44.5304199999999994)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (250, 2.10000000000000009, -9.22000000000000064, 10.0299999999999994, 34.7700000000000031)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (46, 3.71111000000000013, -5.01499999999999968, 11.1406600000000005, 18.6436099999999989)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (47, -10.8813200000000005, -21.9408299999999983, -165.848340000000007, -157.703769999999992)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (48, 11.2128499999999995, 8.02566999999999986, -85.9113899999999973, -82.5614000000000061)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (98, 46.5358299999999971, 42.3999900000000025, 13.5047899999999998, 19.4250000000000007)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (49, 23.1940300000000015, 19.8219400000000014, -84.9529299999999949, -74.1312599999999975)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (50, 35.6886099999999971, 34.640270000000001, 32.2698600000000013, 34.586039999999997)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (167, 51.0524899999999988, 48.5813800000000029, 12.0937000000000001, 18.8522199999999991)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (54, 57.7459699999999998, 54.5619399999999999, 8.09291999999999945, 15.1491699999999998)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (72, 12.7083300000000001, 10.9422200000000007, 41.7598600000000033, 43.4204099999999968)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (55, 15.6319400000000002, 15.1980599999999999, -61.4913900000000027, -61.2507000000000019)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (56, 19.9308300000000003, 17.6041699999999999, -72.0030699999999939, -68.3229299999999995)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (324, 55.0200000000000031, -12.7300000000000004, 40.25, 158.710000000000008)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (176, -8.01999999999999957, -9.38000000000000078, 124.840000000000003, 127.349999999999994)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (313, 19.2199999999999989, -28.1099999999999994, 13.5500000000000007, 70.5)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (334, 55.4600000000000009, 39.5499999999999972, 9.78999999999999915, 31.9100000000000001)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (58, 1.43778000000000006, -5.00030999999999981, -91.6638999999999982, -75.2168400000000048)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (59, 31.6469400000000007, 21.9941600000000008, 24.7068000000000012, 36.8958299999999966)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (60, 14.4319799999999994, 13.15639, -90.1080599999999947, -87.6946700000000021)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (61, 3.76332999999999984, 0.930159999999999987, 8.42417000000000016, 11.3538899999999998)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (178, 17.9948799999999984, 12.3638899999999996, 36.4432800000000015, 43.121380000000002)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (63, 59.6647200000000026, 57.5226299999999995, 21.8373600000000003, 28.1940899999999992)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (238, 14.8836099999999991, 3.40667000000000009, 32.9917999999999978, 47.9882399999999976)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (335, 81.4000000000000057, 35.2999999999999972, -11.5, 43.2000000000000028)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (427, 82.6400000000000006, 29.3999999999999986, -29.629999999999999, 45.0499999999999972)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (64, 62.3575000000000017, 61.3883300000000034, -7.4334699999999998, -6.38971999999999962)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (65, -51.2494500000000031, -52.3430600000000013, -61.148060000000001, -57.7331999999999965)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (322, 56.1499999999999986, -12.4700000000000006, 52.9500000000000028, 148.509999999999991)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (66, -16.1534699999999987, -19.1627800000000015, 175, 180)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (67, 70.0886100000000027, 59.8068000000000026, 19.5113899999999987, 31.5819599999999987)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (69, 5.75541999999999998, 2.11346999999999996, -54.6037800000000004, -51.648060000000001)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (70, -8.7782, -17.8708300000000015, -151.497770000000003, -138.809750000000008)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (71, -46.3276400000000024, -49.7250099999999975, 51.6508299999999991, 70.5674900000000065)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (68, 51.0911100000000005, 41.3649300000000011, -5.79028000000000009, 9.56221999999999994)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (74, 2.31789999999999985, -3.92527999999999988, 8.70082999999999984, 14.5195799999999995)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (75, 13.82639, 13.0599799999999995, -16.821670000000001, -13.79861)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (76, 31.5960999999999999, 31.2165399999999984, 34.2166599999999974, 34.5588899999999981)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (73, 43.5847199999999972, 41.0480400000000003, 40.0029699999999977, 46.7108199999999982)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (79, 55.0565300000000022, 47.2747200000000021, 5.86500000000000021, 15.0338200000000004)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (81, 11.1556899999999999, 4.72707999999999995, -3.24888999999999983, 1.20277999999999996)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (82, 36.1633100000000027, 36.1120700000000028, -5.35616999999999965, -5.33450999999999986)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (84, 41.7477700000000027, 34.9305499999999967, 19.6400000000000006, 28.2380500000000012)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (85, 83.6235999999999962, 59.7902800000000028, -73.053600000000003, -12.1576400000000007)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (86, 12.2371499999999997, 11.9969400000000004, -61.7851799999999969, -61.5963899999999995)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (87, 16.5129200000000012, 15.8699999999999992, -61.7961099999999988, -61.1870800000000017)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (88, 13.6522900000000007, 13.2349999999999994, 144.634160000000008, 144.953309999999988)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (89, 17.8211100000000009, 13.7458299999999998, -92.2467800000000011, -88.2147400000000061)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (90, 12.6775000000000002, 7.19392999999999994, -15.0808300000000006, -7.65336999999999978)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (175, 12.6847200000000004, 10.9251000000000005, -16.7177700000000016, -13.6438900000000007)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (91, 8.5352800000000002, 1.18687999999999994, -61.3897300000000001, -56.4706299999999999)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (93, 20.0914600000000014, 18.0227800000000009, -74.4677899999999937, -71.6291800000000052)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (92, -52.9651500000000013, -53.1994499999999988, 73.2347100000000069, 73.7738800000000055)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (95, 16.4358299999999993, 12.9851700000000001, -89.3504899999999935, -83.13185)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (97, 48.5761800000000008, 45.7483300000000028, 16.1118099999999984, 22.8948)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (99, 66.5361000000000047, 63.3900000000000006, -24.5383999999999993, -13.4994499999999995)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (100, 35.5056200000000004, 6.74582999999999977, 68.1442299999999932, 97.3805399999999963)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1019, -45, -70.0180699999999945, 148.004199999999997, 148.389800000000008)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1018, 22.757200000000001, -55, 77, 150)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1017, 30.5061, -45, 30, 80)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (101, 5.91347000000000023, -10.9296500000000005, 95.2109499999999969, 141.007020000000011)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (307, 23.3399999999999999, -5.78000000000000025, 18.4499999999999993, 54.9099999999999966)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (102, 39.7791599999999974, 25.0759700000000016, 44.034950000000002, 63.3302699999999987)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (103, 37.3836799999999982, 29.0616599999999998, 38.7946999999999989, 48.560690000000001)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (104, 55.3799899999999994, 51.4455499999999972, -10.4747199999999996, -6.01306000000000029)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (264, 54.4163899999999998, 54.0555499999999967, -4.78714999999999957, -4.30867999999999984)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (105, 33.2702699999999965, 29.4867099999999986, 34.2675800000000024, 35.6811099999999968)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (106, 47.0945800000000006, 36.649160000000002, 6.62396999999999991, 18.5144400000000005)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (107, 10.7352600000000002, 4.34471999999999969, -8.6063799999999997, -2.48777999999999988)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (109, 18.5225000000000009, 17.6972200000000015, -78.3739000000000061, -76.2211199999999991)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (110, 45.4863799999999969, 24.2513900000000007, 123.67886, 145.81241)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (112, 33.3775899999999979, 29.1888900000000007, 34.9604199999999992, 39.3011100000000013)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (108, 55.4426300000000012, 40.5944399999999987, 46.4991600000000034, 87.3482099999999946)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (114, 4.62249999999999961, -4.6696200000000001, 33.9072200000000024, 41.9051699999999983)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (83, 2.0330499999999998, 1.70500000000000007, -157.581700000000012, -157.172550000000001)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (116, 43.0061000000000035, 37.6713799999999992, 124.323949999999996, 130.697419999999994)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (117, 38.625239999999998, 33.1922100000000029, 126.099010000000007, 129.586870000000005)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (118, 30.0841600000000007, 28.5388799999999989, 46.5469399999999993, 48.4165899999999993)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (113, 43.2169000000000025, 39.1954700000000003, 69.2494999999999976, 80.2815899999999942)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (120, 22.4999299999999991, 13.92666, 100.091369999999998, 107.695250000000001)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (315, 33.1700000000000017, -25.4400000000000013, -120.170000000000002, -58.7800000000000011)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (348, 32.7000000000000028, -55.3999999999999986, -117, -33.7999999999999972)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (119, 58.0832600000000028, 55.6748400000000032, 20.9686100000000017, 28.2359699999999982)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (121, 34.6475000000000009, 33.0620800000000017, 35.100830000000002, 36.623739999999998)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (122, -28.5706999999999987, -30.6505299999999998, 27.0139700000000005, 29.4555499999999988)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (123, 8.51277999999999935, 4.34360999999999997, -11.4923300000000008, -7.36840000000000028)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (124, 33.1711400000000012, 19.4990699999999997, 9.31138999999999939, 25.1516699999999993)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (125, 47.2745400000000018, 47.057459999999999, 9.47464000000000084, 9.63388999999999918)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (126, 56.4498499999999979, 53.8903400000000019, 20.9428300000000007, 26.8130500000000005)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (256, 50.1818099999999987, 49.4484700000000004, 5.7344400000000002, 6.52402999999999977)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (154, 42.3589500000000001, 40.8558900000000023, 20.4588199999999993, 23.0309699999999999)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (129, -11.9455600000000004, -25.5883399999999988, 43.2368200000000016, 50.5013900000000007)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (130, -9.37667000000000073, -17.1352800000000016, 32.6818700000000035, 35.920969999999997)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (131, 7.35292000000000012, 0.852779999999999982, 99.6419400000000053, 119.275819999999996)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (132, 7.02777999999999992, -0.641669999999999963, 72.8633899999999954, 73.6372800000000041)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (133, 25.0002800000000001, 10.1421500000000009, -12.2448300000000003, 4.25138999999999978)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (134, 35.9919399999999996, 35.7999999999999972, 14.3291000000000004, 14.5700000000000003)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (127, 14.5940300000000001, 5.6002799999999997, 162.324970000000008, 169.971620000000001)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (135, 14.8801400000000008, 14.4027799999999999, -61.2315299999999993, -60.8169499999999985)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (136, 27.2904599999999995, 14.7256400000000003, -17.0755599999999994, -4.80611000000000033)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (137, -19.6733399999999996, -20.5205599999999997, 57.3063100000000034, 63.4957599999999971)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (270, -12.6624999999999996, -12.9924999999999997, 45.0391600000000025, 45.2297200000000004)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1013, 47.2719000000000023, 30.2735999999999983, -5.61000000000000032, 41.7586000000000013)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (138, 32.7184600000000003, 14.5505499999999994, -118.404169999999993, -86.7386199999999974)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (145, 6.97764000000000006, 5.26166999999999963, 158.120100000000008, 163.04289)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (139, 28.2215200000000017, 28.1841599999999985, -177.395839999999993, -177.360549999999989)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (146, 48.4683199999999985, 45.4486500000000007, 26.6350000000000016, 30.1287100000000017)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (140, 43.7683000000000035, 43.7275500000000008, 7.39090000000000025, 7.43928999999999974)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (141, 52.1427699999999987, 41.586660000000002, 87.761099999999999, 119.931510000000003)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (142, 16.8123600000000017, 16.6713899999999988, -62.2369500000000002, -62.1388900000000035)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (143, 35.9191700000000012, 27.6642399999999995, -13.1749600000000004, -1.0118100000000001)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (144, -10.4711099999999995, -26.8602799999999995, 30.2130200000000002, 40.846110000000003)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (28, 28.546520000000001, 9.83957999999999977, 92.2049899999999951, 101.169430000000006)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (163, 15.2681900000000006, 14.9080499999999994, 145.572679999999991, 145.818090000000012)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (147, -16.9541700000000013, -28.9618800000000007, 11.7163900000000005, 25.2644300000000008)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (148, -0.493329999999999991, -0.552220000000000044, 166.904419999999988, 166.95705000000001)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (319, 51.1599999999999966, -5.09999999999999964, 6.71999999999999975, 77.5)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (325, 62.2000000000000028, -6.46999999999999975, -17.129999999999999, 78.6899999999999977)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (320, 34.259999999999998, 2.27000000000000002, 4.12999999999999989, 44.1300000000000026)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (321, 48.4200000000000017, 5.66999999999999993, 23.5500000000000007, 77.0300000000000011)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (149, 30.4247200000000007, 26.3683599999999991, 80.0521999999999991, 88.1945599999999956)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (151, 12.3838899999999992, 12.0205599999999997, -69.1636199999999945, -68.1929200000000009)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (150, 53.4658299999999969, 50.7538800000000023, 3.37087000000000003, 7.21096999999999966)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (156, -32.4147200000000026, -52.5780600000000007, 170, 180)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (153, -20.0879200000000004, -22.6738900000000001, 163.982740000000007, 168.130509999999987)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (157, 15.0222200000000008, 10.7096900000000002, -87.6898300000000006, -83.13185)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (158, 23.5223100000000009, 11.6932700000000001, 0.166670000000000013, 15.9966699999999999)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (159, 13.8915000000000006, 4.27285000000000004, 2.69249999999999989, 14.6496499999999994)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (160, -18.9633299999999991, -19.1455599999999997, -169.952239999999989, -169.781560000000013)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (161, -29.0005600000000001, -29.0811099999999989, 167.910950000000014, 167.998870000000011)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (421, 46.2000000000000028, 10.4000000000000004, -15.3000000000000007, 39.1000000000000014)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (428, 85, 7.20000000000000018, -166.300000000000011, -14.4000000000000004)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (310, 39.2800000000000011, 17.1999999999999993, -18.4499999999999993, 13.3000000000000007)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (162, 71.1547099999999944, 57.9879200000000026, 4.78957999999999995, 31.0735400000000013)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (221, 26.3687100000000001, 16.6427799999999984, 51.999290000000002, 59.8470799999999983)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1026, -60, -78.5666999999999973, 150, -105.224199999999996)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1023, 40.5, -25, -175, -77.8889599999999973)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1021, 66.1336000000000013, 40, -175, -122.226399999999998)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1020, 66.5, 15, 105.623599999999996, -175)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1025, 7.21119000000000021, -60, -120, -65.7199999999999989)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1024, -25, -60, 149.891359999999992, -120)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (1022, 20, -28.1499999999999986, 99.158600000000007, -175)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (165, 37.0607899999999972, 23.6880500000000005, 60.8663000000000025, 77.8239300000000043)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (166, 9.62013999999999925, 7.20610999999999979, -83.0302899999999937, -77.1983299999999986)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (168, -1.35528000000000004, -11.6425000000000001, 140.858859999999993, 155.966839999999991)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (169, -19.2968100000000007, -27.5847200000000008, -62.6437700000000035, -54.2438999999999965)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (170, -0.03687, -18.3485499999999995, -81.3551499999999947, -68.6739000000000033)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (171, 19.3911100000000012, 5.04917000000000016, 116.950000000000003, 126.598039999999997)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (172, -24.3258399999999995, -25.0822299999999991, -130.105060000000009, -128.286120000000011)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (173, 54.836039999999997, 49.00291, 14.1476400000000009, 24.1434700000000007)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (174, 44, 35, -10, -6.19045000000000023)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (177, 18.5194399999999995, 17.9222199999999994, -67.2664000000000044, -65.3011199999999974)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (179, 26.1524999999999999, 24.5560399999999994, 50.7519399999999976, 51.6158300000000025)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (182, -20.8565299999999993, -21.3738899999999994, 55.2205500000000029, 55.8530500000000032)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (183, 48.2638900000000035, 43.6233099999999965, 20.2610199999999985, 29.6722199999999994)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (185, 81.8519299999999959, 41.1965799999999973, -36, 180)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (184, -1.05445000000000011, -2.82548999999999984, 28.8544400000000003, 30.8932600000000015)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (244, -13.4605599999999992, -14.0574999999999992, -172.780030000000011, -171.429200000000009)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (192, 43.9868700000000032, 43.8986799999999988, 12.4069400000000005, 12.5111100000000004)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (193, 1.70124999999999993, 0.0183299999999999991, 6.46513999999999989, 7.46347000000000005)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (194, 32.1549400000000034, 15.6169399999999996, 34.5721500000000006, 55.6661100000000033)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (195, 16.6906199999999991, 12.3017500000000002, -17.5327799999999989, -11.3699300000000001)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (196, -4.55166999999999966, -9.46306000000000047, 46.205689999999997, 55.5405500000000032)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (197, 9.9975000000000005, 6.92361000000000004, -13.2956099999999999, -10.26431)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (200, 1.4452799999999999, 1.25903000000000009, 103.640950000000004, 103.997950000000003)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (199, 49.600830000000002, 47.7374999999999972, 16.8447199999999988, 22.5580500000000015)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (198, 46.8762499999999989, 45.4258200000000016, 13.3834700000000009, 16.6078699999999984)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (25, -6.60552000000000028, -11.8458299999999994, 155.671300000000002, 166.931839999999994)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (201, 11.9791699999999999, -1.67487000000000008, 40.9886100000000013, 51.4113200000000035)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (202, -22.1363899999999987, -46.9697299999999984, 14.4093099999999996, 37.8922200000000018)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (318, 13, -55.3999999999999986, -82.0999999999999943, -33.7999999999999972)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (323, 38.7800000000000011, -2.20000000000000018, 57.4399999999999977, 100.670000000000002)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (390, 30.3099999999999987, -33.1899999999999977, -27.8900000000000006, 65.269999999999996)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (314, -14.9000000000000004, -32.5600000000000023, 10.8900000000000006, 33.009999999999998)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (271, -53.9897199999999984, -58.4986099999999993, -38.0237499999999997, -26.2413899999999991)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (203, 43.7642999999999986, 27.6374999999999993, -18.1698699999999995, 4.31693999999999978)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (38, 9.82818999999999932, 5.91805999999999965, 79.6960899999999981, 81.8916600000000017)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (187, -15.9037500000000005, -16.0219500000000004, -5.7927799999999996, -5.64527999999999963)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (188, 17.4101399999999984, 17.2088900000000002, -62.8627800000000008, -62.6225099999999983)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (189, 14.1092999999999993, 13.7094400000000007, -61.0795900000000032, -60.8780599999999978)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (190, 47.1358299999999986, 46.7798500000000033, -56.3977799999999974, -56.2326400000000035)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (191, 13.3831900000000008, 13.1302800000000008, -61.2801400000000029, -61.1202899999999971)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (206, 22.2322200000000016, 3.49339000000000022, 21.8291000000000004, 38.6075000000000017)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (207, 6.00180999999999987, 1.83624999999999994, -58.071399999999997, -53.9861199999999997)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (260, 80.7641600000000039, 74.3430500000000052, 10.4879099999999994, 33.6375000000000028)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (209, -25.7283399999999993, -27.3163899999999984, 30.79833, 32.1334000000000017)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (210, 69.060299999999998, 55.3391700000000029, 11.1133299999999995, 24.1670100000000012)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (211, 47.8066600000000008, 45.8294399999999982, 5.96701000000000015, 10.4882100000000005)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (212, 37.29054, 32.3136099999999971, 35.6144600000000011, 42.3783299999999983)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (208, 41.0492599999999968, 36.6718400000000031, 67.3646999999999991, 75.1874899999999968)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (215, -0.997219999999999995, -11.7404200000000003, 29.3408300000000004, 40.4368100000000013)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (216, 20.45458, 5.63346999999999998, 97.3472799999999978, 105.639290000000003)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (217, 11.1385400000000008, 6.10055000000000014, -0.149760000000000004, 1.79780000000000006)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (218, -9.17062999999999917, -9.21889000000000003, -171.862719999999996, -171.843770000000006)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (219, -18.5680599999999991, -21.2680599999999984, -175.360000000000014, -173.906830000000014)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (220, 11.3455499999999994, 10.0403500000000001, -61.921599999999998, -60.5208399999999997)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (222, 37.3404099999999985, 30.2343900000000012, 7.49221999999999966, 11.5816700000000008)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (223, 42.1099900000000034, 35.8184400000000025, 25.6658299999999997, 44.8205499999999972)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (213, 42.7961699999999965, 35.1459899999999976, 52.4400699999999986, 66.6708799999999968)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (224, 21.9577799999999996, 21.7397199999999984, -72.0314599999999956, -71.6336199999999934)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (227, -6.08943999999999974, -8.56128999999999962, 176.295260000000013, 179.232290000000006)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (229, 60.8433300000000017, 49.9552800000000019, -8.17167000000000066, 1.74944000000000011)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (231, 71.3514399999999966, 18.9254800000000003, -178.216550000000012, -68)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (232, 28.2215200000000017, -0.398060000000000025, 166.608980000000003, -177.395839999999993)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (240, 17.7925000000000004, 17.6766700000000014, -64.8961199999999963, -64.562579999999997)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (226, 4.2227800000000002, -1.47611000000000003, 29.5743000000000009, 35.0097200000000015)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (230, 52.3785999999999987, 44.3791500000000028, 22.1514400000000009, 40.1787500000000009)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (225, 26.0838900000000002, 22.6333300000000008, 51.5833299999999966, 56.3816599999999966)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (234, -30.0966699999999996, -34.9438200000000023, -58.4386099999999971, -53.0983000000000018)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (469, 88.230000000000004, 32.1799999999999997, 16.1000000000000014, 183.389999999999986)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (235, 45.5705999999999989, 37.1849899999999991, 55.9974899999999991, 73.1675500000000056)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (155, -13.7072199999999995, -20.2541699999999985, 166.521639999999991, 169.893859999999989)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (236, 12.1974999999999998, 0.649170000000000025, -73.3780699999999939, -59.8030600000000021)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (237, 23.3241599999999991, 8.55924000000000085, 102.140749999999997, 109.464839999999995)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (242, 19.324580000000001, 19.279440000000001, 166.608980000000003, 166.662200000000013)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (243, -13.2148599999999998, -14.3238900000000005, -178.190280000000001, -176.121929999999992)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (245, 32.5463900000000024, 31.3506900000000002, 34.8881900000000016, 35.5706100000000021)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (311, 29.0399999999999991, -17.0599999999999987, -33.4500000000000028, 17.5599999999999987)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (303, 82.9899999999999949, 26.9200000000000017, -37.3200000000000003, 39.240000000000002)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (205, 27.6669599999999996, 20.7640999999999991, -17.1015300000000003, -8.66638999999999982)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (9999, 90, -90, -180, 180)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (249, 18.9993400000000001, 12.1447199999999995, 42.5559700000000021, 54.4734699999999989)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (251, -8.19167000000000023, -18.0749199999999988, 21.9963900000000017, 33.7022800000000018)");
        $this->addSql("INSERT INTO public.regions (id, north, south, west, east) VALUES (181, -15.6165299999999991, -22.4147600000000011, 25.237919999999999, 33.0715900000000005)");

        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1, 'eng', 'Armenia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (2, 'eng', 'Afghanistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (3, 'eng', 'Albania')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (4, 'eng', 'Algeria')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (5, 'eng', 'American Samoa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (6, 'eng', 'Andorra')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (7, 'eng', 'Angola')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (8, 'eng', 'Antigua and Barbuda')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (9, 'eng', 'Argentina')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (10, 'eng', 'Australia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (11, 'eng', 'Austria')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (12, 'eng', 'Bahamas')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (13, 'eng', 'Bahrain')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (14, 'eng', 'Barbados')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (16, 'eng', 'Bangladesh')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (17, 'eng', 'Bermuda')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (18, 'eng', 'Bhutan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (19, 'eng', 'Bolivia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (20, 'eng', 'Botswana')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (21, 'eng', 'Brazil')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (22, 'eng', 'Aruba')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (23, 'eng', 'Belize')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (24, 'eng', 'British Indian Ocean Ter')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (25, 'eng', 'Solomon Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (26, 'eng', 'Brunei Darussalam')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (27, 'eng', 'Bulgaria')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (28, 'eng', 'Myanmar')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (29, 'eng', 'Burundi')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (30, 'eng', 'Antarctica')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (31, 'eng', 'Bouvet Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (32, 'eng', 'Cameroon')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (33, 'eng', 'Canada')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (35, 'eng', 'Cape Verde')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (36, 'eng', 'Cayman Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (37, 'eng', 'Central African Republic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (38, 'eng', 'Sri Lanka')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (39, 'eng', 'Chad')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (40, 'eng', 'Chile')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (41, 'eng', 'China, Mainland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (45, 'eng', 'Comoros')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1, 'fre', 'Arménie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (2, 'fre', 'Afghanistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (3, 'fre', 'Albanie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (4, 'fre', 'Algérie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (5, 'fre', 'Samoa américaines')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (6, 'fre', 'Andorre')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (7, 'fre', 'Angola')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (8, 'fre', 'Antigua-et-Barbuda')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (9, 'fre', 'Argentine')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (10, 'fre', 'Australie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (11, 'fre', 'Autriche')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (12, 'fre', 'Bahamas')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (13, 'fre', 'Bahreïn')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (14, 'fre', 'Barbade')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (16, 'fre', 'Bangladesch')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (17, 'fre', 'Bermudes')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (18, 'fre', 'Bhoutan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (19, 'fre', 'Bolivie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (20, 'fre', 'Botswana')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (21, 'fre', 'Brésil')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (22, 'fre', 'Aruba')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (23, 'fre', 'Belize')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (24, 'fre', 'British Indian Ocean Ter')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (25, 'fre', 'Salomon, Îles')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (26, 'fre', 'Brunei Darussalam')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (27, 'fre', 'Bulgarie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (28, 'fre', 'Myanmar')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (29, 'fre', 'Burundi')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (30, 'fre', 'Antartique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (31, 'fre', 'Bouvet, Île')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (32, 'fre', 'Cameroun')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (33, 'fre', 'Canada')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (35, 'fre', 'Cap-Vert')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (36, 'fre', 'Cayman Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (37, 'fre', 'Centrafricaine, République')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (38, 'fre', 'Sri Lanka')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (39, 'fre', 'Tchad')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (40, 'fre', 'Chili')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (46, 'eng', 'Congo, Republic of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (47, 'eng', 'Cook Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (48, 'eng', 'Costa Rica')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (49, 'eng', 'Cuba')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (50, 'eng', 'Cyprus')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (52, 'eng', 'Azerbaijan, Republic of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (53, 'eng', 'Benin')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (54, 'eng', 'Denmark')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (55, 'eng', 'Dominica')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (56, 'eng', 'Dominican Republic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (57, 'eng', 'Belarus')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (58, 'eng', 'Ecuador')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (59, 'eng', 'Egypt')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (60, 'eng', 'El Salvador')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (61, 'eng', 'Equatorial Guinea')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (63, 'eng', 'Estonia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (64, 'eng', 'Faeroe Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (65, 'eng', 'Falkland Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (66, 'eng', 'Fiji Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (67, 'eng', 'Finland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (68, 'eng', 'France')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (69, 'eng', 'French Guiana')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (70, 'eng', 'French Polynesia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (71, 'eng', 'French South Terr')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (72, 'eng', 'Djibouti')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (73, 'eng', 'Georgia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (74, 'eng', 'Gabon')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (75, 'eng', 'Gambia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (76, 'eng', 'Gaza Strip')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (79, 'eng', 'Germany')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (80, 'eng', 'Bosnia and Herzegovina')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (81, 'eng', 'Ghana')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (82, 'eng', 'Gibraltar')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (83, 'eng', 'Kiribati')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (84, 'eng', 'Greece')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (85, 'eng', 'Greenland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (86, 'eng', 'Grenada')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (46, 'fre', 'Congo')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (47, 'fre', 'Cook, Îles')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (48, 'fre', 'Costa Rica')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (49, 'fre', 'Cuba')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (50, 'fre', 'Chypre')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (52, 'fre', 'Azerbaijan, Republique de')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (53, 'fre', 'Bénin')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (54, 'fre', 'Danemark')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (55, 'fre', 'Dominique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (56, 'fre', 'Dominicaine, République')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (57, 'fre', 'Bélarus')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (58, 'fre', 'Équateur')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (59, 'fre', 'Égypte')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (60, 'fre', 'El Salvador')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (61, 'fre', 'Guinée équatoriale')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (63, 'fre', 'Estonie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (64, 'fre', 'Faeroe Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (65, 'fre', 'Falkland, Iles')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (66, 'fre', 'Fiji, Iles')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (67, 'fre', 'Finlande')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (68, 'fre', 'France')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (69, 'fre', 'Guyane française')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (70, 'fre', 'Polynésie française')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (71, 'fre', 'French South Terr')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (72, 'fre', 'Djibouti')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (73, 'fre', 'Géorgie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (74, 'fre', 'Gabon')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (75, 'fre', 'Gambie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (76, 'fre', 'Bande de Gaza')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (79, 'fre', 'Allemagne')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (80, 'fre', 'Bosnie-et-Herzégovine')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (81, 'fre', 'Ghana')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (82, 'fre', 'Gibraltar')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (83, 'fre', 'Kiribati')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (84, 'fre', 'Grèce')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (85, 'fre', 'Groenland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (86, 'fre', 'Grenade')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (87, 'fre', 'Guadeloupe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (93, 'eng', 'Haiti')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (95, 'eng', 'Honduras')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (97, 'eng', 'Hungary')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (98, 'eng', 'Croatia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (99, 'eng', 'Iceland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (100, 'eng', 'India')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (101, 'eng', 'Indonesia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (102, 'eng', 'Iran, Islamic Rep of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (103, 'eng', 'Iraq')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (104, 'eng', 'Ireland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (105, 'eng', 'Israel')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (106, 'eng', 'Italy')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (107, 'eng', 'Ivory Coast')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (108, 'eng', 'Kazakhstan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (109, 'eng', 'Jamaica')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (110, 'eng', 'Japan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (112, 'eng', 'Jordan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (113, 'eng', 'Kyrgyzstan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (114, 'eng', 'Kenya')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (115, 'eng', 'Cambodia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (116, 'eng', 'Korea, Dem People''s Rep')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (117, 'eng', 'Korea, Republic of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (118, 'eng', 'Kuwait')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (119, 'eng', 'Latvia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (120, 'eng', 'Laos')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (121, 'eng', 'Lebanon')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (122, 'eng', 'Lesotho')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (123, 'eng', 'Liberia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (124, 'eng', 'Libyan Arab Jamahiriya')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (125, 'eng', 'Liechtensten')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (126, 'eng', 'Lithuania')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (127, 'eng', 'Marshall Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (129, 'eng', 'Madagascar')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (130, 'eng', 'Malawi')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (131, 'eng', 'Malaysia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (132, 'eng', 'Maldives')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (133, 'eng', 'Mali')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (134, 'eng', 'Malta')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (135, 'eng', 'Martinique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (136, 'eng', 'Mauritania')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (93, 'fre', 'Haïti')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (95, 'fre', 'Honduras')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (97, 'fre', 'Hongrie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (98, 'fre', 'Croatie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (99, 'fre', 'Islande')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (100, 'fre', 'Inde')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (101, 'fre', 'Indonésie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (102, 'fre', 'Iran')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (103, 'fre', 'Irak')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (104, 'fre', 'Irlande')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (105, 'fre', 'Israël')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (106, 'fre', 'Italie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (107, 'fre', 'Côte d''Ivoire')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (108, 'fre', 'Kazakhstan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (109, 'fre', 'Jamaïque')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (110, 'fre', 'Japon')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (112, 'fre', 'Jordanie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (113, 'fre', 'Kirghizistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (114, 'fre', 'Kenya')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (115, 'fre', 'Cambodge')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (116, 'fre', 'Corée du nord')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (117, 'fre', 'Corée du sud')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (118, 'fre', 'Koweït')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (119, 'fre', 'Lettonie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (120, 'fre', 'Lao, République démocratique pop')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (121, 'fre', 'Liban')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (122, 'fre', 'Lesotho')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (123, 'fre', 'Liberia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (124, 'fre', 'Libyan Arab Jamahiriya')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (125, 'fre', 'Liechtensten')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (126, 'fre', 'Lituanie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (127, 'fre', 'Marshall Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (129, 'fre', 'Madagascar')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (130, 'fre', 'Malawi')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (131, 'fre', 'Malaisie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (132, 'fre', 'Maldives')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (133, 'fre', 'Mali')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (134, 'fre', 'Malte')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (135, 'fre', 'Martinique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (141, 'eng', 'Mongolia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (142, 'eng', 'Montserrat')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (143, 'eng', 'Morocco')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (144, 'eng', 'Mozambique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (145, 'eng', 'Micronesia,Fed States of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (146, 'eng', 'Moldova, Republic of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (147, 'eng', 'Namibia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (148, 'eng', 'Nauru')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (149, 'eng', 'Nepal')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (150, 'eng', 'Netherlands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (151, 'eng', 'Neth Antilles')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (153, 'eng', 'NewCaledonia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (154, 'eng', 'Macedonia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (155, 'eng', 'Vanuatu')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (156, 'eng', 'New Zealand')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (157, 'eng', 'Nicaragua')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (158, 'eng', 'Niger')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (159, 'eng', 'Nigeria')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (160, 'eng', 'Niue')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (161, 'eng', 'Norfolk Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (162, 'eng', 'Norway')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (163, 'eng', 'Northern Mariana Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (165, 'eng', 'Pakistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (166, 'eng', 'Panama')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (167, 'eng', 'Czech Republic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (168, 'eng', 'Papua New Guinea')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (169, 'eng', 'Paraguay')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (170, 'eng', 'Peru')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (171, 'eng', 'Philippines')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (172, 'eng', 'Pitcairn Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (173, 'eng', 'Poland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (174, 'eng', 'Portugal')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (175, 'eng', 'Guinea-Bissau')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (176, 'eng', 'East Timor')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (177, 'eng', 'Puerto Rico')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (178, 'eng', 'Eritrea')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (179, 'eng', 'Qatar')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (181, 'eng', 'Zimbabwe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (182, 'eng', 'Reunion')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (141, 'fre', 'Mongolie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (142, 'fre', 'Montserrat')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (143, 'fre', 'Maroc')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (144, 'fre', 'Mozambique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (145, 'fre', 'Micronesia,Fed States of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (146, 'fre', 'Moldova, Republic of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (147, 'fre', 'Namibie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (148, 'fre', 'Nauru')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (149, 'fre', 'Népal')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (150, 'fre', 'Pays-Bas')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (151, 'fre', 'Neth Antilles')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (153, 'fre', 'NewCaledonia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (154, 'fre', 'Macédoine, L''ex-République yougo')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (155, 'fre', 'Vanuatu')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (156, 'fre', 'Nouvelle-Zélande')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (157, 'fre', 'Nicaragua')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (158, 'fre', 'Niger')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (159, 'fre', 'Nigeria')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (160, 'fre', 'Niué')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (161, 'fre', 'Norfolk, Île')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (162, 'fre', 'Norvège')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (163, 'fre', 'Northern Mariana Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (165, 'fre', 'Pakistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (166, 'fre', 'Panama')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (167, 'fre', 'Tchèque, République')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (168, 'fre', 'Papouasie-Nouvelle-Guinée')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (169, 'fre', 'Paraguay')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (170, 'fre', 'Pérou')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (171, 'fre', 'Philippines')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (172, 'fre', 'Pitcairn Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (173, 'fre', 'Pologne')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (174, 'fre', 'Portugal')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (175, 'fre', 'Guinée-Bissau')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (176, 'fre', 'Timor est')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (177, 'fre', 'Porto Rico')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (178, 'fre', 'Érythrée')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (179, 'fre', 'Qatar')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (188, 'eng', 'Saint Kitts and Nevis')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (189, 'eng', 'Saint Lucia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (190, 'eng', 'Saint Pierre & Miquelon')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (191, 'eng', 'Saint Vincent/Grenadines')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (192, 'eng', 'San Marino')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (193, 'eng', 'Sao Tome and Principe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (194, 'eng', 'Saudi Arabia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (195, 'eng', 'Senegal')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (196, 'eng', 'Seychelles')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (197, 'eng', 'Sierra Leone')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (198, 'eng', 'Slovenia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (199, 'eng', 'Slovakia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (200, 'eng', 'Singapore')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (201, 'eng', 'Somalia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (202, 'eng', 'South Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (203, 'eng', 'Spain')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (205, 'eng', 'Western Sahara')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (206, 'eng', 'Sudan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (207, 'eng', 'Suriname')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (208, 'eng', 'Tajikistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (209, 'eng', 'Swaziland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (210, 'eng', 'Sweden')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (211, 'eng', 'Switzerland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (212, 'eng', 'Syrian Arab Republic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (213, 'eng', 'Turkmenistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (214, 'eng', 'China, Taiwan Prov of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (215, 'eng', 'Tanzania, United Rep of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (216, 'eng', 'Thailand')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (217, 'eng', 'Togo')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (218, 'eng', 'Tokelau')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (219, 'eng', 'Tonga')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (220, 'eng', 'Trinidad and Tobago')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (221, 'eng', 'Oman')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (222, 'eng', 'Tunisia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (223, 'eng', 'Turkey')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (224, 'eng', 'Turks and Caicos Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (225, 'eng', 'United Arab Emirates')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (187, 'fre', 'Sainte-Hélène')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (188, 'fre', 'Saint-Kitts-et-Nevis')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (189, 'fre', 'Sainte-Lucie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (190, 'fre', 'Saint Pierre & Miquelon')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (191, 'fre', 'Saint Vincent/Grenadines')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (192, 'fre', 'Saint-Marin')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (193, 'fre', 'Sao Tomé-et-Principe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (194, 'fre', 'Saudi Arabia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (195, 'fre', 'Sénégal')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (196, 'fre', 'Seychelles')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (197, 'fre', 'Sierra Leone')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (198, 'fre', 'Slovénie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (199, 'fre', 'Slovaquie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (200, 'fre', 'Singapour')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (201, 'fre', 'Somalie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (202, 'fre', 'Afrique du Sud')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (203, 'fre', 'Espagne')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (205, 'fre', 'Sahara occidental')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (206, 'fre', 'Soudan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (207, 'fre', 'Suriname')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (208, 'fre', 'Tadjikistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (209, 'fre', 'Swaziland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (210, 'fre', 'Suède')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (211, 'fre', 'Suisse')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (212, 'fre', 'Syrie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (213, 'fre', 'Turkménistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (214, 'fre', 'China, Taiwan Prov of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (215, 'fre', 'Tanzanie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (216, 'fre', 'Thaïlande')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (217, 'fre', 'Togo')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (218, 'fre', 'Tokelau')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (219, 'fre', 'Tonga')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (220, 'fre', 'Trinité-et-Tobago')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (221, 'fre', 'Oman')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (222, 'fre', 'Tunisie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (223, 'fre', 'Turquie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (224, 'fre', 'Turks and Caicos Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (225, 'fre', 'Émirats arabes unis')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (232, 'eng', 'US Minor Outlying Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (233, 'eng', 'Burkina Faso')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (234, 'eng', 'Uruguay')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (235, 'eng', 'Uzbekistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (236, 'eng', 'Venezuela')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (237, 'eng', 'Viet Nam')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (238, 'eng', 'Ethiopia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (239, 'eng', 'British Virgin Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (240, 'eng', 'US Virgin Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (242, 'eng', 'Wake Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (243, 'eng', 'Wallis and Futuna Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (244, 'eng', 'Samoa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (245, 'eng', 'West Bank')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (249, 'eng', 'Yemen')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (250, 'eng', 'Congo, Dem Republic of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (251, 'eng', 'Zambia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (255, 'eng', 'Belgium')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (256, 'eng', 'Luxembourg')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (258, 'eng', 'Anguilla')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (260, 'eng', 'Svalbard Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (264, 'eng', 'Isle of Man')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (270, 'eng', 'Mayotte')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (271, 'eng', 'SouthGeorgia/Sandwich Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (303, 'eng', 'Western Europe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (307, 'eng', 'Intergvt Author Devpment')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (310, 'eng', 'North Western Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (311, 'eng', 'Western Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (312, 'eng', 'Central Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (313, 'eng', 'Eastern Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (314, 'eng', 'Southern Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (315, 'eng', 'Latin Amer & Caribbean')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (316, 'eng', 'Central America')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (317, 'eng', 'Caribbean')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (318, 'eng', 'South America')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (319, 'eng', 'Near East')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (320, 'eng', 'Near East in Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (322, 'eng', 'Far East')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (231, 'fre', 'États-Unis')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (232, 'fre', 'US Minor Outlying Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (233, 'fre', 'Burkina Faso')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (234, 'fre', 'Uruguay')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (235, 'fre', 'Ouzbékistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (236, 'fre', 'Venezuela')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (237, 'fre', 'Viet Nam')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (238, 'fre', 'Éthiopie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (239, 'fre', 'British Virgin Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (240, 'fre', 'US Virgin Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (242, 'fre', 'Wake Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (243, 'fre', 'Wallis and Futuna Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (244, 'fre', 'Samoa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (245, 'fre', 'West Bank')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (249, 'fre', 'Yemen')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (250, 'fre', 'Congo, République démocratique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (251, 'fre', 'Zambie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (255, 'fre', 'Belgique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (256, 'fre', 'Luxembourg')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (258, 'fre', 'Anguilla')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (260, 'fre', 'Svalbard Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (264, 'fre', 'Île de Man')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (270, 'fre', 'Mayotte')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (271, 'fre', 'Ile Sandwich')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (303, 'fre', 'Europe de l''ouest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (307, 'fre', 'Intergvt Author Devpment')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (310, 'fre', 'Afrique - nord-ouest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (311, 'fre', 'Afrique de l''ouest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (312, 'fre', 'Afrique - centre')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (313, 'fre', 'Afrique de l''est')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (314, 'fre', 'Afrique - sud')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (315, 'fre', 'Caraïbes & Amérique latine')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (316, 'fre', 'Amérique centrale')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (317, 'fre', 'Caraïbes')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (318, 'fre', 'Amérique du sud')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1, 'chi', 'Armenia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (2, 'chi', 'Afghanistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (3, 'chi', 'Albania')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (4, 'chi', 'Algeria')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (5, 'chi', 'American Samoa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (6, 'chi', 'Andorra')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (7, 'chi', 'Angola')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (8, 'chi', 'Antigua and Barbuda')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (9, 'chi', 'Argentina')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (10, 'chi', 'Australia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (11, 'chi', 'Austria')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (12, 'chi', 'Bahamas')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (13, 'chi', 'Bahrain')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (14, 'chi', 'Barbados')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (16, 'chi', 'Bangladesh')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (17, 'chi', 'Bermuda')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (18, 'chi', 'Bhutan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (19, 'chi', 'Bolivia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (20, 'chi', 'Botswana')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (21, 'chi', 'Brazil')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (22, 'chi', 'Aruba')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (325, 'eng', 'Near East and North Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (334, 'eng', 'Eastern Europe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (335, 'eng', 'Europe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (337, 'eng', 'Asia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (338, 'eng', 'Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (348, 'eng', 'Latin America')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (390, 'eng', 'South of Sahara')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (421, 'eng', 'North Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (427, 'eng', 'Europe, Non-EU Countries')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (428, 'eng', 'North America')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (469, 'eng', 'USSR, Former Area of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1008, 'eng', 'Arctic Sea')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1009, 'eng', 'Atlantic, Northwest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1010, 'eng', 'Atlantic, Northeast')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1011, 'eng', 'Atlantic, Western Central')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1012, 'eng', 'Atlantic, Eastern Central')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1013, 'eng', 'Mediterran and Black Sea')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1014, 'eng', 'Atlantic, Southwest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1015, 'eng', 'Atlantic, Southeast')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1016, 'eng', 'Atlantic, Antarctic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1017, 'eng', 'Indian Ocean, Western')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1018, 'eng', 'Indian Ocean, Eastern')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1019, 'eng', 'Indian Ocean, Antarctic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1020, 'eng', 'Pacific, Northwest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1021, 'eng', 'Pacific, Northeast')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1022, 'eng', 'Pacific, Western Central')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1023, 'eng', 'Pacific, Eastern Central')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1024, 'eng', 'Pacific, Southwest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1025, 'eng', 'Pacific, Southeast')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (324, 'fre', 'Asie - est et sud-est')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (325, 'fre', 'Afrique - nord et proche orient')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (334, 'fre', 'Europe de l''Est')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (335, 'fre', 'Europe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (337, 'fre', 'Asie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (338, 'fre', 'Afrique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (348, 'fre', 'Amérique Latine')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (390, 'fre', 'Sud du Sahara')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (421, 'fre', 'Afrique du Nord')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (427, 'fre', 'Europe, Pays hors EU')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (428, 'fre', 'Amérique du Nord')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (469, 'fre', 'URSS, Ancienne')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1008, 'fre', 'Mer Arctique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1009, 'fre', 'Atlantique, Nord-ouest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1010, 'fre', 'Atlantique, Nord-est')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1011, 'fre', 'Atlantique, Centre-ouest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1012, 'fre', 'Atlantique, Centre-est')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1013, 'fre', 'Méditerranée et Mer noire')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1014, 'fre', 'Atlantique, Sud-ouest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1015, 'fre', 'Atlantique, Sud-est')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1016, 'fre', 'Atlantique, Antarctique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1017, 'fre', 'Ocean Indien, Ouest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1018, 'fre', 'Ocean Indien, Est')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1019, 'fre', 'Ocean Indien, Antarctique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1020, 'fre', 'Pacifique, Nord-ouest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1021, 'fre', 'Pacifique, Nord-est')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1022, 'fre', 'Pacifique, Centre-ouest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1023, 'fre', 'Pacific, Centre-est')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1024, 'fre', 'Pacifique, Sud-ouest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (23, 'chi', 'Belize')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (24, 'chi', 'British Indian Ocean Ter')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (25, 'chi', 'Solomon Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (26, 'chi', 'Brunei Darussalam')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (27, 'chi', 'Bulgaria')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (28, 'chi', 'Myanmar')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (29, 'chi', 'Burundi')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (30, 'chi', 'Antarctica')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (31, 'chi', 'Bouvet Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (32, 'chi', 'Cameroon')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (33, 'chi', 'Canada')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (35, 'chi', 'Cape Verde')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (36, 'chi', 'Cayman Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (37, 'chi', 'Central African Republic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (38, 'chi', 'Sri Lanka')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (39, 'chi', 'Chad')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (40, 'chi', 'Chile')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (41, 'chi', 'China, Mainland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (42, 'chi', 'Christmas Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (43, 'chi', 'Cocos Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (44, 'chi', 'Colombia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (45, 'chi', 'Comoros')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (46, 'chi', 'Congo, Republic of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (47, 'chi', 'Cook Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (48, 'chi', 'Costa Rica')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (49, 'chi', 'Cuba')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (50, 'chi', 'Cyprus')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (52, 'chi', 'Azerbaijan, Republic of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (53, 'chi', 'Benin')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (54, 'chi', 'Denmark')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (55, 'chi', 'Dominica')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (56, 'chi', 'Dominican Republic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (57, 'chi', 'Belarus')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (58, 'chi', 'Ecuador')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (59, 'chi', 'Egypt')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (60, 'chi', 'El Salvador')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (61, 'chi', 'Equatorial Guinea')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (63, 'chi', 'Estonia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (64, 'chi', 'Faeroe Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (65, 'chi', 'Falkland Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (66, 'chi', 'Fiji Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (67, 'chi', 'Finland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (68, 'chi', 'France')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (69, 'chi', 'French Guiana')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (70, 'chi', 'French Polynesia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (71, 'chi', 'French South Terr')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (72, 'chi', 'Djibouti')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (73, 'chi', 'Georgia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (74, 'chi', 'Gabon')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (75, 'chi', 'Gambia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (76, 'chi', 'Gaza Strip')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (79, 'chi', 'Germany')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (80, 'chi', 'Bosnia and Herzegovina')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (81, 'chi', 'Ghana')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (82, 'chi', 'Gibraltar')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (83, 'chi', 'Kiribati')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (84, 'chi', 'Greece')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (85, 'chi', 'Greenland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (86, 'chi', 'Grenada')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (87, 'chi', 'Guadeloupe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (88, 'chi', 'Guam')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (89, 'chi', 'Guatemala')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (90, 'chi', 'Guinea')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (91, 'chi', 'Guyana')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (92, 'chi', 'Heard and McDonald Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (93, 'chi', 'Haiti')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (95, 'chi', 'Honduras')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (97, 'chi', 'Hungary')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (98, 'chi', 'Croatia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (99, 'chi', 'Iceland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (100, 'chi', 'India')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (101, 'chi', 'Indonesia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (102, 'chi', 'Iran, Islamic Rep of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (103, 'chi', 'Iraq')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (104, 'chi', 'Ireland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (105, 'chi', 'Israel')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (106, 'chi', 'Italy')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (107, 'chi', 'Ivory Coast')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (108, 'chi', 'Kazakhstan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (109, 'chi', 'Jamaica')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (110, 'chi', 'Japan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (112, 'chi', 'Jordan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (113, 'chi', 'Kyrgyzstan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (114, 'chi', 'Kenya')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (115, 'chi', 'Cambodia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (116, 'chi', 'Korea, Dem People''s Rep')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (117, 'chi', 'Korea, Republic of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (118, 'chi', 'Kuwait')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (119, 'chi', 'Latvia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (120, 'chi', 'Laos')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (121, 'chi', 'Lebanon')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (122, 'chi', 'Lesotho')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (123, 'chi', 'Liberia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (124, 'chi', 'Libyan Arab Jamahiriya')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (125, 'chi', 'Liechtensten')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (126, 'chi', 'Lithuania')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (127, 'chi', 'Marshall Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (129, 'chi', 'Madagascar')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (130, 'chi', 'Malawi')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (131, 'chi', 'Malaysia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (132, 'chi', 'Maldives')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (133, 'chi', 'Mali')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (134, 'chi', 'Malta')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (135, 'chi', 'Martinique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (136, 'chi', 'Mauritania')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (137, 'chi', 'Mauritius')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (138, 'chi', 'Mexico')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (139, 'chi', 'Midway Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (140, 'chi', 'Monaco')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (141, 'chi', 'Mongolia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (142, 'chi', 'Montserrat')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (143, 'chi', 'Morocco')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (144, 'chi', 'Mozambique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (145, 'chi', 'Micronesia,Fed States of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (146, 'chi', 'Moldova, Republic of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (147, 'chi', 'Namibia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (148, 'chi', 'Nauru')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (149, 'chi', 'Nepal')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (150, 'chi', 'Netherlands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (151, 'chi', 'Neth Antilles')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (153, 'chi', 'NewCaledonia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (154, 'chi', 'Macedonia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (155, 'chi', 'Vanuatu')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (156, 'chi', 'New Zealand')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (157, 'chi', 'Nicaragua')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (158, 'chi', 'Niger')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (159, 'chi', 'Nigeria')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (160, 'chi', 'Niue')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (161, 'chi', 'Norfolk Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (162, 'chi', 'Norway')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (163, 'chi', 'Northern Mariana Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (165, 'chi', 'Pakistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (166, 'chi', 'Panama')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (167, 'chi', 'Czech Republic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (168, 'chi', 'Papua New Guinea')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (169, 'chi', 'Paraguay')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (170, 'chi', 'Peru')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (171, 'chi', 'Philippines')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (172, 'chi', 'Pitcairn Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (173, 'chi', 'Poland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (174, 'chi', 'Portugal')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (175, 'chi', 'Guinea-Bissau')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (176, 'chi', 'East Timor')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (177, 'chi', 'Puerto Rico')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (178, 'chi', 'Eritrea')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (179, 'chi', 'Qatar')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (181, 'chi', 'Zimbabwe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (182, 'chi', 'Reunion')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (183, 'chi', 'Romania')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (184, 'chi', 'Rwanda')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (185, 'chi', 'Russian Federation')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (187, 'chi', 'Saint Helena')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (188, 'chi', 'Saint Kitts and Nevis')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (189, 'chi', 'Saint Lucia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (190, 'chi', 'Saint Pierre & Miquelon')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (191, 'chi', 'Saint Vincent/Grenadines')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (192, 'chi', 'San Marino')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (193, 'chi', 'Sao Tome and Principe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (194, 'chi', 'Saudi Arabia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (195, 'chi', 'Senegal')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (196, 'chi', 'Seychelles')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (197, 'chi', 'Sierra Leone')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (198, 'chi', 'Slovenia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (199, 'chi', 'Slovakia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (200, 'chi', 'Singapore')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (201, 'chi', 'Somalia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (202, 'chi', 'South Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (203, 'chi', 'Spain')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (205, 'chi', 'Western Sahara')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (206, 'chi', 'Sudan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (207, 'chi', 'Suriname')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (208, 'chi', 'Tajikistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (209, 'chi', 'Swaziland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (210, 'chi', 'Sweden')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (211, 'chi', 'Switzerland')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (212, 'chi', 'Syrian Arab Republic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (213, 'chi', 'Turkmenistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (214, 'chi', 'China, Taiwan Prov of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (215, 'chi', 'Tanzania, United Rep of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (216, 'chi', 'Thailand')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (217, 'chi', 'Togo')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (218, 'chi', 'Tokelau')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (219, 'chi', 'Tonga')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (220, 'chi', 'Trinidad and Tobago')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (221, 'chi', 'Oman')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (222, 'chi', 'Tunisia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (223, 'chi', 'Turkey')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (224, 'chi', 'Turks and Caicos Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (225, 'chi', 'United Arab Emirates')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (226, 'chi', 'Uganda')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (227, 'chi', 'Tuvalu')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (229, 'chi', 'United Kingdom')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (230, 'chi', 'Ukraine')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (231, 'chi', 'United States of America')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (232, 'chi', 'US Minor Outlying Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (233, 'chi', 'Burkina Faso')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (234, 'chi', 'Uruguay')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (235, 'chi', 'Uzbekistan')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (236, 'chi', 'Venezuela')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (237, 'chi', 'Viet Nam')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (238, 'chi', 'Ethiopia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (239, 'chi', 'British Virgin Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (240, 'chi', 'US Virgin Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (242, 'chi', 'Wake Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (243, 'chi', 'Wallis and Futuna Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (244, 'chi', 'Samoa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (245, 'chi', 'West Bank')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (249, 'chi', 'Yemen')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (250, 'chi', 'Congo, Dem Republic of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (251, 'chi', 'Zambia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (255, 'chi', 'Belgium')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (256, 'chi', 'Luxembourg')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (258, 'chi', 'Anguilla')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (260, 'chi', 'Svalbard Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (264, 'chi', 'Isle of Man')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (270, 'chi', 'Mayotte')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (271, 'chi', 'SouthGeorgia/Sandwich Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (303, 'chi', 'Western Europe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (307, 'chi', 'Intergvt Author Devpment')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (310, 'chi', 'North Western Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (311, 'chi', 'Western Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (312, 'chi', 'Central Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (313, 'chi', 'Eastern Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (314, 'chi', 'Southern Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (315, 'chi', 'Latin Amer & Caribbean')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (316, 'chi', 'Central America')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (317, 'chi', 'Caribbean')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (318, 'chi', 'South America')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (319, 'chi', 'Near East')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (320, 'chi', 'Near East in Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (321, 'chi', 'Near East in Asia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (322, 'chi', 'Far East')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (323, 'chi', 'South Asia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (324, 'chi', 'East & South East Asia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (325, 'chi', 'Near East and North Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (334, 'chi', 'Eastern Europe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (335, 'chi', 'Europe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (337, 'chi', 'Asia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (338, 'chi', 'Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (348, 'chi', 'Latin America')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (390, 'chi', 'South of Sahara')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (421, 'chi', 'North Africa')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (427, 'chi', 'Europe, Non-EU Countries')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (428, 'chi', 'North America')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (469, 'chi', 'USSR, Former Area of')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1008, 'chi', 'Arctic Sea')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1009, 'chi', 'Atlantic, Northwest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1010, 'chi', 'Atlantic, Northeast')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1011, 'chi', 'Atlantic, Western Central')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1012, 'chi', 'Atlantic, Eastern Central')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1013, 'chi', 'Mediterran and Black Sea')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1014, 'chi', 'Atlantic, Southwest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1015, 'chi', 'Atlantic, Southeast')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1016, 'chi', 'Atlantic, Antarctic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1017, 'chi', 'Indian Ocean, Western')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1018, 'chi', 'Indian Ocean, Eastern')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1019, 'chi', 'Indian Ocean, Antarctic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1020, 'chi', 'Pacific, Northwest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1021, 'chi', 'Pacific, Northeast')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1022, 'chi', 'Pacific, Western Central')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1023, 'chi', 'Pacific, Eastern Central')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1024, 'chi', 'Pacific, Southwest')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1025, 'chi', 'Pacific, Southeast')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1026, 'chi', 'Pacific, Antarctic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1220, 'chi', 'All fishing areas')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (9999, 'chi', 'World')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (42, 'eng', 'Christmas Island')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (43, 'eng', 'Cocos Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (44, 'eng', 'Colombia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (87, 'eng', 'Guadeloupe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (88, 'eng', 'Guam')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (89, 'eng', 'Guatemala')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (90, 'eng', 'Guinea')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (91, 'eng', 'Guyana')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (92, 'eng', 'Heard and McDonald Is')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (137, 'eng', 'Mauritius')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (138, 'eng', 'Mexico')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (139, 'eng', 'Midway Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (140, 'eng', 'Monaco')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (183, 'eng', 'Romania')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (184, 'eng', 'Rwanda')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (185, 'eng', 'Russian Federation')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (187, 'eng', 'Saint Helena')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (226, 'eng', 'Uganda')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (227, 'eng', 'Tuvalu')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (229, 'eng', 'United Kingdom')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (230, 'eng', 'Ukraine')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (231, 'eng', 'United States of America')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (321, 'eng', 'Near East in Asia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (323, 'eng', 'South Asia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (324, 'eng', 'East & South East Asia')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1026, 'eng', 'Pacific, Antarctic')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1220, 'eng', 'All fishing areas')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (9999, 'eng', 'World')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (41, 'fre', 'Chine')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (42, 'fre', 'Île de Christmas')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (43, 'fre', 'Cocos Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (44, 'fre', 'Colombie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (45, 'fre', 'Comores')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (88, 'fre', 'Guam')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (89, 'fre', 'Guatemala')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (90, 'fre', 'Guinée')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (91, 'fre', 'Guyana')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (92, 'fre', 'Heard, Île, et îles McDonald')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (136, 'fre', 'Mauritanie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (137, 'fre', 'Maurice')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (138, 'fre', 'Mexique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (139, 'fre', 'Midway Islands')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (140, 'fre', 'Monaco')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (181, 'fre', 'Zimbabwe')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (182, 'fre', 'Réunion')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (183, 'fre', 'Roumanie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (184, 'fre', 'Rwanda')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (185, 'fre', 'Fédération de Russie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (226, 'fre', 'Ouganda')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (227, 'fre', 'Tuvalu')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (229, 'fre', 'Royaume-Uni')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (230, 'fre', 'Ukraine')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (319, 'fre', 'Proche orient')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (320, 'fre', 'Proche orient en Afrique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (321, 'fre', 'Proche orient en Asie')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (322, 'fre', 'Moyen orient')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (323, 'fre', 'Asie - sud')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1025, 'fre', 'Pacifique, Sud-est')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1026, 'fre', 'Pacifique, Antarctique')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (1220, 'fre', 'Toutes zones de pêches')");
        $this->addSql("INSERT INTO public.regionsdes (iddes, langid, label) VALUES (9999, 'fre', 'Monde')");

        $this->addSql("INSERT INTO public.schematron (id, displaypriority, filename, schemaname) VALUES (40000, 0, 'schematron-rules-inspire.xsl', 'iso19139')");
        $this->addSql("INSERT INTO public.schematron (id, displaypriority, filename, schemaname) VALUES (40778, 1, 'schematron-rules-inspire-strict.disabled.xsl', 'iso19139')");
        $this->addSql("INSERT INTO public.schematron (id, displaypriority, filename, schemaname) VALUES (40780, 2, 'schematron-rules-inspire-sds.disabled.xsl', 'iso19139')");
        $this->addSql("INSERT INTO public.schematron (id, displaypriority, filename, schemaname) VALUES (40782, 3, 'schematron-rules-url-check.report_only.xsl', 'iso19139')");
        $this->addSql("INSERT INTO public.schematron (id, displaypriority, filename, schemaname) VALUES (40002, 4, 'schematron-rules-geonetwork.xsl', 'iso19139')");
        $this->addSql("INSERT INTO public.schematron (id, displaypriority, filename, schemaname) VALUES (40004, 5, 'schematron-rules-iso.xsl', 'iso19139')");

        $this->addSql("INSERT INTO public.schematroncriteriagroup (name, schematronid, requirement) VALUES ('*Generated*', 40000, 'REQUIRED')");
        $this->addSql("INSERT INTO public.schematroncriteriagroup (name, schematronid, requirement) VALUES ('*Generated*', 40002, 'REQUIRED')");
        $this->addSql("INSERT INTO public.schematroncriteriagroup (name, schematronid, requirement) VALUES ('*Generated*', 40004, 'REQUIRED')");
        $this->addSql("INSERT INTO public.schematroncriteriagroup (name, schematronid, requirement) VALUES ('*Generated*', 40778, 'DISABLED')");
        $this->addSql("INSERT INTO public.schematroncriteriagroup (name, schematronid, requirement) VALUES ('*Generated*', 40780, 'DISABLED')");
        $this->addSql("INSERT INTO public.schematroncriteriagroup (name, schematronid, requirement) VALUES ('*Generated*', 40782, 'REPORT_ONLY')");

        $this->addSql("INSERT INTO public.schematroncriteria (id, type, uitype, uivalue, value, group_name, group_schematronid) VALUES (40001, 'ALWAYS_ACCEPT', NULL, NULL, '_ignored_', '*Generated*', 40000)");
        $this->addSql("INSERT INTO public.schematroncriteria (id, type, uitype, uivalue, value, group_name, group_schematronid) VALUES (40003, 'ALWAYS_ACCEPT', NULL, NULL, '_ignored_', '*Generated*', 40002)");
        $this->addSql("INSERT INTO public.schematroncriteria (id, type, uitype, uivalue, value, group_name, group_schematronid) VALUES (40005, 'ALWAYS_ACCEPT', NULL, NULL, '_ignored_', '*Generated*', 40004)");
        $this->addSql("INSERT INTO public.schematroncriteria (id, type, uitype, uivalue, value, group_name, group_schematronid) VALUES (40779, 'ALWAYS_ACCEPT', NULL, NULL, '_ignored_', '*Generated*', 40778)");
        $this->addSql("INSERT INTO public.schematroncriteria (id, type, uitype, uivalue, value, group_name, group_schematronid) VALUES (40781, 'ALWAYS_ACCEPT', NULL, NULL, '_ignored_', '*Generated*', 40780)");
        $this->addSql("INSERT INTO public.schematroncriteria (id, type, uitype, uivalue, value, group_name, group_schematronid) VALUES (40783, 'ALWAYS_ACCEPT', NULL, NULL, '_ignored_', '*Generated*', 40782)");

        $this->addSql("INSERT INTO public.schematrondes (iddes, label, langid) VALUES (40000, 'schematron-rules-inspire', 'eng')");
        $this->addSql("INSERT INTO public.schematrondes (iddes, label, langid) VALUES (40002, 'schematron-rules-geonetwork', 'eng')");
        $this->addSql("INSERT INTO public.schematrondes (iddes, label, langid) VALUES (40004, 'schematron-rules-iso', 'eng')");
        $this->addSql("INSERT INTO public.schematrondes (iddes, label, langid) VALUES (40778, 'schematron-rules-inspire-strict', 'eng')");
        $this->addSql("INSERT INTO public.schematrondes (iddes, label, langid) VALUES (40780, 'schematron-rules-inspire-sds', 'eng')");
        $this->addSql("INSERT INTO public.schematrondes (iddes, label, langid) VALUES (40782, 'schematron-rules-url-check', 'eng')");

        $this->addSql("INSERT INTO public.selections (id, name, iswatchable) VALUES (0, 'PreferredList', 'n')");
        $this->addSql("INSERT INTO public.selections (id, name, iswatchable) VALUES (1, 'WatchList', 'y')");

        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'ara')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'ara')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'cat')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'cat')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'chi')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'chi')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'dut')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'dut')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'eng')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'eng')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'fin')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'fin')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Fiches préférées', 'fre')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Fiches observées', 'fre')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'ger')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'ger')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'ita')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'ita')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'nor')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'nor')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'pol')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'pol')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'por')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'por')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'rus')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'rus')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'spa')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'spa')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'tur')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'tur')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (0, 'Preferred records', 'vie')");
        $this->addSql("INSERT INTO public.selectionsdes (iddes, label, langid) VALUES (1, 'Watch list', 'vie')");

        $this->addSql("SELECT pg_catalog.setval('public.serviceparameter_id_seq', 1, false)");

        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/intranet/network', '127.0.0.1', 0, 310, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/intranet/netmask', '255.0.0.0', 0, 320, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/proxy/use', 'false', 2, 510, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/proxy/host', NULL, 0, 520, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/proxy/port', NULL, 1, 530, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/proxy/username', NULL, 0, 540, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/proxy/password', NULL, 0, 550, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/feedback/email', NULL, 0, 610, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/feedback/mailServer/host', NULL, 0, 630, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/feedback/mailServer/port', '25', 1, 640, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/csw/enable', 'true', 2, 1210, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/csw/contactId', NULL, 0, 1220, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/site/svnUuid', 'f900dee6-0873-4a41-b726-48d36166754a', 0, 170, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/site/name', 'PRODIGE', 0, 110, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/site/siteId', '7fc45be3-9aba-4198-920c-b8737112d522', 0, 120, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/site/organization', 'PRODIGE', 0, 130, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/server/port', '443', 1, 220, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/userSelfRegistration/enable', 'false', 2, 1910, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/searchStats/enable', 'true', 2, 2510, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/inspire/enableSearchPanel', 'true', 2, 7220, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/harvester/enableEditing', 'false', 2, 9010, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/metadataprivs/usergrouponly', 'false', 2, 9180, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/hidewithheldelements/enable', 'false', 2, 9570, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/server/securePort', '443', 1, 240, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/selectionmanager/maxrecords', '1000', 1, 910, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/csw/metadataPublic', 'false', 2, 1310, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/clickablehyperlinks/enable', 'true', 2, 2010, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/localrating/enable', 'false', 2, 2110, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/downloadservice/withdisclaimer', 'false', 0, 2230, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/autofixing/enable', 'true', 2, 2410, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/indexoptimizer/enable', 'true', 2, 6010, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/indexoptimizer/at/hour', '0', 1, 6030, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/indexoptimizer/at/min', '0', 1, 6040, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/indexoptimizer/at/sec', '0', 1, 6050, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/indexoptimizer/interval', '', 0, 6060, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/indexoptimizer/interval/day', '0', 1, 6070, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/indexoptimizer/interval/hour', '24', 1, 6080, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/indexoptimizer/interval/min', '0', 1, 6090, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/oai/mdmode', '1', 0, 7010, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/oai/tokentimeout', '3600', 1, 7020, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/oai/cachesize', '60', 1, 7030, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/inspire/enable', 'false', 2, 7210, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/threadedindexing/maxthreads', '1', 1, 9210, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/autodetect/enable', 'true', 2, 9510, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/requestedLanguage/only', 'false', 0, 9530, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/requestedLanguage/sorted', 'false', 2, 9540, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/feedback/mailServer/username', '', 0, 642, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/feedback/mailServer/password', '', 0, 643, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/feedback/mailServer/ssl', 'false', 2, 641, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/harvesting/mail/recipient', NULL, 0, 9020, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/harvesting/mail/template', '', 0, 9021, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/harvesting/mail/templateError', 'There was an error on the harvesting: \$\$errorMsg\$\$', 0, 9022, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/harvesting/mail/templateWarning', '', 0, 9023, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/harvesting/mail/subject', '[\$\$harvesterType\$\$] \$\$harvesterName\$\$ finished harvesting', 0, 9024, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/harvesting/mail/enabled', 'false', 2, 9025, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/harvesting/mail/level1', 'false', 2, 9026, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/harvesting/mail/level2', 'false', 2, 9027, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/harvesting/mail/level3', 'false', 2, 9028, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/requestedLanguage/ignorechars', '', 0, 9590, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/requestedLanguage/preferUiLanguage', 'true', 2, 9595, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/csw/transactionUpdateCreateXPath', 'true', 2, 1320, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/downloadservice/leave', 'false', 0, 2210, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/downloadservice/simple', 'false', 0, 2220, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/server/host', 'catalogue.prodige.internal', 0, 210, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/server/protocol', 'https', 0, 230, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/userFeedback/enable', 'false', 2, 1911, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/xlinkResolver/enable', 'true', 2, 2310, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('region/getmap/background', 'osm', 0, 9590, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('region/getmap/width', '500', 0, 9590, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('region/getmap/summaryWidth', '500', 0, 9590, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('region/getmap/mapproj', 'EPSG:3857', 0, 9590, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/proxy/ignorehostlist', NULL, 0, 560, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/inspire/atom', 'disabled', 0, 7230, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/inspire/atomSchedule', '0 0 0/24 ? * *', 0, 7240, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/inspire/atomProtocol', 'INSPIRE-ATOM', 0, 7250, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/metadata/prefergrouplogo', 'true', 2, 9111, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/metadata/allThesaurus', 'false', 2, 9160, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/ui/defaultView', 'default', 0, 10100, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/server/log', 'log4j.xml', 0, 250, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('metadata/workflow/draftWhenInGroup', '', 0, 100002, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/oai/maxrecords', '10', 1, 7040, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('metadata/workflow/allowPublishInvalidMd', 'true', 2, 100003, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('metadata/workflow/automaticUnpublishInvalidMd', 'false', 2, 100004, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('metadata/workflow/forceValidationOnMdSave', 'false', 2, 100005, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/feedback/mailServer/tls', 'false', 2, 644, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/feedback/mailServer/ignoreSslCertificateErrors', 'false', 2, 645, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/xlinkResolver/ignore', 'operatesOn,featureCatalogueCitation,Anchor,source', 0, 2312, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/cors/allowedHosts', '*', 0, 561, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('metadata/resourceIdentifierPrefix', 'https://catalogue.prodige.internal/geonetwork/srv/', 0, 10001, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/hidewithheldelements/enableLogging', 'false', 2, 2320, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/xlinkResolver/localXlinkEnable', 'true', 2, 2311, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/metadatacreate/generateUuid', 'true', 2, 9100, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/csw/enabledWhenIndexing', 'true', 2, 1211, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('metadata/import/restrict', '', 0, 11000, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/xlinkResolver/referencedDeletionAllowed', 'true', 2, 2313, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('metadata/backuparchive/enable', 'false', 2, 12000, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/userSelfRegistration/recaptcha/enable', 'false', 2, 1910, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/userSelfRegistration/recaptcha/publickey', '', 0, 1910, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/userSelfRegistration/recaptcha/secretkey', '', 0, 1910, 'y')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('ui/config', '{\"langDetector\":{\"fromHtmlTag\":false,\"regexp\":\"^/[a-zA-Z0-9_-]+/[a-zA-Z0-9_-]+/([a-z]{3})/\",\"default\":\"eng\"},\"nodeDetector\":{\"regexp\":\"^/[a-zA-Z0-9_-]+/([a-zA-Z0-9_-]+)/[a-z]{3}/\",\"default\":\"srv\"},\"mods\":{\"header\":{\"enabled\":true,\"languages\":{\"eng\":\"en\",\"dut\":\"nl\",\"fre\":\"fr\",\"ger\":\"ge\",\"kor\":\"ko\",\"spa\":\"es\",\"cze\":\"cz\",\"cat\":\"ca\",\"fin\":\"fi\",\"ice\":\"is\", \"rus\": \"ru\", \"chi\": \"zh\"}},\"home\":{\"enabled\":true,\"appUrl\":\"../../srv/{{lang}}/catalog.search#/home\"},\"search\":{\"enabled\":true,\"appUrl\":\"../../srv/{{lang}}/catalog.search#/search\",\"hitsperpageValues\":[10,50,100],\"paginationInfo\":{\"hitsPerPage\":20},\"facetsSummaryType\":\"details\",\"facetConfig\":[],\"facetTabField\":\"\",\"filters\":{},\"sortbyValues\":[{\"sortBy\":\"relevance\",\"sortOrder\":\"\"},{\"sortBy\":\"changeDate\",\"sortOrder\":\"\"},{\"sortBy\":\"title\",\"sortOrder\":\"reverse\"},{\"sortBy\":\"rating\",\"sortOrder\":\"\"},{\"sortBy\":\"popularity\",\"sortOrder\":\"\"},{\"sortBy\":\"denominatorDesc\",\"sortOrder\":\"\"},{\"sortBy\":\"denominatorAsc\",\"sortOrder\":\"reverse\"}],\"sortBy\":\"relevance\",\"resultViewTpls\":[{\"tplUrl\":\"../../catalog/components/search/resultsview/partials/viewtemplates/grid.html\",\"tooltip\":\"Grid\",\"icon\":\"fa-th\"}],\"resultTemplate\":\"../../catalog/components/search/resultsview/partials/viewtemplates/grid.html\",\"formatter\":{\"list\":[{\"label\":\"full\",\"url\":\"../api/records/{{uuid}}/formatters/xsl-view?root=div&view=advanced\"}]},\"grid\":{\"related\":[\"parent\",\"children\",\"services\",\"datasets\"]},\"linkTypes\":{\"links\":[\"LINK\",\"kml\"],\"downloads\":[\"DOWNLOAD\"],\"layers\":[\"OGC\"],\"maps\":[\"ows\"]},\"isFilterTagsDisplayedInSearch\":false},\"map\":{\"enabled\":true,\"appUrl\":\"../../srv/{{lang}}/catalog.search#/map\",\"is3DModeAllowed\":true,\"isSaveMapInCatalogAllowed\":true,\"isExportMapAsImageEnabled\":true,\"bingKey\":\"AnElW2Zqi4fI-9cYx1LHiQfokQ9GrNzcjOh_p_0hkO1yo78ba8zTLARcLBIf8H6D\",\"storage\":\"sessionStorage\",\"map\":\"../../map/config-viewer.xml\",\"listOfServices\":{\"wms\":[],\"wmts\":[]},\"useOSM\":true,\"context\":\"\",\"layer\":{\"url\":\"http://www2.demis.nl/mapserver/wms.asp?\",\"layers\":\"Countries\",\"version\":\"1.1.1\"},\"projection\":\"EPSG:3857\",\"projectionList\":[{\"code\":\"EPSG:4326\",\"label\":\"WGS84(EPSG:4326)\"},{\"code\":\"EPSG:3857\",\"label\":\"Googlemercator(EPSG:3857)\"}],\"disabledTools\":{\"processes\":false,\"addLayers\":false,\"layers\":false,\"filter\":false,\"contexts\":false,\"print\":false,\"mInteraction\":false,\"graticule\":false,\"syncAllLayers\":false,\"drawVector\":false},\"searchMapLayers\":[],\"viewerMapLayers\":[]},\"editor\":{\"enabled\":true,\"appUrl\":\"../../srv/{{lang}}/catalog.edit\",\"isUserRecordsOnly\":false,\"isFilterTagsDisplayed\":false,\"createPageTpl\": \"../../catalog/templates/editor/new-metadata-horizontal.html\"},\"admin\":{\"enabled\":true,\"appUrl\":\"../../srv/{{lang}}/admin.console\"},\"signin\":{\"enabled\":true,\"appUrl\":\"../../srv/{{lang}}/catalog.signin\"},\"signout\":{\"appUrl\":\"../../signout\"}}}', 3, 10000, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/metadata/validation/removeSchemaLocation', 'false', 2, 9170, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/platform/version', '3.4.1', 0, 150, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/platform/subVersion', '0', 0, 160, 'n')");
        $this->addSql("INSERT INTO public.settings (name, value, datatype, \"position\", internal) VALUES ('system/userFeedback/lastNotificationDate', '2019-06-02T04:00:00', 0, 1912, 'y')");

        $this->addSql("INSERT INTO public.sources (uuid, name, islocal) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'y')");

        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'ara')");
        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'cat')");
        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'chi')");
        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'dut')");
        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'eng')");
        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'fin')");
        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'fre')");
        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'ger')");
        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'nor')");
        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'por')");
        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'rus')");
        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'spa')");
        $this->addSql("INSERT INTO public.sourcesdes (iddes, label, langid) VALUES ('7fc45be3-9aba-4198-920c-b8737112d522', 'PRODIGE', 'vie')");

        $this->addSql("INSERT INTO public.spatialindex (fid, id, the_geom) VALUES (1, '-6', '0106000020E61000000100000001030000000100000005000000143BEFFFFFCB15C07B96FC4913AC4940DFE0EDFFFF8D27407B96FC4913AC4940DFE0EDFFFF8D2740EA2162D04B0E4440143BEFFFFFCB15C0EA2162D04B0E4440143BEFFFFFCB15C07B96FC4913AC4940')");
        $this->addSql("INSERT INTO public.spatialindex (fid, id, the_geom) VALUES (2, '-5', '0106000020E61000000100000001030000000100000005000000143BEFFFFFCB15C07B96FC4913AC4940DFE0EDFFFF8D27407B96FC4913AC4940DFE0EDFFFF8D2740EA2162D04B0E4440143BEFFFFFCB15C0EA2162D04B0E4440143BEFFFFFCB15C07B96FC4913AC4940')");
        $this->addSql("INSERT INTO public.spatialindex (fid, id, the_geom) VALUES (3, '-4', '0106000020E61000000100000001030000000100000005000000143BEFFFFFCB15C07B96FC4913AC4940DFE0EDFFFF8D27407B96FC4913AC4940DFE0EDFFFF8D2740EA2162D04B0E4440143BEFFFFFCB15C0EA2162D04B0E4440143BEFFFFFCB15C07B96FC4913AC4940')");
        $this->addSql("INSERT INTO public.spatialindex (fid, id, the_geom) VALUES (4, '-3', '0106000020E610000001000000010300000001000000050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000')");
        $this->addSql("INSERT INTO public.spatialindex (fid, id, the_geom) VALUES (5, '-2', '0106000020E610000001000000010300000001000000050000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000')");
        $this->addSql("INSERT INTO public.spatialindex (fid, id, the_geom) VALUES (6, '1', '0106000020E610000001000000010300000001000000050000003D0AD7A3703D11C00000000000804940CDCCCCCCCCCC20400000000000804940CDCCCCCCCCCC20401F85EB51B89E44403D0AD7A3703D11C01F85EB51B89E44403D0AD7A3703D11C00000000000804940')");
        $this->addSql("INSERT INTO public.spatialindex (fid, id, the_geom) VALUES (7, '2', '0106000020E610000001000000010300000001000000050000003D0AD7A3703D11C00000000000804940CDCCCCCCCCCC20400000000000804940CDCCCCCCCCCC20401F85EB51B89E44403D0AD7A3703D11C01F85EB51B89E44403D0AD7A3703D11C00000000000804940')");
        $this->addSql("INSERT INTO public.spatialindex (fid, id, the_geom) VALUES (8, '3', '0106000020E6100000010000000103000000010000000500000045BB0A293F2917C0562B137EA98B49407250C24CDB1F2340562B137EA98B49407250C24CDB1F234026AAB706B6AE444045BB0A293F2917C026AAB706B6AE444045BB0A293F2917C0562B137EA98B4940')");
        $this->addSql("INSERT INTO public.spatialindex (fid, id, the_geom) VALUES (9, '9', '0106000020E610000001000000010300000001000000050000003D0AD7A3703D11C00000000000804940CDCCCCCCCCCC20400000000000804940CDCCCCCCCCCC20401F85EB51B89E44403D0AD7A3703D11C01F85EB51B89E44403D0AD7A3703D11C00000000000804940')");
        $this->addSql("INSERT INTO public.spatialindex (fid, id, the_geom) VALUES (10, '10', '0106000020E610000001000000010300000001000000050000003D0AD7A3703D11C00000000000804940CDCCCCCCCCCC20400000000000804940CDCCCCCCCCCC20401F85EB51B89E44403D0AD7A3703D11C01F85EB51B89E44403D0AD7A3703D11C00000000000804940')");

        $this->addSql("INSERT INTO public.statusvaluesdes (iddes, langid, label) VALUES (0, 'fre', 'Inconnu')");
        $this->addSql("INSERT INTO public.statusvaluesdes (iddes, langid, label) VALUES (1, 'fre', 'Brouillon')");
        $this->addSql("INSERT INTO public.statusvaluesdes (iddes, langid, label) VALUES (2, 'fre', 'Validé')");
        $this->addSql("INSERT INTO public.statusvaluesdes (iddes, langid, label) VALUES (3, 'fre', 'Retiré')");
        $this->addSql("INSERT INTO public.statusvaluesdes (iddes, langid, label) VALUES (4, 'fre', 'A valider')");
        $this->addSql("INSERT INTO public.statusvaluesdes (iddes, langid, label) VALUES (5, 'fre', 'Rejeté')");

        $this->addSql("INSERT INTO public.useraddress (userid, addressid) VALUES (1, 1)");

        $this->addSql("INSERT INTO public.usergroups (userid, groupid, profile) VALUES (1, 217, 2)");
        $this->addSql("INSERT INTO public.usergroups (userid, groupid, profile) VALUES (40102, 217, 0)");
        $this->addSql("INSERT INTO public.usergroups (userid, groupid, profile) VALUES (40784, 40785, 0)");

        $this->addSql("INSERT INTO public.validation (metadataid, valtype, status, tested, failed, valdate, required) VALUES (10, 'schematron-rules-geonetwork', 1, 1, 0, '2012-04-11T13:53:41', NULL)");
        $this->addSql("INSERT INTO public.validation (metadataid, valtype, status, tested, failed, valdate, required) VALUES (10, 'xsd', 0, 0, 0, '2012-04-11T13:53:41', NULL)");
        $this->addSql("INSERT INTO public.validation (metadataid, valtype, status, tested, failed, valdate, required) VALUES (10, 'schematron-rules-iso', 1, 41, 0, '2012-04-11T13:53:41', NULL)");
        $this->addSql("INSERT INTO public.validation (metadataid, valtype, status, tested, failed, valdate, required) VALUES (2, 'schematron-rules-geonetwork', 1, 1, 0, '2012-04-11T13:54:36', NULL)");
        $this->addSql("INSERT INTO public.validation (metadataid, valtype, status, tested, failed, valdate, required) VALUES (2, 'xsd', 0, 0, 0, '2012-04-11T13:54:36', NULL)");
        $this->addSql("INSERT INTO public.validation (metadataid, valtype, status, tested, failed, valdate, required) VALUES (2, 'schematron-rules-inspire', 0, 20, 7, '2012-04-11T13:54:36', NULL)");
        $this->addSql("INSERT INTO public.validation (metadataid, valtype, status, tested, failed, valdate, required) VALUES (2, 'schematron-rules-iso', 0, 44, 3, '2012-04-11T13:54:36', NULL)");
        $this->addSql("INSERT INTO public.validation (metadataid, valtype, status, tested, failed, valdate, required) VALUES (9, 'schematron-rules-geonetwork', 1, 1, 0, '2012-04-11T13:48:57', true)");
        $this->addSql("INSERT INTO public.validation (metadataid, valtype, status, tested, failed, valdate, required) VALUES (9, 'xsd', 0, 0, 0, '2012-04-11T13:48:57', true)");
        $this->addSql("INSERT INTO public.validation (metadataid, valtype, status, tested, failed, valdate, required) VALUES (9, 'schematron-rules-inspire', 1, 38, 0, '2012-04-11T13:48:57', true)");
        $this->addSql("INSERT INTO public.validation (metadataid, valtype, status, tested, failed, valdate, required) VALUES (9, 'schematron-rules-iso', 1, 49, 0, '2012-04-11T13:48:57', true)");
   }

    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('set search_path to public');

        $this->addSql("delete from bdterr.bdterr_referentiel_carto");
        $this->addSql("delete from bdterr.bdterr_referentiel_recherche");
        $this->addSql("delete from bdterr.bdterr_donnee");
        $this->addSql("delete from bdterr.bdterr_referentiel_intersection");
        $this->addSql("delete from bdterr.bdterr_contenus_tiers");
        $this->addSql("delete from bdterr.bdterr_rapports");
        $this->addSql("delete from bdterr.bdterr_rapports_profils");
        $this->addSql("delete from bdterr.bdterr_lot");
        $this->addSql("delete from bdterr.bdterr_champ_type");
        $this->addSql("delete from bdterr.bdterr_couche_champ");
        $this->addSql("delete from bdterr.bdterr_themes");

        $this->addSql("delete from catalogue.prodige_database_requests");
        $this->addSql("delete from catalogue.prodige_carto_colors");
        $this->addSql("delete from catalogue.prodige_colors");
        $this->addSql("delete from catalogue.prodige_session_user");
        $this->addSql("delete from catalogue.prodige_settings");
        $this->addSql("delete from catalogue.raster_info");
        $this->addSql("delete from catalogue.rawgraph_couche_config");
        $this->addSql("delete from catalogue.rubric_param");
        $this->addSql("delete from catalogue.test_requests");
        $this->addSql("delete from catalogue.competence_accede_carte");
        $this->addSql("delete from catalogue.competence_accede_couche");
        $this->addSql("delete from catalogue.fiche_metadonnees");
        $this->addSql("delete from catalogue.scheduled_command");
        $this->addSql("delete from catalogue.execution_report");
        $this->addSql("delete from catalogue.competence");
        $this->addSql("delete from catalogue.standards_conformite");
        $this->addSql("delete from catalogue.grp_accede_competence");
        $this->addSql("delete from catalogue.standards_fournisseur");
        $this->addSql("delete from catalogue.standards_standard");
        $this->addSql("delete from catalogue.acces_serveur");
        $this->addSql("delete from catalogue.stockage_carte");
        $this->addSql("delete from catalogue.utilisateur_carte");
        $this->addSql("delete from catalogue.prodige_fonts");
        $this->addSql("delete from catalogue.prodige_geosource_colors");
        $this->addSql("delete from catalogue.prodige_param");
        $this->addSql("delete from catalogue.grp_accede_dom");
        $this->addSql("delete from catalogue.grp_accede_ssdom");
        $this->addSql("delete from catalogue.traitement");
        $this->addSql("delete from catalogue.grp_autorise_trt");
        $this->addSql("delete from catalogue.grp_regroupe_usr");
        $this->addSql("delete from catalogue.grp_trt_objet");
        $this->addSql("delete from catalogue.harves_opendata_node");
        $this->addSql("delete from catalogue.harves_opendata_node_params");
        $this->addSql("delete from catalogue.prodige_external_access");
        $this->addSql("delete from catalogue.groupe_profil");
        $this->addSql("delete from catalogue.prodige_help");
        $this->addSql("delete from catalogue.prodige_help_group");
        $this->addSql("delete from catalogue.domaine");
        $this->addSql("delete from catalogue.ssdom_dispose_couche");
        $this->addSql("delete from catalogue.ssdom_dispose_metadata");
        $this->addSql("delete from catalogue.carte_projet");
        $this->addSql("delete from catalogue.sous_domaine");
        $this->addSql("delete from catalogue.ssdom_visualise_carte");
        $this->addSql("delete from catalogue.tache_import_donnees");
        $this->addSql("delete from catalogue.objet_type");
        $this->addSql("delete from catalogue.trt_objet");
        $this->addSql("delete from catalogue.trt_autorise_objet");
        $this->addSql("delete from catalogue.usr_accede_perimetre_edition");
        $this->addSql("delete from catalogue.usr_accede_perimetre");
        $this->addSql("delete from catalogue.perimetre");
        $this->addSql("delete from catalogue.utilisateur");
        $this->addSql("delete from catalogue.usr_alerte_perimetre_edition");
        $this->addSql("delete from catalogue.couche_donnees");
        $this->addSql("delete from catalogue.zonage");

        //$this->addSql("delete from public.spatial_ref_sys");
        //$this->addSql("delete from public.migration_version");
        $this->addSql("delete from public.customelementset");
        $this->addSql("delete from public.files");
        $this->addSql("delete from public.harvesterdata");
        $this->addSql("delete from public.harvesthistory");
        $this->addSql("delete from public.categoriesdes");
        $this->addSql("delete from public.cswservercapabilitiesinfo");
        $this->addSql("delete from public.mapservers");
        $this->addSql("delete from public.metadatafiledownloads");
        $this->addSql("delete from public.metadatafileuploads");
        $this->addSql("delete from public.metadataidentifiertemplate");
        $this->addSql("delete from public.relations");
        $this->addSql("delete from public.requests");
        $this->addSql("delete from public.params");
        $this->addSql("delete from public.settings");
        $this->addSql("delete from public.spatialindex");
        $this->addSql("delete from public.thesaurus");
        $this->addSql("delete from public.statusvaluesdes");
        $this->addSql("delete from public.email");
        $this->addSql("delete from public.sources");
        $this->addSql("delete from public.sourcesdes");
        $this->addSql("delete from public.schematroncriteriagroup");
        $this->addSql("delete from public.schematroncriteria");
        $this->addSql("delete from public.inspireatomfeed");
        $this->addSql("delete from public.inspireatomfeed_entrylist");
        $this->addSql("delete from public.metadatanotifiers");
        $this->addSql("delete from public.metadatanotifications");
        $this->addSql("delete from public.usersavedselections");
        $this->addSql("delete from public.selections");
        $this->addSql("delete from public.selectionsdes");
        $this->addSql("delete from public.group_category");
        $this->addSql("delete from public.schematron");
        $this->addSql("delete from public.schematrondes");
        $this->addSql("delete from public.groupsdes");
        $this->addSql("delete from public.harvestersettings");
        $this->addSql("delete from public.isolanguages");
        $this->addSql("delete from public.isolanguagesdes");
        $this->addSql("delete from public.categories");
        $this->addSql("delete from public.metadatacateg");
        $this->addSql("delete from public.metadatarating");
        $this->addSql("delete from public.statusvalues");
        $this->addSql("delete from public.metadatastatus");
        $this->addSql("delete from public.operationallowed");
        $this->addSql("delete from public.operations");
        $this->addSql("delete from public.operationsdes");
        $this->addSql("delete from public.regions");
        $this->addSql("delete from public.languages");
        $this->addSql("delete from public.regionsdes");
        $this->addSql("delete from public.services");
        $this->addSql("delete from public.serviceparameters");
        $this->addSql("delete from public.address");
        $this->addSql("delete from public.useraddress");
        $this->addSql("delete from public.groups");
        $this->addSql("delete from public.users");
        $this->addSql("delete from public.usergroups");
        $this->addSql("delete from public.metadata");
        $this->addSql("delete from public.validation");
    }
}

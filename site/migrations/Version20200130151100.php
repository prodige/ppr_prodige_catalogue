<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200130151100 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version tampon : Patch 4.2.1';
    }

    public function up(Schema $schema): void
    {

        #  Name: utilisateur_contexte; Type: TABLE; Schema: catalogue;


        $this->addSql("CREATE SEQUENCE catalogue.seq_utilisateur_contexte
            START WITH 1
            INCREMENT BY 1
            NO MINVALUE
            NO MAXVALUE
            CACHE 1");

        $this->addSql("CREATE TABLE catalogue.utilisateur_contexte (
            id bigint DEFAULT nextval('catalogue.seq_utilisateur_contexte'::regclass) NOT NULL,
            fk_utilisateur integer NOT NULL,
            context text
        )");

        $this->addSql("ALTER TABLE ONLY catalogue.utilisateur_contexte
            ADD CONSTRAINT utilisateur_contexte_pkey PRIMARY KEY (id)");

        $this->addSql("CREATE INDEX idx_utilisateur_contexte_fk_utilisateur ON catalogue.utilisateur_contexte USING btree (fk_utilisateur)");

        $this->addSql("ALTER TABLE ONLY catalogue.utilisateur_contexte
            ADD CONSTRAINT fk_utilisateur_contexte_fk_utilisateur FOREIGN KEY (fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE");


        #  Name: utilisateur_zone_favorite; Type: TABLE; Schema: catalogue;


        $this->addSql("CREATE SEQUENCE catalogue.seq_utilisateur_zone_favorite
            START WITH 1
            INCREMENT BY 1
            NO MINVALUE
            NO MAXVALUE
            CACHE 1");

        $this->addSql("CREATE TABLE catalogue.utilisateur_zone_favorite (
            id bigint DEFAULT nextval('catalogue.seq_utilisateur_zone_favorite'::regclass) NOT NULL,
            fk_utilisateur integer NOT NULL,
            zone_favorite text
        )");

        $this->addSql("ALTER TABLE ONLY catalogue.utilisateur_zone_favorite
            ADD CONSTRAINT utilisateur_zone_favorite_pkey PRIMARY KEY (id)");

        $this->addSql("CREATE INDEX idx_utilisateur_zone_favorite_fk_utilisateur ON catalogue.utilisateur_zone_favorite USING btree (fk_utilisateur)");

        $this->addSql("ALTER TABLE ONLY catalogue.utilisateur_zone_favorite
            ADD CONSTRAINT fk_utilisateur_zone_favorite_fk_utilisateur FOREIGN KEY (fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE");


        #  Ajout des structures
        #  

        $this->addSql("CREATE SEQUENCE catalogue.seq_structure
            START WITH 1
            INCREMENT BY 1
            NO MINVALUE
            NO MAXVALUE
            CACHE 1");


        $this->addSql("CREATE TABLE catalogue.structure (
            pk_structure integer DEFAULT nextval('catalogue.seq_structure'::regclass) NOT NULL,
            ts timestamp without time zone DEFAULT now(),
            structure_nom character varying(255) ,
            structure_sigle character varying(40) ,
            structure_siren character varying(9)
        )");

        $this->addSql("ALTER TABLE ONLY catalogue.structure
            ADD CONSTRAINT pk_pk_structure PRIMARY KEY (pk_structure)");



        #  Name: utilisateur_structure; Type: TABLE; Schema: catalogue;


        $this->addSql("CREATE SEQUENCE catalogue.seq_utilisateur_structure
            START WITH 1
            INCREMENT BY 1
            NO MINVALUE
            NO MAXVALUE
            CACHE 1");

        $this->addSql("CREATE TABLE catalogue.utilisateur_structure (
            id integer DEFAULT nextval('catalogue.seq_utilisateur_structure'::regclass) NOT NULL,
            fk_utilisateur integer NOT NULL,
            fk_structure integer NOT NULL
        )");

        $this->addSql("ALTER TABLE ONLY catalogue.utilisateur_structure
            ADD CONSTRAINT utilisateur_structure_pkey PRIMARY KEY (id)");

        $this->addSql("CREATE INDEX idx_utilisateur_structure_fk_utilisateur ON catalogue.utilisateur_structure USING btree (fk_utilisateur)");

        $this->addSql("ALTER TABLE ONLY catalogue.utilisateur_structure
            ADD CONSTRAINT fk_utilisateur_structure_fk_utilisateur FOREIGN KEY (fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE");

        $this->addSql("CREATE INDEX idx_utilisateur_structure_fk_structure ON catalogue.utilisateur_structure USING btree (fk_structure)");

        $this->addSql("ALTER TABLE ONLY catalogue.utilisateur_structure
            ADD CONSTRAINT fk_utilisateur_structure_fk_structure FOREIGN KEY (fk_structure) REFERENCES catalogue.structure(pk_structure) MATCH FULL ON DELETE CASCADE");

        #  restriction attributaire des droits
        $this->addSql("alter table catalogue.couche_donnees add column couchd_restriction_attributaire integer default 0");
        $this->addSql("alter table catalogue.couche_donnees add column couchd_restriction_attributaire_propriete varchar(255)");

        $this->addSql("CREATE SEQUENCE catalogue.seq_grp_restriction_attributaire
            START WITH 1
            INCREMENT BY 1
            NO MINVALUE
            NO MAXVALUE
            CACHE 1");


        $this->addSql("CREATE TABLE catalogue.grp_restriction_attributaire (
            pk_grp_restriction_attributaire integer DEFAULT nextval('catalogue.seq_grp_restriction_attributaire'::regclass) NOT NULL,
            grprat_fk_couche_donnees integer,
            grprat_fk_groupe_profil integer,
            grprat_champ varchar(255)
            
        )");

        $this->addSql("ALTER TABLE ONLY catalogue.grp_restriction_attributaire
            ADD CONSTRAINT pk_grp_restriction_attributaire PRIMARY KEY (pk_grp_restriction_attributaire)");


        $this->addSql("ALTER TABLE ONLY catalogue.grp_restriction_attributaire
            ADD CONSTRAINT grprat_fk_groupe_profil_fkey FOREIGN KEY (grprat_fk_groupe_profil) REFERENCES catalogue.groupe_profil(pk_groupe_profil) MATCH FULL ON DELETE CASCADE");


        $this->addSql("ALTER TABLE ONLY catalogue.grp_restriction_attributaire
            ADD CONSTRAINT grprat_fk_couche_donnees_fkey FOREIGN KEY (grprat_fk_couche_donnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees) MATCH FULL ON DELETE CASCADE");


        $this->addSql("insert into catalogue.prodige_settings (prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type) 
            values ('PRO_MODULE_TABLE_EDITION', 'Module édition de table activé', 'off', 'Module édition de table activé', 'checkboxfield')");



        // si la base est déjà dans cette version, il faut exécuter ces requêtes à la base CATALOGUE puis jouer la procédure de mise à jour

        /*
        CREATE TABLE public.migration_version (version character varying(14) NOT NULL, executed_at timestamp(0) without time zone NOT NULL);
        ALTER TABLE public.migration_version OWNER TO user_prodige;
        COMMENT ON COLUMN public.migration_version.executed_at IS '(DC2Type:datetime_immutable)';
        INSERT INTO public.doctrine_migration VALUES ('20191129120000', '2019-12-03 14:24:44');
        INSERT INTO public.migration_version VALUES ('20191129120001', '2019-12-03 14:24:44');
        INSERT INTO public.migration_version VALUES ('20191129120002', '2019-12-03 14:24:48');
        INSERT INTO public.migration_version VALUES ('20191129120003', '2019-12-03 14:24:48');
        INSERT INTO public.migration_version VALUES ('20191129120004', '2019-12-03 14:24:48');
        INSERT INTO public.migration_version VALUES ('20191129120005', '2019-12-03 14:24:48');
        INSERT INTO public.migration_version VALUES ('20200130151100', '2020-01-30 15:24:00');
        ALTER TABLE ONLY public.migration_version ADD CONSTRAINT migration_version_pkey PRIMARY KEY (version);
        */
    }

    public function down(Schema $schema): void
    {
        // mettre en commentaire cette ligne si vous souhaitez réellement réinitialiser votre base.
        $this->abortIf(true, 'Si vous continuez, vous risquez de perdre toutes vos données. Traitement interrompu par sécurité.');
    }
}

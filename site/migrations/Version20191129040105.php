<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191129040105 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version tampon : Patch 4.1.1';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("update public.settings set value= replace(value, '\"map\":{\"enabled\":true', '\"map\":{\"enabled\":false') where name='ui/config'");

        // si la base est déjà dans cette version, il faut exécuter ces requêtes à la base CATALOGUE puis jouer la procédure de mise à jour

        /*
        CREATE TABLE public.migration_version (version character varying(14) NOT NULL, executed_at timestamp(0) without time zone NOT NULL);
        ALTER TABLE public.migration_version OWNER TO user_prodige;
        COMMENT ON COLUMN public.migration_version.executed_at IS '(DC2Type:datetime_immutable)';
        INSERT INTO public.migration_version VALUES ('20191129120000', '2019-12-03 14:24:44');
        INSERT INTO public.migration_version VALUES ('20191129120001', '2019-12-03 14:24:44');
        INSERT INTO public.migration_version VALUES ('20191129120002', '2019-12-03 14:24:48');
        INSERT INTO public.migration_version VALUES ('20191129120003', '2019-12-03 14:24:48');
        INSERT INTO public.migration_version VALUES ('20191129120004', '2019-12-03 14:24:48');
        INSERT INTO public.migration_version VALUES ('20191129120005', '2019-12-03 14:24:48');
        ALTER TABLE ONLY public.migration_version ADD CONSTRAINT migration_version_pkey PRIMARY KEY (version);
        */
    }

    public function down(Schema $schema): void
    {
        // mettre en commentaire cette ligne si vous souhaitez réellement réinitialiser votre base.
        $this->abortIf(true, 'Si vous continuez, vous risquez de perdre toutes vos données. Traitement interrompu par sécurité.');
    }
}

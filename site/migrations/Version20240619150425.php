<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240619150425 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE catalogue.couche_donnees ADD COLUMN couchd_api_restricted_ip TEXT");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE catalogue.couche_donnees DROP COLUMN couchd_api_restricted_ip");
    }
}

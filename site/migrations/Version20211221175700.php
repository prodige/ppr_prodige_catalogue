<?php

namespace DoctrineMigrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20211221175700 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE catalogue.structure add column structure_siret character varying(14)");
        $this->addSql("ALTER TABLE catalogue.grp_restriction_attributaire add column grprat_type character varying(255)");
        $this->addSql("update catalogue.grp_restriction_attributaire set grprat_type = (select couchd_restriction_attributaire_propriete from catalogue.couche_donnees where pk_couche_donnees = grprat_fk_couche_donnees)");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE catalogue.couche_donnees drop column structure_siret");
        $this->addSql("ALTER TABLE catalogue.grp_restriction_attributaire drop column grprat_type");

    }
}

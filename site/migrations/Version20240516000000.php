<?php


declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\Process\Process;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240516000000 extends AbstractMigration
{
   public function getDescription(): string
    {
        return 'clean geonetwork custom CSW xpath';
    }

    public function up(Schema $schema): void
    {   
        $this->addSql("delete from public.customelementset;");
    }


    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}

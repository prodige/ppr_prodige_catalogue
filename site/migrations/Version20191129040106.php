<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191129040106 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version tampon : Patch 4.1.4';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("update public.settings set value= replace(value, '\"map\":{\"enabled\":true', '\"map\":{\"enabled\":false') where name='ui/config'");


        $this->addSql("update metadata set data =  replace(data, '&amp;amp;amp;', '&amp;amp;')  where data like '%&amp;amp;amp;%' and isharvested='n';");
        $this->addSql("update metadata set data = replace(data, '/geonetwork//srv/fre/xml.keyword.get?', '/geonetwork/srv/fre/xml.keyword.get?') where  isharvested='n';");
        $this->addSql("update metadata set data = replace(data, '<gfc:featureCatalogue xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#\" />', '') where  isharvested='n';");
        $this->addSql("update metadata set data = replace(data, '<gfc:featureCatalogue xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#\"/>', '') where  isharvested='n';");
        // suppression des xlink locaux, non fonctionnels
        $this->addSql("update public.settings set value='false' where name='system/xlinkResolver/localXlinkEnable';");
        // update metadata set data = replace(data, 'local://fre/xml.keyword.get?', 'https://www-test.datara.gouv.fr/geonetwork/srv/fre/xml.keyword.get?') where  isharvested='n';
        // synchroniser les tables catalogue.utilisateur et public.users
        $this->addSql("update users set surname = utilisateur.usr_prenom , name = utilisateur.usr_nom from catalogue.utilisateur where username = utilisateur.usr_id");

        // si la base est déjà dans cette version, il faut exécuter ces requêtes à la base CATALOGUE puis jouer la procédure de mise à jour

        /*
        CREATE TABLE public.migration_version (version character varying(14) NOT NULL, executed_at timestamp(0) without time zone NOT NULL);
        ALTER TABLE public.migration_version OWNER TO user_prodige;
        COMMENT ON COLUMN public.migration_version.executed_at IS '(DC2Type:datetime_immutable)';
        INSERT INTO public.doctrine_migration VALUES ('20191129120000', '2019-12-03 14:24:44');
        INSERT INTO public.migration_version VALUES ('20191129120001', '2019-12-03 14:24:44');
        INSERT INTO public.migration_version VALUES ('20191129120002', '2019-12-03 14:24:48');
        INSERT INTO public.migration_version VALUES ('20191129120003', '2019-12-03 14:24:48');
        INSERT INTO public.migration_version VALUES ('20191129120004', '2019-12-03 14:24:48');
        INSERT INTO public.migration_version VALUES ('20191129120005', '2019-12-03 14:24:48');
        INSERT INTO public.migration_version VALUES ('20191129120006', '2019-12-03 14:24:48');
        ALTER TABLE ONLY public.migration_version ADD CONSTRAINT migration_version_pkey PRIMARY KEY (version);
        */
    }

    public function down(Schema $schema): void
    {
        // mettre en commentaire cette ligne si vous souhaitez réellement réinitialiser votre base.
        $this->abortIf(true, 'Si vous continuez, vous risquez de perdre toutes vos données. Traitement interrompu par sécurité.');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191129040103 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout des contraintes de la base CATALOGUE';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('set search_path to public');

        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_donnee ADD CONSTRAINT fk_30055129141f7bf0 FOREIGN KEY (donnee_lot_id) REFERENCES bdterr.bdterr_lot(lot_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_lot ADD CONSTRAINT fk_4674d3dfa9ee0a03 FOREIGN KEY (lot_referentiel_id) REFERENCES bdterr.bdterr_referentiel_intersection(referentiel_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_lot ADD CONSTRAINT fk_4674d3dffbc885b3 FOREIGN KEY (lot_theme_id) REFERENCES bdterr.bdterr_themes(theme_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_contenus_tiers ADD CONSTRAINT fk_4819e6af8ae06949 FOREIGN KEY (contenu_type) REFERENCES bdterr.bdterr_champ_type(champtype_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_contenus_tiers ADD CONSTRAINT fk_4819e6afa8cba5f7 FOREIGN KEY (lot_id) REFERENCES bdterr.bdterr_lot(lot_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_rapports_profils ADD CONSTRAINT fk_51faf077d1eae64c FOREIGN KEY (bterr_rapport_id) REFERENCES bdterr.bdterr_rapports(rapport_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_couche_champ ADD CONSTRAINT fk_52a0ca003dc30acd FOREIGN KEY (champ_lot_id) REFERENCES bdterr.bdterr_lot(lot_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_couche_champ ADD CONSTRAINT fk_52a0ca00d43aa457 FOREIGN KEY (champ_type) REFERENCES bdterr.bdterr_champ_type(champtype_id)");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_themes ADD CONSTRAINT fk_a06003abedb65433 FOREIGN KEY (theme_parent) REFERENCES bdterr.bdterr_themes(theme_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.carte_projet ADD CONSTRAINT carte_projet_cartp_fk_fiche_metadonnees_fkey FOREIGN KEY (cartp_fk_fiche_metadonnees) REFERENCES catalogue.fiche_metadonnees(pk_fiche_metadonnees)");
        $this->addSql("ALTER TABLE ONLY catalogue.carte_projet ADD CONSTRAINT carte_projet_cartp_fk_stockage_carte_fkey FOREIGN KEY (cartp_fk_stockage_carte) REFERENCES catalogue.stockage_carte(pk_stockage_carte) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_param ADD CONSTRAINT carto_colors_fkey FOREIGN KEY (prodige_carto_color_id) REFERENCES catalogue.prodige_carto_colors(prodige_carto_color_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.carte_projet ADD CONSTRAINT cartp_fk_utilisateur FOREIGN KEY (cartp_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_param ADD CONSTRAINT colors_fkey FOREIGN KEY (prodige_color_id) REFERENCES catalogue.prodige_colors(prodige_color_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.competence_accede_carte ADD CONSTRAINT competencecarte_fk_carte_projet_fkey FOREIGN KEY (competencecarte_fk_carte_projet) REFERENCES catalogue.carte_projet(pk_carte_projet) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.competence_accede_carte ADD CONSTRAINT competencecarte_fk_competence_id_fkey FOREIGN KEY (competencecarte_fk_competence_id) REFERENCES catalogue.competence(pk_competence_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.competence_accede_couche ADD CONSTRAINT competencecouche_fk_competence_id_fkey FOREIGN KEY (competencecouche_fk_competence_id) REFERENCES catalogue.competence(pk_competence_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.competence_accede_couche ADD CONSTRAINT competencecouche_fk_couche_donnees_fkey FOREIGN KEY (competencecouche_fk_couche_donnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.couche_donnees ADD CONSTRAINT couche_donnees_couchd_fk_acces_server_fkey FOREIGN KEY (couchd_fk_acces_server) REFERENCES catalogue.acces_serveur(pk_acces_serveur) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.couche_donnees ADD CONSTRAINT couche_donnees_fk_zonage_fkey FOREIGN KEY (couchd_zonageid_fk) REFERENCES catalogue.zonage(pk_zonage_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.domaine ADD CONSTRAINT dom_rubric_fk FOREIGN KEY (dom_rubric) REFERENCES catalogue.rubric_param(rubric_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.fiche_metadonnees ADD CONSTRAINT fiche_metadonnees_fmeta_fk_couche_donnees_fkey FOREIGN KEY (fmeta_fk_couche_donnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees)");
        $this->addSql("ALTER TABLE ONLY catalogue.execution_report ADD CONSTRAINT fk_8e1b7bbcad93e023 FOREIGN KEY (scheduled_command_id) REFERENCES catalogue.scheduled_command(id)");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_accede_competence ADD CONSTRAINT fk_competence_id_fkey FOREIGN KEY (fk_competence_id) REFERENCES catalogue.competence(pk_competence_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.standards_conformite ADD CONSTRAINT fk_conformite_metadata FOREIGN KEY (metadata_id) REFERENCES public.metadata(id)");
        $this->addSql("ALTER TABLE ONLY catalogue.standards_conformite ADD CONSTRAINT fk_conformite_standard FOREIGN KEY (standard_id) REFERENCES catalogue.standards_standard(standard_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_accede_competence ADD CONSTRAINT fk_grp_id_fkey FOREIGN KEY (fk_grp_id) REFERENCES catalogue.groupe_profil(pk_groupe_profil) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.perimetre ADD CONSTRAINT fk_perimetre_zonage_id_fkey FOREIGN KEY (perimetre_zonage_id) REFERENCES catalogue.zonage(pk_zonage_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.standards_standard ADD CONSTRAINT fk_standard_fournisseur FOREIGN KEY (fournisseur_id) REFERENCES catalogue.standards_fournisseur(fournisseur_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.standards_standard ADD CONSTRAINT fk_standard_metadata FOREIGN KEY (fmeta_id) REFERENCES public.metadata(id)");
        $this->addSql("ALTER TABLE ONLY catalogue.stockage_carte ADD CONSTRAINT fk_stk_server_acces_server FOREIGN KEY (stk_server) REFERENCES catalogue.acces_serveur(pk_acces_serveur) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.utilisateur_carte ADD CONSTRAINT fk_utilisateur_carte_fk_stockage_carte FOREIGN KEY (fk_stockage_carte) REFERENCES catalogue.stockage_carte(pk_stockage_carte)");
        $this->addSql("ALTER TABLE ONLY catalogue.utilisateur_carte ADD CONSTRAINT fk_utilisateur_carte_fk_utilisateur FOREIGN KEY (fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur)");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_param ADD CONSTRAINT fonts_fkey FOREIGN KEY (font_id) REFERENCES catalogue.prodige_fonts(font_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_param ADD CONSTRAINT geosource_colors_fkey FOREIGN KEY (prodige_geosource_color_id) REFERENCES catalogue.prodige_geosource_colors(prodige_geosource_color_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_accede_dom ADD CONSTRAINT grp_accede_dom_grpdom_fk_domaine_fkey FOREIGN KEY (grpdom_fk_domaine) REFERENCES catalogue.domaine(pk_domaine) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_accede_dom ADD CONSTRAINT grp_accede_dom_grpdom_fk_groupe_profil_fkey FOREIGN KEY (grpdom_fk_groupe_profil) REFERENCES catalogue.groupe_profil(pk_groupe_profil) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_accede_ssdom ADD CONSTRAINT grp_accede_ssdom_grpss_fk_groupe_profil_fkey FOREIGN KEY (grpss_fk_groupe_profil) REFERENCES catalogue.groupe_profil(pk_groupe_profil) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_accede_ssdom ADD CONSTRAINT grp_accede_ssdom_grpss_fk_sous_domaine_fkey FOREIGN KEY (grpss_fk_sous_domaine) REFERENCES catalogue.sous_domaine(pk_sous_domaine) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_autorise_trt ADD CONSTRAINT grp_autorise_trt_grptrt_fk_groupe_profil_fkey FOREIGN KEY (grptrt_fk_groupe_profil) REFERENCES catalogue.groupe_profil(pk_groupe_profil) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_autorise_trt ADD CONSTRAINT grp_autorise_trt_grptrt_fk_traitement_fkey FOREIGN KEY (grptrt_fk_traitement) REFERENCES catalogue.traitement(pk_traitement) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_regroupe_usr ADD CONSTRAINT grp_regroupe_usr_grpusr_fk_groupe_profil_fkey FOREIGN KEY (grpusr_fk_groupe_profil) REFERENCES catalogue.groupe_profil(pk_groupe_profil) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_regroupe_usr ADD CONSTRAINT grp_regroupe_usr_grpusr_fk_utilisateur_fkey FOREIGN KEY (grpusr_fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_trt_objet ADD CONSTRAINT grptrtobj_fk_grp_id_fkey FOREIGN KEY (grptrtobj_fk_grp_id) REFERENCES catalogue.groupe_profil(pk_groupe_profil) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_trt_objet ADD CONSTRAINT grptrtobj_fk_obj_type_id_fkey FOREIGN KEY (grptrtobj_fk_obj_type_id) REFERENCES catalogue.objet_type(pk_objet_type_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_trt_objet ADD CONSTRAINT grptrtobj_fk_trt_id_fkey FOREIGN KEY (grptrtobj_fk_trt_id) REFERENCES catalogue.trt_objet(pk_trt_objet)");
        $this->addSql("ALTER TABLE ONLY catalogue.harves_opendata_node_params ADD CONSTRAINT \"harves_opendata_node_params_FK_node_id_fkey\" FOREIGN KEY (fk_node_id) REFERENCES catalogue.harves_opendata_node(pk_node_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_external_access ADD CONSTRAINT prodige_extaccess_fk_utilisateur_fkey FOREIGN KEY (prodige_extaccess_fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_help_group ADD CONSTRAINT prodige_help_group_hlpgrp_fk_groupe_profil_fkey FOREIGN KEY (hlpgrp_fk_groupe_profil) REFERENCES catalogue.groupe_profil(pk_groupe_profil) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_help_group ADD CONSTRAINT prodige_help_group_hlpgrp_fk_help_id_fkey FOREIGN KEY (hlpgrp_fk_help_id) REFERENCES catalogue.prodige_help(pk_prodige_help_id) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.sous_domaine ADD CONSTRAINT sous_domaine_ssdom_fk_domaine_fkey FOREIGN KEY (ssdom_fk_domaine) REFERENCES catalogue.domaine(pk_domaine) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.ssdom_dispose_couche ADD CONSTRAINT ssdom_dispose_couche_ssdcouch_fk_couche_donnees_fkey FOREIGN KEY (ssdcouch_fk_couche_donnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.ssdom_dispose_couche ADD CONSTRAINT ssdom_dispose_couche_ssdcouch_fk_sous_domaine_fkey FOREIGN KEY (ssdcouch_fk_sous_domaine) REFERENCES catalogue.sous_domaine(pk_sous_domaine) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.ssdom_dispose_metadata ADD CONSTRAINT ssdom_dispose_metadata_ssdcouch_fk_sous_domaine_fkey FOREIGN KEY (ssdcouch_fk_sous_domaine) REFERENCES catalogue.sous_domaine(pk_sous_domaine) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.ssdom_visualise_carte ADD CONSTRAINT ssdom_visualise_carte_ssdcart_fk_carte_projet_fkey FOREIGN KEY (ssdcart_fk_carte_projet) REFERENCES catalogue.carte_projet(pk_carte_projet) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.ssdom_visualise_carte ADD CONSTRAINT ssdom_visualise_carte_ssdcart_fk_sous_domaine_fkey FOREIGN KEY (ssdcart_fk_sous_domaine) REFERENCES catalogue.sous_domaine(pk_sous_domaine) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.tache_import_donnees ADD CONSTRAINT tacheimportdonnees_fk_couchedonnees FOREIGN KEY (pk_couche_donnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees) ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.trt_autorise_objet ADD CONSTRAINT trtautoobjet_fk_obj_type_id_fkey FOREIGN KEY (trtautoobjet_fk_obj_type_id) REFERENCES catalogue.objet_type(pk_objet_type_id)");
        $this->addSql("ALTER TABLE ONLY catalogue.trt_autorise_objet ADD CONSTRAINT trtautoobjet_fk_trt_id_fkey FOREIGN KEY (trtautoobjet_fk_trt_id) REFERENCES catalogue.trt_objet(pk_trt_objet)");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_accede_perimetre_edition ADD CONSTRAINT usr_accede_perim_usrperim_edit_fk_perimetre_fkey FOREIGN KEY (usrperim_fk_perimetre) REFERENCES catalogue.perimetre(pk_perimetre_id) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_accede_perimetre ADD CONSTRAINT usr_accede_perim_usrperim_edit_fk_utilisateur_fkey FOREIGN KEY (usrperim_fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_accede_perimetre ADD CONSTRAINT usr_accede_perim_usrperim_fk_perimetre_fkey FOREIGN KEY (usrperim_fk_perimetre) REFERENCES catalogue.perimetre(pk_perimetre_id) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_accede_perimetre ADD CONSTRAINT usr_accede_perim_usrperim_fk_utilisateur_fkey FOREIGN KEY (usrperim_fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_alerte_perimetre_edition ADD CONSTRAINT usr_alerte_perim_edit_fk_perimetre_fkey FOREIGN KEY (usralert_fk_perimetre) REFERENCES catalogue.perimetre(pk_perimetre_id) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_alerte_perimetre_edition ADD CONSTRAINT usr_alerte_perim_edit_usrperim_fk_couchedonnees_fkey FOREIGN KEY (usralert_fk_couchedonnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_alerte_perimetre_edition ADD CONSTRAINT usr_alerte_perim_edit_usrperim_fk_utilisateur_fkey FOREIGN KEY (usralert_fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE");
        $this->addSql("ALTER TABLE ONLY catalogue.zonage ADD CONSTRAINT zonaget_fk_couche_donnees_fkey FOREIGN KEY (zonage_fk_couche_donnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees)");
        $this->addSql("ALTER TABLE ONLY public.categoriesdes ADD CONSTRAINT categoriesdes_iddes_fkey FOREIGN KEY (iddes) REFERENCES public.categories(id)");
        $this->addSql("ALTER TABLE ONLY public.categoriesdes ADD CONSTRAINT categoriesdes_langid_fkey FOREIGN KEY (langid) REFERENCES public.languages(id)");
        $this->addSql("ALTER TABLE ONLY public.cswservercapabilitiesinfo ADD CONSTRAINT cswservercapabilitiesinfo_langid_fkey FOREIGN KEY (langid) REFERENCES public.languages(id)");
        $this->addSql("ALTER TABLE ONLY public.email ADD CONSTRAINT email_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id)");
        $this->addSql("ALTER TABLE ONLY public.statusvaluesdes ADD CONSTRAINT fk_2vkxyjsd2d3tdwn38p5yjhb71 FOREIGN KEY (iddes) REFERENCES public.statusvalues(id)");
        $this->addSql("ALTER TABLE ONLY public.params ADD CONSTRAINT fk_6d52bqoq3c2eitpdq5r7y872g FOREIGN KEY (requestid) REFERENCES public.requests(id)");
        $this->addSql("ALTER TABLE ONLY public.schematroncriteriagroup ADD CONSTRAINT fk_atfj71dq82he6n77lqofjxui6 FOREIGN KEY (schematronid) REFERENCES public.schematron(id)");
        $this->addSql("ALTER TABLE ONLY public.groups ADD CONSTRAINT fk_balio8qkvhnitbdw241e4ryb8 FOREIGN KEY (defaultcategory_id) REFERENCES public.categories(id)");
        $this->addSql("ALTER TABLE ONLY public.sourcesdes ADD CONSTRAINT fk_c3jxktm4qwai73lddsm5fiecb FOREIGN KEY (iddes) REFERENCES public.sources(uuid)");
        $this->addSql("ALTER TABLE ONLY public.schematroncriteria ADD CONSTRAINT fk_dh2vjs226vjp2anrvj3nuvt8x FOREIGN KEY (group_name, group_schematronid) REFERENCES public.schematroncriteriagroup(name, schematronid)");
        $this->addSql("ALTER TABLE ONLY public.inspireatomfeed_entrylist ADD CONSTRAINT fk_eyt177hveh5vlxyxq6wpl3vqi FOREIGN KEY (inspireatomfeed_id) REFERENCES public.inspireatomfeed(id)");
        $this->addSql("ALTER TABLE ONLY public.usersavedselections ADD CONSTRAINT fk_f558oonrxskvsnoc835m73sru FOREIGN KEY (selectionid) REFERENCES public.selections(id)");
        $this->addSql("ALTER TABLE ONLY public.group_category ADD CONSTRAINT fk_j8nj5ssnar3byh882nuf38tqw FOREIGN KEY (category_id) REFERENCES public.categories(id)");
        $this->addSql("ALTER TABLE ONLY public.metadatanotifications ADD CONSTRAINT fk_jbkvo3w3g4twk2bo1b8jn0sw8 FOREIGN KEY (notifierid) REFERENCES public.metadatanotifiers(id)");
        $this->addSql("ALTER TABLE ONLY public.usersavedselections ADD CONSTRAINT fk_m7d8cfojvibxag0wtg6pqe3d FOREIGN KEY (userid) REFERENCES public.users(id)");
        $this->addSql("ALTER TABLE ONLY public.selectionsdes ADD CONSTRAINT fk_r0y9hytqn3nodmwn86hn2vsgf FOREIGN KEY (iddes) REFERENCES public.selections(id)");
        $this->addSql("ALTER TABLE ONLY public.group_category ADD CONSTRAINT fk_r1y7atocbww201qaj87h9j62e FOREIGN KEY (group_id) REFERENCES public.groups(id)");
        $this->addSql("ALTER TABLE ONLY public.schematrondes ADD CONSTRAINT fk_sh1xwulyb1jeoc6puqpiuc5d2 FOREIGN KEY (iddes) REFERENCES public.schematron(id)");
        $this->addSql("ALTER TABLE ONLY public.groups ADD CONSTRAINT groups_referrer_fkey FOREIGN KEY (referrer) REFERENCES public.users(id)");
        $this->addSql("ALTER TABLE ONLY public.groupsdes ADD CONSTRAINT groupsdes_iddes_fkey FOREIGN KEY (iddes) REFERENCES public.groups(id)");
        $this->addSql("ALTER TABLE ONLY public.groupsdes ADD CONSTRAINT groupsdes_langid_fkey FOREIGN KEY (langid) REFERENCES public.languages(id)");
        $this->addSql("ALTER TABLE ONLY public.harvestersettings ADD CONSTRAINT harvestersettings_parentid_fkey FOREIGN KEY (parentid) REFERENCES public.harvestersettings(id)");
        $this->addSql("ALTER TABLE ONLY public.isolanguagesdes ADD CONSTRAINT isolanguagesdes_iddes_fkey FOREIGN KEY (iddes) REFERENCES public.isolanguages(id)");
        $this->addSql("ALTER TABLE ONLY public.isolanguagesdes ADD CONSTRAINT isolanguagesdes_langid_fkey FOREIGN KEY (langid) REFERENCES public.languages(id)");
        $this->addSql("ALTER TABLE ONLY public.metadata ADD CONSTRAINT metadata_groupowner_fkey FOREIGN KEY (groupowner) REFERENCES public.groups(id)");
        $this->addSql("ALTER TABLE ONLY public.metadata ADD CONSTRAINT metadata_owner_fkey FOREIGN KEY (owner) REFERENCES public.users(id)");
        $this->addSql("ALTER TABLE ONLY public.metadatacateg ADD CONSTRAINT metadatacateg_categoryid_fkey FOREIGN KEY (categoryid) REFERENCES public.categories(id)");
        $this->addSql("ALTER TABLE ONLY public.metadatacateg ADD CONSTRAINT metadatacateg_metadataid_fkey FOREIGN KEY (metadataid) REFERENCES public.metadata(id)");
        $this->addSql("ALTER TABLE ONLY public.metadatarating ADD CONSTRAINT metadatarating_metadataid_fkey FOREIGN KEY (metadataid) REFERENCES public.metadata(id)");
        $this->addSql("ALTER TABLE ONLY public.metadatastatus ADD CONSTRAINT metadatastatus_metadataid_fkey FOREIGN KEY (metadataid) REFERENCES public.metadata(id)");
        $this->addSql("ALTER TABLE ONLY public.metadatastatus ADD CONSTRAINT metadatastatus_statusid_fkey FOREIGN KEY (statusid) REFERENCES public.statusvalues(id)");
        $this->addSql("ALTER TABLE ONLY public.metadatastatus ADD CONSTRAINT metadatastatus_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(id)");
        $this->addSql("ALTER TABLE ONLY public.operationallowed ADD CONSTRAINT operationallowed_groupid_fkey FOREIGN KEY (groupid) REFERENCES public.groups(id)");
        $this->addSql("ALTER TABLE ONLY public.operationallowed ADD CONSTRAINT operationallowed_metadataid_fkey FOREIGN KEY (metadataid) REFERENCES public.metadata(id)");
        $this->addSql("ALTER TABLE ONLY public.operationallowed ADD CONSTRAINT operationallowed_operationid_fkey FOREIGN KEY (operationid) REFERENCES public.operations(id)");
        $this->addSql("ALTER TABLE ONLY public.operationsdes ADD CONSTRAINT operationsdes_iddes_fkey FOREIGN KEY (iddes) REFERENCES public.operations(id)");
        $this->addSql("ALTER TABLE ONLY public.operationsdes ADD CONSTRAINT operationsdes_langid_fkey FOREIGN KEY (langid) REFERENCES public.languages(id)");
        $this->addSql("ALTER TABLE ONLY public.regionsdes ADD CONSTRAINT regionsdes_iddes_fkey FOREIGN KEY (iddes) REFERENCES public.regions(id)");
        $this->addSql("ALTER TABLE ONLY public.regionsdes ADD CONSTRAINT regionsdes_langid_fkey FOREIGN KEY (langid) REFERENCES public.languages(id)");
        $this->addSql("ALTER TABLE ONLY public.serviceparameters ADD CONSTRAINT serviceparameters_service_fkey FOREIGN KEY (service) REFERENCES public.services(id)");
        $this->addSql("ALTER TABLE ONLY public.useraddress ADD CONSTRAINT useraddress_addressid_fkey FOREIGN KEY (addressid) REFERENCES public.address(id)");
        $this->addSql("ALTER TABLE ONLY public.useraddress ADD CONSTRAINT useraddress_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(id)");
        $this->addSql("ALTER TABLE ONLY public.usergroups ADD CONSTRAINT usergroups_groupid_fkey FOREIGN KEY (groupid) REFERENCES public.groups(id)");
        $this->addSql("ALTER TABLE ONLY public.usergroups ADD CONSTRAINT usergroups_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(id)");
        $this->addSql("ALTER TABLE ONLY public.validation ADD CONSTRAINT validation_metadataid_fkey FOREIGN KEY (metadataid) REFERENCES public.metadata(id)");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_donnee DROP CONSTRAINT fk_30055129141f7bf0");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_lot DROP CONSTRAINT fk_4674d3dfa9ee0a03");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_lot DROP CONSTRAINT fk_4674d3dffbc885b3");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_contenus_tiers DROP CONSTRAINT fk_4819e6af8ae06949");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_contenus_tiers DROP CONSTRAINT fk_4819e6afa8cba5f7");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_rapports_profils DROP CONSTRAINT fk_51faf077d1eae64c");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_couche_champ DROP CONSTRAINT fk_52a0ca003dc30acd");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_couche_champ DROP CONSTRAINT fk_52a0ca00d43aa457");
        $this->addSql("ALTER TABLE ONLY bdterr.bdterr_themes DROP CONSTRAINT fk_a06003abedb65433");
        $this->addSql("ALTER TABLE ONLY catalogue.carte_projet DROP CONSTRAINT carte_projet_cartp_fk_fiche_metadonnees_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.carte_projet DROP CONSTRAINT carte_projet_cartp_fk_stockage_carte_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_param DROP CONSTRAINT carto_colors_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.carte_projet DROP CONSTRAINT cartp_fk_utilisateur");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_param DROP CONSTRAINT colors_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.competence_accede_carte DROP CONSTRAINT competencecarte_fk_carte_projet_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.competence_accede_carte DROP CONSTRAINT competencecarte_fk_competence_id_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.competence_accede_couche DROP CONSTRAINT competencecouche_fk_competence_id_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.competence_accede_couche DROP CONSTRAINT competencecouche_fk_couche_donnees_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.couche_donnees DROP CONSTRAINT couche_donnees_couchd_fk_acces_server_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.couche_donnees DROP CONSTRAINT couche_donnees_fk_zonage_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.domaine DROP CONSTRAINT dom_rubric_fk");
        $this->addSql("ALTER TABLE ONLY catalogue.fiche_metadonnees DROP CONSTRAINT fiche_metadonnees_fmeta_fk_couche_donnees_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.execution_report DROP CONSTRAINT fk_8e1b7bbcad93e023");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_accede_competence DROP CONSTRAINT fk_competence_id_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.standards_conformite DROP CONSTRAINT fk_conformite_metadata");
        $this->addSql("ALTER TABLE ONLY catalogue.standards_conformite DROP CONSTRAINT fk_conformite_standard");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_accede_competence DROP CONSTRAINT fk_grp_id_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.perimetre DROP CONSTRAINT fk_perimetre_zonage_id_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.standards_standard DROP CONSTRAINT fk_standard_fournisseur");
        $this->addSql("ALTER TABLE ONLY catalogue.standards_standard DROP CONSTRAINT fk_standard_metadata");
        $this->addSql("ALTER TABLE ONLY catalogue.stockage_carte DROP CONSTRAINT fk_stk_server_acces_server");
        $this->addSql("ALTER TABLE ONLY catalogue.utilisateur_carte DROP CONSTRAINT fk_utilisateur_carte_fk_stockage_carte");
        $this->addSql("ALTER TABLE ONLY catalogue.utilisateur_carte DROP CONSTRAINT fk_utilisateur_carte_fk_utilisateur");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_param DROP CONSTRAINT fonts_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_param DROP CONSTRAINT geosource_colors_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_accede_dom DROP CONSTRAINT grp_accede_dom_grpdom_fk_domaine_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_accede_dom DROP CONSTRAINT grp_accede_dom_grpdom_fk_groupe_profil_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_accede_ssdom DROP CONSTRAINT grp_accede_ssdom_grpss_fk_groupe_profil_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_accede_ssdom DROP CONSTRAINT grp_accede_ssdom_grpss_fk_sous_domaine_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_autorise_trt DROP CONSTRAINT grp_autorise_trt_grptrt_fk_groupe_profil_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_autorise_trt DROP CONSTRAINT grp_autorise_trt_grptrt_fk_traitement_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_regroupe_usr DROP CONSTRAINT grp_regroupe_usr_grpusr_fk_groupe_profil_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_regroupe_usr DROP CONSTRAINT grp_regroupe_usr_grpusr_fk_utilisateur_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_trt_objet DROP CONSTRAINT grptrtobj_fk_grp_id_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_trt_objet DROP CONSTRAINT grptrtobj_fk_obj_type_id_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_trt_objet DROP CONSTRAINT grptrtobj_fk_trt_id_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.harves_opendata_node_params DROP CONSTRAINT \"harves_opendata_node_params_FK_node_id_fkey\"");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_external_access DROP CONSTRAINT prodige_extaccess_fk_utilisateur_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_help_group DROP CONSTRAINT prodige_help_group_hlpgrp_fk_groupe_profil_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.prodige_help_group DROP CONSTRAINT prodige_help_group_hlpgrp_fk_help_id_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.sous_domaine DROP CONSTRAINT sous_domaine_ssdom_fk_domaine_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.ssdom_dispose_couche DROP CONSTRAINT ssdom_dispose_couche_ssdcouch_fk_couche_donnees_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.ssdom_dispose_couche DROP CONSTRAINT ssdom_dispose_couche_ssdcouch_fk_sous_domaine_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.ssdom_dispose_metadata DROP CONSTRAINT ssdom_dispose_metadata_ssdcouch_fk_sous_domaine_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.ssdom_visualise_carte DROP CONSTRAINT ssdom_visualise_carte_ssdcart_fk_carte_projet_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.ssdom_visualise_carte DROP CONSTRAINT ssdom_visualise_carte_ssdcart_fk_sous_domaine_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.tache_import_donnees DROP CONSTRAINT tacheimportdonnees_fk_couchedonnees");
        $this->addSql("ALTER TABLE ONLY catalogue.trt_autorise_objet DROP CONSTRAINT trtautoobjet_fk_obj_type_id_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.trt_autorise_objet DROP CONSTRAINT trtautoobjet_fk_trt_id_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_accede_perimetre_edition DROP CONSTRAINT usr_accede_perim_usrperim_edit_fk_perimetre_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_accede_perimetre DROP CONSTRAINT usr_accede_perim_usrperim_edit_fk_utilisateur_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_accede_perimetre DROP CONSTRAINT usr_accede_perim_usrperim_fk_perimetre_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_accede_perimetre DROP CONSTRAINT usr_accede_perim_usrperim_fk_utilisateur_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_alerte_perimetre_edition DROP CONSTRAINT usr_alerte_perim_edit_fk_perimetre_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_alerte_perimetre_edition DROP CONSTRAINT usr_alerte_perim_edit_usrperim_fk_couchedonnees_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.usr_alerte_perimetre_edition DROP CONSTRAINT usr_alerte_perim_edit_usrperim_fk_utilisateur_fkey");
        $this->addSql("ALTER TABLE ONLY catalogue.zonage DROP CONSTRAINT zonaget_fk_couche_donnees_fkey");
        $this->addSql("ALTER TABLE ONLY public.categoriesdes DROP CONSTRAINT categoriesdes_iddes_fkey");
        $this->addSql("ALTER TABLE ONLY public.categoriesdes DROP CONSTRAINT categoriesdes_langid_fkey");
        $this->addSql("ALTER TABLE ONLY public.cswservercapabilitiesinfo DROP CONSTRAINT cswservercapabilitiesinfo_langid_fkey");
        $this->addSql("ALTER TABLE ONLY public.email DROP CONSTRAINT email_user_id_fkey");
        $this->addSql("ALTER TABLE ONLY public.statusvaluesdes DROP CONSTRAINT fk_2vkxyjsd2d3tdwn38p5yjhb71");
        $this->addSql("ALTER TABLE ONLY public.params DROP CONSTRAINT fk_6d52bqoq3c2eitpdq5r7y872g");
        $this->addSql("ALTER TABLE ONLY public.schematroncriteriagroup DROP CONSTRAINT fk_atfj71dq82he6n77lqofjxui6");
        $this->addSql("ALTER TABLE ONLY public.groups DROP CONSTRAINT fk_balio8qkvhnitbdw241e4ryb8");
        $this->addSql("ALTER TABLE ONLY public.sourcesdes DROP CONSTRAINT fk_c3jxktm4qwai73lddsm5fiecb");
        $this->addSql("ALTER TABLE ONLY public.schematroncriteria DROP CONSTRAINT fk_dh2vjs226vjp2anrvj3nuvt8x");
        $this->addSql("ALTER TABLE ONLY public.inspireatomfeed_entrylist DROP CONSTRAINT fk_eyt177hveh5vlxyxq6wpl3vqi");
        $this->addSql("ALTER TABLE ONLY public.usersavedselections DROP CONSTRAINT fk_f558oonrxskvsnoc835m73sru");
        $this->addSql("ALTER TABLE ONLY public.group_category DROP CONSTRAINT fk_j8nj5ssnar3byh882nuf38tqw");
        $this->addSql("ALTER TABLE ONLY public.metadatanotifications DROP CONSTRAINT fk_jbkvo3w3g4twk2bo1b8jn0sw8");
        $this->addSql("ALTER TABLE ONLY public.usersavedselections DROP CONSTRAINT fk_m7d8cfojvibxag0wtg6pqe3d");
        $this->addSql("ALTER TABLE ONLY public.selectionsdes DROP CONSTRAINT fk_r0y9hytqn3nodmwn86hn2vsgf");
        $this->addSql("ALTER TABLE ONLY public.group_category DROP CONSTRAINT fk_r1y7atocbww201qaj87h9j62e");
        $this->addSql("ALTER TABLE ONLY public.schematrondes DROP CONSTRAINT fk_sh1xwulyb1jeoc6puqpiuc5d2");
        $this->addSql("ALTER TABLE ONLY public.groups DROP CONSTRAINT groups_referrer_fkey");
        $this->addSql("ALTER TABLE ONLY public.groupsdes DROP CONSTRAINT groupsdes_iddes_fkey");
        $this->addSql("ALTER TABLE ONLY public.groupsdes DROP CONSTRAINT groupsdes_langid_fkey");
        $this->addSql("ALTER TABLE ONLY public.harvestersettings DROP CONSTRAINT harvestersettings_parentid_fkey");
        $this->addSql("ALTER TABLE ONLY public.isolanguagesdes DROP CONSTRAINT isolanguagesdes_iddes_fkey");
        $this->addSql("ALTER TABLE ONLY public.isolanguagesdes DROP CONSTRAINT isolanguagesdes_langid_fkey");
        $this->addSql("ALTER TABLE ONLY public.metadata DROP CONSTRAINT metadata_groupowner_fkey");
        $this->addSql("ALTER TABLE ONLY public.metadata DROP CONSTRAINT metadata_owner_fkey");
        $this->addSql("ALTER TABLE ONLY public.metadatacateg DROP CONSTRAINT metadatacateg_categoryid_fkey");
        $this->addSql("ALTER TABLE ONLY public.metadatacateg DROP CONSTRAINT metadatacateg_metadataid_fkey");
        $this->addSql("ALTER TABLE ONLY public.metadatarating DROP CONSTRAINT metadatarating_metadataid_fkey");
        $this->addSql("ALTER TABLE ONLY public.metadatastatus DROP CONSTRAINT metadatastatus_metadataid_fkey");
        $this->addSql("ALTER TABLE ONLY public.metadatastatus DROP CONSTRAINT metadatastatus_statusid_fkey");
        $this->addSql("ALTER TABLE ONLY public.metadatastatus DROP CONSTRAINT metadatastatus_userid_fkey");
        $this->addSql("ALTER TABLE ONLY public.operationallowed DROP CONSTRAINT operationallowed_groupid_fkey");
        $this->addSql("ALTER TABLE ONLY public.operationallowed DROP CONSTRAINT operationallowed_metadataid_fkey");
        $this->addSql("ALTER TABLE ONLY public.operationallowed DROP CONSTRAINT operationallowed_operationid_fkey");
        $this->addSql("ALTER TABLE ONLY public.operationsdes DROP CONSTRAINT operationsdes_iddes_fkey");
        $this->addSql("ALTER TABLE ONLY public.operationsdes DROP CONSTRAINT operationsdes_langid_fkey");
        $this->addSql("ALTER TABLE ONLY public.regionsdes DROP CONSTRAINT regionsdes_iddes_fkey");
        $this->addSql("ALTER TABLE ONLY public.regionsdes DROP CONSTRAINT regionsdes_langid_fkey");
        $this->addSql("ALTER TABLE ONLY public.serviceparameters DROP CONSTRAINT serviceparameters_service_fkey");
        $this->addSql("ALTER TABLE ONLY public.useraddress DROP CONSTRAINT useraddress_addressid_fkey");
        $this->addSql("ALTER TABLE ONLY public.useraddress DROP CONSTRAINT useraddress_userid_fkey");
        $this->addSql("ALTER TABLE ONLY public.usergroups DROP CONSTRAINT usergroups_groupid_fkey");
        $this->addSql("ALTER TABLE ONLY public.usergroups DROP CONSTRAINT usergroups_userid_fkey");
        $this->addSql("ALTER TABLE ONLY public.validation DROP CONSTRAINT validation_metadataid_fkey");

    }
}

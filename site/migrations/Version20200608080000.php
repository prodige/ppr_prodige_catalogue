<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200608080000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version tampon : Patch 4.2.7';
    }

    public function up(Schema $schema): void
    {
        //ajout des traitements dans les restrictions attributaires
        $this->addSql("ALTER TABLE catalogue.grp_restriction_attributaire add column grprat_fk_trt_id integer");
        $this->addSql("ALTER TABLE ONLY catalogue.grp_restriction_attributaire ADD CONSTRAINT grprat_fk_traitement_fkey FOREIGN KEY (grprat_fk_trt_id) REFERENCES catalogue.traitement(pk_traitement) MATCH FULL ON DELETE CASCADE");
        //migration de l'existant
        $this->addSql("update catalogue.grp_restriction_attributaire set grprat_fk_trt_id= (select pk_traitement from catalogue.traitement where trt_id='EDITION EN LIGNE')");
        $this->addSql("insert into catalogue.grp_restriction_attributaire (grprat_fk_couche_donnees, grprat_fk_groupe_profil, grprat_champ, grprat_fk_trt_id) select grprat_fk_couche_donnees, grprat_fk_groupe_profil, grprat_champ,2 from catalogue.grp_restriction_attributaire");
        
         
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE catalogue.grp_restriction_attributaire drop column grprat_fk_trt_id");
    }
}

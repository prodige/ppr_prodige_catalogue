<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ProdigeCatalogue\StandardsBundle\Common;



/**
 * Description of MessageCode
 *
 * @author ndurand
 */
class PostgresqlUtil {

    /**
     * 
     */
    public static function checkOneColumnInTable($conn, $table, $columnName){
        $sql = "SELECT column_name FROM information_schema.columns 
                WHERE table_name=:table_name and column_name = :columnName"; //"and column_name=:column_name";

        $sth = $conn->prepare($sql);
        $sth->execute(array('table_name' => $table, 'columnName' => $columnName ) );

        $result = $sth->fetchAllAssociative();

        return !empty($result);
    }
    
    /**
     * 
     */
    public static function checkColumnsInTable($conn, $table, $columnsFilters = array()){
        $sql = "SELECT column_name FROM information_schema.columns 
                WHERE table_name = :table_name"; //"and column_name=:column_name";

        $sth = $conn->prepare($sql);
        $sth->execute(array('table_name' => $table ) );

        $result = $sth->fetchAllAssociative();

        $isOk = true;
        $columnInDb = array();
        foreach($result as $key => $column){
            $columnInDb[] = $column["column_name"];
        }

        foreach($columnsFilters as $key => $column){
            $isOk = $isOk && (in_array($column, $columnInDb) );
        }

        return  $isOk;
    }


    /**
     * 
     */
    public static function getColumnInTableWithFilter($conn, $table, $columnsFilters = array()){
        $sql = "SELECT column_name FROM information_schema.columns 
                WHERE table_name=:table_name"; //"and column_name=:column_name";

        $sth = $conn->prepare($sql);
        $sth->execute(array('table_name' => $table ) );

        $result = $sth->fetchAllAssociative();

        $columns = array();

        foreach($result as $key => $column){
            if(isset($column['column_name']) ){
                if( empty($columnsFilters) ){
                    $columns[] = $column['column_name'];
                }
                else if( in_array($column['column_name'], $columnsFilters) ){
                    $columns[] = $column['column_name'];
                }
            } 
    
        }

        return $columns;
    }
    
    /**
     * Vérifie si la table en paramètre existe
     * @param $conn conexion pdo
     * @param $table nom de la table
     * 
     * @return true si table exist sinon false
     */
    public static function checkIfTableExist($conn, $table){
        $sql = "SELECT EXISTS (SELECT relname FROM pg_class WHERE relname = :table_name)";

        $sth = $conn->prepare($sql);
        $sth->execute(array('table_name' => $table));

        $result = $sth->fetchAllAssociative();

        if(!empty($result) && isset($result[0]['exists'])){
            return $result[0]['exists'];
        }

        return false;
    }

    /**
     * Retourne le nom et le type des collones d'une table
     * 
     * @return Array
     */
    public static function getColumnAndType($conn, $tableName, $schema){
        $sql = "SELECT c.column_name as name, c.data_type as type, pgd.description, is_nullable
                from pg_catalog.pg_statio_all_tables as st
                inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid)
                right outer join information_schema.columns c on (pgd.objsubid=c.ordinal_position and  c.table_schema=st.schemaname and c.table_name=st.relname)
                where table_schema = :schema and table_name = :tableName";

        return PostgresqlUtil::executeQuery($conn, $sql, array('tableName' => $tableName, 'schema' => $schema) );
    }


    /**
     * 
     * Récupère la liste des tables d'une base en fonction de son schéma
     * 
     * @return Array
     */
    static function getTablesFromDatabase($conn, $schema = 'public'){
        $sql = "SELECT tablename as table_name, schemaname as table_schema from pg_tables 
                WHERE schemaname = :tableSchema and tableowner <> 'postgres'";
        
        return PostgresqlUtil::executeQuery($conn, $sql, array('tableSchema' => $schema) );
    }

    /**
     * Récupère le nom de cahque contraintes d'une table
     */
    static function getTablesContraints($conn, $tableName, $schema = 'public'){
        $sql = "SELECT  conname as name  FROM pg_catalog.pg_constraint con
                INNER JOIN pg_catalog.pg_class rel ON rel.oid = con.conrelid
                INNER JOIN pg_catalog.pg_namespace nsp ON nsp.oid = connamespace
                WHERE nsp.nspname = :schema AND rel.relname = :tableName";
        
        return PostgresqlUtil::executeQuery($conn, $sql, array('schema' => $schema, 'tableName' => $tableName) );
    }

    static function executeQuery($conn, $sql, $params = array()){
        $sth = $conn->prepare($sql);
        $sth->execute($params);

        $result = $sth->fetchAllAssociative();

        return $result;
    }
}

/*
-- Activation of the standards module
*/
insert into catalogue.prodige_settings (prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type) 
values ('PRO_MODULE_STANDARDS', 'Active le module de vérification des métadonnées conformément à des standards', 'off', 'Active le module de vérification des métadonnées conformément à des standards', 'checkboxfield');

create table catalogue.standards_fournisseur(
    fournisseur_id serial not null, 
    fournisseur_name text not null, 
    fournisseur_url text not null,
    CONSTRAINT pk_standards_fournisseur PRIMARY KEY (fournisseur_id)
);
create table catalogue.standards_standard(
    standard_id serial not null, 
    fournisseur_id int not null, 
    standard_name text not null, 
    standard_url text not null,
    standard_database text,
 	  standard_schema text,
    fmeta_id int,
    CONSTRAINT pk_standards_standard   PRIMARY KEY (standard_id),
    CONSTRAINT fk_standard_fournisseur FOREIGN KEY (fournisseur_id) REFERENCES catalogue.standards_fournisseur (fournisseur_id),
    CONSTRAINT fk_standard_metadata    FOREIGN KEY (fmeta_id)       REFERENCES public.metadata (id)
);
create table catalogue.standards_conformite(
    metadata_id int not null,
    standard_id int not null,
    conformite_prefix varchar(100),
    conformite_suffix varchar(100),
    conformite_success boolean default true,
    CONSTRAINT pk_standards_conformite  PRIMARY KEY (metadata_id),
    CONSTRAINT fk_conformite_metadata   FOREIGN KEY (metadata_id)   REFERENCES public.metadata (id),
    CONSTRAINT fk_conformite_standard   FOREIGN KEY (standard_id)   REFERENCES catalogue.standards_standard (standard_id)
);

insert into catalogue.standards_fournisseur(fournisseur_name, fournisseur_url) values ('Prodige', 'prodige.org');
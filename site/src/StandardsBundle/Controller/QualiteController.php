<?php

namespace ProdigeCatalogue\StandardsBundle\Controller;

use Prodige\ProdigeBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use FOS\RestBundle\Controller\Annotations\Delete,
    FOS\RestBundle\Controller\Annotations\Get,
    FOS\RestBundle\Controller\Annotations\Route,
    FOS\RestBundle\Controller\Annotations\Head, 
    FOS\RestBundle\Controller\Annotations\Link, 
    FOS\RestBundle\Controller\Annotations\Patch,
    FOS\RestBundle\Controller\Annotations\Post, 
    FOS\RestBundle\Controller\Annotations\Put, 
    FOS\RestBundle\Controller\Annotations\Unlink;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Version;


use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

use Prodige\ProdigeBundle\Common\Modules\Standard\MetadataUtil;
use Prodige\ProdigeBundle\Common\PostgresqlUtil;

use Prodige\ProdigeBundle\Common\Modules\Standard\StandardCtrlQualite;


use DOMDocument;
use DOMXpath;
use DateTime;

/**
 * Description of QualiteController
 *
 * @Route("/standards",  defaults={}, options={"expose"=true})
 */
class QualiteController extends BaseController{
    
    /** */
    protected $typeCompatible = array(
        array('numeric', 'float', 'double', 'real', 'number'),
        array('character varying','varchar', 'text'), 
        array('integer', 'int4', 'int8', 'bigint', 'smallint')
    );

    protected $urlConformiteReport = 'upload/';

    /**
     * définition de la route pour extjs
     * 
     * @Head("/",  name="standard_api_qualite", options={"expose"=true})
     */
    public function restAction(ParamFetcherInterface $paramFetcher){
        throw new NotAcceptableHttpException("Cette route ne doit pas être accédée directement. Fixez la méthode d'appel à une des valeurs parmi {'GET','POST','PATCH','PUT','DELETE'}");
    }


    /**
     * Affichage de la page d'accueil de Controle Qualite StandardsBundle
     * 
     * @IsGranted("ROLE_USER")
     * 
     *
     * @Get("/qualite/{uuid}")
     *
     * @Route("/", name="home")
     * @param Request $request  request object
     */
    public function indexAction(Request $request, $uuid = null){
        $conn = $this->getCatalogueConnection();

        // requette pour savoir un metadata est déjà conforme 
        $sql = "SELECT m.id, sc.conformite_report_url as url, sc.conformite_success as success from catalogue.standards_conformite sc
                       inner join public.metadata m on sc.metadata_id = m.id
                       where m.uuid = :uuid";

        $sth = $conn->prepare($sql);
        $sth->execute(array('uuid' => $uuid));
        $result = $sth->fetchAllAssociative();
        
        // génération de l'url de rapport de conformité
        $url = "";
        if(!empty($result)){
            $url = $result[0]['url'];
            $urlCatalogue = $this->container->hasParameter('PRODIGE_URL_CATALOGUE') ? $this->container->getParameter('PRODIGE_URL_CATALOGUE') : "";

            $url = str_replace(PRO_PATH_WEB, "", $url );
            $urlCatalogue = str_replace("/app_dev.php", "", $urlCatalogue);
        
            $url = $urlCatalogue."/".$url;
        }
        
        $parameters = array(
            "user" => ($this->getUser() ? $this->getUser() : null),
            "uuid" => $uuid,
            'isConforme' => ( !empty($result) && $result[0]['success'] ) ? 1 : 0,
            'repportUrl' => $url
        );

        return $this->render('StandardsBundle/Qualite/index.html.twig', $parameters);
    }
    
    /**
     * @Get("/update_catalogue/{uuid}")
     *
     * @Route("/", name="home")
     * @param Request $request  request object
     */
    public function updataAtributCatalogue(Request $request, $uuid ){
        $data = array('msg' => 'error');

        if(!$uuid){
            return new JsonResponse(array('msg' => 'error', 'error' => 'uuid null'), Response::HTTP_NOT_FOUND); 
        }

        // On récupère le metadata dans la base
        $conn = $this->getCatalogueConnection();
        $metadata = $this->getMetadataByUuid($conn, $uuid);
        
        if(empty($metadata)){
            return new JsonResponse(array('msg' => 'error', 'error' => 'metadata not found'), Response::HTTP_NOT_FOUND); 
        }
        // on récupère le Metadata du standard
        $sql =  "SELECT * from public.metadata as md
                 INNER JOIN catalogue.standards_standard std on std.fmeta_id = md.id
                 INNER JOIN catalogue.standards_conformite as sc on sc.standard_id = std.standard_id
                 where sc.metadata_id = :metadataId and sc.conformite_success = true";

        $sth = $conn->prepare($sql);
        $sth->execute( array( 'metadataId' => $metadata['id'] ));

        $result = $sth->fetchAllAssociative();
        if(empty($result)){
            return new JsonResponse(array('msg' => 'error', 'error' => 'standard not found'), Response::HTTP_NOT_FOUND); 
        }

        $stdMetadata = $result[0];
        
        // on suprime les catalogues dans les data xml  
        $this->removeCatalogueFromMetadata($metadata);

        // on ajoute les catalogue du standard

        $this->updateMetadataWithStandard($conn, $metadata['id'], $stdMetadata['data']);

        $data["success"] = true;
        
        return new JsonResponse($data, ($data["success"] ? Response::HTTP_OK : Response::HTTP_NOT_FOUND)); 
    }

    /**
     * Liste des standards, format pour les stores extjs
     * 
     * @IsGranted("ROLE_USER")
     * 
     *
     * @Get("/list")
     *
     * 
     * @param Request $request  request object
     */
    function getStandard(Request $request){
        $conn = $this->getCatalogueConnection();

        $data = array("success" => false);

        $data['Standard'] = $this->getStandardList($conn);
        $data['success'] = !empty($data['Standard']);
      
        return new JsonResponse($data, ($data["success"] ? Response::HTTP_OK : Response::HTTP_NOT_FOUND)); 
    }
    
    /**
     * @return Array
     */
    function getStandardList($conn){
        
        $sql = "SELECT standard_id as id, standard_name as name from catalogue.standards_standard";

        $sth = $conn->prepare($sql);
        $sth->execute();

        $result = $sth->fetchAllAssociative();

        return $result;
    }

    /**
     * Liste des standards, format pour les stores extjs
     * 
     * @IsGranted("ROLE_USER")
     * 
     *
     * @Post("/analyse")
     *
     * 
     * @param Request $request  request object
     */
    function postAnalyse(Request $request){
        $conn = $this->getCatalogueConnection();
        $connProdige = $this->getProdigeConnection();
        
        $data = array("msg"=> "erreur", "success" => false);
        
        $standardId = $request->request->get('standard');
        $prefix = $request->request->get('prefix');
        $suffix = $request->request->get('suffix');
        $uuid =  $request->request->get('uuid');

        // On récupère le standard
        $standard = StandardCtrlQualite::getStandardById($conn, $standardId);

        if(empty($standard) || !isset($standard['database']) || is_null($uuid)){
            return new JsonResponse(array_merge(array("test" => array(empty($standard), !isset($standard['database']),is_null($uuid))), $data), Response::HTTP_NOT_FOUND); 
        }

        // On se connecte à la base tampon
        $tamponConn = $this->getDBTamponConnection($standard['database']);
       
        $ctrlQualite = new StandardCtrlQualite($conn, $connProdige, $tamponConn, $this->isProd() );
        if(is_null($tamponConn)){
            return new JsonResponse(array_merge(array("test " => 'tamponConn null ou la base '.$standard['database']. " n'existe pas"), $data), Response::HTTP_NOT_FOUND); 
        }
        
        // On récupère les nom des tables de la base tampons
        $tablesTampons = PostgresqlUtil::getTablesFromDatabase($tamponConn, $standard['schema'] );

        // On récupère les noms des tables liées au Metadata
        $metadataTables = $ctrlQualite->getMetadataTablesByUuid($uuid);

        // Comparaison des table metadata avec le standard
        $results = $ctrlQualite->compareMetadataToStandard($metadataTables, $tablesTampons, $prefix, $suffix);

        $data['result'] = $results;
      
        // Création du fichier html
        $html =  $this->render('@ProdigeProdige/Templates/standard/repport.html.twig', $results);

        $data["success"] = true;
       
        $data["url"] = $ctrlQualite->updateOrAddStandardConformite($uuid, $standardId, $prefix, $suffix, $results['allOk'], $html);

        $urlCatalogue = $this->container->hasParameter('PRODIGE_URL_CATALOGUE') ? $this->container->getParameter('PRODIGE_URL_CATALOGUE') : "";

        $data['url'] = str_replace(PRO_PATH_WEB, "", $data['url'] );
        $urlCatalogue = str_replace("/app_dev.php", "", $urlCatalogue);
    
        $data['url'] = $urlCatalogue."/".$data['url'];

        return new JsonResponse($data, ($data["success"] ? Response::HTTP_OK : Response::HTTP_NOT_FOUND)); 
    }

    /**
     * @Get("/conformite")
     * 
     * @QueryParam(name="uuid",description="", default="", nullable = false )
     */
    function getConformiteAction(ParamFetcherInterface $paramFetcher){
        $uuid =  $paramFetcher->get('uuid');
        $data["success"] = !is_null($uuid);

        $conn = $this->getCatalogueConnection();

        if($uuid){
            // Recherche des metadata
            $metadata = $this->getMetadataByUuid($conn, $uuid);

            // Si on ne trouve rien
            if(empty($metadata)){
                return new JsonResponse( array("success" => false, "message" => "metadata with uuid $uuid not found"), Response::HTTP_NOT_FOUND); 
            }

            $standCtrlQualite = new StandardCtrlQualite($conn, null, null);

            // ajout de la conformité aux metadata
            $xmlData = $standCtrlQualite->metadataToConformite($metadata);
           
            $standCtrlQualite->updateMetadataXml($uuid, $xmlData);
        }


        return new JsonResponse($data, ($data["success"] ? Response::HTTP_OK : Response::HTTP_NOT_FOUND)); 
    }
    
    /** 
     * 
     */
    function getMetadataByUuid($conn, $uuid){
        // Recherche de la metadata
        $sth = $conn->prepare( "select * from public.metadata where uuid = :uuid" );
        $sth->execute( array( 'uuid' => $uuid ));

        $result = $sth->fetchAllAssociative();

        return !empty($result) ? $result[0] : array();
    }

    /** */
    public function removeCatalogueFromMetadata($metadata){
        $isOk = false;
      
        $xmlDoc = new DOMDocument("1.0", "UTF-8");
       
        try{
            $xml = $metadata['data'];

            $xmlDoc->loadXML($xml);
            $xpath = new  DOMXPath($xmlDoc);
            $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:contentInfo");

            foreach($objXPath as $featureCatalogue){ 
                $featureCatalogue->parentNode->removeChild($featureCatalogue);
            }

            $xml = $xmlDoc->saveXml();
            $this->updateMetadataWithXml($metadata['id'], $xml);
            $isOk = true;
        }
        catch(Exception $exception){
            //echo $exception->getMessage();
        }

        return $isOk;
    }

    /**
     * 
     * @param xml string
     */
    public function updateMetadataWithStandard($conn, $metadataId, $standardXml){
        $geonetwork = $this->getGeonetworkInterface(true);
        
        $xmlDoc = new DOMDocument("1.0", "UTF-8");
        $xmlDoc->loadXML($standardXml);
        $xpath = new  DOMXPath($xmlDoc);

        $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:contentInfo/gmd:MD_FeatureCatalogueDescription/gmd:featureCatalogueCitation");
        
        // Récupération du type catalogue
        $typeId = MetadataUtil::getTypeIdForCreateMetadata($geonetwork);
        if(!$typeId){
            return false;
        }


        foreach($objXPath as $featureCatalogue){
            $stdCatalogue = null;
            $metadataCatalogue = null;
            $uuid = $featureCatalogue->attributes->getNamedItem('uuidref');
            $uuid = $uuid && $uuid->nodeValue ? $uuid->nodeValue : "";

            if($uuid){
                $stdCatalogue = $this->getMetadataByUuid($conn, $uuid);
            }

            if($stdCatalogue){
                $metadataCatalogue = MetadataUtil::createMetadata($conn, $typeId);
            }

            if($metadataCatalogue){
                MetadataUtil::updateMetadataWithXml($geonetwork, $metadataCatalogue['id'], $stdCatalogue['data'], 'n');

                $jsonResp = $geonetwork->linkFeatureCatalog($metadataId, $metadataCatalogue['uuid']);
                $jsonResp = $jsonResp ? json_decode($jsonResp, true) : null;
            }
        }
    }

    public function updateMetadataWithXml($metadataId, $xml, $template = 'n'){
        $xml = str_replace('<?xml version="1.0" encoding="UTF-8"?>', "", $xml);
        
        $geonetwork = $this->getGeonetworkInterface(true);
        $urlUpdateData = "md.edit.save";
        $formData = array (
            "id" => $metadataId,
            "data" => $xml,
            "template" => $template
        );
    
        // send to geosource
        $jsonResp = $geonetwork->post($urlUpdateData, $formData);
        $jsonResp = $jsonResp ? json_decode($jsonResp, true) : null;

    }
}
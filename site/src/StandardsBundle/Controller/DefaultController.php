<?php

namespace ProdigeCatalogue\StandardsBundle\Controller;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\Routing\Annotation\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Description of FournisseurController
 *
 */
class DefaultController extends BaseController
{
    /**
     * @Route("/standards/administration", name="standards_administration", options={"expose"=true})
     * 
     * @return type
     */
    public function indexAction()
    {
        return $this->render('StandardsBundle/Default/index.html.twig');
    }
}

<?php

namespace ProdigeCatalogue\StandardsBundle\Controller;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use FOS\RestBundle\Controller\Annotations\Delete,
    FOS\RestBundle\Controller\Annotations\Get,
    FOS\RestBundle\Controller\Annotations\Route,
    FOS\RestBundle\Controller\Annotations\Head, 
    FOS\RestBundle\Controller\Annotations\Link, 
    FOS\RestBundle\Controller\Annotations\Patch,
    FOS\RestBundle\Controller\Annotations\Post, 
    FOS\RestBundle\Controller\Annotations\Put, 
    FOS\RestBundle\Controller\Annotations\Unlink;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Version;

use Prodige\ProdigeBundle\Services\CurlUtilities;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

use Symfony\Component\Process\Process;

use Nelmio\ApiDocBundle\Annotation\Operation;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

use ProdigeCatalogue\StandardsBundle\Controller\FournisseurController;

use DOMDocument;
use Exception;
use DOMXPath;
/**
 * Description of StandardController
 *
 * @Route("/standards/rest/standard/{standard_id}", defaults={"standard_id"=null, "activation": "any"}, requirements={"standard_id": "^\d+$"}, options={"expose"=true})
 */
class StandardController extends BaseController
{   

    const TAMPON_TEMPLATE       = "tampon_tpl";
    const DEFAULT_SCHEMA_NAME = "defaut";
    
    /** Pour les requette Curl */
    protected $ticketNeeded = false;

    /**
     * Restfull api to serve Entity.
     *
     * @Head(name="prodige_standards_standard_rest")
     * 
     * @param Request $request
     * @param unknown $entity
     * @param unknown $id
     * @QueryParam(name="sort", description="fields available for sorting", requirements="(-|\+)?(id|name|model)", default="standard_id", nullable=true)
     * @QueryParam(name="limit", description="limit", requirements="\d+", default="-1", nullable=true)
     * @QueryParam(name="offset", description="offset", requirements="\d+", default="-1", nullable=true)
     * @QueryParam(name="callback", description="JSONP callback", default="", nullable=true) 
     */
    public function restAction(Request $request, $standard_id = null)
    {
        throw new NotAcceptableHttpException("Cette route ne doit pas être accédée directement. Fixez la méthode d'appel à une des valeurs parmi {'GET','POST','PATCH','PUT','DELETE'}");
    }
    
    /**
     * Get standards
     * 
     * @Get()
     * #Security("has_role('ROLE_ADMIN')")
     * 
     * @Operation(
     *   tags={"Standards de standards"},
     *   summary="Get standards configuration and property",
     *   @OA\Response(
     *       response="200",
     *      description="Succès"
     *   ),
     * )
     */    
    public function getAction(Request $request, $standard_id=null)
    {
        
        $data = array("success" => true, "standards"=>array());

        $CATALOGUE = $this->getCatalogueConnection("catalogue"); // Doctrine\DBAL\Connection     
        
        
        if( !$standard_id ) {            
            $metadata = $CATALOGUE->fetchAllAssociative(" SELECT * FROM catalogue.standards_standard order by standard_name "); 
            $data["standards"] = $metadata;
        } else {
            $metadata = $CATALOGUE->fetchAssociative(" SELECT * FROM catalogue.standards_standard where standard_id = :standard_id", compact("standard_id")); 
            $data["success"] = !empty($metadata);
            $data["standards"] = $metadata;
            if ( !$data["success"] ){
                $data["message"] = "Ce standard n'existe pas";
            }
        }     
        return new JsonResponse($data, ($data["success"] ? Response::HTTP_OK : Response::HTTP_NOT_FOUND)); 
    }
    
    /**
     * Exemple POST
     * 
     * @Post()
     * 
     * #Security("has_role('ROLE_ADMIN')")
     * 
     * 
     * description="Sends a new standard",
     * parameters={
     *      {"name"="title",        "dataType"="payload",  "required"=true, "format"="json, {reset_counter}, To be placed in the JSON payload !", "description"="standards title"},
     *      {"name"="content",      "dataType"="payload",  "required"=true, "format"="json, {}, To be placed in the JSON payload !", "description"="standards contents"},
     *      {"name"="date",      "dataType"="payload",  "required"=true, "format"="json, {}, To be placed in the JSON payload !", "description"="standards date"},
     * },
     * @Operation(
     *   tags={"Standards de standards"},
     *   summary="Post a new standard",
     *   @OA\Response(
     *      response="200",
     *      description="Succès"
     *   ),
     * )
     * @return JsonResponse
     */
    public function postAction(Request $request)
    {
        set_time_limit(60);
        $CATALOGUE = $this->getCatalogueConnection("catalogue"); // Doctrine\DBAL\Connection
        
        //it's required to define specifique data type
        $types = array(); 

        //all submitted values     
        $params = $request->request->all();
        
        // all params are the columns of table
        $keys = array_combine(array_keys($params), array_keys($params));
      
        $data = array("success"=>false);
        
        try {
            $standardXml = $this->getXmlByCurl($params["standard_url"]); 

            $tabInfoStandard = FournisseurController::getStandardInfo($params["standard_url"], $standardXml);
            list($dbName, $dbSchema) = $this->importStandard($tabInfoStandard["dumpUrl"]);
            $keys[] = "standard_database";
            $keys[] = "standard_schema";
            
            $params["standard_database"] = $dbName;
            $params["standard_schema"] = $dbSchema;
            
            // Création du Metadata et des catlogues
            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
            $group_id = 1;
            $jsonResp = $geonetwork->get('md.create?_content_type=json&id=' . PRO_WFS_METADATA_ID . "&group=" . $group_id . "&template=y&_isTemplate=y&istemplate=y&child=n&fullPrivileges=false");
            
            $jsonResp = $jsonResp ? json_decode($jsonResp, true) : null;

            $metadataId = isset($jsonResp['id'])  ? $jsonResp['id'] : null;
            $metadata = null;
            $uuid = null;

            if($metadataId){
                $metadata = $this->getMetadataById($metadataId);

                $uuid = isset($metadata['uuid']) ? $metadata['uuid'] : null;

                $standardXml = $this->updateStandardXml($standardXml, $uuid);
                $this->updateMetadataWithXml($metadataId, $standardXml);

                $uuidToRemoves = $this->addCatalogueToMetadata($standardXml, $metadataId);
                $this->removeUuidFromMetadata($metadataId, $uuidToRemoves);
                
                $params["fmeta_id"] = $metadataId;
                $keys[] = "fmeta_id";
            }

            //
            $data["standards"] = $CATALOGUE->fetchAssociative("insert into catalogue.standards_standard (".implode(", ", $keys).") values (:".implode(", :", $keys).") returning *", array_merge($params), $types);
            $data["message"] = "Ce standard a été correctement créé";
            
            if ( !is_array($data["standards"]) ){
                $data["message"] = "Une erreur s'est produite";
                $data["standards"] = array();
            } else {
                $data["success"] = true;
            }
            
        }
         catch(Exception $exception){
            $data["message"] = "Une erreur s'est produite";
            $data["exception"] = $exception->getMessage();
        }

        return new JsonResponse($data, ($data["success"] ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST)); 
    }       
    
    /** */
    public function removeUuidFromMetadata($metadataId, $uuids){
        $isOk = false;
        if(!$uuids || !is_array($uuids)){
            return $isOk;
        }
        $metadata = $this->getMetadataById($metadataId);
        $xml = $metadata['data'];

        $xmlDoc = new DOMDocument("1.0", "UTF-8");

       
        try{
            $xmlDoc->loadXML($xml);
            $xpath = new  DOMXPath($xmlDoc);
            $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:contentInfo");

            foreach($objXPath as $featureCatalogue){ 
                $uuid = null;
                
                $node = $featureCatalogue->getElementsByTagName("featureCatalogueCitation");
                
                if($node  && $node->length > 0){
                    $uuid = $node->item(0)->attributes->getNamedItem('uuidref');
                    $uuid = $uuid && $uuid->nodeValue ? $uuid->nodeValue : "";
                }

                if($uuid && isset($uuids[$uuid])){
                    $featureCatalogue->parentNode->removeChild($featureCatalogue);
                }
            }
            $xml = $xmlDoc->saveXml();
            $this->updateMetadataWithXml($metadataId, $xml);
            $isOk = true;
        }
        catch(Exception $exception){
            //echo $exception->getMessage();
        }

        return $isOk;
    }

    /**
     * 
     */
    public function updateStandardXml($xml, $uuid){
        $xmlDoc = new DOMDocument("1.0", "UTF-8");
        $xmlDoc->loadXML($xml);
        $xpath = new  DOMXPath($xmlDoc);

        $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:fileIdentifier/gco:CharacterString");
        if ($objXPath && $objXPath->length>0) {
           $objXPath->item(0)->nodeValue = $uuid;
        }

        $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:applicationSchemaInfo/gmd:MD_ApplicationSchemaInformation/gmd:name/gmd:CI_Citation/gmd:identifier/gmd:MD_Identifier");
        if ($objXPath && $objXPath->length > 0) {
            $urlCatalogue = $this->container->hasParameter('PRO_GEONETWORK_URLBASE') ? $this->container->getParameter('PRO_GEONETWORK_URLBASE') : "";
            $urlCatalogue = str_replace("/app_dev.php", "", $urlCatalogue). "srv/". $uuid;
         
            $nodeCode = $xmlDoc->createElement("gmd:code");
            $nodeGcoChar = $xmlDoc->createElement("gco:CharacterString", $urlCatalogue);

            $nodeCode->appendChild($nodeGcoChar);
            $oldNode = $objXPath->item(0)->getElementsByTagName('gmd:code');

            if($oldNode && $oldNode->length > 0){
                $objXPath->item(0)->replaceChild($nodeCode, $oldNode->item(0));
            }
            else{
                $objXPath->item(0)->appendChild($nodeCode);
            }
        }

       return $xmlDoc->saveXml();
    }
  
    /**
     * @deprecated utiliser la version metadataUtil 
     */
    public function updateMetadataWithXml($metadataId, $xml, $template = 'y'){
        $xml = str_replace('<?xml version="1.0" encoding="UTF-8"?>', "", $xml);
        
        $geonetwork = $this->getGeonetworkInterface(true);
        $urlUpdateData = "md.edit.save";
        $formData = array (
            "id" => $metadataId,
            "data" => $xml,
            "template" => $template
        );
    
        // send to geosource
        $jsonResp = $geonetwork->post($urlUpdateData, $formData);
        $jsonResp = $jsonResp ? json_decode($jsonResp, true) : null;
    }

    /** */
    public function getXmlByCurl($url, $withIdent = false){
       
        $this->ticketNeeded = CurlUtilities::ticketNeeded();
        CurlUtilities::ticketNeeded(false);
        
        $this->curlUseDefaultAuthentication($withIdent);
        $result = $this->curl($url);
       
        CurlUtilities::ticketNeeded($this->ticketNeeded);
        
        return $result;
    }

    /**
     * @deprecated utiliser la version de metadataUtil
     * @return array{uuid,id} uuid => le nouvel uuid ; id => de la metadata
     */
    public function createMetadata($id = PRO_WFS_METADATA_ID){
      
        $group_id = 1;
    
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');

        $jsonResp = $geonetwork->get('md.create?_content_type=json&id=' . $id . "&group=" . $group_id . "&child=n&fullPrivileges=false");
        $jsonResp = $jsonResp ? json_decode($jsonResp, true) : null;
       
        $metadataId = isset($jsonResp['id'])  ? $jsonResp['id'] : null;

        return ( $metadataId ? $this->getMetadataById($metadataId) : null );
    }

    /**
     *  @deprecated utiliser la foncton dans medataUtil
     */
    public function getTypeIdForCreateMetadata(){
        $geonetwork = $this->getGeonetworkInterface(true);
        $url = "/qi";
        $args = array(
            "_content_type" => "json", 
            "template"      => "y", 
            "type"          => "featureCatalog", 
            "fast"          => "y", 
            "sortBy"        => "title", 
            "sortOrder"     => "reverse",
        );
        
        $url = $url . "?" . http_build_query($args);
        
        $jsonResp = $geonetwork->get($url);
        $jsonResp = $jsonResp ? json_decode($jsonResp, true) : null;

        $info = null;
       
        if(isset($jsonResp['gfc:FC_FeatureCatalogue']) && isset($jsonResp['gfc:FC_FeatureCatalogue'][0]) && isset($jsonResp['gfc:FC_FeatureCatalogue'][0]['geonet:info']) ){
            $info = $jsonResp['gfc:FC_FeatureCatalogue'][0]['geonet:info'];
        }
        else if(isset($jsonResp['gfc:FC_FeatureCatalogue']) && isset($jsonResp['gfc:FC_FeatureCatalogue']['geonet:info'])){
            $info = $jsonResp['gfc:FC_FeatureCatalogue']['geonet:info'];
        }
      
        return ( $info && isset($info['id']) ) ? $info['id'] : null;
    }
     
    /**
     * 
     * @param xml string
     */
    public function addCatalogueToMetadata($xml, $metadataId){
        $xmlDoc = new DOMDocument("1.0", "UTF-8");
        $xmlDoc->loadXML($xml);
        $xpath = new  DOMXPath($xmlDoc);
        $isOk = false;
        $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:contentInfo/gmd:MD_FeatureCatalogueDescription/gmd:featureCatalogueCitation");

        $typeId = $this->getTypeIdForCreateMetadata();

        if(!$typeId){
            return false;
        }

        $nodesToRemoves = array();

        foreach($objXPath as $featureCatalogue){
          
            // 1 - Pour chaque noeud on récupère le xml via Curl
            $urlDoc = $featureCatalogue->attributes->getNamedItemNS('http://www.w3.org/1999/xlink', 'href');
            
            $urlDoc = $urlDoc && $urlDoc->nodeValue ? $urlDoc->nodeValue : null;
            
            
            $uuid = $featureCatalogue->attributes->getNamedItem('uuidref');
            $uuid = $uuid && $uuid->nodeValue ? $uuid->nodeValue : "";
            
            //Hack : l'URL associée ne renvoie pas le contenu du catalogue d'attrubut
            $parsedUrl = parse_url($urlDoc);
            $urlDoc = $parsedUrl['scheme'] . '://' . $parsedUrl['host'] . '/geonetwork/srv/'.$uuid;

            $nodesToRemoves[$uuid] = true;
          
            //echo $urlDoc;
            $xmlCatalogue = !is_null($urlDoc) ?  $this->getXmlByCurl($urlDoc) : null;
    
            // 2 - on enregistre le xml dans la metadata
            $catalogueDoc = new DOMDocument("1.0", "UTF-8");
            
            try{
               
                if($xmlCatalogue && is_string($xmlCatalogue) && $catalogueDoc->loadXml($xmlCatalogue)){
                    $xpathCatalogue = new  DOMXPath($catalogueDoc);
                    $objXPathCatalogue = $xpathCatalogue->query('/html'); // Pour éviter d'enregistrer la page HTML de connection
                    $metadataCatalogue = null;
                  
                    if( !($objXPathCatalogue && $objXPathCatalogue->length > 0) ){
                        $metadataCatalogue = $this->createMetadata($typeId);
                    }
                
                    if($metadataCatalogue && is_array($metadataCatalogue) && isset($metadataCatalogue['uuid']) && isset($metadataCatalogue['id'])){
                        $this->updateMetadataWithXml($metadataCatalogue['id'], $xmlCatalogue);
                        $geonetwork = $this->getGeonetworkInterface(true);
                        $json = $geonetwork->linkFeatureCatalog($metadataId, $metadataCatalogue['uuid']);
                    }
                }
              
                $isOk = $isOk && true;
            }
            catch(Exception $e){
              // echo  $e->getMessage()."\n";
            }
           
        }

        return $nodesToRemoves;
    }
    
    /**
     * 
     * @param xml string
     */
    public function deleteCatalogueFromMetadata($conn, $xml){
        $geonetwork = $this->getGeonetworkInterface(true);
        $xmlDoc = new DOMDocument("1.0", "UTF-8");
        $xmlDoc->loadXML($xml);
        $xpath = new  DOMXPath($xmlDoc);
        $isOk = false;
        $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:contentInfo/gmd:MD_FeatureCatalogueDescription/gmd:featureCatalogueCitation");

        foreach($objXPath as $featureCatalogue){
            $uuid = $featureCatalogue->attributes->getNamedItem('uuidref');
            $uuid = $uuid && $uuid->nodeValue ? $uuid->nodeValue : "";
            
           
            $fcat = $conn->fetchOne("select id from public.metadata where uuid=:uuid", array("uuid"=>$uuid));
            $jsonp = $geonetwork->get('md.delete?_content_type=json&id=' . $fcat);
        }
    }
    
    /** 
     * Retourne la liste des schemas d'un fichier
     * 
     * @param  $file
     * @return 
     */
    protected function getSchemasFromFile($file)
    {
        $schemas = array();
        if( file_exists($file) ) {
            $regexExp = 'create schema';
            $cmd = 'grep -i "'.$regexExp.'" '.$file;
            $process = Process::fromShellCommandline($cmd);
            $process->run();
            $strOutput = $process->getOutput();
            
            if( !empty($strOutput) ) {
                $arOutput = explode($regexExp, strtolower($strOutput));
                if( count($arOutput) ) {
                    foreach( $arOutput as $output ) {
                        if( !empty(trim($output)) ) {
                            $strSchemas = str_replace('IF NOT EXISTS ', '', str_replace(';', '', trim(strtolower($output))));
                            $arSchemas  = explode(',', $strSchemas);
                            
                            if( count($arSchemas) ) {
                                foreach($arSchemas as $schema) {
                                    preg_match('/^[a-z][a-z0-9_]*$/', $schema, $matches);
                                    
                                    if( count($matches) ) {
                                        $schemas[] = trim($schema);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $schemas;
    }
    
    /**
     * @deprecated utiliser la version de metadataUtil
     */
    public function getMetadataById($metadataId){
        $conn = $this->getCatalogueConnection();

        $sql = "SELECT * FROM public.metadata where metadata.id = :metadataId";

        $sth = $conn->prepare($sql);
        $sth->execute(array('metadataId' => $metadataId));
        $result = $sth->fetchAllAssociative();

        return empty($result) ? array() : $result[0];
    }   

    /**
     * 
     * @param type $dumpUrl
     * @return typeImort Standard in a new database
     * @param $dumpUrl : File URL
     */
    protected function importStandard($dumpUrl ){
        //TODO mode update      
        $TAMPON_AUTH           = $this->container->getParameter("tampon_user");
        $TAMPON_AUTH_PWD       = $this->container->getParameter("tampon_password");
        
        //first download file
        $ticketNeeded = CurlUtilities::ticketNeeded();
        CurlUtilities::ticketNeeded(false);
        $sqlFile = PRO_MAPFILE_PATH."temp/".basename($dumpUrl);

        $options = array();

        $fh = fopen($sqlFile, 'w');
        $options = array(CURLOPT_FILE => $fh);
        


        $dbName = basename($sqlFile, ".".pathinfo($sqlFile, PATHINFO_EXTENSION));

        $this->curl($dumpUrl, 'GET', array(), array(), $options);
        CurlUtilities::ticketNeeded($ticketNeeded);

        $schemas = $this->getSchemasFromFile($sqlFile);
         
        if (empty($schemas)){
            $schemaName= self::DEFAULT_SCHEMA_NAME;
            
            $cmdBashs[] = "sed -i -e '/CREATE SCHEMA $schemaName/d' $sqlFile";  /* supprimer create schema si il exist */
            $cmdBashs[] = "sed -i -e '1 i\CREATE SCHEMA $schemaName;' $sqlFile"; /* puis ajoute create schema si il n'existe pas*/
            $cmdBashs[] = "sed -i -e 's/^SET search_path.*$/SET search_path to $schemaName, public ;/' $sqlFile"; /* ajout du schema dans SET search_path*/ 
            $cmdBashs[] = "grep 'SET search_path' $sqlFile || sed -i -e '2 i\SET search_path to $schemaName, public;' $sqlFile "; /* ajout du SET search_path si il n'existe pas */
                        
            $process = new Process($cmdBashs[0]);
            foreach($cmdBashs as $cmdBash){
                $process->setCommandLine($cmdBash);
                $process->run();

                if( !empty($process->getErrorOutput()) ) {
                //  die("EEROR " .json_encode((array)$process->getErrorOutput()));
                }
            }
        }else{
            $schemaName= $schemas[0];
        }


        $cmd     = '';
        $process = Process::fromShellCommandline($cmd);
        

        /*        if( $mode == self::ACTION_REPLACE ) {
            $this->dropDb($dbname);
        }*/
        //then import in new database
        $cmd = 'PGPASSWORD='.$TAMPON_AUTH_PWD.' createdb -h '.$this->container->getParameter('prodige_host').' -U '.$TAMPON_AUTH.' -E UTF8 -O '.$TAMPON_AUTH.' -T '.self::TAMPON_TEMPLATE.' '.$dbName;
        $process->setCommandLine($cmd);
        $process->run();
        if( !empty($process->getErrorOutput()) ) {
            //$this->dropDb($dbname);

            //return array('status' => 500, 'description' => $process->getErrorOutput());
        }
        

        $cmd = 'PGPASSWORD='.$TAMPON_AUTH_PWD.' psql -h '.$this->container->getParameter('prodige_host').' -U '.$TAMPON_AUTH.' -d '.$dbName.' -f '.$sqlFile;
       
        $process->setCommandLine($cmd);
        $process->run();

        if( !empty($process->getErrorOutput()) ) {
            //if( $mode != self::ACTION_ADD ) {
                //$this->dropDb($dbname);
            //}
            //TODO throw error
            $this->getLogger()->info(__METHOD__, (array)$process->getErrorOutput());
            //return array('status' => 500, 'description' => $process->getErrorOutput());
        }

        return array($dbName, $schemaName);
      
      
    }
    
    /**
     * PATCH standards
     * 
     * @Patch()
     * @Put()
     * 
     * #Security("has_role('ROLE_ADMIN')")
     * 
     * description="Post a new standard",
     * 
     * @Operation(
     *   tags={"Standards de standards"},
     *   summary="Patch a standard selected by id",
     *   @OA\Response(
     *      response="200",
     *      description="Succès"
     *   ),
     * )
     * 
     * @return JsonResponse
     */ 
    public function patchAction (Request $request, $standard_id=null) {
        $CATALOGUE = $this->getCatalogueConnection("catalogue"); // Doctrine\DBAL\Connection
        
        //it's required to define specifique data type
        $types = array(); 
        //all submitted values     
        $params = $request->request->all();
        // all params are the columns of table
        $keys = array_combine(array_keys($params), array_keys($params));
        //linearise as SQL SET instruction in prepared query
        $sets = str_replace("=", " = :", http_build_query($keys, null, ", "));
        
        $data = array("success"=>false);
        if ( !empty($sets) ){
            $exists = $CATALOGUE->fetchOne("select true from catalogue.standards_standard where standard_id=:standard_id", array("standard_id" => $standard_id));
            if($exists !== false){
                try {
                    $data["success"] = $CATALOGUE->executeStatement("update catalogue.standards_standard set ".$sets ." where standard_id =:standard_id ", array_merge($params, array("standard_id" => $standard_id)), $types);
                    $data["message"] = "Ce standard a été correctement modifié";
                    $data["standards"] = $params;
                } catch(\Exception $exception){
                    $data["message"] = "Une erreur s'est produite";
                    $data["exception"] = $exception->getMessage();
                }
            } else {
                $data["message"] = "Ce standard n'existe pas";
            }
        }
        return new JsonResponse($data, ($data["success"] ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST));     
    }

    /**
     * DELETE standards
     * 
     * @Delete()
     * 
     * #Security("has_role('ROLE_ADMIN')")
     * 
     * description="Delete a standard",
     * 
     * @Operation(
     *   tags={"Standards de standards"},
     *   summary="Delete a standard selected by id",
     *   @OA\Response(
     *      response="200",
     *      description="Succès"
     *   ),
     * )
     * 
     * @return JsonResponse
     */ 
    public function deleteAction(Request $request, $standard_id=null){
        $data = array("success" => false);
        //$this->deleteList();

        $CATALOGUE = $this->getCatalogueConnection("catalogue");
        
        $params = $request->request->all(); // Doctrine\DBAL\Connection     
        
        if($standard_id !=-1) {

            $metadata = $CATALOGUE->fetchAssociative("select * from catalogue.standards_standard where standard_id =:standard_id ",array("standard_id" => $standard_id)); 
            if($metadata !== false){
                $data["success"] = true;

                $CATALOGUE->fetchAssociative("delete from catalogue.standards_conformite where standard_id =:standard_id ",array("standard_id" => $standard_id)); 
                $CATALOGUE->fetchAssociative("delete from catalogue.standards_standard where standard_id =:standard_id ",array("standard_id" => $standard_id)); 
           
                $data["deleteMetadata"] = $this->deleteMetadata($metadata['fmeta_id']);

                $data["message"] = "Ce standard a été correctement supprimé";
            } else {
                $data["message"] = "Ce standard n'existe pas";
                $data["standards"] = $params;
            }          
        } else {
            $data["standards"] = $params;
            $data["message"] = "wrong standards_id";
        } 
        
        return new JsonResponse($data, ($data["success"] ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST)); 
    }

    public function deleteMetadata($metadataId){
       
        $responses = array();
        $metadata = $this->getMetadataById($metadataId);

        if( !is_array($metadata) || empty($metadata)){
            return $responses[] = 'pas de metadata trouvé';
        }
       
        $conn = $this->getCatalogueConnection('catalogue');
        
        $geonetwork = $this->getGeonetworkInterface(true);

        $this->deleteCatalogueFromMetadata($conn, $metadata['data'] );

        $jsonp = $geonetwork->deleteMetadata(null, $metadataId);

        return $responses;
    }
}

<?php

namespace ProdigeCatalogue\StandardsBundle\Controller;

use Doctrine\DBAL\Exception;
use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\Head;

use FOS\RestBundle\Controller\Annotations\QueryParam;
use OpenApi\Annotations as OA;

use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;

/**
 * Description of FournisseurController
 *
 * @Route("/standards/fournisseur", defaults={"fournisseur_id"=null}, requirements={"fournisseur_id": "^\d+$"}, options={"expose"=true})
 */
class FournisseurController extends BaseController
{

    /**
     * Restfull api to serve Entity.
     *
     * @Head("/rest/{fournisseur_id}", name="prodige_standards_fournisseur_rest")
     *
     * @param Request $request
     * @param unknown $entity
     * @param unknown $id
     * @QueryParam(name="sort", description="fields available for sorting", requirements="(-|\+)?(id|name|model)", default="fournisseur_id", nullable=true)
     * @QueryParam(name="limit", description="limit", requirements="\d+", default="-1", nullable=true)
     * @QueryParam(name="offset", description="offset", requirements="\d+", default="-1", nullable=true)
     * @QueryParam(name="callback", description="JSONP callback", default="", nullable=true)
     */
    public function restAction(Request $request, $fournisseur_id = null)
    {
        throw new NotAcceptableHttpException(
            "Cette route ne doit pas être accédée directement. Fixez la méthode d'appel à une des valeurs parmi {'GET','POST','PATCH','PUT','DELETE'}"
        );
    }

    /**
     * Get fournisseurs
     *
     * @Route("/rest/{fournisseur_id}", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Get fournisseurs configuration and property"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Returned when successful"
     * )
     * @OA\Tag(name="Fournisseurs de standards")
     *
     */
    public function getAction(Request $request, $fournisseur_id = null)
    {
        $CATALOGUE = $this->getCatalogueConnection("catalogue");

        $data = array("success" => true, "fournisseurs" => array());

        try {
            if (!$fournisseur_id) {
                $dataset = $CATALOGUE->fetchAllAssociative(
                    " SELECT * FROM catalogue.standards_fournisseur order by fournisseur_name "
                );

                $data["fournisseurs"] = $dataset;
            } else {
                $dataset = $CATALOGUE->fetchAssociative(
                    " SELECT * FROM catalogue.standards_fournisseur where fournisseur_id = :fournisseur_id",
                    compact("fournisseur_id")
                );
                $data["success"] = !empty($dataset);
                $data["fournisseurs"] = $dataset;
                if (!$data["success"]) {
                    $data["message"] = "Ce fournisseur n'existe pas";
                }
            }
        } catch (\Exception $exception) {
            $data["success"] = false;
            $data["fournisseurs"] = array();
            $data["message"] = $exception->getMessage();
        }

        return new JsonResponse($data, ($data["success"] ? Response::HTTP_OK : Response::HTTP_NOT_FOUND));
    }

    /**
     * Exemple POST
     *
     * @Route("/rest/{fournisseur_id}", methods={"POST"})
     *
     * @OA\Post(
     *     summary="Sends a new fournisseur"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Returned when successful"
     * )
     * @OA\Tag(name="Fournisseurs de standards")
     *
     * @return JsonResponse
     */
    public function postAction(Request $request)
    {
        $CATALOGUE = $this->getCatalogueConnection("catalogue"); // Doctrine\DBAL\Connection

        //it's required to define specifique data type
        $types = array();
        //all submitted values     
        $params = $request->request->all();

        // all params are the columns of table
        $keys = array_combine(array_keys($params), array_keys($params));

        $data = array("success" => false);

        try {
            $data["fournisseurs"] = $CATALOGUE->fetchAssociative(
                "insert into catalogue.standards_fournisseur (".implode(", ", $keys).") values (:".implode(
                    ", :",
                    $keys
                ).") returning *",
                array_merge($params),
                $types
            );
            $data["message"] = "Ce fournisseur a été correctement créé";
            if (!is_array($data["fournisseurs"])) {
                $data["message"] = "Une erreur s'est produite";
                $data["fournisseurs"] = array();
            } else {
                $data["success"] = true;
            }

        } catch (\Exception $exception) {
            $data["message"] = "Une erreur s'est produite";
            $data["exception"] = $exception->getMessage();
        }

        return new JsonResponse($data, ($data["success"] ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST));
    }

    /**
     * PATCH fournisseurs
     *
     * @Route("/rest/{fournisseur_id}", methods={"PUT", "PATCH"})
     *
     * @OA\Patch(
     *     summary="Patch a fournisseur selected by id"
     * )
     * @OA\Put(
     *     summary="Patch a fournisseur selected by id"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Tag(name="Fournisseurs de standards")
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function patchAction(Request $request, $fournisseur_id = null)
    {
        $CATALOGUE = $this->getCatalogueConnection("catalogue"); // Doctrine\DBAL\Connection

        //it's required to define specifique data type
        $types = array();
        //all submitted values     
        $params = $request->request->all();
        // all params are the columns of table
        $keys = array_combine(array_keys($params), array_keys($params));
        //linearise as SQL SET instruction in prepared query
        $sets = str_replace("=", " = :", http_build_query($keys, null, ", "));

        $data = array("success" => false);
        if (!empty($sets)) {
            $exists = $CATALOGUE->fetchOne(
                "select true from catalogue.standards_fournisseur where fournisseur_id=:fournisseur_id",
                array("fournisseur_id" => $fournisseur_id)
            );
            if ($exists !== false) {
                try {
                    $data["success"] = $CATALOGUE->executeStatement(
                        "update catalogue.standards_fournisseur set ".$sets." where fournisseur_id =:fournisseur_id ",
                        array_merge($params, array("fournisseur_id" => $fournisseur_id)),
                        $types
                    );
                    $data["message"] = "Ce fournisseur a été correctement modifié";
                    $data["fournisseurs"] = $params;
                } catch (\Exception $exception) {
                    $data["message"] = "Une erreur s'est produite";
                    $data["exception"] = $exception->getMessage();
                }
            } else {
                $data["message"] = "Ce fournisseur n'existe pas";
            }
        }

        return new JsonResponse($data, ($data["success"] ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST));
    }

    /**
     * DELETE fournisseurs
     *
     * @Route("/rest/{fournisseur_id}", methods={"DELETE"})
     *
     * @OA\Delete (
     *     summary="Delete a fournisseur selected by id"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Tag(name="Fournisseurs de standards")
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function deleteAction(Request $request, $fournisseur_id = null)
    {
        $data = array("success" => false);
        $CATALOGUE = $this->getCatalogueConnection("catalogue");

        $params = $request->request->all(); // Doctrine\DBAL\Connection     

        if ($fournisseur_id != -1) {
            $dataset = $CATALOGUE->fetchAssociative(
                "select * from catalogue.standards_fournisseur where fournisseur_id =:fournisseur_id ",
                array("fournisseur_id" => $fournisseur_id)
            );
            if ($dataset !== false) {
                $data["success"] = true;
                $CATALOGUE->fetchAssociative(
                    "delete from catalogue.standards_fournisseur where fournisseur_id =:fournisseur_id ",
                    array("fournisseur_id" => $fournisseur_id)
                );
                $data["message"] = "Ce fournisseur a été correctement supprimé";
            } else {
                $data["message"] = "Ce fournisseur n'existe pas";
                $data["fournisseurs"] = $params;
            }
        } else {
            $data["fournisseurs"] = $params;
            $data["message"] = "wrong fournisseurs_id";
        }

        return new JsonResponse($data, ($data["success"] ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST));
    }

    /**
     * get StandardInfo with URI
     * @param URI Unique Ressource identifier
     * @param string metadataXML ISO19139
     * @return array
     */
    public function getStandardInfo($URI, $standardXml)
    {


        $version = "1.0";
        $encoding = "UTF-8";
        $metadata_doc = new \DOMDocument($version, $encoding);
        $metadata_doc->loadXML($standardXml);
        $xpath = new \DOMXPath($metadata_doc);
        $featureCatalogs = array();
        $uuid = $title = $abstract = $dumpUrl = "";

        $objXPath = $xpath->query(
            "/gmd:MD_Metadata/gmd:contentInfo/gmd:MD_FeatureCatalogueDescription/gmd:featureCatalogueCitation"
        );
        foreach ($objXPath as $entry) {
            $featureCatalogs[] = array(
                "uuid" => $entry->getAttribute("uuidref"),
                "URI" => $entry->getAttribute("xlink:href"),
            );
        }
        $objXPath = $xpath->query(
            "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString"
        );
        if ($objXPath && $objXPath->length > 0) {
            $title = $objXPath->item(0)->nodeValue;
        }
        $objXPath = $xpath->query(
            "/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract/gco:CharacterString"
        );
        if ($objXPath && $objXPath->length > 0) {
            $abstract = $objXPath->item(0)->nodeValue;
        }
        $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:fileIdentifier/gco:CharacterString");
        if ($objXPath && $objXPath->length > 0) {
            $uuid = $objXPath->item(0)->nodeValue;
        }

        $objXPath = $xpath->query(
            "/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:linkage/gmd:URL"
        );
        foreach ($objXPath as $entry) {
            $ressource = $entry->nodeValue;
            $urlInfos = pathinfo($ressource);
            if (strtolower($urlInfos["extension"]) == "sql") {
                $dumpUrl = $ressource;
            }
        }

        $keeps = array(
            "uuid" => $uuid,
            "URI" => $URI,
            "name" => $title,
            "abstract" => $abstract,
            "catalogs" => $featureCatalogs,
            "dumpUrl" => $dumpUrl,
        );

        return $keeps;

    }

    /**
     * Search standards under Prodige Fournisseur
     *
     * @Route("/search/standards", name="prodige_standards_fournisseur_search_standards", defaults={"fournisseur_id"=null}, options={"expose"=true})
     *
     *
     * @return JsonResponse
     */
    public function searchStandards(Request $request)
    {

        $fournisseurService = $request->get('fournisseur');

        try {

            $ticketNeeded = \Prodige\ProdigeBundle\Services\CurlUtilities::ticketNeeded();
            \Prodige\ProdigeBundle\Services\CurlUtilities::ticketNeeded(false);
            $keeps = array();
            $fournisseurJson = $this->curl($fournisseurService);
            $fournisseur = json_decode($fournisseurJson);
            foreach ($fournisseur as $key => $standard) {

                if (!isset($standard->URI) || !isset($standard->Name)) {
                    $data["success"] = false;
                    $data["fournisseurs"] = array();
                    $data["message"] = "le service fournisseur n'est pas dans le format atttendu";

                    return $data;
                }
                $standardXml = $this->curl($standard->URI);

                $keeps[] = $this->getStandardInfo($standard->URI, $standardXml);

            }

            \Prodige\ProdigeBundle\Services\CurlUtilities::ticketNeeded($ticketNeeded);

        } catch (\Exception $exception) {
            $data["success"] = false;
            $data["fournisseurs"] = array();
            $data["message"] = $exception->getMessage();
        }

        return new JsonResponse(array("success" => true, "metadata" => $keeps));
    }
}

/**
 *
 */

// Globals
var fieldName_global = '';
var json_enums_global = '';
var enum_name_global = '';
var backUrl_global = '';

init = function(tableName, backUrl) {
  backUrl_global = backUrl;
  fCreateFormPanelStep1(tableName, true);
}

//-----------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------

fCreateFormPanelStep1 = function(tableName, isReadOnly) {

  var cmb_geometryType_store = Ext.create('Ext.data.Store', {
                                 fields: ['text', 'value'],
                                 data:   [
                                          { 'text': 'point', 'value': 'POINT' },
                                          { 'text': 'line','value': 'LINE' },
                                          { 'text': 'polygone','value': 'POLYGONE' }
                                         ]
                               });

  var cmb_projectionsValues_store = Ext.create('Ext.data.Store', {
                                      fields: ['text', 'value'],
                                      data:   []
                                    });

  Ext.Ajax.request({
    url: Routing.generate('prodige_getProjections')/*'../../PRRA/Services/getProjections.php'*/,
    method: 'POST',
    success: function(response) {
      var response = Ext.decode(response.responseText);
      //console.log('response is : ', response);
      for(var i=0 ; i<response.length ; i++) {
        cmb_projectionsValues_store.add(
                                        { 'text': response[i]['display'],'value': response[i]['epsg'] }
                                       );
      }
    },
    failure: function(response) {
      //
    }
  });

  var formPanelStep1 = Ext.create('Ext.form.Panel', {
        id : 'CreationFormPanelStep1_id',
        frame : true,
        title : 'Création d\'une donnée (étape 1)',
        //labelWidth : 150,
        width: 400,
        heigth: 150,
        bodyPadding: 10,
        renderTo: 'form_panel_to_render', // id for a html div
        fieldDefaults : {
          labelAlign : 'right'
        },
        items :
        [
         {
           id:'txt_TableName_id',
           xtype: 'textfield',
           name: 'TableName_name',
           fieldLabel:'Nom de la table',
           value: (tableName) ? tableName:'',
           minLength: 3,
           maxLength: 55,
           vtype: 'alphanum',
           readOnly: isReadOnly ? true:false
         },
         {
           id:'nbr_FieldNumber_id',
           xtype:'numberfield',
           name: 'FieldNumber_name',
           fieldLabel:'Nombre de champs',
           minValue: 1,
           maxValue: 99,
           hideTrigger: true
         },
         {
           id:'cmb_GeometryType_id',
           xtype: 'combo',
           name : 'GeometryType_name',
           fieldLabel:'Type de la géométrie',
           store: cmb_geometryType_store,
           queryMode: 'local', //important
           allowBlank: false,
           editable: false,
           valueField: 'value',
           displayField:'text'
         },
         {
           id:'cmb_Projection_id',
           xtype: 'combo',
           name : 'Projection_name',
           fieldLabel:'Projection',
           store: cmb_projectionsValues_store,
           queryMode: 'local', //important
           allowBlank: false,
           editable: false,
           valueField: 'value',
           displayField:'text'
         }
        ],
        buttons : [
                   {
                     id : 'btn_NextStep_id',
                     text : 'Etape suivante',
                     listeners: {
                       'click': {
                         fn: function() {
                           if((Ext.getCmp('txt_TableName_id').getValue().trim() !== '') && (Ext.getCmp('nbr_FieldNumber_id').getValue() > 0) && (Ext.getCmp('nbr_FieldNumber_id').getValue() < 100) && (Ext.getCmp('cmb_GeometryType_id').getValue() != null) && (Ext.getCmp('cmb_Projection_id').getValue() != null)) {
                             fieldName_global = Ext.getCmp('txt_TableName_id').getValue();
                             var numberOfFields = Ext.getCmp('nbr_FieldNumber_id').getValue();
                             Ext.Ajax.request({
                               url: 'viewServices.php',
                               method: 'POST',
                               params: {
                                 action: 'createTable',
                                 fieldName: fieldName_global,
                                 geometryType: Ext.getCmp('cmb_GeometryType_id').getValue(),
                                 projectionsValue: Ext.getCmp('cmb_Projection_id').getValue()
                               },
                               success: function(response) {
                                 var response = Ext.decode(response.responseText);
                                 //console.log('response is : ', response);
                                 if(response.createTable) {
                                   formPanelStep1.destroy();
                                   fCreateFormPanelStep2(fieldName_global, numberOfFields);
                                 }
                                 else {
                                   Ext.Msg.alert('Erreur', 'La table n\'a été crée');
                                 }
                               },
                               failure: function(response) {
                                 Ext.Msg.alert('Erreur', 'La table n\'a été crée');
                               }
                             });
                           }
                           else {
                             Ext.Msg.alert('Attention', 'Attention aux valeurs saisies ...');
                           }
                         }
                       }
                     }
                   },
                   {
                     id : 'btn_Cancel_id',
                     text : 'Annuler',
                     listeners : {
                       'click': {
                         fn:function() {
                           formPanelStep1.destroy();
                           document.location.href = backUrl_global;
                         }
                       }
                     }
                   }
                 ]
  });
}

//-----------------------------------------------------------------------------------------------------------------------------
var gPanelStep2_store = Ext.create('Ext.data.Store', {
fields:['FieldName', 'Type', 'VarcharLength', 'Enum', 'DefaultValue', 'Comment', 'Mandatory'],
data: [
   { 'FieldName': 'gid', 'Type':'real',  'VarcharLength':'', 'Enum':'', 'DefaultValue':'', 'Comment':'', 'Mandatory':'Oui'},
   { 'FieldName': 'the_geom', 'Type':'geometry',  'VarcharLength':'', 'Enum':'', 'DefaultValue':'', 'Comment':'', 'Mandatory':'Oui'}
]
});

var enum_store_step2 = Ext.create('Ext.data.Store', {
  fields: ['text', 'value'],
  data: []
});

var enum_values_store = Ext.create('Ext.data.Store', {
  fields: ['text', 'value'],
  data: []
});

fGetAllEnum = function(enum_store) {

  json_enums_global = '';

  Ext.Ajax.request({
    url: 'viewServices.php',
    method: 'POST',
    params: {
      action: 'getAllEnum',
    },
    success: function(response) {
      json_enums_global = Ext.decode(response.responseText);
      if(json_enums_global.getAllEnum) {
        var data = json_enums_global.allEnums;
        for(var i=0 ; i<data.length; i++) {
          enum_store.add({'text': data[i]['enum_name'], 'value': data[i]['enum_name'] });
        }
      }
      else {
        console.log('problem in getAllEnum');
      }

    },
    failure: function(response) {
      console.log('Failure : action = getAllEnum - response is : ', response.responseText);
    }
  });
}

fRefreshEnumStore =  function(enum_store) {
  json_enums_global = '';
  enum_store.removeAll();
  enum_store.add({'text':'Créer énumération', 'value':'enum'});
  fGetAllEnum(enum_store);
}

fRefreshEnumValuesStore = function(enum_name, values_store) {

  var data = json_enums_global.allEnums;
  if(data.length > 0) {
    var str_enum_values = '';
    var arr_enum_values = [];
    for(var i=0 ; i<data.length; i++) {
      if(data[i]['enum_name'] == enum_name) {
        str_enum_values = data[i]['enum_value'];
        break;
      }
    }
    arr_enum_values = str_enum_values.split(',');
    for(var i=0 ; i<arr_enum_values.length; i++) {
      values_store.add({'text': arr_enum_values[i], 'value': arr_enum_values[i] });
    }
  }
  return arr_enum_values;
}

fIsValide = function(gPanelStore) {
  var fieldsNameArray = [];
  for(var i=0; i<gPanelStore.data.length; i++) {
    if(fieldsNameArray.indexOf(gPanelStore.getAt(i).data['FieldName']) == -1 ) {
      fieldsNameArray.push(gPanelStore.getAt(i).data['FieldName']);
    }
  }
  return (fieldsNameArray.length == gPanelStore.data.length) ? true : false;
}

fCreateFormPanelStep2 = function(fieldName, nbrOfField) {

  for(var i=0; i<nbrOfField; i++) {
    gPanelStep2_store.add({ 'FieldName': '', 'Type':'',  'VarcharLength':'', 'Enum':'', 'DefaultValue':'', 'Comment':'', 'Mandatory':''});
  }

  var combo_store = Ext.create('Ext.data.Store', {
    fields: ['text', 'value'],
    data: [
           {'text':'bigint', 'value':'bigint'},
           {'text':'real', 'value':'real'},
           {'text':'varchar', 'value':'varchar'},
           {'text':'date', 'value':'date'},
           {'text':'enum', 'value':'enum'}
          ]
  });

  fRefreshEnumStore(enum_store_step2);

  var formPanelStep2 = Ext.create('Ext.form.Panel', {
    id : 'CreationFormPanelStep2_id',
    frame : true,
    title : 'Création d\'une donnée (étape 2)',
    width: 1065,
    //heigth: 550,
    bodyPadding: 10,
    renderTo: 'form_panel_to_render', // id for a html div
    items :
    [
     {
       // Label
       id: 'lbl_TableName_id',
       xtype: 'label',
       html: 'Nom de la table : ' + fieldName +  '</br>'
     },
     {
       // Label
       id: 'lbl_NumberOfField_id',
       xtype: 'label',
       html: '</br>Nombre de champs : ' + nbrOfField + '</br>'
     },
     {
       //gridPanel
       xtype: 'gridpanel',
       id: 'GridPanelStep2_id',
       width: 1050,
       //height: 535,
       store: gPanelStep2_store,
       columns: [
                 {
                   id: 'FieldName_id',
                   header: 'Nom champ',
                   width: 150,
                   sortable: true,
                   dataIndex: 'FieldName',
                   xtype : 'widgetcolumn',
                   onWidgetAttach : function(widget, record, column) {
                     var dataIndex = 'FieldName';
                     var isReadOnly = ['gid', 'the_geom'].indexOf(record.get(dataIndex)) != -1;
                     if(isReadOnly) {
                       var readonly = widget.down('[name=readonly]');
                       readonly.setVisible(true);
                       readonly.setValue(record.get(dataIndex));
                       Ext.each(widget.query(':not([name=readonly])'), function(cmp){
                         cmp.setVisible(false);
                      });
                       return;
                     }
                     // TODO
                     // begin 
                     widget.items.each(function(cmp) {
                       cmp.on('change', function() {
                         record.set(dataIndex, widget.getValue());
                         record.commit();
                       });
                     }); // end
                   },
                   widget : {
                     xtype : 'fieldcontainer',
                     defaultBindProperty : 'value',
                     value : null,
                     setValue : function(value) {
                       var activeField = this.down('[name=varchar]');
                       if (activeField && activeField.isVisible()){
                         activeField.setVisible(true);
                         activeField.setValue(this.value);
                       }
                     },
                     getValue : function(value) {
                       var activeField = this.down('[name=varchar]');
                       if(activeField && activeField.isVisible()){
                         return this.value = activeField.getValue();
                       }
                       var readonlyField = this.down('[name=readonly]');
                       if (readonlyField && readonlyField.isVisible() ){
                         return this.value = readonlyField.getValue();
                       }
                       return this.value;
                     },
                     items : [
                              {
                                xtype: 'displayfield',
                                name : 'readonly',
                                width: 135,
                                hidden : true
                              },
                              {
                                xtype: 'textfield',
                                name : 'varchar',
                                width: 135,
                                minLength: 3,
                                maxLength: 63,
                                vtype: 'alphanum'
                              }
                             ]
                   }
                 },
                 {
                   id: 'Type_id',
                   header: 'Type',
                   width: 150,
                   sortable: true,
                   dataIndex: 'Type',
                   xtype : 'widgetcolumn',
                   onWidgetAttach : function(widget, record, column) {
                     var dataIndex = 'FieldName';
                     var isReadOnly = ['gid', 'the_geom'].indexOf(record.get(dataIndex))!=-1;
                     if(isReadOnly){
                       var readonly = widget.down('[name=readonly]');
                       readonly.setVisible(true);
                       readonly.setValue(record.get('Type'));
                       Ext.each(widget.query(':not([name=readonly])'), function(cmp){
                         cmp.setVisible(false);
                      });
                       return;
                     }
                     // TODO
                     // begin 
                     widget.items.each(function(cmp){
                       cmp.on('change', function(){
                         record.set('Type', widget.getValue());
                         record.commit();
                       });
                     }); // end
                   },
                   widget : {
                     xtype : 'fieldcontainer',
                     defaultBindProperty : 'value',
                     value : null,
                     setValue : function(value) {
                       var activeField = this.down('[name=combo]');
                       if(activeField){
                         activeField.setValue(this.value);
                         activeField.setVisible(true);
                       }
                     },
                     getValue : function(value) {
                       var activeField = this.down('[name=combo]');
                       if(activeField && activeField.isVisible()){
                         return this.value = activeField.getValue();
                       }
                       var readonlyField = this.down('[name=readonly]');
                       if (readonlyField && readonlyField.isVisible() ){
                         return this.value = readonlyField.getValue();
                       }
                       return this.value;
                     },
                     items : [
                              {
                                xtype: 'displayfield',
                                name : 'readonly',
                                width: 135,
                                hidden : true
                              },
                              {
                                xtype: 'combo',
                                name : 'combo',
                                store: combo_store,
                                queryMode: 'local', //important
                                valueField: 'value',
                                displayField:'text',
                                width: 135,
                                hidden : true
                              }
                             ]
                   }
                 },
                 {
                   id: 'VarcharLength_id',
                   header: 'Longueur/Liste',
                   width: 150,
                   sortable: true,
                   dataIndex: 'VarcharLength',
                   xtype : 'widgetcolumn',
                   onWidgetAttach : function(widget, record, column) {
                     var dataIndex = 'FieldName';
                     var isReadOnly = ['gid', 'the_geom'].indexOf(record.get(dataIndex))!=-1;
                     if(isReadOnly){
                       var readonly = widget.down('[name=readonly]');
                       readonly.setVisible(true);
                       Ext.each(widget.query(':not([name=readonly])'), function(cmp){
                         cmp.setVisible(false);
                      });
                       return;
                     }
                     //return;
                     // TODO
                     // begin 
                     widget.items.each(function(cmp){
                       cmp.on('change', function(){
                         record.set('VarcharLength', widget.getValue());
                         record.commit();
                       });
                     }); // end
                   },
                   widget : {
                     xtype : 'fieldcontainer',
                     defaultBindProperty : 'value',
                     value : null,
                     setValue : function(value) {
                       var record = this.getWidgetRecord();
                       if(!record) {
                         return;
                       }
                       var currentType = this.getWidgetRecord().get('Type');
                       var isNotEnumMode = ['readonly' , 'real', 'bigint', 'geometry', 'date', 'varchar'].indexOf(currentType) != -1;
                       if(isNotEnumMode) {
                         var activeField = this.down('[name=' + currentType + ']');
                         if (activeField){
                           activeField.setValue(this.value);
                           activeField.setVisible(true);
                         }
                         var inactiveFields = this.query(':not([name=' + currentType + '])');
                         Ext.each(inactiveFields, function(inactiveField){
                           inactiveField.setVisible(false);
                         });
                       }
                     },
                     getValue : function(value) {
                       var currentType = this.getWidgetRecord().get('Type');
                       var activeField = this.down('[name=' + currentType + ']');
                       if(activeField && activeField.isVisible()){
                         return this.value = activeField.getValue();
                       }
                       var readonlyField = this.down('[name=readonly]');
                       if ( readonlyField && readonlyField.isVisible() ){
                         return this.value = readonlyField.getValue();
                       }
                       return this.value;
                     },
                     items : [
                              {
                                xtype: 'displayfield',
                                name : 'readonly',
                                width: 135,
                                hidden : true
                              },
                              {
                                xtype : 'numberfield',
                                name : 'varchar',
                                minValue: 1,
                                maxValue: 1000,
                                width: 135,
                                hidden : true
                              },
                             ]
                   }
                 },
                 {
                   id: 'Enum_id',
                   header: 'Enumération',
                   width: 150,
                   dataIndex: 'Enum',
                   xtype : 'widgetcolumn',
                   onWidgetAttach : function(widget, record, column) {
                     var dataIndex = 'FieldName';
                     var isReadOnly = ['gid', 'the_geom'].indexOf(record.get(dataIndex)) != -1;
                     if(isReadOnly){
                       var readonly = widget.down('[name=readonly]');
                       readonly.setVisible(true);
                       Ext.each(widget.query(':not([name=readonly])'), function(cmp){
                         cmp.setVisible(false);
                      });
                       return;
                     }
                     // TODO
                     // begin 
                     widget.items.each(function(cmp){
                       cmp.on('change', function(){
                         record.set('Enum', widget.getValue());
                         record.commit();
                       });
                     }); // end
                   },
                   widget : {
                     xtype : 'fieldcontainer',
                     defaultBindProperty : 'value',
                     value : null,
                     setValue : function(value) {
                       var record = this.getWidgetRecord();
                       if (!record) {
                         return;
                       }
                       var currentType = this.getWidgetRecord().get('Type');
                       var activeField = this.down('[name=' + currentType + ']');
                       if (activeField) {
                         activeField.setValue(this.value);
                         activeField.setVisible(true);
                       }
                       var inactiveFields = this.query(':not([name=' + currentType + '])');
                       Ext.each(inactiveFields, function(inactiveField){
                         inactiveField.setVisible(false);
                       });
                     },
                     getValue : function(value) {
                       var currentType = this.getWidgetRecord().get('Type');
                       var activeField = this.down('[name=' + currentType + ']');
                       if(activeField && activeField.isVisible()){
                         return this.value = activeField.getValue();
                       }
                       var readonlyField = this.down('[name=readonly]');
                       if ( readonlyField && readonlyField.isVisible() ){
                         return this.value = readonlyField.getValue();
                       }
                       return this.value;
                     },
                     items : [
                              {
                                xtype: 'displayfield',
                                name : 'readonly',
                                width: 135,
                                hidden : true
                              },
                              {
                                xtype: 'combo',
                                name : 'enum',
                                store: enum_store_step2,
                                queryMode: 'local', //important
                                valueField: 'value',
                                displayField:'text',
                                width: 135,
                                hidden : true,
                                listeners : {
                                  'select' : {
                                    fn: function(combo, record, eOpts) {
                                      //console.log('combo.getValue() is : ', combo.getValue());
                                      enum_values_store.removeAll();
                                      if(combo.getValue() != "enum") {
                                        enum_name_global = combo.getValue();
                                        var values = fRefreshEnumValuesStore(enum_name_global, enum_values_store);
                                      }
                                      fCreateFormPanelStep3(enum_name_global, values);
                                    }
                                  }
                                }
                              }
                             ]
                   }
                 },
                 {
                   id: 'DefaultValue_id',
                   header: 'Valeur par défaut',
                   width: 150,
                   sortable: true,
                   dataIndex: 'DefaultValue',
                   xtype : 'widgetcolumn',
                   onWidgetAttach : function(widget, record, column) {
                     var dataIndex = 'FieldName';
                     var isReadOnly = ['gid', 'the_geom'].indexOf(record.get(dataIndex))!=-1;
                     if(isReadOnly){
                       var readonly = widget.down('[name=readonly]');
                       readonly.setVisible(true);
                       Ext.each(widget.query(':not([name=readonly])'), function(cmp){
                         cmp.setVisible(false);
                      });
                       return;
                     }
                     // TODO
                     // begin 
                     widget.items.each(function(cmp){
                       cmp.on('change', function(){
                         record.set('DefaultValue', widget.getValue());
                         record.commit();
                       });
                     }); // end
                   },
                   widget : {
                     xtype : 'fieldcontainer',
                     defaultBindProperty : 'value',
                     value : null,
                     setValue : function(value){
                       var record = this.getWidgetRecord();
                       if (!record) {
                         return;
                       }
                       var currentType = this.getWidgetRecord().get('Type');
                       var activeField = this.down('[name=' + currentType + ']');
                       if (activeField){
                         activeField.setValue(this.value);
                         activeField.setVisible(true);
                       }
                       var inactiveFields = this.query(':not([name=' + currentType + '])');
                       Ext.each(inactiveFields, function(inactiveField){
                         inactiveField.setVisible(false);
                       });
                     },
                     getValue : function(value){
                       var currentType = this.getWidgetRecord().get('Type');
                       var activeField = this.down('[name=' + currentType + ']');
                       if(activeField && activeField.isVisible()){
                         return this.value = activeField.getValue();
                       }
                       var readonlyField = this.down('[name=readonly]');
                       if ( readonlyField && readonlyField.isVisible() ){
                         return this.value = readonlyField.getValue();
                       }
                       return this.value;
                     },
                     items : [
                              {
                                xtype: 'displayfield',
                                name : 'readonly',
                                width: 135,
                                hidden : true
                              },
                              {
                                xtype : 'textfield',
                                name : 'varchar',
                                width: 135,
                                hidden : true
                              },
                              {
                                xtype : 'numberfield',
                                name : 'real',
                                minValue: 0,
                                width: 135,
                                hidden : true
                              },
                              {
                                xtype : 'numberfield',
                                name : 'bigint',
                                minValue: 0,
                                width: 135,
                                hidden : true
                              },
                              {
                                xtype : 'datefield',
                                name : 'date',
                                width: 135,
                                hidden : true
                              },
                              {
                                xtype: 'combo',
                                name : 'enum',
                                store: enum_values_store,
                                queryMode: 'local', //important
                                valueField: 'value',
                                displayField:'text',
                                width: 135,
                                hidden : true
                              }
                             ]
                   }
                 },
                 {
                   id: 'Comment_id',
                   header: 'Commentaire',
                   width: 150,
                   sortable: true,
                   dataIndex: 'Comment',
                   xtype : 'widgetcolumn',
                   onWidgetAttach : function(widget, record, column){
                     var dataIndex = 'FieldName';
                     var isReadOnly = ['gid', 'the_geom'].indexOf(record.get(dataIndex))!=-1;
                     if(isReadOnly){
                       var readonly = widget.down('[name=readonly]');
                       readonly.setVisible(true);
                       Ext.each(widget.query(':not([name=readonly])'), function(cmp){
                         cmp.setVisible(false);
                      });
                       return;
                     }
                     // TODO
                     // begin 
                     widget.items.each(function(cmp){
                       cmp.on('change', function(){
                         record.set('Comment', widget.getValue());
                         record.commit();
                       });
                     }); // end
                   },
                   widget : {
                     xtype : 'fieldcontainer',
                     defaultBindProperty : 'value',
                     value : null,
                     setValue : function(value){
                       var record = this.getWidgetRecord();
                       if (!record) {
                         return;
                       }
                       var currentType = this.getWidgetRecord().get('Type');
                       if(currentType) {
                         var activeField = this.down('[name=varchar]');
                         if (activeField){
                           activeField.setValue(this.value);
                           activeField.setVisible(true);
                         }
                         var inactiveFields = this.query(':not([name=varchar])');
                         Ext.each(inactiveFields, function(inactiveField){
                           inactiveField.setVisible(false);
                         });
                       }
                     },
                     getValue : function(value){
                       var activeField = this.down('[name=varchar]');
                       if(activeField && activeField.isVisible()){
                         return this.value = activeField.getValue();
                       }
                       var readonlyField = this.down('[name=readonly]');
                       if ( readonlyField && readonlyField.isVisible() ){
                         return this.value = readonlyField.getValue();
                       }
                       return this.value;
                     },
                     items : [
                              {
                                xtype: 'displayfield',
                                name : 'readonly',
                                width: 135,
                                hidden : true
                              },
                              {
                                xtype : 'textfield',
                                name : 'varchar',
                                width: 135,
                                hidden : true
                              }
                             ]
                   }
                 },
                 {
                   id: 'Mandatory_id',
                   header: 'Obligatoire',
                   width: 148,
                   sortable: true,
                   dataIndex: 'Mandatory',
                   xtype : 'widgetcolumn',
                   onWidgetAttach : function(widget, record, column){
                     var dataIndex = 'FieldName';
                     var isReadOnly = ['gid', 'the_geom'].indexOf(record.get(dataIndex))!=-1;
                     if(isReadOnly){
                       var readonly = widget.down('[name=readonly]');
                       readonly.setVisible(true);
                       readonly.setValue(record.get('Mandatory'));
                       Ext.each(widget.query(':not([name=readonly])'), function(cmp){
                         cmp.setVisible(false);
                      });
                       return;
                     }
                     // TODO
                     // begin 
                     widget.items.each(function(cmp){
                       cmp.on('change', function(){
                         record.set('Mandatory', widget.getValue());
                         record.commit();
                       });
                     }); // end
                   },
                   widget : {
                     xtype : 'fieldcontainer',
                     defaultBindProperty : 'value',
                     value : null,
                     setValue : function(value){
                       var record = this.getWidgetRecord();
                       if (!record) {
                         return;
                       }
                       var currentType = this.getWidgetRecord().get('Type');
                       if(currentType) {
                         var activeField = this.down('[name=check]');
                         if (activeField){
                           activeField.setValue(this.value);
                           activeField.setVisible(true);
                         }
                         var inactiveFields = this.query(':not([name=check])');
                         Ext.each(inactiveFields, function(inactiveField){
                           inactiveField.setVisible(false);
                         });
                       }

                     },
                     getValue : function(value){
                       var activeField = this.down('[name=check]');
                       if(activeField && activeField.isVisible()){
                         return this.value = activeField.getValue();
                       }
                       var readonlyField = this.down('[name=readonly]');
                       if ( readonlyField && readonlyField.isVisible() ){
                         return this.value = readonlyField.getValue();
                       }
                       return this.value;
                     },
                     items : [
                              {
                                xtype: 'displayfield',
                                name : 'readonly',
                                width: 135,
                                hidden : true
                              },
                              {
                                xtype : 'checkbox',
                                name : 'check',
                                boxLabel:'Oui',
                                width: 135,
                                hidden : true
                              }
                             ]
                   }

                 }
       ],
     }
    ],
    buttons : [
               {
                 id : 'btn_SaveStep2_id',
                 text : 'Enregistrer',
                 listeners : {
                   'click' : {
                     fn : function() {
                       //
                       if(fIsValide(gPanelStep2_store)) {
                         //
                         var fieldsToAddArray = [];
                         for(var i=0; i<gPanelStep2_store.data.length; i++) {
                           fieldsToAddArray.push(
                               {
                                 'FieldName': gPanelStep2_store.getAt(i).data['FieldName'],
                                 'Type': gPanelStep2_store.getAt(i).data['Type'],
                                 'VarcharLength': gPanelStep2_store.getAt(i).data['VarcharLength'],
                                 'EnumName': gPanelStep2_store.getAt(i).data['Enum'],
                                 'DefaultValue': gPanelStep2_store.getAt(i).data['DefaultValue'],
                                 'Comment': gPanelStep2_store.getAt(i).data['Comment'],
                                 'Mandatory': gPanelStep2_store.getAt(i).data['Mandatory']
                               }
                           );
                         }
                         //console.log('fieldsToAddArray is : ', fieldsToAddArray);
                         Ext.Ajax.request({
                           url: 'viewServices.php',
                           method: 'POST',
                           params: {
                             action: 'alterTable',
                             tableName: fieldName_global,
                             fieldsToAdd: JSON.stringify(fieldsToAddArray)
                           },
                           success: function(response) {
                             var response = Ext.decode(response.responseText);
                             //console.log('response is : ', response);
                             if(response.alterTable) {
                               //Ext.getCmp('btn_SaveStep2_id').setDisabled(true);
                               document.location.href = backUrl_global;
                               //Ext.Msg.alert('Réussi', 'L\ajout des champs à la table a réussi');
                             }
                             else {
                               Ext.Msg.alert('Echec', 'L\ajout des champs à la table n\'a pas réussi');
                             }
                           },
                           failure: function(response) {
                             Ext.Msg.alert('Echec', 'L\ajout des champs à la table n\'a pas réussi');
                           }
                       });
                       }
                       else {
                         Ext.Msg.alert('Attention', 'Les noms des champs doivent être rempli et leurs noms vont être unique');
                       }
                     }
                   }
                 }
               },
               {
                 id : 'btn_CancelStep2_id',
                 text : 'Annuler',
                 listeners: {
                   'click': {
                     fn: function() {
                       Ext.Ajax.request({
                         url: 'viewServices.php',
                         method: 'POST',
                         params: {
                           action: 'dropTable',
                           tableName: fieldName_global
                         },
                         success: function(response) {
                           var response = Ext.decode(response.responseText);
                           //console.log('response is : ', response);
                           if(response.dropTable) {
                             formPanelStep2.destroy();
                             gPanelStep2_store.removeAll();
                             //fCreateFormPanelStep1('', false);
                             document.location.href = backUrl_global;
                           }
                           else {
                             Ext.Msg.alert('Echec', 'L\annulation n\'a pas réussi');
                           }
                         },
                         failure: function(response) {
                           Ext.Msg.alert('Echec', 'L\annulation n\'a pas réussi');
                         }
                       });
                     }
                   }
                 }
               }
              ]
  });
}

// -----------------------------------------------------------------------------------------------------------------------------

fCreateFormPanelStep3 = function(enum_name, enums_values) {

  var enum_store_step3 =  Ext.create('Ext.data.Store', {
    fields:['Value', 'DeleteAction']
  });

  if((typeof enums_values !== 'undefined' && enums_values && enums_values.constructor === Array)) {
    for(var i=0; i<enums_values.length; i++) {
      enum_store_step3.add({'Value': enums_values[i], 'DeleteAction': 'readOnly'});
    }
  }

  var formPanelEnum = Ext.create('Ext.form.Panel', {
    id : 'CreationFormPanelEnum_id',
    frame : true,
    //labelWidth : 150,
    width: 400,
    heigth: 300,
    bodyPadding: 10,
    fieldDefaults : {
      labelAlign : 'right'
    },
    items :
    [
     {
       id:'txt_EnumName_id',
       xtype: 'textfield',
       name: 'EnumName_name',
       fieldLabel:'Nom de la liste',
       value:(enum_name !== '') ? enum_name:'',
       readOnly:(enum_name !== '') ? true:false
     },
     {
       //gridPanel
       xtype: 'gridpanel',
       id: 'GridPanelEnum_id',
       //width: 550,
       height: 250,
       store: enum_store_step3,
       hideHeaders: true,
       tbar: [
              {
                xtype: 'displayfield',
                value: 'Valeur',
                width: 175
              },
              {
                xtype: 'button',
                text: 'Ajouter',
                width: 135,
                handler : function(btn){
                  enum_store_step3.add({'Value':'', 'DeleteAction':'Supprimer'});
                }
              }
             ],
       columns: [
                 {
                   id: 'Value_id',
                   width: 175,
                   dataIndex: 'Value',
                   xtype : 'widgetcolumn',
                   onWidgetAttach : function(widget, record, column){
                     if(record.get('DeleteAction') == 'readOnly') {
                       var readOnly = widget.down('[name=readOnly]');
                       readOnly.setValue(record.get('Value'));
                       readOnly.setVisible(true);
                       Ext.each(widget.query(':not([name=readOnly])'), function(cmp){
                         cmp.setVisible(false);
                       });
                       return;
                     }
                     //
                     // TODO
                     // begin
                     widget.items.each(function(cmp){
                       cmp.on('change', function(){
                         record.set('Value', widget.getValue());
                         record.commit();
                       });
                     }); // end
                   },
                   widget : {
                     xtype : 'fieldcontainer',
                     defaultBindProperty : 'value',
                     value : null,
                     setValue : function(value){
                       var activeField = this.down('[name=varchar]');
                       if(activeField){
                         activeField.setValue(this.value);
                         activeField.setVisible(true);
                       }
                       var inactiveFields = this.query(':not([name=varchar])');
                       Ext.each(inactiveFields, function(inactiveField){
                         inactiveField.setVisible(false);
                       });
                     },
                     getValue : function(value){
                       var activeField = this.down('[name=varchar]');
                       if(activeField && activeField.isVisible()){
                         return this.value = activeField.getValue();
                       }
                       var readonlyField = this.down('[name=readonly]');
                       if (readonlyField && readonlyField.isVisible()){
                         return this.value = readonlyField.getValue();
                       }
                       return this.value;
                     },
                     items : [
                              {
                                xtype: 'displayfield',
                                name: 'readOnly',
                                width: 150,
                                hidden: true
                              },
                              {
                                xtype: 'textfield',
                                name : 'varchar',
                                width: 150,
                                minLength: 3,
                                maxLength: 100,
                                value: 'Valeur',
                                vtype: 'alphanum',
                                hidden: true
                              }
                             ]
                   }
                 },
                 {
                   id: 'DeleteAction_id',
                   width: 175,
                   dataIndex: 'DeleteAction',
                   xtype : 'widgetcolumn',
                   onWidgetAttach : function(widget, record, column){
                     if(record.get('DeleteAction') == 'readOnly') {
                       widget.setVisible(false);
                     }
                     else {
                       widget.setVisible(true);
                     }
                     return;
                   },
                   widget : {
                             xtype : 'button',
                             text: 'Supprimer',
                             width: 150,
                             handler : function(btn){
                               enum_store_step3.removeAt(enum_store_step3.data.length - 1, 1);
                             }
                           }
                 }
                ]
     }
    ],
    buttons : [
               {
                 id : 'btn_SaveEnum_id',
                 text : 'Enregistrer',
                 width: 85,
                 listeners : {
                   'click': {
                     fn: function() {
                       var enumValuesToAdd = [];
                       for(var i=0; i<enum_store_step3.data.length; i++) {
                         if(enum_store_step3.getAt(i).data['DeleteAction'] != 'readOnly') {
                           enumValuesToAdd.push(enum_store_step3.getAt(i).data['Value']);
                         }
                       }
                       Ext.Ajax.request({
                         url: 'viewServices.php',
                         method: 'POST',
                         params: {
                           enumValuesToAdd: JSON.stringify(enumValuesToAdd),
                           action: (enum_name_global !== '') ? 'alterEnum':'createEnum',
                           enum: Ext.getCmp('txt_EnumName_id').getValue()
                         },
                         success: function(response) {
                           var response = Ext.decode(response.responseText);
                           //console.log('response is : ', response);
                           if(response.createEnum || response.alterEnum) {
                             //Ext.getCmp('btn_SaveEnum_id').setDisabled(true);
                             enum_name_global = '';
                             fRefreshEnumStore(enum_store_step2);
                             popupEnum.close();
                           }
                           else {
                             Ext.Msg.alert('Echec', 'Echec ...');
                           }
                         },
                         failure: function(response) {
                           console.log('Failure - create/alter enumeration - reposne is : ', response.responseText);
                         }
                     });
                     }
                   }
                 }
               },
               {
                 id : 'btn_DeleteEnum_id',
                 text : 'Supprimer',
                 width: 85,
                 listeners: {
                   'click': {
                     fn: function() {
                       //
                       Ext.Ajax.request({
                         url: 'viewServices.php',
                         method: 'POST',
                         params: {
                           action: 'deleteEnum',
                           enum: Ext.getCmp('txt_EnumName_id').getValue()
                         },
                         success: function(response) {
                           var response = Ext.decode(response.responseText);
                           //console.log('response is : ', response);
                           if(response.deleteEnum) {
                             enum_name_global = '';
                             fRefreshEnumStore(enum_store_step2);
                             enum_values_store.removeAll();
                             enum_store_step3.removeAll();
                             popupEnum.close();
                           }
                           else {
                             Ext.Msg.alert('Echec', 'Echec dans la suppression de l\'énumération');
                           }
                         },
                         failure: function(response) {
                           console.log('Failure - delete enumeration - reposne is : ', response.responseText);
                         }
                     });
                     }
                   }
                 }
               },
               {
                 id : 'btn_Close_id',
                 text : 'Fermer',
                 width: 85,
                 listeners: {
                   'click': {
                     fn: function() {
                       //fRefreshComboStore(combo_store);
                       enum_name_global = '';
                       enum_store_step3.removeAll();
                       popupEnum.close();
                     }
                   }
                 }
               }
             ]
  });

  var popupEnum = Ext.create('Ext.window.Window', {
    title: 'Gestion liste de valeurs',
    width: 425,
    height: 425,
    items: formPanelEnum
  });
  popupEnum.show();
}
//-----------------------------------------------------------------------------------------------------------------------------
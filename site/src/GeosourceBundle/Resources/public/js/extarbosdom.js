var treePanel = null;
var clsSDom = {'rubric' : {}, 'dom' : {}};
function affectStyleNode(node){
	var id = node.id.replace(/\D/g, '');
	var type = node.id.replace(/[\d_]/g, '');
	
	if ( typeof tabSdom == "undefined" || !(tabSdom instanceof Array) ) return;
	
	if ( node.leaf ){
		var item = tabSdom[parseInt(id)];
		if ( item && item.cls ) {
		  switch (item.cls){
		  	case 'typeaccesadmin' : node.setText(node.attributes.text+"<span class='"+item.cls+" setdomsdom'>&nbsp;Administration&nbsp;</span>"); break;
		  	case 'typeaccesconsult' : node.setText(node.attributes.text+"<span class='"+item.cls+" setdomsdom'>&nbsp;Consultation&nbsp;</span>"); break;
		  }
		}
	}
	node.childNodes.forEach(affectStyleNode);
}

Ext.onReady( function() {
	var labelWidth = 200;
  treePanel = new Ext.tree.TreePanel({
    renderTo:'treePanelSdom',
    id:'ext-arbo-sdom',
    root:new Ext.tree.AsyncTreeNode({
      nodeType:"async",
      loader:new Ext.tree.TreeLoader({
        url:domaineService,
        baseParams:{fmeta_id:( typeof fmeta_id != 'undefined' ? fmeta_id : 0 ) },
        listeners : {
        	load : function(loader, node, response){
        		affectStyleNode(node);
        	}
        }
      })
    }),
    rootVisible:false,
    border:true,
    autoScroll:true,
    enableDD:false,
    fieldLabel:"Domaine/Sous-domaine",
    labelWidth : labelWidth,
    height:Ext.getBody().getViewSize().height-100,
    width:Ext.getBody().getViewSize().width-labelWidth*.7
  });
});


/**
 * true if needle is in array (php equivalent)
 * @param needle
 * @param haystack
 * @param argStrict
 * @return
 */
function in_array (needle, haystack, argStrict) {
   
  var key = '', strict = !!argStrict; 
  if (strict) {
      for (key in haystack) {
        if (haystack[key] === needle) {
          return true;            
        }
      }
  } else {
      for (key in haystack) {
        if (haystack[key] == needle) {   
          return true;
        }
      }
  }
  return false;
}

/**
 * @brief Valide l'ajout d'une carte
 */
function administration_carto_ajout_valider() {
	var serveur, format, fichier;
	var valid = true;
	var msg = "";
	var errormsg;
	format = document.getElementById("FORMAT");
	serveur = document.getElementById("SERVEUR");
	//var treePanel = Ext.getCmp('ext-arbo-sdom'); // plus de verif sur les sdom
	var importCheckbox = document.getElementById("IMPORT");
	if (format.value == 0) {
		fichier = document.getElementById("MAP");
		var modele = /^[a-z]+[a-z0-9_]*$/i;
		if (modele.test(fichier.value)) {
		} else {
			parent.Ext.Msg
					.alert(
							"Ajout de carte",
							"Le nom de fichier saisi ne doit pas contenir de caractères\n"
									+ "accentués ou non alphanumériques comme -()\"'. etc.");
			return;
		}
		if (in_array(fichier.value, tabData[serveur.value])) {
			parent.Ext.Msg.alert("Ajout de carte",
					"Une carte portant ce nom existe déjà.");
			return;
		}

	} else {
	  fichier = document.getElementById("MAP");
	}

	if (serveur && format && fichier /*&& treePanel*/) {
		if ((serveur.value == "") || (format.value == "")
				|| (fichier.value == "") /*|| (treePanel.getChecked().length == 0)*/
				 ){
			valid = false;
			parent.Ext.Msg.alert("Ajout de carte",
					"Tous les champs sont obligatoires");
			return;
		}
	}
	if (valid) {
		//var oCtrlSdom = document.getElementById("SDOM");
		//parent.checkAndSetCurrentSousDomaine(treePanel, oCtrlSdom, tabSdom, function(){document.forms[0].submit();}, this);
                document.forms[0].submit();
	}
}

/**
 * @brief Gère le changement de type de couche
 * @param format
 * @return
 */
function administration_carte_ajout_ontypechange(format) {
	var fichier, table;
	var parcourir;

	fichier = document.getElementById("file_item");
	table = document.getElementById("map_item");
	if (fichier && table) {
		switch (format.value) {
		case "0":
			fichier.style.display = "none";
			table.style.display = "";
			break;
		default:
			fichier.style.display = "";
			table.style.display = "none";
			break;
		}
	}
}

/**
 * @brief Fonction qui permet de changer le chemin de la carte
 * @param obj
 */
function administration_carto_ajout_changepath(obj) {
	var text = obj.value.toString();

	text = text.toLowerCase();
	text = text.replace(/[àâä]/ig, "a");
	text = text.replace(/[éèêë]/ig, "e");
	text = text.replace(/[îï]/ig, "i");
	text = text.replace(/[ôö]/ig, "o");
	text = text.replace(/[ù]/ig, "u");
	text = text.replace(/[ç]/ig, "c");
	text = text.replace(/\s/g, "_");
	obj.value = text;
}


/**
 * @brief Valide l'ajout d'un service
 */
function administration_service_ajout_valider() {
	var valid = true;
	var msg = "";
	var errormsg;
	var treePanel = Ext.getCmp('ext-arbo-sdom');
	var importCheckbox = document.getElementById("IMPORT");
	var mefFile = document.getElementById("mefFile");

	if (treePanel.getChecked().length == 0 || (importCheckbox.checked && mefFile.value == "")) {
		valid = false;
		parent.Ext.Msg
				.alert("Ajout de carte", "Tous les champs sont obligatoires");
		return;
	}
	if (valid) {
		var oCtrlSdom = document.getElementById("SDOM");
		parent.checkAndSetCurrentSousDomaine(treePanel, oCtrlSdom, tabSdom, function(){parent.IHM_Chargement_start();document.forms[0].submit();}, this);
	}
}
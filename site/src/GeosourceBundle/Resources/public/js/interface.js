var jsonMaps_catalog = null ; //layer list from catalog
var jsonMapsPerso_catalog = null;
var jsonMaps_carto = null ; //layer list from carto
var tabMaps = null ; //layer list fusion from catalog and carto (keep the ones in carto that are listed in catalog)
var tabMapsPerso = null;
var nbAjaxRequest = 0 ; //number of current ajax requests;
var ajaxCartoRequest = null;//ajax request for carto Maps

if (ajaxCatalogRequest){
  ajaxCatalogRequest = null;
}
else{
  var ajaxCatalogRequest = null;
}
if (loadmask){
  loadmask = null;//extjs loading mask
}
else{
  var loadmask = null;//extjs loading mask
}
if (selectedLayer){
  selectedLayer = null;
}
else{
  var selectedLayer = null;
}



/**
 * @brief Load cartographic maps from the prodige database
 */
function LoadModelMaps(url){
  //ajax call
  //var url = "tSAjoutDeCarte_MapModelList";
  nbAjaxRequest ++;
  ajaxCartoRequest = Ext.Ajax.request({
      url: url,
      method: "GET",
      success: function (response){
        //TODO banish eval
        try{tabMaps = eval(response.responseText);}catch(error){tabMaps = [];}
        /* load carto Maps
        organize Maps in their respective geomtype, 
        hide loadmask and display Maps */
        ProcessMaps();
      },
      failure: function (response){
         //hide loadmask and print error
         ProcessFailures();
      },
      params : {
        //"SRV_CARTO" : const_serveurCarto
      },
      scope: this
    });
}

/**
 * @brief Load carto Maps and do the fusion with Maps from catalog :
 * Organize Maps
 */
function ProcessMaps(){
  nbAjaxRequest --;
  if (nbAjaxRequest==0){
    //loadmask.hide();
    //if(type!="modele")
    //  FusionMaps();
    DisplayMaps();
  }
}
/**
 * @brief Print the layer tree for the selected type
 */
function DisplayMaps(){
  var tabMapsToPrint = tabMaps;
  //create the treepanel
  var Tree = Ext.tree;
  var tree = new Tree.TreePanel({
    animate:true,
    enableDD:false,
    loader: new Tree.TreeLoader(),
    lines: true,
    selModel: new Ext.tree.DefaultSelectionModel(),
    containerScroll: true,
    border: false,
    autoScroll: true,
    width : 400,
    height : 250,
    rootVisible : false,
    listeners:{
      "click" : function(treeNode){
        //set layer title text
        //TSAjoutDeCouche_setLAYER_TITLE(treeNode.attributes.text);
      	if ( !treeNode.leaf ) return;
        selectedLayer = [treeNode.attributes.map_name, treeNode.attributes.text];
        if (selectedLayer){
        	new Ext.LoadMask(myWin.getEl(), {msg:"Génération de la carte..."}).show();
            var mapname = treeNode.attributes.map_id;
            document.choix_modele.model.value = mapname;
            interface_ModelSubmit();
        }
      }
    }
  });
  var tabChildren = new Array();
  if(tabMapsToPrint.length>0)
    tabChildren.push( {
      "text" : "Cartes du portail", "id":"rubric_1", "leaf":false, "expanded":true, "cls":"folder", "children":
      eval(Ext.util.JSON.encode(tabMapsToPrint))
      });
  else 
    tabChildren.push( {
      "text" : "Aucune carte modèle", "id":"rubric_1", "leaf":false, "expanded":true, "cls":"folder", children : []
      });
  if(tabMapsPerso && tabMapsPerso.length!=0){
  tabChildren.push({
      "text" : "Mes cartes", "id":"rubric_2", "leaf":false, "expanded":true, "cls":"folder", "children":
        eval(Ext.util.JSON.encode(tabMapsPerso))
  });
  }
  var root = new Tree.AsyncTreeNode({
    draggable:false,
    children: tabChildren
  });
    
  tree.setRootNode(root);
  var myWin = new Ext.Window({
    layout: 'fit',
    title: 'Cartes Modèles',
    width: 400,
    height: 300,
    closable: true,
    buttonAlign: 'center',
    items: [tree]
  });
  myWin.show();
  root.expand();
}



/**
 * soumission du formulaire
 * @return
 */
function  interface_ModelSubmit()
{
  document.getElementById('choix_modele').submit();
}
var type ="";
var siteId="";

/**
 * @brief Initialisation de la page
 * @return
 */
function	donnees_init(typeMetadata, siteIdDefault)
{
	type = typeMetadata;
	siteId = siteIdDefault;
	try
	{
		parent.IHM_Chargement_stop();
		donnees_domaineChange();
		parent.domaines_event_onchange_add(parent.events_donnees_changedomaine);
		//donnees_onfocus();
	}
	catch(e)
	{}
}

function	donnees_onfocus()
{
	var		domaine;
	
	try
	{
		domaine = parent.domaines_getCurentDomaine();
		if (domaine)
			parent.IHM_Domaines_resume();
		else
			parent.IHM_Domaines_show();
		if (parent.consultation_win_cartodynamique)
			parent.consultation_win_cartodynamique.focus();
	}
	catch(e){}
}
/**
 * @brief Gestion du changement de domaine
 * @return
 */
function	donnees_domaineChange()
{
  var    domaine;
  var   sousdomaine;
  try
  {
    domaine = parent.domaines_getCurentDomaine();
    sousdomaine = parent.domaines_getCurentSousDomaine();
    if (domaine){
      donnees_loadData();
    }
    else
    {
     /* parent.Ext.getCmp('westLayout').setTitle("Domaine / Sous-domaine");
      parent.IHM_Panier_hide();
      parent.IHM_Domaines_show();
      parent.IHM_Chargement_stop();
      parent.IHM_NoSelectionDomain_show();
      window.timerOnload = setTimeout("window.onload()", 2500);*/
    }
  }
  catch(e){}
}
/**
 * v3.2 inutile de passer le domaine/sous-domaine car ceux-ci sont récupérés directement par la surcharge de Géosource
 * @brief Affichage des données d'un domaine/sous domaine
 * @param domaine
 * @param sousdomaine
 * @return
 */
function	donnees_loadData()
{
  parent.IHM_Chargement_start();
  document.location.replace("/"+parent.geonetwork_dir+"/srv/fre/find?hl=fre&bSearch=1&bSearchPanel=0&s_E_hitsperpage=100&bAffDomSdom=1&type="+type+"&s_E_siteId="+siteId);	
}
/**
 * @brief Paramètre le texte résumé pour un sous-domaine
 * @param domaine
 * @param sousdomaine
 * @return
 */
function	donnees_setResume(domaine, sousdomaine)
{
	var		div_dom;
	var		div_sdom;
	var		node;
	
	div_dom = document.getElementById("domaine");
	if (domaine && div_dom)
	{
		node = div_dom.firstChild;
		if (node && (node.nodeType == 3))
			node.data = domaine;
		else
		{
			node = document.createTextNode(domaine);
			if (node)
				div_dom.appendChild(node);
		}
	}
	div_sdom = document.getElementById("sousdomaine");
	if (sousdomaine && div_sdom)
	{
		node = div_sdom.firstChild;
		if (node && (node.nodeType == 3))
			node.data = sousdomaine;
		else
		{
			node = document.createTextNode(sousdomaine);
			if (node)
				div_sdom.appendChild(node);
		}
	}
}


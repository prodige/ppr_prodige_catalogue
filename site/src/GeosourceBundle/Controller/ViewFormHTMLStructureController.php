<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Prodige\ProdigeBundle\Common\Util;

/**
 * @Route("/geosource")
 */
class ViewFormHTMLStructureController extends BaseController
{
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/viewFormHTMLStructure/{token}", name="catalogue_geosource_viewFormHTMLStructure", options={"expose"=true})
     */
    public function viewFormHTMLStructureAction(Request $request, $token)
    {

        //require_once("./path.php");
        //require_once("../lib/util.php");
        //require_once("../lib/viewFactory.class.php");
        //require_once ("../../lib_file.php"); // for use addScriptCSS & addScriptJS methods

        //set_error_handler("handle_error", E_ALL | E_STRICT);
        if ($token == null) {
            //header("location:errors/error_communication.html");
        }

        $jsonParams = Util::getDecodeParam($token);

        $params = json_decode($jsonParams, true);
        $url_back = (isset($params["url_back"]) ? $params["url_back"] : null);

        $theme_id_for_ext = isset($params["theme_id_for_ext"]) ? intval($params["theme_id_for_ext"]) : null;
        $colorOverloadfile = "";
        /*if($theme_id_for_ext != null && $PRODIGE_EXT_THEME_COLOR[$theme_id_for_ext]["color"] != "") {
            $colorOverloadfile = array_search(strtolower($PRODIGE_EXT_THEME_COLOR[$theme_id_for_ext]["color"]), $EXT_THEME_COLOR);
        }*/

        //$theme_id_for_ext = 8;
        if ($params == null) {
            //header("location:errors/error_communication.html");
        }

        $ext_css = "/Script/ext-5.0.1/build/packages/ext-theme-crisp/build/resources/ext-theme-crisp-all.css";

        $ar_parameters = array(
            "colorOverloadfile" => $colorOverloadfile,
            "url_back" => $url_back,
            "table_name" => (isset($params['data_name']) ? $params['data_name'] : ""),
        );

        return $this->render('GeosourceBundle\Default\view_form_HTML_Structure_template.html.twig', $ar_parameters);

    }
}

<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;


use Symfony\Component\Routing\Annotation\Route; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\Common\Mail\AlkMail;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


/**
 * @Route("/geosource")
 */
class GeosourceController extends BaseController {
    /**
     * @Route("/get_config")
     */
    public function getConfigAction(Request $request) {
        $settings = array();
        $this->getUser();
        $user = User::GetUser();
        
        
        $connection = $this->getConnection('prodige', 'parametrage');
        $stmt = $connection->executeQuery("select * from prodige_settings");
        foreach($stmt as $row) {
            $settings[$row["prodige_settings_constant"]] = $row["prodige_settings_value"];
        }
        $connection = $this->getConnection('catalogue', 'catalogue');
        $stmt = $connection->executeQuery("select * from prodige_settings");
        foreach ($stmt as $row){
            $settings[$row["prodige_settings_constant"]] = $row["prodige_settings_value"];
        }

        $sessionNumber = null;
        if(isset($settings["PRO_CATALOGUE_NB_SESSION_USER"]) && $settings["PRO_CATALOGUE_NB_SESSION_USER"] == "on") {
            $connection = $this->getConnection('catalogue', 'catalogue');
            $query = "select  count(date_connexion) from PRODIGE_SESSION_USER where date_connexion > (now()- interval '30 minute');";
            $sessionNumber = $connection->executeQuery($query)->fetchOne();
        }
        
        // supprimer les mots de passe passés en clair
        foreach (array_keys($settings) as $key) {
            if(strpos($key, 'PWD') !== false) unset($settings[$key]);
        }
        
        $callback = $request->query->get('callback', null);
        
        // disaptch event to allow optional bundles to contribute to menu entires
        $rme = new \Prodige\ProdigeBundle\Event\RegisterMenuEvent();
        $this->container->get('event_dispatcher')->dispatch($rme);
         
        $additionalEntries = $rme->getEntries();
        
        $catalogueEntries = array();
        foreach ($additionalEntries['catalogue'] as $entry) {
            $catalogueEntries[$entry['route']] = $this->generateUrl($entry['route'], array(), UrlGeneratorInterface::ABSOLUTE_URL);
        }
        $adminEntries = array();
        foreach ($additionalEntries['admin'] as $entry) {
            $adminEntries[$entry['route']] = $this->generateUrl($entry['route'], array(), UrlGeneratorInterface::ABSOLUTE_URL);
        }
        $carte_perso_is_actif = false ; 
        if(strtolower(PRO_IS_CARTEPERSO_ACTIF) ==="on") {
            $carte_perso_is_actif = true; 
        }
        $response = array_merge(
            $twigParams = array(
                "URL"           => $this->container->getParameter('PRODIGE_URL_CATALOGUE'),
                "admincartoURL" => $this->container->getParameter('PRODIGE_URL_ADMINCARTO'),
                "errormsg"      => null,
                "captcha"       => rand(),
                "isSend"        => false,
                "sessionNumber" => $sessionNumber,
                "usr_name"      => $user->GetNom(),
                "usr_firstname" => $user->GetPrenom(),
                "usr_mail"      => $user->GetEmail(),
                "userDetails"   => $user->getUserAccountExpirationDetails(),
            ),
            $settings, 
            array(
                "routes" => array(
                    "catalogue" => array_merge(
                        array(
                            "geosource_contact_admin"   => $this->generateUrl("geosource_contact_admin", array(), UrlGeneratorInterface::ABSOLUTE_URL),
                            "prodige_verify_rights_url" => $this->generateUrl("prodige_verify_rights_url", array(), UrlGeneratorInterface::ABSOLUTE_URL),
                            "geosource_get_help"        => $this->generateUrl("geosource_get_help", array(), UrlGeneratorInterface::ABSOLUTE_URL),
                            "tableviewer_url"           => $this->container->hasParameter('PRODIGE_URL_TABLEVIEWER') ? $this->container->getParameter('PRODIGE_URL_TABLEVIEWER') : '#',
                            "geosource_user_details"    => $this->generateUrl("catalogue_web_userdetails", array(), UrlGeneratorInterface::ABSOLUTE_URL),
                            "carteperso"                => $carte_perso_is_actif && $user->hasTraitement('CARTEPERSO') ? $this->generateUrl("catalogue_carteperso", array(), UrlGeneratorInterface::ABSOLUTE_URL) : false, false,
                        ),
                        array('optional'=>$catalogueEntries)
                    ),
                    "admin" => $user->IsProdigeAdmin() ? array_merge(
                        array(
                            "prodige_admin"     => $this->generateUrl("catalogue_web_index", array(), UrlGeneratorInterface::ABSOLUTE_URL),
                            "prodige_adminsite" => $this->container->hasParameter('PRODIGE_URL_ADMINSITE') ? $this->container->getParameter('PRODIGE_URL_ADMINSITE') : '#',
                            "prodige_standards" => (PRO_MODULE_STANDARDS=="on" ? $this->generateUrl("standards_administration", array(), UrlGeneratorInterface::ABSOLUTE_URL): false),
                            "prodige_base_territoriale" => (PRO_MODULE_BASE_TERRITORIALE=="on" && $this->container->hasParameter('PRODIGE_URL_BDTERRADMIN') ? $this->container->getParameter('PRODIGE_URL_BDTERRADMIN')  : false)
                        ), 
                        array('optional'=>$adminEntries)
                    ) : array(),
                ), 
            )
        );


        if ( !$callback ) {
            return new JsonResponse($response);
        }
        return new Response($callback."(".json_encode($response).")");
         
    }

    /**
     * WARNING : cette route ne doit pas être sécurisée, et autorisée à utiliser les sessions
     * 
     * @Route("/contact_admin", name="geosource_contact_admin", methods={"POST"})
     */
    public function contactAdmin(Request $request) {
        /*
        $user = User::GetUser();
        $userId = User::GetUserId();
        $usr_name = "";
        $usr_mail = "";
        $usr_firstname = "";
        if(!is_null($userId))
            User::SetUserCookie($userId);
        */
        /*
        if($user->isConnected() && $user->PasswordIsValid()){
            $usr_name = $user->GetNom();
            $usr_mail = User::GetEmail();
            $usr_firstname = $user->GetPrenom();
        }
        */
        
        $_6_letters_code = $request->request->get("captcha", null);
        if(isset($_6_letters_code)) {
            //if(isset($_SESSION['key']) && $_SESSION['key'] == $_6_letters_code) {
            if( $request->getSession()->has('key') && $request->getSession()->get('key') == $_6_letters_code) {
                $message = nl2br($request->request->get("message"));
                $usr_mail = $request->request->get("usr_email");
                $usr_name = $request->request->get("usr_name");
                $usr_firstname = $request->request->get("usr_firstname");
                $oAlkMail = new AlkMail();
                $strSujet = "PRODIGE : Message d'utilisateur";
                $oAlkMail->setSubject($strSujet);
                $oAlkMail->setHtml(true);
                $strReportHtml ="Message envoyé par ".$usr_firstname." ".$usr_name.", le ".date("d/m/Y")." à ".date("H:i")." :<br/>".
                                "<p style='font-style:italic'>".
                                $message."</p><br/>".			
                                "PS : Ce message est généré puis envoyé automatiquement par PRODIGE";
                $oAlkMail->setBody($strReportHtml);
                //Recuperation de l'adresse email admin
                $context = $this->createUnsecureSslContext();
                $admin_email = PRO_CATALOGUE_EMAIL_ADMIN;
                $admin_email_name = PRO_CATALOGUE_NOM_EMAIL_ADMIN;
                $oAlkMail->AddTo($admin_email_name,$admin_email);
                $oAlkMail->SetFrom($usr_name." ".$usr_firstname, $usr_mail);

                // envoi du message
                $oAlkMail->setbDebugMode(false);
                $oAlkMail->setMaxSend(10);
                $oAlkMail->send();  
                $isSend = true;
                return new Response("Votre message vient d'être envoyé à l'administrateur.");
            } else {
                $errormsg = 'Le code de sécurité est erroné.';
            }
        } else {
            $errormsg = 'Absence du code de sécurité.';
        }
        return new Response($errormsg?:'Erreur', 500);
    }
}

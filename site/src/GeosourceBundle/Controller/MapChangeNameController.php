<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Change le nom d'un mapfile
 * @author Alkante
 */

/**
 * @Route("/geosource")
 */
class MapChangeNameController extends BaseController {
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/mapChangeName", name="catalogue_geosource_mapChangeName", options={"expose"=true})
     */
    public function mapChangeNameAction(Request $request) {

        //$AdminPath = "../../";
        //require_once($AdminPath."DAO/DAO/DAO.php");
        //require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php");

        //global $PRO_MAPFILE_PATH;
        $strBaseDir = PRO_MAPFILE_PATH;
        $mapfile = $request->query->get("MAPFILE");
        $new_mapfile =  $request->query->get("NEW_MAPFILE");
        $pk_stockage_carte = $request->query->get("pk_stockage_carte");
        //$url_retour.="&pk_stockage_carte=".$pk_stockage_carte."&NEW_MAPFILE=".$new_mapfile;
        $callback = $request->query->get("callback");
        if(is_file($strBaseDir.$new_mapfile.".map")) {
          //new mapfile already exists
          echo $callback."({success:true, mode:3})";
        } else {
          if(is_file($strBaseDir.$mapfile.".map")) {
            rename($strBaseDir.$mapfile.".map", $strBaseDir.$new_mapfile.".map");
            echo $callback."({success:true, mode:1})";
            //header("location:".$url_retour."&iMode=1");
          } else {
            //mapfile doesn't exist
            echo $callback."({success:true, mode:2})";
            //header("location:".$url_retour."&iMode=2");
          }
        }
        exit();
    }

    /**
     * #hismail ??? : Migré mais ne sera pas utilisé dans ce service
     * @param unknown $action
     * @param unknown $pk_stockage_carte
     * @param unknown $new_mapfile
     * @param unknown $iMode
     */
    protected function getHtmlForm($action, $pk_stockage_carte, $new_mapfile, $iMode){
        $strHtml = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">
                    <html>
                      <head>
                        <title>redirect</title>
                        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">
                      </head>
                      <body onload='document.formRedirect.submit();'>
                      <form method='POST' name='formRedirect' action='".$action."'>
                      <input type='hidden' name='pk_stockage_carte' value='".$pk_stockage_carte."'/>
                      <input type='hidden' name='NEW_MAPFILE' value='".$new_mapfile."'/>
                      <input type='hidden' name='iMode' value='".$iMode."'/>
                      </body>
                    </html>";
        return $strHtml;
    }
}

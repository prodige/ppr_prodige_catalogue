<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;

/**
 * Author Alkante
 * Service de mise à jour d'imagettes de métadonnées
 */

/**
 * @Route("/geosource")
 */
class UpdateMetadataThumbnailsController extends BaseController
{
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/updateMetadataThumbnails", name="catalogue_geosource_updateMetadataThumbnails", options={"expose"=true})
     */
    public function updateMetadataThumbnailsAction(Request $request)
    {

        //require_once("../parametrage.php");
        //require_once("../Administration/DAO/DAO/DAO.php");

        //$dao = new DAO();
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');

        $pathImage = $request->query->get("pathImage");
        $metadata_id = $request->query->get("metadata_id");
        $fileType = $request->query->get("fileType");
        $fileName = $metadata_id."_".$fileType."_".rand(0, 100000).".png";

        if ($metadata_id != "") {
            //arrondi centaine inf et sup
            $valinf = floor($metadata_id / 100) * 100;
            $valsup = ceil($metadata_id / 100) * 100 - 1;
            $valinf = substr_replace("00000", $valinf, -strlen($valinf));
            $valsup = substr_replace("00000", $valsup, -strlen($valsup));

            if (!is_dir(PRO_GEOSOURCE_ROOT."WEB-INF/data/data/metadata_data".$valinf."-".$valsup)) {
                //var_dump(PRO_GEOSOURCE_ROOT."WEB-INF/data/data/metadata_data".$valinf."-".$valsup);
                mkdir(PRO_GEOSOURCE_ROOT."WEB-INF/data/data/metadata_data/".$valinf."-".$valsup, 0777, true);
            }
            if (!is_dir(PRO_GEOSOURCE_ROOT."WEB-INF/data/data/metadata_data/".$valinf."-".$valsup."/".$metadata_id)) {
                mkdir(PRO_GEOSOURCE_ROOT."WEB-INF/data/data/metadata_data/".$valinf."-".$valsup."/".$metadata_id);
            }
            if (!is_dir(
                PRO_GEOSOURCE_ROOT."WEB-INF/data/data/metadata_data/".$valinf."-".$valsup."/".$metadata_id."/public"
            )) {
                mkdir(
                    PRO_GEOSOURCE_ROOT."WEB-INF/data/data/metadata_data/".$valinf."-".$valsup."/".$metadata_id."/public"
                );
            }
            //écriture du fichier image
            file_put_contents(
                PRO_GEOSOURCE_ROOT."WEB-INF/data/data/metadata_data/".$valinf."-".$valsup."/".$metadata_id."/public/".$fileName,
                file_get_contents($pathImage)
            );
            $result['index_success'] = true;
            //TODO hismail - Service pas terminé ...
        } else {
            $result['index_success'] = false;
        }
        echo json_encode($result);
    }
}

<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Common\Util;
use Prodige\ProdigeBundle\Controller\BaseController;
//use Prodige\ProdigeBundle\Controller\User;
//use Prodige\ProdigeBundle\DAOProxy\DAO;
//
//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/geosource")
 */
class DeleteCarteStaticController extends BaseController {
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/deleteCarteStatic", name="catalogue_geosource_deleteCarteStatic", options={"expose"=true})
     */
    public function deleteCarteStaticAction(Request $request) {

        //$AdminPath = "../Administration/";
        //require("path.php");
        //require_once("../util.php");

        $token = $request->query->get("token", null);

        if($token == null) {
            echo "service indisponible";
            exit();
        }

        $strParams = Util::getDecodeParam($token);

        $tabParams = explode("&", $strParams);
        foreach($tabParams as $strParam) {
            $tabParam = explode("=", $strParam,2);
            if(count($tabParam) == 2) {
                $tabGet[$tabParam[0]] = $tabParam[1];
            }
        }
        if(!isset($tabGet["fileName"])) {
            echo "service indisponible"; 
            exit();
        }
        $data = array();
        //$res = @unlink($CheminCartesStatiques.$tabGet["fileName"]);
        $res = @unlink(PRO_MAPFILE_STATIC_PATH.$tabGet["fileName"]);
        echo json_encode(array(
          "success" => $res,
          "msg"     => "",
        ));
        exit();
    }
}

<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

/**
 * Contenu de l'onglet cartes
 * @author Alkante
 */

/**
 * @Route("/geosource")
 */
class CaptchaController extends BaseController {
    
    /**
     * @Route("/captcha", name="catalogue_geosource_captcha", options={"expose"=true})
     */
    public function captchaAction(Request $request) {
        /********************************************************
         * File:        captcha.php                             *
         * Author:      Yash Gupta (http://thetechnofreak.com)  *
         * Date:        12-Mar-2009                             *
         * Description: This file can be embedded as image      *
         *              to show CAPTCHA/                        *
         ********************************************************/
        // The number of characters you
        // want your CAPTCHA text to have
        define('CAPTCHA_STRENGTH', 5);
        /****************************
         *        INITIALISE        *
         ****************************/
        
        // Md5 to generate the random string

        $random_str = md5(/*$_GET["rand"]*/ random_bytes(10));
        // Trim required number of characters
        $captcha_str = substr($random_str, 0, CAPTCHA_STRENGTH);
        // Allocate new image
        $width = (CAPTCHA_STRENGTH * 10)+10;
        $height = 20;
        $captcha_img =ImageCreate($width, $height);
        // ALLOCATE COLORS
        // Background color-black
        $back_color = ImageColorAllocate($captcha_img, 255, 255, 255);
        // Text color-white
        $text_color = ImageColorAllocate($captcha_img, 0, 0, 0);
        // Line color-red
        $line_color = ImageColorAllocate($captcha_img, 255, 0, 0);
        /****************************
         *     DRAW BACKGROUND &amp;    *
         *           LINES          *
         ****************************/
        // Fill background color
        ImageFill($captcha_img, 0, 0, $back_color);
        // Draw lines accross the x-axis
        /*for($i = 0; $i < $width; $i += 5)
            ImageLine($captcha_img, $i, 0, $i, 20, $line_color);
        // Draw lines accross the y-axis
        for($i = 0; $i < 20; $i += 5)
            ImageLine($captcha_img, 0, $i, $width, $i , $line_color);*/
        /****************************
         *      DRAW AND OUTPUT     *
         *          IMAGE           *
         ****************************/
        // Draw the random string
        ImageString($captcha_img, 5, 5, 2, $captcha_str, $text_color);
        // Carry the data (KEY) through session
        //$_SESSION['key'] = $captcha_str;
        $request->getSession()->set('key', $captcha_str); // if session is not started, writing in the session starts it
        // Send data type
        header("Content-type: image/jpeg");
        // Output image to browser
        ImageJPEG($captcha_img,null,30);
        // Free-Up resources
        ImageDestroy($captcha_img);
        
        return new Response();
    }
}

<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Change le nom d'un mapfile
 * @author Alkante
 */

/**
 * @Route("/geosource")
 */
class GetTabLayerFromMapFileController extends BaseController {
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/getTabLayerFromMapFile", name="catalogue_geosource_getTabLayerFromMapFile", options={"expose"=true})
     */
    public function getTabLayerFromMapFileAction(Request $request) {

        //require("./path.php");

        //global $PRO_MAPFILE_PATH;
        $strBaseDir = PRO_MAPFILE_PATH;
        $mapfile = $request->query->get("MAPFILE");

        // Génération de la carte WMS correspondante
        return new Response($this->getTabLayerFromMapfile($strBaseDir, $mapfile));
    }

    protected function	getTabLayerFromMapfile($cur_map_path, $map_file) {
        $tabresult = array();

        $m_mapWms = ms_newMapObj($cur_map_path.$map_file);
        for($i = 0; $i < $m_mapWms->numlayers; $i++) {
            $tabLayerResult = array();
            $oLayer = $m_mapWms->getLayer($i);
            if($oLayer) {
                $name = $oLayer->getMetadata("LAYER_TITLE");
                $formal_name = strtr($name, utf8_decode('àáâãäåòóôõöøèéêëçìíîïùúûüÿñ'), 'aaaaaaooooooeeeeciiiiuuuuyn');
                $tabLayerResult["name"] = utf8_encode($formal_name);
                $tabLayerResult["fmeta_id"] = utf8_encode($oLayer->getMetadata("GEONETWORK_METADATA_ID"));
                $tabresult[]  = $tabLayerResult;
            }
        }
        return json_encode($tabresult);
    }
}

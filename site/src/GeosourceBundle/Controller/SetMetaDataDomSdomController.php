<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Controller\User;

/**
 * brief service qui permet de mettre à jour les domaines/sous-domaines d'une métadonnée
 *        met préalablement à jour le catalogue uniquement si les sous-domaines sont passés en paramètre
 * @author Alkante
 * @param id    identifiant de la métadonnée (fmeta_id)
 * @param mode  type de donnée (couche, carte)
 * @param sdom  tableau des nouveaux sous-domaines, utilisé uniquement pour mettre à jour le catalogue (optionnel)
 */

/**
 * @Route("/geosource")
 */
class SetMetaDataDomSdomController extends BaseController {
    
    
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/setDomsdom/{uuid}", name="catalogue_geosource_setDomsdom", options={"expose"=true})
     */
    public function setDomsdomAction(Request $request, $uuid) {

      
        $mode = $request->get("mode");
        $strJs = "";
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        $dao->setSearchPath("public, catalogue");
        if($dao)
        {
            $rs = $dao->BuildResultSet("SELECT id FROM METADATA WHERE UUID=?", array($uuid));
            $rs->First();
            $fmeta_id = $rs->Read(0);
            if ($request->getMethod() == 'POST') {
                $response = $this->forward('Prodige\ProdigeBundle\Controller\UpdateDomSdomController::updateDomSdomAction',
                   array(
                    'fmeta_id'  => $fmeta_id,
                    'modeData' => $mode
                ));
            }
            $strInitJs = "";
            // construit un tableau javascript contenant les sous-domaines
            $query = "select *, (case when usr_right is null then '' when usr_right then 'typeaccesadmin' else 'typeaccesconsult' end) as stylecolor from ".
                " (SELECT distinct DOM_SDOM.PK_SOUS_DOMAINE, RUBRIC_PARAM.RUBRIC_ID, DOM_SDOM.PK_DOMAINE, DOM_SDOM.DOM_NOM, DOM_SDOM.SSDOM_NOM ".
                ",  (ssdom_admin_fk_groupe_profil   in (select gusr.grpusr_fk_groupe_profil from grp_regroupe_usr gusr where gusr.grpusr_fk_utilisateur=:user_id) ) as usr_right ".
                " FROM dom_sdom ".
                " inner JOIN RUBRIC_PARAM ON DOM_SDOM.DOM_RUBRIC=RUBRIC_PARAM.RUBRIC_ID ".
                " inner join sous_domaine on sous_domaine.pk_sous_domaine=DOM_SDOM.pk_sous_domaine ".
                ") foo order by RUBRIC_ID, DOM_NOM, SSDOM_NOM";
            $rs = $dao->BuildResultSet($query, array("user_id"=>User::GetUserId()));
            $strInitJs .= "tabSdom = new Array();\n";
            for ($rs->First(); !$rs->EOF(); $rs->Next())
            {
                $sdom_id = $rs->Read(0);
                $rub_id  = $rs->Read(1);
                $dom_id  = $rs->Read(2);
                
                $strInitJs .= "tabSdom[".$sdom_id."] = new Object();\n";
                $strInitJs .= "tabSdom[".$sdom_id."].rubric_id = ".$rub_id.";\n";
                $strInitJs .= "tabSdom[".$sdom_id."].dom_id = ".$dom_id.";\n";
                $strInitJs .= "tabSdom[".$sdom_id."].cls = '".$rs->Read(6)."';\n";
            }
            
            $rs = $dao->BuildResultSet("SELECT id FROM METADATA WHERE UUID=?", array($uuid));
            $rs->First();
            $fmeta_id = $rs->Read(0);
        }
        
        $strInitJs .= "var fmeta_id= '".$fmeta_id."';";
        $strInitJs .= "var domaineService= '".$this->generateUrl('catalogue_geosource_getDomaines')."';";
        $strCss  = '<link rel="stylesheet" type="text/css" href="'.$this->generateUrl('catalogue_get_css_file', array('css_file'=>'administration_utilisateur')).'">';
        $strCss .= '<link rel="stylesheet" type="text/css" href="'.$this->generateUrl('catalogue_geosource_get_css_file', array('css_file'=>'administration_carto_ajout')).'">';
        //$strCss.= '<link rel="stylesheet" type="text/css" href="/bundles/geosource/css/Administration.css">';
        $strCss.= '<link rel="stylesheet" type="text/css" href="/Scripts/ext3.0/resources/css/ext-all.css">';
        $strCss.= '<link rel="stylesheet" type="text/css" href="/bundles/prodigeprodige/bootstrap/css/bootstrap.min.css">';
        
        $strJs = '<script  src="/Scripts/ext3.0/adapter/ext/ext-base.js"> </script>';
        $strJs.= '<script  src="/Scripts/ext3.0/ext-all.js"> </script>';
        $strJs.= '<script  src="/Scripts/ext3.0/ext-lang-fr.js"> </script>';
        $strJs.= '<script  src="/Scripts/ext3.0/miframe.js"> </script>';
        $strJs.= '<script  src="/geosourcebundle/js/extarbosdom.js"> </script>'.
            '<script  src="/geosourcebundle/js/domaines.js"> </script>'.
            '<script  src="/geosourcebundle/js/administration_carto_ajout.js"> </script>'.
            '<script language="javascript">'.$strInitJs.'</script>';
        
        $strHtml = "
        <!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">
        <html>
        	<head>
        		<title>Domaines de publication</title>
        		<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">
            <!-- IE specific : forcing IE9 Document model because ext 3.0 incompatibility with IE10 -->
            <META HTTP-EQUIV='X-UA-Compatible' CONTENT='IE=9'/>".
                    $strCss.$strJs."
        	</head>
        	<body>
        		<div class=\"titre\"><h2>Affectation des domaines / sous-domaines </h2></div>
        		<div class=\"errormsg\" id=\"errormsg\">". (isset($errormsg) ? $errormsg : "")."</div>
        		<div class=\"formulaire\">
        			<form method=\"POST\">
        				<table>
        					<tr id=\"sdom_item\" >
                    <th>Sous-domaines</th>
                    <td>
                      <div id=\"treePanelSdom\"></div>
                      <select id=\"SDOM\" name=\"sdom[]\" style=\"display:none;\" multiple=\"true\"></select>
                    </td>
                  </tr>
                  </table>
                  <p></p>
                  <p class=\"text-center\">
        			<input class=\"btn btn-primary\" type=\"button\" name=\"VALIDATION\" value=\"Enregistrer\" onclick=\"administration_domaines_valider()\">
        			<input class=\"btn btn-primary\" type=\"button\" name=\"RETOUR\" value=\"Fermer\" onclick=\"window.close();\">
                  </p>
        				
        			</form>
        		</div>
        	</body>
        </html>";
        return new Response ($strHtml);
        
    }
    
    
}

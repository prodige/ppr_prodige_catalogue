<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

use ProdigeCatalogue\GeosourceBundle\Common\MetadataDesc;
use ProdigeCatalogue\GeosourceBundle\Common\ClassRessourcesDownload;
use ProdigeCatalogue\GeosourceBundle\Common\PanierDownloadFrontalParametrageCommonFunctions;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\DAOProxy\DAO;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/geosource")
 */
class PanierDownloadFrontalParametrageVectorController extends BaseController
{
    /**
     * @Route("/panierDownloadFrontalParametrageVector", name="catalogue_geosource_panierDownloadFrontalParametrageVector", options={"expose"=true})
     */
    public function catalogue_geosource_panierDownloadFrontalParametrageVectorAction(
        Request $request,
        $tabTablename_vector,
        $service_idx,
        $userEmail,
        $strMetadataId_vector,
        $tabCoucheName_vector,
        $tabMetadataId_vector,
        $tabRestrictTerr,
        $m_Idts_table,
        $m_Idts_vector,
        $check_extract_territoire,
        $table_cb_id,
        $extract_tolerance,
        $m_serveur
    ) {

        $metadata = new MetadataDesc();
        $conn = $this->getCatalogueConnection('catalogue');
        $metadata->setConnection($conn);
        $metadata->setContainer($this->container);

        $vectorTables = $tabTablename_vector;

        $url = $this->generateUrl('prodige_getAreasFromDB');

        $infoLayers = array();
        $cgu = '';
        foreach ($tabCoucheName_vector as $key => $value) {
            $infoLayers[] = array("id" => $key, "name" => addslashes($value["name"]), "server" => $value["server"]);
            $query = "SELECT couchd_cgu_display, couchd_cgu_message "
                . "FROM couche_donnees cd "
                . "JOIN fiche_metadonnees fd ON fd.fmeta_fk_couche_donnees = cd.pk_couche_donnees "
                . "WHERE fmeta_id = :id ";
            $stmt = $conn->executeQuery($query, array('id'=>$key));
            $results = $stmt->fetchAllAssociative();
            if ($results[0]['couchd_cgu_display'] && !empty($results[0]['couchd_cgu_message'])) {
                $cgu .= "<h3>Conditions générales d'utilisation pour la couche ". addslashes($value["name"]) . "</h3>";
                $cgu .= $results[0]['couchd_cgu_message'];
            }
        }

        $str_download_data = "";
        foreach ($tabMetadataId_vector as $key => $idMetadata) {

            $str_download_data .= " restricted_area_field += '%';";
            $str_download_data .= " territoire_area +='%';";
            $str_download_data .= " restricted_area_buffer +='%';";

        }

        $areAllVisualisable = 0;
        for ($idx = 0; $idx < count($m_Idts_vector); $idx++) {
            $areAllVisualisable += PanierDownloadFrontalParametrageCommonFunctions::presenterIs3D($m_Idts_vector[$idx]);
        }
        $formats = array();
        $m_Format = ClassRessourcesDownload::getFormats($areAllVisualisable);
        for ($idx = 0; $idx < count($m_Format); $idx++) {
            list($nomFormat, $valFormat) = explode("||", $m_Format[$idx]);
            $formats[] = array(ClassRessourcesDownload::encodeValue($valFormat), $nomFormat);
        }

        $projections = array();
        $m_Projection = ClassRessourcesDownload::getProjections();
        for ($idx = 0; $idx < count($m_Projection); $idx++) {
            list($nomProj, $valProj) = explode("|", $m_Projection[$idx]);
            $projections[] = array(ClassRessourcesDownload::encodeValue($valProj), $nomProj);
        }

        $str3 = '<input id="check_extract_territoire_vector" name="check_extract_territoire_vector" '.(($check_extract_territoire == 1) ? 'checked ' : '').'type="checkbox" value="1" onclick=\'print_territoire(configTerritoire, "vector");\'/>';
        $str4 = "";
        if ($cgu != '') { // Display CGU in priority over licence
            $str4 .= '<div id="cgu-content" style="display:none;">'.$cgu.'</div>';
            $str4 .= '<input type="Button" value="Exécution directe" onclick="affCGU(\'execute_telecharger_vector(true);\', \'verifCtrl_vector()\')"> &nbsp; <input type="Button" value="Exécution différée" onclick="affCGU(\'execute_telecharger_vector(false);\', \'verifCtrl_vector()\')">';
        } elseif (PRO_CATALOGUE_TELECHARGEMENT_LICENCE == "on") {
            $str4 .= '<input type="Button" value="Exécution directe" onclick="affLicence(\'execute_telecharger_vector(true);\', \''.PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL.'\', \'verifCtrl_vector()\')"> &nbsp; <input type="Button" value="Exécution différée" onclick="affLicence(\'execute_telecharger_vector(false);\', \''.PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL.'\', \'verifCtrl_vector()\')">';
        } else {
            $str4 .= '<input type="Button" value="Exécution directe" onclick="execute_telecharger_vector(true);"> &nbsp; <input type="Button" value="Exécution différée" onclick="execute_telecharger_vector(false);">';
        }


        $withJoin = (count($m_Idts_table) != 0);
        $str5 = PanierDownloadFrontalParametrageCommonFunctions::printFormHeader($withJoin);
        $tabSrv = explode("|", $m_serveur);

        $str5 .= PanierDownloadFrontalParametrageCommonFunctions::presenterServeur(
            $tabSrv[0],
            $m_Idts_vector,
            0,
            "vector",
            $withJoin,
            $m_Idts_table,
            (isset($tabRestrictTerr) ? $tabRestrictTerr : array())
        );


        $ar_parameters = array(
            "CARMEN_URL_SERVER_DOWNLOAD" => CARMEN_URL_SERVER_DOWNLOAD,
            "vectorTables" => $vectorTables,
            "service_idx" => $service_idx,
            "email" => ClassRessourcesDownload::encodeValue($userEmail),
            "metadata_id" => ClassRessourcesDownload::encodeValue($strMetadataId_vector),
            "url" => $url,
            "infoLayers" => $infoLayers,
            "str_download_data" => $str_download_data,
            "params_direct" => ClassRessourcesDownload::encodeValue("1"),
            "form_action" => $request->getRequestUri(),
            "formats" => $formats,
            "projections" => $projections,
            "str3" => $str3,
            "table_cb_id" => $table_cb_id,
            "extract_tolerance" => $extract_tolerance,
            "m_Idts_vector" => implode($m_Idts_vector),
            "str4" => $str4,
            "str5" => $str5,
        );

        return $this->render(
            'GeosourceBundle\Default\panierDownloadFrontal_parametrage_vector_template.html.twig',
            $ar_parameters
        );
    }

}

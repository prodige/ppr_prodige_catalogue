<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route; 
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/geosource")
 */
class BridgeController extends BaseController {
    /**
     * @Route("/bridge", name="catalogue_geosource_bridge", options={"expose"=true})
     */
    public function bridgeAction(Request $request) {
        $strhtml = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
                    <html>
                        <head>
                            <title> Bridge page used to bring JS variable up the top window </title>
                            <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
                            <script type="text/javascript">';
        if($request->query->get("callback", false)) {
            if($request->query->get("param", false))
                $strhtml .= 'var args = "'.$request->query->get("param").'".split(";");';
            else
                $strhtml .= 'var args = [];';
            $strhtml .=  'parent.parent.'.$request->query->get("callback").'.apply(parent.parent,args);';
        }

        $strhtml .= '       </script>
                        </head>
                        <body>
                        </body>
                    </html>';
        echo $strhtml;
        exit();
    }
}

<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Get map list from server files
 * @author Alkante
 * Prodige V3 ---> V4
 */

/**
 * @Route("/geosource")
 */
class TSAjoutDeCarteMapModelListController extends BaseController
{

    /**
     *
     * @Route("/tSAjoutDeCarte_MapModelList", name="catalogue_geosource_tSAjoutDeCarte_MapModelList", options={"expose"=true})
     */
    public function tSAjoutDeCarte_MapModelListAction(Request $request)
    {
        //require_once("path.php");

        $callback = $request->get("callback", "");
        $modelType = $request->get("modelType", "1");

        /*JSON object to return */
        $returnJSON = $this->getMapList($modelType);

        return new Response($callback."(".$returnJSON.")");
    }

    /**
     * brief Returns a json object containing the list of the layers availaible in the cartp database
     * The pattern is :
     * {"name" : "name", "type" : "raster or vector (O or 1)",
     *  "table" : "postgis_table", "id" : "metadata id", "text" : "metadata title and text for extjs tree",
     *  "leaf" : true or false, "cls" : style, "children" : null}
     *
     * @param $dbconn
     * @return json text
     */
    protected function getMapList($modelType)
    {
        //build the list of tables availaible on the postgis server
        //global $PRO_MAPFILE_PATH;
        $ALL_MAPS = array();
        $validModels = array();

        if (file_exists(PRO_MAPFILE_PATH."Modeles_Administration.xml")) {
            $xmlDoc = simplexml_load_file(PRO_MAPFILE_PATH."Modeles_Administration.xml");
            foreach ($xmlDoc->MODELE as $modele) {
                if ($modelType == $modele["TYPE"]) {
                    array_push($validModels, $modele["ID"]);
                }
            }
        }
        $conn_prodige = $this->getProdigeConnection('carmen');
        $tabInfoModel = $conn_prodige->fetchAllAssociative(
            "SELECT map_file, map_id, map_title from map where map_model =true and published_id is null"
        );

        foreach ($tabInfoModel as $modele) {
            if (in_array($modele["map_file"].".map", $validModels)) {
                $ALL_MAPS[] = array(
                    "map_name" => (file_exists(
                        PRO_MAPFILE_PATH."/modele_".$modele["map_file"].".map"
                    ) ? "modele_".$modele["map_file"].".map" : $modele["map_file"].".map"),
                    "map_id" => $modele["map_id"],
                    "text" => $modele["map_title"],
                    "leaf" => true,
                    "cls" => "folder",
                    "children" => array(),
                );
            }
        }

        return json_encode($ALL_MAPS);
    }

    /* GetExtensionName - Renvoie l'extension d'un fichier
     . $File (char): Nom du fichier
     . $Dot  (bool): avec le point true/false
     */
    protected function GetExtensionName($File, $Dot = false)
    {
        if ($Dot == true) {
            $Ext = strtolower(substr($File, strrpos($File, '.')));
        } else {
            $Ext = strtolower(substr($File, strrpos($File, '.') + 1));
        }

        return $Ext;
    }
}

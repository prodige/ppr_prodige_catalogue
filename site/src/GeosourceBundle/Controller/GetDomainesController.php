<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

use ProdigeCatalogue\GeosourceBundle\Common\Domaines;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/geosource")
 */
class GetDomainesController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/getDomaines", name="catalogue_geosource_getDomaines", options={"expose"=true})
     */
    public function getDomainesAction(Request $request) {

        Domaines::setConnection($this->getCatalogueConnection('catalogue'));

        //$AdminPath = "../Administration/";
        //include_once("../include/domaines.php");

        header("Content-type: text/plain; charset=UTF-8\r\n");

        /**
         * mode=0 : return response for tree (with structures)
         * mode=1 : return response for store
         * mode=2 : return a list of sub-domains the user can administrate
         */

        $mode = $request->get("mode", 0);
        $callback = $request->get("callback", "");

        switch($mode) {
          case 0 :
            /**
             * id of metadata
             */
            $fmeta_id = $request->get("fmeta_id", -1);
            $format = $request->get("format", "arbo");

            switch($format) {
              case "list" :
                return new Response(Domaines::getMetadataSousDomainesList($fmeta_id));
              break;
              case "list_x_domain" :
                $domaine = Domaines::getDomaines(true, $fmeta_id, true);
                return new Response($callback."(".$domaine.")");
              break;
              case "arbo" :
              default :
                $domaine = Domaines::getDomaines(true, $fmeta_id, false);
                if( $callback ) return new Response($callback."(".$domaine.")");
                else return new Response($domaine);
              break;
            }
          break;
          case 1 :
            /**
             * q is a filter on domaine/sub-domaine name
             */
            $q = $request->get("q", "");
            return new Response(Domaines::getDomainesForStore($q));
          break;
          case 2 :
            return new Response(Domaines::getUserSDomainesAdminList());
          break;
        }
        exit;
    }
}

<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route; 
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\DAOProxy\DAO;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Page de construction du mapfile de covisualisation de couches
 * permet de visualiser plusieurs données sur une même carte
 * fait appel au service prodigeadmincarto/PRRA/Services/addLayerToMap.php pour l'ajout des couches
 * @author Alkante
 */

/**
 * @Route("/geosource")
 */
class CovisualisationController extends BaseController {
    
    
}

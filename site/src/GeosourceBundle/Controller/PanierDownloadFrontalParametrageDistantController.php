<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use ProdigeCatalogue\GeosourceBundle\Common\MetadataDesc;

use Prodige\ProdigeBundle\Controller\BaseController;

//use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\DAOProxy\DAO;

//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use ProdigeCatalogue\GeosourceBundle\Common\ClassRessourcesDownload;

/**
 * paramétrage pour exécution du chargement des données sur le serveur (format zip)
 * @author Alkante
 */

/**
 * @Route("/geosource")
 */
class PanierDownloadFrontalParametrageDistantController extends BaseController
{
    /**
     * @Route("/panierDownloadFrontalParametrageDistant", name="catalogue_geosource_panierDownloadFrontalParametrageDistant", options={"expose"=true})
     */
    public function catalogue_geosource_panierDownloadFrontalParametrageDistantAction(Request $request, $m_Idts_distant)
    {

        $metadata = new MetadataDesc();
        $metadata->setConnection($this->getCatalogueConnection('catalogue'));
        $metadata->setContainer($this->container);

        $strhtml = "";

        if (count($m_Idts_distant) > 0) {
            $conn = $this->getCatalogueConnection('catalogue');
            $dao = new DAO($conn, 'catalogue');
            if ($dao) {
                $tabProtocol = array(
                    "WWW:DOWNLOAD" => array("priority" => 4, "params" => ""),
                    "OGC:WFS" => array("priority" => 3, "params" => ""),
                    "WWW:LINK" => array("priority" => 1, "params" => ""),
                );

                $rs = $dao->BuildResultSet(
                    "select id, data from public.metadata where id in (".implode(", ", $m_Idts_distant).")"
                );

                for ($rs->First(); !$rs->EOF(); $rs->Next()) {
                    $version = "1.0";
                    $encoding = "UTF-8";
                    $xml = new \DOMDocument($version, $encoding);
                    $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
                    $metadata_data = $entete.$rs->Read(1);
                    $metadata_data = str_replace("&", "&amp;", $metadata_data);
                    $bLoad = $xml->loadXML($metadata_data);
                    if ($bLoad !== false) {
                        $title = $xml->getElementsByTagName('title')->item(0)->getElementsByTagName(
                            'CharacterString'
                        )->item(0)->nodeValue;

                        $onlineResources = $xml->getElementsByTagName('CI_OnlineResource');
                        $priority = -1;
                        $tabUrl = array();

                        for ($i = 0; $i < $onlineResources->length; $i++) {
                            $protocolTag = $onlineResources->item($i)->getElementsByTagName('protocol');
                            if ($protocolTag && $protocolTag->length > 0) {
                                $protocol = $protocolTag->item(0)->getElementsByTagName('CharacterString')->item(
                                    0
                                )->nodeValue;
                                $protocol_key = "";
                                foreach ($tabProtocol as $key => $properties) {
                                    if (strpos($protocol, $key) !== false) {
                                        $protocol_key = $key;
                                        continue;
                                    }
                                }
                                if (isset($tabProtocol[$protocol_key])) {
                                    switch ($protocol_key) {
                                        case "WWW:DOWNLOAD":
                                            $url = $onlineResources->item($i)
                                                ->getElementsByTagName('linkage')->item(0)
                                                ->getElementsByTagName('URL')->item(0)->nodeValue;
                                            $name = $onlineResources->item($i)
                                                ->getElementsByTagName("name")->item(0)->nodeValue;

                                            $tabUrl[] = array(
                                                "name" => $name,
                                                "protocol" => $protocol,
                                                "target" => "_blank",
                                                "text" => $name,
                                                "title" => "Télécharger les données",
                                                "url" => $url,
                                            );
                                            break;
                                        case "WWW:LINK":

                                            $url = $onlineResources->item($i)
                                                ->getElementsByTagName('linkage')->item(0)
                                                ->getElementsByTagName('URL')->item(0)->nodeValue;
                                            $name = $onlineResources->item($i)
                                                ->getElementsByTagName("name")->item(0)->nodeValue;

                                            $tabUrl[] = array(
                                                "name" => $name,
                                                "protocol" => $protocol,
                                                "target" => "_blank",
                                                "text" => $name,
                                                "title" => "Accéder aux données",
                                                "url" => $url,
                                            );
                                            break;
                                        case "OGC:WFS":  // WFS
                                            $uuid = $xml->getElementsByTagName('fileIdentifier')->item(
                                                0
                                            )->getElementsByTagName('CharacterString')->item(0)->nodeValue;
                                            $url = $this->getUrlForWFS($uuid, $onlineResources->item($i), $dao);
                                            $name = $onlineResources->item($i)
                                                ->getElementsByTagName("name")->item(0)->nodeValue;

                                            $tabUrl[] = array(
                                                "protocol" => $protocol,
                                                "target" => "_self",
                                                "text" => $name,
                                                "title" => "Consulter les données",
                                                "url" => "javascript:downloadWFS('".$url."', '".$rs->Read(
                                                        0
                                                    )."','".addslashes($title)."');",
                                            );
                                            break;
                                    }
                                }
                            }
                        }
                        $strhtml .= "<tr>";
                        $strhtml .= "  <td ".(count($tabUrl) > 1 ? "rowspan='".count(
                                    $tabUrl
                                )."'" : "").">".$title."</td>";
                        $strhtml .= "  <td ".(count($tabUrl) > 1 ? "rowspan='".count(
                                    $tabUrl
                                )."'" : "").">Identifiant : ".$rs->Read(0)."</td>";

                        if (count($tabUrl) > 0) {
                            for ($i = 0; $i < count($tabUrl); $i++) {
                                if ($i == 0) {
                                    $strhtml .= "<td>".$tabUrl[$i]["protocol"]."</td><td><a target=\"".$tabUrl[$i]["target"]."\" title=\"".$tabUrl[$i]["title"]."\" href=\"".$tabUrl[$i]["url"]."\">".$tabUrl[$i]["text"]."</a></td>";
                                    $strhtml .= "</tr>";
                                } else {
                                    $strhtml .= "<tr>";
                                    $strhtml .= "<td>".$tabUrl[$i]["protocol"]."</td><td><a target=\"".$tabUrl[$i]["target"]."\" title=\"".$tabUrl[$i]["title"]."\" href=\"".$tabUrl[$i]["url"]."\">".$tabUrl[$i]["text"]."</a></td>";
                                    $strhtml .= "</tr>";
                                }

                            }
                        } else {
                            $strhtml .= "<td></td><td></td></tr>";
                        }

                        //$strhtml .= "  </td>";
                    }
                }
            }
        }

        return $this->render(
            'GeosourceBundle\Default\panierDownloadFrontal_parametrage_distant_template.html.twig',
            array(
                "CARMEN_URL_SERVER_DOWNLOAD" => CARMEN_URL_SERVER_DOWNLOAD,
                "strhtml" => $strhtml,
                "direct" => ClassRessourcesDownload::encodeValue("1"),
            )
        );
    }

    /**
     * construit l'URL de consultation des données WFS
     * @return string URL
     */
    protected function getUrlForWFS($uuid, $onlineResource, $dao)
    {

        //global $dao, $PRO_GEONETWORK_DIRECTORY;
        $url = "";

        // récupère les informations du service WFS dans la métadonnée de service si elle existe

        //TODO hismail
        $xmlRelations = @file_get_contents(
            rtrim(PRO_GEONETWORK_URLBASE, "/")."/srv/fre/xml.relation?type=service&fast=false&uuid=".$uuid,
            false,
            $context
        );
        $version = "1.0";
        $encoding = "UTF-8";
        $docRelations = new \DOMDocument($version, $encoding);
        if (!empty($xmlRelations)) {
            $bLoadXML = $docRelations->loadXML($xmlRelations);
            if ($bLoadXML) {
                $relations = $docRelations->getElementsByTagName('relation');
                for ($i = 0; $i < $relations->length; $i++) {
                    if ($relations->item($i)->getElementsByTagName('title')->item(0)->nodeValue == 'Service WFS') {
                        $uuidServiceWFS = $relations->item($i)->getElementsByTagName('uuid')->item(0)->nodeValue;
                        $strSql = "select data from public.metadata where metadata.uuid = '".$uuidServiceWFS."'";
                        $rs = $dao->BuildResultSet($strSql);
                        $rs->First();
                        if (!$rs->EOF()) {
                            $data = $rs->Read(0);
                            $version = "1.0";
                            $encoding = "UTF-8";
                            $metadata_doc = new \DOMDocument($version, $encoding);
                            $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
                            $metadata_data = $entete.$data;
                            $metadata_data = str_replace("&", "&amp;", $metadata_data);
                            $metadata_doc->loadXML($metadata_data);
                            $xpath = new \DOMXpath($metadata_doc);
                            $uuidNodes = $xpath->query(
                                "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource/srv:SV_CoupledResource/srv:identifier/gco:CharacterString[.='".$uuid."']"
                            );
                            if ($uuidNodes && $uuidNodes->length > 0) {
                                $layerName = $uuidNodes->item(0)->parentNode->parentNode->getElementsByTagName(
                                    'ScopedName'
                                )->item(0)->nodeValue;
                                $getFeatureNodes = $xpath->query(
                                    "/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:containsOperations/srv:SV_OperationMetadata/srv:operationName/gco:CharacterString[.='GetFeature']"
                                );
                                if ($getFeatureNodes && $getFeatureNodes->length > 0) {
                                    $url .= $getFeatureNodes->item(0)->parentNode->parentNode->getElementsByTagName(
                                        'URL'
                                    )->item(0)->nodeValue;
                                    $url .= "service=WFS&version=1.0.0&request=getFeature&typeName=".$layerName;

                                    return $url;
                                }
                            }
                        }
                    }
                }
            }
        }

        // sinon récupère les informations du service WFS directement dans la métadonnée de donnée
        $links = $onlineResource->getElementsByTagName('linkage');
        if ($links->length > 0) {
            $url .= $links->item(0)->getElementsByTagName('URL')->item(0)->nodeValue;
            $params = "";

            $names = $onlineResource->getElementsByTagName('name');
            if ($names->length > 0) {
                if (preg_match("/service=WFS/i", $url) == 0) {
                    $params .= "&service=WFS";
                }
                if (preg_match("/version=1.0.0/i", $url) == 0) {
                    $params .= "&version=1.0.0";
                }
                if (preg_match("/request=getFeature/i", $url) == 0) {
                    $params .= "&request=getFeature";
                }
                if (preg_match(
                        "/typeName=".$names->item(0)->getElementsByTagName('CharacterString')->item(0)->nodeValue."/i",
                        $url
                    ) == 0) {
                    $params .= "&typeName=".$names->item(0)->getElementsByTagName('CharacterString')->item(
                            0
                        )->nodeValue;
                }
                $url .= (substr($url, -1) == "?" ? substr($params, 1) : $params);
            }

            return $url;
        }

        return "";
    }

}

<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use Prodige\ProdigeBundle\Controller\User;
//use Prodige\ProdigeBundle\Common\Mail\AlkMail;
use Prodige\ProdigeBundle\DAOProxy\DAO;

/**
 * @Route("/geosource")
 */
class GetHelpController extends BaseController {
    
    /**
     * @Route("/gethelp", name="geosource_get_help", methods={"GET", "OPTIONS"})
     */
    public function getHelpAction(Request $request) {
        $tabAide = array();

        $userId = User::GetUserId();

        //$dao = new DAO();
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');

        if($dao && $userId) {
            
            $rs_sdom = $dao->BuildResultSet(
                "SELECT distinct prodige_help_title, prodige_help_desc, prodige_help_url, pk_prodige_help_id ".
                "from prodige_help ph ".
                "LEFT JOIN prodige_help_group phg on ph.pk_prodige_help_id = phg.hlpgrp_fk_help_id ".
                "WHERE (phg.hlpgrp_fk_groupe_profil IN ".
                "(SELECT grpusr_fk_groupe_profil FROM grp_regroupe_usr where grpusr_fk_utilisateur =  ? ))", array($userId));
            for($rs_sdom->First(); !$rs_sdom->EOF() ; $rs_sdom->Next()) {
                $title = html_entity_decode($rs_sdom->Read(0), ENT_QUOTES, 'UTF-8');
                $desc = html_entity_decode($rs_sdom->Read(1), ENT_QUOTES, 'UTF-8');
                
                $url = $this->generateUrl('geosource_download_resource', array('id'=>$rs_sdom->Read(3), 'filename'=>basename($rs_sdom->Read(2))), UrlGeneratorInterface::ABSOLUTE_URL);
                
                $tabAide[] = array("title" => $title, "desc" => $desc, "url" => $url);
            }
        }
        return new JsonResponse($tabAide);
    }
    
    /**
     * @Route("/download/resource/{id}", name="geosource_download_resource", requirements={"id":"\d+"}, methods={"GET"})
     */
    public function downloadResource(Request $request, $id)
    {
        $userId = User::GetUserId();
        
        $conn = $this->getCatalogueConnection('catalogue');
        
        $query = "SELECT ph.prodige_help_url "
               . "FROM prodige_help ph "
               . "LEFT JOIN prodige_help_group phg ON ph.pk_prodige_help_id = phg.hlpgrp_fk_help_id "
               . "WHERE pk_prodige_help_id = :id "
               . "AND phg.hlpgrp_fk_groupe_profil IN (SELECT grpusr_fk_groupe_profil FROM grp_regroupe_usr where grpusr_fk_utilisateur = :user )"
        ;
        
        $stmt = $conn->executeQuery($query, array('id'=>$id, 'user'=>$userId));
        $url = $stmt->fetchOne();
        
        if( $url ) {
            
            $file = realpath($this->container->getParameter("PRODIGE_PATH_CATALOGUE").'/public/upload/'.$url);
            if( $file ) {
                
                return new \Symfony\Component\HttpFoundation\BinaryFileResponse($file, 200, array(
                    'Cache-Control' => 'no-cache, no-store, must-revalidate',
                    'Pragma'        => 'no-cache',
                    'Expires'       => "0",
                ));
                
            } else {
                throw $this->createNotFoundException("La ressource demandée n'existe pas, fichier non trouvé");
            }
            
        } else {
            throw $this->createNotFoundException("La ressource demandée n'existe pas ou vous n'avez pas les droits suffisants pour y accéder");
        }
    }
}

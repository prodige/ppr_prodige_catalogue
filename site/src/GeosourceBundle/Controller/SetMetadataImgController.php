<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;

/**
 * @Route("/geosource")
 */
class SetMetadataImgController extends BaseController
{
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/setMetadataImg/{uuid}/{mapfile}", name="catalogue_geosource_setMetadataImg", options={"expose"=true})
     */
    public function setMetadataImgAction(Request $request, $uuid, $mapfile)
    {

        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');

        $metadata_id = -1;
        if ($dao) {
            if ($uuid != "") {
                $query = "SELECT od FROM metadata WHERE uuid=".$uuid;
                $rs = $dao->BuildResultSet($query);
            }
        }
        $metadata_id = $rs->First()->Read(0);

        if ($metadata_id != -1) {
            $oLayerMap = ms_newMapObj(PRO_MAPFILE_PATH.$layerMapFile);

            $oLayerMap->set("width", 600);
            $oLayerMap->set("height", 600);
            $m_oImageCarte = $oLayerMap->draw();
            $m_oUrlCarte = $m_oImageCarte->saveWebImage();
            $metadata_id = substr(
                $metadata_str,
                strpos($metadata_str, "?id=") + 4,
                strlen($metadata_str) - strpos($metadata_str, "?id=") - 4
            );

            //$res_catalogue = json_decode(file_get_contents("http://".$PRO_SITE_URL."/PRRA/Services/setMetadataImg.php?metadata_id=".$metadata_id."&pathImage=".$m_oUrlCarte."&fileType=large"));
            $context = $this->createUnsecureSslContext();
            $res_catalogue = json_decode(
                file_get_contents(
                    $request->getSchemeAndHttpHost().$this->generateUrl(
                        'catalogue_geosource_updateMetadataThumbnails'
                    )."?metadata_id=".$metadata_id."&pathImage=".$m_oUrlCarte."&fileType=large",
                    false,
                    $context
                )
            );
            if ($res_catalogue != null && $res_catalogue->update_success == true) {
                $oLayerMap->set("width", 180);
                $oLayerMap->set("height", 140);
                $m_oImageCarte = $oLayerMap->draw();
                $m_oUrlCarte = $m_oImageCarte->saveWebImage();
                //$res_catalogue = json_decode(file_get_contents("http://".$PRO_SITE_URL."/PRRA/Services/setMetadataImg.php?metadata_id=".$metadata_id."&pathImage=".$m_oUrlCarte."&fileType=small"));
                $res_catalogue = json_decode(
                    file_get_contents(
                        $this->generateUrl(
                            'catalogue_geosource_updateMetadataThumbnails'
                        )."?metadata_id=".$metadata_id."&pathImage=".$m_oUrlCarte."&fileType=small"
                    ),
                    false,
                    $context
                );
                if ($res_catalogue->update_success == true) {
                    echo '{"success":true}';
                    exit();
                }
            }
        }
        echo '{"success":false}';
        exit();
    }
}

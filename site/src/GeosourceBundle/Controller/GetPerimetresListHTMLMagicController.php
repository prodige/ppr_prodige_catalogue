<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use JMS\SecurityExtraBundle\Annotation\Secure;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
//use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Page retournant la liste des perimetres matchant avec les données majic correspondant au metadata_id
 **/

/**
 * #Route("/geosource")
 */

class GetPerimetresListHTMLMagicController extends BaseController {
    /**
     * #Secure(roles="ROLE_USER")
     * #Route("/getPerimetresListHTMLMagic", name="catalogue_geosource_getPerimetresListHTMLMagic", options={"expose"=true})
     */
    public static function getPerimetresListHTMLMagic($metadataIdts) {

        //require_once("path.php");
        $rootPath  = PRO_MAPFILE_PATH."majic";

        //$metadataIdts = $request->query->get("metadataIdts");
        $tabMetadata = explode("|",$metadataIdts);
        $fileArray = array();
        //$strArray = "\$tabINSEE = array();";
        $tabINSEE = array();
        foreach($tabMetadata as $key => $value) {
            $tabINSEE = self::parseDirectory($rootPath."/".$value, "/", $tabINSEE);
        }
        //echo($strArray);
        return $tabINSEE;
    }

    /**
     * Retourne tous les répertoires, sous répertoires et fichiers du répertoire majic
     *
     * @param string $rootPath
     * @param string $seperator
     * @return array
     */
    protected static function parseDirectory($rootPath, $seperator="/", $tabINSEE) {
        //global $strArray;
        $fileArray = array();
        if(($handle = @opendir($rootPath)) !== false) {
            while(($file = readdir($handle)) !== false) {
                if($file != '.' && $file != '..') {
                    if(is_dir($rootPath.$seperator.$file)) {
                        //$array = parseDirectory($rootPath.$seperator.$file);
                        //$fileArray=array_merge($array,$fileArray);
                        if(strlen($file) == 5)
                            //$strArray.= "\$tabINSEE[]= '".$file."';";
                            $fileArray[] = $file;
                    }
                    else {
                        //$fileArray[]=$rootPath.$seperator.$file;
                    }
                }
            }
        }
        return $fileArray;
    }

}

<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use ProdigeCatalogue\GeosourceBundle\Common\MetadataDesc;
use ProdigeCatalogue\GeosourceBundle\Common\ClassRessourcesDownload;
use ProdigeCatalogue\GeosourceBundle\Common\PanierDownloadFrontalParametrageCommonFunctions;

use Prodige\ProdigeBundle\Controller\BaseController;

//use Prodige\ProdigeBundle\Controller\User;
//use Prodige\ProdigeBundle\DAOProxy\DAO;
//
//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/geosource")
 */
class PanierDownloadFrontalParametrageRasterController extends BaseController
{
    /**
     * * @Route("/panierDownloadFrontalParametrageRaster", name="catalogue_geosource_panierDownloadFrontalParametrageRaster", options={"expose"=true})
     */
    public function catalogue_geosource_panierDownloadFrontalParametrageRasterAction(
        Request $request,
        $service_idx,
        $userEmail,
        $strData_raster,
        $strMetadataId_raster,
        $tabCoucheName_raster,
        $raster_format,
        $is_raster_hr,
        $raster_srid,
        $m_Idts_raster,
        $extract_tolerance,
        $m_serveur
    ) {

        $metadata = new MetadataDesc();
        $conn = $this->getCatalogueConnection('catalogue');
        $metadata->setConnection($conn);
        $metadata->setContainer($this->container);

        $infoLayers = array();
        $cgu = '';
        foreach ($tabCoucheName_raster as $key => $value) {
            $infoLayers[] = array("id" => $key, "name" => addslashes($value["name"]), "server" => $value["server"]);
            $query = "SELECT couchd_cgu_display, couchd_cgu_message "
                . "FROM couche_donnees cd "
                . "JOIN fiche_metadonnees fd ON fd.fmeta_fk_couche_donnees = cd.pk_couche_donnees "
                . "WHERE fmeta_id = :id ";
            $stmt = $conn->executeQuery($query, array('id'=>$key));
            $results = $stmt->fetchAllAssociative();
            if ($results[0]['couchd_cgu_display'] && !empty($results[0]['couchd_cgu_message'])) {
                $cgu .= "<h3>Conditions générales d'utilisation pour la couche ". addslashes($value["name"]) . "</h3>";
                $cgu .= "<p>". $results[0]['couchd_cgu_message'] . "</p>";
            }
        }

        $formats = array();
        $m_Format_raster = $is_raster_hr ? ClassRessourcesDownload::getFormats_raster_hr(
            $raster_format
        ) : ClassRessourcesDownload::getFormats_raster();
        for ($idx = 0; $idx < count($m_Format_raster); $idx++) {
            list($nomFormat, $valFormat) = explode("||", $m_Format_raster[$idx]);
            $formats[] = array(ClassRessourcesDownload::encodeValue($valFormat), $nomFormat);
        }

        $projections = array();
        $m_Projection = $is_raster_hr ? ClassRessourcesDownload::getProjections2(
            $raster_srid
        ) : ClassRessourcesDownload::getProjections();
        for ($idx = 0; $idx < count($m_Projection); $idx++) {
            list($nomProj, $valProj) = explode("|", $m_Projection[$idx]);
            $projections[] = array(ClassRessourcesDownload::encodeValue($valProj), $nomProj);
        }



        $str3 = '&nbsp';
        if ($is_raster_hr) {
            if ($cgu != '') {
                if (count($m_Idts_raster) > 1) {
                    $str3 .= "<script language='javascript'>Ext.Msg.alert(\"Téléchargement\", \"Veuillez réorganiser votre panier pour avoir une seule couche raster haute résolution.<br>Le choix des formats et projections est contraint pour les données haute résolution.\");</script>";
                } else {
                    $str3 .= '<div id="cgu-content" style="display:none;">'.$cgu.'</div>';
                    $str3 .= '<input type="Button" value="Exécution différée" onclick="affCGU(\'execute_telecharger_raster(false, true);\', \'verifCtrl_raster()\')">';
                }
            } elseif (PRO_CATALOGUE_TELECHARGEMENT_LICENCE == "on") {
                if (count($m_Idts_raster) > 1) {
                    $str3 .= "<script language='javascript'>Ext.Msg.alert(\"Téléchargement\", \"Veuillez réorganiser votre panier pour avoir une seule couche raster haute résolution.<br>Le choix des formats et projections est contraint pour les données haute résolution.\");</script>";
                } else {
                    $str3 .= '<input type="Button" value="Exécution différée" onclick="affLicence(\'execute_telecharger_raster(false,true);\', \''.PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL.'\', \'verifCtrl_raster()\')">';
                }
            } else {
                if (count($m_Idts_raster) > 1) {
                    $str3 .= "<script language='javascript'>Ext.Msg.alert(\"Téléchargement\", \"Veuillez réorganiser votre panier pour avoir une seule couche raster haute résolution.<br>Le choix des formats et projections est contraint pour les données haute résolution.\");</script>";
                } else {
                    $str3 .= '<input type="Button" value="Exécution différée" onclick="execute_telecharger_raster(false,true);">';
                }
            }
        } else {
            if ($cgu != '') {
                $str3 .= '<div id="cgu-content" style="display:none;">'.$cgu.'</div>';
                $str3 .= '<input type="Button" value="Exécution directe" onclick="affCGU(\'execute_telecharger_raster(true,false);\', \'verifCtrl_raster()\')"> &nbsp; <input type="Button" value="Exécution différée" onclick="affCGU(\'execute_telecharger_raster(false,false);\', \'verifCtrl_raster()\')">';
            } elseif (PRO_CATALOGUE_TELECHARGEMENT_LICENCE == "on") {
                $str3 .= '<input type="Button" value="Exécution directe" onclick="affLicence(\'execute_telecharger_raster(true,false);\', \''.PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL.'\', \'verifCtrl_raster()\')"> &nbsp; <input type="Button" value="Exécution différée" onclick="affLicence(\'execute_telecharger_raster(false,false);\', \''.PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL.'\', \'verifCtrl_raster()\')">';
            } else {
                $str3 .= '<input type="Button" value="Exécution directe" onclick="execute_telecharger_raster(true,false);"> &nbsp; <input type="Button" value="Exécution différée" onclick="execute_telecharger_raster(false,false);">';
            }
        }

        $str4 = "";
        $tabSrv = explode("|", $m_serveur);
        $str4 .= PanierDownloadFrontalParametrageCommonFunctions::presenterServeur(
            $tabSrv[0],
            $m_Idts_raster,
            0,
            "raster"
        );

        $ar_parameters = array(
            "CARMEN_URL_SERVER_DOWNLOAD" => CARMEN_URL_SERVER_DOWNLOAD,
            "service_idx" => $service_idx,
            "email" => ClassRessourcesDownload::encodeValue($userEmail),
            "data" => ClassRessourcesDownload::encodeValue($strData_raster),
            "metadata_id" => ClassRessourcesDownload::encodeValue($strMetadataId_raster),
            "infoLayers" => $infoLayers,
            "params_direct" => ClassRessourcesDownload::encodeValue("1"),
            "form_action" => $request->getRequestUri(),
            "formats" => $formats,
            "projections" => $projections,
            "m_Idts_raster" => implode($m_Idts_raster),
            "extract_tolerance" => $extract_tolerance,
            "str3" => $str3,
            "str4" => $str4,
        );

        return $this->render(
            'GeosourceBundle\Default\panierDownloadFrontal_parametrage_raster_template.html.twig',
            $ar_parameters
        );

    }

}

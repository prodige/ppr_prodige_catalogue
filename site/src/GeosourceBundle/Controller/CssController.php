<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/geosource")
 */
class CssController extends BaseController {
    
    /**
     * @Route("/css/get_css_file/{css_file}", name="catalogue_geosource_get_css_file", options={"expose"=true} )
     * #Method({"GET", "POST"})
     * */
    public function getCssFileAction(Request $request, $css_file) {
        $response = new Response();
        $response->headers->add(array(
            'Content-type' => 'text/css'
        ));
        
        $params = $this->getProColorParams();
        
        return $this->render('GeosourceBundle/css/'.$css_file.'.css.twig', $params, $response);
    }
}

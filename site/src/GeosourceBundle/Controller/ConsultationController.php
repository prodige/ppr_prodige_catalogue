<?php
namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\DAOProxy\DAO;
//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Contenu de l'onglet cartes
 * 
 * @author Alkante
 */

/**
 * @Route("/geosource")
 */
class ConsultationController extends BaseController
{

    /**
     * 
     * @Route("/consultation", name="catalogue_geosource_consultation", options={"expose"=true})
     */
    public function consultationAction(Request $request)
    {
        $id = null;
        if ($request->get('id', null))
            $id = $request->get('id');
        if (! is_null($id)) {
            
            $user = User::GetUser();
            $userId = User::GetUserId();
            $url = "";
            
            $conn = $this->getCatalogueConnection('catalogue');
            $dao = new DAO($conn, 'catalogue');
            if ($dao) {
                $query = "SELECT PK_CARTE_PROJET, STKCARD_PATH, CARTP_FORMAT, ACCS_ADRESSE, PATH_CONSULTATION, PATH_CARTE_STATIQUE, ACCS_SERVICE_ADMIN , ACCS_ADRESSE_ADMIN FROM CARTES_SDOM WHERE FMETA_ID = ?";
                $rs = $dao->BuildResultSet($query, array(
                    $id
                ));
                if ($rs) {
                    $rs->First();
                    if ($rs->Read(3) != "") {
                        if ($rs->Read(2))
                            $url = $this->container->getParameter('PRODIGE_URL_FRONTCARTO') . $rs->Read(5) . $rs->Read(1);
                        else
                            $url = $this->container->getParameter('PRODIGE_URL_FRONTCARTO') . $rs->Read(4) . $rs->Read(6) . "/" . $rs->Read(1);
                    } 
                }
            }            
            if (! empty($url)) {
                // vérification des droits sur un des domaines de la carte
                $query = "SELECT DOM_NOM, SSDOM_NOM, PK_CARTE_PROJET FROM CARTES_SDOM WHERE FMETA_ID =?";
                $rs = $dao->BuildResultSet($query, array(
                    $id
                ));
                $bAllow = false;
                for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                    $domaine = $rs->read(0);
                    $sous_domaine = $rs->read(1);
                    $objetCarte = $rs->read(2);
                    
                    // vérification de l'autorisation sur le domaine et sur l'objet
                    $bAllow = $user->HasTraitement('NAVIGATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_CONSULTATION, $objetCarte, PRO_OBJET_TYPE_CARTE);
                    
                    if ($bAllow)
                        break;
                }
                // vérification des restrictions par competences
                $bAllow = $bAllow && $user->HasTraitementCompetence('NAVIGATION', $objetCarte);
                if ($bAllow) {
                    header('location:' . $url);
                    exit(0);
                } else {
                    if($userId === User::getUserInternet()){
                       $catalogue_url = $this->container->getParameter("PRODIGE_URL_CATALOGUE");
                       $cas_url = "https://".$this->container->getParameter("cas_host").":".$this->container->getParameter("cas_port").$this->container->getParameter("cas_context");
                       $url = $cas_url."/login?service=".urlencode($catalogue_url."/geosource/consultation?id=".$id); 
                       return $this->redirect($url); // redirection vers le cas
                    } else {
                        throw new \Exception("Vous n'avez pas les droits suffisants pour accéder à la visualisation de cette carte.");
                        exit(0);
                    }
                }
            } else {
                // cas Harvested, récupération du premier link trouvé
                $strSql = "select data from public.metadata where isharvested ='y' and metadata.id = ?";
                $rs = $dao->BuildResultSet($strSql, array($id));
                for ($rs->First(); ! $rs->EOF(); $rs->Next()) {
                    $data = $rs->Read(0);
                    $version = "1.0";
                    $encoding = "UTF-8";
                    $metadata_doc = new \DOMDocument($version, $encoding);
                    // chargement de la métadonnée
                    $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
                    $metadata_data = $entete . $data;
                    $metadata_data = str_replace("&", "&amp;", $metadata_data);
                    // echo $metadata_data;
                    $metadata_doc->loadXML(($metadata_data));
                    $xpath = new \DOMXpath($metadata_doc);
                    $linkedUrl = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:linkage/gmd:URL");
                    foreach ($linkedUrl as $key => $node) {
                        
                        if ($node->nodeValue && preg_match('%^((https?://)|(www\.))([a-z0-9-].?)+(:[0-9]+)?(/.*)?$%i', $node->nodeValue)) {
                            header('location:' . $node->nodeValue);
                            exit();
                        } else {
                            throw new \Exception("Vous n'avez pas les droits suffisants pour accéder à la visualisation de cette carte.");

                        }
                    }
                    throw new \Exception("Vous n'avez pas les droits suffisants pour accéder à la visualisation de cette carte.");
                
                }
                throw new \Exception("Vous n'avez pas les droits suffisants pour accéder à la visualisation de cette carte.");
                
            }
        }
    }
}

<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Routing\Annotation\Route; 
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use Prodige\ProdigeBundle\Controller\User;

/**
 * @Route("/geosource")
 */
class DataController extends BaseController  
{  
   /**
    * @Route("/getDataHeaders/{uuid}", options={"expose"=true}, name="getDataHeaders", methods={"GET"})
    */
    public function getDataHeadersAction(Request $request, $uuid) 
    {
        $data = array();
        
        $CATALOGUE = $this->getCatalogueConnection("catalogue, public");
        $PRODIGE = $this->getProdigeConnection("public");                   
        
        $strSQL = "SELECT cd.couchd_emplacement_stockage, cd.couchd_type_stockage
                  FROM metadata m 
                  INNER JOIN fiche_metadonnees fm 
                  ON fm.fmeta_id = m.id::text 
                  INNER JOIN couche_donnees cd 
                  ON fm.fmeta_fk_couche_donnees = cd.pk_couche_donnees 
                  WHERE uuid = :uuid";
        
        $result = $CATALOGUE->fetchArray($strSQL, array("uuid" => $uuid));

        $couchd_emplacement_stockage = $result[0];        
                
        $strSQL = "SELECT array_to_string(ARRAY(SELECT '' || c.column_name
                    FROM information_schema.columns As c
                    WHERE table_name = '$couchd_emplacement_stockage' 
                    AND  c.column_name NOT IN('the_geom', 'locked', '_edit_datemaj', '_edit_datecrea')), ',')";
        
        $result = $PRODIGE->fetchArray($strSQL);

        if(!empty($result[0])) {
             $data = explode(",", $result[0]);
        }
        
        return new JsonResponse($data);    
    }  
  
   /**
    * @Route("/getData/{uuid}", options={"expose"=true}, name="getData", methods={"GET", "POST"})
    */
    public function getDataAction(Request $request, $uuid) 
    {      
        $columns = $request->request->get('columns', array());
      
        if(count($columns) > 0) {
          
            $CATALOGUE = $this->getCatalogueConnection("catalogue, public");
            $PRODIGE = $this->getProdigeConnection("public");             
          
            $strSQL = "SELECT cd.pk_couche_donnees, cd.couchd_emplacement_stockage, cd.couchd_type_stockage
                      FROM metadata m 
                      INNER JOIN fiche_metadonnees fm 
                      ON fm.fmeta_id = m.id::text 
                      INNER JOIN couche_donnees cd 
                      ON fm.fmeta_fk_couche_donnees = cd.pk_couche_donnees 
                      WHERE uuid = :uuid";

            $result = $CATALOGUE->fetchArray($strSQL, array("uuid" => $uuid));

            $pk_couche_donnees            = $result[0];
            $couchd_emplacement_stockage  = $result[1];
            $couchd_type_stockage         = $result[2]; 
            
            
            if(is_int($couchd_type_stockage)) {
              $authorized_data_types = array(-4, -3, 1);
              if(in_array($couchd_type_stockage, $authorized_data_types)) {
                $getParams = array("uuid"         => $uuid,
                                   "TRAITEMENTS" => "NAVIGATION");
                $getParams['OBJET_TYPE']  = "dataset";

                $serv = $this->container->get('prodige.verifyrights');           

                $req = new \Symfony\Component\HttpFoundation\Request($getParams);
                $rights = $serv->verifyRightsAction($req, true);

                if(!@$rights['NAVIGATION']) {
                   $msg = "Accès interdit aux données ";
                }
                else {                  
                    $strSQL = "SELECT c.column_name, c.data_type
                               FROM information_schema.columns As c
                               WHERE table_name = '$couchd_emplacement_stockage' 
                               AND  c.column_name NOT IN('the_geom', 'locked', '_edit_datemaj', '_edit_datecrea'); ";
                    $ar_fields = $PRODIGE->fetchAllAssociative($strSQL);  

                    /*
                     * Paging
                     */
                    $sLimit = "";
                    if($request->request->get('length')  && $request->request->get('length') != -1) {
                        $sLimit = " OFFSET ".intval($request->request->get('start'))."  LIMIT ".intval($request->request->get('length'));
                    }

                    /*
                     * Ordering
                     */
                    $order = '';
                    $orderBy = array();
                    if($request->request->get('order') && count($request->request->get('order')) > 0) {
                      for($i=0, $ien=count($request->request->get('order')) ; $i<$ien ; $i++) {        
                        $columnIdx = intval($request->request->get('order')[$i]['column']);                        
                        $requestColumn = $columns[$columnIdx];
                        if($requestColumn['orderable'] == 'true') {
                          $dir = $request->request->get('order')[$i]['dir'] === 'asc' ? 'ASC' : 'DESC';                          
                          $orderBy[] = ''.$ar_fields[$columnIdx]['column_name'].' '.$dir;
                        }
                      }                      
                      if(count($orderBy) > 0) {
                          $order = 'ORDER BY '.implode(', ', $orderBy);
                      }
                    }

                    //vérification des restricitions territoriales
                    $params = array();
                    $the_geom = '';  
                    
                    $user = User::GetUser();
                    if($user) {
                        if($pk_couche_donnees) {
                            $tabTerr = $user->GetTraitementTerritoire('NAVIGATION', $pk_couche_donnees);
                            if($tabTerr != false) {
                                $params["perimetreTable"] = 'prodige_perimetre';                                            
                                $params["perimetreInfo"] = $tabTerr; 
                                $params["layerTable"] = $couchd_emplacement_stockage;
                            }
                            if(isset($params["perimetreTable"]) && isset($params["perimetreInfo"]) && isset($params["layerTable"])) {  
                                $strPerimetresIds = "";
                                if(is_array($params["perimetreInfo"]) && count($params["perimetreInfo"]) > 0) {
                                    foreach($params["perimetreInfo"] as $id => $perimetre){
                                        $strPerimetresIds.= "'".$perimetre[0]."',";
                                    }
                                    $strPerimetresIds = substr($strPerimetresIds, 0, -1);
                                    $the_geom = " INNER JOIN prodige_perimetre b ON (a.the_geom && b.the_geom AND st_intersects(a.the_geom, b.the_geom)) where b.". 
                                                $params["perimetreInfo"][0][2]." in (".$strPerimetresIds.")  ";                                    
                                }
                            }                         
                        }                       
                    }        
                    
                    $strSQL = "SELECT array_to_string(ARRAY(SELECT 'a.\"' || c.column_name || '\"'
                                FROM information_schema.columns As c
                                WHERE table_name = '$couchd_emplacement_stockage' 
                                AND  c.column_name NOT IN('the_geom', 'locked', '_edit_datemaj','_edit_datecrea')), ',')";
                    $fields = $PRODIGE->fetchArray($strSQL)[0];                    
                    
                    $strSQL = "SELECT a.\"".$ar_fields[0]['column_name']."\" FROM $couchd_emplacement_stockage a $the_geom ; ";
                    
                    $recordsTotal = $recordsFiltered  = $PRODIGE->executeQuery($strSQL)->rowCount();
                    
                    $strSQL = "SELECT $fields FROM $couchd_emplacement_stockage a $the_geom $order $sLimit; ";                                        
                    $stmt = $PRODIGE->executeQuery($strSQL, array(), array("filter_values" => $PRODIGE::PARAM_STR_ARRAY));
                    $data = $stmt->fetchAllAssociative(\PDO::FETCH_NUM);                    
                    
                    /*
                     * Filtering
                     * NOTE This assumes that the field that is being searched on is a string typed field (ie. one
                     * on which ILIKE can be used). Boolean fields etc will need a modification here.
                     */
                    $where = '';
                    $globalSearch = array();                    
                    if($request->request->get('search') && $request->request->get('search')['value'] != '' ) {
                      $str = $request->request->get('search')['value'];                      
                      for($i=0, $ien=count($columns) ; $i<$ien ; $i++ ) {
                        $requestColumn = $columns[$i];
                        if($requestColumn['searchable'] == 'true') {
                          if(in_array($ar_fields[$i]['data_type'], array('character varying', 'character', 'text'))) {
                              $globalSearch[] = "".$ar_fields[$i]['column_name']." ILIKE '%".pg_escape_string($str)."%' ";
                          }
                          elseif(in_array($ar_fields[$i]['data_type'], array('smallint', 'bigint', 'integer', 'real', 'numeric', 'double precision'))) {
                              if(intval($str) && !\DateTime::createFromFormat('Y-m-d',$str)) {
                                  $globalSearch[] = "".$ar_fields[$i]['column_name']." = ".$str. " ";
                              }
                          }
                          elseif($ar_fields[$i]['data_type'] == 'date') {                            
                            if(\DateTime::createFromFormat('Y-m-d',$str)) {         
                              $globalSearch[] = "".$ar_fields[$i]['column_name']." = '".$str. "' ";
                            }
                          }
                          elseif($ar_fields[$i]['data_type'] == 'USER-DEFINED') {
                            if(!preg_match("/(\d{2})-(\d{2})-(\d{4})$/", $str)){         
                              $globalSearch[] = "".$ar_fields[$i]['column_name']."::text = '".$str. "' ";
                            }
                          }
                          elseif($ar_fields[$i]['data_type'] == 'ARRAY') {
                            if(!preg_match("/(\d{2})-(\d{2})-(\d{4})$/", $str)){                                       
                              $globalSearch[] = "'".$str. "' = ANY (".$ar_fields[$i]['column_name']."::text[]) ";
                            }
                          }                     
                        }
                      }

                      // Combine the filters into a single string                      
                      if(count($globalSearch) > 0) {
                        $where = 'WHERE ('.implode(' OR ', $globalSearch).')';                        
                        $strSQL = "SELECT $fields FROM $couchd_emplacement_stockage a $where $order; ";
                        $stmt = $PRODIGE->executeQuery($strSQL, array(), array("filter_values" => $PRODIGE::PARAM_STR_ARRAY));
                        $data = $stmt->fetchAllAssociative(\PDO::FETCH_NUM);
                        $recordsFiltered = count($data);                       
                      
                        $strSQL = "SELECT $fields FROM $couchd_emplacement_stockage a $where $the_geom $order $sLimit; ";                                                
                        $stmt = $PRODIGE->executeQuery($strSQL, array(), array("filter_values" => $PRODIGE::PARAM_STR_ARRAY));
                        $data = $stmt->fetchAllAssociative(\PDO::FETCH_NUM);                        
                      }
                    }                    

                    $datatable = array("draw"             => $request->request->get('draw'),
                                       "recordsFiltered"  => $recordsFiltered,
                                       "recordsTotal"     => $recordsTotal,
                                       "data"             => $data);
                    
                    return new JsonResponse($datatable, 200, array('Access-Control-Allow-Origin' => '*'));                    
                }
              }
              else {
                $msg = "La métadonnée : $uuid n'est pas une table, une vue ou même une couche ";
              }
            }
            else {
              $msg = "couchd_type_stockage de la métadonnée : $uuid a un format bizarre ";
            }             
    }
    
    return new JsonResponse(array("succes" => false, "msg" => (isset($msg) ? $msg : "L'appel doit se faire dans le cadre d'un datatable ")), 500, array('Access-Control-Allow-Origin' => '*'));
  }
    
  /**
   * @Route("/testGetData", options={"expose"=true}, name="testGetData", methods={"GET"})
   */
   public function testGetDataAction(Request $request) 
   {
       return $this->render('ProdigeFrontCartoWebBundle\Default\testGetData.html.twig');
   }
}

<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use ProdigeCatalogue\GeosourceBundle\Common\MetadataDesc;
use ProdigeCatalogue\GeosourceBundle\Common\ClassRessourcesDownload;
use ProdigeCatalogue\GeosourceBundle\Common\PanierDownloadFrontalParametrageCommonFunctions;

use Prodige\ProdigeBundle\Controller\BaseController;

//use Prodige\ProdigeBundle\Controller\User;
//use Prodige\ProdigeBundle\DAOProxy\DAO;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/geosource")
 */
class PanierDownloadFrontalParametrageTableController extends BaseController
{
    /**
     * @Route("/panierDownloadFrontalParametrageTable", name="catalogue_geosource_panierDownloadFrontalParametrageTable", options={"expose"=true})
     */
    public function catalogue_geosource_panierDownloadFrontalParametrageTableAction(
        Request $request,
        $service_idx,
        $userEmail,
        $strTablename_table,
        $strMetadataId_table,
        $tabCoucheName_table,
        $m_Idts_table,
        $m_serveur,
        $tabRestrictTerr
    ) {

        $metadata = new MetadataDesc();
        $conn = $this->getCatalogueConnection('catalogue');
        $metadata->setConnection($conn);
        $metadata->setContainer($this->container);

        $infoLayers = " ";
        $cgu = '';
        foreach ($tabCoucheName_table as $key => $value) {
            $infoLayers .= "{ id : '".$key."', name : '".addslashes(
                    $value["name"]
                )."', server : '".$value["server"]."'},";
            $query = "SELECT couchd_cgu_display, couchd_cgu_message "
                . "FROM couche_donnees cd "
                . "JOIN fiche_metadonnees fd ON fd.fmeta_fk_couche_donnees = cd.pk_couche_donnees "
                . "WHERE fmeta_id = :id ";
            $stmt = $conn->executeQuery($query, array('id'=>$key));
            $results = $stmt->fetchAllAssociative();
            if ($results[0]['couchd_cgu_display'] && !empty($results[0]['couchd_cgu_message'])) {
                $cgu .= "<h3>Conditions générales d'utilisation pour la couche ". addslashes($value["name"]) . "</h3>";
                $cgu .= "<p>". $results[0]['couchd_cgu_message'] . "</p>";
            }
        }
        $infoLayers = substr($infoLayers, 0, -1);

        $str1 = 'var data = "[";'."\n";
        $m_Format = ClassRessourcesDownload::getFormats_table();
        for ($idx = 0; $idx < count($m_Format); $idx++) {
            list($nomFormat, $valFormat) = explode("||", $m_Format[$idx]);
            if ($idx < count($m_Format) - 1) {
                $str1 .= "data += '[\"".ClassRessourcesDownload::encodeValue(
                        $valFormat
                    )."\", \"".$nomFormat."\"], ';\n";
            } else {
                $str1 .= "data += '[\"".ClassRessourcesDownload::encodeValue($valFormat)."\", \"".$nomFormat."\"]';\n";
            }
        }
        $str1 .= 'data += "]";';

        $str2 = "";
        if ($cgu != '') {
            $str2 .= '<div id="cgu-content" style="display:none;">'.$cgu.'</div>';
            $str2 .= '<input type="Button" value="Exécution directe" onclick="affCGU(\'execute_telecharger_table(true);\', \'verifCtrl_table()\')"> &nbsp; <input type="Button" value="Exécution différée" onclick="affLicence(\'execute_telecharger_table(false), \'verifCtrl_table()\')">';
        }elseif (PRO_CATALOGUE_TELECHARGEMENT_LICENCE == "on") {
            $str2 .= '<input type="Button" value="Exécution directe" onclick="affLicence(\'execute_telecharger_table(true);\', \''.PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL.'\', \'verifCtrl_table()\')"> &nbsp; <input type="Button" value="Exécution différée" onclick="affLicence(\'execute_telecharger_table(false), \''.PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL.'\', \'verifCtrl_table()\')">';
        } else {
            $str2 .= '<input type="Button" value="Exécution directe" onclick="execute_telecharger_table(true);"> &nbsp; <input type="Button" value="Exécution différée" onclick="execute_telecharger_table(false);">';
        }

        $str3 = "";
        $tabSrv = explode("|", $m_serveur);
        $str3 .= PanierDownloadFrontalParametrageCommonFunctions::printFormHeader();
        $str3 .= PanierDownloadFrontalParametrageCommonFunctions::presenterServeur(
            $tabSrv[0],
            $m_Idts_table,
            0,
            "table",
            false,
            array(),
            (isset($tabRestrictTerr) ? $tabRestrictTerr : array())
        );

        $ar_parameters = array(
            "CARMEN_URL_SERVER_DOWNLOAD" => CARMEN_URL_SERVER_DOWNLOAD,
            "service_idx" => $service_idx,
            "email" => ClassRessourcesDownload::encodeValue($userEmail),
            "data" => ClassRessourcesDownload::encodeValue("POSTGIS_DATA:".$strTablename_table),
            "metadata_id" => ClassRessourcesDownload::encodeValue($strMetadataId_table),
            "infoLayers" => $infoLayers,
            "form_action" => $request->getRequestUri(),
            "params_direct" => ClassRessourcesDownload::encodeValue("1"),
            "str1" => $str1,
            "m_Idts_table" => implode($m_Idts_table),
            "str2" => $str2,
            "str3" => $str3,
        );

        return $this->render(
            'GeosourceBundle\Default\panierDownloadFrontal_parametrage_table_template.html.twig',
            $ar_parameters
        );

    }

}

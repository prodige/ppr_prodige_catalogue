<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use ProdigeCatalogue\GeosourceBundle\Common\MetadataDesc;
use ProdigeCatalogue\GeosourceBundle\Common\ClassRessourcesDownload;
use ProdigeCatalogue\GeosourceBundle\Common\PanierDownloadFrontalParametrageCommonFunctions;

use Prodige\ProdigeBundle\Controller\BaseController;

//use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\DAOProxy\DAO;

//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @Route("/geosource")
 */
class PanierDownloadFrontalParametrageMagicController extends BaseController
{
    /**
     * @Route("/panierDownloadFrontalParametrageMagic", name="catalogue_geosource_panierDownloadFrontalParametrageMagic", options={"expose"=true})
     */
    public function catalogue_geosource_panierDownloadFrontalParametrageMagicAction(
        Request $request,
        $m_Idts_majic,
        $service_idx,
        $userId,
        $userEmail,
        $strMetadataId_majic,
        $tabCoucheName_majic,
        $m_serveurs
    ) {

        $metadata = new MetadataDesc();
        $metadata->setConnection($this->getCatalogueConnection('catalogue'));
        $metadata->setContainer($this->container);

        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');

        //récupération des codes INSEE possibles (au regard des métadonnées téléchargées)

        //eval("\$tabINSEE = ".file_get_contents((isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on" ? "https" : "http")."://".$accs_adress."/HTML_MAJIC/getPerimetresList.php?metadataIdts=".implode("|", $m_Idts_majic)));

        $tabINSEE = GetPerimetresListHTMLMagicController::getPerimetresListHTMLMagic(implode("|", $m_Idts_majic));
        $query = 'SELECT zonage_field_id, zonage_field_name, zonage_nom from zonage where zonage_minimal = 1';
        $rs = $dao->BuildResultSet($query);
        if ($rs->GetNbRows() > 1) {
            echo "erreur, plus d'un enregistrement dans la table zonage avec zonage_minimal=1 ";
            //die();
            exit();
        }
        for ($rs->First(); !$rs->EOF(); $rs->Next()) {
            $tabParam["init_zonage_field_id"] = $rs->Read(0);
            $tabParam["init_zonage_field_name"] = $rs->Read(1);
            $zonage_init_nom = $rs->Read(2);
        }

        $query = "select distinct perimetre.perimetre_code, zonage.zonage_field_id, perimetre.perimetre_nom, zonage.zonage_nom from usr_accede_perimetre ".
            " left join perimetre on usr_accede_perimetre.usrperim_fk_perimetre  = perimetre.pk_perimetre_id".
            " inner join zonage on perimetre.perimetre_zonage_id = zonage.pk_zonage_id".
            " where usr_accede_perimetre.usrperim_fk_utilisateur = ?";
        $rs = $dao->BuildResultSet($query, array($userId));
        $strTokenMetadataAllowed = "";
        for ($rs->First(); !$rs->EOF(); $rs->Next()) {
            $tabPerimretreAllowed[$rs->Read(0)]["zonage_nom"] = $rs->Read(3);
            $tabPerimretreAllowed[$rs->Read(0)]["zonage_field_id"] = $rs->Read(1);
            $tabPerimretreAllowed[$rs->Read(0)]["perimetre_nom"] = $rs->Read(2);
            $tabParam["perimetreAllowed"][] = $rs->Read(0)."|".$rs->Read(1);
        }

        if (count($tabINSEE) > 0) {
            $tabParam["tabINSEE"] = implode("|", $tabINSEE);

            //$postdata = http_build_query($tabParam);
            //$opts = array(
            //    'http'=>array(
            //        'method'  => 'POST',
            //        'header'  => 'Content-type: application/x-www-form-urlencoded',
            //        'content' => $postdata
            //    )
            //);

            //$context  = stream_context_create($opts);

            //eval(file_get_contents((isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on" ? "https" : "http")."://".$accs_adress."/PRRA/Administration/Administration/Perimetres/PerimetresGetList.php", false, $context));
            $tabINSEEAllowed = GetPerimetresListAdministrationPerimetreController::getPerimetresListAdministrationPerimetre(
                $tabParam["tabINSEE"],
                $tabParam["init_zonage_field_id"],
                $tabParam["init_zonage_field_name"],
                $tabParam["perimetreAllowed"]
            );
        }

        $bIsAllowed = false;
        $strSelectZonage = "";
        $strSelectPerim = "tabPerim = new Array();";

        //liste des perimetres autorisés
        if (!empty($tabPerimAllowed)) {
            $tabZonage = array();
            foreach ($tabPerimAllowed as $key => $value) {
                $zonage_field_id = $tabPerimretreAllowed[$key]["zonage_field_id"];
                if (!in_array($zonage_field_id, $tabZonage)) {
                    $strSelectPerim .= "tabPerim['".$zonage_field_id."'] = new Array();";
                    $tabZonage[] = $zonage_field_id;
                    $strSelectZonage .= "<option value='".$zonage_field_id."'>".$tabPerimretreAllowed[$key]["zonage_nom"]."</option>";
                }
                $strSelectPerim .= "tabPerim['".$zonage_field_id."']['".implode(
                        "¤",
                        $value
                    )."'] = '".$tabPerimretreAllowed[$key]["perimetre_nom"]."';";
            }
        }
        //liste des communes autorisées
        if (!empty($tabINSEEAllowed)) {
            //aucun insee autorisé => pas de téléchargement
            $bIsAllowed = true;
            $strSelectZonage .= "<option value='".$tabParam["init_zonage_field_id"]."'>".$zonage_init_nom."</option>";
            $strSelectPerim .= "tabPerim['".$tabParam["init_zonage_field_id"]."'] = new Array();";
            foreach ($tabINSEEAllowed as $code_insee => $nom_com) {
                $strSelectPerim .= "tabPerim['".$tabParam["init_zonage_field_id"]."']['".$code_insee."'] = '".addslashes(
                        $nom_com
                    )."';";
            }
        }

        $str1 = " ";
        foreach ($tabCoucheName_majic as $key => $value) {
            $str1 .= "{ id : '".$key."', name : '".$value["name"]."', server : '".$value["server"]."'},";
        }
        $str1 = substr($str1, 0, -1);

        $str2 = "<select name='zonage' id= 'zonage' onchange='changeZonage(this);'>".$strSelectZonage."</select>";

        $str3 = "<select name='perimetre' id='perimetre'> </select>";

        $str4 = "";
        if (PRO_CATALOGUE_TELECHARGEMENT_LICENCE == "on") {
            $str4 .= '<input type="Button" value="Exécution différée" onclick="affLicence(\'execute_telecharger_majic();\', \''.PRO_CATALOGUE_TELECHARGEMENT_LICENCE_URL.'\', true)">';
        } else {
            $str4 .= '<input type="Button" value="Exécution différée" onclick="execute_telecharger_majic()">';
        }

        $str5 = "";
        for ($idxSrv = 0; $idxSrv < count($m_serveurs); $idxSrv++) {
            $tabSrv = explode("|", $m_serveurs[$idxSrv]);
            $str5 .= PanierDownloadFrontalParametrageCommonFunctions::presenterServeur(
                $tabSrv[0],
                $m_Idts_majic,
                $idxSrv
            );
        }

        $str6 =
            '<script language="javascript">var bShowTabMajic = true;</script><div id="downloadBlock_majic" class="downloadBlock" align="center"><div class="sousTitre">
                 <span> Données MAJIC </span>
                 </div>
                    <center>
                        <strong> <i> Le panier ne contient aucune donnée majic que vous puissiez télécharger ! </i> </strong>
                        <br>
                        <br>
                    </center>
                </div>';


        if ($bIsAllowed) {
            $ar_parameters = array(
                "CARMEN_URL_SERVER_DOWNLOAD" => CARMEN_URL_SERVER_DOWNLOAD,
                "m_Idts_majic" => implode($m_Idts_majic),
                "bIsAllowed" => $bIsAllowed,
                "strSelectPerim" => $strSelectPerim,
                "service_idx" => $service_idx,
                "email" => ClassRessourcesDownload::encodeValue($userEmail),
                "format" => ClassRessourcesDownload::encodeValue("magic"),
                "metadata_id" => ClassRessourcesDownload::encodeValue($strMetadataId_majic),
                "infoLayers" => $str1,
                "action_panierDownloadFrontal" => $request->getRequestUri(),
                "strSelectZonage" => $str2,
                "strSelectTerritoire" => $str3,
                "btnExécutionDifférée" => $str4,
                "str5" => $str5,
            );
        } else {
            $ar_parameters = array(
                "CARMEN_URL_SERVER_DOWNLOAD" => CARMEN_URL_SERVER_DOWNLOAD,
                "bIsAllowed" => $bIsAllowed,
                "str6" => $str6,
            );
        }

        return $this->render(
            'GeosourceBundle\Default\panierDownloadFrontal_parametrage_magic_template.html.twig',
            $ar_parameters
        );

    }

}

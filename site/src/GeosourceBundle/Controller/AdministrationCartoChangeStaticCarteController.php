<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route; 
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\DAOProxy\DAO;

use Prodige\ProdigeBundle\Common\Util;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Utile pour l'importation de données à partir de l'administration des droits
 * @author Alkante
 */

/**
 * @Route("/geosource")
 */
class AdministrationCartoChangeStaticCarteController extends BaseController {
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/administrationCartoChangeStaticCarte", name="catalogue_geosource_administrationCartoChangeStaticCarte", options={"expose"=true})
     */
    public function administrationCartoChangeStaticCarteAction(Request $request) {

        unset($errormsg);
        unset($user);
        //require_once('include/ClassUser.php');
        //require_once('include/alkrequest.class.php');
        $user = User::GetUser();
        $userId = User::GetUserId();
        /*  @deprecated prodige4 *
        if(!is_null($userId))
            User::SetUserCookie($userId);

        if(is_null($user)) {
            //header('location:connexion.php');
            exit(0);
        }
        if(!$user->PasswordIsValid()) {
            //header('location:administration_utilisateur.php');
            exit(0);
        }
        */

        //$AdminPath = "Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");
        //$dao = new DAO();
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        $strJs = "";
        //TODO à refaire
        if($dao) {
            $query = 'SELECT PK_ACCES_SERVEUR, ACCS_ID, ACCS_ADRESSE_ADMIN FROM ACCES_SERVEUR ORDER BY ACCS_ID';
            $rs = $dao->BuildResultSet($query);
            $countAdresses = 0;
            $strJs .= "var serveurs = new Array();\n";
            $strAdresses = "";
            for($rs->First(); !$rs->EOF(); $rs->Next()) {
                $pk_acc_server = $rs->Read(0);
                $access_admincarto= $rs->Read(2);
                $selected = "";
                if($request->request->get('SERVEUR', '') == $rs->Read(0))
                    $selected = "selected";
                $strAdresses .= "<option value=\"". $pk_acc_server. "\" ".$selected.">".$rs->Read(1)."</option>";
                $countAdresses ++;
                $strJs .= "serveurs[". $pk_acc_server. "] = '". $rs->Read(2)."';\n";
            }
        }

        $id = $request->get("Id", null);
        $old_fichier = $request->get("file_name", "");
        $new_fichier = $request->get("PATH", null);
        $fmeta_id = $request->get("fmeta_id", null);
        $errormsg = "Le changement de carte supprimera automatiquement l'ancien fichier (".$old_fichier.").<br/> Assurez-vous que ce fichier n'est pas utilisé par d'autres cartes.";
        //$dao = new DAO();
        if($dao && $new_fichier && $id) {
          $dao->setSearchPath("catalogue");
          /*$sqlQuery = "UPDATE STOCKAGE_CARTE SET STKCARD_PATH='".$new_fichier."',
           stkcard_id=SUBSTRING('".$_POST['SERVEUR']."_' || pk_stockage_carte || '_".$_POST['PATH']."' FROM 1 FOR 30) WHERE PK_STOCKAGE_CARTE = ".$id.";".
           "UPDATE CARTE_PROJET SET cartp_id=SUBSTRING('".$_POST['SERVEUR']."_' || pk_carte_projet || '_".$_POST['PATH']."' FROM 1 FOR 30) WHERE cartp_fk_stockage_carte = ".$id.";";*/
          $ar_sqlQuery = array();
          $ar_sqlQuery[] = "UPDATE STOCKAGE_CARTE SET STKCARD_PATH=:STKCARD_PATH, stkcard_id=:stkcard_id WHERE PK_STOCKAGE_CARTE = :PK_STOCKAGE_CARTE";
          $ar_sqlQuery[] = "UPDATE CARTE_PROJET SET cartp_id=:cartp_id WHERE cartp_fk_stockage_carte=:cartp_fk_stockage_carte";
          foreach($ar_sqlQuery as $sqlQuery) {
              if(pg_result_status($dao->Execute($sqlQuery, array("STKCARD_PATH"=>$new_fichier,"stkcard_id"=>"SUBSTRING('".$request->request->get('SERVEUR')."_' || pk_stockage_carte || '_".$request->request->get('PATH')."' FROM 1 FOR 30)","PK_STOCKAGE_CARTE"=>$id,"cartp_id"=>"SUBSTRING('".$request->request->get('SERVEUR')."_' || pk_carte_projet || '_".$request->request->get('PATH')."' FROM 1 FOR 30)","cartp_fk_stockage_carte"=>$id))) == PGSQL_COMMAND_OK) {
                  //TODO hismail
                  //$res = json_decode(file_get_contents(PRO_CATALOGUE_URLBASE."Services/setMetadataDomSdom.php?mode=carte&id=".$fmeta_id));
                  $res = "";
                  if($res && $res->success == true) {

                      $param = Util::getEncodeParam("fileName=".$old_fichier);
                      //$res = json_decode(file_get_contents("http://".$access_admincarto."/PRRA/Services/deleteCarteStatic.php?token=".$param));
                      $context = $this->createUnsecureSslContext();
                      $res = json_decode(file_get_contents($request->getSchemeAndHttpHost().$this->generateUrl('catalogue_geosource_deleteCarteStatic')."?token=".$param, false, $context));
                      //header('location:administration_carto.php');
                  } else {
                      $errormsg = "Erreur lors de la mise à jour de la fiche de métadonnées";
                  }
              } else {
                  $errormsg = "Erreur lors de la mise à jour de la carte";
              }
          }

        }

        $strHtml = "
                    <!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">
                    <html>
                        <head>
                            <title> Administration Cartographique </title>
                            <META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">
                            <!-- IE specific : forcing IE9 Document model because ext 3.0 incompatibility with IE10 -->
                            <META HTTP-EQUIV='X-UA-Compatible' CONTENT='IE=9'/>
                            <link rel='stylesheet' type='text/css' href=\"".$this->generateUrl('catalogue_geosource_get_css_file', array('css_file'=>'administration_carto_ajout'))."\">
                            <link rel=\"stylesheet\" type=\"text/css\" href=\"/Scripts/ext3.0/resources/css/ext-all.css\">
                            <script  src=\"/Scripts/administration_carto.js\"> </script>
                            <script  src=\"/Scripts/administration_carto_ajout.js\"> </script>
                            <script  src=\"/Scripts/ext3.0/adapter/ext/ext-base.js\"> </script>
                            <script  src=\"/Scripts/ext3.0/ext-all.js\"> </script>
                            <script  src=\"/Scripts/ext3.0/ext-lang-fr.js\"> </script>
                            <script  src=\"/Scripts/ext3.0/miframe.js\"> </script>
                            <script language=\"javascript\">".$strJs."</script>
                        </head>
                        <body onload=\"administration_carto_ajout_init();\">
                            <div class=\"titre\"><h2>Changement du fichier de la carte statique</h2></div>
                            <div class=\"errormsg\" id=\"errormsg\">". (isset($errormsg) ? $errormsg : "")."</div>
                            <div class=\"formulaire\">
                                <form action=\"". $request->getRequestUri()."\" method=\"POST\" enctype=\"multipart/form-data\">
                                    <table>
                                        <tr ".($countAdresses == 1 ? "style=\"display:none\"" : "")." >
                                            <th>Serveur</th>
                                            <td>".($countAdresses == 1
                                                  ? "<input type=\"hidden\" name=\"SERVEUR\" ID=\"SERVEUR\" value=\"".$pk_acc_server."\"/>"
                                                  : "<select name=\"SERVEUR\" ID=\"SERVEUR\">".$strAdresses."</select>")."
                                            </td>
                                        </tr>
                                        <tr id=\"file_item\">
                                            <th> Fichier </th>
                                            <td>
                                                <input type='text' name=\"PATH\" id=\"PATH\" readonly value=\"".$old_fichier."\" size=\"50\" maxlength=\"1024\" onchange=\"administration_carto_ajout_changepath(this)\">&nbsp;
                                                <input id=\"parcourir\" type=\"button\" value=\"Parcourir...\" onclick=\"administration_carto_ajout_ChooseFile()\">&nbsp;
                                                <input id=\"telecharger\" type=\"button\" value=\"Télécharger...\" onclick=\"administration_carto_ajout_UploadFile()\">
                                                <input id=\"FORMAT\" type=\"hidden\" value=1 >
                                                <input id=\"file_name\" name=\"file_name\" type=\"hidden\" value=\"".$old_fichier."\" >
                                                <input id=\"fmeta_id\" name=\"fmeta_id\" type=\"hidden\" value=\"".$fmeta_id."\" >
                                                <input id=\"Id\" type=\"hidden\" name=\"Id\" value=\"".$id."\" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan=\"2\" class=\"validation\">
                                                <input type=\"submit\" name=\"VALIDATION\" value=\"Valider\" \">
                                                <input type=\"button\" name=\"RETOUR\" value=\"Retour\" onclick=\"window.location='administration_carto.php';\">
                                            </td>
                                        </tr>
                                    </table>
                                </form>
                            </div>
                        </body>
                    </html>";
        echo $strHtml;
        exit;
    }
}

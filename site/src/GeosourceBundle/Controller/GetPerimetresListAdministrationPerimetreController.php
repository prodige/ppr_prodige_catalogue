<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use JMS\SecurityExtraBundle\Annotation\Secure;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
//use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;

//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * #Route("/geosource")
 */

class GetPerimetresListAdministrationPerimetreController extends BaseController {
    /**
     * #Secure(roles="ROLE_USER")
     * #Route("/getPerimetresListAdministrationPerimetre", name="catalogue_geosource_getPerimetresListAdministrationPerimetre", options={"expose"=true})
     */
    public function getPerimetresListAdministrationPerimetre($tab_INSEE, $init_zonage_field_id, $init_zonage_field_name, $tabPerimetreAllowed=array()) {

        //$AdminPath = "../../";
        //require_once($AdminPath."DAO/DAO/DAO.php");
        //require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php");
        //ConnectionFactory::BeginTransaction();
        //$dao = new DAO();
        $conn = $this->getProdigeConnection('public');
        $dao = new DAO($conn, 'public');

        //$tabINSEE = explode("|", $request->request->get("tabINSEE"));
        $tabINSEE = explode("|", $tab_INSEE);
        $tabInseeAllowed = array();
        $tabPerimAllowed = array();
        //$init_zonage_field_id = $request->request->get("init_zonage_field_id");
        //$init_zonage_field_name = $request->request->get("init_zonage_field_name");
        //$tabPerimetreAllowed = $request->request->get("perimetreAllowed", array());
        //hismail - Prodige 4
        //$strArray = "\$tabPerimAllowed = array();";
        $tabPerimAllowed = array();
        foreach($tabPerimetreAllowed as $key => $value){
          $tabParam = explode("|", $value);
          $query = "select ".$init_zonage_field_id.", ".$init_zonage_field_name." from prodige_perimetre where ".$tabParam[1]."='".$tabParam[0]."' order by ".$init_zonage_field_name;
          $rs = $dao->BuildResultSet($query);
          $bPerimAllowed = true;
          $tabPerimInsee = array();
          for($rs->First(); !$rs->EOF(); $rs->Next()) {
            if(in_array($rs->Read(0), $tabINSEE)) {
              $bPerimAllowed = $bPerimAllowed && true;
              $tabPerimInsee[] = $rs->Read(0); //tableau des codes INSEE du perimetres
              $tabInseeAllowed[$rs->Read(0)] =  $rs->Read(1); //tableau total des codes INSEE
            } else {
              $bPerimAllowed = false;
            }
          }
          if($bPerimAllowed){
            foreach($tabPerimInsee as $key2 => $value2){
                //hismail - Prodige 4
                //$strArray.= "\$tabPerimAllowed['".$tabParam[0]."'][]= '".$value2."';";
                $tabPerimAllowed[$tabParam[0]][] = $value2;
            }
          }
        }

        //$strArray .= "\$tabINSEEAllowed = array();";
        foreach($tabInseeAllowed as $key => $value) {
            //hismail - Prodige 4
            //$strArray.= "\$tabINSEEAllowed['".$key."']= '".addslashes($value)."';";
            $tabINSEEAllowed[$key] = $value;
        }
        //echo $strArray;
        return $tabINSEEAllowed;
    }
}

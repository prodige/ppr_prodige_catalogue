<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Controller\User;

use ProdigeCatalogue\GeosourceBundle\Common\MetadataDesc;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Get layer list from postgresql catalogue database
 * @author Alkante
 * Prodige V3 ---> V4
 */

/**
 * @Route("/geosource")
 */
class AdministrationCartoGetCarteController extends BaseController {
    
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/administrationCartoGetCarte", name="catalogue_geosource_administrationCartoGetCarte", options={"expose"=true})
     */
    public function administrationCartoGetCarteAction(Request $request) {

        //require_once("../../parametrage.php");

        $callback = $request->get("callback", "");

        /*JSON object to return */
        $returnJSON = $this->getMapList($request);

        echo $callback."(".$returnJSON.")";
        exit;
    }


    /**
     * brief Returns a json object containing the list of the layers availaible in the catalog
     * The pattern is :
     * {"name" : "name", "type" : "raster or vector (O or 1)",
     *  "table_name" : "postgis_table or raster file", "id" : "metadata id", "text" : "metadata title and text for extjs tree",
     *  "leaf" : true or false, "cls" : style, "children" : null}
     *
     * @return json text
     */
    protected function getMapList(Request $request) {

        $user = User::GetUser();
        
        //$m_srv = null;
        $m_frame = null;
        $m_responsePhp = null;
        //$coucheType = 1;

        $errMessage = null;
        $tabMaps = array();

        $namespaces = "ARRAY[ARRAY['gmd', 'http://www.isotc211.org/2005/gmd'], ARRAY['gco','http://www.isotc211.org/2005/gco'], ARRAY['srv','http://www.isotc211.org/2005/srv']]";
        
        $m_sSql     = "SELECT array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, data::xml,
                       {$namespaces}), ' ') as cartp_nom, stkcard_path,  PK_CARTE_PROJET from carte_projet inner join stockage_carte on
                       carte_projet.cartp_fk_stockage_carte  = stockage_carte.pk_stockage_carte
                       inner join fiche_metadonnees on carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees
                       inner join metadata on fiche_metadonnees.fmeta_id::bigint = metadata.id".
                       // métadonnées publiées = operationallowed 0 (view) pour groups 1 (All Internet)
                       //" inner join operationallowed on ( metadata.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)
                       " where  carte_projet.cartp_format=0 ".//and  fiche_metadonnees.statut =4
                       "order by cartp_nom";
        
        $m_sSqlMaps    = "SELECT map_id, map_file from map where published_id is null";
        $conn_prodige = $this->getProdigeConnection('carmen');
        $tabInfoMaps = $conn_prodige->fetchAllAssociative($m_sSqlMaps);
        $tabCarmenMaps = array();
        foreach($tabInfoMaps as $modele) {
            $tabCarmenMaps[$modele["map_file"]] = $modele["map_id"];
        }
                
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue,public');
        if($dao) {
            $rs = $dao->buildResultSet($m_sSql);
            if($rs->GetNbRows() > 0) {
                $i=0;
                for($rs->First();!$rs->EOF();$rs->Next()) {
                
                      $query = "SELECT DOM_NOM, SSDOM_NOM, PK_CARTE_PROJET FROM CARTES_SDOM WHERE PK_CARTE_PROJET =?";
                      $rsCarte = $dao->BuildResultSet($query, array($rs->Read(2)));
                        
                      for ($rsCarte->First(); !$rsCarte->EOF(); $rsCarte->Next()) {
                          $domaine = $rsCarte->read(0);
                          $sous_domaine = $rsCarte->read(1);
                          $objetCarte = $rsCarte->read(2);
                            
                          //vérification de l'autorisation sur le domaine et sur l'objet
                          $bAllow = $user->HasTraitement('NAVIGATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_CONSULTATION, $objetCarte, PRO_OBJET_TYPE_CARTE);
                          if ($bAllow)
                              break;
                      }
                      $bAllow = $bAllow && $user->HasTraitementCompetence('NAVIGATION', $objetCarte);
                      
                      if($bAllow && array_key_exists(substr($rs->Read(1), 0, -4),$tabCarmenMaps )){
                          $tabMaps[$i]["text"] = $rs->Read(0);//text for extjs tree
                          //replace ; by nothing (because used to build the param tab when adding the layer)
                          $tabMaps[$i]["text"] = str_replace(";", "",$tabMaps[$i]["text"]);
                          $tabMaps[$i]["map_name"] = $tabCarmenMaps[substr($rs->Read(1), 0, -4)];
                          $tabMaps[$i]["map_id"] = $tabCarmenMaps[substr($rs->Read(1), 0, -4)];
                          $tabMaps[$i]["id"] = $rs->Read(1);//id for postprocessing and for extjs tree
                          $tabMaps[$i]["leaf"] = true;//leaf for extjs tree
                          $tabMaps[$i]["cls"] = "folder";//cls for extjs tree
                          $tabMaps[$i]["children"] = "";//children for extjs tree
                          $i++;
                      }
                }
            }
        }

        if(empty($tabMaps)) 
            $errMessage = "Le serveur cartographique ne dispose d\'aucune couche de données.";
       

        if(!is_null($errMessage)) {
            return "'".($errMessage)."'";
        }
        else {
            return (json_encode($tabMaps));
        }
    }
}


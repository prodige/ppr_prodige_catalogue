<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

//use Symfony\Component\HttpFoundation\Session\Session;
//use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

//use Prodige\ProdigeBundle\Controller\GetProjectionsController;
use Prodige\ProdigeBundle\DAOProxy\DAO;

/**
 * Get map list from server files
 * @author Alkante
 * Prodige V3 ---> V4
 */

/**
 * @Route("/geosource")
 */
class TSAjoutDeCarteLayerListController extends BaseController
{
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/tSAjoutDeCarte_LayerListController", name="catalogue_geosource_tSAjoutDeCarte_LayerListController", options={"expose"=true})
     */
    public function tSAjoutDeCarte_LayerListControllerAction(Request $request)
    {

        //require_once("path.php");

        $callback = $request->get("callback", "");
        $map_id = -1;
        $map_file = "";
        $response = array();

        //$PN_MAP_ADD = $request->get("PN_MAP_ADD", "");
        //$PN_CONTEXT_ADD = $request->get("PN_CONTEXT_ADD", "");
        if (isset($_FILES['UploadContextField']) && substr(
                strtolower(pathinfo($_FILES['UploadContextField']["name"], PATHINFO_EXTENSION)),
                0,
                3
            ) === "ows") {
            $PN_CONTEXT_ADD = $_FILES['UploadContextField']['tmp_name'];
        } else {
            throw new \Exception("Aucun fichier uploadé");
        }

        if ($PN_CONTEXT_ADD != "") {
            $map_file = $this->loadContext($PN_CONTEXT_ADD);
            if ($map_file != "") {
                $map_file = substr_replace($map_file, '', 0, 0);
                $response["mapfile_name"] = $map_file;
                $m_sSqlMaps = "SELECT map_id FROM map WHERE map_file=?";
                $conn_prodige = $this->getProdigeConnection('carmen');
                $dao = new DAO($conn_prodige, 'carmen');
                if ($dao) {
                    $rs = $dao->buildResultSet($m_sSqlMaps, array($map_file));
                    if ($rs->GetNbRows() > 0) {
                        $response["mapfile_id"] = $map_id;
                        for ($rs->First(); !$rs->EOF(); $rs->Next()) {
                            $response["mapfile_id"] = $rs->Read(0);
                        }
                    }
                }
            }
        }

        return new Response(json_encode($response));
    }

    /**
     *
     * @param string $contextFile
     * @return mapfile name
     */
    protected function loadContext($contextFile)
    {
        //global $CheminPublication;
        //global $PRO_MAPFILE_PATH;

        $ows_content = file_get_contents(/*$CheminPublication.*/ $contextFile);
        if ($ows_content == false) {
            error_log("Problème dans l'accès au fichier contexte.");
        }
        $ows_content = stripslashes($ows_content);

        $ows_xml = @\DOMDocument::loadXML($ows_content);
        //var_dump($ows_xml); die();

        if ($ows_xml == false) {
            error_log("Problème dans l'accès au fichier contexte : ".$contextName);
        }
        $n_mapfiles = $ows_xml->getElementsBytagName("ViewContext"/*mapSettings_mapfile*/);
        //var_dump($n_mapfiles); die();
        if (count($n_mapfiles) == 0) {
            trigger_error("Fichier contexte mal formé");
        } else {
            $mapfile = $n_mapfiles->item(0)->getAttribute('id')/*textContent*/
            ;
        }

        // supprimer le fichier temporaire
        @unlink($contextFile);

        //$mapfile = str_replace(PRO_MAPFILE_PATH, "", $mapfile);
        return $mapfile;
    }

}

<?php

namespace ProdigeCatalogue\GeosourceBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;

//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
* Page de consultation des couches présentes sur le serveur wms
* @author Alkante
*/

/**
 * @Route("/geosource")
 */
class PerimetresGetExtentController extends BaseController {
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/perimetresGetExtent", name="catalogue_geosource_perimetresGetExtent", options={"expose"=true})
     */
    public function perimetresGetExtentAction(Request $request) {

        //$AdminPath = "../../";
        //require_once($AdminPath."DAO/DAO/DAO.php");
        //require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php");
        //ConnectionFactory::BeginTransaction();
        //$dao = new DAO();

        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');

        $zonage_field_id = $request->query->get("zonage_field_id", "");
        $zonage_field_name = $request->query->get("zonage_field_name", "");
        $tabCouple = json_decode($request->query->get("tabCouple", ""));

        if(count($tabCouple) > 0) {
            $query = "select st_xmin(extent(the_geom)), st_ymin(extent(the_geom)),st_xmax(extent(the_geom)), st_ymax(extent(the_geom)) from public.prodige_perimetre where (";
            foreach($tabCouple as $key => $array) {
                $query .= $array[1]."='".$array[0]."'"." or ";
            }
            $query = substr($query, 0, -4).") ";
            $rs = $dao->BuildResultSet($query);
            $strExtent = "";
            for($rs->First(); !$rs->EOF(); $rs->Next()) {
                $strExtent=  "\$strExtent = '".$rs->Read(0).",".$rs->Read(1).",".$rs->Read(2).",".$rs->Read(3)."';";
            }
            echo $strExtent;
        }
        exit();
    }
}

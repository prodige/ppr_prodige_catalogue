<?php
namespace ProdigeCatalogue\GeosourceBundle\Common;

use Prodige\ProdigeBundle\DAOProxy\DAO;

// pixel weight in octets (bytes, * 8 for value in bits)
if (! defined('RASTER_INFO_PIXEL_WEIGHT_PER_BAND')) define("RASTER_INFO_PIXEL_WEIGHT_PER_BAND", 1);

// hd resolution pixel en metre
if (! defined('RASTER_HR_RESOLUTION')) define("RASTER_HR_RESOLUTION", 1);

// max raster size that can be handled in memory with free licensed ecw compressor 500M. Bytes
//define("RASTER_INFO_ECW_SIZE_LIMIT", 500000000);

//if(!isset($AdminPath))
    //$AdminPath = "Administration/";

//require_once($AdminPath."../parametrage.php");
//require_once($AdminPath."DAO/DAO/DAO.php");


//if(!class_exists('RasterInfo')) {
    /**
     * brief classe permettant l'acces a la vue v_desc_raster
         SELECT
         m.id AS metadataid,
         r.id AS layerid,
         r.name,
         r.srid,
         r.ulx,
         r.uly,
         r.lrx,
         r.lry,
         r.bands,
         r.cols,
         r.rows,
         r.resx,
         r.resy,
         r.format,
         r.vrt_path,
         r.compression,
         r.color_interp
         FROM
         ((public.metadata m
         JOIN
         fiche_metadonnees fm ON (((m.id)::text = (fm.fmeta_id)::text)))
         JOIN
         raster_info r ON ((fm.fmeta_fk_couche_donnees = r.id)));
     *
     *
     * @author Alkante
     */
//}

class ClassRasterInfo {

    static private    $SQL_SELECT_FROM_PK       = "SELECT metadataid, layerid, name, srid, ulx, uly, lrx, lry, bands, cols, rows, resx, resy, format, vrt_path, compression, color_interp from {SCHEMA}.v_desc_raster where metadataid = '{1}'";

    static private    $SQL_CONDITION_PATTERN    = "{1}";
    static private    $SQL_PATTERN_SRV      = "{SRV}";
    static private    $SQL_CATALOGUE_SCHEMA_NAME      = "{SCHEMA}";


    protected     $m_id;

    public 	$r_info = array();
    
    /**
     * @var \Doctrine\DBAL\Connection
     */
    private static $conn;
    
    /**
     * @param \Doctrine\DBAL\Connection $conn
     */
    public static function setConnection(\Doctrine\DBAL\Connection $conn) {
        self::$conn = $conn;
    }

    public function __construct($pk) {
        $this->m_id = $pk;
        $this->r_info = array();
        if (!is_null($pk)) {

            //$dao = new DAO();
            $dao = new DAO(self::$conn, 'catalogue');
            
            if($dao) {
                $m_sSql = ClassRasterInfo::$SQL_SELECT_FROM_PK;
                $m_sSql = str_replace(ClassRasterInfo::$SQL_CONDITION_PATTERN, $pk, $m_sSql);
                $m_sSql = str_replace(ClassRasterInfo::$SQL_CATALOGUE_SCHEMA_NAME, PRO_SCHEMA_NAME, $m_sSql);
                $rs = $dao->BuildResultSet($m_sSql);
                if($rs->GetNbRows() > 0) {
                    $rs->First();
                    if(!$rs->EOF()) {
                        $this->m_id       = $rs->Read(0);
                        $this->r_info["metadata_id"] = $this->m_id;
                        $this->r_info["layer_id"] = $rs->Read(1);
                        $this->r_info["name"] = $rs->Read(2);
                        $this->r_info["srid"] = $rs->Read(3);
                        $this->r_info["ulx"] = $rs->Read(4);
                        $this->r_info["uly"] = $rs->Read(5);
                        $this->r_info["lrx"] = $rs->Read(6);
                        $this->r_info["lry"] = $rs->Read(7);
                        $this->r_info["bands"] = $rs->Read(8);
                        $this->r_info["cols"] = $rs->Read(9);
                        $this->r_info["rows"] = $rs->Read(10);
                        $this->r_info["resx"] = $rs->Read(11);
                        $this->r_info["resy"] = $rs->Read(12);
                        $this->r_info["format"] = $rs->Read(13);
                        $this->r_info["vrt_path"] = $rs->Read(14);
                        $this->r_info["compression"] = $rs->Read(15);
                        $this->r_info["color_interp"] = $rs->Read(16);
                    }
                }
            }
        }
    }

    /**
     * brief Retourne l'identifiant
     * @return m_id
     */
    public function GetMetatadaId() {
        return $this->GetValue($this->m_Id);
    }

    /**
     * brief Retourne le srid natif de la couche raster
     * @return string
     */
    public function GetSrid() {
        return $this->GetValue($this->r_info["srid"]);
    }

    /**
     * brief Retourne le nom de la couche raster
     * @return m_Type
     */
    public function GetName() {
        return $this->GetValue($this->r_info["name"]);
    }

    /**
     * brief Retourne le chemin du fichier vrt associée à la couche raster
     * @return string
     */
    public function GetVrtPath() {
        return $this->GetValue($this->r_info["vrt_path"]);
    }

    /**
     * brief Retourne la valeur du champ $infoDesc
     * @return string
     */
    public function GetInfo($infoDesc) {
        return $this->GetValue($this->r_info[$infoDesc]);
    }

    /**
     * brief Retourne la valeur
     * @param $value
     * @return $l_sResult
     */
    private function GetValue($value) {
        $l_sResult = "";
        if(!is_null($value)) $l_sResult = $value;
        return $l_sResult;
    }

    public function getMaxAreaFromSize($size) {
        $area = abs($size*($this->r_info["resx"]*$this->r_info["resy"])/($this->r_info["bands"] * RASTER_INFO_PIXEL_WEIGHT_PER_BAND));
        return $area;
    }

    public function getMaxAreaForECW() {
        global $PRO_RASTER_INFO_ECW_SIZE_LIMIT;
        $PRO_RASTER_INFO_ECW_SIZE_LIMIT = isset($PRO_RASTER_INFO_ECW_SIZE_LIMIT) ?
        $PRO_RASTER_INFO_ECW_SIZE_LIMIT : 500000000; // 500M
        return $this->getMaxAreaFromSize($PRO_RASTER_INFO_ECW_SIZE_LIMIT);
    }

    public function getMaxAreaForGTIFF() {
        global $PRO_RASTER_INFO_GTIFF_SIZE_LIMIT;
        $PRO_RASTER_INFO_GTIFF_SIZE_LIMIT = isset($PRO_RASTER_INFO_GTIFF_SIZE_LIMIT) ?
        $PRO_RASTER_INFO_GTIFF_SIZE_LIMIT : 4000000000; // 4GB
        return $this->getMaxAreaFromSize($PRO_RASTER_INFO_GTIFF_SIZE_LIMIT);
    }
}
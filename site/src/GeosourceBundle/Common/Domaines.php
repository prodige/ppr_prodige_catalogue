<?php
namespace ProdigeCatalogue\GeosourceBundle\Common;

use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Controller\User;

/**
 * brief page de génération de l'arbre json des domaines/sous-domaines
 * @author Alkante
 */

class Domaines {

    /**
     * @var \Doctrine\DBAL\Connection
     */
    private static $conn;

    /**
     * @param \Doctrine\DBAL\Connection $conn
     */
    public static function setConnection(\Doctrine\DBAL\Connection $conn) {
        self::$conn = $conn;
    }

    /**
     * retourne la liste des domaines.sous-domaines
     * @param $bAjax      true : appel AJAX
     *                    false : autre
     * @param $fmeta_id   identifiant d'une métadonnée, permet de récupérer l'information "checked" associé au sous-domaine d'une métadonnée (optionnel)
     */
    public static function getDomaines($bAjax=false, $fmeta_id=-1, $bAucunFiltre = false) {
        if (!$bAjax) {
            return "<script language='javascript'>var domaineList = [];</script>";
        }

        $strJson = '[';
        if ($bAucunFiltre) {
            $strJson .= '{"text":"Aucun filtre (toutes les donn&eacute;es)","isDomain":true,"id":"-1","leaf":true,"expanded":false,"cls":"folder","children":[]},';
        }
        
        $dao = new DAO(self::$conn, 'catalogue, public');
        if($dao) {
            $bFirstRubrique = true;
            $glueRub = "\n";
            //loop on rubrics
            $rs_rubric = $dao->BuildResultSet("SELECT RUBRIC_ID, RUBRIC_NAME FROM RUBRIC_PARAM ORDER BY RUBRIC_ID");
            for($rs_rubric->First(); !$rs_rubric->EOF();  $rs_rubric->Next()) {
                $rubric_id = $rs_rubric->Read(0);
                $rubric_name = html_entity_decode($rs_rubric->Read(1), ENT_QUOTES, 'UTF-8');
                $strJson.= $glueRub.'{"text" : "'.$rubric_name.'", "id":"rubric_'.$rubric_id.'", "leaf":false, "expanded":'.($bFirstRubrique || $fmeta_id != -1 ? "true" : "false").', "cls":"folder", "children":[';
                $bFirstRubrique = false;

                $rs_dom = null;

            
                $rs_dom = $dao->BuildResultSet("SELECT PK_DOMAINE, DOM_NOM".
                                            " FROM DOMAINE".
                                            " WHERE DOM_RUBRIC=?".
                                            " ORDER BY DOM_NOM", array($rubric_id));
                

                $glueDom = "";
                for($rs_dom->First(); !$rs_dom->EOF() ; $rs_dom->Next()) {
                    $pk_domaine = $rs_dom->Read(0);
                    $nom_domaine = html_entity_decode($rs_dom->Read(1), ENT_QUOTES, 'UTF-8');                 
                    $strJson.= $glueDom.'{ "id": "dom_'.$pk_domaine.'", "isDomain" : true, "icon":"/images/domaines.gif", "text": "'.$nom_domaine.'", "leaf": false, "expanded":'.( $fmeta_id != -1 ? 'true' : 'false' ).', "cls": "file", "children":[';
                    $rs_sdom = $dao->BuildResultSet("SELECT PK_SOUS_DOMAINE, SSDOM_NOM" .
                                                    ( $fmeta_id != -1
                                                      ? ", (SELECT COUNT(PK_SOUS_DOMAINE) AS NB from ".
                                                            "(SELECT sdom.PK_SOUS_DOMAINE, sdom.SSDOM_NOM ".
                                                                "FROM SOUS_DOMAINE sdom, SSDOM_DISPOSE_METADATA dispose, METADATA meta ".
                                                                "where ".
                                                                    "meta.uuid = dispose.uuid and ".
                                                                    "sdom.PK_SOUS_DOMAINE = dispose.ssdcouch_fk_sous_domaine and ".
                                                                    "meta.id = :FMETA_ID and SOUS_DOMAINE.PK_SOUS_DOMAINE = sdom.PK_SOUS_DOMAINE)".
                                                           " COUCHE_CARTE_SDOM )"
                                                      : ""
                                                    ) .
                                                    " FROM SOUS_DOMAINE" .
                                                    " WHERE ssdom_fk_domaine=:SSDOM_FK_DOMAINE" .
                                                    " ORDER BY SSDOM_NOM", array("FMETA_ID"=>$fmeta_id, "SSDOM_FK_DOMAINE"=>$pk_domaine));
                                                    $glueSDom = "";
                            
                            for($rs_sdom->First(); !$rs_sdom->EOF() ; $rs_sdom->Next()) {
                                $pk_sous_domaine = $rs_sdom->Read(0);
                                $nom_sous_domaine = html_entity_decode($rs_sdom->Read(1), ENT_QUOTES, 'UTF-8');
                                $strJson.= $glueSDom.'{ "id": "sdom_'.$pk_sous_domaine.'", "isSubDomain" : true, "rubric" :"'.$rubric_name.'", "domain" :"'.$nom_domaine.'", "text": "'.$nom_sous_domaine.'", "leaf": true,'.( $fmeta_id != -1 ? ' "checked": '.( $rs_sdom->Read(2) > 0 ? 'true' : 'false' ).',' : '' ).'"cls": "file" }';
                                $glueSDom = ",\n";
                            }
                            $strJson .= "]}";//suppression de la virgule

                            $glueDom = ",\n";
                }
                $strJson .= "]}";

                $glueRub = ",\n";
            }
            $strJson .= "]";
        }

        if($bAjax)
            return $strJson;

        return "<script language='javascript'>var domaineList = ".$strJson.";</script>";
    }

    /**
     * retourne la liste des domaines/sous-domaines pour un store ExtJs
     */
    public static function getDomainesForStore($q="") {

        $tabDom = array("success" => false, "message" => "", "q" => $q, "rows" => array(), "total" => 0);

        //$dao = new DAO();
        $dao = new DAO(self::$conn, 'catalogue');
        if($dao) {
            $rs_dom = $dao->BuildResultSet("SELECT PK_DOMAINE, DOM_NOM, PK_SOUS_DOMAINE, SSDOM_NOM" .
                                           " FROM DOMAINE DOM LEFT JOIN SOUS_DOMAINE SDOM ON SDOM.SSDOM_FK_DOMAINE=DOM.PK_DOMAINE" .
                                           " ORDER BY DOM_NOM, SSDOM_NOM");

            $pk_domaine_previous = -1;
            for($rs_dom->First(); !$rs_dom->EOF() ; $rs_dom->Next()) {
                $pk_domaine = $rs_dom->Read(0);
                $dom_nom = html_entity_decode($rs_dom->Read(1), ENT_QUOTES, 'UTF-8');
                $pk_sous_domaine = $rs_dom->Read(2);
                $ssdom_nom = html_entity_decode($rs_dom->Read(3), ENT_QUOTES, 'UTF-8');
                if($pk_domaine != $pk_domaine_previous && ( $q == "" || strstr($dom_nom, $q))) {
                    $tabDom["rows"][] = array("id" => $dom_nom, "value" => $dom_nom);
                    $pk_domaine_previous = $pk_domaine;
                }
                if($q == "" || strstr($ssdom_nom, $q)) {
                    $tabDom["rows"][] = array("id" => $ssdom_nom, "value" => "&nbsp;&nbsp;&nbsp;&nbsp;".$ssdom_nom);
                }
            }
        } else {
            $tabDom["message"] = "Impossible de se connecter à la base de données.";
            return json_encode($tabDom);
        }

        $tabDom["success"] = true;

        return json_encode($tabDom);
    }

    /**
     * retourne la liste des sous-domaines administrables par l'utilisateur
     */
    public static function getUserSDomainesAdminList() {

        $tabSDom = array();

        //global $AdminPath;
        //require_once($AdminPath."/../include/ClassUser.php");

        $userId = User::GetUserId();

        //$dao = new DAO();
        $dao = new DAO(self::$conn, 'catalogue');

        if($dao && $userId) {
            $rs_sdom = $dao->BuildResultSet(
                                            "SELECT PK_SOUS_DOMAINE, SSDOM_NOM" .
                                            " FROM ADMINISTRATEURS_SOUS_DOMAINE" .
                                            " WHERE PK_UTILISATEUR = ?" .
                                            " ORDER BY SSDOM_NOM", array($userId));
            for($rs_sdom->First(); !$rs_sdom->EOF() ; $rs_sdom->Next()) {
                $pk_sous_domaine = $rs_sdom->Read(0);
                $nom_sous_domaine = html_entity_decode($rs_sdom->Read(1), ENT_QUOTES, 'UTF-8');
                $tabSDom[] = array("id" => $pk_sous_domaine, "nom" => $nom_sous_domaine);
            }
        }

        return json_encode($tabSDom);
    }

    /**
     * retourne la liste des sous-domaines d'une métadonnée
     */
    public static function getMetadataSousDomainesList($fmeta_id) {

        $tabSDom = array();

        //$dao = new DAO();
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        if($dao) {
            $rs_sdom = $dao->buildResultSet(
                "SELECT sdom.PK_SOUS_DOMAINE, sdom.SSDOM_NOM ".
                "FROM SOUS_DOMAINE sdom, SSDOM_DISPOSE_METADATA dispose, METADATA meta ".
                "WHERE ".
                  "meta.uuid = dispose.uuid and ".
                  "sdom.PK_SOUS_DOMAINE = dispose.ssdcouch_fk_sous_domaine and ".
                  "meta.id = ? ".
                "ORDER BY sdom.SSDOM_NOM", array($fmeta_id));
            for($rs_sdom->First(); !$rs_sdom->EOF() ; $rs_sdom->Next()) {
                $pk_sous_domaine = $rs_sdom->Read(0);
                $nom_sous_domaine = html_entity_decode($rs_sdom->Read(1), ENT_QUOTES, 'UTF-8');
                $tabSDom[] = array("id" => $pk_sous_domaine, "nom" => $nom_sous_domaine);
            }
        }

        return json_encode($tabSDom);
    }

}
<?php

namespace ProdigeCatalogue\GeosourceBundle\Common;

/**
 * @class RessourcesDownload
 * brief classe contenant les constantes utiles au téléchargement de données
 * @author Alkante
 *
*/

class ClassRessourcesDownload {

    static public $LICENCE_FILE = '/home/sites/siteCatalogue/www/PRRA/ressources/licence.pdf';

    public static function getFormats($areVisualisable=1) {
        $idx = 0;
        $format = array();
        $format[$idx++] = "MapInfo tab (fichier *.tab)||tab";
        $format[$idx++] = "ESRI (fichier *.shp)||shp";
        $format[$idx++] = "Atlas BNA (fichier *.bna)||bna";
        $format[$idx++] = "Comma Separated Value (fichier *.csv)||csv";
        $format[$idx++] = "Géoconcept Export (fichier *.gxt)||gxt";
        $format[$idx++] = "MapInfo mif/mid (fichier *.mif,*.mid)||mif";
        $format[$idx++] = "GML (fichier *.gml)||gml";
        $format[$idx++] = "GMT (fichier *.gmt)||gmt";
        $format[$idx++] = "KML (fichier *.kml)||kml";
        $format[$idx++] = "GeoJson (fichier *.json)||json";
        $format[$idx++] = "Geopackage vecteur(fichier *.gpkg)||gpkg";
        $format[$idx++] = "Microstation DGN (fichier *.dgn)||dgn";
        $format[$idx++] = "DXF (fichier *.dxf)||dxf";
        $format[$idx++] = "Dump postgis (fichier *.sql)||sql";
        
        if($areVisualisable == 0) {
            $format[$idx++] = "XYZ (fichier *.xyz)||xyz";
            $format[$idx++] = "ASC (fichier *.asc)||asc";
        }
        return $format;
    }

    public static function getFormats_raster() {
        $idx = 0;
        $format = array();
        $format[$idx++] = "GeoTIFF||GTIFF";
        $format[$idx++] = "ECW||ECW";
        $format[$idx++] = "JPEG||JPEG";
        return $format;
    }
    
    public static function getFormats_raster_hr($str_format)
    {
        $idx = 0;
        $format = array();
        if($str_format=="TILED"){
            $format[$idx++] = "Tuilé||".$str_format;
        }else{
            $format[$idx++] = $str_format."||".$str_format;
        }
    
        return $format;
    }

    public static function getFormats_table() {
        $idx = 0;
        $format = array();
        $format[$idx++] = "CSV||CSV";
        $format[$idx++] = "ODS||ODS";
        $format[$idx++] = "XLSX||XLSX";
        $format[$idx++] = "Dump postgis||sql";
        return $format;
    }

    protected static function readProjections() {
        //global $accs_adresse_admin;
        //$tabEPSGCodes = json_decode(file_get_contents("http://".accs_adresse_admin."/PRRA/Services/getProjections.php"),true);
        //TODO BFE sortir cette lecture de cette fonction
        $file = PRO_MAPFILE_PATH."Projections_Administration.xml";
        $tabEPSGCodes = array();
        if(file_exists($file)) {
            $xml = simplexml_load_file($file);
            for($i=0;$i<count($xml->PROJECTION);$i++){
                $tabEPSGCodes[] = array(
                    "epsg"=>(string)$xml->PROJECTION[$i]['EPSG'],
                    "nom"=>(string)$xml->PROJECTION[$i]['NOM'],
                    "proj4"	=> (string)$xml->PROJECTION[$i]['PROJ4'],
                    "display"=>"EPSG:".(string)$xml->PROJECTION[$i]['EPSG']." - ".(string)$xml->PROJECTION[$i]['NOM']
                );
            }
        }
        return $tabEPSGCodes;
    }

    public static function getProjections() {
        $tabEPSGCodes = self::readProjections();
        
        $projection = array();
        for($i=0;$i<count($tabEPSGCodes);$i++) {
            $projection[$i] = $tabEPSGCodes[$i]["nom"] . " [EPSG:" . $tabEPSGCodes[$i]["epsg"] . "]|" . $tabEPSGCodes[$i]["epsg"];
        }
        return $projection;
    }


    public static function getProjections2($srid)
    {
        $tabEPSGCodes = self::readProjections();
        $projection = array();
        for($i=0;$i<count($tabEPSGCodes);$i++){
            
            if($srid==$tabEPSGCodes[$i]["epsg"]){
                $projection[0] = $tabEPSGCodes[$i]["nom"] . " [EPSG:" . $tabEPSGCodes[$i]["epsg"] . "]|" . $tabEPSGCodes[$i]["epsg"];
                break;
            }
        }
        return $projection;
    }

    public static function getProjectionsDefault() {
        return 'epsg:' . $PRO_IMPORT_EPSG;
    }

    public static function encodeValue($value) {
        return base64_encode(urlencode($value));
    }

    public static function decodeValue($value) {
        return urldecode(base64_decode($value));
    }
}
<?php
namespace ProdigeCatalogue\GeosourceBundle\Common;

/**
 * librairie permettant de récupérer les fonctions définies dans panierDownloadFrontal_parametrage.php après la migration vers Prodige 4.0
 * @author Alkante
 */

class PanierDownloadFrontalParametrageCommonFunctions {

    /**
     * Display the header of the information download form (server, layer, id, status...)
     */
    public static function printFormHeader($withJoin = false) {
        $rowspan = 2;
        $str = '<tr>
                    <th style="text-align:center;" rowspan="'.$rowspan.'">Métadonnée</th>
                    <th style="text-align:center;" rowspan="'.$rowspan.'">Informations</th>';
        $str .= '<th style="text-align:center;" colspan="'. ($withJoin ? "4" : "2").'">Table(s) carto</th>';
        
        $str .= '<th style="text-align:center;" rowspan="'.$rowspan.'">Etat</th>
                </tr>';
        //if ( $withJoin ){
        $str .= '<tr><th style="text-align:center;">Table d\'origine</th>';
        $str .= '<th style="text-align:center;">Droits d\'extraction</th>';
        if ( $withJoin ){
            $str .= '<th style="text-align:center;">Table téléchargée</th>'.
                    '<th style="text-align:center;">Joindre les données</th>';
        }
        $str .= '</tr>';
        //}
        return $str;
    }

    public static function presenterServeur($serveur, $metadataIdts, $idxSrv, $type ="", $withJoin=false, $metadataIdtsJoinTable=array(), $tabRestrictTerr=array()){

        $metadatas = MetadataDesc::GetIdsByServeur($serveur, $metadataIdts);
        $metadatas = array_values(array_unique($metadatas));
        $result = "";
        
        $dataType="";
        switch ($type){
          case "vector":
            $dataType = MetadataDesc::$TYPE_STOCKAGE_POSTGIS;
            break;
          case "table":
            $dataType = MetadataDesc::$TYPE_STOCKAGE_TABLE;
            break;
          case "raster":
            $dataType = MetadataDesc::$TYPE_STOCKAGE_RASTER;
            break;
          case "model":
            $dataType = "";//no filter on couchd_type_stockage
            break;
        }

        // if join is activated, building layername and tablenames datastores...
        $layernamesStore = array();
        $tablenamesStore = array();
        if($withJoin) {
            for($idM=0;$idM<count($metadatas);$idM++) {
                $meta = new MetadataDesc($idM);
                array_push($layernamesStore, array(($meta->GetTable()),($meta->GetTitre())));
            }
            $metadatasTable = MetadataDesc::GetIdsByServeur($serveur, $metadataIdtsJoinTable);
            for($idM=0;$idM<count($metadatasTable);$idM++){
                $meta = new MetadataDesc($metadatasTable[$idM]);
                array_push($tablenamesStore, array(($meta->GetTable()),($meta->GetTitre())));
            }
        }
        
        if(count($metadatas) > 0) {
            $nbRowMax = 0;
            $result .= "\n <tr id=\"" . $type . "_" . $metadatas[0] . "_" . "row" ."\">";
            /*$result .= "\n <td rowspan=\"{nbRowMax}\" valign=\"middle\">";
            $result .= "<strong>".$serveur."</strong>";

            $result .= "\n </td>";*/
            $result .= self::presenterCouche($metadatas[0], $withJoin, $tablenamesStore, $tabRestrictTerr, $nbRowMax, true, $dataType);
            $result .= "\n </tr>";

            for ($idxMetadata=1;$idxMetadata<count($metadatas);$idxMetadata++){
                $result .= "\n<tr  id=\"" . $type . "_" . $metadatas[$idxMetadata] . "_" . "row" ."\">";
                $result .= self::presenterCouche($metadatas[$idxMetadata], $withJoin, $tablenamesStore, $tabRestrictTerr, $nbRowMax, false, $dataType);
                $result .= "\n</tr>";
            }

            $result = str_replace("{nbRowMax}", $nbRowMax, $result);
            $result = str_replace("{idxSrv}", $idxSrv, $result);
            $result = str_replace("{type}", $type, $result);
        }
        
        return $result;
    }

    public static function presenterIs3D($idt){
        $metadataDesc = new MetadataDesc($idt);
        return $metadataDesc->Get3D();
    }

    protected static function presenterCouche($idt, $withJoin=false, $tablenamesStore=array(), $tabTerr=array(), &$nbRowMax=0, $isFirst=false, $dataType){
        $metedataDesc = new MetadataDesc($idt, $dataType);
        
        $metadataTables = $metedataDesc->GetTable();
        if(empty($metadataTables)) {
            $metedataDesc = new MetadataDesc($idt);
            $metadataTables = $metedataDesc->GetTable();
        }
        
        $nbRowMax += count($metadataTables);

        
        $result =  "\n <td valign=\"middle\" rowspan='".count($metadataTables)."'>".$metedataDesc->GetTitre()."</td>";
        $result .= "\n <td align=\"left\" rowspan='".count($metadataTables)."'>";
        $result .= "\n  <strong><u>Identifiant Geonetwork</u></strong> : ".$metedataDesc->GetId();
        $result .= "\n <br>";
        $result .= "\n  <strong><u>Nombre de tables</u></strong> : ".count($metadataTables);
        
        if($metedataDesc->GetExtractionattributaire() != ""){
            $result .= "\n <br>";
            $result .= "\n <strong><u>Extraction attributaire (couche)</u></strong> : <span id='extractionattributaire'>".$metedataDesc->GetExtractionattributaire()."</span>";
        }

        $result .= "\n </td>";
        
        foreach ($metadataTables as $index =>$table){
            if ( $index>0 ) {
                $result .= "</tr><tr>";
            }
            $result .= "\n  <td width='250px'>".$table."</td>";
            $result .= "<td>";
            if(isset($tabTerr[$idt][$table])) {
                $result .= "Extraction automatique sur les territoires :";
                $result .= "<ul>";
                
                foreach($tabTerr[$idt][$table] as $key => $params) {
                    $result .= "<li>".$params[1]."</li>";
                }
                $result .= "</ul>";
            }else{
                $result .= "sans limitation";
            }
            $result .= "</td>";
            if($withJoin){
                if(in_array(MetadataDesc::$TYPE_STOCKAGE_POSTGIS, $metedataDesc->GetType())) {
                    $layernamesStore = array($table, $metedataDesc->GetNom()." [".$table."]");

                    $data = array(
                        "layer_id" => $idt,
                        "layername" => $metedataDesc->GetNom()." [".$table."]",
                        "table" => $table,
                        "theme_id_for_ext" => PRO_COLOR_THEME_IDin_array ,
                        "layers" => $layernamesStore,
                        "jointables" => $tablenamesStore,
                    );
                    $result .=
                    "\n
                    <td style='text-align:center' width='150px' id='td_view_" . $table . "'>
                            <input type='hidden' name='viewname_of_" . $table . "' value='" . $table . "'/>
                            <i>Table d'origine</i>
                            </td>
                    <td style='text-align:center' id='td_actions_" . $table . "'>
                    <input type=\"Button\" value=\"Paramétrer\" onclick=\"launchViewManager('" . $table . "');\"/>
                    <script type='text/javascript'>
                    launchViewProperties = window.launchViewProperties || {};
                    launchViewProperties['" . $table . "'] = ".json_encode($data).";
                    </script>
                    </td>";
                }else{
                    $result .="<td></td><td></td>";
                }
            }
            if ( $isFirst && $index==0 ){
                $result .= "\n <td rowspan=\"{nbRowMax}\" valign=\"middle\" align=\"left\">
                        <div id=\"D_{idxSrv}_TO_{type}\">En attente de téléchargement</div></td>";
            }
        }
        return $result;
    }
}
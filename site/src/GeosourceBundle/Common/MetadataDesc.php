<?php
namespace ProdigeCatalogue\GeosourceBundle\Common;

use Prodige\ProdigeBundle\DAOProxy\DAO;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
     brief classe permettant l'acces a la vue v_desc_metadata
     SELECT
     m."id"                as metadataId,
     cd."couchd_nom"           as coucheNom,
     cd."couchd_type_stockage"     as coucheType,
     acs."accs_adresse"          as coucheSrv,
     cd."couchd_emplacement_stockage"  as coucheTable,
     m."data"              as metadataXml
     FROM
     "public"."metadata" m
     LEFT JOIN
     "public"."fiche_metadonnees" fm ON (m."id" = fm."fmeta_id")
     LEFT JOIN
     "public"."couche_donnees" cd ON (fm."fmeta_fk_couche_donnees" = cd."pk_couche_donnees")
     LEFT JOIN
 "public"."acces_serveur" acs ON (cd."couchd_fk_acces_server" = acs."pk_acces_serveur")
 * @author Alkante
 *
 */

class MetadataDesc {
    static private    $SQL_SELECT_ALL_FOR_SRV   = "SELECT metadataId from {SCHEMA}.v_desc_metadata order by couchenom";
    static private    $SQL_SELECT_SRVS          = "SELECT coucheSrv from {SCHEMA}.v_desc_metadata order by coucheSrv";

    static private    $SQL_SELECT_ALL_BY_SRV    = "SELECT metadataId from {SCHEMA}.v_desc_metadata where metadataId in ({1}) order by couchenom";
    static private    $SQL_SELECT_ALL_SRV       = "SELECT distinct coucheSrv, couchesrv_tele, couchesrv_service from {SCHEMA}.v_desc_metadata where metadataId in ({1}) order by coucheSrv";
    static private    $SQL_SELECT_FROM_PK       = "SELECT distinct metadataId, coucheNom, coucheType, coucheSrv, coucheTable, 
                                                   couchd_visualisable, metadataXml, couchd_extraction_attributaire_champ, schema,
                                                   array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, metadataXml::xml, ARRAY[ARRAY['gmd', 'http://www.isotc211.org/2005/gmd'], ARRAY['gco','http://www.isotc211.org/2005/gco']]), ' ') as metadata_title
                                                   from {SCHEMA}.v_desc_metadata vdesc
                                                   inner join {SCHEMA}.couche_donnees cd on coucheTable  = cd.couchd_emplacement_stockage
                                                   where metadataId = '{1}' {FILTER}";

    static private    $SQL_SELECT_RASTERINFO_FROM_PK = "SELECT distinct id, name, srid, ulx, uly, lrx, lry, bands, cols, rows, resx, resy, format, vrt_path, compression FROM {SCHEMA}.raster_info WHERE id = '{1}'";

    static private    $SQL_CONDITION_PATTERN    = "{1}";
    static private    $SQL_PATTERN_SRV      = "{SRV}";
    static private    $SQL_CATALOGUE_SCHEMA_NAME      = "{SCHEMA}";
    static private    $SQL_FILTER_TYPE      = "{FILTER}";
    

    static public     $TYPE_STOCKAGE_RASTER     = "0";
    static public     $TYPE_STOCKAGE_POSTGIS    = "1";
    static public     $TYPE_STOCKAGE_MAJIC      = "-2";
    static public     $TYPE_STOCKAGE_TABLE      = "-3";
    static public     $TYPE_STOCKAGE_VUES       = "-4";


    static private    $SQL_IDX_ID         = 0;
    static private    $SQL_IDX_NOM        = 1;
    static private    $SQL_IDX_TYPE       = 2;
    static private    $SQL_IDX_SRV        = 3;
    static private    $SQL_IDX_TABLE      = 4;
    static private 	  $SQL_IDX_3D			    = 5;
    static private    $SQL_IDX_XML        = 6;
    static private    $SQL_IDX_EXTRACTIONATTRIBUTAIRE = 7;
    static private    $SQL_IDX_SCHEMA = 8;
    static private    $SQL_IDX_TITRE = 9;

    protected     $m_Id;
    protected     $m_Titre;
    protected     $m_Nom;
    protected     $m_Type;
    protected     $m_Srv;
    protected     $m_Table;
    protected 	  $m_3D;
    protected 	  $m_Schema;
    protected     $m_Xml;
    protected     $m_Extractionattributaire;

    protected $params;

    /**
     * @var \Doctrine\DBAL\Connection
     */
    static private $conn;
    
    public $container;

    /**
     * @param \Doctrine\DBAL\Connection $conn
     */
    static public function setConnection(\Doctrine\DBAL\Connection $conn) {
        self::$conn = $conn;
    }
    
    public function setContainer($container)
    {
        $this->container = $container;
        $this->m_Srv      = $this->container->getParameter('PRODIGE_URL_FRONTCARTO');/*$rs->Read(MetadataDesc::$SQL_IDX_SRV);*/
    }


    public function __construct($pk=null, $type="") {
        $this->m_Id = $pk;
        $this->m_Nom = null;
        $this->m_Type = array();
        $this->m_Srv = null;
        $this->m_Table = array();
        $this->m_3D = null;
        $this->m_Xml = null;
        $this->m_Schema = null;
        $this->m_Extractionattributaire = null;

        if(!is_null($pk)) {

            //$dao = new DAO();
            $dao = new DAO(self::$conn, 'catalogue');

            if($dao) {
                $m_sSql = MetadataDesc::$SQL_SELECT_FROM_PK;
                $m_sSql = str_replace(MetadataDesc::$SQL_CONDITION_PATTERN, $pk, $m_sSql);
                $m_sSql = str_replace(MetadataDesc::$SQL_CATALOGUE_SCHEMA_NAME, PRO_SCHEMA_NAME, $m_sSql);
                
                if($type!=""){
                  $m_sSql = str_replace(MetadataDesc::$SQL_FILTER_TYPE, "and couchd_type_stockage=".$type, $m_sSql);
                }else{
                  $m_sSql = str_replace(MetadataDesc::$SQL_FILTER_TYPE, "", $m_sSql);
                }

                $rs = $dao->BuildResultSet($m_sSql);
                if($rs->GetNbRows() > 0) {
                    $rs->First();
                    while($rs!=null && !$rs->EOF()){
                        $this->m_Id       = $rs->Read(MetadataDesc::$SQL_IDX_ID);
                        $this->m_Titre    =  $rs->Read(MetadataDesc::$SQL_IDX_TITRE);
                        $this->m_Nom      = $rs->Read(MetadataDesc::$SQL_IDX_NOM);
                        $this->m_Type[]     = $rs->Read(MetadataDesc::$SQL_IDX_TYPE);
                        $this->m_Table[]  = $rs->Read(MetadataDesc::$SQL_IDX_TABLE);
                        $this->m_3D 	    = $rs->Read(MetadataDesc::$SQL_IDX_3D);
                        $this->m_Xml      = "<?xml version='1.0' encoding='UTF-8'?>\n".$rs->ReadHtml(MetadataDesc::$SQL_IDX_XML);
                        $this->m_Extractionattributaire = $rs->Read(MetadataDesc::$SQL_IDX_EXTRACTIONATTRIBUTAIRE);
                        $this->m_Schema   = $rs->Read(MetadataDesc::$SQL_IDX_SCHEMA);
                        $rs->Next();
                    }
                }
            }
        }
    }

    /**
     * brief Retourne l'identifiant
     * @return m_id
     */
    public function GetId(){
        return $this->GetValue($this->m_Id);
    }
    /**
     * brief Retourne le nom
     * @return m_Nom
     */
    public function GetNom(){
        return $this->GetValue(str_replace(array("\r","\n","\t"), " ", $this->m_Nom));
    }
    
     /**
     * brief Retourne le nom
     * @return m_Nom
     */
    public function GetTitre(){
        return $this->GetValue(str_replace(array("\r","\n","\t"), " ", $this->m_Titre));
    }
    /**
     * brief Retourne le type
     * @return m_Type
     */
    public function GetType(){
        return $this->GetValue($this->m_Type);
    }
    /**
     * brief Retourne le serveur
     * @return m_Srv
     */
    public function GetSrv(){
        return $this->GetValue($this->m_Srv);
    }
    /**
     * brief Retourne la table
     * @return m_Table
     */
    public function GetTable(){
        return $this->GetValue($this->m_Table);
    }

    /**
     * brief Retourne la valeur du champ couchd_extraction_attributaire_champ (NULL si pas d'extraction attributaire)
     * @return m_Extractionattributaire
     */
    public function GetExtractionattributaire(){
        return $this->GetValue($this->m_Extractionattributaire);
    }
    /**
     * brief Retourne la valeur du champ couchd_visualisable (0 si données 3D)
     * @return m_3D
     */
    public function Get3D(){
        return $this->GetValue($this->m_3D);
    }
    
    /**
     * brief Retourne la valeur du schema
     * @return schema
     */
    public function GetSchema(){
        return $this->GetValue($this->m_Schema);
    }
    /**
     * brief Retourne le xml des métadonnées
     * @return m_Xml
     */
    public function GetXml(){
        return $this->GetValue($this->m_Xml);
    }
    /**
     * brief Retourne la valeur
     * @param $value
     * @return $l_sResult
     */
    private function GetValue($value){
        $l_sResult = "";
        if (!is_null($value)) $l_sResult = $value;
        return $l_sResult;
    }
    /**
     * brief Retourne les identifiants des métadonnées du serveur $ srv
     * @param $srv : serveur
     * @return $ids : les identifiants
     */
    static public function GetMetadataOfServeur($srv) {
        $ids = array();

        //$dao = new DAO();
        $dao = new DAO(self::$conn, 'catalogue');
        if($dao) {
            $m_sSql = MetadataDesc::$SQL_SELECT_ALL_FOR_SRV;
            $m_sSql = str_replace(MetadataDesc::$SQL_PATTERN_SRV, $srv, $m_sSql);
            $m_sSql = str_replace(MetadataDesc::$SQL_CATALOGUE_SCHEMA_NAME, PRO_SCHEMA_NAME, $m_sSql);
            $rs = $dao->BuildResultSet($m_sSql);
            if ($rs->GetNbRows() > 0){
                $rs->First();
                $idx = 0;
                while($rs!=null && !$rs->EOF()){
                    $ids[$idx] = $rs->Read(MetadataDesc::$SQL_IDX_ID);
                    $rs->Next();
                    $idx++;
                }
            }
        }

        return $ids;
    }
    /**
     * brief Retourne les identifiants $metadataIdts des métadonnées présentes sur le serveur $srv
     * @param $srv
     * @param $metadataIdts
     * @return $ids
     */
    static public function GetIdsByServeur($srv, $metadataIdts){
        $sqlCondition_layers = "";
        for ($idx=0;$idx<count($metadataIdts);$idx++){
            if ($sqlCondition_layers != "") $sqlCondition_layers .= ", ";
            $sqlCondition_layers .= "'".$metadataIdts[$idx]."'";
        }

        $ids = array();

        //$dao = new DAO();
        $dao = new DAO(self::$conn, 'catalogue');
        if($dao) {
            $m_sSql = MetadataDesc::$SQL_SELECT_ALL_BY_SRV;
            $m_sSql = str_replace(MetadataDesc::$SQL_PATTERN_SRV, $srv, $m_sSql);
            $m_sSql = str_replace(MetadataDesc::$SQL_CONDITION_PATTERN, $sqlCondition_layers, $m_sSql);
            $m_sSql = str_replace(MetadataDesc::$SQL_CATALOGUE_SCHEMA_NAME, PRO_SCHEMA_NAME, $m_sSql);
            
            $rs = $dao->BuildResultSet($m_sSql);
            if ($rs->GetNbRows() > 0){
                $rs->First();
                $idx = 0;
                while($rs!=null && !$rs->EOF()){
                    $ids[$idx] = $rs->Read(MetadataDesc::$SQL_IDX_ID);
                    $rs->Next();
                    $idx++;
                }
            }
        }

        return $ids;
    }
    /**
     * brief Retourne les identifiants des serveurs hébergeant les métadonnées ayant pour identifiants $metadataIdts
     *        Le résultat est un tableau qui contient les valeurs suivantes : couchesrv|couchesrv_tele|couchesrv_service
     * @param $metadataIdts
     * @return $ids
     */
    public function GetServeurs($metadataIdts){
        $sqlCondition_layers = "";
        for ($idx=0;$idx<count($metadataIdts);$idx++){
            if ($sqlCondition_layers != "") $sqlCondition_layers .= ", ";
            $sqlCondition_layers .= "'".$metadataIdts[$idx]."'";
        }

        $ids = array();

        //$dao = new DAO();
        $dao = new DAO(self::$conn, 'catalogue');
        if($dao) {
            $m_sSql = MetadataDesc::$SQL_SELECT_ALL_SRV;
            $m_sSql = str_replace(MetadataDesc::$SQL_CONDITION_PATTERN, $sqlCondition_layers, $m_sSql);
            $m_sSql = str_replace(MetadataDesc::$SQL_CATALOGUE_SCHEMA_NAME, PRO_SCHEMA_NAME, $m_sSql);
            $rs = $dao->BuildResultSet($m_sSql);
            if ($rs->GetNbRows() > 0){
                $rs->First();
                $idx = 0;
                while($rs!=null && !$rs->EOF()){
                    $ids[$idx] = $this->container->getParameter('PRODIGE_URL_FRONTCARTO')/*$rs->Read(0)*/."|".$this->container->getParameter('PRODIGE_URL_TELECARTO')/*$rs->Read(1)*/."|".$rs->Read(2);
                    $rs->Next();
                    $idx++;
                }
            }
        }

        return $ids;
    }
    /**
     * brief Retourne les identifiants de tous les serveurs
     * @return $ids
     */
    static public function GetAllServeurs(){
        $ids = array();

        //$dao = new DAO();
        $dao = new DAO(self::$conn, 'catalogue');
        if ($dao){
            $m_sSql = MetadataDesc::$SQL_SELECT_SRVS;
            $m_sSql = str_replace(MetadataDesc::$SQL_CATALOGUE_SCHEMA_NAME, PRO_SCHEMA_NAME, $m_sSql);
            $rs = $dao->BuildResultSet($m_sSql);
            if ($rs->GetNbRows() > 0){
                $rs->First();
                $idx = 0;
                while($rs!=null && !$rs->EOF()){
                    $ids[$idx] = $rs->Read(0);
                    $rs->Next();
                    $idx++;
                }
            }
        }

        return $ids;
    }
}
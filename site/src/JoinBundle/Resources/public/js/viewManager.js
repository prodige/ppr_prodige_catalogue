Carmen.ViewManager = Ext.extend(Ext.util.Observable, {
  previewRowCount : 15,
  renderInWindow : true,
  wins : {},

  callbacks : {},
  closeCallback : Ext.emptyFn,
  beforeUpdateViewAction : Ext.emptyFn,

  viewCreationPanel : new Ext.form.FormPanel({
    width : '100%',
    height : 150,
    frame : true,

    title : 'Création de la vue ',
    id : 'creationPanel',
    labelWidth : 250,
    items : [],
    buttons : [{
      id : 'creationPanelCancel',
      text : 'Annuler'
    }, {
      id : 'creationPanelValidate',
      text : 'Valider'
    }]
  }),

  joinCreationPanel : new Ext.form.FormPanel({
    width : 400,
    height : 150,
    frame : true,

    title : 'Création de la jointure',
    id : 'joinCreationPanel',
    labelWidth : 150,
    items : [],
    buttons : [{
      id : 'joinCreationPanelCancel',
      text : 'Annuler'
    }, {
      id : 'joinCreationPanelValidate',
      text : 'Valider'
    }]
  }),

  joinEditionPanel : new Ext.form.FormPanel({
    width : 400,
    height : 150,
    frame : true,

    title : 'Paramétrage de la jointure ',
    id : 'joinEditionPanel',
    labelWidth : 150,
    items : [],
    buttons : [{
      id : 'joinEditionPanelCancel',
      text : 'Annuler'
    }, {
      id : 'joinEditionPanelValidate',
      text : 'Valider'
    }]
  }),

  fieldCreationPanel : new Ext.form.FormPanel({
    width : 400,
    height : 150,
    frame : true,

    title : 'Création du champ calculé',
    id : 'fieldCreationPanel',
    labelWidth : 150,
    items : [{
      xtype : 'textfield',
      fieldLabel : 'Nom',
      id : 'fcp_fieldnametext',
      name : 'fieldname',
      width : 200,
      blankText : 'Ce champ est obligatoire',
      vtype : 'alphanum',
      allowBlank : false,
      emptyText : 'Entrez le nom du champ'
    }, {
      xtype : 'combo',
      tpl : '<tpl for="."><div ext:qtip="{desc}" class="x-combo-list-item">{desc}</div></tpl>',
      id : 'fcp_fieldTypeCombo',
      allowBlank : false,
      fieldLabel : 'Type',
      name : 'fieldtype',
      width : 200,
      blankText : 'Ce champ est obligatoire',
      allowBlank : false,
      emptyText : 'Choisissez le type de données',
      mode : 'local',
      triggerAction : 'all',
      displayField : 'desc',
      valueField : 'value',
      store : new Ext.data.ArrayStore({
        fields : ["desc", "value"],
        data : [
          ["Entier", "int4"], 
          ["Réel", "float8"],
          ["Chaîne de caractères", "varchar"], 
          ["Booléen", "boolean"]
        ]
      })
    }],
    buttons : [{
      id : 'fieldCreationPanelCancel',
      text : 'Annuler'
    }, {
      id : 'fieldCreationPanelValidate',
      text : 'Valider'
    }]
  }),

  groupViewPanel : new Ext.TabPanel({
    manager : null,
    viewInfo : null,
    height : 330,
    frame : true,
    activeTab : 0,
    title : 'Paramétrage de la vue',
    id : 'groupViewPanel',
    labelWidth : 150,
    items : []

  }),

  constructor : function(config) {
    Ext.apply(this, config);
    this.addEvents({
      "fired" : true,
      "quit" : true
    });

    // Copy configured listeners into *this* object so that the base class's
    // constructor will add them.
    this.listeners = config.listeners;

    // Call our superclass constructor to complete construction process.
    Carmen.ViewManager.superclass.constructor.call(this, config);

    // linking actions if possible
    if (config.callbacks != null){
    	if ( Ext.isObject(config.callbacks) ){
        Ext.iterate(config.callbacks, function(step, callback){
        	this.callbacks[step] = eval(callback);
        }, this);
    	} else {
    		this.callbacks = eval('('+config.callbacks+')');
    	}
    }
    if (config.closeCallback != null){
      this.closeCallback = eval(config.closeCallback);
    } else if ( this.callbacks.close ){
    	this.closeCallback = this.callbacks.close;
    }
    else if (config.closeCallbackUrl != null) {
      this.closeCallbackUrl = config.closeCallbackUrl;
      this.closeCallback = function() {
        document.location.href = this.closeCallbackUrl;
      };
    }

    if (this.URL_updateLinkedMapService) {
      this.beforeUpdateViewAction = function(onlyAddition, okCallback, cancelCallback, noRemoval, scope) {
        noRemoval = noRemoval == null ? false : noRemoval;
        if (!onlyAddition)
          this.updateLinkedMaps(okCallback, cancelCallback, noRemoval, scope);
        else
          okCallback.call(this);
      }
    } else {
      this.beforeUpdateViewAction = function(onlyAddition, okCallback, cancelCallback, noRemoval, scope) {
        okCallback.call(this);
      }
    }

    // initializing current view info struture
    this.compositeViewInfo = {
      viewId : config.viewDef.viewId,
      viewName : config.viewDef.viewName,
      viewDesc : config.viewDesc,
      isGeoView : config.viewDef.isGeoView,
      initialViewDesc : config.viewDef.initialViewDesc,
      groupingsDesc : config.viewDef.groupingsDesc
    };
    this.viewId = this.compositeViewInfo.viewId;
    this.initComponents();

  },

  // ///////////////////////////////////
  // Action functions (triggered when the state of the view changes)
  // ///////////////////////////////////

  updateLinkedMaps : function(okCallback, cancelCallback, noRemoval, scope) {
    scope = scope == null ? this : scope;

    var removeViewFromMaps = function(callback) {
      Ext.Ajax.request({
        url : this.vm.URL_updateLinkedMapService,
        method : 'POST',
        params : {
          couche : (this.vm.compositeViewInfo.viewName)
        },
        callback : function(options, success, response) {
          response = Ext.decode(response.responseText);
          this.nextStep.call(this.scope);
        },
        scope : {
          scope : this.scope,
          nextStep : callback
        }
      });
    };

    var handleResponse = function(buttonId, text, opt) {
      if (buttonId == "yes") {
        if (!this.noRemoval)
          this.removeViewFromMaps.call(this, this.okCallback);
        this.okCallback.call(this.scope);
      } else {
        this.cancelCallback.call(this.scope);
      }
    };

    // retrieving the map that are linked to the current view
    Ext.Ajax.request({
      url : this.URL_getLinkedMapService,
      method : 'GET',
      params : {
        layerTable : this.compositeViewInfo.viewName
      },
      callback : function(options, success, response) {
        if (success) {
          response = Ext.decode(response.responseText);
          if (response && response.length > 0) {
            var strMaps = "";
            for (var i = 0; i < response.length; i++)
              strMaps += "<b>" + response[i] + "</b><br>";
            // if in "no removal" mode, only check for linked maps, do not ask
            // for continue and removal of views from maps
            if (noRemoval) {
              Ext.Msg.confirm(
                  "Mise à jour de la vue",
                  "La vue est contenue dans les cartes suivantes :<br>"
                      + strMaps
                      + "Cette modification de la vue nécessitera sans doute une mise à jour manuelle de ces cartes."
                      + " Souhaitez-vous poursuivre la modification ?",
                  this.handleResponse, this);
            } else {
              Ext.Msg.confirm(
                  "Mise à jour de la vue",
                  "La vue est contenue dans les cartes suivantes :<br>"
                      + strMaps
                      + "Cette modification de la vue entraînera la suppression de la vue dans ces cartes."
                      + " Souhaitez-vous poursuivre la modification ?",
                  this.handleResponse, this);
            }
          } else
            this.okCallback.call(this.scope);
        } else {
          Ext.Msg.alert(
              'Erreur',
              "Il n'a pas été possible de déterminer si des cartes étaient liées à la vue courante."
                  + " Les modifications sur la vue peuvent nécessiter la mise à jour manuelle de certaines cartes.");
          this.okCallback.call(this.scope);
        }

      },
      scope : {
        vm : this,
        okCallback : okCallback,
        cancelCallback : cancelCallback,
        handleResponse : handleResponse,
        removeViewFromMaps : removeViewFromMaps,
        noRemoval : noRemoval,
        scope : scope
      }
    });

  },

  // ///////////////////////////////////
  // creation Panel
  // ///////////////////////////////////
  viewCreationPanel_init : function() {

    var btnCancel = this.viewCreationPanel.buttons[0];
    btnCancel.setHandler(function() {
      this.fireEvent('quit');
    }, this);

    var btnValidate = this.viewCreationPanel.buttons[1];
    btnValidate.setHandler(this.viewCreationPanelValidate, this);

    var layerNameCombo = new Ext.form.ComboBox({
      //tpl : '<tpl for="."><div ext:qtip="{desc}" class="x-combo-list-item">{desc}</div></tpl>',
      id : 'cp_layerNameCombo',
      allowBlank : false,
      fieldLabel : 'Série de données géographiques associée',
      name : 'layername',
      emptyText : 'Choisissez une série de données géographiques',
      width : 400,
      listWidth : 'auto',
      loadingText  : 'Chargement en cours...',
      store : this.compositeViewInfo.isGeoView
          ? this.layerNameStore
          : this.tableNameStore,
      mode : 'local',
      triggerAction : 'all',
      displayField : 'desc',
      valueField : 'value'
    });


    this.viewCreationPanel.add(layerNameCombo);
  },

  viewCreationPanel_update : function() {
    this.viewCreationPanel.setTitle('Création de la vue "'+ this.compositeViewInfo.viewName+'"');
    if (this.compositeViewInfo.isGeoView) {
      var layerNameStore = Ext.StoreMgr.get('cp_layerNameStore');
      layerNameStore.load();
      layerNameStore.load({
        callback : function(r, opt, success) {
          if (success) {
            var found = layerNameStore.findExact('value',
                this.compositeViewInfo.viewName);
            if (found != -1) {
              var rec = layerNameStore.getAt(found);
              layerNameStore.remove(rec);
            }
          }
        },
        scope : this
      });
    } else {
      var tableNameStore = Ext.StoreMgr.get('cp_tableNameStore');
      tableNameStore.load();
      tableNameStore.load({
        callback : function(r, opt, success) {
          if (success) {
            var found = tableNameStore.findExact('value',
                this.compositeViewInfo.viewName);
            if (found != -1) {
              var rec = tableNameStore.getAt(found);
              tableNameStore.remove(rec);
            }
          }
        },
        scope : this
      });
    }
  },

  viewCreationPanelValidate : function() {
    var form = this.viewCreationPanel.getForm();
    var fieldValues = form.getFieldValues();
    this.currentViewInfo = {
      "viewId" : this.compositeViewInfo.viewId,
      "layerName" : fieldValues["layername"]
    };
    var params = {
      viewId : this.compositeViewInfo.viewId,
      viewName : this.compositeViewInfo.viewName,
      layerName : this.currentViewInfo.layerName,
      isGeoView : this.compositeViewInfo.isGeoView ? 1 : 0,
      urlUpdateMetadataView : this.URL_updateMetadataDateService,
      urlUpdateMetadataField : this.URL_updateMetadataCatalogueService
    };
    Ext.fly('creationPanel').mask("Opération en cours...", 'creationPanel');
    form.submit({
      clientValidation : true,
      url : this.URL_viewManageService,
      params : {
        serviceName : 'createCompositeView',
        parameters : Ext.encode(params)
      },
      success : function(form, action) {
        Ext.fly('creationPanel').unmask();
        Ext.Msg.alert('Success', action.result.msg);
        if ( this.callbacks.createView ) this.callbacks.createView.call(this, params);
        this.hideComponent(this.viewCreationPanel);
        this.showComponent(this.groupViewPanel);
      },
      failure : function(form, action) {
        Ext.fly('creationPanel').unmask();
        switch (action.failureType) {
          case Ext.form.Action.CLIENT_INVALID :
            Ext.Msg.alert('Erreur', 'Valeurs incorrectes ou manquantes pour certains champs');
          break;
          case Ext.form.Action.CONNECT_FAILURE :
            Ext.Msg.alert('Erreur', 'Problème de communication avec le serveur');
          break;
          default :
            Ext.Msg.alert('Erreur',
                ((action.result && action.result.errmsg)
                    ? action.result.errmsg
                    : "Erreur lors de l'appel du service"));
        }
      },
      scope : this
    });
  },

  // //////////////////////////////
  // Join Creation Panel
  // //////////////////////////////
  joinCreationPanel_init : function() {
    var btnJoinCancel = this.joinCreationPanel.buttons[0];
    btnJoinCancel.setHandler(function() {
      this.hideComponent(this.joinCreationPanel);
      // this.showComponent(this.viewEditionPanel);
      this.showComponent(this.groupViewPanel);
    }, this);

    var btnJoinValidate = this.joinCreationPanel.buttons[1];
    btnJoinValidate.setHandler(this.joinCreationPanelValidate, this);

    var tableNameCombo = new Ext.form.ComboBox({
      tpl : '<tpl for="."><div ext:qtip="{desc}" class="x-combo-list-item">{desc}</div></tpl>',
      id : 'jcp_tableNameCombo',
      allowBlank : false,
      fieldLabel : 'Table à joindre',
      name : 'tablename',
      emptyText : 'Choisissez une table',
      width : 400,
      store : this.tableNameStore,
      mode : 'local',
      triggerAction : 'all',
      displayField : 'desc',
      valueField : 'value'
    });
    this.joinCreationPanel.add(tableNameCombo);
  },

  joinCreationPanelValidate : function() {
    var form = this.joinCreationPanel.getForm();
    var fieldValues = form.getFieldValues();
    var tableName = fieldValues["tablename"];
    if (form.isValid()) {
      this.currentJoin.tableName = tableName;
      this.hideComponent(this.joinCreationPanel);
      this.showComponent(this.joinEditionPanel, [this.currentJoin]);
    }
  },

  joinCreationPanel_update : function(currentJoin) {
    this.currentJoin = currentJoin;
    var tableNameCombo = this.joinCreationPanel.items.first();
    tableNameCombo.reset();
    // exclude the current view from the tables available for joins
    tableNameCombo.getStore().reload({
      callback : function(r, opt, success) {
        if (success) {
          var found = this.store.findExact('value',
              this.control.compositeViewInfo.viewName);
          if (found != -1) {
            var rec = this.store.getAt(found);
            this.store.remove(rec);
          }
        }
      },
      scope : {
        control : this,
        store : tableNameCombo.getStore()
      }
    });
  },

  // //////////////////////////////
  // Join Edition Panel
  // //////////////////////////////
  joinEditionPanel_init : function() {
    var btnJoinEditionCancel = this.joinEditionPanel.buttons[0];
    btnJoinEditionCancel.setHandler(function() {
      this.hideComponent(this.joinEditionPanel);
      // this.showComponent(this.viewEditionPanel);
      this.showComponent(this.groupViewPanel);
    }, this);

    var btnJoinEditionValidate = this.joinEditionPanel.buttons[1];
    btnJoinEditionValidate.setHandler(this.joinEditionPanelValidate, this);

    var viewFieldNameCombo = new Ext.form.ComboBox({
      tpl : '<tpl for="."><div ext:qtip="{desc}" class="x-combo-list-item">{desc}</div></tpl>',
      id : 'jep_layerFieldNameCombo',
      allowBlank : false,
      fieldLabel : 'Champ de la vue',
      name : 'viewfieldname',
      emptyText : 'Choisissez un champ',
      width : 200,
      store : this.viewFieldNameStore,
      mode : 'local',
      triggerAction : 'all',
      displayField : 'desc',
      valueField : 'value'
    });
    this.joinEditionPanel.add(viewFieldNameCombo);

    // table fieldname combo
    var tableFieldNameStore = new Ext.data.JsonStore({
      url : this.URL_viewManageService,
      baseParams : {
        serviceName : "getTableFields"
      },
      fields : [{
        name : "value",
        convert : function(v, r) {
          return {
            name : r.fieldname,
            type : r.type
          };
        }
      }, {
        name : "desc",
        convert : function(v, r) {
          return r.fieldname + '  (' + r.type + ')';
        }
      }, "fieldname", "type"],
      root : "data",
      idProperty : "fieldname",
      successProperty : "success"
    });
    var tableFieldNameCombo = new Ext.form.ComboBox({
      tpl : '<tpl for="."><div ext:qtip="{desc}" class="x-combo-list-item">{desc}</div></tpl>',
      id : 'jep_tableFieldNameCombo',
      allowBlank : false,
      fieldLabel : 'Champ de la table',
      name : 'tablefieldname',
      emptyText : 'Choisissez un champ',
      width : 200,
      store : tableFieldNameStore,
      mode : 'local',
      triggerAction : 'all',
      displayField : 'desc',
      valueField : 'value'
    });
    this.joinEditionPanel.add(tableFieldNameCombo);

    var restrictionCheckBox = new Ext.form.Checkbox({
      fieldLabel : 'Jointure restrictive',
      name : 'restriction',
      width : 200
    });
    this.joinEditionPanel.add(restrictionCheckBox);
  },

  joinEditionPanelValidate : function() {
    var form = this.joinEditionPanel.getForm();
    var fieldValues = form.getFieldValues();
    var viewField = fieldValues["viewfieldname"];
    var tableField = fieldValues["tablefieldname"];
    var restriction = fieldValues["restriction"];
    if (form.isValid()) {
      // checking that fields have compatible types...
      if (pg2prodigeType(viewField.type) != pg2prodigeType(tableField.type)) {
        Ext.Msg.alert('Erreur', 'Les deux champs doivent avoir le même type');
      } else {
        form.submit({
          url : this.URL_viewManageService,
          params : {
            serviceName : 'createJoin',
            parameters : Ext.encode({
              viewId : this.viewId,
              tableName : this.currentJoin.tableName,
              viewField : viewField.name,
              tableField : tableField.name,
              restriction : restriction,
              joinPos : this.currentJoin.joinPos,
              urlUpdateMetadataView : this.URL_updateMetadataDateService,
              urlUpdateMetadataField : this.URL_updateMetadataCatalogueService
            })
          },
          success : function(form, action) {
            Ext.Msg.alert('Opération réussie', action.result.msg);
            this.hideComponent(this.joinEditionPanel);
            // this.showComponent(this.viewEditionPanel);
            this.showComponent(this.groupViewPanel);
          },
          failure : function(form, action) {
            switch (action.failureType) {
              case Ext.form.Action.CLIENT_INVALID :
                Ext.Msg.alert('Erreur', 'Valeurs incorrectes ou manquantes pour certains champs');
              break;
              case Ext.form.Action.CONNECT_FAILURE :
                Ext.Msg.alert('Erreur', 'Problème de communication avec le serveur');
              break;
              default :
                Ext.Msg.alert('Erreur',
                    ((action.result && action.result.errmsg)
                        ? action.result.errmsg
                        : "Erreur lors de l'appel du service"));
            }
          },
          scope : this
        });
      }
    }
  },

  joinEditionPanel_update : function(currentJoin) {
    this.currentJoin = currentJoin;
    var vParams = {
      parameters : Ext.encode({
        viewId : this.getCurrentViewInfo().viewId,
        joinFilter : this.currentJoin.joinPos
      })
    };
    var viewFieldNameCombo = this.joinEditionPanel.items.first();
    viewFieldNameCombo.reset();
    var viewFieldNameStore = viewFieldNameCombo.getStore();
    // loading viewField combo and setting curent value if existing...
    viewFieldNameStore.load({
      params : vParams,
      callback : function(r, o, s) {
        if (s && this.viewField != null) {
        	var index = this.store.find('fieldname', this.viewField);
        	if ( index<0 ) return;
          var rec = this.store.getAt(index);
          if (rec)
            this.combo.setValue(rec.get('value'));
        }
      },
      scope : {
        combo : viewFieldNameCombo,
        store : viewFieldNameStore,
        viewField : this.currentJoin.viewField
      }
    });

    var tParams = {
      parameters : Ext.encode({
        tablename : this.currentJoin.tableName
      })
    };
    var tableFieldNameCombo = this.joinEditionPanel.items.itemAt(1);
    tableFieldNameCombo.reset();
    var tableFieldNameStore = tableFieldNameCombo.getStore();
    // loading tableField combo and setting curent value if existing...
    tableFieldNameStore.load({
      params : tParams,
      callback : function(r, o, s) {
        if (s && this.tableField != null) {
          var rec = this.store.getById(this.tableField);
          if (rec)
            this.combo.setValue(rec.data.value);
        }
      },
      scope : {
        combo : tableFieldNameCombo,
        store : tableFieldNameStore,
        tableField : this.currentJoin.tableField
      }
    });

    var restrictionCheckBox = this.joinEditionPanel.items.itemAt(2);
    restrictionCheckBox.reset();
    if (this.currentJoin.restriction != null)
      restrictionCheckBox.setValue(this.currentJoin.restriction);
  },

  // //////////////////////////////
  // Field Creation Panel
  // //////////////////////////////
  fieldCreationPanel_init : function() {
    var btnFieldCancel = this.fieldCreationPanel.buttons[0];
    btnFieldCancel.setHandler(function() {
      this.hideComponent(this.fieldCreationPanel);
      // this.showComponent(this.viewEditionPanel);
      this.showComponent(this.groupViewPanel);
    }, this);

    var btnFieldValidate = this.fieldCreationPanel.buttons[1];
    btnFieldValidate.setHandler(this.fieldCreationPanelValidate, this);
  },

  fieldCreationPanelValidate : function() {
    var form = this.fieldCreationPanel.getForm();
    var fieldValues = form.getFieldValues();
    var fieldname = fieldValues["fieldname"];
    var fieldtype = fieldValues["fieldtype"];
    if (form.isValid()) {
      this.currentField.fieldname = "cc_" + fieldname;
      this.currentField.fieldtype = fieldtype;
      this.currentField.expression = null;
      // checking if the field name is correct
      Ext.Ajax.request({
        url : this.URL_viewManageService,
        method : 'POST',
        params : {
          serviceName : 'getFields',
          parameters : Ext.encode({
            viewId : this.viewId
          })
        },
        success : function(response) {
          response = Ext.decode(response.responseText);
          if (response && response.success) {
            var fields = response.fields;
            if (fields.indexOf(this.currentField.fieldname) == -1) {
              this.hideComponent(this.fieldCreationPanel);
              this.showComponent(this.fieldEditionPanel, [this.currentViewInfo, this.currentField]);
            } else {
              Ext.Msg.alert(
                      'Vérification du nom du champ',
                      "Le nom du champ : '"
                          + this.currentField.fieldname
                          + "' entre en conflit avec un champ existant dans la vue. Veuillez saisir un autre nom.");
              this.fieldCreationPanel.getForm().findField("fcp_fieldnametext")
                  .markInvalid();
            }
          } else
            Ext.Msg.alert('Erreur dans la vérification du nom du champ',
                ((response && response.errmsg)
                    ? response.errmsg
                    : "Erreur lors de l'appel du service"));
        },
        failure : function(response) {
          response = Ext.decode(response.responseText);
          Ext.Msg.alert('Erreur dans la vérification du nom du champ',
              ((response && response.errmsg)
                  ? response.errmsg
                  : "Erreur lors de l'appel du service"));
        },
        scope : this
      });
    }
  },
  fieldCreationPanel_update : function(viewInfo, currentField) {
    this.currentViewInfo = viewInfo;
    this.currentField = currentField;
    this.fieldCreationPanel.getForm().reset();
  },
  // //////////////////////////////
  // Field Edition Panel
  // //////////////////////////////

  fieldEditionPanelValidate : function(expression) {
    var updateFieldMode = (this.currentField.expression != "");
    // passing current field to manager to prevent scope pbs...
    // should be done another way...
    this.manager.currentField = this.currentField;

    if (expression != "") {
      this.currentField.expression = expression;
      if (!updateFieldMode)
        this.manager.updateFieldExpression();
      else
        this.manager.beforeUpdateViewAction.call(this.manager, false, this.manager.updateFieldExpression, Ext.emptyFn, true);
    }
  },

  updateFieldExpression : function() {
    // checking if the field name is correct and
    // updating fieldExpression in the view definition
    Ext.Ajax.request({
      url : this.URL_viewManageService,
      method : 'POST',
      params : {
        serviceName : 'createComputedField',
        parameters : Ext.encode({
          viewId : this.viewId,
          fieldname : this.currentField.fieldname.substr(3),
          fieldtype : this.currentField.fieldtype,
          expression : this.currentField.expression,
          urlUpdateMetadataView : this.URL_updateMetadataDateService,
          urlUpdateMetadataField : this.URL_updateMetadataCatalogueService
        })
      },
      success : function(response) {
        response = Ext.decode(response.responseText);
        if (response && response.success) {
          Ext.Msg.alert('Opération réussie', response.msg,
              this.fieldExpressionCheck, this);
        } else {
          Ext.Msg.alert('Erreur dans la construction du champ',
              ((response && response.errmsg)
                  ? response.errmsg
                  : "Erreur lors de l'appel du service"));
          this.fieldEditionPanel.invalidateExpression();
          // this.fieldEditionPanel.getForm().findField("expression").markInvalid();
        }
      },
      failure : function(response) {
        response = Ext.decode(response.responseText);
        Ext.Msg.alert('Erreur dans la construction du champ',
            ((response && response.errmsg)
                ? response.errmsg
                : "Erreur lors de l'appel du service"));
      },
      scope : this
    });
  },

  fieldExpressionCheck : function() {
    // checking if the field expression is valid with respect
    // to data, e.g. a division by zero is not performed...
    Ext.Ajax.request({
      url : this.URL_viewManageService,
      method : 'POST',
      params : {
        serviceName : 'checkComputedFieldWithData',
        parameters : Ext.encode({
          viewId : this.viewId,
          fieldname : this.currentField.fieldname.substr(3)
        })
      },
      success : function(response) {
        response = Ext.decode(response.responseText);
        if (response && !response.success) {
          Ext.Msg.show({
            title : 'Vérification du champ',
            msg : 'La vérification du champ calculé sur les données a provoqué une erreur (une division par zéro ou une autre opération interdite...). Voulez vous revenir sur la définition du champ ?',
            buttons : Ext.Msg.YESNO,
            fn : function(bId) {
              if (bId == "no") {
                this.hideComponent(this.fieldEditionPanel);
                // this.showComponent(this.viewEditionPanel);
                this.showComponent(this.groupViewPanel);
              }
            },
            icon : Ext.MessageBox.ALERT,
            scope : this
          });
        } else { // no errors have been found
          this.hideComponent(this.fieldEditionPanel);
          // this.showComponent(this.viewEditionPanel);
          this.showComponent(this.groupViewPanel);
        }
      },
      failure : function(response) {
        response = Ext.decode(response.responseText);
        Ext.Msg.alert('Erreur dans la vérification du champ',
            ((response && response.errmsg)
                ? response.errmsg
                : "Erreur lors de l'appel du service"));
      },
      scope : this
    });
  },

  fieldEditionPanelCheck : function() {
    var form = this.fieldEditionPanel.getForm();
    if (form.isValid()) {
      var expression = form.getFieldValues()["expression"];
      // checking if the field name is correct
      Ext.Ajax.request({
        url : this.URL_viewManageService,
        method : 'POST',
        params : {
          serviceName : 'checkFieldExpression',
          parameters : Ext.encode({
            viewId : this.viewId,
            expression : expression
          })
        },
        success : function(response) {
          response = Ext.decode(response.responseText);
          if (response && response.success) {
            Ext.Msg.alert('Vérification de l\'expression', response.msg);
          } else
            Ext.Msg.alert('Vérification de l\'expression', (response.errmsg
                    ? response.errmsg
                    : "Erreur lors de l'appel du service"));
        },
        failure : function(response) {
          response = Ext.decode(response.responseText);
          Ext.Msg.alert('Erreur dans la vérification du nom du champ',
              ((response && response.errmsg)
                  ? response.errmsg
                  : "Erreur lors de l'appel du service"));
        },
        scope : this
      });
    } else {
      Ext.Msg.alert('Définition du champ',
          'Vous devez saisir une expression pour le champ');
    }
  },

  // //////////////////////////////
  // Filter Edition Panel
  // //////////////////////////////

  filterEditionPanelValidate : function(expression) {
    this.getCurrentViewInfo().filterExpression = expression;
    this.beforeUpdateViewAction.call(this, false, this.updateFilterExpression,
        Ext.emptyFn, true);
  },

  updateFilterExpression : function() {
    // checking if the field name is correct and
    // updating fieldExpression in the view definition
    Ext.Ajax.request({
      url : this.URL_viewManageService,
      method : 'POST',
      params : {
        serviceName : 'updateFilter',
        parameters : Ext.encode({
          viewId : this.compositeViewInfo.viewId,
          expression : this.getCurrentViewInfo().filterExpression,
          urlUpdateMetadataView : this.URL_updateMetadataDateService,
          urlUpdateMetadataField : this.URL_updateMetadataCatalogueService
        })
      },
      success : function(response) {
        response = Ext.decode(response.responseText);
        if (response && response.success) {
          Ext.Msg.alert('Opération réussie', response.msg, function() {
            this.hideComponent(this.filterEditionPanel);
            // this.showComponent(this.viewEditionPanel);
            this.showComponent(this.groupViewPanel);
          }, this);
        } else {
          Ext.Msg.alert('Erreur dans la vérification du filtre',
              ((response && response.errmsg)
                  ? response.errmsg
                  : "Erreur lors de l'appel du service"));
          this.filterEditionPanel.invalidateExpression();
        }
      },
      failure : function(response) {
        response = Ext.decode(response.responseText);
        Ext.Msg.alert('Erreur dans la vérification du filtre',
            ((response && response.errmsg)
                ? response.errmsg
                : "Erreur lors de l'appel du service"));
      },
      scope : this
    });
  },

  // //////////////////////////////////////////////////
  // Preview window
  // //////////////////////////////////////////////////
  showViewPreview : function(viewInfo) {
    var fields = new Array();
    var tabColumns = new Array();

    // Requesting for field desc...
    Ext.Ajax.request({
      url : this.URL_viewManageService,
      method : 'POST',
      params : {
        serviceName : 'getFields',
        parameters : Ext.encode({
          viewId : viewInfo.viewId
        })
      },
      success : function(response) {
        response = Ext.decode(response.responseText);
        fields = response.fields;

        var store = new Ext.data.JsonStore({
          url : this.URL_viewManageService,
          baseParams : {
            serviceName : "getData",
            parameters : Ext.encode({
              viewId : viewInfo.viewId,
              limit : this.previewRowCount
            })
          },
          fields : fields,
          root : 'data',
          succesProperty : 'success',
          listeners : {
            load : function() {
              Ext.fly("viewPreviewWindow").unmask()
            }
          }
        });

        var bodySize = Ext.getBody().getSize();
        
        var columnWidth = 100;
        for (var i = 0; i < fields.length; i++) {
          tabColumns.push({
            id : "fields_" + i,
            header : fields[i],
            width : columnWidth,
            sortable : true,
            dataIndex : fields[i]
          });
        }
        // create the Grid
        var grid = new Ext.grid.GridPanel({
          store : store,
          columns : tabColumns,
          stripeRows : true,
          view : new Ext.ux.grid.BufferView({
            // render rows as they come into viewable area.
            scrollDelay : false,
            // cleanDelay: 200,
            deferEmptyText : false,
            // forceFit: true, // to adapt column width so that all
            // columns fit in the grid width
            showPreview : true, // custom property
            enableRowBody : true
              // required to create a second, full-width row to show
              // expanded Record data
            }),
          height : bodySize.height - 50,
          selModel : new Ext.grid.RowSelectionModel
        });

        var win = new Ext.Window({
          id : "viewPreviewWindow",
          resizable : true,
          title : "Aperçu de la vue " + viewInfo.viewName + " (" + (this.previewRowCount) + " premières lignes)",
          width : bodySize.width - 100,
          height : 400,
          layout : 'fit',
          autoScroll : true,
          items : [grid]
        });
        win.show();
        grid.render('db-grid');
        Ext.fly("viewPreviewWindow").mask("Récupération des données en cours...", 'viewPreviewWindow');
        // load data
        store.load();
      },
      failure : function(response) {
        var w = Ext.fly("viewPreviewWindow");
        if (w)
          w.unmask();
        response = Ext.decode(response.responseText);
        Ext.Msg.alert('Erreur dans l\'aperçu',
            ((response && response.errmsg)
                ? response.errmsg
                : "Erreur lors de l'appel du service"));

      },
      scope : this
    });
  },

  // ///////////////////////////////////////////////////////////////////////////////////
  // Group View Panel
  // ///////////////////////////////////////////////////////////////////////////////////

  retrieveCompositeViewDesc : function(viewId, callback) {
    // retrieving the map that are linked to the current view
    Ext.Ajax.request({
      url : this.getManageServiceURL(),
      method : 'POST',
      params : {
        serviceName : "getCompositeViewDesc",
        parameters : Ext.encode({
          viewId : this.viewId
        })
      },
      callback : function(options, success, response) {
        if (success) {
          response = Ext.decode(response.responseText);
          if (response && response.success) {
            if (response.data.viewId = this.scope.compositeViewInfo.viewId) {
              this.scope.compositeViewInfo = response.data;
              this.callback.call(this.scope);
            } else
              Ext.Msg.alert('Erreur', "Erreur dans la récupération des données de la vue.");
          } else
            Ext.Msg.alert('Erreur', "Impossible de récupérer la définition de la vue.");
        } else {
          Ext.Msg.alert('Erreur', "Impossible de récupérer la définition de la vue.");
        }
      },
      scope : {
        callback : callback,
        scope : this
      }
    });
  },

  groupViewPanel_init : function() {

  },

  groupViewPanel_update : function() {
    this.retrieveCompositeViewDesc(this.compositeViewInfo.viewId,
        this.groupViewPanel_updateIHM);
  },

  groupViewPanel_updateIHM : function() {
    // upd of currentViewInfo
    var grpCount = this.compositeViewInfo.groupingsDesc.length;
    this.currentViewInfo = grpCount > 0
        ? this.compositeViewInfo.groupingsDesc[grpCount - 1]
        : this.compositeViewInfo.initialView;

    this.groupViewPanel.removeAll();

    // build editions panel wrt groupings...
    var viewEditionPanel = new Carmen.View.EditionPanel({
      isAgregate : false,
      title : 'Vue initiale : "'+ this.compositeViewInfo.viewName+'"',
      tabTip : this.prettyPrintViewInfo(this.compositeViewInfo.initialViewDesc),
      manager : this,
      viewManageServiceURL : this.getManageServiceURL(),
      viewId : this.compositeViewInfo.initialViewDesc.viewId,
      viewInfo : this.compositeViewInfo.initialViewDesc,
      compositeViewId : this.compositeViewInfo.viewId
    });
    this.groupViewPanel.add(viewEditionPanel);
    for (var i = 0; i < this.compositeViewInfo.groupingsDesc.length; i++) {
      viewEditionPanel = new Carmen.View.EditionPanel({
        isAgregate : true,
        agregatePos : i + 1,
        title : "agrégation " + (i + 1),
        tabTip : this.prettyPrintViewInfo(this.compositeViewInfo.groupingsDesc[i]),
        manager : this,
        viewManageServiceURL : this.getManageServiceURL(),
        viewId : this.compositeViewInfo.groupingsDesc[i].viewId,
        viewInfo : this.compositeViewInfo.groupingsDesc[i],
        compositeViewId : this.compositeViewInfo.viewId
      });
      // group view panel
      this.groupViewPanel.add(viewEditionPanel);
    }

    var lastTab = this.groupViewPanel.items.getCount() - 1;
    for (var i = 0; i < lastTab; i++) {
      this.groupViewPanel.items.get(i).disable();
    }
    this.groupViewPanel.setActiveTab(lastTab);
    this.groupViewPanel.getActiveTab().update();
    this.groupViewPanel.setHeight(this.groupViewPanel.getActiveTab().getHeight()+30);
    this.groupViewPanel.getActiveTab().show();

  },

  getParentViewInfo : function() {
    var res = this.compositeViewInfo.initialViewDesc;
    var groupingCount = this.compositeViewInfo.groupingsDesc.length;
    if (groupingCount > 1)
      res = this.compositeViewInfo.groupingsDesc[groupingCount - 2];
    return res;
  },

  getCurrentViewInfo : function() {
    var res = this.compositeViewInfo.initialViewDesc;
    var groupingCount = this.compositeViewInfo.groupingsDesc.length;
    if (groupingCount > 0)
      res = this.compositeViewInfo.groupingsDesc[groupingCount - 1];
    return res;
  },

  prettyPrintViewInfo : function(viewInfo) {
    var res = '<ul>';
    res += '<li>' + 'Nom de la table : ' + viewInfo.layerName + '</li>';
    res += '<li>' + 'Filtre : ' + viewInfo.filterExpression + '</li>';
    res += '<li>' + 'Tri : ' + viewInfo.sortingField + ' (' + viewInfo.sortingFunction + ')' + '</li>';
    if (viewInfo.joins) {
      for (var i = 0; i < viewInfo.joins.length; i++) {
        res += '<li>' + 'Jointure avec : ' + viewInfo.joins[i].tablename + ' ('
            + viewInfo.joins[i].criteria + ')' + '</li>';
      }
    }
    if (viewInfo.computedFields) {
      for (var i = 0; i < viewInfo.computedFields.length; i++) {
        res += '<li>' + 'Champ calculé : '
            + viewInfo.computedFields[i].fieldname + ' ('
            + viewInfo.computedFields[i].expression + ')' + '</li>';
      }
    }
    res += '</ul>';

    return res;
  },

  // ///////////////////////////////////////
  // Main component init...
  // ////////////////////////////////////////

  initComponents : function() {
    // //////////////////////
    // initializing stores
    // //////////////////////

    // view fieldname store
    this.viewFieldNameStore = new Ext.data.JsonStore({
      id : 'viewFieldNameStore',
      url : this.URL_viewManageService,
      baseParams : {
        serviceName : "getViewFields"
      },
      fields : [{
        name : "value",
        convert : function(v, r) {
          return {
            name : r.fieldname,
            type : r.type
          };
        }
      }, {
        name : "desc",
        convert : function(v, r) {
          return r.fieldname + '  (' + r.type + ')';
        }
      }, "fieldname", "type"],
      root : "data",
      idProperty : "fieldname",
      successProperty : "success"
    });

    // layernames combo
    if (this.localLayerStoreConfigData) {
      this.layerNameStore = new Ext.data.ArrayStore({
        fields : [{
          name : "value"
        }, {
          name : "desc"
        }],
        storeId : 'cp_layerNameStore',
        data : this.localLayerStoreConfigData
      });
      this.layerNameStore.load = Ext.emptyFn;
    } else if (this.URL_layerNamesService) {

      this.layerNameStore = new Ext.data.Store({
        reader : Carmen.ProdigeCatalogueReader,
        proxy : new Ext.data.ScriptTagProxy({
          url : this.URL_layerNamesService
        }),
        storeId : 'cp_layerNameStore'
      });
    }

    // tablenames combo
    if (this.localTableStoreConfigData) {
      this.tableNameStore = new Ext.data.ArrayStore({
        fields : [{
          name : "value"
        }, {
          name : "desc"
        }],
        storeId : 'cp_tableNameStore',
        data : this.localTableStoreConfigData
      });
      // overwriting load function for simplicity...
      this.tableNameStore.load = Ext.emptyFn;
    } else if (this.URL_tableNamesService) {
      this.tableNameStore = new Ext.data.Store({
        reader : Carmen.ProdigeCatalogueReader,
        proxy : new Ext.data.ScriptTagProxy({
          url : this.URL_tableNamesService
        }),
        storeId : 'cp_tableNameStore'
      });
    }

    this.sortDataFunctionStore = new Ext.data.JsonStore({
      id : 'sortDataFunctionStore',
      fields : [{
            name : "functionName",
        mapping : 0
      }, {
        name : "functionId",
        mapping : 1
      }, {
        name : "value",
        convert : function(v, r) {
          return {
            name : r[0],
            id : r[1]
          };
        }
      }, {
        name : "functionDesc",
        mapping : 2
      }],
      root : "data",
      url : this.getManageServiceURL(),
      baseParams : {
        serviceName : "getSortDataFunctions"
      },
      successProperty : "success"
    });
    this.sortDataFunctionStore.load();

    // //////////////////////
    // initializing panels
    // //////////////////////

    // sort data window
    this.sortDataWindow = new sortDataWindow();

    // field visibility panel
    this.fieldVisibilityPanel = new Carmen.View.FieldVisibilityPanel({
      manager : this,
      viewInfo : this.getCurrentViewInfo(),
      compositeViewInfo : this.compositeViewInfo
    });

    // field edition panel
    this.fieldEditionPanel = new Carmen.View.ExpressionPanel({
      title : 'definition du champ calculé',
      manager : this,
      operatorStore : {
        url : this.URL_viewManageService,
        baseParams : {
          serviceName : "getFieldsExpOperators"
        }
      },
      fieldStore : {
        url : this.URL_viewManageService,
        baseParams : {
          serviceName : "getViewFields",
          parameters : Ext.encode({
            viewId : this.viewId
          })
        }
      },
      listeners : {
        "cancel" : {
          fn : function() {
            this.hideComponent(this.fieldEditionPanel);
            this.showComponent(this.groupViewPanel);
          },
          scope : this
        },
        "validate" : {
          fn : function(cpt, expression) {
            this.manager.fieldEditionPanelValidate.apply(this, [expression]);
          }
        }
      },
      update : function(viewInfo, currentField) {
        this.setTitle("Définition du champ calculé " + currentField.fieldname);
        var vParams = {
          parameters : Ext.encode({
            viewId : viewInfo.viewId,
            fieldFilter : currentField.fieldname.substr(3)
          })
        };
        this.getFieldStore().load({
          params : vParams
        });
        this.getFieldStore().load({
          params : vParams
        });
        this.getOperatorStore().load();
        this.getOperatorStore().load();
        this.currentField = currentField;
        if (currentField.expression)
          this.setExpression(currentField.expression);
        else
          this.clearExpression();
      }
    });

    this.filterEditionPanel = new Carmen.View.ExpressionPanel({
      title : 'Paramétrage du filtre de la vue',
      operatorStore : {
        url : this.URL_viewManageService,
        baseParams : {
          serviceName : "getFieldsExpOperators"
        }
      },
      fieldStore : {
        url : this.URL_viewManageService,
        baseParams : {
          serviceName : "getViewFields",
          parameters : Ext.encode({
            viewId : this.getCurrentViewInfo().viewId
          })
        }
      },
      listeners : {
        "cancel" : {
          fn : function() {
            this.hideComponent(this.filterEditionPanel);
            this.showComponent(this.groupViewPanel);
          },
          scope : this
        },
        "validate" : {
          fn : function(cpt, expression) {
            this.filterEditionPanelValidate(expression);
          },
          scope : this
        }
      },
      expressionArea : {
        allowBlank : true,
        emptyText : "Aucun filtre"
      },

      update : function(viewInfo) {
        this.viewInfo = viewInfo;
        var vParams = {
          parameters : Ext.encode({
            viewId : this.viewInfo.viewId
          })
        };
        this.getFieldStore().load({
          params : vParams
        });
        this.getFieldStore().load({
          params : vParams
        });
        this.getOperatorStore().load();
        this.getOperatorStore().load();
        this.setExpression(this.viewInfo.filterExpression);
      }
    });

    this.viewAggregationPanel = new groupingEditionPanel({
      viewFieldNameStore : this.viewFieldNameStore,
      manager : this,
      compositeViewId : this.compositeViewInfo.viewId
    });

    // component initialization and update is driven by component showing...
    this.viewCreationPanel.init = this.viewCreationPanel_init;
    // this.viewEditionPanel.init = this.viewEditionPanel_init;
    this.joinCreationPanel.init = this.joinCreationPanel_init;
    this.joinEditionPanel.init = this.joinEditionPanel_init;
    this.fieldCreationPanel.init = this.fieldCreationPanel_init;
    this.groupViewPanel.init = this.groupViewPanel_init;
    //this.fieldVisibilityPanel.init = this.fieldVisibilityPanel_init;
    //this.filterEditionPanel.init = Ext.emptyFn;

    this.viewCreationPanel.update = this.viewCreationPanel_update;
    this.joinCreationPanel.update = this.joinCreationPanel_update;
    this.joinEditionPanel.update = this.joinEditionPanel_update;
    //this.viewEditionPanel.update = this.viewEditionPanel_update;
    this.fieldCreationPanel.update = this.fieldCreationPanel_update;
    this.groupViewPanel.update = this.groupViewPanel_update;
    //this.fieldEditionPanel.update = this.fieldEditionPanel_update;
    //this.fieldVisibilityPanel.update = this.fieldVisibilityPanel_update;
    //this.filterEditionPanel.update = this.filterEditionPanel_update;

    // Main object configuration
    this.on('quit', this.closeCallback, this);
  },

  show : function() {
    if (this.createMode) {
      this.showComponent(this.viewCreationPanel);
    } else {
      //this.showComponent(this.viewEditionPanel);
      this.showComponent(this.groupViewPanel);
    }
  },

  showComponent : function(panel, params) {
    if (!Ext.isArray(params)) {
      params = [params];
    }
    if (this.renderInWindow) {
      this.wins[panel.id] = new Ext.Window({
        layout : 'fit',
        items : [panel]
      });
      if (panel.rendered) {
        if (panel.update)
          if (panel == this.fieldVisibilityPanel
              || panel == this.filterEditionPanel
              || panel == this.fieldEditionPanel
              || panel == this.viewEditionPanel
              || panel == this.viewAggregationPanel)
            panel.update.apply(panel, params);
          else
            panel.update.apply(this, params);
      } else {
        if (panel.init)
          if (panel == this.viewAggregationPanel)
            panel.init.apply(panel);
          else
            panel.init.apply(this);
        if (panel.update)
          if (panel == this.fieldVisibilityPanel
              || panel == this.filterEditionPanel
              || panel == this.fieldEditionPanel
              || panel == this.viewEditionPanel
              || panel == this.viewAggregationPanel)
            panel.update.apply(panel, params);
          else
            panel.update.apply(this, params);
      }
      this.wins[panel.id].show();
    } else {
      panel.setWidth('auto');
      panel.setHeight('auto');
      if (panel.rendered) {
        if (panel.update)
          if (panel == this.fieldVisibilityPanel
              || panel == this.filterEditionPanel
              || panel == this.fieldEditionPanel
              || panel == this.viewEditionPanel
              || panel == this.viewAggregationPanel)
            panel.update.apply(panel, params);
          else
            panel.update.apply(this, params);

        panel.show();

      } else {
        if (panel.init)
          if (panel == this.viewAggregationPanel)
            panel.init.apply(panel);
          else
            panel.init.apply(this);
        if (panel.update)
          if (panel == this.fieldVisibilityPanel
              || panel == this.filterEditionPanel
              || panel == this.fieldEditionPanel
              || panel == this.viewEditionPanel
              || panel == this.viewAggregationPanel)
            panel.update.apply(panel, params);
          else
            panel.update.apply(this, params);
        panel.render(document.body);
      }
    }
  },

  hideComponent : function(panel) {
    if (this.renderInWindow)
      this.wins[panel.id].hide();
    else
      panel.hide();
  },

  joinRestrictionRenderer : function(value, metaData, record, rowIndex,
      colIndex, store) {
    return value ? 'Oui' : 'Non';
  },

  btnDelRenderer : function(value, metaData, record, rowIndex, colIndex, store) {
    metaData.css = 'silk-delete';
    return '';
  },

  btnEditRenderer : function(value, metaData, record, rowIndex, colIndex, store) {
    metaData.css = 'silk-cog';
    return '';
  },

  getManageServiceURL : function() {
    return this.URL_viewManageService;
  },

  getUpdateMetadataDateServiceURL : function() {
    return this.URL_updateMetadataDateService;
  },

  getUpdateMetadataCatalogueServiceURL : function() {
    return this.URL_updateMetadataCatalogueService;
  }

});

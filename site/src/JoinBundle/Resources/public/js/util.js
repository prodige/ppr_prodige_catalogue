function TextEncode(strParam)
{
  var t = new String("test");
  if (!t.charCodeAt) return strParam;
  strParam = encodeToUTF8(strParam);
  var strEncode = "";
  for(var i=0; i<strParam.length; i++) {
    strEncode += DecToHexa(strParam.charCodeAt(i));
  }
  return strEncode;
}

function TextDecode(strParam)
{  
  var test = "";
  var t = new String("test");
  if (!t.fromCharCode && !String.fromCharCode) return strParam;
  //décodage
  var strDecode = "";
  for(var i=0; i<strParam.length; i+=2 ) {
    strDecode = new String(strDecode) + String.fromCharCode( HexaToDec(strParam.substr(i, 2) ) ) ;
  }

  return decodeFromUTF8(strDecode);
}

function DecToHexa(integer, bIter){
  var tabConv = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
  if (integer<=15 ) {
    if( bIter) return tabConv[integer]; 
    return "0"+tabConv[integer];
  }
  var quotient = Math.floor(integer/16);
  var remainder = integer % 16;
  return new String(DecToHexa(quotient,true))+new String(DecToHexa(remainder,true));
}

function HexaToDec(hexa){
  var tabConv = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F");
  if (hexa.length==1){
    for (var j=0; j<tabConv.length; j++){
      if (tabConv[j].toLowerCase()==hexa.toLowerCase()) return j;
    }
    return 0;
  }
  var res = 0;
  for (var p=0; p<hexa.length; p++){
    res = new Number( new Number(res) + new Number(HexaToDec(hexa.charAt(p))*Math.pow(16, hexa.length - p -1)) );
  }
  return res;
}

function encodeToUTF8(strLatin1) 
{
  var strUTF8 = "";

  for (var n = 0; n < strLatin1.length; n++) {
    var c = strLatin1.charCodeAt(n);
    if (c < 128) {
      strUTF8 += String.fromCharCode(c);
    }
    else if((c > 127) && (c < 2048)) {
      strUTF8 += String.fromCharCode((c >> 6) | 192);
      strUTF8 += String.fromCharCode((c & 63) | 128);
    }
    else {
      strUTF8 += String.fromCharCode((c >> 12) | 224);
      strUTF8 += String.fromCharCode(((c >> 6) & 63) | 128);
      strUTF8 += String.fromCharCode((c & 63) | 128);
    }
  }

  return strUTF8;
}
    
function decodeFromUTF8(strUTF8) {
  var strLatin1 = "";
  var i = 0;
  var c = c1 = c2 = 0;

  while ( i < strUTF8.length ) {
    c = strUTF8.charCodeAt(i);
    if (c < 128) {
      strLatin1 += String.fromCharCode(c);
      i++;
    }
    else if((c > 191) && (c < 224)) {
      c2 = strUTF8.charCodeAt(i+1);
      strLatin1 += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
      i += 2;
    }
    else {
      c2 = strUTF8.charCodeAt(i+1);
      c3 = strUTF8.charCodeAt(i+2);
      strLatin1 += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
      i += 3;
    }
  }

  return strLatin1;
}

function getCheckSumEncodage(strToken)
{
  var tabTrad = new Array();
  for(var i=0; i<10; i++) tabTrad[i] = i;
  tabTrad["A"] = tabTrad["a"] = 10;
  tabTrad["B"] = tabTrad["b"] = 11;
  tabTrad["C"] = tabTrad["c"] = 12;
  tabTrad["D"] = tabTrad["d"] = 13;
  tabTrad["E"] = tabTrad["e"] = 14;
  tabTrad["F"] = tabTrad["f"] = 15;
   
  var iChecksum = 0;
  for(i=0; i<strToken.length; i++) {
    iChecksum += parseInt(tabTrad[strToken.charAt(i)]);
  }
  return iChecksum;
} 

// ExtJs overload to support base64 encoding/decoding
// taken from http://www.sencha.com/forum/showthread.php?35328-Ext.util.base64-(encode-decode)
Ext.util.base64 = {

    base64s : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
    
    encode: function(decStr){
        if (typeof btoa === 'function') {
             return btoa(decStr);            
        }
        var base64s = this.base64s;
        var bits;
        var dual;
        var i = 0;
        var encOut = "";
        while(decStr.length >= i + 3){
            bits = (decStr.charCodeAt(i++) & 0xff) <<16 | (decStr.charCodeAt(i++) & 0xff) <<8 | decStr.charCodeAt(i++) & 0xff;
            encOut += base64s.charAt((bits & 0x00fc0000) >>18) + base64s.charAt((bits & 0x0003f000) >>12) + base64s.charAt((bits & 0x00000fc0) >> 6) + base64s.charAt((bits & 0x0000003f));
        }
        if(decStr.length -i > 0 && decStr.length -i < 3){
            dual = Boolean(decStr.length -i -1);
            bits = ((decStr.charCodeAt(i++) & 0xff) <<16) |    (dual ? (decStr.charCodeAt(i) & 0xff) <<8 : 0);
            encOut += base64s.charAt((bits & 0x00fc0000) >>18) + base64s.charAt((bits & 0x0003f000) >>12) + (dual ? base64s.charAt((bits & 0x00000fc0) >>6) : '=') + '=';
        }
        return(encOut);
    },
    
    decode: function(encStr){
        if (typeof atob === 'function') {
            return atob(encStr); 
        }
        var base64s = this.base64s;        
        var bits;
        var decOut = "";
        var i = 0;
        for(; i<encStr.length; i += 4){
            bits = (base64s.indexOf(encStr.charAt(i)) & 0xff) <<18 | (base64s.indexOf(encStr.charAt(i +1)) & 0xff) <<12 | (base64s.indexOf(encStr.charAt(i +2)) & 0xff) << 6 | base64s.indexOf(encStr.charAt(i +3)) & 0xff;
            decOut += String.fromCharCode((bits & 0xff0000) >>16, (bits & 0xff00) >>8, bits & 0xff);
        }
        if(encStr.charCodeAt(i -2) == 61){
            return(decOut.substring(0, decOut.length -2));
        }
        else if(encStr.charCodeAt(i -1) == 61){
            return(decOut.substring(0, decOut.length -1));
        }
        else {
            return(decOut);
        }
    }

};  


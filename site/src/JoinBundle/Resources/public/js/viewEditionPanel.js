var mask;
Carmen.View.EditionPanel = Ext.extend(Ext.form.FormPanel, {
	config : {
		manager : null,
		viewManageServiceURL : null,
		viewId : null,
		currentJoin : null,
		currentField : null,
		width : 'auto',
		frame : true,
		title : 'Vue initiale',
		items : [{xtype:'panel', html : "<center><i>La vue sera mise à jour immédiatement sur les actions validées.<br/>L'aperçu permet de constater la prise en compte des traitements.</i></center>", height:30}],
		buttons : [
				{ 
					text : 'Agréger'
				},
				{ 
					text : 'Aperçu'
				},
				{ 
					text : 'Fermer'			
				},
				{ 
					text : 'Réinitialiser'
				}]	
	},
	
	initButtonCfg : {
		text : 'Réinitialisation'
	},
	
	agregateSubPanelCfg : {
		width : 505,
		height : 60,
		layout : { type : 'hbox' },
		title : 'Agrégation',
		frame : true,
		items : [
			{ 
				xtype : 'button',
				iconCls : 'silk-cog',
				id : 'agregateSubPanelEdit',
				text : 'Modifier l\'agrégation'
			},
			{ 
				xtype : 'button',
				iconCls : 'silk-delete',
				id : 'agregateSubPanelDel',
				text : 'Supprimer l\'agrégation'
			}
		]
	},
	
	joinSubPanelCfg : {
		width : 'auto',
		height : 260,
		collapsible : true,
		title : 'Jointures',
		frame : true,
		items : [
			{ 
				xtype : 'button',
				iconCls : 'silk-add',
				id : 'joinSubPanelAdd',
				text : 'Ajouter une jointure'
			}
		]
	},
	
	fieldSubPanelCfg : {
    width : 'auto',
    height : 260,
    collapsible : true,
		title : 'Champs calculés',
		frame : true,
		items : [
			{ 
				xtype : 'button',
				iconCls : 'silk-add',
				id : 'fieldSubPanelAdd',
				text : 'Ajouter un champ calculé'
			}
		]
	},
	
	joinStoreCfg : {
		baseParams : {
			serviceName : "getJoins"
		},
		fields : [
			"tablename", 
			"criteria", 
			{ name : "restriction", convert : function(v,r) { return (parseInt(v)==0);}}, 
			"joinPos" ],
		idProperty : "joinPos",
		root : "data",
		successProperty : "success"
	},

	joinGridPanelCfg : {
		columns: [
			{header: "Table", width: 120, sortable: false, menuDisabled : true, dataIndex : "tablename"},
			{header: "Critère de jointure", width: 180, sortable: false, menuDisabled : true, align : 'center', dataIndex : "criteria"},
			{header: "Restriction", width: 110, sortable: false, menuDisabled : true, align : 'center', dataIndex : "restriction", renderer : Carmen.View.Util.joinRestrictionRenderer},
			{header: "", width: 20, sortable: false, menuDisabled : true, align : 'center', renderer : Carmen.View.Util.btnEditRenderer, id : 'vep_joinDelColumn', dataIndex : "joinPos" , resizable: false},
			{header: "", width: 20, sortable: false, menuDisabled : true, align : 'center', renderer : Carmen.View.Util.btnDelRenderer, id : 'vep_joinEditColumn', dataIndex : "joinPos" , resizable: false}
		],
		stripeRows: true,
		enableColumnMove  : false,
		height : 200,
		width : 'auto',
		viewConfig : {forceFit:true},
		frame : true,
		selModel: new Ext.grid.RowSelectionModel()
	},
	
	fieldStoreCfg :  {
		baseParams : {
			serviceName : "getComputedFields"
		},
		fields : [
			"fieldname", 
			"expression",
			"type"
			],
		idProperty : "fieldname",
		root : "data",
		successProperty : "success"
	},

	fieldGridPanelCfg : {
		columns: [
			{header: "Nom du champ", width: 120, sortable: false, menuDisabled : true, dataIndex : "fieldname"},
			{header: "Expression", width: 300, sortable: false, menuDisabled : true, align : 'center', dataIndex : "expression"},
			{header: "", width: 20, sortable: false, menuDisabled : true, align : 'center', renderer : Carmen.View.Util.btnEditRenderer, id : 'vep_fieldDelColumn', dataIndex : "fieldname" , resizable: false},
			{header: "", width: 20, sortable: false, menuDisabled : true, align : 'center', renderer : Carmen.View.Util.btnDelRenderer, id : 'vep_fieldEditColumn', dataIndex : "fieldname" , resizable: false}
		],
		stripeRows: true,
		enableColumnMove  : false,
		height : 200,
		width : 'auto',
    viewConfig : {forceFit:true},
		frame : true,
		selModel: new Ext.grid.RowSelectionModel()
	},
	
	fieldVisibilityButtonCfg : {
		text : 'Paramétrer la visibilité des champs'
	},
		
	filterButtonCfg : {
		text : 'Paramétrer le filtre de la vue'
	},
	
	sortButtonCfg : {
		text : 'Trier les données'
	},
	
	
	
	constructor : function(cfg) {
		// completing config object...
		if ("buttons" in cfg) {
			for (var i=0; i<cfg.buttons.length; i++) {
				this.config.buttons.push(cfg.buttons[i]);
			}
			delete cfg.buttons;
		}
		Ext.apply(this.config, cfg);
		Carmen.View.EditionPanel.superclass.constructor.apply(this, [this.config]);
		
		// events 
		this.addEvents(
			"agregate",
			"close",
			"prevew", 
			"reinit");
			
		// specific store configs
		Ext.apply(this.joinStoreCfg, { url : this.viewManageServiceURL });
		Ext.apply(this.joinStoreCfg.baseParams, { parameters : Ext.encode({ viewId : this.viewId }) });
		
		Ext.apply(this.fieldStoreCfg, { url : this.viewManageServiceURL });
		Ext.apply(this.fieldStoreCfg.baseParams, { parameters : Ext.encode({ viewId : this.viewId }) });
		
		// stores
		this.joinStore = new Ext.data.JsonStore(this.joinStoreCfg);
		this.fieldStore = new Ext.data.JsonStore(this.fieldStoreCfg);
		
		// first load of stores
		this.joinStore.load();
		this.fieldStore.load();
		
    this.viewOptionPanel = new Ext.Panel({
      width : 'auto',
      height : 25,
      layout : { type : 'hbox' }
    });
    // inside buttons (filter and visibility)
    this.filterButton = new Ext.Button(this.filterButtonCfg);
    this.fieldVisibilityButton = new Ext.Button(this.fieldVisibilityButtonCfg);
    this.sortButton = new Ext.Button(this.sortButtonCfg);
    this.viewOptionPanel.add(this.filterButton);
    this.viewOptionPanel.add(this.fieldVisibilityButton);
    this.viewOptionPanel.add(this.sortButton);
    this.add(this.viewOptionPanel);
    
		// panels
		if (this.isAgregate) {
			this.agregatePanel = new Ext.Panel(this.agregateSubPanelCfg);
			this.add(this.agregatePanel);
		}
		
		this.joinGridPanel = new Ext.grid.GridPanel(
			Ext.apply(this.joinGridPanelCfg, 
				{ store : this.joinStore })
		);
		this.joinSubPanel = new Ext.Panel(this.joinSubPanelCfg);
		this.joinSubPanel.add(this.joinGridPanel);
		this.add(this.joinSubPanel);
		
		this.fieldGridPanel = new Ext.grid.GridPanel(
			Ext.apply(this.fieldGridPanelCfg, 
				{ store : this.fieldStore })
		);
		this.fieldSubPanel = new Ext.Panel(this.fieldSubPanelCfg);
		this.fieldSubPanel.add(this.fieldGridPanel);
		this.add(this.fieldSubPanel);
		
		////////////////////////////
		// handlers
		////////////////////////////
		// add handler for click event on edit and del button in gridPanel	
		this.joinGridPanel.on('cellclick',
			function(grid, rowIndex, colIndex, evt) {
				var store = grid.getStore();
				if (colIndex==3 || colIndex==4) {
					if ((rowIndex+1)<store.getCount()) {
						Ext.Msg.alert("Attention", "Seule la dernière jointure est éditable");
					}
					else {
						var id = rowIndex + 1;
						var rec = store.getById(id);
						var criteria = Carmen.View.Util.decodeCriteria(rec.data.criteria);
						this.currentJoin = {
							tableName : rec.data.tablename,
							viewField : criteria.viewField,
							tableField : criteria.tableField,
							restriction : rec.data.restriction,
							joinPos : rec.data.joinPos };
						// edition mode
						if (colIndex==3) { 
							this.manager.hideComponent(this.manager.groupViewPanel);
							this.manager.showComponent(this.manager.joinEditionPanel, [this.currentJoin]);
						}
						// delete mode
						else {
							this.deleteJoin();
						}
					}
				}
			},
			this);
		// add join button			
		var btnJoinAdd = this.joinSubPanel.items.first();
		btnJoinAdd.setHandler(
			function() {
				this.currentJoin = {
					tableName : null,
					viewField : null,
					tableField : null,
					restriction : null,
					joinPos : this.joinStore.getCount()+1
				}
				
        this.manager.hideComponent(this.manager.groupViewPanel); 
        this.manager.showComponent(this.manager.joinCreationPanel, [this.currentJoin]); 
			}, this);
		
		// add handler for click event on edit and del button in gridPanel	
		this.fieldGridPanel.on('cellclick',
			function(grid, rowIndex, colIndex, evt) {
				var store = grid.getStore();
				if (colIndex==2 || colIndex==3) {
					var rec = store.getAt(rowIndex);
					this.currentField = {
							fieldname : 'cc_' + rec.data.fieldname,
							expression : rec.data.expression,
							fieldtype : rec.data.type
					};
					// edition mode
					if (colIndex==2) { 
						this.manager.hideComponent(this.manager.groupViewPanel);
						this.manager.showComponent(this.manager.fieldEditionPanel, [this.viewInfo, this.currentField]);
					}
					// delete mode
					else {
						this.deleteField();
					}
				}
			},
			this);
		// add field button
		var btnFieldAdd = this.fieldSubPanel.items.first();
		btnFieldAdd.setHandler(
			function() {
				this.currentField = {
					fieldname : null,
					expression : null
				};
				this.manager.hideComponent(this.manager.groupViewPanel); 
				this.manager.showComponent(this.manager.fieldCreationPanel, [this.viewInfo, this.currentField]); 
			}, this);

		// filter button
		this.filterButton.setHandler(
			function() {
				this.manager.hideComponent(this.manager.groupViewPanel); 
				this.manager.showComponent(this.manager.filterEditionPanel, this.viewInfo); 
			}, this);
		// field visibility button
		this.fieldVisibilityButton.setHandler(
			function() {
				this.manager.hideComponent(this.manager.groupViewPanel); 
				this.manager.showComponent(this.manager.fieldVisibilityPanel, this.viewInfo); 
			}, this);
		// sort button and sort param window
		this.manager.sortDataWindow.setValidateCallback(
			function() { 
				if (this.ve_sortWindow.check()) {
					var fieldname = this.ve_sortWindow.getField();	
					var sort_fun = this.ve_sortWindow.getSortFunction();	
					//this.ge_panel.addAgregateToView(fieldname, ag_fun);
					this.ve_panel.updateSorting.call(this.ve_panel, fieldname, sort_fun);
					this.ve_sortWindow.hide();
				}
				else {
					Ext.Msg.alert("Erreur","Merci de bien préciser le nom du champ servant pour le tri et la méthode de tri associée.");
				}
			},
			{
				ve_panel : this,
				ve_sortWindow : this.manager.sortDataWindow
			});
		
		this.sortButton.setHandler(
			function() {
				// update of viewFieldNameStore used by sortDataWindow
				this.manager.viewFieldNameStore.load(
					{ 
						params : { parameters : Ext.encode({viewId : this.viewInfo.viewId})},
						callback : function() {
							this.manager.sortDataWindow.init(this.viewInfo.sortingField, this.viewInfo.sortingFunction);
							this.manager.sortDataWindow.show();
						},
						scope : this
					});
			}, this);
				
	
		
		// close and preview button
    this.buttons[1].setHandler(function() { /*preview*/
      this.manager.showViewPreview(this.viewInfo); 
    }, this);
		this.buttons[2].setHandler(function() { /*close*/
		  this.manager.fireEvent('quit'); 
		}, this);
		this.buttons[3].setHandler(function() { /*reinit*/
			this.doReInitVerif();
		}, this);
		this.buttons[0].setHandler(
			function() { 
				this.manager.hideComponent(this.manager.groupViewPanel); 
				this.manager.showComponent(
					this.manager.viewAggregationPanel,
					{
						viewId : this.viewInfo.viewId,
						groupingField : null,
						agregateMapping : [],
						agregatePos : -1
					});
			}, this);
		//Hide agregate button
		this.buttons[0].hidden = true;
    //disabling agregate for geo view
    if (this.manager.compositeViewInfo.isGeoview) 
      this.buttons[0].disable();
		// agregate panel buttons
		if (this.isAgregate) {
			this.agregatePanel.findById("agregateSubPanelEdit").setHandler(
				this.editAgregate,
				this
			);
			this.agregatePanel.findById("agregateSubPanelDel").setHandler(
				this.deleteAgregate,
				this
			);
		}
	},

	doReInitVerif : function(){
	  
		var removeViewFromMaps = function() {
		  
		  mask = new Ext.LoadMask(Ext.getBody(), {msg:"Opération en cours..."}).show();
		  Ext.Ajax.request({
				url: this.scope.manager.URL_updateLinkedMapService,
				method: 'POST',
				params: { 
					couche: Ext.util.base64.encode(this.scope.manager.compositeViewInfo.viewName),
					action : 'del'
				},
				callback: function(options, success, response) {			
					//window.location.reload();
				  Ext.Ajax.request({
		        url: this.scope.manager.URL_deleteViewService,
		        method: 'POST',
		        params: { 
		          couche: Ext.util.base64.encode(this.scope.manager.compositeViewInfo.viewName)
		        },
		        callback: function(options, success, response) {      
		          window.location.reload();
		        },
		        scope : { vm : this, 
		                  scope : this.scope }
		      });
				},
				scope : { vm : this, 
					handleResponse : handleResponse, 
					removeViewFromMaps : removeViewFromMaps,
					scope : this.scope }
			});
		};

		var handleResponse = function(buttonId, text, opt) {
			if (buttonId=="yes") {
				this.removeViewFromMaps.call(this);
			}
			else {
				this.cancelCallback.call(this);
			}
		};


		
		var handleResponse_0 = function(buttonId, text, opt) {
			
			if (buttonId=="yes") {

				Ext.Ajax.request({

					url: this.manager.URL_getLinkedMapService,
					method: 'GET',
					params: { layerTable: this.manager.compositeViewInfo.viewName },
					callback: function(options, success, response) {
					   if (success) {
						   response = Ext.decode(response.responseText);						   
						   if (response && response.length>0) {
								var strMaps ="";
								for (var i=0; i<response.length;i++)			   
									strMaps+= "<b>"+response[i]+"</b><br>"; 

								Ext.MessageBox.confirm("Réinitialisation de la vue", "La vue est contenue dans les cartes suivantes :<br>" +
										strMaps + "Cette modification de la vue entraînera la suppression de la vue dans ces cartes." + 
										" Souhaitez-vous poursuivre la réinitialisation ?", this.handleResponse, this);
						   }else{
							   this.removeViewFromMaps.call(this);
						   }
							 
					   }else{
						   Ext.Msg.alert('Erreur', "Il n'a pas été possible de déterminer si des cartes étaient liées à la vue courante." +
							" Les modifications sur la vue peuvent nécessiter la mise à jour manuelle de certaines cartes.");
						}
							
					},			
					scope : { vm : this, 
						//okCallback : okCallback, 
						//cancelCallback : cancelCallback, 
						handleResponse : handleResponse, 
						removeViewFromMaps : removeViewFromMaps,
						//noRemoval : noRemoval,
						scope : this }
					
				});
			}
			else {
				// arrive ici
				this.cancelCallback.call(this.scope);
			}
		};		
	
		Ext.Msg.confirm("Réinitialisation de la vue", 
		   		" Cette réinitialisation entrainera la suppression de la vue en base de données et la suppression de sa présence dans les cartes." + 
				" Souhaitez-vous poursuivre la réinitialisation ?", handleResponse_0, this);
	
	},

	
	
	initComponent : function() {
		Carmen.View.EditionPanel.superclass.initComponent.apply(this);
	},
		
	deleteJoin : function() {
		var msg = "Voulez vous vraiment supprimer la jointure avec la table " + this.currentJoin.tableName + " ? La suppression sera définitive et ne pourra être annulée.";
		var handleResponse = function(buttonId) {
			if (buttonId=="yes") {
				this.manager.beforeUpdateViewAction.call(this.manager, false, this.doDeleteJoin, Ext.emptyFn, false, this);
			}
		};
		Ext.Msg.confirm("Attention", msg, handleResponse, this);
	},
	doDeleteJoin : function() {
		Ext.Ajax.request({
			url: this.manager.getManageServiceURL(),
			method: 'POST',
			params: { 
				serviceName: 'deleteJoin',
				parameters : Ext.encode({
					viewId : this.viewId,
					joinPos : this.currentJoin.joinPos,
					urlUpdateMetadataView : this.manager.getUpdateMetadataDateServiceURL(),
					urlUpdateMetadataField : this.manager.getUpdateMetadataCatalogueServiceURL()
				})
			},
			success: function(response) {
			   response = Ext.decode(response.responseText);
			   if (response && response.success) {
				   Ext.Msg.alert('Opération réussie', response.msg);
				   this.update.apply(this);
			   }
			   else
				   Ext.Msg.alert('Erreur', ((response && response.errmsg)  ? response.errmsg : "Erreur lors de l'appel du service"));
			},
			failure: function(response) {
         response = Ext.decode(response.responseText);
			   Ext.Msg.alert('Erreur', ((response && response.errmsg)  ? response.errmsg : "Erreur lors de l'appel du service"));
			},
			scope : this
		});
	},
	
	deleteField : function() {
		var msg = "Voulez vous vraiment supprimer le champ calculé " + this.currentField.fieldname + " ? La suppression sera définitive et ne pourra être annulée.";
		var handleResponse = function(buttonId) {
			if (buttonId=="yes") {
				this.manager.beforeUpdateViewAction.call(this.manager, false, this.doDeleteField, Ext.emptyFn, false, this);
			}
		};
		Ext.Msg.confirm("Attention",msg, handleResponse, this);
	},
	doDeleteField : function() {
		Ext.Ajax.request({
			url: this.manager.getManageServiceURL(),
			method: 'POST',
			params: { 
				serviceName: 'deleteComputedField',
				parameters : Ext.encode({
					viewId : this.compositeViewId,
					fieldname : this.currentField.fieldname.substr(3),
					urlUpdateMetadataView : this.manager.getUpdateMetadataDateServiceURL(),
					urlUpdateMetadataField : this.manager.getUpdateMetadataCatalogueServiceURL()
				})
			},
			success: function(response) {
			   response = Ext.decode(response.responseText);
			   if (response && response.success) {
				   Ext.Msg.alert('Opération réussie', response.msg);
				   this.update.apply(this);
			   }
			   else
				   Ext.Msg.alert('Erreur', ((response && response.errmsg)  ? response.errmsg : "Erreur lors de l'appel du service"));
			},
			failure: function(response) {
			   response = Ext.decode(response.responseText);
         Ext.Msg.alert('Erreur', ((response && response.errmsg)  ? response.errmsg : "Erreur lors de l'appel du service"));
			},
			scope : this
		});
	},
	
	deleteAgregate : function() {
		var msg = "Voulez vous vraiment supprimer l'agrégation ? La suppression sera définitive et ne pourra être annulée.";
		var handleResponse = function(buttonId) {
			if (buttonId=="yes") {
				this.manager.beforeUpdateViewAction.call(this.manager, false, this.doDeleteAgregate, Ext.emptyFn, false, this);
			}
		};
		Ext.Msg.confirm("Attention",msg, handleResponse, this);
	},
	
	doDeleteAgregate : function() {
		Ext.Ajax.request({
			url: this.manager.getManageServiceURL(),
			method: 'POST',
			params: { 
				serviceName: 'removeGrouping',
				parameters : Ext.encode({
					viewId : this.compositeViewId,
					urlUpdateMetadataView : this.manager.getUpdateMetadataDateServiceURL(),
					urlUpdateMetadataField : this.manager.getUpdateMetadataCatalogueServiceURL()
				})
			},
			success: function(response) {
			   response = Ext.decode(response.responseText);
			   if (response && response.success) {
				   Ext.Msg.alert('Opération réussie', response.msg);
				   this.manager.groupViewPanel.update.call(this.manager);
			   }
			   else
				   Ext.Msg.alert('Erreur', ((response && response.errmsg)  ? response.errmsg : "Erreur lors de l'appel du service"));
			},
			failure: function(response) {
         response = Ext.decode(response.responseText);
			   Ext.Msg.alert('Erreur', ((response && response.errmsg)  ? response.errmsg : "Erreur lors de l'appel du service"));
			},
			scope : this
		});
	},
	
	
	retrieveAgregateInfo : function(callback, scope) {
		// retrieving the map that are linked to the current view
		Ext.Ajax.request({
			url: this.manager.getManageServiceURL(),
			method: 'POST',
			params: { 
				serviceName : "getGroupingDesc",
				parameters : Ext.encode({
					viewId : this.compositeViewId 
				})
			},
			callback: function(options, success, response) {
			   if (success) {
				   response = Ext.decode(response.responseText);
				   if (response && response.success) {
						var cfg = {
							"groupingField" : response.data.groupingFields[0],
							"agregates" : response.data.agregates,
							"viewId" : this.scope.manager.getParentViewInfo().viewId,
							"agregatePos" : this.scope.agregatePos
						};
						this.callback.call(this.scope,cfg);
				   }
				   else
					 Ext.Msg.alert('Erreur', "Impossible de récupérer les informations d'agrégation.");
			   }
			   else {
				   Ext.Msg.alert('Erreur', "Impossible de récupérer les informations d'agrégation.");
				}
			},
			scope : { callback : callback, scope : this}
		});
	},
	
	editAgregate : function() {
		this.retrieveAgregateInfo(this.editAgregate_aux, this);
	},
	
	editAgregate_aux : function(cfg) {
		this.manager.hideComponent(this.manager.groupViewPanel); 
		this.manager.showComponent(this.manager.viewAggregationPanel, cfg);
	},
	
	//////////////////////////////////
	// sorting
	//////////////////////////////////
	updateSorting : function(fieldname, sort_fun) {
		Ext.Ajax.request({
			url: this.manager.getManageServiceURL(),
			method: 'POST',
			params: { 
				serviceName : "setSortParameters",
				parameters : Ext.encode({
					viewId : this.compositeViewId,
					sortingField : fieldname.name,
					sortingFunction : sort_fun.id
				})
			},
			callback: function(options, success, response) {
			   response = Ext.decode(response.responseText);
         if (success) {
				   if (response && response.success) {
						Ext.Msg.alert('Opération réussie', response.msg);
						this.manager.sortDataWindow.hide();
						this.viewInfo.sortingField=fieldname.name;
						this.viewInfo.sortingFunction=sort_fun.id;
				   }
				   else
					 Ext.Msg.alert('Erreur', ((response && response.errmsg)  ? response.errmsg : "Echec lors de la mise à jour des informations de tri"));
			   }
			   else {
				   Ext.Msg.alert('Erreur', ((response && response.errmsg)  ? response.errmsg : "Echec lors de la mise à jour des informations de tri"));
				}
			},
			scope : this
		});
	},
	
	/////////////////////////////////////
	// Miscellaneous
	/////////////////////////////////////
	update : function() {
		this.joinStore.reload({
			callback : function(r,o,s) {			
				this.viewInfo.joinInfos = [];
				this.viewInfo.joinCount = this.joinStore.getTotalCount();
				for (var i=0; i< this.joinStore.getTotalCount(); i++) {
					var rec = this.joinStore.getAt(i);
					this.viewInfo.joinInfos.push({
						tableName : rec.data.tablename,
						criteria : rec.data.criteria,
						restriction : rec.data.restriction,
						pos : i+1
					});
				}
			},
			scope : this
		});
		this.fieldStore.reload({
			callback : function(r,o,s) {			
				this.viewInfo.fieldInfos = [];
				this.viewInfo.fieldCount = this.fieldStore.getTotalCount();
				for (var i=0; i< this.fieldStore.getTotalCount(); i++) {
					var rec = this.fieldStore.getAt(i);
					this.viewInfo.fieldInfos.push({
						fieldname : rec.data.fieldname,
						expression : rec.data.expression,
						fieldtype : rec.data.type
					});
				}
			},
			scope : this
		});
	}
	
});
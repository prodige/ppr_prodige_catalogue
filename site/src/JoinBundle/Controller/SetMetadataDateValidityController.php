<?php

namespace ProdigeCatalogue\JoinBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;

//use Prodige\ProdigeBundle\Controller\User;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session; //TODO hismail
//use Prodige\ProdigeBundle\Common;
//use Prodige\ProdigeBundle\Common\Util;
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * #brief service qui permet de mettre à jour la date de validité d'une métadonnée
 * @author Alkante
 * @param metadata_id   identifiant de la métadonnée (REQUEST)
 * @param table_name    nom de la table si metadata_id n'est pas fourni (REQUEST, codage base64)
 * @param srv           url du serveur carto si table_name est fourni (REQUEST, codage base64)
 * #from /PRRA/Services/setMetadataDateValidity.php
 */
class SetMetadataDateValidityController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/join/services/setMetadata_date_validity", name="catalogue_join_services_setMetadata_date_validity", options={"expose"=true})
     */
    public function setMetadataDateValidityAction(Request $request) {

        //require_once("../parametrage.php");
        //require_once("../Administration/DAO/DAO/DAO.php");

        //$dao = new DAO();
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');

        $result = array();
        // récupère l'identifiant de la métadonnée
        $metadata_id = -1;
        //if(isset($_REQUEST['metadata_id']) && $_REQUEST['metadata_id'] != "" ) {
        if($request->get("metadata_id", "") != "") {
            //$metadata_id = $_REQUEST['metadata_id'];
            $metadata_id = $request->get("metadata_id");
        } else {
            //if(isset($_REQUEST['table_name']) && $_REQUEST['table_name'] != "" ) {
            if($request->get("table_name", "") != "" ) {
                //if(isset($_REQUEST['srv']) && $_REQUEST['srv'] != "" ) {
                if($request->get("srv", "") != "" ) {
                    //$table_name = base64_decode($_REQUEST['table_name']);
                    $table_name = base64_decode($request->get("table_name"));
                    //$srv        = base64_decode($_REQUEST['srv']);
                    $srv        = base64_decode($request->get("srv"));
                    $dao->setSearchPath(PRO_SCHEMA_NAME);
                    $query = "SELECT fmeta_id FROM v_acces_couche LEFT JOIN fiche_metadonnees" .
                             " on v_acces_couche.couche_pk=fiche_metadonnees.fmeta_fk_couche_donnees" .
                             //" WHERE couche_table='".$table_name."' AND couche_srv='". $srv."'";
                    				 " WHERE couche_table=? AND couche_srv=?";
                    $rs = $dao->BuildResultSet($query, array($table_name, $srv));
                    for($rs->First(); !$rs->EOF(); $rs->Next()) {
                        $metadata_id = $rs->Read(0);
                    }
                    if($metadata_id == -1) {
                        $result['update_success'] = false;
                        $result['msg']            = htmlentities("Impossible de récupérer la métadonnée associée à la table '".$table_name."' et au serveur '".$srv."'.", ENT_QUOTES, "UTF-8");
                        return new JsonResponse($result);
                    }
                } else {
                    $result['update_success'] = false;
                    $result['msg']            = htmlentities("Le paramètre 'srv' est manquant.", ENT_QUOTES, "UTF-8");
                    return new JsonResponse($result);
                }
            } else {
                $result['update_success'] = false;
                $result['msg']            = htmlentities("Le paramètre 'metadata_id' ou 'table_name' est manquant.", ENT_QUOTES, "UTF-8");
                return new JsonResponse($result);
            }
        }

        $dao->setSearchPath("public");
        //$strSql = "SELECT changedate, data FROM metadata WHERE id=".$metadata_id;
        $strSql = "SELECT changedate, data FROM metadata WHERE id=?";
        $rs     = $dao->BuildResultSet($strSql, array($metadata_id));

        if($rs->GetNbRows() > 0) {
            $rs->First();
            $lastChangeDate = $rs->Read(0);
            $data = $rs->Read(1);

            $currentDateTime = date('Y-m-d\TH:i:s');

            $newData = $this->updateData($data, $currentDateTime);

            if($newData != "" ) {
                /*$strSql = "UPDATE metadata SET changedate='".$currentDateTime."', data='".str_replace("'", "''",$newData)."'".
                    " WHERE id=".$metadata_id;*/
                
                //Prodige4
                $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE);
                //send to geosource
                if( $geonetwork->editMetadata($metadata_id, $newData) ) {
                    $result['index_success'] = true;
                }
                /*$strSql = "UPDATE metadata SET changedate=?, data=?".
                          " WHERE id=?";
                if(pg_result_status($dao->Execute($strSql, array($currentDateTime, str_replace("'", "''",$newData),$metadata_id))) == 1) {
                    //$dao->Execute("COMMIT");
                    $dao->commit();

                    $result['update_success'] = true;
                    $result['msg']            = htmlentities("La date de validité de la métadonnée ".$metadata_id." a bien été mise à jour.", ENT_QUOTES, "UTF-8");

                    //indexation de métadonnée
                    require_once("../include/geonetworkInterface.php");
                    $geonetwork = new Geonetwork();
                    $res = $geonetwork->get($PRO_GEONETWORK_DIRECTORY."/srv/fre/metadata.show?id=".$metadata_id);
                    $doc = new \DOMDocument();
                    if(@$doc->loadHTML($res)) {
                        $status = $doc->getElementsByTagName('error');
                        if($status && $status->length > 0) {
                            $result['index_success'] = false;
                        } else {
                            $result['index_success'] = true;
                        }
                    } else {
                        $result['index_success'] = false;
                    }
                }*/
            }

            // erreur de mise à jour de la date de validité
            else {
                $result['update_success'] = false;
                $result['msg']            = htmlentities("Impossible de mettre à jour ou d'ajouter la nouvelle date de validité.", ENT_QUOTES, "UTF-8");
            }
        }
        // métadonnée inexistante
        else {
            $result['update_success'] = false;
            $result['msg']            = htmlentities("La métadonnée n'existe pas.", ENT_QUOTES, "UTF-8");
        }
        return new JsonResponse($result);
    }


    /**
     * brief met à jour la date de validité et la date de mise à jour dans le XML de la métadonnée
     * @param data                contenu XML de la métadonnée
     * @param newLastChangeDate   chaîne contenant la date de mise à jour
     * @return contenu XML de la métadonnée mis à jour, chaîne vide si une erreur est survenue
     */
    protected function updateData($data, $currentDateTime) {

        $newData = "";

        //global $currentDateTime;

        $version  = "1.0";
        $encoding = "UTF-8";
        $entete   = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";

        $data = str_replace("&", "&amp;", $data);
        $data = $entete.$data;

        $doc = new \DOMDocument($version, $encoding);

        if(@$doc->loadXML($data)) {

            $CI_Citation = $doc->getElementsByTagName('CI_Citation')->item(0);
            $CI_Date = $CI_Citation->getElementsByTagName('CI_Date');
            $bExistDateValidity = false;
            $codeList = "";

            foreach($CI_Date as $date_info) {
                $CI_DateTypeCode = $date_info->getElementsByTagName('CI_DateTypeCode')->item(0);
                if($CI_DateTypeCode->hasAttribute('codeListValue')) {
                    if ( $CI_DateTypeCode->getAttribute('codeListValue') == 'revision' ) {
                        $date = $date_info->getElementsByTagName('date')->item(0);
                        $DateTime = $date->getElementsByTagName('DateTime')->item(0);
                        if(!empty($DateTime)){
                            $DateTime->nodeValue = $currentDateTime;
                            $bExistDateValidity = true;
                        }
                    }
                }
                if($CI_DateTypeCode->hasAttribute('codeList') ) {
                    $codeList = $CI_DateTypeCode->getAttribute('codeList');
                }
            }

            if(!$bExistDateValidity) {
                $new_dates            = $doc->createElement('gmd:date');
                $new_CI_Date          = $doc->createElement('gmd:CI_Date');
                $new_date             = $doc->createElement('gmd:date');
                $new_DateTime         = $doc->createElement('gco:DateTime');
                $new_dateType         = $doc->createElement('gmd:dateType');
                $new_CI_DateTypeCode  = $doc->createElement('gmd:CI_DateTypeCode');

                $titleNextSibling = null;
                $alernateTitles = $CI_Citation->getElementsByTagName('alternateTitle');
                if($alernateTitles && $alernateTitles->length > 0) {
                    $titleNextSibling = $alernateTitles->item($alernateTitles->length-1)->nextSibling;
                } else {
                    $titles = $CI_Citation->getElementsByTagName('title');
                    if($titles && $titles->length > 0) {
                        $titleNextSibling = $titles->item($titles->length-1)->nextSibling;
                    }
                }

                if($titleNextSibling) {
                    $CI_Citation->insertBefore($new_dates, $titleNextSibling);
                } else {
                    $CI_Citation->appendChild($new_dates);
                }
    
                $new_dates->appendChild($new_CI_Date);
                $new_CI_Date->appendChild($new_date);
                $new_date->appendChild($new_DateTime);
                $new_DateTime->nodeValue = $currentDateTime;
                $new_CI_Date->appendChild($new_dateType);
                $new_dateType->appendChild($new_CI_DateTypeCode);
                $new_CI_DateTypeCode->setAttribute("codeList", $codeList);
                $new_CI_DateTypeCode->setAttribute("codeListValue", "revision");
            }

            // met à jour la date de mise à jour de la métadonnée
            $dateStamp = $doc->getElementsByTagName('dateStamp')->item(0);
            $DateTime = $dateStamp->getElementsByTagName('DateTime')->item(0);
            $DateTime->nodeValue = $currentDateTime;

            $newData = $doc->saveXML();

            $newData = str_replace($entete, "", $newData);

        }

        return $newData;

    }

}

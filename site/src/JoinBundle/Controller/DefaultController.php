<?php

namespace ProdigeCatalogue\JoinBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;
use Prodige\ProdigeBundle\Common\Util;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;

//use Prodige\ProdigeBundle\Common;
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;

class DefaultController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/join/{token}", name="catalogue_join", options={"expose"=true})
     */
    public function indexAction(Request $request, $token=null) {
       
        if($token == null) {
            return $this->render('JoinBundle/Default/error_communication.html.twig');
        }

        $jsonParams = Util::getDecodeParam($token);
        $params = json_decode($jsonParams, true);

        if($params == null) {
            return $this->render('JoinBundle/Default/error_communication.html.twig');
        }
        $parameterBag = new ParameterBag();
        $parameterBag->replace(array_merge($params, $request->query->all(), $request->request->all()));
        
        // Getting back params
        $url_back   = $parameterBag->get("url_back", null);
        $pk_view    = $parameterBag->get("pk_view", null);
        $view_title = $parameterBag->get("view_title", null);
        $view_name  = $parameterBag->get("view_name", null);
        $view_type  = $parameterBag->get("view_type", null);
        
         //building external services urls
        $urlServices = array(
            "URL_layerNamesService" => $this->generateUrl('catalogue_join_services_AdministrationCarto_getCouche', array("SRV_CARTO"=> urlencode(str_replace("https://", "", CARMEN_URL_SERVER_FRONT)), "coucheType"=>1))/*"http://".$PRO_SITE_URL."/PRRA/Administration/CommunicationTranservale/AdministrationCarto_getCouche.php?SRV_CARTO=". str_replace("http://", "", $CARMEN_URL_SERVER_FRONT)."&coucheType=1"*/,
            "URL_tableNamesService" => $this->generateUrl('catalogue_join_services_AdministrationCarto_getCouche', array("SRV_CARTO"=> urlencode(str_replace("https://", "", CARMEN_URL_SERVER_FRONT)), "coucheType"=>-3))/*"http://".$PRO_SITE_URL."/PRRA/Administration/CommunicationTranservale/AdministrationCarto_getCouche.php?SRV_CARTO=". str_replace("http://", "", $CARMEN_URL_SERVER_FRONT)."&coucheType=-3"*/,
            "URL_viewManageService" => $this->generateUrl('catalogue_join_view_services')/*$CARMEN_URL_SERVER_BACK."/HTML_JOIN/gui/viewServices.php?"*/,
            "URL_updateMetadataDateService" => $this->generateUrl('catalogue_join_services_setMetadata_date_validity')/*"http://".$PRO_SITE_URL."/PRRA/Services/setMetadataDateValidity.php"*/,
            "URL_updateMetadataCatalogueService" => $this->generateUrl('prodige_services_featurecatalogue')/*"http://".$PRO_SITE_URL."/PRRA/Services/setMetadataCatalogue.php"*/,
            "URL_getLinkedMapService" => $this->generateUrl('catalogue_join_services_layer_get_maps', array("coucheType"=>-4))/*$CARMEN_URL_SERVER_BACK."/PRRA/Administration/Administration/Layers/LayerGetMaps.php?coucheType=-4"*/,
            "URL_updateLinkedMapService" => $this->generateUrl('catalogue_geosource_dellayersfrommap', array("type"=>-4, "couche" => $view_name )),/*$CARMEN_URL_SERVER_BACK."/PRRA/Synchronisation_couches.php?type=-4"*/
            "URL_deleteViewService" => $this->generateUrl('catalogue_join_services_synchronisation_couches')/*$CARMEN_URL_SERVER_BACK."/PRRA/Synchronisation_couches.php?type=-4"*/
        );
        
        $urlServices["callbacks"] = $parameterBag->get("callbacks", null);
        $urlServices["closeCallback"] = $parameterBag->get("closeCallback", null);
        $urlServices["closeCallbackUrl"] = $parameterBag->get("closeCallbackUrl", null);
        
        $urlServices["localLayerStoreConfigData"] = $parameterBag->get("layerStore", null);
        $urlServices["localTableStoreConfigData"] = $parameterBag->get("tableStore", null);
        
        $keys = array_keys($urlServices, null, true);
        foreach ($keys as $key){unset($urlServices[$key]);}

        $theme_id_for_ext = isset($params["theme_id_for_ext"]) ? intval($params["theme_id_for_ext"]) : null;

        if($url_back == null || $pk_view == null) {
            return $this->render('JoinBundle/Default/error_communication.html.twig');
        }

        // check if the view already exists
        
        $factory = ViewFactory::getInstance($this->getProdigeConnection());
        $c_view = $factory->loadCompositeView($pk_view);
        $createMode = ($c_view == null);
        
        $viewDef = array();
        if($createMode) {
            $viewDef = array(
                "viewName" => $view_name,
                "viewId" => $pk_view,
                "isGeoView" => $view_type==ViewFactory::$VIEW_TYPE_LAYER,
                "initialViewDesc" => array(),
                "groupingsDesc" => array()
            );
        }

        $viewDefJSON = json_encode(array_merge($viewDef, (!$createMode ? $c_view->getDesc() : array())));
        
        global $PRODIGE_EXT_THEME_COLOR;
        $ext_css = ($theme_id_for_ext != null) && ($PRODIGE_EXT_THEME_COLOR[$theme_id_for_ext] != "")
        ? $PRODIGE_EXT_THEME_COLOR[$theme_id_for_ext]["css_file"]
        : "/Scripts/ext3.0/resources/css/ext-all.css";

        return $this->render('JoinBundle/Default/index.html.twig', array('createMode'=>($createMode) ? 'true' : 'false',
                                                                        'viewDef'=>$viewDefJSON,
                                                                        'viewTitle'=>$view_title,
                                                                        'viewName'=>$view_name,
                                                                        'viewId'=>$pk_view,
                                                                        'isGeoView'=>($view_type==ViewFactory::$VIEW_TYPE_LAYER ? 'true' : 'false'),
                                                                        'backUrl'=>$url_back, //TODO hismail
                                                                        'css_file'=>$ext_css,
                                                                        'urlServices'=>json_encode($urlServices))
            );
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/temporary/join", name="catalogue_join_temporary", options={"expose"=true})
     */
    public function temporaryJoinAction(Request $request) {
        $layer_id = $request->get("layer_id", null);
        $layername = $request->get("layername", null);
        
        $layerStore = array($request->get("layers", array()));
        $table = $request->get("table", array());
        $jointables = $request->get("jointables", array());
//         $layerStore = array();
//         foreach ($layers[0] as $tableName){
//             $layerStore[] = array($tableName, "[".$tableName."] ".$layers[1]);
//         }
        $tableStore = array();
        foreach ($jointables as $join){
            foreach ($join[0] as $tableName){
                $tableStore[] = array($tableName, "[".$tableName."] ".$join[1]);
            }
        }
    
        $factory = ViewFactory::getInstance($this->getProdigeConnection());
        $pk_view = null;
        $view_name = null;
        $factory->getUniqueIdsForTemporaryView($table, $pk_view, $view_name);
        $view_name = $factory->createCompositeView($pk_view, $view_name, $table, true);
        
        
        return new JsonResponse(array(
            "properties" => array(
                "pk_view" => $pk_view,
                "view_name" => $view_name->getName(),
                "view_title" => "Jointure avec la donnée \"".$layername."\"",
                "view_type" => ViewFactory::$VIEW_TYPE_LAYER,
            ),
            "queryparams" => array(
                "layerStore" => (empty($layerStore) ? null : $layerStore),
                "tableStore" => (empty($tableStore) ? null : $tableStore),
            )
        ));
        
    }

}

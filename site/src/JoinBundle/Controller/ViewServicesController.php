<?php

namespace ProdigeCatalogue\JoinBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;
use Prodige\ProdigeBundle\Common\DBManager\CompositeView;
use Prodige\ProdigeBundle\Controller\BaseController;

//use Prodige\ProdigeBundle\Controller\User;
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use ProdigeCatalogue\JoinBundle\Common\MetadataDesc;
//use Prodige\ProdigeBundle\Common;
//use Prodige\ProdigeBundle\Common\Util;


class ViewServicesController extends BaseController {

    protected static $ProdigePgTypes2Str = array ( 
        "int4" => "Numérique",
        "int2" => "Numérique",
        "int8" => "Numérique",
        "float8" => "Real",
        "float4" => "Real",
        "numeric" => "Numérique",
        "varchar" => "Chaine de caractères",
        "bpchar" => "Chaine de caractères",
        "text" => "Chaine de caractères",
        "date" => "Date" 
    );
    
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/join/services/view_services", name="catalogue_join_view_services", options={"expose"=true})
     */
    public function viewServicesAction(Request $request) {

        //require_once("./path.php");
        //require_once("../lib/util.php");

        //require_once("../lib/viewFactory.class.php");

        ////////////////////////////////////////////////////////////////////////////////
        // Retrieving parameters
        ////////////////////////////////////////////////////////////////////////////////

        //$serviceName = http_params("serviceName", REQ_GET_POST, null);
        $serviceName = $request->get("serviceName", null);
        //$parameters = http_params("parameters", REQ_GET_POST, null, true);
        $parameters = json_decode($request->get("parameters", null), true);
        //$callback = http_params("callback", REQ_GET_POST, null);
        $callback = $request->get("callback", null);
        //$checkLinkedViews = http_params("checkLinkedViews", REQ_GET_POST, true);
        $checkLinkedViews = $request->get("checkLinkedViews", null);


        // gestion des cas d'erreur
        //Util::set_error_handler("handle_error_ajax_ext", E_ALL | E_STRICT);

        if($serviceName == null) {
            throw new \Exception("Missing parameter 'serviceName' in viewServices call.");
        }
        /////////////////////////////////////////////////////////////////////////////////
        // Main part
        /////////////////////////////////////////////////////////////////////////////////

        $PRODIGE = $this->getProdigeConnection();
        !$PRODIGE->isTransactionActive() && $PRODIGE->beginTransaction();
        $factory = ViewFactory::getInstance($PRODIGE);
         

        $successMview = true;
        $msgMview = "";
        $successMfield = true;
        $msgMfield = "";
        $res_json = array("success" => false);

        try {
            switch($serviceName) {
                case "createCompositeView" :
                    if( empty($parameters)
                     || empty($parameters["viewId"])
                     || empty($parameters["viewName"])
                     || empty($parameters["layerName"])
                    ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.{available=".implode(", ", array_keys(array_diff($parameters, array("", null))))."}");
                    }
                    $isGeoView = (empty($parameters["isGeoView"]) ? true : ($parameters["isGeoView"] == 0 ? false : true));

                    $c_view = $factory->createCompositeView(
                        intval($parameters["viewId"]),
                        $parameters["viewName"],
                        $parameters["layerName"],
                        false,
                        $isGeoView,
                        true
                    );
                    $c_view->storeInDb();
                    $msg = "La vue " . $c_view->getName() . " a été bien créée.";
                    
                   
                    if(isset($parameters["urlUpdateMetadataView"])) {
                        $this->updateMetadataView($parameters["urlUpdateMetadataView"], $c_view, $successMview, $msgMview);
                    }
                    if(isset($parameters["urlUpdateMetadataField"])) {
                        $this->updateMetadataFields($parameters["urlUpdateMetadataField"], $c_view, $successMfield, $msgMfield);
                    }
                    $res = array(
                        'success' => true, //$successMview && $successMfield,
                        'msg' => $msg . $msgMview . $msgMfield 
                    );
                    
                    $res_json = json_encode($res);
                break;
                case "removeCompositeView" :
                    //obedel test
                    if( empty($parameters) || empty($parameters["viewId"]) ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }

                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    if($checkLinkedViews){
                        $this->stopIfLinkedViews($c_view);
                    }

                    $viewName = $c_view->getName();
                    $factory->removeCompositeView(intval($parameters["viewId"]));
                    $c_view->removeFromDb();

                    $msg = "La vue " . $viewName . " a été bien supprimée.";
                    if(isset($parameters["urlUpdateMetadataView"])){
                        $this->updateMetadataView($parameters["urlUpdateMetadataView"], $c_view, $successMview, $msgMview);
                    }
                    if(isset($parameters["urlUpdateMetadataField"])){
                        $this->updateMetadataFields($parameters["urlUpdateMetadataField"], $c_view, $successMfield, $msgMfield);
                    }
                    $res = array(
                        'success' => true, //$successMview && $successMfield,
                        'msg' => $msg . $msgMview . $msgMfield );
                    $res_json = json_encode($res);
                    
                break;
                case "createJoin" :
                    if( empty($parameters)
                     || empty($parameters["viewId"])
                     || empty($parameters["tableName"])
                     || empty($parameters["viewField"])
                     || empty($parameters["tableField"])
                     || !isset($parameters["restriction"])
                     || empty($parameters["joinPos"])
                    ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }

                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    if ($checkLinkedViews) {
                        $this->stopIfLinkedViews($c_view);
                    }

                    $view = $c_view->getCurrentView();
                    $joinPos = $parameters["joinPos"];
                    if ($joinPos==$view->joinCount) {
                        //echo "removing join in pos " . $joinPos;
                        $view->removeJoin($joinPos);
                    }
                    else {
                        if($joinPos<$view->joinCount)
                            throw new \Exception("Il est impossible de supprimer ou de mettre à jour cette jointure. Seule la dernière jointure est modifiable.");
                    }

                    $joinCriteria = ViewFactory::buildJoinCriteria($parameters["viewField"], $parameters["tableField"], $joinPos);

                    $joinType = $parameters["restriction"] ? ViewFactory::$T_JOIN_RESTRICTED : ViewFactory::$T_JOIN_UNRESTRICTED;

                    $view->createJoin($parameters["tableName"], $joinType, $joinCriteria);
                    $view->removeFromDb();
                    $view->storeInDb();
                    $c_view->removeFromDb();
                    $c_view->storeInDb();
                    $msg = "La jointure " . $c_view->getName() . " - " . $parameters["tableName"] . " a bien été mise à jour.";
                    if(isset($parameters["urlUpdateMetadataView"])){
                        $this->updateMetadataView($parameters["urlUpdateMetadataView"], $c_view, $successMview, $msgMview);
                    }
                    if(isset($parameters["urlUpdateMetadataField"])){
                        $this->updateMetadataFields($parameters["urlUpdateMetadataField"], $c_view, $successMfield, $msgMfield);
                    }
                    $res = array(
                        'success' => true, //$successMview && $successMfield,
                        'msg' => $msg . $msgMview . $msgMfield 
                    );
                    $res_json = json_encode($res);
                break;
                case "deleteJoin" :
                    //obedel test
                    if( empty($parameters) 
                     || empty($parameters["viewId"]) 
                     || empty($parameters["joinPos"])
                    ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }

                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    if($checkLinkedViews) {
                        $this->stopIfLinkedViews($c_view);
                    }

                    $view = $c_view->getCurrentView();
                    $joinPos = $parameters["joinPos"];
                    if($joinPos==$view->joinCount) {
                        $ccFieldsUsingJoin = $view->getComputedFieldsUsingJoin($joinPos);

                        if(!empty($ccFieldsUsingJoin)) {
                            $msg = "Il est impossible de supprimer ou de mettre à jour cette jointure car des champs joints entrent dans la définition des champs calculés '" .
                                implode(',', $ccFieldsUsingJoin) .  "'. Veuillez supprimer ces champs avant de supprimer la vue." . $joinPos;
                                throw new \Exception($msg);
                        }
                        $view->removeJoin($joinPos);
                    }
                    else {
                        if($joinPos<$view->joinCount)
                            throw new \Exception("Il est impossible de supprimer ou de mettre à jour cette jointure. Seule la dernière jointure est modifiable.");
                    }
                    $view->removeFromDb();
                    $view->storeInDb();
                    $c_view->removeFromDb();
                    $c_view->storeInDb();
                    $msg = "La jointure a bien été supprimée.";
                    if(isset($parameters["urlUpdateMetadataView"])) {
                        $this->updateMetadataView($parameters["urlUpdateMetadataView"], $c_view, $successMview, $msgMview);
                    }
                    if(isset($parameters["urlUpdateMetadataField"])) {
                        $this->updateMetadataFields($parameters["urlUpdateMetadataField"], $c_view, $successMfield, $msgMfield);
                    }
                    $res = array(
                        'success' => true,//$successMview && $successMfield,
                        'msg' => $msg . $msgMview . $msgMfield 
                    );
                    $res_json = json_encode($res);
                break;

                case "getFields" :
                    if( empty($parameters) || empty($parameters["viewId"]) ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }

                    $view = $factory->loadView(intval($parameters["viewId"]));

                    $fields = $view->getAllFields(true, false, true, false, null, null, true);

                    $fieldsDesc = array();
                    foreach($fields as $key=>$val) {
                        array_push($fieldsDesc, $key);
                    }
                    $res = array("success"=>true, "fields"=>$fieldsDesc);
                    $res_json = json_encode($res);
                break;
                case "getViewFields" :
                    if( empty($parameters) || empty($parameters["viewId"]) ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }

                    $joinFilter = isset($parameters["joinFilter"]) ? intval($parameters["joinFilter"]) - 1 : null;

                    $fieldFilter = isset($parameters["fieldFilter"]) ? $parameters["fieldFilter"] : null;
                    $onlyVisible = isset($parameters["onlyVisible"]) ? $parameters["onlyVisible"] : false;
                    $view = $factory->loadView(intval($parameters["viewId"]));
                    $fields = $view->getAllFields($onlyVisible, false, true, false, $joinFilter, $fieldFilter);

                    $fieldsDesc = array();
                    foreach($fields as $key=>$val) {
                        array_push($fieldsDesc, array("fieldname" => $key, "type" => $val));
                    }
                    $res = array("success"=>true, "data"=>$fieldsDesc);
                    $res_json = json_encode($res);
                break;
                case "getTableFields" :
                    if( empty($parameters) || empty($parameters["tablename"]) ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $fields = $factory->getFields($parameters["tablename"]);
                    $fieldsDesc = array();
                    foreach($fields as $key=>$val) {
                        array_push($fieldsDesc, array("fieldname" => $key, "type" => $val));
                    }
                    $res = array("success"=>true, "data"=>$fieldsDesc);
                    $res_json = json_encode($res);
                break;

                case "getData" :
                    if( empty($parameters) || empty($parameters["viewId"]) ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }

                    $view = $factory->loadView(intval($parameters["viewId"]));

                    $dataLimit = empty($parameters["limit"]) ? null : intval($parameters["limit"]);
                    $data = $view->getData(false, $dataLimit, true);
                    $res = array("success"=>true, "data"=>$data);
                    $res_json = json_encode($res);
                break;
                case "getJoins" :
                    if( empty($parameters) || empty($parameters["viewId"]) ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $view = $factory->loadView(intval($parameters["viewId"]));
                    $def = array();
                    for ($i=0; $i<$view->joinCount; $i++) {
                        $join = $view->joins[$i];
                        $def[] = $join->getDesc();
                    }
                    $res = array("success"=>true, "data"=>$def);
                    $res_json = json_encode($res);
                break;
                case "getComputedFields" :
                    if(empty($parameters) ||
                    empty($parameters["viewId"])) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $view = $factory->loadView(intval($parameters["viewId"]));
                    $def = array();
                    foreach($view->computedFields as $fieldname=>$ccField) {
                        $def[] = $ccField->getDesc();
                    }
                    $res = array("success"=>true, "data"=>$def);
                    $res_json = json_encode($res);
                break;
                case "createComputedField" :
                    //obedel test
                    if( empty($parameters)
                     || empty($parameters["viewId"])
                     || empty($parameters["fieldname"]) 
                     || empty($parameters["expression"]) 
                     || empty($parameters["fieldtype"])
                    ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call");
                    }
                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    if($checkLinkedViews) {
                        $this->stopIfLinkedViews($c_view);
                    }

                    $view = $c_view->getCurrentView();
                    $fieldname = $parameters["fieldname"];
                    $expression = $parameters["expression"];
                    $fieldtype = $parameters["fieldtype"];
                   

                    try{
                    $ccField = isset($view->computedFields[$fieldname]) ? $view->computedFields[$fieldname] : null;
                    if($ccField==null)
                        $ccField = $view->createComputedField($fieldname, $expression, $fieldtype);
                    else
                        $ccField->updateExpression($expression);
                    $view->removeFromDb();
                    $view->storeInDb();
                    $c_view->removeFromDb();
                    $c_view->storeInDb();
                    $msg = "Le champ calculé " . $fieldname . " a bien été mis à jour.";
                    if(isset($parameters["urlUpdateMetadataView"])) {
                        $this->updateMetadataView($parameters["urlUpdateMetadataView"], $c_view, $successMview, $msgMview);
                    }
                    if(isset($parameters["urlUpdateMetadataField"])) {
                        $this->updateMetadataFields($parameters["urlUpdateMetadataField"], $c_view, $successMfield, $msgMfield);
                    }
                    $res = array(
                        'success' => true, //$successMview && $successMfield,
                        'msg' => $msg . $msgMview . $msgMfield 
                    );

}catch (\Exception $exception) {
                        //error_log("impossible d'ouvrir ".$Directory."/".$Entry);
                        //$this->getLogger()->debug("impossible d'ouvrir ".$Directory."/".$Entry);
                        
                        $res = array(
                            'success' => false,
                            'stack' => $exception->getTraceAsString(),
                                'context' => sprintf('%s[%d]', $exception->getFile(), $exception->getLine()),
                            'msg' => "erreur de syntaxe, veuillez modifier l'expression", 
                            'errmsg' => "erreur de syntaxe, veuillez modifier l'expression" 
                        );
                    }
                    $res_json = json_encode($res);
                break;
                case "deleteComputedField" :
                    if( empty($parameters)
                     || empty($parameters["viewId"]) 
                     || empty($parameters["fieldname"])
                    ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call");
                    }
                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    if($checkLinkedViews) {
                        $this->stopIfLinkedViews($c_view);
                    }

                    $view = $c_view->getCurrentView();
                    $fieldname = $parameters["fieldname"];

                    // checking that the field is not implied in the definition of another computed field...
                    $fields = $view->getComputedFieldsUsingComputedField($fieldname);
                    if (!empty($fields)) {
                        $msg = 	"Il est impossible de supprimer ce champ car il entre dans la définition des champs calculés '" .
                            implode(',', $fields) .  "'. Veuillez supprimer d'abord ces champs dépendant.";
                        throw new \Exception($msg);
                    }
                    // TODO check that the field is not implied in a join criteria or in the definition of another computed field...

                    $view->removeComputedField($fieldname);

                    $view->removeFromDb();
                    $view->storeInDb();
                    $c_view->removeFromDb();
                    $c_view->storeInDb();
                    $msg = "Le champ calculé " . $fieldname . " a bien été supprimé";
                    if(isset($parameters["urlUpdateMetadataView"])) {
                        $this->updateMetadataView($parameters["urlUpdateMetadataView"], $c_view, $successMview, $msgMview);
                    }
                    if(isset($parameters["urlUpdateMetadataField"])) {
                        $this->updateMetadataFields($parameters["urlUpdateMetadataField"], $c_view, $successMfield, $msgMfield);
                    }
                    $res = array(
                        'success' => true, //$successMview && $successMfield,
                        'msg' => $msg . $msgMview . $msgMfield 
                    );
                    $res_json = json_encode($res);
                break;
                case "updateFilter" :
                    if( empty($parameters) || empty($parameters["viewId"]) ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call");
                    }
                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    if($checkLinkedViews) {
                        $this->stopIfLinkedViews($c_view);
                    }

                    $view = $c_view->getCurrentview();

                    $expression = $parameters["expression"];

                    // checking the expression
                    $errMsg = "";
                    $checked = $view->checkFilterExpression($expression, $errMsg);
                    if(!$checked) {
                        $msg = 	"Il y a une erreur dans l'expression \n" . $errMsg;
                        throw new \Exception($msg);
                    }
                    $view->setFilterExpression($expression);
                    $view->removeFromDb();
                    $view->storeInDb();
                    $c_view->removeFromDb();
                    $c_view->storeInDb();
                    $msg = "Le filtre de la vue a bien été mis à jour";
                    if(isset($parameters["urlUpdateMetadataView"])){
                        $this->updateMetadataView($request->getSchemeAndHttpHost().$parameters["urlUpdateMetadataView"], $c_view, $successMview, $msgMview);
                    }
                    if(isset($parameters["urlUpdateMetadataField"])){
                        $this->updateMetadataFields($request->getSchemeAndHttpHost().$parameters["urlUpdateMetadataField"], $c_view, $successMfield, $msgMfield);
                    }
                    $res = array(
                        'success' => true,
                        'msg' => $msg . $msgMview . $msgMfield 
                    );
                    $res_json = json_encode($res);
                break;
                case "checkComputedFieldWithData" :
                    if( empty($parameters)
                     || empty($parameters["viewId"]) 
                     || empty($parameters["fieldname"])
                    ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call");
                    }
                    $success = true;

                    $view = $factory->loadView(intval($parameters["viewId"]));
                    $fieldname = $parameters["fieldname"];
                    $ccField = isset($view->computedFields[$fieldname]) ? $view->computedFields[$fieldname] : null;
                    if(isset($view->computedFields[$fieldname])) {
                        $msg = "Le paramétrage du champ calculé a bien été pris en compte.";
                        $errMsg = "Le champ calculé " . $fieldname . " n'est pas correctement défini.";
                        $success = $view->checkComputedFieldWithData($fieldname, $errMsg);
                        $msg = (!$success) ? $errMsg : $msg;
                    }
                    else {
                        $msg = "Le champ calculé " . $fieldname . " n'est pas correctement défini.";
                    }
                    $res = array(
                        'success' => $success,
                        'msg' => $msg);
                    $res_json = json_encode($res);
                break;
                case "getFieldVisibility" :
                    //print_r($parameters);
                    if( empty($parameters)
                     || empty($parameters["viewId"]) 
                     || !isset($parameters["src"])
                    ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    $view = $c_view->getCurrentView();
                    $def = '[';
                    $src = $view;
                    $index= intval($parameters["src"]);
                    if($index>0) {
                        if($index<=$view->joinCount)
                            $src = $view->joins[$index-1];
                        else
                            throw new \Exception("Wrong value for 'src' parameter in viewServices call. Maybe you wrongly reference a join.");
                    }
                    $field_names = $src->getFields(false, true, false, false);
                    $field_alias = $src->getFields(false, true, true, false);
                    $field_visible = $src->getFields(true, true, false, false);

                    $def = array();
                    for($i=0;$i<count($field_names); $i++) {
                        $visible = in_array($field_names[$i], $field_visible);
                        $def[] = array(
                            "name"  => $field_names[$i],
                        		"alias" => $field_alias[$i],
                        		"visible" => (bool)$visible
                        );
                    }

                    $res = array("success"=>true, "data"=>$def);
                    $res_json = json_encode($res);
                break;
                case "setFieldVisibility" :
                    if( empty($parameters)
                     || empty($parameters["viewId"]) 
                     || !isset($parameters["visibleFields"]) 
                     || !isset($parameters["src"])
                    ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    if($checkLinkedViews) {
                        $this->stopIfLinkedViews($c_view);
                    }

                    $view = $c_view->getCurrentView();
                    $src = $view;
                    $index= intval($parameters["src"]);
                    if($index>0) {
                        if($index<=$view->joinCount)
                            $src = $view->joins[$index-1];
                        else
                            throw new \Exception("Wrong value for 'src' parameter in viewServices call. Maybe you wrongly reference a join.");
                    }
                    $src->setVisibleFields($parameters["visibleFields"]);

                    $view->removeFromDb();
                    $view->storeInDb();
                    $c_view->removeFromDb();
                    $c_view->storeInDb();
                    $msg = "Le paramétrage des champs visibles a bien été pris en compte.";

                    if(isset($parameters["urlUpdateMetadataView"])){
                        $this->updateMetadataView($parameters["urlUpdateMetadataView"], $c_view, $successMview, $msgMview);
                    }
                    if(isset($parameters["urlUpdateMetadataField"])){
                        $this->updateMetadataFields($parameters["urlUpdateMetadataField"], $c_view, $successMfield, $msgMfield);
                    }
                    $res = array(
                        'success' => true, //$successMview && $successMfield,
                        'msg' => $msg . $msgMview . $msgMfield 
                    );
                    $res_json = json_encode($res);
                break;
                case "getFieldsExpOperators" :
                    $res = array("success"=>true, "data"=>ViewFactory::getExpressionOperators());
                    $res_json = json_encode($res);
                break;
                case "checkFieldExpression" :
                    if( empty($parameters)
                     || empty($parameters["viewId"])
                     || !isset($parameters["expression"])
                    ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $view = $factory->loadView(intval($parameters["viewId"]));

                    if($view->checkComputedFieldExpression($parameters["expression"], $errMsg)) {
                        $res = array("success"=>true, "msg" => "La syntaxe de l'expression est correcte.");
                    }
                    else {
                        $res = array("success"=>false, "errmsg" => $errMsg);
                    }
                    $res_json = json_encode($res);
                break;
                case "getViewsFromTablenames" :
                    if(empty($parameters) ||
                    !isset($parameters["tablenames"])) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $tabnameToViewIds = $factory->getViewsFromTablenames($parameters["tablenames"]);
                    $res = array("success"=>true, "data"=>$tabnameToViewIds);
                    $res_json = json_encode($res);
                break;

                case "getViewsFromLayernames" :
                    if(empty($parameters) ||
                    !isset($parameters["layernames"])) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $tabnameToViewIds = $factory->getViewsFromLayernames($parameters["layernames"]);
                    $res = array("success"=>true, "data"=>$tabnameToViewIds);
                    $res_json = json_encode($res);
                break;
                case "getViewsFromViewnames" :
                    if(empty($parameters) ||
                    !isset($parameters["viewnames"])) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $viewnameToViewIds = $factory->getViewsFromViewnames($parameters["viewnames"]);
                    $res = array("success"=>true, "data"=>$viewnameToViewIds);
                    $res_json = json_encode($res);
                break;

                case "getGroupingDesc" :
                    if(empty($parameters) ||
                    empty($parameters["viewId"])) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }

                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    if($c_view->groupingCount == 0)
                        throw new \Exception("The view contains no grouping.");
                    $res = array("success"=>true, "data"=>$c_view->groupings[$c_view->groupingCount-1]->getDesc());
                    $res_json = json_encode($res);
                break;

                case "getAgregateFunctions" :
                    $res = array("success"=>true, "data"=>ViewFactory::getAgregateFunctions());
                    $res_json = json_encode($res);
                break;

                case "getSortDataFunctions" :
                    $res = array("success"=>true, "data"=>ViewFactory::getSortDataFunctions());
                    $res_json = json_encode($res);
                break;

                case "setSortParameters" :
                    if( empty($parameters)
                     || empty($parameters["viewId"]) 
                     || empty($parameters["sortingField"]) 
                     || empty($parameters["sortingFunction"])
                    ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    if($checkLinkedViews){
                        $this->stopIfLinkedViews($c_view);
                    }

                    $view = $c_view->getCurrentView();
                    $field= $parameters["sortingField"];
                    $method= $parameters["sortingFunction"];
                    $view->updateSorting($field, $method);

                    $view->removeFromDb();
                    $view->storeInDb();
                    $c_view->removeFromDb();
                    $c_view->storeInDb();

                    $msg = "L'ordre de tri de la vue a bien été mise a jour.";
                    if(isset($parameters["urlUpdateMetadataView"])){
                        $this->updateMetadataView($parameters["urlUpdateMetadataView"], $c_view, $successMview, $msgMview);
                    }
                    if(isset($parameters["urlUpdateMetadataField"])){
                        $this->updateMetadataFields($parameters["urlUpdateMetadataField"], $c_view, $successMfield, $msgMfield);
                    }
                    $res = array(
                        'success' => true,
                        'msg' => $msg . $msgMview . $msgMfield 
                    );
                    $res_json = json_encode($res);
                break;

                case "getCompositeViewDesc":
                    if( empty($parameters) || empty($parameters["viewId"]) ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    
                    $res = array("success"=>true, "data"=>$c_view->getDesc());
                    $res_json = json_encode($res);
                break;

                case "createGrouping" :
                    if( empty($parameters) 
                    || empty($parameters["viewId"]) 
                    || empty($parameters["groupingFields"]) 
                    || empty($parameters["agregateMapping"])
                    || empty($parameters["agregatePos"])
                    ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    if($checkLinkedViews){
                        $this->stopIfLinkedViews($c_view);
                    }

                    $agregatePos = intval($parameters["agregatePos"]);
                    if($agregatePos==$c_view->groupingCount) {
                        $c_view->removeGrouping($agregatePos);
                    }
                    else {
                        if($agregatePos<$c_view->groupingCount && $agregatePos>0)
                            throw new \Exception("Il est impossible de supprimer ou de mettre à jour cette agrégation. Seule la dernière agrégation est modifiable.");
                    }

                    $groupingFields = $parameters["groupingFields"];
                    $agregates = $parameters["agregateMapping"];

                    $c_view->createGrouping($groupingFields,$agregates);
                    $c_view->removeFromDb();
                    $c_view->storeInDb();

                    $msg = "La vue a bien été agrégée.";
                    if(isset($parameters["urlUpdateMetadataView"])){ 
                        $this->updateMetadataView($parameters["urlUpdateMetadataView"], $c_view, $successMview, $msgMview);
                    }
                    if(isset($parameters["urlUpdateMetadataField"])){
                        $this->updateMetadataFields($parameters["urlUpdateMetadataField"], $c_view, $successMfield, $msgMfield);
                    }
                    $res = array(
                        'success' => true,
                        'msg' => $msg . $msgMview . $msgMfield 
                    );
                    $res_json = json_encode($res);
                break;
                case "removeGrouping" :
                    //obedel test
                    if( empty($parameters) || empty($parameters["viewId"]) ) {
                        throw new \Exception("Missing parameter 'parameters' in viewServices call.");
                    }
                    $c_view = $factory->loadCompositeView(intval($parameters["viewId"]));
                    if($checkLinkedViews){
                        $this->stopIfLinkedViews($c_view);
                    }

                    $groupingPos = $c_view->groupingCount;
                    $c_view->removeGrouping($groupingPos);

                    $c_view->removeFromDb();
                    $c_view->storeInDb();

                    $msg = "L'agrégation a bien été supprimée.";
                    if(isset($parameters["urlUpdateMetadataView"])){
                        $this->updateMetadataView($parameters["urlUpdateMetadataView"], $c_view, $successMview, $msgMview);
                    }
                    if(isset($parameters["urlUpdateMetadataField"])){
                        $this->updateMetadataFields($parameters["urlUpdateMetadataField"], $c_view, $successMfield, $msgMfield);
                    }
                    $res = array(
                        'success' => true,
                        'msg' => $msg . $msgMview . $msgMfield );
                    $res_json = json_encode($res);
                break;

                case "" :
                break;
                default :
                    throw new \Exception("Service inconnu");
                break;
            }
            $PRODIGE->isTransactionActive() && $PRODIGE->commit();
        } catch (\Exception $exception){
            $PRODIGE->isTransactionActive() && $PRODIGE->rollBack();
            $res = array(
                'success' => true,
                'stack' => $exception->getTraceAsString(),
            		'context' => sprintf('%s[%d]', $exception->getFile(), $exception->getLine()),
                'msg' => $exception->getMessage(), 
                'errMsg' => $exception->getMessage() 
            );
            $res_json = json_encode($res);
            throw $exception;
        }
        
        $res_json = $callback===null ? $res_json : $callback . "(" . $res_json . ")";
        if ( is_bool($res_json) ) var_dump($res, $res_json, json_last_error(), json_encode($res), json_last_error_msg());
        return new Response($res_json);
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Misc Function for updating metadata associated with views
    ////////////////////////////////////////////////////////////////////////////////
    protected function updateMetadataView($url, CompositeView $c_view, &$success, &$msg) {
        //hismail - Prodige4
        /*$r = new HttpRequest($url, HttpRequest::METH_POST);
        $r->addQueryData(array("metadata_id" => $c_view->id));
        try {
            $r->send();
            if ($r->getResponseCode() == 200) {
                $json_res = $r->getResponseBody();
                $res = json_decode($json_res, true);
                if ($res) {
                    $success = $res["update_success"];
                    $msg = "<br>"  . ( $success 
                        ?  $res["msg"] 
                        :  "Les métadonnées de la vue n'ont pu être mises à jour : " . $res["msg"]);
                }
                else {
                    $success = false;
                    $msg = "<br> Les métadonnées de la vue n'ont pu être mises à jour.";
                }
            }
        } catch (\HttpException $ex) {
            throw new \Exception("Problème dans la communication avec le service de mise à jour des métadonnées : " . htmlentities($ex));
        }*/

        $option = $this->getUnsecureCurlOptions();

        $requestData = array("metadata_id" => $c_view->id);
        try {
            $res = $this->curl($url, 'POST', array(), $requestData, $option);
            if(isset($res["success"]) && $res["success"]) {
                $success = false;
                $msg = "<br> Les métadonnées de la vue n'ont pu être mises à jour.";
            }
            else {
                $success = true;
                $msg = "<br> Les métadonnées de la vue ont pu être mises à jour.";
            }
        }
        catch(\Exception $ex) {
            throw new \Exception("Problème dans la communication avec le service de mise à jour des métadonnées : " . htmlentities($ex));
        }
    }

    protected function updateMetadataFields($url, CompositeView $c_view, &$success, &$msg) {
        $view = $c_view->getCurrentView();
        $fields = $view->getAllFields(true, false, true, false, null, null);
        $fieldsDescStr = "";
        foreach($fields as $key=>$val) {
            $type = isset(self::$ProdigePgTypes2Str[$val]) ? self::$ProdigePgTypes2Str[$val] : "non défini";
            $fieldsDescStr .= $key . "|" . $type . "||";
        }
        $fieldsDescStr = strlen($fieldsDescStr)>0 ? substr($fieldsDescStr,0, strlen($fieldsDescStr)-2) : $fieldsDescStr;
        $fieldsDescStrBase64 = base64_encode($fieldsDescStr);

        //hismail - Prodige4
        /*$r = new \HttpRequest($url, \HttpRequest::METH_POST);
        $r->addQueryData(
            array("metadata_id" => $c_view->id,
                "mode" => "replace",
                "champs" => $fieldsDescStrBase64
            ));
        try {
            $r->send();
            if ($r->getResponseCode() == 200) {
                $json_res = $r->getResponseBody();
                $res = json_decode($json_res, true);
                if ($res) {
                    $success = $res["update_success"];
                    $msg = "<br>"  . ( $success 
                        ?  $res["msg"] 
                        :  "Les métadonnées des champs de la vue n'ont pu être mises à jour : " . $res["msg"]);
                }
                else {
                    $success = false;
                    $msg = "<br>Les métadonnées des champs de la vue n'ont pu être mises à jour.";
                }
            }
        } catch (\HttpException $ex) {
            throw new \Exception("Problème dans la communication avec le service de mise à jour des métadonnées : " . htmlentities($ex));
        }*/

        $option = $this->getUnsecureCurlOptions();

        $requestData = array("metadata_id" => $c_view->id,
                             "mode" => "replace",
                             "champs" => $fieldsDescStrBase64
        );
        try {
            $res = $this->curl($url, 'POST', array(), $requestData, $option);
            if(isset($res["success"]) && $res["success"]) {
                $success = false;
                $msg = "<br> Les métadonnées de la vue n'ont pu être mises à jour.";
            }
            else {
                $success = true;
                $msg = "<br> Les métadonnées de la vue ont pu être mises à jour.";
            }
        }
        catch (\Exception $ex) {
            throw new \Exception("Problème dans la communication avec le service de mise à jour des métadonnées : " . htmlentities($ex));
        }

    }

    // function that checks if the view $c_view is used in others views
    // and launched an erro if triggerError si set to true
    // else return the number of linkedViews
    protected function stopIfLinkedViews($c_view, $triggerError = true) {
        $viewIds = $c_view->factory->getViewsFromViewname($c_view->getName());
        $viewIds =  array_diff($viewIds, array(0 => $c_view->id));
        $count = count($viewIds);
        if($triggerError && $count > 0)
            throw new \Exception("La vue " . $c_view->getName() . " ne peut être modifiée car elle est utilisée dans au moins " . $count . " autre(s) vue(s) : " . implode(',', $viewIds) . "  " . $c_view->id);
        return $count;
    }

}

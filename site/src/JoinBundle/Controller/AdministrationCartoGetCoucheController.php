<?php

namespace ProdigeCatalogue\JoinBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\DAOProxy\DAO;


/**
 * Get layer list from postgresql catalogue database
 * @author Alkante
 * Prodige V4
 */

class AdministrationCartoGetCoucheController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/join/services/AdministrationCarto_getCouche/{SRV_CARTO}/{coucheType}/{domain}/{subdomain}", name="catalogue_join_services_AdministrationCarto_getCouche", options={"expose"=true})
     */
    public function AdministrationCarto_getCoucheAction(Request $request, $SRV_CARTO, $coucheType, $domain=null, $subdomain=null) {

        $callback = $request->get("callback");

        $user = User::GetUser();
        $userId = User::GetUserId();
       
        $m_srv = $SRV_CARTO;
        $returnJSON = $this->getLayerList($user, $m_srv, $coucheType, $domain, $subdomain);
        echo $callback."(".$returnJSON.")";
        exit;
    }

    /**
     * #brief Returns a json object containing the list of the layers availaible in the catalog
     * The pattern is :
     * {"name" : "name", "type" : "raster or vector (O or 1)",
     *  "table_name" : "postgis_table or raster file", "id" : "metadata id", "text" : "metadata title and text for extjs tree",
     *  "leaf" : true or false, "cls" : style, "children" : null}
     *
     * @param $dbconn
     * @return json text
     */
    protected function getLayerList($user, $m_srv, $coucheType, $domain, $subdomain){
        //  $start = time();
        //global $coucheType, $m_srv, $domain, $subdomain;

        $errMessage = null;
        $tabLayers = array();

        //recup des couche carto du serveur
        if(!is_null($m_srv)) {
            if($m_srv != "") {
                
                $m_sSql = "SELECT distinct fmeta_id
                       , couchd_nom
                       , couchd_type_stockage
                       , couchd_emplacement_stockage
                       , metadata.uuid
                       , xpath
                         ('//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text,
                         ('<?xml version=\"1.0\" encoding=\"utf-8\"?>'::text || data)::xml,
                         ARRAY[ARRAY['gmd'::text, 'http://www.isotc211.org/2005/gmd'::text],
                         ARRAY['gco'::text, 'http://www.isotc211.org/2005/gco'::text]])::text AS \"metadatatitle\"
                         
                       from couche_sdom 
                       inner join public.metadata on (couche_sdom.fmeta_id::bigint = metadata.id) 

                       where couchd_type_stockage in (" . $coucheType . ") and couchd_visualisable=1 ";
                
                if ($domain != "" && $subdomain != "") {
                    if (strpos($domain, "rubric_") === false) {
                        $subdomain = ($subdomain == - 1 ? "(SELECT pk_sous_domaine FROM sous_domaine" . ($domain == - 1 ? "" : " WHERE ssdom_fk_domaine = " . $domain) . ")" : $subdomain);
                    } else {
                        $rubric = str_replace("rubric_", "", $domain);
                        $subdomain = "(SELECT pk_sous_domaine FROM sous_domaine WHERE ssdom_fk_domaine IN (SELECT pk_domaine FROM domaine WHERE dom_rubric=" . $rubric . "))";
                    }
                    $m_sSql .=                     // "SELECT cd.couchd_nom as coucheNom, cd.couchd_emplacement_stockage as coucheTable, cd.couchd_type_stockage as coucheType, fm.fmeta_id as metadataId
                    " and pk_sous_domaine IN (" . $subdomain . ")";
                }
                $m_sSql .= " order by metadatatitle, couchd_nom";
                
                
                
                
                //$dao = new DAO();
                $conn = $this->getCatalogueConnection('catalogue');
                $dao = new DAO($conn, 'catalogue');
                if($dao) {
                    $rs = $dao->BuildResultSet($m_sSql);
                    if ($rs->GetNbRows() > 0) {
                        $i = 0;
                        for($rs->First();!$rs->EOF();$rs->Next()){
                            //echo $i." || ".((time()-$start))."<br>";
                            //vérification des droits sur un des domaines de la donnée
                            $bAllow = false;
    
                            //$query = "SELECT DOM_NOM, SSDOM_NOM, PK_COUCHE_DONNEES, COUCHD_RESTRICTION FROM COUCHE_SDOM WHERE FMETA_ID ='".$rs->Read(0)."'";
                            $query = "SELECT DOM_NOM, SSDOM_NOM, PK_COUCHE_DONNEES, COUCHD_RESTRICTION FROM COUCHE_SDOM WHERE FMETA_ID =?";
                            $rs2 = $dao->BuildResultSet($query, array($rs->Read(0)));
                            for($rs2->First(); !$rs2->EOF(); $rs2->Next()) {
                                $domaine = $rs2->read(0);
                                $sous_domaine = $rs2->read(1);
                                $objetCouche = $rs2->read(2);
                                $restriction = $rs2->read(3);
                                
                                //vérification de l'autorisation sur le domaine et sur l'objet
                                $bAllow = $user->HasTraitement('NAVIGATION', html_entity_decode($domaine, ENT_QUOTES, 'UTF-8'), html_entity_decode($sous_domaine, ENT_QUOTES, 'UTF-8'), PRO_TRT_TYPE_CONSULTATION, $objetCouche, PRO_OBJET_TYPE_COUCHE);
                                if($bAllow) {
                                    break;
                                }
                            }
                            //vérification des restrictions de compétences
                            $bAllow = $bAllow && $user->HasTraitementCompetence('NAVIGATION', $objetCouche);
                            if($bAllow) {
                                $tabLayers[$i]["text"] = stripslashes((htmlspecialchars_decode($rs->Read(1), ENT_QUOTES)));//text for extjs tree
                                //replace ; by nothing (because used to build the param tab when adding the layer)
                                $tabLayers[$i]["text"] = str_replace(";", "",$tabLayers[$i]["text"]);
                                if($coucheType == 0) { //raster
                                    $const_typeRasterTuile  = ".shp";
                                    $const_typeRasterUnique = ".ecw|.tif";
                                    //process filename $rs->Read(3)
                                    $file = $rs->Read(3);
                                    $typeGeom = strtolower(substr($file, strrpos($file, ".")));
                                    $typerasterTuile = explode("|", strtolower($const_typeRasterTuile));
                                    $typerasterUnique = explode("|", strtolower($const_typeRasterUnique));
                                    if(in_array($typeGeom, $typerasterUnique)) {
                                        $tabLayers[$i]["type"]=0; //"TYPE_UNIQUE";
                                    }
                                    else if(in_array($typeGeom, $typerasterTuile)) {
                                        $tabLayers[$i]["type"]=1; //"TYPE_TUILE";
                                    }
                                }
                                else { //vector
                                    $tabLayers[$i]["type"] = $rs->Read(2); //type for postprocessing
                                }
                                $tabLayers[$i]["table_name"] = $rs->Read(3); //table for postprocessing
                                $tabLayers[$i]["id"] = $rs->Read(0); //id for postprocessing and for extjs tree
                                $tabLayers[$i]["leaf"] = true; //leaf for extjs tree
                                $tabLayers[$i]["cls"] = "folder"; //cls for extjs tree
                                $tabLayers[$i]["children"] = ""; //children for extjs tree
                                $tabLayers[$i]["uuid"] = $rs->Read(4);
                                $tabLayers[$i]["dataset_title"] =  str_replace(array("{",
                                                          "\"",
                                                          "}"
                                                    ), "", $rs->Read(5));
                                $tabLayers[$i]["coucheNom"] =  $rs->Read(1);
                                
                                $i++;
                            }
                        }
                    }
                }

                if(empty($tabLayers)) {
                    $errMessage = "Le serveur cartographique ne dispose d\'aucune couche de données.";
                }
            }
            else {
                $errMessage = "Erreur interne: le serveur cartographique est vide.";
            }
        }
        else {
            $errMessage = "Erreur interne: ";
            if(is_null($m_srv)) {
                $errMessage .= "\\nAucun serveur cartographique n'est défini [SRV_CARTO].";
            }
        }
        if(!is_null($errMessage)) {
            return json_encode(array());
        }
        else {
            return (json_encode($tabLayers));
        }
    }
}

<?php
namespace ProdigeCatalogue\JoinBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route; ;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Prodige\ProdigeBundle\Common\DBManager\ViewFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Controller\BaseController;

//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Session\Session; // TODO hismail
//use Prodige\ProdigeBundle\DAOProxy\DAO;
//use Prodige\ProdigeBundle\Controller\User;
//use Prodige\ProdigeBundle\Common;
//use Prodige\ProdigeBundle\Common\Util;
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class SynchronisationCouchesController extends BaseController
{

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/join/services/synchronisation_couches", name="catalogue_join_services_synchronisation_couches", options={"expose"=true})
     */
    public function synchronisationCouchesAction(Request $request)
    {
        //TODO check rights on uuid
        
        $couche = $request->get("couche", null);
        if ($couche !== null)
            $couche = base64_decode($couche);
        
        $result = array(
            "success" => true,
            "action" => ""
        );
        if (is_null($couche))
            return new JsonResponse($result);
        
        $PRODIGE = $this->getProdigeConnection('public');
        $CATALOGUE = $this->getCatalogueConnection('catalogue');

        ! $PRODIGE->isTransactionActive() && $PRODIGE->beginTransaction();

        $rs = $PRODIGE->executeQuery('select pk_view, view_name from parametrage.prodige_view_info where view_name=?', array(
            ViewFactory::$INITIAL_VIEW_PREFIX . '_' . $couche
        ));
        if ($rs->rowCount() > 0) {
            $row = $rs->fetch(\PDO::FETCH_ASSOC);
            $pk_view = $row["pk_view"];
            $view_composite_name = $row["view_name"];
            $arSQL = array();
            $arSQL[] = "DELETE FROM parametrage.prodige_computed_field_info WHERE pk_view=:pk_view;";
            $arSQL[] = "DELETE FROM parametrage.prodige_join_info WHERE pk_view=:pk_view;";
            $arSQL[] = "DELETE FROM parametrage.prodige_view_info WHERE pk_view=:pk_view;";
            $arSQL[] = "DELETE FROM parametrage.prodige_view_composite_info WHERE pk_view=:pk_view;";
            
            $arSQL[] = "DROP VIEW IF EXISTS \"".$couche."\"";
            if ($view_composite_name) {
                $arSQL[] = "DROP VIEW IF EXISTS \"".$view_composite_name."\";";
            }
            foreach ($arSQL as $SQL) {
                $PRODIGE->executeQuery($SQL, array(
                    "pk_view" => $pk_view
                ));
                
            }
            $PRODIGE->commit();
        }

        return new JsonResponse($result);
    }
}

<?php

namespace ProdigeCatalogue\JoinBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;


use Prodige\ProdigeBundle\Common;
use Prodige\ProdigeBundle\Common\Util;
use Symfony\Component\HttpFoundation\Session\Session; //TODO hismail
use Prodige\ProdigeBundle\Controller\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Retourne la liste des cartes comprenant une table
 * @author Alkante
 */

class LayerGetMapsController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/join/services/layer_get_maps", name="catalogue_join_services_layer_get_maps", options={"expose"=true})
     */
    public function layerGetMapsAction(Request $request) {
        //if(!isset($AdminPath))
            //$AdminPath = "../../";
            
            //require_once("../../DAO/DAO/DAO.php");
            //require_once("../../DAO/ConnectionFactory/ConnectionFactory.php");
            $result = array();
            $result['success'] = true;
            $result['msg'] = 'Votre demande est prise en compte.';
            //$callback = (isset($_GET["callback"]) ? $_GET["callback"] : "");
            $callback = $request->query->get("callback", "");
            
            //global $PRO_MAPFILE_PATH; 
            //$strBaseDir = $PRO_MAPFILE_PATH."Publication/";

            //$strBaseDir = $PRO_MAPFILE_PATH;
            $strBaseDir = PRO_MAPFILE_PATH;
            //$data = $_GET["layerTable"];
            $data = $request->query->get("layerTable");
            //$couche_type = $_GET["coucheType"];
            $couche_type = $request->query->get("coucheType");
            $strJson = array();

            $strJson = array_merge($strJson, $this->checkListMaps($strBaseDir, $data, $couche_type, false));
            $strJson = array_merge($strJson, $this->checkListMaps($strBaseDir."/carteperso", $data, $couche_type, true));
            // à partir du fichier .map retrouver les nom de carte

            $strJson = array_merge($strJson, $this->layerMapNameAction($request,true));

            return new Response($callback."(".json_encode($strJson).")");
            
    }

    protected function checkListMaps($Directory, $data, $couche_type, $bSubDir=false) {
        $strJson = array();
        //TODO hismail - Added by Marie - Migration Prodige 3.4 ---> Prodife 4.0
        if(!file_exists($Directory))
            return $strJson;

        $MyDirectory = opendir($Directory) or die('Erreur');
        while($Entry = readdir($MyDirectory)) {
            if(is_dir($Directory.'/'.$Entry) && $Entry != '.' && $Entry != '..') {
                // do not convert sub repertories
                //print('scan directory '.$Directory);printf("\n");
                if($bSubDir) {
                    $strJson = array_merge($strJson, $this->checkListMaps($Directory.'/'.$Entry, $data, $couche_type, $bSubDir));
                }
            }
            else {
                if($this->GetExtensionName($Entry)=="map" && substr($Entry, 0, 10)!="temp_admin" &&  substr($Entry, 0, 4)!="TMP_" && strpos($Entry, ".inc.") === false) {
                    try {
                        $oMap = @ms_newMapObj($Directory."/".$Entry); //echo $Entry."<br><br>";
                        if($oMap) {
                            //echo $data;
                            for($i=0; $i<$oMap->numlayers; $i++) {
                                $oLayer = $oMap->getLayer($i);
                                if($couche_type=="raster") {
                                    if($oLayer->data == $Directory.$data || $oLayer->tileindex == $Directory.$data)
                                        $strJson[] = $Entry;
                                } else { //echo "<br>".$oLayer->data.'<br>';
                                    if($oLayer->connectiontype == MS_POSTGIS) {
                                        if(stripos($oLayer->data, $data)) {
                                            $strJson[] = $Entry;
                                        }
                                    }
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        //error_log("impossible d'ouvrir ".$Directory."/".$Entry);
                        $this->getLogger()->debug("impossible d'ouvrir ".$Directory."/".$Entry);
                    }
                }
            }
        }
        return $strJson;
    }

    /* GetExtensionName - Renvoie l'extension d'un fichier
     . $File (char): Nom du fichier
     . $Dot  (bool): avec le point true/false
     */
    protected function GetExtensionName($File, $Dot=false, $toLower=true) {
        $Ext = pathinfo($File, PATHINFO_EXTENSION);
        if ( $toLower ) $Ext = strtolower($Ext);
        if($Dot == true) {
            $Ext = '.'.$Ext;
        }
        return $Ext;
    }


    /*
     * Created on 15 nov. 10
     *
     * To change the template for this generated file go to
     * Window - Preferences - PHPeclipse - PHP - Code Templates
     * Moved on 18 may. 16 by hismail - Prodige V.4
     */
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/join/services/layer_map_name", name="catalogue_join_services_layer_map_name", options={"expose"=true})
     */
    public function layerMapNameAction(Request $request, $returnStringValue = false) {

        //$AdminPath = "../Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");
        $ListMapFName = "";
        //$ListMapFName = $_GET['ListMapName'];
        $ListMapFName = $request->query->get("ListMapName");
        $ListMapFName = substr($ListMapFName, 1, -1);
        $tabNameMapFile = array();
        if($ListMapFName != "") {
            $tabNameMapFile = explode(",", $ListMapFName);
        }

        $ListMapName = array();
        foreach($tabNameMapFile as $mapFileName1) {
            // generation liste des noms de cartes

            $strSql = "set search_path to catalogue;" .
                      //" SELECT distinct cartp_nom FROM cartes_sdom WHERE stkcard_path =".$mapFileName1;
                      " SELECT distinct cartp_nom FROM cartes_sdom WHERE stkcard_path =?";

            $mapfileName = "";

            //$dao = new DAO();
            $conn = $this->getCatalogueConnection('catalogue');
            $dao = new DAO($conn, 'catalogue');
            if($dao) {
                $rs_sdom = $dao->BuildResultSet($strSql, array($mapFileName1));
                $i = 0;
                for($rs_sdom->First(); !$rs_sdom->EOF() ; $rs_sdom->Next()) {
                    $mapfileName = $rs_sdom->Read(0);
                    $i++;
                }
            }

            if($mapfileName != "" && $mapfileName != "1_") {
                $ListMapName[] = $mapfileName;
            } else { // si pas de resultat on affiche tout de meme le nom du mapfile
                $ListMapName[] = $mapFileName1;
            }
        }

        if ($returnStringValue) {
          return $ListMapName;
        } else {
          new JsonResponse($ListMapName);
        }
    }
}

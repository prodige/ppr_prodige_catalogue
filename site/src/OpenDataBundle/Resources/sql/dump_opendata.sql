--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.9
-- Dumped by pg_dump version 9.6.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;
--
-- Name: harves_opendata_node; Type: TABLE; Schema: catalogue; Owner: user_admin
--
CREATE TABLE catalogue.harves_opendata_node (
    pk_node_id integer NOT NULL,
    node_name text,
    node_logo text,
    api_url text,
    node_last_modification text,
    url_dcat text,
    node_log_file text,
    command_id integer DEFAULT 0 NOT NULL
);
ALTER TABLE catalogue.harves_opendata_node OWNER TO user_admin;
--
-- Name: TABLE harves_opendata_node; Type: COMMENT; Schema: catalogue; Owner: user_admin
--
COMMENT ON TABLE catalogue.harves_opendata_node IS 'moissonnage DCAT';
--
-- Name: harves_opendata_node_pk_node_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_admin
--
CREATE SEQUENCE catalogue.harves_opendata_node_pk_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER TABLE catalogue.harves_opendata_node_pk_node_id_seq OWNER TO user_admin;
--
-- Name: harves_opendata_node_pk_node_id_seq; Type: SEQUENCE OWNED BY; Schema: catalogue; Owner: user_admin
--
ALTER SEQUENCE catalogue.harves_opendata_node_pk_node_id_seq OWNED BY catalogue.harves_opendata_node.pk_node_id;
--
-- Name: harves_opendata_node pk_node_id; Type: DEFAULT; Schema: catalogue; Owner: user_admin
--
ALTER TABLE ONLY catalogue.harves_opendata_node ALTER COLUMN pk_node_id SET DEFAULT nextval('catalogue.harves_opendata_node_pk_node_id_seq'::regclass);
--
-- Name: harves_opendata_node harves_opendata_node_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_admin
--
ALTER TABLE ONLY catalogue.harves_opendata_node
    ADD CONSTRAINT harves_opendata_node_pkey PRIMARY KEY (pk_node_id);

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;
SET default_tablespace = '';
SET default_with_oids = false;
--
-- Name: harves_opendata_node_params; Type: TABLE; Schema: catalogue; Owner: user_admin
--
CREATE TABLE catalogue.harves_opendata_node_params (
    pk_params_id integer NOT NULL,
    fk_node_id integer NOT NULL,
    key text,
    value text
);
ALTER TABLE catalogue.harves_opendata_node_params OWNER TO user_admin;
--
-- Name: TABLE harves_opendata_node_params; Type: COMMENT; Schema: catalogue; Owner: user_admin
--
COMMENT ON TABLE catalogue.harves_opendata_node_params IS 'paramètres nœuds de moissonnage DCAT';
--
-- Name: harves_opendata_node_params_FK_node_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_admin
--
CREATE SEQUENCE catalogue."harves_opendata_node_params_FK_node_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE catalogue."harves_opendata_node_params_FK_node_id_seq" OWNER TO user_admin;
--
-- Name: harves_opendata_node_params_FK_node_id_seq; Type: SEQUENCE OWNED BY; Schema: catalogue; Owner: user_admin
--
ALTER SEQUENCE catalogue."harves_opendata_node_params_FK_node_id_seq" OWNED BY catalogue.harves_opendata_node_params.fk_node_id;
--
-- Name: harves_opendata_node_params_pk_params_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_admin
--
CREATE SEQUENCE catalogue.harves_opendata_node_params_pk_params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE catalogue.harves_opendata_node_params_pk_params_id_seq OWNER TO user_admin;
--
-- Name: harves_opendata_node_params_pk_params_id_seq; Type: SEQUENCE OWNED BY; Schema: catalogue; Owner: user_admin
--
ALTER SEQUENCE catalogue.harves_opendata_node_params_pk_params_id_seq OWNED BY catalogue.harves_opendata_node_params.pk_params_id;
--
-- Name: harves_opendata_node_params pk_params_id; Type: DEFAULT; Schema: catalogue; Owner: user_admin
--
ALTER TABLE ONLY catalogue.harves_opendata_node_params ALTER COLUMN pk_params_id SET DEFAULT nextval('catalogue.harves_opendata_node_params_pk_params_id_seq'::regclass);
--
-- Name: harves_opendata_node_params fk_node_id; Type: DEFAULT; Schema: catalogue; Owner: user_admin
--
ALTER TABLE ONLY catalogue.harves_opendata_node_params ALTER COLUMN fk_node_id SET DEFAULT nextval('catalogue."harves_opendata_node_params_FK_node_id_seq"'::regclass);
--
-- Name: harves_opendata_node_params harves_opendata_node_params_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_admin
--
ALTER TABLE ONLY catalogue.harves_opendata_node_params
    ADD CONSTRAINT harves_opendata_node_params_pkey PRIMARY KEY (pk_params_id);
--
-- Name: harves_opendata_node_params harves_opendata_node_params_FK_node_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_admin
--
ALTER TABLE ONLY catalogue.harves_opendata_node_params
    ADD CONSTRAINT "harves_opendata_node_params_FK_node_id_fkey" FOREIGN KEY (fk_node_id) REFERENCES catalogue.harves_opendata_node(pk_node_id);

--
-- PostgreSQL database dump complete
--

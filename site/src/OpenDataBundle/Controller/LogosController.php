<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace ProdigeCatalogue\OpenDataBundle\Controller;


use Symfony\Component\HttpFoundation\Response;
use Prodige\ProdigeBundle\Controller\BaseController;


/**
 * Description of LogosController
 *
 * @author hroignant
 */
class LogosController extends BaseController {
    
    public function GetLogosHarvesAction() {
        
        
        $logos = array(); 
        
        $url = $this->container->getParameter('PRO_GEONETWORK_URLBASE').'srv/api/logos';
        $response = $this->doCurl($url);
        $path =  $this->container->getParameter('PRO_GEONETWORK_URLBASE')."images/harvesting/";
        $array_logos = json_decode($response);
        $k = 0; 
        
        for($i = 0 ; $i<count($array_logos); $i++ ) {
            if(strpos($array_logos[$i],".png") !== false || strpos($array_logos[$i],".png") !== false || strpos($array_logos[$i],".png") !== false) {
                $logos[$k]["path"] = $path.$array_logos[$i]; 
                $k++; 
            }             
        }
        
        
        return new Response(json_encode($logos)); 
    }
    
    
    // appel de l'url
    public function doCurl($url) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");


        $headers = array();
        $headers[] = "Accept: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close ($ch);
        
        return $result; 
    }    
}

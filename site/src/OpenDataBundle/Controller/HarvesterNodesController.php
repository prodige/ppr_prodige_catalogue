<?php
namespace ProdigeCatalogue\OpenDataBundle\Controller;
 

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;


class HarvesterNodesController extends BaseController {

    
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/opendata", name="catalogue_opendata", options={"expose"=true}, methods={"GET"})
     */
    public function GetHarvesterNodesAction() {
        $session = $this->container->get('request_stack')->getCurrentRequest()->getSession();      
        if($session->get('orga')) {
            $orga = $session->get('orga'); 
        } else {
            $orga = $this->getOrganizations(); 
            $session->set("orga", $orga); 
        }
        
        $conn = $this->getCatalogueConnection('catalogue');
        $query = "SELECT * FROM harves_opendata_node order by pk_node_id asc"; 
        $nodes = $conn->fetchAllAssociative($query);
        
        for($i = 0 ; $i < count($nodes) ; $i++) {
            $query ="SELECT uuid FROM public.sources where name =:node_name"; 
            $result = $conn->fetchAllAssociative($query, array("node_name"=>$nodes[$i]["node_name"]));
            if(isset($result[0])) {
                $nodes[$i]["uuid_source"] = $result[0]["uuid"];              
            }
            
            $nodes[$i]["node_logo"] = PRO_GEONETWORK_URLBASE.$nodes[$i]["node_logo"]; 
        }
        
        $query = "SELECT * FROM harves_opendata_node_params"; 
        $nodes_params = $conn->fetchAllAssociative($query);
        for($i=0; $i<count($nodes_params); $i++) {
            if($nodes_params[$i]["key"] === "organization") {
                $nodes_params[$i]["value"] = explode( ",", $nodes_params[$i]["value"]); 
            }
        }
        for($i = 0 ; $i < count($nodes); $i++) {
            $querry = "select * from catalogue.scheduled_command where id =:command_id"; 
            $result = $conn->fetchAllAssociative($querry, array("command_id"=>$nodes[$i]["command_id"]));
            if (isset($result) && !empty($result)) {
                $nodes[$i]["node_last_exec"] = $result[0]["last_execution"];
                $nodes[$i]["node_exec_frequency"] = $result[0]["frequency"];
                $cron_expression = $result[0]['cron_expression']; 
                $minutes = substr (  $cron_expression , 0 , 2  ); 
                $heures = substr ($cron_expression, 3, 2); 
                $nodes[$i]["node_planif_time"] = $heures.":".$minutes; 
            }
            $querry = "select * from catalogue.execution_report where scheduled_command_id =:command_id order by date_time DESC limit 1"; 
            $result = $conn->fetchAllAssociative($querry, array("command_id"=>$nodes[$i]["command_id"]));
            
            if (isset($result[0]) && !empty($result[0])) {
                $nodes[$i]["node_last_exec_status"] = $result[0]["status"];
                $status = $nodes[$i]["node_last_exec_status"]; 
                $nodes[$i]["node_last_exec_img_status"] = $this->getImageStatusExec($status);   
                // creation de la route vers le fichier de log
                $nodes[$i]["file"] = $this->downloadFileAction($result[0]["id"]); 
                $nodes[$i]["report_id"] = $result[0]["id"]; 
            }
        }
        
        for($i = 0 ; $i < count($nodes); $i++) {
            $k = 0;
            for($j = 0 ; $j<count($nodes_params); $j++) {
                if(isset($nodes[$i]["pk_node_id"]) && isset($nodes_params[$j]["fk_node_id"])) {
                    if($nodes_params[$j]["fk_node_id"] === $nodes[$i]["pk_node_id"]) {
                        $nodes[$i]["node_params"][$k]["pk_id"] = $nodes_params[$j]["pk_params_id"]; 
                        $nodes[$i]["node_params"][$k]["fk_id"] = $nodes_params[$j]["fk_node_id"]; 
                        $nodes[$i]["node_params"][$k]["key"] = $nodes_params[$j]["key"]; 
                        $nodes[$i]["node_params"][$k]["value"] = $nodes_params[$j]["key"]!=='organization' ? urldecode($nodes_params[$j]["value"]): $nodes_params[$j]["value"];
                        $k++; 
                    }                            
                }
            }
        }
        $logosjson = $this->forward('ProdigeCatalogue\OpenDataBundle\Controller\LogosController::GetLogosHarvesAction')->getContent();
        $logos = json_decode($logosjson); 
        
        $target_template = "OpenDataBundle/Default/index.html.twig";
        
        return $this->render($target_template, array("nodes"=>$nodes, "logos"=> $logos, "orga"=>$orga ));
    }
   
    /**
     * 
     * @param type $status
     * @return typeGet Image css class
     * @param status (success|warning)
     */
    public function getImageStatusExec($status) {

        $image_status = image_status_fail;

        if(strtolower($status)==="success" ) {
            $image_status = image_status_success; 
        } else if (strtolower($status)==="warning") {
            $image_status = image_status_warning; 
        } 

        return $image_status; 
    }
   
    
    /**
    * @Route("/opendata", name="post_catalogue_opendata_harvester_nodes", options={"expose"=true}, methods={"POST"})
    */
    public function PostHarvesterNodeAction(Request $request) {
        
        $conn = $this->getCatalogueConnection('catalogue');

        if($request->request->get('node_config')){
            
            $request_params = $request->request->get('node_config'); 
            $node['node_name'] = $request_params[0]; 
            $node['node_logo'] = $request_params[1]; 
            $node['node_logo'] = str_replace(PRO_GEONETWORK_URLBASE, "", $node['node_logo']); 
            $node["api_url"] = $this->forceSlashUrl($request_params[2]);             
            $node["url_dcat"] = $this->forceSlashUrl($request_params[3]);               
            $node["node_id"] = intval($request_params[4]);  
            $node["source_uuid"] = $request_params[5];             
            $node["node_exec_period"] = $request_params[6]; 
            $node["node_exec_day"] = $request_params[7]; 
            $node["node_exec_time"] = $request_params[8]; 
                       
            $node["node_last_modification"] = date("Y-m-d");    
            $node["node_last_modification"]  .="T".date("H:i:s"); 


            if($node["node_id"] === 0) {
                $public_conn = $this->getCatalogueConnection('public');
                $catalogue_uuid = $this->create_catalogue_uuid (); 
                
                // ajout verif absence d'un catalogue de même nom
                $sql = "SELECT COUNT(*) FROM public.sources where name =:new_source_name"; 
                $result = $conn->fetchAllAssociative($sql, array('new_source_name'=>$node["node_name"]));
                $nb_result = $result[0]["count"];

                $indice = 1; 
                while ($nb_result > 0 ) {

                        $marque_indice_pos = strpos($node["node_name"], "-"); 
                        if($marque_indice_pos !== false ) {
                            $node["node_name"] = substr($node["node_name"] , 0, $marque_indice_pos); 
                        }

                        $node["node_name"] =  $node["node_name"]."-".$indice;

                        $sql = "SELECT COUNT(*) FROM public.sources where name = :new_source_name"; 
                        $result = $conn->fetchAllAssociative($sql, array('new_source_name'=>$node["node_name"]));
                        $nb_result = $result[0]["count"];

                        $indice++; 

                }
                
                $querry = "INSERT INTO sources (uuid, name) VALUES (:uuid, :new_source_name)";
                $public_conn->executeStatement($querry, array("uuid" => $catalogue_uuid, "new_source_name" => $node['node_name']));

                $table_langue = array("ara", "cat","chi", "dut", "eng", "fin", "fre", "ger", "nor", "por", "rus", "spa", "vie" ); 
                
                for($j = 0 ; $j < count($table_langue); $j++) {
                    $querry = "INSERT INTO sourcesdes (iddes, label, langid) VALUES (:iddes, :new_source_name, :new_source_langid)";     
                    $public_conn->executeStatement($querry, array("iddes" => $catalogue_uuid, "new_source_name" => $node['node_name'], "new_source_langid"=>$table_langue[$j]));
                }
                $conn = $this->getCatalogueConnection('catalogue');
               
                $query = "SELECT MAX(id) FROM scheduled_command"; 
                $result = $conn->fetchAllAssociative($query);
                $command_id = $result[0]['max'] +1 ;                   
                
                $sql = "INSERT INTO harves_opendata_node (node_name, node_logo, api_url, url_dcat, node_last_modification, command_id) VALUES (:new_node_name,:new_node_logo,:new_api_url,:new_url_dcat, :node_last_modification, :command_id) RETURNING pk_node_id";     
                $conn->executeStatement($sql, array("new_node_name" => $node['node_name'],"new_node_logo" => $node['node_logo'], "new_api_url" => $node["api_url"], "new_url_dcat" => $node["url_dcat"],"node_last_modification" => $node["node_last_modification"],"command_id" => $command_id));

                
                $query = "SELECT pk_node_id FROM harves_opendata_node ORDER BY node_last_modification DESC LIMIT 1"; 
                $result = $conn->fetchAllAssociative($query);
                $node["node_id"] = $result[0]['pk_node_id'];      
                
                // ajout de la commande sur la table scheduled_command
                $command_name = 'Moissonnage Dcat node '.$node['node_name']; 
                $cron_expression = $this->build_crontab_expression($node["node_exec_period"], $node["node_exec_day"], $node["node_exec_time"], $node["node_id"]); 
                
                $query = "insert into catalogue.scheduled_command (id, name, command, arguments, cron_expression, is_periodic, frequency) values (:id, :name, :command, :arguments, :cron_expression, :is_periodic, :frequency)"; 
                $conn->executeStatement($query, array("id"=>$command_id, "name" => $node['node_name'],"command" => $command_name, "arguments" => $node["node_id"], "cron_expression" => $cron_expression,"is_periodic" => true,"frequency"=> $node["node_exec_period"]));
                
            } else {
                
                $sql = "UPDATE harves_opendata_node SET node_name = :new_node_name, node_logo=:new_node_logo, api_url=:new_api_url, url_dcat=:new_url_dcat,node_last_modification = :node_last_modification WHERE pk_node_id = :node_id"; 
                $result = $conn->executeStatement($sql, array(
                    "new_node_name" => $node['node_name'],"new_node_logo" => $node['node_logo'], "new_api_url" => $node["api_url"], "new_url_dcat" => $node["url_dcat"],"node_last_modification"=>$node["node_last_modification"],"node_id"  => $node["node_id"]
                ));      
                $querry = "UPDATE sources  SET name =:new_source_name WHERE uuid =:uuid";     
                $public_conn = $this->getCatalogueConnection('public');
                $public_conn->executeStatement($querry, array("new_source_name" => $node['node_name'], "uuid" => $node['source_uuid']));
                $catalogue_uuid = $node['source_uuid']; 
                $querry = "UPDATE sourcesdes SET label =:new_source_name WHERE iddes =:uuid";     
                $public_conn->executeStatement($querry, array("new_source_name" => $node['node_name'], "uuid" => $node['source_uuid']));
                
                
                $conn = $this->getCatalogueConnection('catalogue');
                // ajout de la commande sur la table scheduled_command
                $command_name = 'Moissonnage Dcat node '.$node['node_name']; 
                $cron_expression = $this->build_crontab_expression($node["node_exec_period"], $node["node_exec_day"], $node["node_exec_time"], $node["node_id"]); 
                
                $query = "update catalogue.scheduled_command set name =:name, command=:command, cron_expression=:cron_expression, is_periodic=:is_periodic, frequency=:frequency where arguments=:arguments"; 
                $conn->executeStatement($query, array("name" => $node['node_name'],"command" => $command_name, "cron_expression" => $cron_expression,"is_periodic" => true,"frequency"=> $node["node_exec_period"], "arguments" => $node["node_id"]));
                
            }
            $target_dir = $this->container->getParameter('PRO_GEONETWORK_DIRECTORY')."/WEB-INF/data/data/resources/images/logos/";
    
            $logo_name = $catalogue_uuid.".png"; 
            $target_logo = $target_dir.$logo_name; 
                
            $origine_logo = $node['node_logo']; 
            if(file_exists($origine_logo)){
                copy($origine_logo, $target_logo);             
            }
            
            if($request->request->get('api_params')) {
                $api_params = $request->request->get('api_params');      
                for ($i = 0 ; $i<count($api_params); $i++) {
                    $query = "SELECT pk_params_id FROM harves_opendata_node_params WHERE ( fk_node_id =:new_node_id AND key =:new_api_param_key )"; 
                    $result = $conn->fetchAssociative($query, array('new_node_id'=>$node["node_id"],"new_api_param_key" => $api_params[$i][0]));
                    if(isset($result["pk_params_id"]) && $result["pk_params_id"] !=="" ) {
                        // MISE A JOUR DU PARAM
                        if(isset($api_params[$i][1]) && $api_params[$i][1] !=="") {
                            $sql = "UPDATE harves_opendata_node_params SET value =  :new_value WHERE (pk_params_id=:pk_params_id )"; 
                            $result = $conn->executeUpdate($sql, array(
                                "pk_params_id" => $result["pk_params_id"], "new_value" => urlencode($api_params[$i][1])
                            ));                             
                        } else {
                            // SUPRRESSION
                            $sql = "DELETE from harves_opendata_node_params WHERE (pk_params_id=:pk_params_id )"; 
                            $result = $conn->executeUpdate($sql, array(
                                "pk_params_id" => $result["pk_params_id"]
                            ));                               
                        }
                    } else if($api_params[$i][1] !=="" || $i == 0) {
                        
                        $sql = "INSERT INTO harves_opendata_node_params (FK_node_id, key, value) VALUES (:new_node_id,:new_api_param_key,:new_api_param_value)"; 
                        $result = $conn->executeStatement($sql, array(
                                "new_node_id" => $node["node_id"],
                                "new_api_param_key" => $api_params[$i][0], 
                                "new_api_param_value" => urlencode($api_params[$i][1])
                        ));                                
                    }
                }
            }
            
            $arrData = ['output' => 'success'];
            
        } else if($request->request->get('mode') === "add_new_param_value") {
            
            $resquest_params = $request->request->get('api_params'); 
            $param_fk_id = $resquest_params[0]; 
            $param_key = $resquest_params[1];
            $param_new_val = $resquest_params[2]; 
            
            $sql = "SELECT value FROM harves_opendata_node_params WHERE (fk_node_id=:fk_id AND key =:key)";
            $result = $conn->fetchAllAssociative($sql, array(
                    "fk_id" => $param_fk_id,
                    "key" => $param_key
            ));  
            if(isset($result[0]["value"])) {
                $old_params_value = $result[0]["value"]; 
                $new_params_value = $old_params_value.", ".$param_new_val; 
                $sql = "UPDATE harves_opendata_node_params SET value =  :new_value WHERE (fk_node_id=:fk_id AND key =:key)"; 
                $result = $conn->executeStatement($sql, array(
                    "fk_id" => $param_fk_id,
                    "key" => $param_key, 
                    "new_value" => $new_params_value
                ));  
            }
            $arrData = ['output' => 'success new param value addded'];
        } 
        $execution_mode = $request->request->get('exec'); 
        if($execution_mode === "true"){
            // appel via Symfony Processs 
            $process = new Process(array("php", "bin/console" ,"prodige:call-harves-api", $node["node_id"])); 
            
            $process->setWorkingDirectory($this->container->getParameter('PRODIGE_PATH_CATALOGUE'));
            $process->setTimeout(null);
            $process->run();
            $resultcommande = $process->getOutput();
            
            // version avant passage par symfony Process
//                $resultcommande = array(); 
//                $catalogue_root_dir = $this->getParameter('PRODIGE_PATH_CATALOGUE');
//
//                $cd_command = "cd ".$catalogue_root_dir.";";
//                $dcat_command = "php bin/console prodige:call-harves-api ".$node["node_id"];
//                $cmd = $cd_command.$dcat_command; 
//
//                exec($cmd, $resultcommande); 
            // ------------------------------------------
            
            
            $arrData = ['output' => $resultcommande];              
            
        }    
        return new Response(json_encode($arrData));
    }

    /**
    * @Route("/opendata", name="delete_catalogue_opendata_harvester_nodes", options={"expose"=true}, methods={"DELETE"})
    */    
    public function DeleteHarvesterNodeAction(Request $request) {
        
        $conn = $this->getCatalogueConnection('catalogue');

        if($request->request->get('node_id')){
            $node_id = $request->request->get('node_id');
            
            // supprimer l'ensemble des métadonnées du catalogue
            $this->DeleteMetadataAction($conn, $node_id); 
            
            $sql = "select command_id from catalogue.harves_opendata_node where pk_node_id =:node_id"; 
            $command_id = $conn->fetchAllAssociative($sql, array("node_id"=>$node_id))[0]['command_id']; 
            
            $this->deleteOldLogFile($command_id); 
            
            $sql = "delete from catalogue.execution_report where scheduled_command_id =:scheduled_command_id"; 
            $conn->executeStatement($sql, array("scheduled_command_id"  => $command_id));       
            
            $sql = "delete from catalogue.scheduled_command where id=:scheduled_command_id";
            $conn->executeStatement($sql, array("scheduled_command_id"  => $command_id));      
            
            
            $sql = "SELECT node_name FROM catalogue.harves_opendata_node WHERE pk_node_id =:node_id"; 
            $result = $conn->fetchAllAssociative($sql, array(
                "node_id"  => $node_id
            ));   
            $node_name = $result[0]["node_name"]; 
            
            $sql = "DELETE FROM catalogue.harves_opendata_node_params WHERE fk_node_id = :node_id"; 
            $conn->executeStatement($sql, array(
                "node_id"  => $node_id
            ));                       
            $sql = "DELETE FROM catalogue.harves_opendata_node WHERE pk_node_id = :node_id"; 
            $conn->executeStatement($sql, array(
                "node_id"  => $node_id
            ));
            
            // supprimer le catalogue
            $public_conn = $this->getCatalogueConnection('public');
            $sql = "delete from public.sourcesdes where label=:label; "; 
            $public_conn ->executeStatement($sql, array("label"=>$node_name));             
            
            $sql = "delete from public.sources where name=:name; "; 
            $public_conn ->executeStatement($sql, array("name"=>$node_name)); 
            
            $arrData = ['output' => 'success delete harvester node'];           
        } else if($request->request->get('pk_param_id')) {
            // supression de paramètres
            $pk_param_id = intval($request->request->get('pk_param_id'));
            $fk_node_id = intval($request->request->get('fk_param_id'));
            
            $sql = "DELETE FROM harves_opendata_node_params WHERE pk_params_id = :pk_params_id"; 
            $conn->executeStatement($sql, array(
                "pk_params_id"  => $pk_param_id
            ));            
            
            $node_last_modification = date("Y-m-d");    
            $node_last_modification .="T".date("H:i:s"); 
            $sql = "UPDATE harves_opendata_node SET node_last_modification = :node_last_modification WHERE pk_node_id = :fk_node_id"; 
            $conn->executeStatement($sql, array(
                "node_last_modification"=>$node_last_modification,
                "fk_node_id"  => $fk_node_id
            ));    
            
            $arrData = ['output' => 'success delete param api'];
        } else  {
            $arrData = ['output' => 'fail here the result which will appear in div'];
        }

        return new Response(json_encode($arrData));
    }
    
    // -----------------------------------------------------------
    // suppression des fichiers de logs de reports d'execution à la suppression du catalogue  
    public function deleteOldLogFile($scheduled_command_id) {
        
        $conn = $this->getCatalogueConnection('catalogue');
        $sql = "select file from catalogue.execution_report where scheduled_command_id=:scheduled_command_id"; 
        
        $result = $conn ->fetchAllAssociative($sql, array('scheduled_command_id'=>$scheduled_command_id)); 
        if(!empty($result)) {
            $log_file = $result[0]['file']; 
            
            unlink($log_file);
        }
    }
    
    /**
     * Get organization list
     * @return type
     */
    public function getOrganizations() {
        $resultjson = ""; 
        if(PRO_HARVES_DCAT_ORGANIZATIONS) {
            $url = PRO_HARVES_DCAT_ORGANIZATIONS; 
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $output = curl_exec($ch);
        if($output === false) {
           $result = curl_error($ch); 
        } else{
            $resultjson = $output; 
        }
        curl_close($ch);        
        if($resultjson !=="") {
            $result = json_decode($resultjson, true);
            $nb_orga = $result["total"] ; 
            $resultjson = ""; 

            $url .= "?sort=name&page_size=".$nb_orga; 
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, false);
            $output = curl_exec($ch);
            if($output === false) {
               $result = curl_error($ch); 
            } else{
                $resultjson = $output; 
            }
            curl_close($ch);              
            }
            if($resultjson !=="") {
                $result = json_decode($resultjson, true); 
                $orga = $result["data"]; 
                $light_orga = array() ; 
                for($i = 0 ; $i < count($orga); $i++) {
                    $light_orga[$i]["id"] = $orga[$i]["id"]; 
                    $light_orga[$i]["name"] = $orga[$i]["name"]; 
                }
            }
        return $light_orga; 
    }
    
    protected function forceSlashUrl($url) {
        
        if(substr($url, -1, 1) !=="/") {
                $url.= "/"; 
        }        
        return $url;
    }
    
    // -----------------------------------------------------------
    // création de l'uuid du catalogue   
    protected function create_catalogue_uuid () {
        
        $rand = mt_rand ( 0, 0xffffffff ) ; 
        $rand = dechex($rand & 0xffffffff); 
        $catalogue_uuid = str_pad($rand, 8, '0', STR_PAD_LEFT)."-";
        
        
        $rand = mt_rand ( 0, 0xffff ) ; 
        $rand = dechex($rand & 0xffff);         
        $catalogue_uuid .= str_pad($rand, 4, '0', STR_PAD_LEFT)."-";
        $rand = mt_rand ( 0, 0xffff ) ; 
        $rand = dechex($rand & 0xffff);         
        $catalogue_uuid .= str_pad($rand, 4, '0', STR_PAD_LEFT)."-";
        $rand = mt_rand ( 0, 0xffff ) ; 
        $rand = dechex($rand & 0xffff);         
        $catalogue_uuid .= str_pad($rand, 4, '0', STR_PAD_LEFT)."-";

        $rand = mt_rand ( 0, 0xffffffff ) ; 
        $rand = dechex($rand & 0xffffffff);         
        $catalogue_uuid .= str_pad($rand, 8, '0', STR_PAD_LEFT); 
        $rand = mt_rand ( 0, 0xffff ) ; 
        $rand = dechex($rand & 0xffff);         
        $catalogue_uuid .= str_pad($rand, 4, '0', STR_PAD_LEFT);


        return $catalogue_uuid; 
    }
    
    // -----------------------------------------------------------
    // construction de l'expression de la tache planifiée
    protected function build_crontab_expression ($node_exec_period, $node_exec_day, $node_exec_time, $node_id) {
        
        $MMM = '*'; 
        $JJJ = '*'; 
        $jj = '*'; 
        if($node_exec_period==='weekly') {
            $JJJ = $node_exec_day; 
        } 
        $hh = substr($node_exec_time, 0, 2); 
        $mm = substr($node_exec_time, 3); 
       
        $current_path = getcwd (); 
        $command_path = str_replace("public", "bin/console", $current_path); 
            
        $command_name = $command_path." prodige:call-harves-api";   
        
        $cron_expression = $mm." ".$hh." ".$jj." ".$MMM." ".$JJJ." php ".$command_name." ".$node_id; 
        
        return $cron_expression; 
    }
    
    // -----------------------------------------------------------
    // Supprimer l'ensemble des métadonnées du catalogue
    protected function DeleteMetadataAction($conn, $node_id) {
        
        
        $user_login = $this->container->getParameter("phpcli_default_login");
        $user_pwd =   $this->container->getParameter("phpcli_default_password");
        $user_cert =  $this->container->getParameter("server_ca_cert_path");
            
        // récupération de l'uuid du catalogue
        $node_config = $conn->fetchAllAssociative("select node_name from harves_opendata_node where pk_node_id =:node_id", array("node_id"=>$node_id));           
        $source_name = $node_config[0]["node_name"]; 
        
        
        $prodige_conn = $this->getCatalogueConnection(); 
        $source_conf_array = $prodige_conn->fetchAllAssociative("select uuid from sources where name =:name", array("name"=>$source_name));   
        if(isset($source_conf_array[0])) {
            $source_uuid = $source_conf_array[0]["uuid"]; 

            $sql = "select id from public.metadata where source =:source_uuid"; 
            $array_metadata = $prodige_conn->fetchAllAssociative($sql, array("source_uuid"=>$source_uuid));
            if(!empty($array_metadata)) {
                // supression des métadatas
                CurlUtilities::initialize($user_login, $user_pwd, $user_cert);
                $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
                for($i = 0 ; $i < count($array_metadata) ; $i++) {
                    $metadata_id = $array_metadata[$i]["id"]; 
                    $geonetwork->get("md.delete?id=".$metadata_id);  
                }            
            }            
        }
     
    }
    
   
    /**
    * 
    * @Get("/opendata/logfile/{repport_id}") 
    * 
    * @RequestParam(name="from_app_dir",   description="", nullable = true, requirements="\d+", default="false" )
    *
    */    
    public function downloadFileAction($repport_id){

        $response = "";
        $conn = $this->getCatalogueConnection('catalogue');
        $sql = 'select file from catalogue.execution_report where id =:repport_id'; 
        $result = $conn->fetchAllAssociative($sql, array("repport_id"=>$repport_id)); 
        
        if(!empty($result)) {
            $filePath = $result[0]['file']; 
            $filename_pos = strpos($filePath, "node");
            $filename = substr($filePath, $filename_pos); 
            // check if file exists
            $fs = new FileSystem();
            if (!$fs->exists($filePath)) {
              return false;
            }
            // prepare BinaryFileResponse
            $response = new BinaryFileResponse($filePath);
            $response->trustXSendfileTypeHeader();
            $response->setContentDisposition(
                ResponseHeaderBag::DISPOSITION_INLINE,
                $filename,
                iconv('UTF-8', 'ASCII//TRANSLIT', $filename)
            );    
        }
        
        return $response;
    }    
}
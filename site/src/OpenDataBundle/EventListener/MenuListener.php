<?php

namespace ProdigeCatalogue\OpenDataBundle\EventListener;

/**
 * Description of MenuListener
 *
 * @author hroignant
 */
class MenuListener {
    
    public function onRegisterMenus(\Prodige\ProdigeBundle\Event\RegisterMenuEvent $rme)
    {
        if(PRO_IS_OPENDATA_ACTIF=="on"){
            $rme->addAdminMenuEntry('OpenData', 'catalogue_opendata');
        }
    }
        
}

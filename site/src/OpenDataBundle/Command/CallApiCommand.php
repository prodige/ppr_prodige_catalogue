<?php

namespace ProdigeCatalogue\OpenDataBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Prodige\ProdigeBundle\Services\CurlUtilities;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Description of CallApiCommand
 *
 */
class CallApiCommand extends Command
{

    public function __construct(
        ContainerInterface $container
    ) {
        parent::__construct();
        $this->container = $container;
    }

    // -----------------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------------    
    // Définition du nom d'appel, de la description et des paramètres de la commande
    protected function configure()
    {
        $this->setName("prodige:call-harves-api")
            ->setDescription("OpendataBundle : exec of a harves node")
            ->setHelp("This command allow you to start a new harves session to te url given")
            ->addArgument('node_id', InputArgument::REQUIRED, 'The id of the harves node.');
    }
    // -----------------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------------

    // appel de l'url
    protected function doCurl($url, $verbose = false, $basicAuthentication = null, $params = array())
    {
        $result = "";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);

        foreach ($params as $key => $value) {
            curl_setopt($ch, $key, $value);
        }

        if ($basicAuthentication !== null) {
            curl_setopt(
                $ch,
                CURLOPT_USERPWD,
                implode(":", $basicAuthentication)
            ); #A username and password formatted as "[username]:[password]" to use for the connection.
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        }
        $out = curl_exec($ch);
        if ($out === false) {
            $result = false;
        } else {
            $result = $out;
        }
        curl_close($ch);

        return $result;
    }

    // récupération de la connection
    protected function getConnection($connection_name, $schema = "public")
    {
        $conn = $this->container->get("doctrine")->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);

        return $conn;
    }

    // récupération de la connection
    protected function getSchemaConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }

    // -----------------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------------
    // function execute de la commande
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $nb_error = 0;
        $array_error = array();
        // initialisation du retour 
        $return_array = array();
        $array_data_iso = array();

        // ouverture du fichier de log : transmis a la fonction update_db
        // récupération de l'id node
        $node_id = $input->getArgument('node_id');
        if ($node_id !== "") {
            // supprimer les anciens fichiers de logs
            $this->delete_old_log_file($node_id);
            $file = $this->create_log_file($node_id);
        } else {
            $file = $this->create_log_file();
        }

        $log_file = $this->create_log_file_path($file);

        // permet l'écriture en console
        $output->setDecorated(true);


        // écriture du message en fichier de log et en console
        $this->write_log(
            $log_file,
            date("Y-m-d") . "T" . date("H:i:s") . ": nouvelle exécution du noeud " . $node_id,
            $output
        );

        // initialisation du modèle de métadonnées
        $metadata_template_id = $this->init_metadata_template($log_file, $output);
        $this->write_log($log_file, "modèle de métadonnée: " . $metadata_template_id, $output);

        // récupération de la connection au catalogue
        $conn = $this->getSchemaConnection('catalogue');
        // récupération du configreader
        $this->container->get('prodige.configreader');

        // récupération en base du noeud
        $node = $conn->fetchAllAssociative(
            "select * from harves_opendata_node where pk_node_id=:id",
            array("id" => $node_id)
        );
        // récupération des pararms pour construction de l'url
        $node_params = $conn->fetchAllAssociative(
            "select * from harves_opendata_node_params where fk_node_id=:id",
            array("id" => $node_id)
        );
        $metadata_array = array();

        if (isset($node[0])) {
            // une seule demande d'éxecution traitée; 
            $node = $node[0];
            // construction de l'url cible
            $request_url = $this->getFullUrl($node, $node_params);
            $this->write_log($log_file, "API url: " . $request_url, $output);
            // appel de l'API
            $resultjson = $this->doCurl($request_url);

            /*if (strpos($resultjson, "Error") !== false) {
                $return_array = $this->buildErrorReturn($log_file, $request_url);
                $this->buildMessageReturn($return_array);
                $this->update_node_db($node_id, $file, "FAILURE");

                $output->writeln(json_encode($return_array));
                return Command::SUCCESS;
            }*/
            $this->write_log($log_file, "HTTP return code 200 OK", $output);

            $indice_call = 0;
            $curl_indice = $indice_call + 1;
            $result[$indice_call] = json_decode($resultjson, true);

            if (empty($result[$indice_call])) {
                $return_array = $this->buildErrorReturn($log_file, $request_url);
                $this->buildMessageReturn($return_array);
                $this->update_node_db($node_id, $file, "FAILURE");
                $output->writeln(json_encode($return_array));

                return Command::SUCCESS;
            }
            // récupération de l'uuid du catalogue distant
            $source_uuid = $this->get_source_uuid($node_id);
            $nb_insert = 0;
            $nb_update = 0;
            $nb_ignore = 0;
            if (isset($result[$indice_call]["next_page"]) && $result[$indice_call]["next_page"] !== null || $result[$indice_call]["total"] !== 0) {
                if ($result[$indice_call]["next_page"] !== null) {
                    while ($result[$indice_call]["next_page"] !== null) {
                        $resultjson = $this->doCurl($result[$indice_call]["next_page"]);    // appel récursif de l'api
                        $indice_call++;
                        $curl_indice++;
                        $result[$indice_call] = json_decode($resultjson, true);
                    }
                }
                $metadata_array = $this->extract_data($result, $log_file, $output);
                $url_dcat = $node["url_dcat"];

                for ($i = 0; $i < count($metadata_array); $i++) {
                    // modif des specs : contournement impossibilité de mettre à jour l'uuid
                    $metadata_title = $metadata_array[$i]['title'];
                    $harvestuuid = $metadata_array[$i]["id"];
                    // 
                    $public_conn = $this->getSchemaConnection("public");
                    // modif des specs : contournement impossibilité de mettre à jour l'uuid 
                    $result = $public_conn->fetchAllAssociative(
                        "select * from metadata where title=:title",
                        array("title" => $metadata_title)
                    );

                    if (empty($result)) {
                        $metadata_array[$i]["mode"] = "insert";
                        $nb_insert++;
                    } else {
                        if ($result[0]["source"] === $source_uuid) {
                            $metadata_array[$i]["mode"] = "none";
                        } else {
                            $metadata_array[$i]["mode"] = "update";
                            $nb_update++;
                        }
                    }


                    $message = "\nMétadonnée courante :\n-title: " . $metadata_array[$i]['title'] . "\n-harvestesuuid: " . $metadata_array[$i]['id'];
                    $message .= "\n-mode: " . $metadata_array[$i]['mode'];

                    $this->write_log($log_file, $message, $output);
                    if ($metadata_array[$i]['mode'] !== "none") {
                        $this->write_log($log_file, "création ou récupération de la métadonnée au format ISO", $output);
                        // récupération de la métadonnée au format DCAT
                        if ($metadata_array[$i]['uri_inspire'] === "" || !strpos(
                                $metadata_array[$i]['uri_inspire'],
                                "http"
                            )) {
                            $metadata[$i] = $this->getDCATMetadataArray(
                                $url_dcat,
                                $metadata_array[$i],
                                $output,
                                $log_file
                            );
                            $extract_rdf = $metadata[$i]["extract_rdf"];
                            if ($extract_rdf !== "fail") {
                                // formattage des données 
                                $array_dcat_formated_data[$i] = $this->formatData_Dcat($metadata[$i]);
                                // conversion iso
                                $array_data_iso[$i] = $this->GetISOData(
                                    $array_dcat_formated_data[$i],
                                    $output,
                                    $log_file
                                );
                            } else {
                                $array_return_geosource['error'] = "erreur le contenu de la metadonnée n'a pas pu être récupéré";
                                $array_return_geosource['details'] = "erreur le contenu de la metadonnée n'a pas pu être récupéré";
                            }
                        } else {
                            $message .= "\n-uri inspire: " . $metadata_array[$i]['uri_inspire'];
                            $array_data_iso[$i] = $this->doCurl($metadata_array[$i]['uri_inspire']);
                        }
                    } else {
                        $this->write_log(
                            $log_file,
                            "--métadonnée présente dans le catalogue local, conversion ignorée",
                            $output
                        );
                        $array_return_geosource['success'] = "";
                        $array_return_geosource['message'] = "";
                        $array_return_geosource['details'] = "";
                        $nb_ignore++;
                    }
                    if ($metadata_array[$i]['mode'] === "insert" && $extract_rdf !== "fail") {
                        $this->write_log($log_file, "---------insertion en base de donnée", $output);
                        $iso_content = $array_data_iso[$i]["data_iso"];
                        $metadata_title = $metadata_array[$i]['title'];
                        $array_return_geosource = $this->createMetadata(
                            $metadata_template_id,
                            $metadata_title,
                            $iso_content,
                            $source_uuid,
                            $harvestuuid
                        );
                    } else {
                        if ($metadata_array[$i]['mode'] === "update" && $extract_rdf !== "fail") {
                            $this->write_log($log_file, "mise à jour de la métadonnée en base", $output);

                            // recherche de l'identifiant de la fiche de métadonnées
                            $metadata_title = $metadata_array[$i]['title'];
                            $metadata_id = $public_conn->fetchAllAssociative(
                                "select id from metadata where title=:title",
                                array("title" => $metadata_title)
                            )[0]["id"];
                            $iso_content = $array_data_iso[$i]["data_iso"];

                            $array_return_geosource = $this->updateMetada($metadata_id, $iso_content);
                        }
                    }
                    if (isset($array_return_geosource['success'])) {
                        $array_data_iso[$i]['statut_convertion'] = $array_return_geosource['success'];
                    }
                    if (isset($array_return_geosource['error']) && $array_return_geosource['error'] !== "") {
                        $array_error[$nb_error] = $array_return_geosource['error'];
                        $nb_error++;

                        if ($metadata_array[$i]['mode'] === "insert") {
                            $nb_insert--;
                        } else {
                            $nb_update--;
                        }
                    }
                    $this->write_log($log_file, $array_return_geosource['details'], $output);
                }
                $nb_success = $this->get_nb_success($array_data_iso) + $nb_ignore;

                $return_array["data_count"] = count($metadata_array);
                $return_array["data_nb_success"] = $nb_success;
                $return_array["data_ignored"] = $nb_ignore;
                $return_array["data_nb_failure"] = $return_array["data_count"] - $nb_success;

                if ($nb_update < 0) {
                    $nb_update = 0;
                }
                if ($nb_update < 0) {
                    $nb_insert = 0;
                }
                $return_array["nb_update"] = $nb_update;
                $return_array["nb_insert"] = $nb_insert;
                $return_array["node_params"] = $node_params;

                if ($return_array["data_nb_success"] === $return_array["data_count"]) {
                    $return_array["message"] = "Moissonnage terminé, aucune erreur n'a été relevée au cours du processus";
                } else {
                    $return_array["message"] = "Moissonnage terminé, des erreurs ont été rencontrées, veuillez consulter les logs";
                }
                $return_array["command_success"] = "SUCCESS";
                $return_array["success"] = "success";
            } else {
                // écriture message d'erreur
                $this->write_log($log_file, "unrecognized response: \n" . $resultjson, $output);
                $this->update_node_db($node_id, $file, "FAILURE");

                $return_array["command_success"] = "FAILURE";

                $return_array["data_count"] = 0;
                $return_array["data_nb_success"] = 0;
                $return_array["data_ignored"] = 0;
                $return_array["data_nb_failure"] = 0;

                $return_array["nb_update"] = 0;
                $return_array["nb_insert"] = 0;
                $return_array["node_params"] = $node_params;
                $array_data_iso = array();
                $nb_ignore = 0;
                $return_array["message"] = "Moissonnage terminé sans erreur, aucune donnée n'a pas pu être moissonnée ";
                $return_array["success"] = "success";
            }

            $return_array["http_code"] = 200;
            $return_array["OK"] = "OK";
        } else {
            // écriture message d'erreur
            $this->write_log($log_file, "Error node_id " . $node_id . " does not exists", $output);
            $return_array["success"] = "success";
            $return_array["command_success"] = "FAILURE";
            $return_array["http_code"] = 404;
            $return_array["OK"] = "KO";
            $return_array["message"] = "Error node_id " . $node_id . " does not exists";
            $return_array["data_count"] = 0;
            $return_array["data_nb_success"] = 0;
            $return_array["data_ignored"] = 0;
            $return_array["data_nb_failure"] = 0;
            $return_array["nb_update"] = 0;
            $return_array["nb_insert"] = 0;
            $request_url = "";
            $array_data_iso = array();
            $nb_ignore = 0;
        }


        $return_array["log_file"] = $log_file;
        $return_array["finish_time"] = date("d/m/Y - H:i:s");
        $return_array["request_url"] = $request_url;

        // détermine le nombre de données pour lesquelles la conversion est complete
        $nb_success = $this->get_nb_success($array_data_iso) + $nb_ignore;

        $message = $this->buildMessageReturn($return_array);

        if (!empty($array_error)) {
            $message .= "\n\n----------------------------------------------- Erreurs rencontrées lors du moissonnage -----------------------------------------------";
            for ($i = 0; $i < count($array_error); $i++) {
                $message .= "\n" . $array_error[$i];
            }
            $message .= "\n---------------------------------------------------------------------------------------------------------------------------------------\n";
        }

        $this->write_log($log_file, $message, $output);

        // $file
        $this->update_node_db($node_id, $file);
        $output->writeln(json_encode($return_array));
        return Command::SUCCESS;
    }
    // -----------------------------------------------------------------------------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------------

    // retourne un tableau d'id 
    protected function extract_data($data_input)
    {
        $data_output = array();
        $j = 0;
        for ($i = 0; $i < count($data_input); $i++) {
            for ($k = 0; $k < count($data_input[$i]['data']); $k++) {
                $data_output[$j]["id"] = $data_input[$i]['data'][$k]['id'];                 // récupération de l'id
                $extras = $data_input[$i]['data'][$k]['extras'];                            // récupération de l'identifiant inspire:identifier

                if (isset($extras["inspire:identifier"])) {
                    $data_output[$j]["id_inspire"] = $extras["inspire:identifier"];
                } else {
                    $data_output[$j]["id_inspire"] = $this->buildInspire_Id($data_output[$j]["id"]);
                }

                if (isset($extras["inspire:resource_identifier"])) {
                    $data_output[$j]["uri_inspire"] = $extras["inspire:resource_identifier"];
                } else {
                    $data_output[$j]["uri_inspire"] = "";
                }

                $data_output[$j]["title"] = $data_input[$i]['data'][$k]['title'];           // récupération du titre 
                $j++;
            }
        }
        return $data_output;
    }

    // construction de l'uuid inspire 
    protected function buildInspire_Id($metadata_id)
    {
        $inspire_id = "";

        $digit_1 = "0000" . "0000";
        $digit_2 = substr($metadata_id, 0, 4);
        $digit_3 = substr($metadata_id, 4, 4);
        $digit_4 = substr($metadata_id, 8, 4);
        $digit_5 = substr($metadata_id, 12, 4);
        $digit_6 = substr($metadata_id, 16);

        $inspire_id .= $digit_1 . "-" . $digit_2 . "-" . $digit_3 . "-" . $digit_4 . "-" . $digit_5 . $digit_6;

        return $inspire_id;
    }

    // construction de l'url cible
    protected function getFullUrl($node, $node_params)
    {
        $params_key_value = "";
        if (isset($node_params) && !empty($node_params)) {
            // formattage de la requete 
            for ($i = 0; $i < count($node_params); $i++) {
                if ($i === 0) {
                    $node_params[$i]["key"] = "?sort=-last_update&" . $node_params[$i]["key"] . "=";
                } else {
                    $node_params[$i]["key"] = "&" . $node_params[$i]["key"] . "=";
                }
                if (strpos($node_params[$i]["value"], ",")) {
                    $node_params[$i]["value"] = str_replace(",", "%7C", $node_params[$i]["value"]);
                    $node_params[$i]["value"] = str_replace(" ", "", $node_params[$i]["value"]);
                }
                $params_key_value .= $node_params[$i]["key"] . $node_params[$i]["value"];
            }
        }
        $request_url = $node["api_url"] . $params_key_value;

        return $request_url;
    }

    // récupération des métadonnées au format DCAT contenues au sein des fichiers {id}/rdf.xml
    protected function getDCATMetadataArray($url_dcat, $metadata_array, $output, $log_file)
    {
        $current_meta_id = $metadata_array["id"];
        $target_url = $url_dcat . $current_meta_id . "/rdf.xml";

        $message = "---rdf access point: " . $target_url;
        $this->write_log($log_file, $message, $output);
        $metadata_array["meta_dcat"] = $this->doCurl($target_url);

        if (strpos($metadata_array["meta_dcat"], "Error") !== false || strpos(
                $metadata_array["meta_dcat"],
                "<!DOCTYPE html>"
            ) !== false) {
            $metadata_array["extract_rdf"] = "fail";
            $message = "-----Unable to access rdf file \n-----error message: " . $metadata_array["meta_dcat"];
        } else {
            $metadata_array["extract_rdf"] = "success";
            $message = "-----extract data from rdf access point---> OK";
        }
        $this->write_log($log_file, $message, $output);

        return $metadata_array;
    }

    // prends le tableau précédent et construit la convertion de chaque métadonnée au format ISO19139
    protected function formatData_Dcat($metada_array)
    {
        $array_meta = array();

        $string_xml = $metada_array['meta_dcat'];
        $array_meta_description = $this->split_string_by_description($string_xml);
        $array_meta['id'] = $metada_array['id'];
        for ($k = 0; $k < count($array_meta_description); $k++) {
            $xml_portion = $this->get_start_tag($array_meta_description[$k]);

            if (strpos($xml_portion, '<skos') !== false) {
                $balise_ouvrante = strpos($xml_portion, '<skos');
                $balise_fermante = strpos($xml_portion, "</skos");
                $pos_chevron_balise_fermante = strpos($xml_portion, ">", $balise_fermante) + 1;

                $skos_string = substr($xml_portion, $balise_ouvrante, $pos_chevron_balise_fermante - $balise_ouvrante);
                $xml_portion = str_replace($skos_string, "", $xml_portion);
            }

            $dom = new \DOMDocument();
            $dom->loadXML($xml_portion);
            $descriptions = $dom->getElementsByTagNameNS('*', '*');
            $param_array = array();
            foreach ($descriptions as $desc) {
                if ($desc->localName !== "RDF") {
                    if (!isset($param_array[$desc->localName])) {
                        $param_array[$desc->localName] = array();
                    }
                    if (!isset($param_array[$desc->localName]["value"])) {
                        $param_array[$desc->localName]["value"] = "";
                    }
                    if ($param_array[$desc->localName]["value"] === "") {
                        $param_array[$desc->localName]["value"] .= $desc->nodeValue;
                    } else {
                        $param_array[$desc->localName]["value"] .= ", " . $desc->nodeValue;
                    }

                    if ($desc->hasAttributes()) {
                        $param_array[$desc->localName]["attributs"] = array();
                        $attributs_values = array();
                        foreach ($desc->attributes as $attr) {
                            if (!isset($attributs_values[$attr->localName])) {
                                $attributs_values[$attr->localName] = "";
                            }
                            if ($attributs_values[$attr->localName] === "") {
                                $attributs_values[$attr->localName] .= $attr->nodeValue;
                            } else {
                                $attributs_values[$attr->localName] .= ", " . $attr->nodeValue;
                            }
                        }
                        $param_array[$desc->localName]["attributs"] = $attributs_values;
                    }
                }
            }
            $array_meta['dcat_data'][$k]["arguments"] = $param_array;
        }

        return $array_meta;
    }

    // découpe le fichier xml au format DCAT en retournant un tableau
    protected function split_string_by_description($str_rdf)
    {
        $str_rdf = str_replace("\n", "", $str_rdf);
        $str_rdf = str_replace("\t", "", $str_rdf);

        $array_desc = explode("</rdf:Description>", $str_rdf);

        for ($i = 0; $i < count($array_desc); $i++) {
            if ($i === 0) {
                $indice_start = stripos($array_desc[0], "<rdf:Description");
                $intro = substr($array_desc[0], $indice_start, strlen($str_rdf) - $indice_start);
                $array_desc[0] = $intro;
            }
            $array_desc[$i] = str_replace("\n", "", $array_desc[$i]);
            $array_desc[$i] .= "</rdf:Description>";

            if ($i === count($array_desc) - 1) {
                unset($array_desc[$i]);
            }
        }
        return $array_desc;
    }

    // fonction permettant executant l'ensemble du process de convertion : en entrée le retour de formatData_Dcat()
    protected function GetISOData($array_dcat_formated_data, $output, $log_file)
    {
        $array_iso_data = array();
        $string_iso = ""; // initialisation string         

        $meta_id = $array_dcat_formated_data['id'];
        $string_iso .= $this->init_string($meta_id);

        if (isset($array_dcat_formated_data['dcat_data'])) {
            $data = $array_dcat_formated_data ['dcat_data'];
            $publisher_array = $this->ExtractPublisher($data);
            $string_iso .= $this->build_contact_section($publisher_array);
            $string_iso .= $this->get_gmd_datestamp();

            $datasets = $this->ExtractDataSets($meta_id, $data);

            $string_iso .= $this->get_gmd_identification($datasets, $publisher_array);
            $online_resources = $this->ExtractOnlineRessources($data, $meta_id);
            $string_iso .= $this->get_gmd_distributionInfo($online_resources);

            // recopie des valeurs pour return tableau complet
            $array_iso_data['data_iso'] = $string_iso;
            $array_iso_data['statut_convertion'] = "success";
            $message = "---------Succcès de la conversion de : " . $array_dcat_formated_data ['id'];
        } else {
            $array_iso_data['statut_convertion'] = "fail";
            $message = "---------Erreur lors de la conversion de: " . $array_dcat_formated_data ['id'] . " fichier rdf non reconnu";
        }

        $this->write_log($log_file, $message, $output);
        $array_iso_data['id'] = $array_dcat_formated_data ['id'];

        return $array_iso_data;
    }

    // retourne la premiere partie de la chaine xml au format ISO 
    protected function init_string($meta_id)
    {
        $str = "<gmd:MD_Metadata xmlns:gml='http://www.opengis.net/gml' xmlns:gmd='http://www.isotc211.org/2005/gmd'" . "\n\txmlns:srv='http://www.isotc211.org/2005/srv'" . "\n\txmlns:gfc='http://www.isotc211.org/2005/gfc'" . "\n\txmlns:gco='http://www.isotc211.org/2005/gco'" . "\n\txmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'" . "\n\txmlns:xlink='http://www.w3.org/1999/xlink'" . "\n\txmlns:gmx='http://www.isotc211.org/2005/gmx'" . "\n\txmlns:gts='http://www.isotc211.org/2005/gts'>"
            . "\n\t<gmd:fileIdentifier>" . "\n\t\t<gco:CharacterString>" . $meta_id . "</gco:CharacterString>" . "\n\t</gmd:fileIdentifier>"
            . "\n\t<gmd:language>" . "\n\t\t<gmd:LanguageCode codeList='http://www.loc.gov/standards/iso639-2/' codeListValue='fre'/>" . "\n\t</gmd:language>"
            . "\n\t<gmd:characterSet>" . "\n\t\t<gmd:MD_CharacterSetCode codeListValue='utf8' codeList='http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_CharacterSetCode'/>" . "\n\t</gmd:characterSet>"
            . "\n\t<gmd:hierarchyLevel>" . "\n\t\t<gmd:MD_ScopeCode codeList='http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#MD_ScopeCode' codeListValue='nonGeographicDataset'/>" . "\n\t</gmd:hierarchyLevel>"
            . "\n\t<gmd:hierarchyLevelName>" . "\n\t\t<gco:CharacterString>Série de données non géographiques</gco:CharacterString>" . "\n\t</gmd:hierarchyLevelName>";

        return $str;
    }

    // retourne la section contact de la chaine xml au format ISO
    protected function build_contact_section($publisher_array)
    {
        $onlineResource = "";
        $CharacterString = "";
        if (isset($publisher_array["name"]) && $publisher_array["name"] !== "") {
            $CharacterString = "\n\t\t\t<gmd:organisationName>\n\t\t\t\t<gco:CharacterString>" . $publisher_array["name"] . "</gco:CharacterString>\n\t\t\t</gmd:organisationName>";
        }

        if (isset($publisher_array["homepage"]) && $publisher_array["homepage"] !== "") {
            $onlineResource = "\n\t\t\t\t\t<gmd:onlineResource>" . "\n\t\t\t\t\t\t<gmd:CI_OnlineResource>" . "\n\t\t\t\t\t\t\t<gmd:linkage>\n\t\t\t\t\t\t\t\t<gmd:URL>" . $publisher_array["homepage"] . "</gmd:URL>\n\t\t\t\t\t\t\t</gmd:linkage>" . "\n\t\t\t\t\t\t</gmd:CI_OnlineResource>\n\t\t\t\t\t</gmd:onlineResource>";
        }

        $str = "\n\t<gmd:contact>"
            . "\n\t\t<gmd:CI_ResponsibleParty>"
            . $CharacterString
            . "\n\t\t\t<gmd:contactInfo>\n\t\t\t\t<gmd:CI_Contact>"
            . $onlineResource
            . "\n\t\t\t\t</gmd:CI_Contact>\n\t\t\t</gmd:contactInfo>
                \n\t\t\t<gmd:role>\n\t\t\t\t<gmd:CI_RoleCode codeList='http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode' codeListValue='pointOfContact'/>\n\t\t\t</gmd:role>"
            . "\n\t\t</gmd:CI_ResponsibleParty>"
            . "\n\t</gmd:contact>";

        return $str;
    }

    // retourne la section datestamp de la chaine xml au format ISO
    protected function get_gmd_datestamp()
    {
        $datetime = date("Y-m-d") . "T" . date("H:i:s");

        $str = "\n\t<gmd:dateStamp>\n\t\t<gco:DateTime>" . $datetime . "</gco:DateTime>\n\t</gmd:dateStamp>"
            . "\n\t<gmd:metadataStandardName>\n\t\t<gco:CharacterString>ISO 19115:2003/19139</gco:CharacterString>\n\t</gmd:metadataStandardName>"
            . "\n\t<gmd:metadataStandardVersion>\n\t\t<gco:CharacterString>1.0</gco:CharacterString>\n\t</gmd:metadataStandardVersion>";

        return $str;
    }

    // construction du tableau contenant l'ensemble des informations de l'organisme ayant publié la ressource 
    protected function ExtractPublisher($array_data)
    {
        $publisher_array = array();
        $publisher = array();

        for ($i = 0; $i < count($array_data); $i++) {
            // recherche du publisher url 
            foreach ($array_data[$i]["arguments"] as $key => $data) {
                if ($key === "publisher") {
                    foreach ($data as $datakey => $datakey_value) {
                        if (gettype($datakey_value) === "array") {
                            $publisher_url = $datakey_value["resource"];
                        }
                    }
                }
            }
            // extraction des données
            foreach ($array_data[$i]["arguments"] as $key => $data) {
                if ($key === "Description") {
                    foreach ($data as $datakey => $datakey_value) {
                        if (gettype($datakey_value) === "array") {
                            if (isset($datakey_value["about"]) && isset($publisher_url)) {
                                if ($datakey_value["about"] === $publisher_url) {
                                    $publisher = $array_data[$i]["arguments"];
                                }
                            }
                        }
                    }
                }
            }
        }

        // formattage des données 
        if (isset($publisher)) {
            foreach ($publisher as $publisher_key => $value) {
                $publisher_array[$publisher_key] = $value["value"];
            }
        }

        return $publisher_array;
    }

    // construction du tableau dataset 
    protected function ExtractDataSets($data_id, $array_data)
    {
        $array_dataset = array();
        for ($i = 0; $i < count($array_data); $i++) {
            foreach ($array_data[$i]["arguments"] as $key => $data) {
                if ($key === "Description") {
                    foreach ($data as $datakey => $datakey_value) {
                        if (gettype($datakey_value) === "array") {
                            if (isset($datakey_value["about"])) {
                                if (strpos($datakey_value["about"], $data_id) !== false) {
                                    if (strpos($datakey_value["about"], "#resource") === false) {
                                        $array_dataset = $array_data[$i]["arguments"];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $array_dataset;
    }

    // retourne la section identification de la chaine xml au format ISO
    protected function get_gmd_identification($datasets, $publisher)
    {
        $str = "\n\t<gmd:identificationInfo>\n\t\t<gmd:MD_DataIdentification>";
        $str .= $this->get_gmd_citation($datasets);
        $str .= $this->get_gmd_abstract($datasets);
        $str .= $this->get_gmd_point_of_contact($publisher);
        $str .= $this->get_gmd_descriptive_keyword($datasets);
        $str .= $this->get_gmd_language();

        $str .= "\n\t\t</gmd:MD_DataIdentification>\n\t</gmd:identificationInfo>";

        return $str;
    }

    // retourne la section citation de la chaine xml au format ISO
    protected function get_gmd_citation($datasets)
    {
        $mettadata_title = $datasets['title']['value'];
        $meta_date_update = $datasets['modified']['value'];
        $statut_update = "revision";
        $meta_date_creation = $datasets['issued']['value'];
        $statut_creation = "creation";

        $str = "\n\t\t\t<gmd:citation>\n\t\t\t\t<gmd:CI_Citation>"
            . "\n\t\t\t\t\t<gmd:title>\n\t\t\t\t\t\t<gco:CharacterString>" . $mettadata_title . "</gco:CharacterString>\n\t\t\t\t\t</gmd:title>"
            . "\n\t\t\t\t\t<gmd:date>\n\t\t\t\t\t\t" .
            "<gmd:CI_Date>"
            . "\n\t\t\t\t\t\t\t<gmd:date>\n\t\t\t\t\t\t\t\t<gco:Date>" . $meta_date_update . "</gco:Date>\n\t\t\t\t\t\t\t</gmd:date>"
            . "\n\t\t\t\t\t\t\t<gmd:dateType>\n\t\t\t\t\t\t\t\t<gmd:CI_DateTypeCode codeList='http:standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode' codeListValue='" . $statut_update . "'/>\n\t\t\t\t\t\t\t</gmd:dateType>"
            . "\n\t\t\t\t\t\t" .
            "</gmd:CI_Date>\n\t\t\t\t\t" .
            "</gmd:date>"
            . "\n\t\t\t\t\t<gmd:date>\n\t\t\t\t\t\t<gmd:CI_Date>"
            . "\n\t\t\t\t\t\t\t<gmd:date>\n\t\t\t\t\t\t\t\t<gco:Date>" . $meta_date_creation . "</gco:Date>\n\t\t\t\t\t\t\t\t</gmd:date>"
            . "\n\t\t\t\t\t\t\t<gmd:dateType>\n\t\t\t\t\t\t\t\t<gmd:CI_DateTypeCode codeList='http:standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_DateTypeCode' codeListValue='" . $statut_creation . "'/>\n\t\t\t\t\t\t\t</gmd:dateType>" .
            "\n\t\t\t\t\t\t</gmd:CI_Date>" . "\n\t\t\t\t\t</gmd:date>"
            . "\n\t\t\t\t</gmd:CI_Citation>\n\t\t\t</gmd:citation>";

        return $str;
    }

    // retourne la section abstract de la chaine xml au format ISO
    protected function get_gmd_abstract($datasets)
    {
        $CharacterString = $datasets["description"]["value"];
        $str = "\n\t\t\t<gmd:abstract>" . "\n\t\t\t\t<gco:CharacterString>" . $CharacterString . "</gco:CharacterString>" . "\n\t\t\t</gmd:abstract>";

        return $str;
    }

    // retourne la section pointofcontact de la chaine xml au format ISO
    protected function get_gmd_point_of_contact($publisher)
    {
        $code_liste = "publisher";
        $onlineResource = "";
        $CharacterString = "";

        if (isset($publisher["name"]) && $publisher["name"] !== "") {
            $CharacterString = "\n\t\t\t<gmd:organisationName>\n\t\t\t\t<gco:CharacterString>" . $publisher["name"] . "</gco:CharacterString>\n\t\t\t</gmd:organisationName>";
        }
        if (isset($publisher["homepage"]) && $publisher["homepage"] !== "") {
            $onlineResource = "\n\t\t\t\t\t<gmd:onlineResource>" . "\n\t\t\t\t\t\t<gmd:CI_OnlineResource>" . "\n\t\t\t\t\t\t\t<gmd:linkage>\n\t\t\t\t\t\t\t\t<gmd:URL>" . $publisher["homepage"] . "</gmd:URL>\n\t\t\t\t\t\t\t</gmd:linkage>" . "\n\t\t\t\t\t\t</gmd:CI_OnlineResource>\n\t\t\t\t\t</gmd:onlineResource>";
        }

        $str = "\n\t\t\t<gmd:pointOfContact>"
            . "\n\t\t\t\t<gmd:CI_ResponsibleParty>"
            . $CharacterString
            . "\n\t\t\t\t\t<gmd:contactInfo>"
            . "\n\t\t\t\t\t\t<gmd:CI_Contact>"
            . $onlineResource
            . "\n\t\t\t\t\t\t</gmd:CI_Contact>"
            . "\n\t\t\t\t\t</gmd:contactInfo>"
            . "\n\t\t\t\t\t<gmd:role>" . "\n\t\t\t\t\t\t<gmd:CI_RoleCode codeList='http:standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_RoleCode' codeListValue='" . $code_liste . "'/>" . "\n\t\t\t\t\t</gmd:role>"
            . "\n\t\t\t\t</gmd:CI_ResponsibleParty>"
            . "\n\t\t\t</gmd:pointOfContact>";

        return $str;
    }

    // retourne la section descriptiveKeywords de la chaine xml au format ISO
    protected function get_gmd_descriptive_keyword($datasets)
    {
        $string_keywords = "";
        if (isset($datasets["keyword"])) {
            $array_keywords = explode(", ", $datasets["keyword"]["value"]);

            if (!empty($array_keywords)) {
                $string_keywords = "\n\t\t\t<gmd:descriptiveKeywords>\n\t\t\t\t<gmd:MD_Keywords>";
                // boucle sur les keywords 
                for ($i = 0; $i < count($array_keywords); $i++) {
                    $string_keywords .= "\n\t\t\t\t\t<gmd:keyword>\n\t\t\t\t\t\t<gco:CharacterString>" . $array_keywords[$i] . "</gco:CharacterString>\n\t\t\t\t\t</gmd:keyword>";
                }
                $string_keywords .= "\n\t\t\t\t</gmd:MD_Keywords>\n\t\t\t</gmd:descriptiveKeywords>";
            }
        }

        return $string_keywords;
    }

    // retourne la section language de la chaine xml au format ISO
    protected function get_gmd_language()
    {
        $locale_str = "\n\t\t\t<gmd:language>\n\t\t\t\t<gco:CharacterString>fre</gco:CharacterString>\n\t\t\t</gmd:language>";

        return $locale_str;
    }

    // construit le tableau contenant l'ensemble des informations associées aux ressources
    protected function ExtractOnlineRessources($array_data, $data_id)
    {
        $array_online_resources = array();
        $k = 0;
        for ($i = 0; $i < count($array_data); $i++) {
            foreach ($array_data[$i]["arguments"] as $key => $data) {
                if ($key === "Description") {
                    foreach ($data as $datakey => $datakey_value) {
                        if (gettype($datakey_value) === "array") {
                            if (isset($datakey_value["about"])) {
                                if (strpos($datakey_value["about"], $data_id) !== false) {
                                    if (strpos($datakey_value["about"], "#resource") !== false) {
                                        $array_online_resources[$k] = $array_data[$i]["arguments"];
                                        $k++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $array_online_resources;
    }

    // retourne la section distributionInfo de la chaine xml au format ISO
    protected function get_gmd_distributionInfo($online_resources)
    {
        $str = "";
        if (!empty($online_resources)) {
            $protocol = "WWW:LINK-1.0-http--link";
            $str = "\n\t<gmd:distributionInfo>\n\t\t<gmd:MD_Distribution>\n\t\t\t<gmd:transferOptions>\n\t\t\t\t<gmd:MD_DigitalTransferOptions>";
            for ($i = 0; $i < count($online_resources); $i++) {
                $str .= "\n\t\t\t\t\t<gmd:onLine>\n\t\t\t\t\t\t<gmd:CI_OnlineResource>";

                $str .= "\n\t\t\t\t\t\t\t<gmd:linkage>\n\t\t\t\t\t\t\t\t<gmd:URL>" . $online_resources[$i]['downloadURL']['attributs']['resource'] . "</gmd:URL>\n\t\t\t\t\t\t\t</gmd:linkage>"
                    . "\n\t\t\t\t\t\t\t<gmd:protocol>\n\t\t\t\t\t\t\t\t<gco:CharacterString>" . $protocol . "</gco:CharacterString>\n\t\t\t\t\t\t\t</gmd:protocol>"
                    . "\n\t\t\t\t\t\t\t<gmd:name>\n\t\t\t\t\t\t\t\t<gco:CharacterString>" . $online_resources[$i]['title']['value'] . "</gco:CharacterString>\n\t\t\t\t\t\t\t</gmd:name>"
                    . "\n\t\t\t\t\t\t\t<gmd:description>\n\t\t\t\t\t\t\t\t<gco:CharacterString>" . $online_resources[$i]['description']['value'] . "</gco:CharacterString>\n\t\t\t\t\t\t\t</gmd:description>";

                $str .= "\n\t\t\t\t\t\t</gmd:CI_OnlineResource>\n\t\t\t\t\t</gmd:onLine>";
            }
            $str .= "\n\t\t\t\t</gmd:MD_DigitalTransferOptions>" . "\n\t\t\t</gmd:transferOptions>" . "\n\t\t</gmd:MD_Distribution>" .
                "\n\t</gmd:distributionInfo>\n</gmd:MD_Metadata>";
        }

        return $str;
    }

    // creation du fichier de log
    protected function create_log_file_path($file)
    {
        $container = $this->container;

        $log_path = "";

        $prodige_logs = $container->getParameter('PRODIGE_PATH_LOGS');

        $array_files = scandir($prodige_logs);
        $opendata_log_exist = false;
        for ($i = 0; $i < count($array_files); $i++) {
            $current_file = $array_files[$i];
            if (strpos($current_file, "opendata") !== false) {
                $opendata_log_exist = true;
            }
        }
        $repository_name = "/opendata";
        $repository_path = $prodige_logs . $repository_name;
        if (!$opendata_log_exist) {
            $create_repo = mkdir($repository_path, 0770);
            if ($create_repo) {
                $log_path = $repository_path . "/" . $file;
            }
        } else {
            $log_path = $repository_path . "/" . $file;
        }


        return $log_path;
    }

    // prefixe du log file
    protected function create_log_file($node_id = "")
    {
        $prefixe_file = "";
        if ($node_id !== "") {
            $prefixe_file = "node_" . $node_id . "_";
        }
        return $prefixe_file . "log" . date("Y") . date("m") . date("d") . "T" . date("H") . date("i") . date(
                "s"
            ) . ".log";
    }

    // supprimer les anciens fichiers de logs du noeud
    protected function delete_old_log_file($node_id)
    {
        $container = $this->container;
        $prodige_logs = $container->getParameter('PRODIGE_PATH_LOGS');
        $log_dir = $prodige_logs . "/opendata/";

        $array_files = scandir($log_dir);
        for ($i = 0; $i < count($array_files); $i++) {
            $current_file = $array_files[$i];
            if (strpos($current_file, "node_" . $node_id) !== false) {
                unlink($log_dir . $current_file);
            }
        }
    }

    // ecriture log
    protected function write_log($log_file, $message, $output)
    {
        $file = fopen($log_file, "a+");

        $message .= "\n";
        fwrite($file, $message);
        chmod($log_file, 0777);
        fclose($file);
    }

    // mise à jour de la base de donnée apres execution de la commande
    protected function update_node_db($node_id, $file, $status = "SUCCESS")
    {
        $conn = $this->getSchemaConnection($schema = "catalogue");

        //$log_file = "bundles/opendata/log_files/".$file;
        $container = $this->container;
        $prodige_logs = $container->getParameter('PRODIGE_PATH_LOGS');
        $repository_name = "/opendata";
        $repository_path = $prodige_logs . $repository_name;
        $log_file = $repository_path . "/" . $file;

        $sql = "UPDATE catalogue.harves_opendata_node SET node_log_file =:node_log_file WHERE pk_node_id = :node_id";
        $result = $conn->executeStatement($sql, array("node_id" => $node_id, "node_log_file" => $log_file));

        // mise à jour : catalogue.scheduled_command
        $last_execution_date = date("Y-m-d") . " " . date("H:i:s");
        $sql = "update catalogue.scheduled_command set last_execution =:last_execution, log_file=:log_file where arguments =:node_id";
        $result = $conn->executeStatement(
            $sql,
            array("last_execution" => $last_execution_date, "log_file" => $log_file, "node_id" => $node_id)
        );

        // récupération de l'id_command; 
        $sql = "select id from catalogue.scheduled_command where arguments =:node_id";
        $result = $conn->fetchAllAssociative($sql, array("node_id" => $node_id));
        if (!empty($result)) {
            $command_id = $result[0]['id'];
        } else {
            return $log_file;
        }


        $query = "select * from catalogue.execution_report where scheduled_command_id	=:command_id";
        $result = $conn->fetchAllAssociative($query, array("command_id" => $command_id));

        if (empty($result)) {
            $sql = "select max(id) from catalogue.execution_report";
            $result = $conn->fetchAllAssociative($sql);
            if (!empty($result)) {
                $report_id = $conn->fetchAllAssociative($sql)[0]["max"] + 1;
            } else {
                return $log_file;
            }


            // insert execution_report
            $sql = "insert into catalogue.execution_report (id, scheduled_command_id, date_time, file, status) values (:id, :scheduled_command_id, :date_time, :file, :status);";
            $conn->executeStatement(
                $sql,
                array(
                    "id" => $report_id,
                    "scheduled_command_id" => $command_id,
                    "date_time" => $last_execution_date,
                    "file" => $log_file,
                    "status" => $status
                )
            );
        } else {
            // mise à jour : execution_report
            $sql = "update catalogue.execution_report set date_time =:date_time, file=:file, status =:status where scheduled_command_id =:scheduled_command_id	";
            $result = $conn->executeStatement(
                $sql,
                array(
                    "date_time" => $last_execution_date,
                    "file" => $log_file,
                    "status" => $status,
                    "scheduled_command_id" => $command_id
                )
            );
        }


        return $log_file;
    }

    // ajout des tags xml acceptables
    protected function get_start_tag($data)
    {
        $str = '<?xml version="1.0" encoding="UTF-8"?>' . '<rdf:RDF
                xmlns:dcat="http://www.w3.org/ns/dcat#"
                xmlns:dct="http://purl.org/dc/terms/"
                xmlns:foaf="http://xmlns.com/foaf/0.1/"
                xmlns:ns1="http://spdx.org/rdf/terms#"
                xmlns:ns2="http://schema.org/"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">' . $data . '</rdf:RDF>';;

        return $str;
    }

    // retourne le nombre de moissonnage reussi
    protected function get_nb_success($array_data_iso)
    {
        $nb_success = 0;
        if (isset($array_data_iso)) {
            for ($i = 0; $i < count($array_data_iso); $i++) {
                if (strtolower($array_data_iso[$i]['statut_convertion']) === "success") {
                    $nb_success++;
                }
            }
        }
        return $nb_success;
    }

    // initialisation du template de metadata pour les données non geolocalisable
    protected function init_metadata_template($log_file, $output)
    {
        $conn = $this->getSchemaConnection('catalogue');

        $querry = "select prodige_settings_value from prodige_settings where prodige_settings_constant = 'PRO_NONGEO_METADATA_ID'";
        $result = $conn->fetchAllAssociative($querry);

        if (!empty($result)) {
            return $result[0]['prodige_settings_value'];
        } else {
            $this->write_log($log_file, "erreur la métadonnée modèle n'existe pas", $output);
        }

        return false;
    }


    // mise à jour via geonetwork des id et titres de la métadonnée
    protected function updateMetada($metadata_id, $metadata_new_data)
    {
        $container = $this->container;
        CurlUtilities::initialize(
            $container->getParameter("phpcli_default_login"),
            $container->getParameter("phpcli_default_password"),
            $container->getParameter("server_ca_cert_path")
        );
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
        $urlUpdateData = "md.edit.save";
        $formData = array(
            "id" => $metadata_id,
            "data" => $metadata_new_data,
        );

        // mise à jour du contenu 
        $post_result = $geonetwork->post($urlUpdateData, $formData);

        if (strpos($post_result, '{"error":') !== false) {
            $status = "fail";
            $jsonObj = $post_result;
            $message = "---------Error: Impossible de mettre à jour le contenu de la métadonnée\n";
        } else {
            $status = "success";
            $jsonObj = json_decode($post_result);
            $message = "---------Le contenu de la métadonnée à été correctement mis à jour";
        }
        $result["success"] = $status;
        $result["response"] = $jsonObj;
        $result["details"] = $message;

        return $result;
    }

    /*
     *  creation via geonetwork de la métadonnée
     */
    protected function createMetadata($metadata_template_id, $metadata_title, $iso_content, $source_uuid, $harvestuuid)
    {
        $result_array = array();

        $container = $this->container;
        CurlUtilities::initialize(
            $container->getParameter("phpcli_default_login"),
            $container->getParameter("phpcli_default_password"),
            $container->getParameter("server_ca_cert_path")
        );
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');

        $conn = $this->getSchemaConnection('public');

        $dao = new DAO($conn, 'public');
        $group_id = 1;
        $query = "select groupowner from public.metadata where id=:id";
        $rs = $dao->BuildResultSet($query, array("id" => $metadata_template_id));
        if ($rs->GetNbRows() > 0) {
            $rs->First();
            $group_id = $rs->Read(0);
        } else {
            $result_array["success"] = "fail";
            $result_array["error"] = "le modèle de métadonnée n'a pas été trouvé";
        }

        if ($group_id === "") {
            $group_id = 1;
        }

        // création de la métadonnée à partir du modèle
        $jsonResp = $geonetwork->get(
            'md.create?_content_type=json&id=' . $metadata_template_id . "&group=" . $group_id . "&template=n&child=n&fullPrivileges=false&harvested=y"
        );
        $jsonObj = json_decode($jsonResp);
        if ($jsonObj && isset($jsonObj->id)) {
            $metadata_id = $jsonObj->id;
        } else {
            $message = $jsonResp . "\nError : Impossible de créer la métadonnée à partir du modèle \n";
            throw new \Exception("Impossible de créer la métadonnée à partir du modèle");
        }

        $urlUpdateData = "md.edit.save";
        $iso_content = str_replace("&", "&amp;", $iso_content);
        $formData = array("id" => $metadata_id, "data" => $iso_content, "title" => $metadata_title);

        // mise à jour du contenu 
        $post_result = $geonetwork->post($urlUpdateData, $formData);
        if (strpos($post_result, '{"error":') !== false) {
            $status = "fail";
            $jsonObj = $post_result;
            $message = "Error: Impossible de mettre à jour le contenu de la métadonnée \n";
            $result_array["error"] = "la métadonnée '" . $metadata_title . "' n'a pas été ajoutée";

            // supprimer la fiche de métadonnée
            $geonetwork->get("md.delete?id=" . $metadata_id);
        } else {
            $message = "---------Le contenu de la métadonnée à été correctement mis à jour\n";

            $querry = "update metadata set title=:title, isharvested =:isharvested,harvestuuid=:harvestuuid	  where id =:id";
            $conn->executeStatement(
                $querry,
                array(
                    "title" => $metadata_title,
                    "isharvested" => 'y',
                    "harvestuuid" => $harvestuuid,
                    "id" => $metadata_id
                )
            );
            // -----------------------------------

            // mise à jour des privilèges
            $jsonResp = $geonetwork->get('md.privileges.update?_content_type=json&_1_0=on&id=' . $metadata_id);
            $jsonObj = json_decode($jsonResp);
            if ($jsonObj && $jsonObj [0]) {
                $status = "success";
                $message .= "---------Privilèges mis à jour\n";

                // mise à jour de la base de données
                $this->updata_metada_in_db($source_uuid, $metadata_id);
                // publication de la fiche
                $message .= "---------Publication de la métadonnée :" . $metadata_id . "\n";
                $this->publish_metadata($metadata_id);
                // indexation de la fiche -> valide l'affectation au catalogue
                $result = $conn->fetchAllAssociative(
                    "select uuid from public.metadata where id =:id",
                    array("id" => $metadata_id)
                );
                $inspire_uuid = $result[0]["uuid"];
                $message .= "---------identifiant de la métadonnée: " . $inspire_uuid . "\n";
                $message .= $this->rebuilt_index($inspire_uuid);
                $result_array["error"] = "";
            } else {
                $status = "fail";
                $message .= "\n-------------Privilèges non mis à jour";
            }
        }

        $result_array["success"] = $status;
        $result_array["response"] = $jsonObj;
        $result_array["details"] = $message;

        return $result_array;
    }

    // retourne l'uuid du catalogue
    function get_source_uuid($node_id)
    {
        $conn = $this->getSchemaConnection('catalogue');

        // récupération de l'uuid du catalogue
        $node_config = $conn->fetchAllAssociative(
            "select node_name from harves_opendata_node where pk_node_id =:node_id",
            array("node_id" => $node_id)
        );
        $source_name = $node_config[0]["node_name"];
        $public_conn = $this->getSchemaConnection('public');
        $source_conf_array = $public_conn->fetchAllAssociative(
            "select uuid from sources where name =:name",
            array("name" => $source_name)
        );
        $source_uuid = $source_conf_array[0]["uuid"];


        return $source_uuid;
    }


    // retourne l'uuid du catalogue local
    function get_local_source_uuid()
    {
        $public_conn = $this->getSchemaConnection('public');
        $local_source_uuid = $public_conn->fetchAllAssociative(
            "select source from metadata where isharvested = 'n' limit 1"
        )[0]['source'];
        return $local_source_uuid;
    }


    /* Indexation de la fiche de métadonnées
     * 
     * 
     */
    function rebuilt_index($metadata_uuid)
    {
        $message = "---------indexation de la métadonnée en cours \n";

        $geonetwork_url = PRO_GEONETWORK_URLBASE . 'srv/api/0.1/records/index?uuids=' . $metadata_uuid;
        $container = $this->container;

        $user_login = $container->getParameter("phpcli_default_login");
        $user_pwd = $container->getParameter("phpcli_default_password");
        $basicAuth = array($user_login, $user_pwd);

        $headers = array();
        $headers[] = "Accept: application/json";

        $result = $this->doCurl($geonetwork_url, true, $basicAuth, array(CURLOPT_HTTPHEADER => $headers));
        if (!$result) {
            $message .= "---------indexation de la métadonnée terminé : result failure - rechargez l'index manuellement \n";
        } else {
            $message .= "---------indexation de la métadonnée terminé : result success \n";
        }

        return $message;
    }

    // publication de la fiche de métadonnées
    function publish_metadata($metadata_id)
    {
        $geonetwork_url = PRO_GEONETWORK_URLBASE . 'srv/api/records/' . $metadata_id . '/sharing';
        $container = $this->container;

        $user_login = $container->getParameter("phpcli_default_login");
        $user_pwd = $container->getParameter("phpcli_default_password");
        $basicAuth = array($user_login, $user_pwd);
        $curl_return = $this->doCurl($geonetwork_url, true, $basicAuth);
    }

    // forcer la mise à jour des titres et id de la métadonnées directement en base
    function updata_metada_in_db($source_uuid, $metadata_id)
    {
        $conn = $this->getSchemaConnection('public');
        $querry = "update metadata set source =:source_uuid where id =:metadata_id";
        $conn->executeStatement($querry, array("source_uuid" => $source_uuid, "metadata_id" => $metadata_id));
    }

    // construction du tableau de retour dans le cas d'une erreur
    function buildErrorReturn($log_file, $request_url, $message = "Erreur impossible de moissonner cette url")
    {
        $return_array = array();

        $return_array["success"] = "success";
        $return_array["command_success"] = "FAILURE";
        $return_array["http_code"] = 404;
        $return_array["OK"] = "KO";
        $return_array["message"] = $message;
        $return_array["data_count"] = 0;
        $return_array["data_nb_success"] = 0;
        $return_array["data_ignored"] = 0;
        $return_array["data_nb_failure"] = 0;

        $return_array["nb_update"] = 0;
        $return_array["nb_insert"] = 0;
        $return_array["log_file"] = $log_file;
        $return_array["finish_time"] = date("d/m/Y - H:i:s");
        $return_array["request_url"] = $request_url;


        return $return_array;
    }

    // construction du message apparaissant dans les logs 
    function buildMessageReturn($return_array)
    {
        $message = "\n\n";
        $message .= "\n----------------------------- STATUT : " . $return_array["success"];
        $message .= "\n----------------------------- HTTP return code : " . $return_array["http_code"];
        $message .= "\n----------------------------- heure de fin d'éxecution : " . $return_array["finish_time"];
        $message .= "\n----------------------------- requête http générée  : " . $return_array["request_url"];
        $message .= "\n----------------------------- nombre de fiches détectées : " . $return_array["data_count"];
        $message .= "\n----------------------------- nombre de fiches moissonnées : " . $return_array["data_nb_success"];
        $message .= "\n----------------------------- nombre de fiches créées : " . $return_array["nb_insert"];
        $message .= "\n----------------------------- nombre de fiches mises à jours : " . $return_array["nb_update"];
        $message .= "\n----------------------------- nombre d'erreurs rencontrées : " . $return_array["data_nb_failure"];
        $message .= "\n----------------------------- message : " . $return_array["message"];
        $message .= "\n----------------------------- fichier de log : " . $return_array["log_file"];

        return $message;
    }
}

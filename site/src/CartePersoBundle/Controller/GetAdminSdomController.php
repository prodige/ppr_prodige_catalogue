<?php

namespace ProdigeCatalogue\CartePersoBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\DAOProxy\DAO;

/**
 * @author alkante
 * #from /PRRA/Services/getAdminSdom.php
 */
class GetAdminSdomController extends BaseController {
    /**
     *
     * @IsGranted("ROLE_USER")
     * @Route("/carteperso/getAdminsdom", name="catalogue_carteperso_getAdminsdom", options={"expose"=true})
     */
    public function getAdminsdomAction(Request $request) {

        //include_once("../parametrage.php");
        //$AdminPath = "../Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");

        //$dao = new DAO();
        //$dao->setSearchPath("public, ".PRO_SCHEMA_NAME);
        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue, '.PRO_SCHEMA_NAME);

        $callback = $request->query->get("callback", "");

        $ssdom_id = $request->query->get("ssdom", -1);
        if($ssdom_id == -1) {
            //aucun sous domaine selectionne
            $strSql = "SELECT uti.pk_utilisateur, uti.usr_nom, uti.usr_prenom, uti.usr_email,
                       array_to_string(array_agg(adm.ssdom_nom), ',') as sdom_nom, array_to_string(array_agg(adm.pk_sous_domaine), ',') as sdom_ids
                       FROM catalogue.utilisateur uti, catalogue.ADMINISTRATEURS_SOUS_DOMAINE adm
                       WHERE adm.pk_utilisateur=uti.pk_utilisateur
                       GROUP BY uti.pk_utilisateur, uti.usr_nom, uti.usr_prenom, uti.usr_email order by uti.usr_nom";
            $rs_tables = $dao->BuildResultSet($strSql);
            $result = array();
            for($rs_tables->First(); !$rs_tables->EOF(); $rs_tables->Next()) {
                $data = array();
                $data["pk_utilisateur"] = $rs_tables->Read(0);
                $data["displayField"] = $rs_tables->Read(2)." ".$rs_tables->Read(1)." (".$rs_tables->Read(3).")";
                $data["pk_sous_domaine"] = $rs_tables->Read(5);
                $data["ssdom_nom"] = $rs_tables->Read(4);
                array_push($result, $data);
            }
        } else {
            //sous domaine selectionne
            //On recupere en premier lieu les administrateurs du sous domaine renseigne
            $strSql = "SELECT uti.pk_utilisateur, uti.usr_nom, uti.usr_prenom, uti.usr_email,
                       array_to_string(array_agg(adm.ssdom_nom), ',') as sdom_nom, array_to_string(array_agg(adm.pk_sous_domaine), ',') as sdom_ids
                       FROM catalogue.utilisateur uti, catalogue.ADMINISTRATEURS_SOUS_DOMAINE adm
                       WHERE adm.pk_utilisateur=uti.pk_utilisateur and adm.pk_sous_domaine=?
                       GROUP BY uti.pk_utilisateur, uti.usr_nom, uti.usr_prenom, uti.usr_email order by uti.usr_nom";
            $rs_tablesAdmin = $dao->BuildResultSet($strSql, array($ssdom_id));
            $result = array();
            $adminIds = "";
            for($rs_tablesAdmin->First(); !$rs_tablesAdmin->EOF(); $rs_tablesAdmin->Next()) {
                $data = array();
                $data["pk_utilisateur"] = $rs_tablesAdmin->Read(0);
                $data["displayField"] = $rs_tablesAdmin->Read(2)." ".$rs_tablesAdmin->Read(1)." (".$rs_tablesAdmin->Read(3).")";
                $data["pk_sous_domaine"] = $rs_tablesAdmin->Read(5);
                $data["ssdom_nom"] = $rs_tablesAdmin->Read(4);
                array_push($result, $data);
                $adminIds .= $data["pk_utilisateur"].",";
            }
            $strSql = "SELECT uti.pk_utilisateur, uti.usr_nom, uti.usr_prenom, uti.usr_email,
                       array_to_string(array_agg(adm.ssdom_nom), ',') as sdom_nom, array_to_string(array_agg(adm.pk_sous_domaine), ',') as sdom_ids
                       FROM catalogue.utilisateur uti, catalogue.ADMINISTRATEURS_SOUS_DOMAINE adm
                       WHERE adm.pk_utilisateur=uti.pk_utilisateur and uti.pk_utilisateur NOT IN (".substr($adminIds,0,strlen($adminIds)-1).")
                       GROUP BY uti.pk_utilisateur, uti.usr_nom, uti.usr_prenom, uti.usr_email order by uti.usr_nom";
            $rs_tablesNotAdmin=$dao->BuildResultSet($strSql);
            for($rs_tablesNotAdmin->First(); !$rs_tablesNotAdmin->EOF(); $rs_tablesNotAdmin->Next()) {
                $data = array();
                $data["pk_utilisateur"] = $rs_tablesNotAdmin->Read(0);
                $data["displayField"] = $rs_tablesNotAdmin->Read(2)." ".$rs_tablesNotAdmin->Read(1)." (".$rs_tablesNotAdmin->Read(3).")";
                $data["pk_sous_domaine"] = $rs_tablesNotAdmin->Read(5);
                $data["ssdom_nom"] = $rs_tablesNotAdmin->Read(4);
                array_push($result, $data);
            }
        }

        if($callback != "") {
            echo $callback."(".json_encode($result).")"; 
        }
        else {
            echo json_encode($result); 
        }
        exit();
    }
}

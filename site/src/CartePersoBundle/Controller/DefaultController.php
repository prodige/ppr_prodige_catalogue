<?php

namespace ProdigeCatalogue\CartePersoBundle\Controller;

use Prodige\ProdigeBundle\Common\SecurityExceptions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\Common\Mail\AlkMail;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

use Prodige\ProdigeBundle\DAOProxy\DAO;

/**
 * Contenu de l'onglet cartographie dynamique
 * @author Alkante
 * #from /PRRA/carteperso.php
 */
class DefaultController extends BaseController
{

    /**
     *
     * @IsGranted("ROLE_USER")
     * @Route("/carteperso", name="catalogue_carteperso", options={"expose"=true})
     */
    public function indexAction(Request $request)
    {


        $gntw = new GeonetworkInterface(PRO_GEONETWORK_URLBASE);

        $user = User::GetUser();
        $userId = User::GetUserId();

        $session = $this->container->get('request_stack')->getCurrentRequest()->getSession();
        $cartp_utilisateur_email =
            ($request->get("cartp_utilisateur_email", "") != ""
                ? $request->get("cartp_utilisateur_email")
                : ($session->has("cartp_utilisateur_email") && $session->get("cartp_utilisateur_email") != ""
                    ? $session->get("cartp_utilisateur_email")
                    : ""
                ));

        $session->set("cartp_utilisateur_email", $cartp_utilisateur_email);

        if (!is_null($userId)) {
            User::SetUserCookie($userId);
        }

        if (!$user->HasTraitement('CARTEPERSO')) {
            //throw SecurityExceptions::throwAccessDeniedException();
            exit(0);
        }
        if ($user->isConnected() === false ) { // cas de l'acces carte perso pour internaute
            //throw SecurityExceptions::throwAccessDeniedException();
            exit(0);
        }

        $conn = $this->getCatalogueConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');

        if ($request->query->get("mode", false)) {
            switch ($request->query->get("mode")) {
                case "Propose":
                    $mapName = $request->get("mapName");
                    $email = $request->get("email", "");
                    $mapComment = $request->get("mapComment");
                    $mapDom = $request->get("mapDom");
                    $mapSDom = $request->get("mapSdom");
                    $pk_carteprojet = $request->get("pk_carteprojet");
                    $pk_sdom = $request->get("pk_sdom");
                    $pk_administrateur = $request->get("pk_administrateur");
                    //assignation du sous-domaine
                    $query = "INSERT INTO SSDOM_VISUALISE_CARTE (SSDCART_FK_CARTE_PROJET, SSDCART_FK_SOUS_DOMAINE) values(?, ?)";
                    $dao->Execute($query, array(intval($pk_carteprojet), intval($pk_sdom)));


                    $query = 'SELECT cartp_nom, stkcard_path, '.
                        ' accs_adresse, accs_service_admin, path_consultation, cartp_utilisateur, usr_nom,  usr_prenom FROM'.
                        ' CARTE_PROJET LEFT JOIN STOCKAGE_CARTE on CARTE_PROJET.cartp_fk_stockage_carte = STOCKAGE_CARTE.pk_stockage_carte'.
                        ' LEFT JOIN ACCES_SERVEUR on STOCKAGE_CARTE.stk_server = ACCES_SERVEUR.pk_acces_serveur '.
                        ' LEFT JOIN UTILISATEUR on CARTE_PROJET.cartp_utilisateur = UTILISATEUR.pk_utilisateur where cartp_format = 2'.
                        ' and pk_carte_projet = ?';
                    $rs = $dao->BuildResultSet($query, array($pk_carteprojet));
                    if ($rs->GetNbRows() > 0) {
                        $rs->First();
                        $urlConsult = $this->container->getParameter('PRODIGE_URL_FRONTCARTO')."/1/".$rs->Read(1);
                        $strReport = "Bonjour,\n\n".
                            "Vous avez reçu une demande d'intégration de carte personnelle.\n".
                            "Caractéristiques de la demande : \n".
                            " - Auteur : ".$rs->Read(6)." ".$rs->Read(
                                7
                            ).($email != "" ? " (adresse électronique: ".$email.")" : "")."\n".
                            " - Nom de la carte :".(stripslashes($mapName))."\n".
                            " - Domaine / sous-domaine souhaité :".(stripslashes($mapDom))." / ".(stripslashes(
                                $mapSDom
                            ))."\n".
                            " - Compléments d'informations :".(stripslashes($mapComment))."\n".
                            "La carte est consultable à <a href='".$urlConsult."'>cette adresse</a>.\n".
                            "Pour traiter cette demande, <a href='".($request->server->get(
                                "HTTPS",
                                "off"
                            ) == "on" ? "https" : "http")."://".$request->server->get(
                                "HTTP_HOST"
                            )."'>connectez-vous</a> à la plateforme.\n\n".
                            "Ce message est généré automatiquement. Merci de ne pas répondre.";

                        $strReportHtml = str_replace("\n", "<br>", $strReport);

                        $oAlkMail = new AlkMail();
                        $strSujet = "Demande d'intégration de carte personnelle";
                        //$strSujet= mb_encode_mimeheader($strSujet,"UTF-8", "B", "\n");
                        $oAlkMail->setSubject($strSujet);
                        $oAlkMail->setHtml(true);

                        $oAlkMail->setBody($strReportHtml);
                        $oAlkMail->setAltBody($strReport);

                        $oAlkMail->setFrom(
                            $user->GetPrenom()." ".$user->GetNom(),
                            ($email != "" ? $email : $user->GetEmail())
                        );

                        // sélection des destinataires
                        $queryAdmin = "select distinct usr_nom, usr_prenom, usr_email from UTILISATEUR where pk_utilisateur in(?)";
                        $rs2 = $dao->buildResultSet($queryAdmin, array($pk_administrateur));
                        for ($rs2->First(); !$rs2->EOF(); $rs2->Next()) {
                            $oAlkMail->addTo($rs2->Read(1)." ".$rs2->Read(0), $rs2->Read(2));
                            if ($email != "" && $email != $user->GetEmail()) {
                                $oAlkMail->addCC($user->getPrenom()." ".$user->getNom(), $user->GetEmail());
                            }
                        }
                        //envoi du message
                        $oAlkMail->setbDebugMode(false);
                        $oAlkMail->setMaxSend(10);
                        $oAlkMail->send();

                        //cause extjs is sending in UTF8
                        $mapName = htmlentities(($mapName), ENT_QUOTES, "UTF-8");
                        //gestion des comptes génériques stockage du mail en description
                        $mapComment = ($email != "" ? $email : htmlentities(($mapComment), ENT_QUOTES, "UTF-8"));
                        $queryUpdate = "update carte_projet set cartp_nom='".$mapName."',cartp_description='".$mapComment."', cartep_etat = 1, cartp_administrateur = '".$pk_administrateur."' where pk_carte_projet = ".$pk_carteprojet;
                        $dao->Execute($queryUpdate);
                        echo '{"success":true}';
                        exit();
                    }
                    break;

                case "Accept" :
                    $pk_carteprojet = $request->get("pk_carteprojet");
                    $fiche_meta_id = $request->query->get("fiche_meta", "");
                    $callback = $request->query->get("callback", "");
                    $query = 'SELECT stkcard_path, pk_stockage_carte, '.
                        ' accs_adresse_admin, cartp_nom ,  usr_nom,  usr_prenom, usr_email,  usr_generic, cartp_description,PK_ACCES_SERVEUR, SSDCART_FK_SOUS_DOMAINE FROM'.
                        ' CARTE_PROJET LEFT JOIN STOCKAGE_CARTE on CARTE_PROJET.cartp_fk_stockage_carte = STOCKAGE_CARTE.pk_stockage_carte'.
                        ' LEFT JOIN ACCES_SERVEUR on STOCKAGE_CARTE.stk_server = ACCES_SERVEUR.pk_acces_serveur '.
                        ' LEFT JOIN UTILISATEUR on CARTE_PROJET.cartp_utilisateur = UTILISATEUR.pk_utilisateur '.
                        '  LEFT JOIN SSDOM_VISUALISE_CARTE on CARTE_PROJET.pk_carte_projet = SSDOM_VISUALISE_CARTE.SSDCART_FK_CARTE_PROJET '.
                        ' where pk_carte_projet = ?';
                    $rs = $dao->BuildResultSet($query, array($pk_carteprojet));
                    if ($rs->GetNbRows() > 0) {
                        $rs->First();
                        $stkcard_path = $rs->Read(0);
                        $map_id = "";
                        $conn_prodige = $this->getProdigeConnection('carmen');
                        $dao_prodige = new DAO($conn_prodige, 'carmen');
                        if ($dao_prodige) {
                            $mapfile_name = explode(".", $rs->Read(0));
                            $query_prodige = "SELECT map_id FROM map WHERE map_file = ? and published_id is null";
                            $rs_prodige = $dao_prodige->buildResultSet($query_prodige, array($mapfile_name[0]));
                            if ($rs_prodige->GetNbRows() > 0) {
                                $rs_prodige->First();
                                $map_id = $rs_prodige->Read(0);
                            }
                        }

                        $newstkcard_path = pathinfo($rs->Read(0), PATHINFO_FILENAME);

                        if ($map_id == "") {
                            throw new \Exception("Carte inexistante");
                        }


                        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');

                        //get first map model found
                        /*$jsonResp = $geonetwork->get("qi?_content_type=json&template=y&_isTemplate=y&type=map");
						            $jsonObj = json_decode($jsonResp);
                        if($jsonObj &&  $jsonObj->metadata  ){
                            if(empty($jsonObj->metadata)){
                                throw new \Exception("Impossible de créer la métadonnée, aucun modèle ne vous est accessible");
                            }else{
                                $PRO_CARTEPERSO_MODELE_ID = $jsonObj->metadata[0]->id;
                            }
                        } else {
                            throw new \Exception("Impossible de récupérer la liste des modèles");
                        }*/

                        $namepspaces = array(
                            "gmd" => "http://www.isotc211.org/2005/gmd",
                            "srv" => "http://www.isotc211.org/2005/srv",
                            "gco" => "http://www.isotc211.org/2005/gco",
                        );
                        $sql_namespaces = array();
                        foreach ($namepspaces as $alias => $path) {
                            $sql_namespaces[] = "{".$alias.",".$path."}";
                        }
                        $sql_namespaces = "{".implode(",", $sql_namespaces)."}";

                        $nodeMap = '//gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:serviceType[gco:LocalName=\'invoke\']';
                        //Template de métadonnée de type invoke (map
                        $where = array(
                            "istemplate='y'",
                            "isharvested='n'",
                            "schemaid='iso19139'",
                            "xpath_exists(:nodeMap, data::xml, :sql_namespaces ::text[][])",
                        );
                        $template_id = $conn->fetchOne(
                            "select id from public.metadata where ".implode(" and ", $where),
                            compact('nodeMap', 'sql_namespaces')
                        );
                        if (!$template_id) {
                            throw new \Exception("Impossible de trouver un modèle de métadonnée pour les cartes.");
                        } else {
                            $PRO_CARTEPERSO_MODELE_ID = $template_id;
                        }


                        //création de la métadonnée à partir de l'existante ou du modèle de base
                        $query = 'SELECT FMETA_ID FROM FICHE_METADONNEES WHERE PK_FICHE_METADONNEES = ?';
                        $rs5 = $dao->BuildResultSet($query, array($fiche_meta_id));
                        $metadataId = $PRO_CARTEPERSO_MODELE_ID;
                        if ($rs5->GetNbRows() > 0) {
                            $rs5->First();
                            $metadataId = $rs5->Read(0);
                        }

                        //get All group in which user can edit
                        $jsonResp = $geonetwork->get("info?_content_type=json&type=groups&profile=Editor");
                        $jsonObj = json_decode($jsonResp);
                        if ($jsonObj && $jsonObj->group) {
                            if (empty($jsonObj->group)) {
                                throw new \Exception(
                                    "Impossible de créer la métadonnée, vous n'avez les droits d'édition sur aucun groupe"
                                );
                            } else {
                                $group_id = $jsonObj->group[0]->{"@id"};
                            }
                        } else {
                            throw new \Exception("Impossible de récupérer les groupes dans Geosource");
                        }
                        $jsonResp = $geonetwork->get(
                            'md.create?_content_type=json&id='.$metadataId."&group=".$group_id."&template=n&child=n&fullPrivileges=false"
                        );

                        $jsonObj = json_decode($jsonResp);
                        if ($jsonObj && $jsonObj->id) {
                            $newMetadataId = $jsonObj->id;
                        } else {
                            throw new \Exception("Impossible de créer la métadonnée");
                        }
                        $query = 'SELECT uuid
                                  FROM public.metadata'.
                            ' where id = ?';
                        $ds = $dao->BuildResultSet($query, array($newMetadataId));
                        $mapUuid = "";
                        if ($ds->GetNbRows() > 0) {
                            $ds->First();
                            $mapUuid = $ds->Read(0);
                        }
                        //publish metadata
                        $jsonResp = $geonetwork->get(
                            'md.privileges.update?_content_type=json&_1_0=on&id='.$newMetadataId
                        );

                        //copy map    
                        $acces_adress_admin = $this->container->getParameter('PRODIGE_URL_ADMINCARTO');

                        $carmenEditMapAction = $acces_adress_admin."/api/map/edit/".$map_id."/resume";
                        $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT(
                            $carmenEditMapAction
                        );
                        $jsonResp = $this->curl($carmenEditMapAction, 'GET', array('ticket' => $ticket), array());

                        $jsonObj = json_decode($jsonResp);
                        if ($jsonObj && $jsonObj->success && $jsonObj->map && $jsonObj->map->mapId) {
                            $new_map_id = $jsonObj->map->mapId;

                            //update parameters
                            $conn_prodige->executeStatement(
                                "update map set published_id =:map_id, map_wmsmetadata_uuid=:uuid where map_id=:map_id",
                                array(
                                    "map_id" => $new_map_id,
                                    "uuid" => $mapUuid,
                                )
                            );
                            //copy map 
                            $carmenSaveMapAction = $acces_adress_admin."/api/map/publish/".$new_map_id."/".$new_map_id;
                            $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT(
                                $carmenSaveMapAction
                            );
                            $jsonResp = $this->curl(
                                $carmenSaveMapAction,
                                'POST',
                                array('ticket' => $ticket),
                                array(
                                    "mapModel" => "false",
                                    "mapFile" => $newstkcard_path,
                                )
                            );

                            $jsonObj = json_decode($jsonResp);
                            if ($jsonObj && $jsonObj->success && $jsonObj->map) {
                                $query = 'SELECT nextval(\'seq_stockage_carte\'),  nextval(\'seq_carte_projet\')';
                                $rs2 = $dao->BuildResultSet($query);
                                if ($rs2->GetNbRows() > 0) {
                                    $rs2->First();
                                    $stockage_carte = $rs2->Read(0);
                                    $carte = $rs2->Read(1);
                                }
                                if (!empty($stockage_carte) && !empty($carte)) {
                                    $query = 'INSERT INTO STOCKAGE_CARTE (PK_STOCKAGE_CARTE, STKCARD_ID, STKCARD_PATH, STK_SERVER)
                                              SELECT '.$stockage_carte.',\''.substr(
                                            $rs->Read(9).'_'.$stockage_carte.'_'.$stkcard_path,
                                            0,
                                            30
                                        ).'\',\''.$newstkcard_path.'.map\',STK_SERVER FROM STOCKAGE_CARTE WHERE PK_STOCKAGE_CARTE =?';
                                    if ($dao->Execute($query, array($rs->Read(1)))) {
                                        $query = 'INSERT INTO CARTE_PROJET (PK_CARTE_PROJET, CARTP_ID, CARTP_NOM, CARTP_FORMAT, CARTP_FK_STOCKAGE_CARTE, CARTP_UTILISATEUR)
                                                  SELECT '.$carte.', \''.substr(
                                                $rs->Read(9).'_'.$carte.'_'.$stkcard_path,
                                                0,
                                                30
                                            ).'\', CARTP_NOM, 0, '.$stockage_carte.', null FROM CARTE_PROJET WHERE PK_CARTE_PROJET=?';
                                        if ($dao->Execute($query, array($pk_carteprojet))) {
                                            $queryUpdate = "update carte_projet set cartep_etat = 0, cartp_administrateur=null where pk_carte_projet = ?";
                                            $dao->Execute($queryUpdate, array($pk_carteprojet));
                                            $queryUpdate = "update stockage_carte set stkcard_path = ? where pk_stockage_carte = ?";
                                            $dao->Execute($queryUpdate, array($newstkcard_path.".map", $stockage_carte)
                                            );
                                            $queryUpdate = "INSERT INTO SSDOM_VISUALISE_CARTE (SSDCART_FK_CARTE_PROJET, SSDCART_FK_SOUS_DOMAINE) values(?, ?)";
                                            $dao->Execute($queryUpdate, array(intval($carte), intval($rs->Read(10))));
                                            $queryUpdate = "DELETE FROM SSDOM_VISUALISE_CARTE WHERE SSDCART_FK_CARTE_PROJET=? AND SSDCART_FK_SOUS_DOMAINE =?";
                                            $dao->Execute($queryUpdate, array($pk_carteprojet, $rs->Read(10)));
                                            $queryUpdate = "INSERT INTO ssdom_dispose_metadata (uuid, ssdcouch_fk_sous_domaine) values(?, ?)";
                                            $dao->Execute($queryUpdate, array($mapUuid, intval($rs->Read(10))));
                                        } else {
                                            throw new \Exception(
                                                'Erreur lors de la cr&eacute;ation de l\'enregistrement de la carte'
                                            );
                                        }
                                    } else {
                                        throw new \Exception(
                                            'Erreur lors de la cr&eacute;ation de l\'emplacement de stockage'
                                        );
                                    }
                                }


                                //$strHTML = $gntw->get(PRO_GEONETWORK_DIRECTORY . "/srv/fre/metadata.admin?id=".$metadataId."&timeType=on&_1_0=on&_2_0=on&_2_2=on","administrateur");
                                //insertion du lien donnée métadonnée
                                $query = 'SELECT nextval(\'seq_fiche_metadonnees\');';
                                $rs3 = $dao->BuildResultSet($query);
                                if ($rs3->GetNbRows() > 0) {
                                    $rs3->First();
                                    $next_fiche_metadonnees = $rs3->Read(0);
                                }
                                if (is_int(intval($next_fiche_metadonnees))) {
                                    $query = 'INSERT INTO FICHE_METADONNEES (PK_FICHE_METADONNEES, FMETA_ID, STATUT) VALUES (?, ?, 4)';
                                    $dao->Execute($query, array($next_fiche_metadonnees, $newMetadataId));

                                    $query = 'update CARTE_PROJET set cartp_fk_fiche_metadonnees = ? where PK_CARTE_PROJET = ?';
                                    $dao->Execute($query, array($next_fiche_metadonnees, $carte));

                                    //update metadata 
                                    $this->forward('Prodige\ProdigeBundle\Controller\UpdateDomSdomController::updateDomSdomAction', array(
                                        'fmeta_id' => $newMetadataId,
                                        'modeData' => 'carte',
                                    ));

                                    //envoi mail
                                    $strReport = "Bonjour,\n\n".
                                        "Votre demande d'intégration de la carte personnelle \"".$rs->Read(
                                            3
                                        )."\" a été acceptée.\n".
                                        "La fiche de métadonnée est disponible à l'adresse suivante :\n".
                                        "<a href='".rtrim(
                                            PRO_GEONETWORK_URLBASE,
                                            "/"
                                        ).'/srv/fre/catalog.search#/metadata/'.$mapUuid."'>".
                                        rtrim(
                                            PRO_GEONETWORK_URLBASE,
                                            "/"
                                        ).'/srv/fre/catalog.search#/metadata/'.$mapUuid."</a>\n\n".
                                        "Ce message est généré automatiquement. Merci de ne pas répondre.";

                                    $strReportHtml = str_replace("\n", "<br>", $strReport);
                                    $oAlkMail = new AlkMail();
                                    $strSujet = "Demande d'intégration de carte personnelle acceptée";
                                    //$strSujet= mb_encode_mimeheader($strSujet,"UTF-8", "B", "\n");
                                    $oAlkMail->setSubject($strSujet);
                                    $oAlkMail->setHtml(true);
                                    $oAlkMail->setBody($strReportHtml);
                                    $oAlkMail->setAltBody($strReport);
                                    $oAlkMail->setFrom($user->GetPrenom()." ".$user->GetNom(), $user->GetEmail());
                                    //gestion des comptes génériques
                                    if ($rs->Read(7) == 1) {
                                        $oAlkMail->addTo($rs->Read(5)." ".$rs->Read(4), $rs->Read(8));
                                        $oAlkMail->addCC($rs->Read(5)." ".$rs->Read(4), $rs->Read(6));
                                    } else {
                                        $oAlkMail->addTo($rs->Read(5)." ".$rs->Read(4), $rs->Read(6));
                                    }
                                    // envoi du message
                                    $oAlkMail->setbDebugMode(false);
                                    $oAlkMail->setMaxSend(10);

                                    $oAlkMail->send();
                                    //Ajout des domaines sous-domaines
                                    //$res = json_decode(file_get_contents(PRO_CATALOGUE_URLBASE."Services/setMetadataDomSdom.php?mode=carte&id=".$metadataId));
                                    //$context = $this->createUnsecureSslContext();
                                    //$res = json_decode(file_get_contents($this->generateUrl('prodige_setMetaDataDomSdom')."?mode=carte&id=".$metadataId));
                                    //if($res && $res->success == true) {
                                    //$sdom_nom = $rs->Read(0);
                                    echo $callback.'{"success":"true"}';
                                    exit();
                                    //}
                                }
                            } else {
                                echo $callback.'{"success":"false"}';
                                exit();
                            }
                        }
                    } else {
                        echo $callback.'{"success":"false"}';
                        exit();
                    }
                    break;
                case "Refus" :
                    $mapComment = $request->get("mapComment");
                    $pk_carteprojet = $request->get("pk_carteprojet");
                    //suppression du sous-domaine assigné
                    $query = "delete from SSDOM_VISUALISE_CARTE where SSDCART_FK_CARTE_PROJET =?";
                    $dao->Execute($query, array($pk_carteprojet));

                    $query = 'SELECT cartp_nom, '.
                        ' cartp_utilisateur, usr_nom,  usr_prenom, usr_email, usr_generic, cartp_description FROM'.
                        ' CARTE_PROJET LEFT JOIN STOCKAGE_CARTE on CARTE_PROJET.cartp_fk_stockage_carte = STOCKAGE_CARTE.pk_stockage_carte'.
                        ' LEFT JOIN ACCES_SERVEUR on STOCKAGE_CARTE.stk_server = ACCES_SERVEUR.pk_acces_serveur '.
                        ' LEFT JOIN UTILISATEUR on CARTE_PROJET.cartp_utilisateur = UTILISATEUR.pk_utilisateur where cartp_format = 2'.
                        ' and pk_carte_projet = ?';

                    $rs = $dao->BuildResultSet($query, array($pk_carteprojet));

                    if ($rs->GetNbRows() > 0) {
                        $rs->First();
                        $strReport = "Bonjour,\n\n".
                            "Votre demande d'intégration de la carte personnelle \"".$rs->Read(0)."\" a été refusée.\n".
                            "Motif du refus : \n".(stripslashes($mapComment))."\n".
                            "La carte est de nouveau accessible dans votre espace personnel.\n\n".
                            "Ce message est généré automatiquement. Merci de ne pas répondre.";

                        $strReportHtml = str_replace("\n", "<br>", $strReport);
                        //echo $strReportHtml;
                        $oAlkMail = new AlkMail();
                        $strSujet = "Demande d'intégration de carte personnelle refusée";
                        //$strSujet= mb_encode_mimeheader($strSujet,"UTF-8", "B", "\n");

                        $oAlkMail->setSubject(($strSujet));
                        $oAlkMail->setHtml(true);

                        $oAlkMail->setBody($strReportHtml);
                        $oAlkMail->setAltBody($strReport);
                        $oAlkMail->setFrom($user->GetPrenom()." ".$user->GetNom(), $user->GetEmail());
                        //gestion des comptes génériques
                        if ($rs->Read(5) == 1) {
                            $oAlkMail->addTo($rs->Read(3)." ".$rs->Read(2), $rs->Read(6));
                            $oAlkMail->addCC($rs->Read(3)." ".$rs->Read(2), $rs->Read(4));
                        } else {
                            $oAlkMail->addTo($rs->Read(3)." ".$rs->Read(2), $rs->Read(4));
                        }

                        // envoi du message
                        $oAlkMail->setbDebugMode(false);
                        $oAlkMail->setMaxSend(10);
                        $oAlkMail->send();

                        $queryUpdate = "update carte_projet set cartep_etat = 0 where pk_carte_projet = ?";
                        $dao->Execute($queryUpdate, array($pk_carteprojet));
                        echo '{"success":true}';
                        exit();
                    }
                    break;
                case "EditMetadata" :
                    $callback = $request->query->get("callback", "");
                    $fiche_meta_id = $request->query->get("fiche_meta", "");
                    $pk_carteprojet = $request->query->get("pk_carteprojet");
                    $admin = $request->query->get("admin", 0);
                    if ($fiche_meta_id == 0) {

                        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');

                        $namepspaces = array(
                            "gmd" => "http://www.isotc211.org/2005/gmd",
                            "srv" => "http://www.isotc211.org/2005/srv",
                            "gco" => "http://www.isotc211.org/2005/gco",
                        );
                        $sql_namespaces = array();
                        foreach ($namepspaces as $alias => $path) {
                            $sql_namespaces[] = "{".$alias.",".$path."}";
                        }
                        $sql_namespaces = "{".implode(",", $sql_namespaces)."}";

                        $nodeMap = '//gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:serviceType[gco:LocalName=\'invoke\']';
                        //Template de métadonnée de type invoke (map
                        $where = array(
                            "istemplate='y'",
                            "isharvested='n'",
                            "schemaid='iso19139'",
                            "xpath_exists(:nodeMap, data::xml, :sql_namespaces ::text[][])",
                        );
                        $template_id = $conn->fetchOne(
                            "select id from public.metadata where ".implode(" and ", $where),
                            compact('nodeMap', 'sql_namespaces')
                        );
                        if (!$template_id) {
                            throw new \Exception("Impossible de trouver un modèle de métadonnée pour les cartes.");
                        } else {
                            $PRO_CARTEPERSO_MODELE_ID = $template_id;
                        }

                        //get first map model found, not working , why ?
                        /*$jsonResp = $geonetwork->get("qi?_content_type=json&template=y&_isTemplate=y&type=map");
						
						            $jsonObj = json_decode($jsonResp);
                        if($jsonObj &&  $jsonObj->metadata  ){
                            if(empty($jsonObj->metadata)){
                                throw new \Exception("Impossible de créer la métadonnée, aucun modèle ne vous est accessible");
                            }else{
                                $PRO_CARTEPERSO_MODELE_ID = $jsonObj->metadata[0]->id;
                            }
                        } else {
                            throw new \Exception("Impossible de récupérer la liste des modèles");
                        }*/

                        //get All group in which user can edit
                        $jsonResp = $geonetwork->get("info?_content_type=json&type=groups&profile=Editor");
                        $jsonObj = json_decode($jsonResp);
                        if ($jsonObj && $jsonObj->group) {
                            if (empty($jsonObj->group)) {
                                throw new \Exception(
                                    "Impossible de créer la métadonnée, vous n'avez les droits d'édition sur aucun groupe"
                                );
                            } else {
                                $group_id = $jsonObj->group[0]->{"@id"};
                            }
                        } else {
                            throw new \Exception("Impossible de récupérer les groupes dans Geosource");
                        }
                        $jsonResp = $geonetwork->get(
                            'md.create?_content_type=json&id='.$PRO_CARTEPERSO_MODELE_ID."&group=".$group_id."&template=n&child=n&fullPrivileges=false"
                        );

                        $jsonObj = json_decode($jsonResp);
                        if ($jsonObj && $jsonObj->id) {
                            $metadataId = $jsonObj->id;
                        } else {
                            throw new \Exception("Impossible de créer la métadonnée à partir du modèle de fiche carte");
                        }

                        //insertion du lien donnée métadonnée
                        $query = 'SELECT nextval(\'seq_fiche_metadonnees\');';
                        $rs = $dao->BuildResultSet($query);
                        if ($rs->GetNbRows() > 0) {
                            $rs->First();
                            $next_fiche_metadonnees = $rs->Read(0);
                        }
                        if ($metadataId) {
                            $query = 'INSERT INTO FICHE_METADONNEES (PK_FICHE_METADONNEES, FMETA_ID, STATUT) VALUES (?, ?, 1)';
                            $dao->Execute($query, array($next_fiche_metadonnees, $metadataId));
                            $query = 'update CARTE_PROJET set cartp_fk_fiche_metadonnees = ? where PK_CARTE_PROJET = ?';
                            $dao->Execute($query, array($next_fiche_metadonnees, $pk_carteprojet));

                            if ($admin == 1) {
                                $res = json_decode(
                                    file_get_contents(
                                        $this->generateUrl('prodige_setMetaDataDomSdom')."?mode=carte&id=".$metadataId
                                    )
                                );
                                //$res = json_decode(file_get_contents(PRO_CATALOGUE_URLBASE."Services/setMetadataDomSdom.php?mode=carte&id=".$metadataId));
                                if ($res && $res->success == true) {
                                    $sdom_nom = $rs->Read(0);
                                    $url_edit = rtrim(
                                            PRO_GEONETWORK_URLBASE,
                                            "/"
                                        ).'/srv/fre/catalog.edit#/metadata/'.$metadataId;
                                    header('location:'.$url_edit);
                                    exit();
                                }
                            } else {
                                $url_edit = rtrim(
                                        PRO_GEONETWORK_URLBASE,
                                        "/"
                                    ).'/srv/fre/catalog.edit#/metadata/'.$metadataId;
                                header('location:'.$url_edit);
                                exit();
                            }
                        }

                    } else {
                        $query = 'SELECT FMETA_ID FROM FICHE_METADONNEES WHERE PK_FICHE_METADONNEES = ?;';
                        $rs = $dao->BuildResultSet($query, array($fiche_meta_id));
                        if ($rs->GetNbRows() > 0) {
                            $rs->First();
                            $metadataId = $rs->Read(0);
                            $url_edit = rtrim(
                                    PRO_GEONETWORK_URLBASE,
                                    "/"
                                ).'/srv/fre/catalog.edit#/metadata/'.$metadataId;
                            header('location:'.$url_edit);
                            exit();
                        }
                    }
                    break;

                case "duplicate" :
                    //1. création d'une carte personnelle par duplication de la carte $_GET["MAP"]
                    //$dao = new DAO(); //hismail - Prodige4.0 - déjà instancié
                    if ($dao) {
                        $pk_acc_server = "";
                        $query = 'SELECT PK_ACCES_SERVEUR, ACCS_ID, ACCS_ADRESSE_ADMIN FROM ACCES_SERVEUR ORDER BY ACCS_ID';
                        $rs = $dao->BuildResultSet($query);
                        for ($rs->First(); !$rs->EOF(); $rs->Next()) {
                            $pk_acc_server = $rs->Read(0);
                        }
                    }
                    $request->request->set("MAP_OLD", $request->query->get("MAP_OLD", "")); // nom du mapfile d'origine
                    $request->request->set(
                        "MAP_TITLE",
                        $request->query->get("MAP_COPY_TITLE", "")
                    ); // nom de la nouvelle carte non transformée
                    $request->request->set("MAP", $request->query->get("MAP", "")); // nom du new mapfile mis en forme
                    $request->request->set(
                        "SERVEUR",
                        (isset($pk_acc_server) && $pk_acc_server != "" ? $pk_acc_server : "")
                    );
                    $request->request->set("PATH", "");
                    $errormsg = "";
                    $callback = $request->query->get("callback", "");
                    $query = 'SELECT accs_adresse, accs_adresse_admin, path_administration, accs_login_admin, accs_pwd_admin FROM ACCES_SERVEUR ORDER BY ACCS_ID';
                    $rs = $dao->BuildResultSet($query);
                    if ($rs->GetNbRows() > 0) {
                        $rs->First();
                        $urlAdmin = ($request->server->get("HTTPS", "off") == "on" ? "https" : "http")."://".$rs->Read(
                                1
                            ).$rs->Read(2);
                    }
                    if ($request->request->get("MAP", "") != "" && $request->request->get(
                            "SERVEUR",
                            ""
                        ) != "" && $request->request->get("MAP_TITLE", "") != "") {
                        $chemin_carte = $request->request->get("MAP");
                        if (!preg_match('/\.map$/i', $chemin_carte)) {
                            $chemin_carte .= '.map';
                        }
                        // chemin de la carte d'origine
                        $chemin_carte_old = $request->request->get("MAP_OLD");
                        if (!preg_match('/\.map$/i', $chemin_carte_old)) {
                            $chemin_carte_old .= '.map';
                        }

                        if ($dao) {
                            unset($stockage_carte);
                            unset($carte);

                            // recuperation des informations (CARTP_UTILISATEUR_EMAIL) de la carte source dans CARTE_PROJET pour connaitre CARTP_UTILISATEUR_EMAIL
                            $CARTP_UTILISATEUR_EMAIL = null;
                            $query = 'SELECT SC.PK_STOCKAGE_CARTE, CARTP_UTILISATEUR_EMAIL FROM STOCKAGE_CARTE SC LEFT JOIN CARTE_PROJET CP ON SC.PK_STOCKAGE_CARTE = CP.CARTP_FK_STOCKAGE_CARTE
                                          WHERE STKCARD_PATH = ? AND STK_SERVER = ?';

                            $rs0 = $dao->BuildResultSet(
                                $query,
                                array($chemin_carte_old, $request->request->get("SERVEUR"))
                            );
                            if ($rs0->GetNbRows() > 0) {
                                $rs0->First();
                                $CARTP_UTILISATEUR_EMAIL = $rs0->Read(1);
                            }

                            $query = 'SELECT PK_STOCKAGE_CARTE FROM STOCKAGE_CARTE WHERE STKCARD_PATH = \'carteperso/'.$userId.'/'.$chemin_carte.'\' AND STK_SERVER = ?';

                            $rs = $dao->BuildResultSet($query, array($request->request->get("SERVEUR")));
                            if ($rs->GetNbRows() == 0) {
                                $query = 'SELECT nextval(\'seq_stockage_carte\'),  nextval(\'seq_carte_projet\')';
                                $rs = $dao->BuildResultSet($query);
                                if ($rs->GetNbRows() > 0) {
                                    $rs->First();
                                    $stockage_carte = $rs->Read(0);
                                    $carte = $rs->Read(1); // nel identifiant PK_CARTE_PROJET
                                }
                                if (!empty($stockage_carte) && !empty($carte)) {
                                    $query = 'INSERT INTO STOCKAGE_CARTE (PK_STOCKAGE_CARTE, STKCARD_ID, STKCARD_PATH, STK_SERVER) VALUES (?, ?, ?, ?)';
                                    if ($dao->Execute(
                                        $query,
                                        array(
                                            $stockage_carte,
                                            substr(
                                                $request->request->get(
                                                    'SERVEUR'
                                                ).'_'.$stockage_carte.'_'.$request->request->get('PATH'),
                                                0,
                                                30
                                            ),
                                            "carteperso/".$userId."/".$chemin_carte,
                                            $request->request->get('SERVEUR'),
                                        )
                                    )) {
                                        $query = 'INSERT INTO CARTE_PROJET (PK_CARTE_PROJET, CARTP_ID, CARTP_NOM, CARTP_FORMAT, CARTP_FK_STOCKAGE_CARTE, CARTP_UTILISATEUR '.($CARTP_UTILISATEUR_EMAIL != null ? ', CARTP_UTILISATEUR_EMAIL' : '').') VALUES ('.$carte.', \''.substr(
                                                $request->request->get('SERVEUR').'_'.$carte.'_'.$request->request->get(
                                                    'PATH'
                                                ),
                                                0,
                                                30
                                            ).'\', \''.$request->request->get(
                                                "MAP_TITLE"
                                            ).'\', 2, '.$stockage_carte.', '.$userId.($CARTP_UTILISATEUR_EMAIL != null ? ', \''.$CARTP_UTILISATEUR_EMAIL.'\'' : '').')';
                                        if ($dao->Execute($query)) {
                                            $request->request->set("mapTitle", $request->request->get("MAP_TITLE"));
                                            $mapUrl = $urlAdmin."carteperso/".$userId."/".$chemin_carte;
                                            //echo http_post($mapUrl);//exit;
                                            // chemin de la carte d'origine
                                            $chemin_carte_old = $request->request->get('MAP_OLD');
                                            if (!preg_match('/\.map$/i', $chemin_carte_old)) {
                                                $chemin_carte_old .= '.map';
                                            }
                                            // carte projet dupliquée PK_CARTE_PROJET = $carte;
                                            //2. duplication fiche meta si elle existe sinon on la créée
                                            //$callback = isset($_GET["callback"]) ? $_GET["callback"] : ""; //hismail - déjà affecté
                                            $fiche_meta_id = 3; // id de la fiche de meta initialisée à 3, fiche de metadonnée "modèle";
                                            $pk_carteprojet = -1; //
                                            if ($dao) {
                                                //recherche de la fiche de meta d'origine si elle existe :
                                                //$fiche_meta_id = isset($_GET["fiche_meta"]) ? $_GET["fiche_meta"] : "";
                                                $query = 'SELECT PK_STOCKAGE_CARTE FROM STOCKAGE_CARTE WHERE STKCARD_PATH = ?\''.$chemin_carte_old.'\' AND STK_SERVER = '.$_POST['SERVEUR'];
                                                $rs = $dao->BuildResultSet(
                                                    $query,
                                                    array($chemin_carte_old, $request->request->get('SERVEUR'))
                                                );
                                                if ($rs->GetNbRows() > 0) {
                                                    $rs->First();
                                                    $stockage_carte = $rs->Read(0);
                                                    if (isset($stockage_carte) && !empty($stockage_carte)) {
                                                        $query2 = 'SELECT fmeta_id, PK_CARTE_PROJET FROM CARTE_PROJET CP
                                                                       LEFT JOIN fiche_metadonnees fm on fm.pk_fiche_metadonnees = CP.cartp_fk_fiche_metadonnees
                                                                       WHERE cartp_fk_stockage_carte=?';
                                                        $rs2 = $dao->BuildResultSet($query2, array($stockage_carte));
                                                        if ($rs2->GetNbRows() > 0) { // il n'y a pas de fiche de meta
                                                            for ($rs2->First(); !$rs2->EOF(); $rs2->Next()) {
                                                                $fiche_meta_id = (is_null($rs2->Read(0)) || $rs2->Read(
                                                                    0
                                                                ) == "" ? 3 : $rs2->Read(
                                                                    0
                                                                )); // id de la fiche de meta d'origine, sinon initialisée à 3 pour obtenir fiche de metadonnée "modèle";
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            $admin = $request->query->get("admin", 0);
                                            $strHTML = $gntw->get(
                                                PRO_GEONETWORK_DIRECTORY.'/srv/fre/metadata.create.new?id='.$fiche_meta_id.'&group=5',
                                                'carte_perso'
                                            );
                                            $xmlDoc = new \DOMDocument('1.0', 'UTF-8');
                                            if (@$xmlDoc->loadHTML($strHTML)) {
                                                $xpath = new \DOMXpath($xmlDoc);
                                                $form = $xpath->query("/html/body/div/form/input");
                                                if ($form->length > 0) {
                                                    foreach ($form as $key => $input) {
                                                        if ($input->hasAttribute("name") && $input->getAttribute(
                                                                "name"
                                                            ) == "id") {
                                                            $metadataId = $input->getAttribute("value");
                                                            //insertion du lien donnée métadonnée
                                                            $query = 'SELECT nextval(\'seq_fiche_metadonnees\');';
                                                            $rs = $dao->BuildResultSet($query);
                                                            if ($rs->GetNbRows() > 0) {
                                                                $rs->First();
                                                                $next_fiche_metadonnees = $rs->Read(0);
                                                            }
                                                            if (isIntVal($metadataId)) {
                                                                $query = 'INSERT INTO FICHE_METADONNEES (PK_FICHE_METADONNEES, FMETA_ID, STATUT) VALUES (?, ?, 1)';
                                                                if (!$dao->Execute(
                                                                    $query,
                                                                    array($next_fiche_metadonnees, $metadataId)
                                                                )) {
                                                                    error_log($query);
                                                                    error_log(pg_last_error());
                                                                }
                                                                $query = 'update CARTE_PROJET set cartp_fk_fiche_metadonnees = ? where PK_CARTE_PROJET = ?';
                                                                if (!$dao->Execute(
                                                                    $query,
                                                                    array($next_fiche_metadonnees, $carte)
                                                                )) {
                                                                    error_log($query);
                                                                    error_log(pg_last_error());
                                                                }
                                                                $dao->commit();

                                                                $tabChemin = explode(
                                                                    "/",
                                                                    $request->request->get("MAP_OLD")
                                                                );
                                                                $strCheminDest = $tabChemin[0]."/".$tabChemin[1]."/".$chemin_carte;
                                                                if ($admin == 1) {
                                                                    //$res = json_decode(file_get_contents(PRO_CATALOGUE_URLBASE."Services/setMetadataDomSdom.php?mode=carte&id=".$metadataId));
                                                                    $context = $this->createUnsecureSslContext();
                                                                    $res = json_decode(
                                                                        file_get_contents(
                                                                            $this->generateUrl(
                                                                                'prodige_setMetaDataDomSdom'
                                                                            )."?mode=carte&id=".$metadataId
                                                                        )
                                                                    );

                                                                    if ($res && $res->success == true) {
                                                                        $sdom_nom = $rs->Read(0);
                                                                        $mapUrl = "carteperso/".$userId."/".$chemin_carte;
                                                                        echo $callback.'({"success":"true", "param" :"carte_perso#%#carte_perso", "reloadNewCarte":"'.$urlAdmin.'carteperso/'.$userId.'/'.$chemin_carte.'", "newMapfile":"'.$strCheminDest.'"});';
                                                                        exit();
                                                                    }
                                                                } else {
                                                                    $mapUrl = "carteperso/".$userId."/".$chemin_carte;
                                                                    echo $callback.'({"success":"true", "param" :"carte_perso#%#carte_perso", "reloadNewCarte":"'.$urlAdmin.'carteperso/'.$userId.'/'.$chemin_carte.'", "newMapfile":"'.$strCheminDest.'"});';
                                                                    exit();
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    echo $callback.'({"success":"false"});';
                                                    exit();
                                                }
                                            } else {
                                                echo $callback.'({"success":"false"});';
                                                exit();
                                            }
                                        } else {
                                            $errormsg = 'Erreur lors de la cr&eacute;ation de l\'enregistrement de la carte';
                                            echo $callback.'({"success":"false", "msgErr" :"'.$errormsg.'"});';
                                            exit();
                                        }
                                    } else {
                                        $errormsg = 'Erreur lors de la cr&eacute;ation de l\'emplacement de stockage';
                                        echo $callback.'({"success":"false", "msgErr" :"'.$errormsg.'"});';
                                        exit();
                                    }
                                } else {
                                    $errormsg = 'Impossible d\'obtenir les identifiants pour le stockage de la carte';
                                    echo $callback.'({"success":"false", "msgErr" :"'.$errormsg.'"});';
                                    exit();
                                }
                            } else {
                                $errormsg = 'Il existe d&eacute;j&agrave; une fiche de m&eacute;tadonn&eacute;es pour cette carte';
                                echo $callback.'({"success":"false", "msgErr" :"'.$errormsg.'"});';
                                exit();
                            }
                        }
                    }
                    //if($errormsg!=""){
                    //echo json_encode($errormsg); exit();
                    //echo $errormsg; exit();
                    //}
                    break;
            }
        }

        $actionContexte = $this->generateUrl('prodige_context_uploadcontext');
        //$urlAdmin =  $this->getParameter('PRODIGE_URL_FRONTCARTO');
        $urlInit = $this->generateUrl('catalogue_carteperso_init');


        //création d'une carte personnelle
        if ($request->isMethod('POST') && $request->request->get("MAP", false) && $request->request->get(
                'SERVEUR',
                false
            ) && $request->request->get("MAP_TITLE", false)) {
            $chemin_carte = $request->request->get('MAP');
            if (!preg_match('/\.map$/i', $chemin_carte)) {
                $chemin_carte .= '.map';
            }
            if ($dao) {
                unset($stockage_carte);
                unset($carte);
                $query = 'SELECT PK_STOCKAGE_CARTE FROM STOCKAGE_CARTE WHERE STKCARD_PATH = ? AND STK_SERVER = ?';
                //error_log($query);
                $rs = $dao->BuildResultSet($query, array($chemin_carte, $request->request->get('SERVEUR')));
                if ($rs->GetNbRows() == 0) {
                    $query = 'SELECT nextval(\'seq_stockage_carte\'),  nextval(\'seq_carte_projet\')';
                    $rs = $dao->BuildResultSet($query);
                    if ($rs->GetNbRows() > 0) {
                        $rs->First();
                        $stockage_carte = $rs->Read(0);
                        $carte = $rs->Read(1);
                    }
                    if (!empty($stockage_carte) && !empty($carte)) {
                        $query = 'INSERT INTO STOCKAGE_CARTE (PK_STOCKAGE_CARTE, STKCARD_ID, STKCARD_PATH, STK_SERVER) VALUES (?, ?, ?, ?)';
                        if ($dao->Execute(
                            $query,
                            array(
                                $stockage_carte,
                                substr(
                                    $request->request->get('SERVEUR').'_'.$stockage_carte.'_'.$request->request->get(
                                        'PATH'
                                    ),
                                    0,
                                    30
                                ),
                                "carteperso/".$userId."/".$chemin_carte,
                                $request->request->get('SERVEUR'),
                            )
                        )) {
                            $query = 'INSERT INTO CARTE_PROJET (PK_CARTE_PROJET, CARTP_ID, CARTP_NOM, CARTP_FORMAT, CARTP_FK_STOCKAGE_CARTE, CARTP_UTILISATEUR '.($cartp_utilisateur_email != '' ? ', CARTP_UTILISATEUR_EMAIL' : '').') VALUES ('.$carte.', \''.substr(
                                    $request->request->get('SERVEUR').'_'.$carte.'_'.$request->request->get('PATH'),
                                    0,
                                    30
                                ).'\', \''.$request->request->get(
                                    "MAP_TITLE"
                                ).'\', 2, '.$stockage_carte.', '.$userId.''.($cartp_utilisateur_email != '' ? ',\''.$cartp_utilisateur_email.'\'' : '').')';
                            if ($dao->Execute($query)) {
                                $uuid_prodige = uniqid();
                                $urlAdmin = $this->generateUrl('catalogue_geosource_cartepersoInit', array(
                                    "uuid" => $uuid_prodige,
                                    "pk_carte_projet" => $carte,
                                ));
                                header('location:'.$urlAdmin);
                                exit();
                            } else {
                                $errormsg = 'Erreur lors de la cr&eacute;ation de l\'enregistrement de la carte';
                            }
                        } else {
                            $errormsg = 'Erreur lors de la cr&eacute;ation de l\'emplacement de stockage';
                        }
                    } else {
                        $errormsg = 'Impossible d\'obtenir les identifiants pour le stockage de la carte';
                    }
                } else {
                    $errormsg = 'Il existe d&eacute;j&agrave; une fiche de m&eacute;tadonn&eacute;es pour cette carte';
                }
            }
        }
       

        $strHtml = "<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">
                        <html>
                          <head>
                            <title>Carte Personnelle</title>
                            <META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=UTF-8\">
                            <!-- IE specific : forcing IE9 Document model because ext 3.0 incompatibility with IE10 -->
                            <META HTTP-EQUIV=\"X-UA-Compatible\" CONTENT=\"IE=9\"/>".
            "<link rel=\"stylesheet\" type=\"text/css\" href=\"/Scripts/ext3.0/resources/css/ext-all.css\">".
            ((defined("PRO_CSS_FILE") && PRO_CSS_FILE != "")
                ? "<link rel=\"stylesheet\" type=\"text/css\" href=".PRO_CSS_FILE.">"
                : "").
            "<link rel='stylesheet' type='text/css' href=\"/cartepersobundle/css/carteperso.css\"/>".
            "<link rel='stylesheet' type='text/css' href=\"/cartepersobundle/css/Ext.ux.form.LovCombo.css\"/>".
            "<script  src=\"/Scripts/ext3.0/adapter/ext/ext-base.js\"> </script>".
            "<script  src=\"/Scripts/ext3.0/ext-all.js\"> </script>".
            "<script  src=\"/Scripts/ext3.0/ext-lang-fr.js\"> </script>".
            "<script  src=\"/Scripts/extjs_overload.js\"> </script>".
            "<script  src=\"/Scripts/Ext.ux.form.LovCombo.js\"> </script>".
            "<script  src=\"/Scripts/carteperso.js\"> </script>".
            "<script  src=\"/Scripts/geonetwork.js\"> </script>".
            "<script src=\"/bundles/fosjsrouting/js/router.js\"> </script>".
            "<script src=\"".$this->generateUrl('fos_js_routing_js', array('callback' => 'fos.Router.setData')
            )."\"> </script>".
            "<script language='javascript'>".
            "var urlInit = '".$urlInit."';".
            "var userId = '".$userId."';
                            </script>".
            "</head>".
            "<body onload=\"carteperso_init('".$urlInit."', '".$userId."');\">".
            "<div id=\"divglobal\">";

        //Création de carte personnelle
        $strHtml .= "<div id=\"cartecreation\" style=\"display:none\"> <div class=\"titre\"><h2>Cr&eacute;er une carte personnelle</h2></div>
                                <div class=\"errormsg\" id=\"errormsg\">".
            (isset($errormsg) ? $errormsg : "").
            "</div>
                                <div class=\"formulaire\">
                                  <form action=\"\" method=\"POST\" name=\"formCartePerso\" target=\"Cartographie_dynamique\">
                                  <input type=\"hidden\" name=\"PN_ADMIN_MODE\" value = \"CARTEPERSO\"/>
                                  <input type=\"hidden\" name=\"PATH\" value = \"\"/>".
            ($cartp_utilisateur_email != ""
                ? "<input type=\"hidden\" name=\"cartp_utilisateur_email\" value = \"".$cartp_utilisateur_email."\"/>"
                : "");

        if ($dao) {
            $query = 'SELECT PK_ACCES_SERVEUR, ACCS_ID, ACCS_ADRESSE_ADMIN FROM ACCES_SERVEUR ORDER BY ACCS_ID';
            $rs = $dao->BuildResultSet($query);
            $countAdresses = 0;
            $strAdresses = "";
            $strJs = "var tabData = new Array();";
            for ($rs->First(); !$rs->EOF(); $rs->Next()) {
                $pk_acc_server = $rs->Read(0);
                $selected = '';
                if ($request->request->get('SERVEUR', '') == $rs->Read(0)) {
                    $selected = 'selected';
                }
                $strAdresses .= '<option value=\"'.$pk_acc_server.'\" '.$selected.'>'.$rs->Read(1).'</option>';
                $adresses[$pk_acc_server] = $rs->Read(2);
                $countAdresses++;
                $strJs .= " tabData[".$pk_acc_server."] = new Array();";
                $queryMaps = 'SELECT stkcard_path FROM stockage_carte LEFT JOIN carte_projet on stockage_carte.pk_stockage_carte = carte_projet.cartp_fk_stockage_carte'.
                    ' where stk_server = '.$pk_acc_server.
                    ' and  cartp_format in (0, 2)'; //cartes perso et cartes dynamiques
                $queryMaps .= ' order by stkcard_path';
                //error_log($query);
                $rsMaps = $dao->BuildResultSet($queryMaps);
                $i = 0;
                for ($rsMaps->First(); !$rsMaps->EOF(); $rsMaps->Next()) {
                    if (strrpos($rsMaps->Read(0), "/")) {
                        $mapName = substr(
                            $rsMaps->Read(0),
                            strrpos($rsMaps->Read(0), "/") + 1,
                            strlen($rsMaps->Read(0))
                        );
                    } else {
                        $mapName = $rsMaps->Read(0);
                    }
                    $strJs .= " tabData[".$pk_acc_server."][".$i."] ='".substr($mapName, 0, -4)."';\n";
                    $i++;
                }
            }
        }

        $strHtml .= "<table>
                                    <tr ".($countAdresses == 1 ? "style=\"display:none\"" : "")." >
                                      <th> Serveur </th>
                                      <td>".($countAdresses == 1
                ? "<input type=\"hidden\" name=\"SERVEUR\" ID=\"SERVEUR\" value=\"".$pk_acc_server."\"/>"
                : "<select name=\"SERVEUR\" ID=\"SERVEUR\">".$strAdresses." </select>")."
                                          <script language=\"javascript\">".$strJs."
                                              var serveurs = new Array();";
        foreach ($adresses as $key => $value) {
            $strHtml .= "serveurs[".$key."] = '".$value."';\n";
        }
        $strHtml .= "             </script>
                                        </td>
                                      </tr>
                                      <tr id=\"map_item\" >
                                        <th> Nom de la carte </th>
                                        <td>
                                          <input type='text' name=\"MAP_TITLE\" id=\"MAP_TITLE\" value=\"\" size=\"50\" maxlength=\"1024\">
                                          <input type='hidden' name =\"MAP\" id=\"MAP\" value=\"\"/>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td colspan=\"2\" class=\"validation\">
                                          <input type=\"button\" name=\"VALIDATION\" value=\"Suivant\" onclick=\"carteperso_ajouter()\">
                                        </td>
                                      </tr>
                                   </table>
                               </form>
                           </div>
                        </div>";

        //liste des cartes
        $strHtml .= " <div id=\"cartelist\" style=\"display:none\">
                                        <div class=\"titre\">
                                            <h2> Gérer la liste des cartes personnelles </h2>
                                        </div>
                                      <div class=\"liste\">";
        if ($dao) {
            $query = 'SELECT pk_carte_projet, cartp_nom, stkcard_path, pk_stockage_carte, accs_adresse_admin, path_administration, accs_login_admin, accs_pwd_admin,'.
                ' accs_adresse, accs_service_admin, path_consultation, cartp_utilisateur, usr_nom,  usr_prenom, cartp_fk_fiche_metadonnees, cartep_etat FROM'.
                ' CARTE_PROJET LEFT JOIN STOCKAGE_CARTE on CARTE_PROJET.cartp_fk_stockage_carte = STOCKAGE_CARTE.pk_stockage_carte'.
                ' LEFT JOIN ACCES_SERVEUR on STOCKAGE_CARTE.stk_server = ACCES_SERVEUR.pk_acces_serveur '.
                ' LEFT JOIN UTILISATEUR on CARTE_PROJET.cartp_utilisateur = UTILISATEUR.pk_utilisateur where cartp_format = 2'.
                ' and (cartep_etat = 0 OR cartep_etat = 1) '.
                'and CARTE_PROJET.cartp_utilisateur='.$userId.($cartp_utilisateur_email != "" ? ' AND CARTE_PROJET.cartp_utilisateur_email=\''.$cartp_utilisateur_email.'\'' : '');
            $query .= ' order by cartp_nom';
            $rs = $dao->BuildResultSet($query);
            if ($rs->GetNbRows() > 0) {
                for ($rs->First(); !$rs->EOF(); $rs->Next()) {
                    $objetCarte = $rs->Read(0);
                    $strHtml .= "<div id=\"carteprojet_".$rs->Read(0)."\" class=\"carteprojet\">";
                    $urlConsult = $this->container->getParameter('PRODIGE_URL_FRONTCARTO')."/1/".$rs->Read(2);
                    //$urlAdmin = ($request->server->get("HTTPS", "off") == "on" ? "https" : "http")."://".$rs->Read(4).$rs->Read(5).$rs->Read(2);
                    $uuid_prodige = "";
                    $conn_prodige = $this->getProdigeConnection('carmen');
                    $dao_prodige = new DAO($conn_prodige, 'carmen');
                    if ($dao_prodige) {
                        $mapfile_name = explode(".", $rs->Read(2));
                        $query_prodige = "SELECT map_wmsmetadata_uuid FROM map WHERE map_file = ?";
                        $rs_prodige = $dao_prodige->buildResultSet($query_prodige, array($mapfile_name[0]));
                        if ($rs_prodige->GetNbRows() > 0) {
                            $rs_prodige->First();
                            $uuid_prodige = $rs_prodige->Read(0);
                        }
                    }
                    //if uuid not set (not existant map), generate one
                    if ($uuid_prodige == "") {
                        $uuid_prodige = uniqid();
                    }
                    $urlAdmin = $this->generateUrl('catalogue_geosource_cartepersoInit', array(
                        "uuid" => $uuid_prodige,
                        "pk_carte_projet" => $objetCarte,
                    )
                    );//($request->server->get("HTTPS", "off") == "on" ? "https" : "http")."://".$rs->Read(4).$rs->Read(5).$uuid_prodige;
                    $strHtml .= "<div class=\"cmd\">";
                    $metadataId = ($rs->Read(14) != "" ? $rs->Read(14) : 0);

                    if ($user->isConnected() /*$userId != $_SESSION["userIdInternet"]*/) {
                        $url_edit = $this->generateUrl(
                                'catalogue_carteperso'
                            ).'?mode=EditMetadata&pk_carteprojet='.$objetCarte.'&fiche_meta='.$metadataId;
                        $strHtml .= '<a href="javascript:editMetadata(\''.$url_edit.'\');" >Éditer la métadonnée</a>';
                        //$strHtml.= "<input type=\"button\" name=\"EditMetadata\" value=\"Éditer une métadonnée\" onclick=\"carteperso_EditFicheMetadata('".$objetCarte."',".($rs->Read(14)+0).")\"/>";
                        if (intval($rs->Read(15)) == 0) {
                            $strHtml .= " <input type=\"button\" name=\"Proposer\" value=\"Proposer\" onclick=\"carteperso_proposer('".$objetCarte."','".htmlentities(
                                    $rs->Read(1),
                                    ENT_QUOTES,
                                    "UTF-8"
                                )."', ".($user->GetUserGeneric(
                                )).", ".($cartp_utilisateur_email != "" ? "'".$cartp_utilisateur_email."'" : "''").")\"/>";
                        } else {
                            $strHtml .= " <input type=\"button\" name=\"Proposer\" value=\"Proposer\" style = 'visibility : hidden'/>";
                        }
                    }
                    $strHtml .= " <input type=\"button\" name=\"Edit\" value=\"Modifier\" onclick=\"window.open('".$urlAdmin."')\"/>";
                    $read14 = ($rs->Read(14) != "") ? $rs->Read(14) + 0 : 0;
                    $strHtml .= " <input type=\"button\" name=\"Del\" value=\"Supprimer\" onclick=\"carteperso_delMap(".$rs->Read(
                            0
                        ).', '.$rs->Read(3).','.$read14.')"/>';
                    $strHtml .= "</div>";
                    $strHtml .= "<h4 onclick=\"window.open('".$urlConsult."')\">".str_replace(
                            '%u2019',
                            '\'',
                            $rs->Read(1)
                        ).
                        ($user->HasTraitement('ADMINISTRATION')
                            ? "<span class=\"etat\">(auteur : ".$rs->Read(12)." ".$rs->Read(13).") </span> "
                            : "").
                        "</h4>";
                    $strHtml .= "</div>";
                }
            } else {
                $strHtml .= "<div class=\"etat\" style=\"padding:5px\">Aucune carte disponible. Pour créer une carte, cliquez sur \"Nouvelle carte\"</div>";
            }
        }
        $strHtml."</div>";

        if ($user->isConnected()/*$userId != $_SESSION["userIdInternet"]*/) {
            $strHtml .= "<div id=\"contexte\"   class=\"formulaire\" style=\"display:none\"><div class=\"titre\"><h2>Charger un contexte</h2></div>
                                            <form method=\"post\" id=\"formContext\" name=\"formContext\" target=\"Cartographie_dynamique\" enctype=\"multipart/form-data\" onsubmit=\"return check();\" action=\"".$actionContexte."?mode=redirect\">
                                                    <table>
                                                    <tr>
                                                        <th>Contexte (format ows)</th>
                                                        <td><input type=\"file\" name=\"uploadedfile\" size=\"40\"> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class=\"validation\" colspan=\"2\">
                                                            <input type=\"button\" value=\"Suivant\"onclick=\" carteperso_loadcontext()\">
                                                        </td>
                                                    </tr>
                                                </table>
                                            </form>
                                        <div>";
        }

        //liste des demandes d'intégration
        //liste des cartes
        if ($user->isConnected() /*$userId != $_SESSION["userIdInternet"]*/) {
            $strHtml .= " <div id=\"demandelist\" style=\"display:none\">
                                            <div class=\"titre\">
                                                <h2>Liste des demandes d'intégration</h2>
                                            </div>
                                            <div class=\"liste\">";
            if ($dao) {
                $query = 'SELECT pk_carte_projet, cartp_nom, stkcard_path, pk_stockage_carte, accs_adresse_admin, path_administration, accs_login_admin, accs_pwd_admin,'.
                    ' accs_adresse, accs_service_admin, path_consultation, cartp_utilisateur, usr_nom,  usr_prenom, cartp_description, '.
                    ' ssdcart_fk_sous_domaine, pk_domaine, dom_rubric, cartp_fk_fiche_metadonnees, cartp_administrateur FROM'.
                    ' CARTE_PROJET LEFT JOIN STOCKAGE_CARTE on CARTE_PROJET.cartp_fk_stockage_carte = STOCKAGE_CARTE.pk_stockage_carte'.
                    ' LEFT JOIN ACCES_SERVEUR on STOCKAGE_CARTE.stk_server = ACCES_SERVEUR.pk_acces_serveur '.
                    ' LEFT JOIN UTILISATEUR on CARTE_PROJET.cartp_utilisateur = UTILISATEUR.pk_utilisateur '.
                    ' LEFT JOIN SSDOM_VISUALISE_CARTE on CARTE_PROJET.pk_carte_projet = SSDOM_VISUALISE_CARTE.SSDCART_FK_CARTE_PROJET'.
                    ' LEFT JOIN DOM_SDOM on DOM_SDOM.pk_sous_domaine = SSDOM_VISUALISE_CARTE.ssdcart_fk_sous_domaine'.
                    ' where cartp_format = 2'.
                    ' and cartep_etat = 1';

                $query .= ' order by cartp_nom';
                $strHtmlList = "";
                $rs = $dao->BuildResultSet($query);
                if ($rs->GetNbRows() > 0) {
                    for ($rs->First(); !$rs->EOF(); $rs->Next()) {
                        $tabAdministrateurs = explode(",", $rs->Read(19));
                        if ($user->HasTraitement('ADMINISTRATION') || in_array($userId, $tabAdministrateurs)) {
                            $objetCarte = $rs->Read(0);

                            $uuid_prodige = "";
                            $conn_prodige = $this->getProdigeConnection('carmen');
                            $dao_prodige = new DAO($conn_prodige, 'carmen');
                            if ($dao_prodige) {
                                $mapfile_name = explode(".", $rs->Read(2));
                                $query_prodige = "SELECT map_wmsmetadata_uuid FROM map WHERE map_file = ?";
                                $rs_prodige = $dao_prodige->buildResultSet($query_prodige, array($mapfile_name[0]));
                                if ($rs_prodige->GetNbRows() > 0) {
                                    $rs_prodige->First();
                                    $uuid_prodige = $rs_prodige->Read(0);
                                }
                            }
                            //if uuid not set (not existant map), generate one
                            if ($uuid_prodige == "") {
                                $uuid_prodige = uniqid();
                            }
                            $urlAdmin = $this->generateUrl('catalogue_geosource_cartepersoInit', array(
                                "uuid" => $uuid_prodige,
                                "pk_carte_projet" => $objetCarte,
                            ));

                            $metadataId = ($rs->Read(18) != "" ? $rs->Read(18) : 0);

                            $url_edit = $this->generateUrl(
                                    'catalogue_carteperso'
                                ).'?mode=EditMetadata&pk_carteprojet='.$objetCarte.'&fiche_meta='.$metadataId;

                            $strHtmlList .= "<div id=\"carteprojet_".$rs->Read(0)."\" class=\"carteprojet\">";
                            $urlConsult = ($request->server->get(
                                    "HTTPS",
                                    "off"
                                ) == "on" ? "https" : "http")."://".$rs->Read(8).$rs->Read(10).$rs->Read(
                                    9
                                )."/".$rs->Read(2);
                            //$urlAdmin = ($request->server->get("HTTPS", "off") == "on" ? "https" : "http")."://".$rs->Read(4).$rs->Read(5).$rs->Read(2);
                            $strHtmlList .= "<div class=\"cmd\">";
                            $strHtmlList .= '<a href="javascript:editMetadata(\''.$url_edit.'\');" >Éditer la métadonnée</a>';

                            $strHtmlList .= " <input type=\"button\" name=\"Edit\" value=\"Accepter\" onclick=\"carteperso_Accepter('".$rs->Read(
                                    0
                                )."','".$rs->Read(17)."','".$rs->Read(16)."','".$rs->Read(15)."','".($rs->Read(
                                        18
                                    ) + 0)."' )\"/>";
                            $strHtmlList .= " <input type=\"button\" name=\"Del\" value=\"Refuser\" onclick=\"carteperso_Refuser('".$rs->Read(
                                    0
                                )."')\"/>";
                            $strHtmlList .= " <input type=\"button\" name=\"Edit\" value=\"Modifier\" onclick=\"window.open('".$urlAdmin."')\"/>";
                            $strHtmlList .= " <input type=\"button\" name=\"Del\" value=\"Supprimer\" onclick=\"carteperso_delMap(".$rs->Read(
                                    0
                                ).', '.$rs->Read(3).','.($rs->Read(18) + 0).')"/>';
                            $strHtmlList .= "</div>";
                            $strHtmlList .= "<h4 onclick=\"consultation_openInterface('".$urlConsult."')\">".str_replace(
                                    '%u2019',
                                    '\'',
                                    $rs->Read(1)
                                ).
                                ($user->HasTraitement('ADMINISTRATION')
                                    ? "<span class=\"etat\">(auteur : ".$rs->Read(12)." ".$rs->Read(13).") </span> "
                                    : "").
                                "</h4>".$rs->Read(14);
                            $strHtmlList .= "</div>";
                        }
                    }
                }
                if ($strHtmlList == "") {
                    $strHtmlList = "<div class=\"etat\" style=\"padding:5px\">Aucune demande en cours</div>";
                }
            }
            $strHtml .= $strHtmlList."</div>";
        }
        $strHtml .= "</div></div></div></div></div></div></body></html>";

        return new Response($strHtml);
    }

    // forward $data to $url via POST
    // if $data is null, send current request body instead
    // $proxy is used to use a proxy URL (not tested)
    protected function http_post($url)
    {
        $strHtml = "<html>
                        <head>
                        <body onload='document.formRedirect.submit();';>
                        <form name='formRedirect' action='".$url."' method='POST'>";
        foreach ($_POST as $key => $value) {
            $strHtml .= "<input type='hidden' name='".$key."' value='".$value."'>";
        }
        $strHtml .= "</form></body></html>";

        return $strHtml;
    }

    // copy physique du mapfile
    protected function duplicateMapfile($strMapfileFrom, $strMapfileDest)
    {
        return copy($strMapfileFrom, $strMapfileDest);
    }
}

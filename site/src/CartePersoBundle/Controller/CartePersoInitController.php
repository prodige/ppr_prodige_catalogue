<?php

namespace ProdigeCatalogue\CartePersoBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;


class CartePersoInitController extends BaseController {
    /**
     * 
     * @IsGranted("ROLE_USER")
     * @Route("/carteperso/init", name="catalogue_carteperso_init", options={"expose"=true})
     */
    public function cartePersoInitAction(Request $request) {

        $callback = $request->query->get("callback", "");
        $userId = User::GetUserId();

        if($userId != "" && is_numeric($userId)) {
            if(!is_dir(PRO_MAPFILE_PATH."carteperso/".$userId)) {
                mkdir(PRO_MAPFILE_PATH."carteperso/".$userId, 0770);
                if(!is_dir(PRO_MAPFILE_PATH."carteperso/".$userId)) {
                    echo $callback.'({ "success" : "false"})';
                    exit();
                }
            }
            if(!is_dir(PRO_MAPFILE_PATH."carteperso/".$userId."/etc")) {  
                //create symlink
                symlink(PRO_MAPFILE_PATH."etc", PRO_MAPFILE_PATH."carteperso/".$userId."/etc");
            }
            if(!file_exists(PRO_MAPFILE_PATH."carteperso/".$userId."/legend.html")) {
                symlink(PRO_MAPFILE_PATH."legend.html", PRO_MAPFILE_PATH."carteperso/".$userId."/legend.html");
            }

        }
        echo $callback.'({ "success" : "true"})';
        exit();
    }
}

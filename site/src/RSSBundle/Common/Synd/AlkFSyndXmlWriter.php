<?php
namespace ProdigeCatalogue\RSSBundle\Common\Synd;

/**
 * @package Alkanet_Class_Pattern
 *
 * @class AlkFSyndXmlWriter
 * Générateur de flux RSS 1.0, RSS2.0 et ATOM
 * FSyndXml pour Format de Syndication xml
 */
class AlkFSyndXmlWriter {

    /** ensemble des éléments channel */
    protected $channels = array();

    /** ensemble d'objet de type AlkFSyndXmlItem */
    protected $items = array();

    /** mémorise les informations du flux */
    protected $data = array();

    /** */
    protected $CDATAEncoding = array();  // The tag names which have to encoded as CDATA

    /** type de flux à générer */
    private $version = null;

    /**
     * Constructeur
     *
     * @param  strVersion  version de la file : ALK_RSS1 / ALK_RSS2 / ALK_ATOM
     */
    function __construct($strVersion = ALK_RSS2) {
        $this->version = $strVersion;

        $this->channels['title'] = $strVersion;
        $this->channels['link']  = 'http://www.alkante.com';

        // nom des tags à encoder dans CDATA
        $this->CDATAEncoding = array('description', 'content:encoded', 'summary');
    }

    /**
     * Mémorise l'élément channel
     *
     * @param strName     nom du tag channel
     * @param strContent  contenu du tag channel
     */
    public function setChannelElement($strName, $strContent) {
        $this->channels[$strName] = $strContent ;
    }

    /**
     * Set multiple channel elements from an array. Array elements
     * should be 'channelName' => 'channelContent' format.
     *
     * @param    array   array of channels
     * @return   void
     */
    public function setChannelElementsFromArray($elementArray) {
        if(!is_array($elementArray)) 
            return;
        foreach($elementArray as $elementName => $content) {
            $this->setChannelElement($elementName, $content);
        }
    }

    /**
     * Ecrit sur la sortie standard la file actuelle RSS/ATOM
     */
    public function genarate() {
        header("Content-type: text/xml");

        $this->printHead();
        $this->printChannels();
        $this->printItems();
        $this->printTale();
    }

    /**
     * Creation puis retourne une instance d'objet de type AlkFSyndXmlItem.
     *
     * @return object de type AlkFSyndXmlItem
     */
    public function createNewItem() {
        $Item = new AlkFSyndXmlItem($this->version);
        return $Item;
    }

    /**
     * Ajoute un élément de type AlkFSyndXmlItem à la classe
     *
     * @param  oItem  instance d'un objet de type AlkFSyndXmlItem
     */
    public function addItem($oItem) {
        $this->items[] = $oItem;
    }

    /**
     * Mémorise l'élément 'title'
     *
     * @param strTitle  value du tag
     */
    public function setTitle($strTitle) {
        $this->setChannelElement('title', $strTitle);
    }

    /**
     * Mémorise l'élément 'description'
     *
     * @param  strDesc  valeur du tag
     */
    public function setDescription($strDesc) {
        $this->setChannelElement('description', $strDesc);
    }

    /**
     * Mémorise l'élément 'link'
     *
     * @param  strLink  valeur du tag
     */
    public function setLink($strLink) {
        $strLink = mb_ereg_replace("&", "&amp;", $strLink);
        $this->setChannelElement('link', $strLink);
    }

    /**
     * Mémorise l'élément 'image'
     *
     * @param    strTitle  titre de l'image
     * @param    strLink   lien url de l'image
     * @param    strUrl    chemin url de l'image
     */
    public function setImage($strTitle, $strLink, $strUrl) {
        $strLink = mb_ereg_replace("&", "&amp;", $strLink);
        $this->setChannelElement('image', array('title'=>$strTitle, 'link'=>$strLink, 'url'=>$strUrl));
    }

    /**
     * Mémorise l'élément 'about' pour RSS 1.0 uniquement
     *
     * @param strUrl   valeur du tag about
     */
    public function setChannelAbout($strUrl) {
        $this->data['ChannelAbout'] = $strUrl;
    }

    /**
     * Génère puis retourne un UUID
     *
     * @param      key     clé optionel pour calculer l'uuid
     * @param      prefix  préfixe optionel de l'uuid
     * @return     string
     */
    public function uuid($key = null, $prefix = '') {
        $key = ($key == null)? uniqid(rand()) : $key;
        $chars = md5($key);
        $uuid  = substr($chars,0,8) . '-';
        $uuid .= substr($chars,8,4) . '-';
        $uuid .= substr($chars,12,4) . '-';
        $uuid .= substr($chars,16,4) . '-';
        $uuid .= substr($chars,20,12);

        return $prefix . $uuid;
    }

    /**
     * Ecrit l'entete xml du flux
     */
    private function printHead() {
        $out  = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL;

        if($this->version == ALK_RSS2) {
            $out .= '<rss version="2.0"'.
                ' xmlns:content="http://purl.org/rss/1.0/modules/content/"'.
                ' xmlns:wfw="http://wellformedweb.org/CommentAPI/"'.
                '>'.PHP_EOL;
        }
        elseif($this->version == ALK_RSS1) {
            $out .= '<rdf:RDF'.
                ' xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"'.
                ' xmlns="http://purl.org/rss/1.0/"'.
                ' xmlns:dc="http://purl.org/dc/elements/1.1/"'.
                '>'.PHP_EOL;
        }
        else if($this->version == ALK_ATOM) {
            $out .= '<feed xmlns="http://www.w3.org/2005/Atom" xmlns:georss="http://www.georss.org/georss" xmlns:gml="http://www.opengis.net/gml" xmlns:inspire_dls="http://inspire.ec.europa.eu/schemas/inspire_dls/1.0">'.PHP_EOL;
        }
        echo $out;
    }

    /**
     * Ecrit et ferme le tag channel
     */
    private function printTale() {
        if($this->version == ALK_RSS2) {
            echo '</channel>'.PHP_EOL.'</rss>';
        }
        elseif( $this->version == ALK_RSS1 ) {
            echo '</rdf:RDF>';
        }
        else if( $this->version == ALK_ATOM ) {
            echo '</feed>';
        }
    }

    /**
     * Création d'un noeud xml
     * Retourne une chaine contenant le tag xml du noeud
     *
     * @param    tagName     nom du tag
     * @param    tagContent  valeur du tag ou tableau de type 'tagName' => 'tagValue'
     * @param    attributes   Attributs au format 'attrName' => 'attrValue' format, null par défaut
     * @return   string
     */
    private function makeNode($tagName, $tagContent, $attributes = null) {
        $nodeText = '';
        $attrText = '';

        if(is_array($attributes)) {
            foreach ($attributes as $key => $value) {
                $attrText .= " $key=\"$value\" ";
            }
        }

        if(is_array($tagContent) && $this->version == ALK_RSS1) {
            $attrText = ' rdf:parseType="Resource"';
        }

        $attrText .= (in_array($tagName, $this->CDATAEncoding) && $this->version == ALK_ATOM)? ' type="html" ' : '';
        $nodeText .= (in_array($tagName, $this->CDATAEncoding))? "<{$tagName}{$attrText}><![CDATA[" : "<{$tagName}{$attrText}>";

        if( is_array($tagContent) ) {
            foreach ($tagContent as $key => $value) {
                $nodeText .= $this->makeNode($key, $value);
            }
        }
        else {
            $nodeText .= (in_array($tagName, $this->CDATAEncoding))? $tagContent : $tagContent; //htmlentities($tagContent);
        }

        $nodeText .= (in_array($tagName, $this->CDATAEncoding))? "]]></$tagName>" : "</$tagName>";

        return $nodeText.PHP_EOL;
    }

    /**
     * Ecrit le contenu de la chaine
     */
    private function printChannels() {
        // début tag channel
        switch ($this->version) {
            case ALK_RSS2:
                echo '<channel>' . PHP_EOL;
                break;
            case ALK_RSS1:
                echo ( isset($this->data['ChannelAbout']))
                ? "<channel rdf:about=\"{$this->data['ChannelAbout']}\">"
                : "<channel rdf:about=\"{$this->channels['link']}\">";
                break;
        }

        // éléments du channel
        foreach ($this->channels as $key => $value) {
            if( $this->version == ALK_ATOM && $key == 'link' ) {
                // link ATOM
                echo $this->makeNode($key,'',array('href'=>$value));
                // id ATOM
                echo $this->makeNode('id',$this->uuid($value,'urn:uuid:'));
            }
            else {
                echo $this->makeNode($key, $value);
            }
        }

        //RSS 1.0 : tag special <rdf:Seq>
        if($this->version == ALK_RSS1) {
            echo "<items>".PHP_EOL."<rdf:Seq>".PHP_EOL;
            foreach ($this->items as $item) {
                $thisItems = $item->getElements();
                echo "<rdf:li resource=\"{".$thisItems['link']['content']."}\"/>".PHP_EOL;
            }
            echo "</rdf:Seq>".PHP_EOL."</items>".PHP_EOL."</channel>".PHP_EOL;
        }
    }

    /**
     * Ecrit le contenu complet d'un tag item
     */
    private function printItems() {
        foreach ($this->items as $item) {
            $thisItems = $item->getElements();

            //the argument is printed as rdf:about attribute of item in rss 1.0
            echo $this->startItem($thisItems['link']['content']);

            foreach ($thisItems as $feedItem ) {
                echo $this->makeNode($feedItem['name'], $feedItem['content'], $feedItem['attributes']);
            }
            echo $this->endItem();
        }
    }

    /**
     * Ecrit et ouvre un tag item
     *
     * @param about  valeur du param about, pour RSS 1.0 seulement
     */
    private function startItem($about = false) {
        if( $this->version == ALK_RSS2 ) {
            echo '<item>'.PHP_EOL;
        }
        elseif( $this->version == ALK_RSS1 ) {
            if( $about ) {
                echo "<item rdf:about=\"$about\">".PHP_EOL;
            }
            else {
                die("L'élémént about est requis en RSS 1.0 pour le tag item.");
            }
        }
        else if( $this->version == ALK_ATOM ) {
            echo "<entry>".PHP_EOL;
        }
    }

    /**
     * écrit et ferme le tag item
     */
    private function endItem() {
        if( $this->version == ALK_RSS2 || $this->version == ALK_RSS1 ) {
            echo '</item>' . PHP_EOL;
        }
        else if( $this->version == ALK_ATOM ) {
            echo "</entry>" . PHP_EOL;
        }
    }
}
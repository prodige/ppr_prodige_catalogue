<?php

namespace ProdigeCatalogue\RSSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route; ; 
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Prodige\ProdigeBundle\Controller\BaseController;

use ProdigeCatalogue\RSSBundle\Common\FeedWriter\Feed;
use ProdigeCatalogue\RSSBundle\Common\FeedWriter\Item;
use ProdigeCatalogue\RSSBundle\Common\FeedWriter\ATOM;

use Prodige\ProdigeBundle\DAOProxy\DAO;

/**
 * TopAtom Controller
 * @author alkante <support@alkante.com>
 * @Route("/rss")
 *
 */

class TopAtomController extends BaseController {
    
    /**
     
     * @Route("/atomfeed/topatom", name="catalogue_atomfeed_topatom", options={"expose"=true} )
     * #Method({"GET", "POST"})
     * #Génération des flux ATOM
     * */
    public function topAtomAction(Request $request) {
        //require_once('../include/FeedWriter/Feed.php');
        //require_once('../include/FeedWriter/Item.php');
        //require_once('../include/FeedWriter/ATOM.php');

        //date_default_timezone_set('UTC');

        //use \FeedWriter\ATOM;


        //require_once('include/ClassSyndXml.php');
        //require_once('../parametrage.php');

        // elements de recherche founit par le flux search..
        //$q = (isset($_GET["q"]) ? $_GET["q"] : "");
        $q = $request->query->get("q", "");

        //$AdminPath = "../Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");

        //$dao = new DAO();
        $conn = $this->getDoctrine()->getConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');

        if($dao) {

            $oSyndWriter = new ATOM;

            $oSyndWriter->addNamespace("inspire_dls", "http://inspire.ec.europa.eu/schemas/inspire_dls/1.0");
            $oSyndWriter->addNamespace("georss", "http://www.georss.org/georss");
            $oSyndWriter->addNamespace("gml", "http://www.opengis.net/gml");

            // construction de la partie flux de la plate forme
            // recherche des info de metadonnees de flux  de service
            $rs = $dao->BuildResultSet("SELECT data,uuid from public.metadata where (data IS NOT NULL and data!='') and ".
                "metadata.id = ?", array(PRO_DOWNLOAD_METADATA_ID));
            $rs->First();

            $XML_FicheMetadataService = "";
            $XML_FicheMetadataService = $rs->ReadHtml(0);
            $uuid_metadataService =  $rs->Read(1);

            $version  = "1.0";
            $encoding = "UTF-8";
            $domMetadataService = new \DOMDocument($version, $encoding);
            $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
            $metadata_data = $entete.$XML_FicheMetadataService;
            $domMetadataService->loadXML($metadata_data);

            $xpath = new \DOMXpath($domMetadataService);

            /*****************
             //serach the titre du flux
             *******************/

            $TitreFlux = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString");
            if($TitreFlux && $TitreFlux->length > 0) {
                $titreFluxService = $TitreFlux->item(0)->nodeValue;
            } else {
                $titreFluxService = "Aucun titre identifié";
            }
            $oSyndWriter->setTitle($titreFluxService);

            /*****************
             *  resume du flux
             ********************/

            $ResumeFlux = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:abstract/gco:CharacterString");
            if($ResumeFlux && $ResumeFlux->length > 0) {
                $strResumeFlux = $ResumeFlux->item(0)->nodeValue;
            } else {
                $strResumeFlux = "Aucun résumé identifié";
            }
            $oSyndWriter->setChannelElement("subtitle", $strResumeFlux);

            /*****************
             * url de opensearch
             *******************/
 
            //$strUrlOpenSearch = PRO_CATALOGUE_URLBASE.'atomfeed/search.php';
            $strUrlOpenSearch = $this->generateUrl('catalogue_atomfeed_search', array(), self::ABSOLUTE_URL);
            $oSyndWriter->setAtomLink($strUrlOpenSearch, "search", "application/opensearchdescription+xml", "", $titreFluxService." (Open Search Description)");

            /*****************
             * url de la metadonnees de flux atom
             *******************/
            $strUrlFluxMetadonneService = rtrim(PRO_GEONETWORK_URLBASE, "/")."/srv/".$uuid_metadataService;
            $oSyndWriter->setAtomLink($strUrlFluxMetadonneService, "describedby", "application/vnd.iso.19139+xml");


            /*****************
             * url du flux
             *******************/
            //$strUrlFlux = PRO_CATALOGUE_URLBASE."atomfeed/topatom.php";
            $strUrlFlux = $this->generateUrl('catalogue_atomfeed_topatom', array(), self::ABSOLUTE_URL);
            $oSyndWriter->setAtomLink($strUrlFlux, "self", "application/atom+xml", "fr", "This document");

            /*****************
             * Rights
             ******************/

            $rights = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:resourceConstraints/gmd:MD_LegalConstraints/gmd:useLimitation/gco:CharacterString");
            if($rights && $rights->length > 0) {
                $strRights = $rights->item(0)->nodeValue;
            } else {
                $strRights = "Aucune limite de droit identifiée";
            }
            $oSyndWriter->setChannelElement("rights", $strRights);

            /**********************
             * Auteur
             **********************/
            /**********
             * org resp.
             **********/
            $org = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:pointOfContact/gmd:CI_ResponsibleParty/gmd:organisationName/gco:CharacterString");
            if($org && $org->length>0){
                $strOrganiostaionResp = $org->item(0)->nodeValue;
            } else {
                $strOrganiostaionResp = "Aucune information sur l'organisation responsable";
            }
            /**********
             * email
             **********/
            $email = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:pointOfContact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:electronicMailAddress/gco:CharacterString");

            if($email && $email->length > 0) {
                $strEmail = $email->item(0)->nodeValue;
            } else {
                $strEmail = "Aucune email de contact identifié";
            }

            $tabElmentName["name"] = $strOrganiostaionResp;
            $tabElmentName["email"] = $strEmail;
            $oSyndWriter->setChannelElement('author', $tabElmentName);



            /*************************************************
             * Recuperation de la liste des uuid filtrés avec les parametre transmis avec l'opensearch
             *************************************************/

            //http://www.geopal.org/geonetwork/srv/fre/q?fast=true&&any=eau&hitsperpage=20
            $tabUuidSearched = array();
            if($q != "") {
                $strUrl = rtrim(PRO_GEONETWORK_URLBASE, "/")."/srv/fre/q?fast=true&&any=".$q;
                $xmlDoc = new \DOMDocument('1.0', 'UTF-8');
                if($xmlDoc->load($strUrl) === TRUE) {
                    //$tabElementAtom = json_decode(file_get_contents("http://".$accs_adresse_admin."/PRRA/administration_get_parametrage_fluxatom.php?service=getSettingsFluxAtom"),true);
                    $tagUuid = $xmlDoc->getElementsByTagName('uuid');
                    $strMetadataId = "";
                    if($tagUuid->length>0){// si il y a des fiche serie de donnée filles
                        for ( $i=0; $i<$tagUuid->length; $i++ ) {
                            $tabUuidSearched[] = $tagUuid->item($i)->nodeValue;
                        }
                    }
                }
            }
            /*************************************************
             * Dataset feed / boucle sur les séries de données
             *************************************************/
            // si  la série de donnée est téléchargeable ( couchd_download=1)
            $rs3 = $dao->BuildResultSet("SELECT couchd_download, data, uuid, meta.changedate, couchd_type_stockage from public.metadata meta
                                          left join catalogue.fiche_metadonnees fm on cast(fm.fmeta_id as INTEGER) = meta.id
                                          left join catalogue.couche_donnees cd on cd.pk_couche_donnees = fm.fmeta_fk_couche_donnees".
                                          //métadonnées publiées = operationallowed 0 (view) pour groups 1 (All Internet)
                                          " inner join public.operationallowed on ( meta.id = operationallowed.metadataid and operationallowed.operationid=0 and operationallowed.groupid=1)
                                          where (data IS NOT NULL and data!='') and couchd_download=1 order by couchd_nom");
            $changeDate = -1;
            // boucle sur les series de de données
            $tabExistingUuid =array();
            for($rs3->First(); !$rs3->EOF(); $rs3->Next()) {
                $uuid = $rs3->Read(2);
                
                if(!in_array($uuid, $tabExistingUuid) && (($q == "") || (!empty($tabUuidSearched) && in_array($rs3->Read(2), $tabUuidSearched)))) {
                    $tabExistingUuid[]=$uuid;
                    $couchd_type_stockage ="";
                    $couchd_type_stockage = $rs3->Read(4);
                    $data = $rs3->ReadHtml(1);
                    
                    
                    $domMetadataItem = new \DOMDocument($version, $encoding);
                    $metadata_data = $entete.$data;
                    $domMetadataItem->loadXML($metadata_data);
                    $xpathItem = new \DOMXpath($domMetadataItem);

                    /*****************
                     *titre series de donnees
                     *******************/
                    $titleItem = @$xpathItem->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString");
                    if($titleItem && $titleItem->length > 0) {
                        $strTitleItem = $titleItem->item(0)->nodeValue;
                    } else {
                        $strTitleItem = "Aucun titre identifié sur cette série de données";
                    }
                    $newItem = $oSyndWriter->createNewItem();
                    $newItem->setTitle($strTitleItem);

                    /*****************
                     *id url flux de series de donnees
                     *******************/
                    //$idFluxSerieDonnees =  PRO_CATALOGUE_URLBASE."atomfeed/atomdata.php?uuid=".$rs3->Read(2);
                    $idFluxSerieDonnees = $this->generateUrl('catalogue_atomfeed_atomdata', array('uuid' => $rs3->Read(2)), self::ABSOLUTE_URL);
                    $newItem->addElement("inspire_dls:spatial_dataset_identifier_code", $idFluxSerieDonnees);


                    /*****************
                     * url de la plate forme
                     ********************/

                    $FluxPlateForme =  rtrim(PRO_GEONETWORK_URLBASE, "/");
                    $newItem->addElement("inspire_dls:spatial_dataset_identifier_namespace", $FluxPlateForme);



                    /*****************
                     * url de la métadonnée de série
                     ********************/

                    //$strUrlFluxMetadonneSerie = PRO_GEONETWORK_URLBASE.$PRO_GEONETWORK_DIRECTORY."/srv/".$rs3->Read(2);
                    $strUrlFluxMetadonneSerie = rtrim(PRO_GEONETWORK_URLBASE, "/")."/srv/".$rs3->Read(2);
                    $newItem->addElement("link", '', array('href'=>$strUrlFluxMetadonneSerie, 'rel'=>'describedby', 'type'=>'application/vnd.iso.19139+xml'), FALSE, TRUE);

                    /*****************
                     * url du flux de série de données
                     ********************/
                    if($couchd_type_stockage != -1) { // cas des serie de données téléchageables
                        //$strUrlFluxSerieDonnees = PRO_CATALOGUE_URLBASE."atomfeed/atomdataset.php?uuid=".$rs3->Read(2);
                        $strUrlFluxSerieDonnees = $this->generateUrl('catalogue_atomfeed_atomdataset', array('uuid' => $rs3->Read(2)), self::ABSOLUTE_URL);
                        $newItem->addElement('link','', array('href'=>$strUrlFluxSerieDonnees, 'rel'=>'alternate', 'type'=>'application/atom+xml', 'hreflang'=>'fr', 'title'=>'Feed containing the pre-defined dataset'), FALSE, TRUE);
                        $newItem->addElement('id', $oSyndWriter::uuid($strUrlFluxSerieDonnees,'urn:uuid:'));
                    } else { // cas d'un ensemble de série de données
                        //$strUrlFluxEnsembleSerieDonnees = PRO_CATALOGUE_URLBASE."atomfeed/atomdataset.php?uuid=".$rs3->Read(2);
                        $strUrlFluxEnsembleSerieDonnees = $this->generateUrl('catalogue_atomfeed_atomdataset', array('uuid' => $rs3->Read(2)), self::ABSOLUTE_URL);
                        $newItem->addElement('link','', array('href'=>$strUrlFluxEnsembleSerieDonnees, 'rel'=>'alternate', 'type'=>'application/atom+xml', 'hreflang'=>'fr', 'title'=>'Feed containing the pre-defined dataset'), FALSE, TRUE);
                        $newItem->addElement('id', $oSyndWriter::uuid($strUrlFluxEnsembleSerieDonnees,'urn:uuid:'));
                    }

                    /*****************
                     * georss
                     ********************/

                    // recherche des coordonn�es
                    //ConnectionFactory::BeginTransaction();
                    $dao->beginTransaction();
                    //$dao = new DAO();
                    $dao->setSearchPath("public,catalogue");
                    //$strSql = "SELECT data from metadata where uuid='".$rs3->Read(2)."'";
                    $strSql = "SELECT data from metadata where uuid=?";
                    $rs4 = $dao->BuildResultSet($strSql, array($rs3->Read(2)));
                    for($rs4->First(); !$rs4->EOF(); $rs4->Next()) {
                        $fiche_metadata_data   = $rs4->ReadHtml(0);
                    }
                    $version  = "1.0";
                    $encoding = "UTF-8";
                    $metadata_doc = new \DOMDocument($version, $encoding);
                    //chargement de la métadonnée
                    $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
                    $metadata_data = $entete.$fiche_metadata_data;
                    //$metadata_data = str_replace("&", "&amp;", $metadata_data);
                    if(@$metadata_doc->loadXML($metadata_data)!==false){
                        // save new metadata_data
                        $xpath = new \DOMXpath($metadata_doc);
                        //serach the data
                        $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:extent/gmd:EX_Extent/gmd:geographicElement/gmd:EX_GeographicBoundingBox");
                        //$transferOptions = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine");
                        if($objXPath->length>0){
                            $strCoord = "";
                            foreach($objXPath as $key => $item) {
                                $westBoundLongitude = $item->getElementsByTagName("westBoundLongitude");
                                if($westBoundLongitude && $westBoundLongitude->item(0)->nodeValue!=""){
                                    $strCoord.= "".trim($westBoundLongitude->item(0)->nodeValue);
                                }
                                $southBoundLatitude = $item->getElementsByTagName("southBoundLatitude");
                                if($southBoundLatitude && $southBoundLatitude->item(0)->nodeValue!=""){
                                    $strCoord.= " ".trim($southBoundLatitude->item(0)->nodeValue);
                                }
                                $eastBoundLongitude = $item->getElementsByTagName("eastBoundLongitude");
                                if($eastBoundLongitude && $eastBoundLongitude->item(0)->nodeValue!=""){
                                    $strCoord.= " ".trim($eastBoundLongitude->item(0)->nodeValue);
                                }

                                $northBoundLatitude = $item->getElementsByTagName("northBoundLatitude");
                                if($northBoundLatitude && $northBoundLatitude->item(0)->nodeValue!=""){
                                    $strCoord.= " ".trim($northBoundLatitude->item(0)->nodeValue);
                                }
                            }
                            $newItem->addElement("georss:polygon", $strCoord);
                        }
                    }
                    /**************
                     * Recuperation des dates
                     **************/
                    $newchangeDate = $rs3->Read(3);
                    if($newchangeDate > $changeDate) {
                        $changeDate = $newchangeDate;
                    }
                    /*****************
                     * updated : date de maj de la serie de donn�es
                     ********************/
                    $newItem->setDate($newchangeDate, '%Y-%m-%eT%H:%M:%S');

                    $oSyndWriter->addItem($newItem);
                }
            }

            /*****************
             * date à récuperer en fonction des dates des séries de données. -> TODO !
             * pour le moment le changedate de la metadonnees de service...
             * Rq. creation de la date avec appel de createNewItem() et on explouite uniquement la val recuperee
             *******************/
            if($changeDate != -1) {
                $newItemForDate = $oSyndWriter->createNewItem();
                $newItemForDate->setDate($changeDate, '%Y-%m-%eT%H:%M:%S');
                $objItemDate = $newItemForDate->getElements();
                $oSyndWriter->setChannelElement("updated", $objItemDate["updated"]["content"]);
            }
        }

        return new Response($oSyndWriter->printFeed(), 200, array(
            'Content-type'=>'text/xml'
        ));
    }
}
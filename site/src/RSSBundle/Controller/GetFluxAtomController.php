<?php

namespace ProdigeCatalogue\RSSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route; 
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Prodige\ProdigeBundle\Controller\BaseController;

/**
 * AtomData Controller
 * @author alkante <support@alkante.com>
 * @Route("/rss")
 *
 */

class GetFluxAtomController extends BaseController {

    /**
     * Get Doctrine connection
     * @param string $name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($name, $schema="public") {
        $conn = $this->getDoctrine()->getConnection($name);
        $conn->exec('set search_path to '.$schema);
        return $conn;
    }

    /**
     * @param  $conn
     * @param  $newSearchePath
     * @return \Doctrine\DBAL\Connection
     */
    protected function changeSearchePath($conn, $newSearchePath) {
        $conn->exec('set search_path to '.$newSearchePath);
        return $conn;
    }

    /**
     * @Route("/atomfeed/getfluxatom/{service}", name="catalogue_atomfeed_getfluxatom", options={"expose"=true} )
     * #Method({"GET", "POST"})
     * */
    public function getFluxAtomAction(Request $request, $service=null) {
        //permet de ne pas prendre en compte le paramétrage SGBD
        $includeParametrageSgbd = false;

        $conn = $this->getConnection('prodige', 'parametrage');
        $response = array();
        
        try {
            if($service && $service == "getSettingsFluxAtom") { // appelé pour récupérer les déclinaison possibles de découpage entre le format, les élement du territoire et les projections possibles
                
                $data = array();
                $conn = $this->changeSearchePath($conn, 'parametrage, public');
                // projections
                $strProjections = "";
                $tabProjections = array();
                $strSql = "SELECT prodige_settings_value FROM prodige_settings_fluxatom where prodige_settings_constant='PRO_FLUXATOM_PROJECTION'";
                $rs3 = $conn->fetchAllAssociative($strSql);
                if(count($rs3) > 0) {
                    $strProjections = $rs3[0]["prodige_settings_value"];
                }
                $tabProjections = explode(" ; ", $strProjections);
                $data["projection"]=$tabProjections;

                // formats
                $strFormats = "";
                $tabFormats = array();
                $tabFormatsBrut = array();
                $strSql = "SELECT prodige_settings_value FROM prodige_settings_fluxatom where prodige_settings_constant='PRO_FLUXATOM_FORMAT'";
                $rs4 = $conn->fetchAllAssociative($strSql);
                if(count($rs4) > 0) {;
                    $strFormats = $rs4[0]["prodige_settings_value"];
                }
                $tabFormatsBrut = explode(" ; ", $strFormats);
                // traitement pour ne récupere que les code des dformats ex . shp
                foreach($tabFormatsBrut as $k => $strFormat){
                    if($strFormat!=""){
                        $tabForm = explode(" - ", $strFormat);
                        $tabFormats[] = $valFormat = substr($tabForm[0], 7); // ex. val = shp
                        $nomFormat = $tabForm[1];
                    }
                }
                $data["formats"]= $tabFormats;
                // territoire
                // table contenant les element geog // provient des couches listées dans  prodige_download_param
                $table = "";
                $data["territoire"]["TABLE"]="";
                $strSql = "SELECT prodige_settings_value FROM prodige_settings_fluxatom where prodige_settings_constant='PRO_FLUXATOM_COUCHE_TERRITOIRE'";
                $rs5 = $conn->fetchAllAssociative($strSql);
                if(count($rs5) > 0) {
                    $table = $rs5[0]["prodige_settings_value"];
                }
                // traitement pour récupérer les parametre du territoire à partir de la table prodige_download_param
                $strSql = "SELECT critere_moteur_table, critere_moteur_champ_id, critere_moteur_champ_nom, pk_critere_moteur  FROM prodige_download_param where b_flux_atom=1";
                $rs6 = $conn->fetchAllAssociative($strSql);
                if (count($rs6) > 0) {
                    $table = $rs6[0]["critere_moteur_table"];
                    $data["territoire"]["TABLE"]=$table;
                    $champsId = $rs6[0]["critere_moteur_champ_id"];
                    $data["territoire"]["CHAMP_ID"]= $champsId;
                    $champslabel = $rs6[0]["critere_moteur_champ_nom"];
                    $data["territoire"]["CHAMP_LABEL"]=$champslabel;
                    $data["territoire"]["ID_TYPE_DOWNLOAD"]= $rs6[0]["pk_critere_moteur"]; // id pk dans la table prodige_download_param, util pour l'utisiation de prodigetelecarto
                }
                if($data["territoire"]["TABLE"]!="" && $data["territoire"]["TABLE"]!= ("Sélectionnez une valeur") && $data["territoire"]["TABLE"]!= ("Sélectionnez")
                    && $data["territoire"]["CHAMP_ID"]!="" && $data["territoire"]["CHAMP_ID"]!= ("Sélectionnez une valeur") && $data["territoire"]["CHAMP_ID"]!= ("Sélectionnez")
                    && $data["territoire"]["CHAMP_LABEL"]!="" && $data["territoire"]["CHAMP_LABEL"]!=("Sélectionnez une valeur") && $data["territoire"]["CHAMP_LABEL"]!=("Sélectionnez")){
                        // recuperation des informations sur la couche permettant de définir les élements du territoire dans la table prodige_download_param
                        // list des id des elements de territoire
                        $strList = "";
                        $strSql = $conn->createQueryBuilder();
                        $strSql->select($data["territoire"]["CHAMP_ID"].", ".$data["territoire"]["CHAMP_LABEL"])
                        ->from("public.".$data["territoire"]["TABLE"]);
                        $rs8 = $conn->fetchAllAssociative($strSql);
                        $j=0;
                        foreach($rs8 as $row) {
                            $data["territoire"]["LISTID"][$j]= $row[$data["territoire"]["CHAMP_ID"]];
                            $data["territoire"]["LISTNOM"][$j]= $row[$data["territoire"]["CHAMP_LABEL"]];
                            $j++;
                        }
                } else {
                    $data["territoire"]["TABLE"]=NULL;
                }
                
                $response = $data;
                
            }
        } catch(\Exception $exception) {
            
        }
        
        return new JsonResponse($response);
    }

}
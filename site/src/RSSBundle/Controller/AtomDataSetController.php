<?php

namespace ProdigeCatalogue\RSSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route; 
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Prodige\ProdigeBundle\Controller\BaseController;

use ProdigeCatalogue\RSSBundle\Common\FeedWriter\Feed;
use ProdigeCatalogue\RSSBundle\Common\FeedWriter\Item;
use ProdigeCatalogue\RSSBundle\Common\FeedWriter\ATOM;

use Prodige\ProdigeBundle\DAOProxy\DAO;

/**
 * AtomData Controller
 * @author alkante <support@alkante.com>
 * @Route("/rss")
 *
 */

class AtomDataSetController extends BaseController {

    /**
     * @Route("/atomfeed/atomdataset/{uuid}", name="catalogue_atomfeed_atomdataset", options={"expose"=true} )
     * #Génération des flux ATOM
     * #Method({"GET", "POST"})
     * */
    public function atomDataAction(Request $request, $uuid) {

        //require_once('../include/FeedWriter/Feed.php');
        //require_once('../include/FeedWriter/Item.php');
        //require_once('../include/FeedWriter/ATOM.php');

        //date_default_timezone_set('UTC');

        //use \FeedWriter\ATOM;

        //require_once('include/ClassSyndXml.php');
        //require_once('../parametrage.php');

        //$AdminPath = "../Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");
        //constante correspondant à la fiche de metadonnée de série de données
        //$uuid = (isset($_GET["uuid"]) ? $_GET["uuid"] : "-1");

        //$dao = new DAO();
        $conn = $this->getDoctrine()->getConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        if($dao) {

          $oSyndWriter = new ATOM;

          $oSyndWriter->addNamespace("inspire_dls", "http://inspire.ec.europa.eu/schemas/inspire_dls/1.0");
          $oSyndWriter->addNamespace("georss", "http://www.georss.org/georss");
          $oSyndWriter->addNamespace("gml", "http://www.opengis.net/gml");  
          // construciton de la partie flux de la plate forme
          // recherche des info de metadonnees de la série de données
          /*$rs = $dao->BuildResultSet("SELECT data, changedate from public.metadata where (data IS NOT NULL and data!='') and ".
              "metadata.uuid = '".$uuid."'");*/
          $rs = $dao->BuildResultSet("SELECT distinct meta.data, meta.changedate from public.metadata meta left join fiche_metadonnees on ". 
                              		" fiche_metadonnees.fmeta_id::bigint = meta.id".
                              		" left join couche_donnees cd on cd.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees".
                              		" where meta.data IS NOT NULL and meta.data!='' and ".
                                  "meta.uuid = ? /*and cd.couchd_download=1*/", array($uuid));
          $rs->First();

          $XML_FicheMetadataSerieDonnee = $rs->ReadHtml(0); 

          $version  = "1.0";
          $encoding = "UTF-8";
          $domMetadataSerieDonnee = new \DOMDocument($version, $encoding);
          $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
          $metadata_data = $entete.$XML_FicheMetadataSerieDonnee;
          if(@$domMetadataSerieDonnee->loadXML($metadata_data)) {

            $xpath = new \DOMXpath($domMetadataSerieDonnee);

            /*****************
            //serach the titre de la metadonnee de serie de donnees
            *******************/
            $TitreFlux = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString");
            if($TitreFlux && $TitreFlux->length > 0) {
                $titreFluxSerie = $TitreFlux->item(0)->nodeValue;
            } else {
                $titreFluxSerie = "Aucun titre identifié";
            }
            $oSyndWriter->setTitle($titreFluxSerie);

            /*****************  
            * url de la metadonnees de serie de donnees
            *******************/
            $strUrlFluxMetadonneSerie = rtrim(PRO_GEONETWORK_URLBASE, "/")."/srv/".$uuid;
            $oSyndWriter->setAtomLink($strUrlFluxMetadonneSerie, "describedby", "application/xml");

            /*****************
            * url du flux de serie de donnees 
            *******************/
            //$strUrlFluxSerieDonnees = PRO_CATALOGUE_URLBASE."atomfeed/atomdataset.php?uuid=".$uuid;
            $strUrlFluxSerieDonnees = $this->generateUrl('catalogue_atomfeed_atomdataset', array("uuid"=>$uuid), self::ABSOLUTE_URL);
            $oSyndWriter->setAtomLink($strUrlFluxSerieDonnees, "self", "application/atom+xml", "fr", "This document");  

            /*****************
            * url flux global
            *******************/
            //$strUrlFluxGlobal = PRO_CATALOGUE_URLBASE."atomfeed/topatom.php"; 
            $strUrlFluxGlobal = $this->generateUrl('catalogue_atomfeed_topatom', array(), self::ABSOLUTE_URL);
            $oSyndWriter->setAtomLink($strUrlFluxGlobal, "up", "application/atom+xml", "fr", "Service de téléchargement simple de la plateforme");

            /*****************
            * Rights
            ******************/
            $rights = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:resourceConstraints/gmd:MD_LegalConstraints/gmd:useLimitation/gco:CharacterString");
            if($rights && $rights->length>0) {
                $strRights = $rights->item(0)->nodeValue;
            } else {
                $strRights = "Aucune limite de droit identifiée";
            }
            $oSyndWriter->setChannelElement("rights", $strRights);

            /*****************
            * date à récuperer en fonction des dates des séries de données. 
            * pour le moment le changedate de la metadonnees de service...
            * Rq. creation de la date avec appel de createNewItem() et on explouite uniquement la val recuperee
            *******************/

            $changeDate = -1;
            $changeDate = $rs->Read(1);
            $newItemForDate = $oSyndWriter->createNewItem();
            $newItemForDate->setDate($changeDate, '%Y-%m-%eT%H:%M:%S');
            $objItemDate = $newItemForDate->getElements();
            $oSyndWriter->setChannelElement("updated", $objItemDate["updated"]["content"]);


            /**********************
            * Auteur
            **********************/
             /**********
              * org resp.
             **********/
            $org = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:pointOfContact/gmd:CI_ResponsibleParty/gmd:organisationName/gco:CharacterString");
            if($org && $org->length > 0) {
                $strOrganiostaionResp = $org->item(0)->nodeValue;
            } else {
                $strOrganiostaionResp = "Aucune information sur l'organisation responsable";
            }
            /**********
             * email 
             **********/
            $email = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:pointOfContact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:electronicMailAddress/gco:CharacterString");

            if($email && $email->length > 0) {
                $strEmail = $email->item(0)->nodeValue;
            } else {
                $strEmail = "Aucune email de contact identifié";
            }

            $tabElmentName["name"] = $strOrganiostaionResp;
            $tabElmentName["email"] = $strEmail; 
            $oSyndWriter->setChannelElement('author', $tabElmentName);

            /**********************
            * Balise Entry : le contenu diffère en fonction que l'on vise une série de donnée ou un ensemble de serie de données () 
            **********************/  
            // on s'assure que la métadonnée est de type "serie de données" ou de type "ensemble de série de données" (cad si couche_type_stockage =-1)
            $couche_type_stockage = "";
            $dao->setSearchPath("catalogue");
            //$strSql = "SELECT couchd_type_stockage, couchd_emplacement_stockage FROM couche_sdom where uuid='".$uuid."'";
            $strSql = "SELECT couchd_type_stockage, couchd_emplacement_stockage FROM couche_sdom where uuid=?";
            //par défaut ensemble de séries de données (non présentes dans couchd_emplacement_stockage)
            $couche_type_stockage =-1;
            $couchd_emplacement_stockage ="";
            $rs1 = $dao->BuildResultSet($strSql, array($uuid));
            if($rs1->GetNbRows() > 0) {
                $rs1->First();
                $couche_type_stockage = $rs1->Read(0);
                $couchd_emplacement_stockage = $rs1->Read(1);
            }


            /*************************************************
            *recherche des paramètres du flux atom
            *************************************************/
            if($couche_type_stockage != -1) { // cas d'une série de données  	
                //génération des entry  pour la série de donnée en fonction des formats, projetcion, territoires 
                //$tabElementAtom = json_decode(file_get_contents("http://".$accs_adresse_admin."/PRRA/administration_get_parametrage_fluxatom.php?service=getSettingsFluxAtom"),true);
                //$context = $this->createUnsecureSslContext();
                //$tabElementAtom = json_decode(file_get_contents($this->generateUrl('catalogue_atomfeed_getfluxatom', array('service'=>'getSettingsFluxAtom'), self::ABSOLUTE_URL), false, $context), true);
                $r = $this->forward('ProdigeCatalogue\RSSBundle\Controller\GetFluxAtomController::getFluxAtomAction', array('service'=>'getSettingsFluxAtom'));
                $tabElementAtom = json_decode($r->getContent(), true);
                $this->getDeclinaisonParametreSettings($oSyndWriter, CARMEN_URL_ADMINCARTO, $titreFluxSerie, $uuid,  $changeDate, $tabElementAtom, $couchd_emplacement_stockage, $couche_type_stockage);
            } else { // cas des ensembles de séries de données	  	
                //$context = $this->createUnsecureSslContext();
                //$tabElementAtom = json_decode(file_get_contents($this->generateUrl('catalogue_atomfeed_getfluxatom', array('service'=>'getSettingsFluxAtom'), self::ABSOLUTE_URL), false, $context), true);
                $r = $this->forward('ProdigeCatalogue\RSSBundle\Controller\GetFluxAtomController::getFluxAtomAction', array('service'=>'getSettingsFluxAtom'));
                $tabElementAtom = json_decode($r->getContent(), true);
                $this->getDeclinaisonParametreSettings($oSyndWriter, CARMEN_URL_ADMINCARTO, $titreFluxSerie, $uuid,  $changeDate, $tabElementAtom, $couchd_emplacement_stockage, $couche_type_stockage);
            }
        } else {
            echo "erreur de structure de la métadonnée";exit();
        }
      }
      //$oSyndWriter->printFeed();
      return new Response($oSyndWriter->printFeed(), 200, array(
        'Content-type'=>'text/xml'
      ));
    }

    /**
     * Fonction permettant de recupérer et restituer sous la forme des balises "entry" dans un flux atom la déclianiason des possibilité liés aux paramètres settings (adminsite) du flux ATOM
     * cette déclinaison est fonction du format, territoire, projetcion
     * @ $oSyndWriter : reference d'un flux atom
     * @ $accs_adresse_admin : chemin d'acces à l'admin
     * @ $uuid : identifiant de la fiche de metadata
     * @ titre de la fiche de serie ou d'ensemble de serie
     * @ $tabElementAtom : tableau de parametre du flux Atom permettant la déclianison des téléchargement en fonction/ territoire / projection / format
     * @ $couchd_emplacement_stockage : le nom de la table physique postgis contenant la geometry vectorielle
     * @ $couche_type_stockage : si $couche_type_stockage == -1 alors ensemble de série de données sinon serie de donnée
     */
    protected function getDeclinaisonParametreSettings($oSyndWriter, $accs_adresse_admin, $titreFluxSerie, $uuid, $changeDate, $tabElementAtom, $couchd_emplacement_stockage, $couche_type_stockage="") {
        $tabTabulaireFormats = array("xls", "ods", "csv");
        foreach($tabElementAtom["projection"] as $key => $projection) {
            //Rappel :  var $projection de type :    "EPSG:2154 - L93"
            $nomProjection = "";
            $valEPSG = "";
            if($projection != "") {
                $tabProj = explode(" - ", $projection);
                $valEPSG = substr($tabProj[0], 5);
                $nomProjection = $tabProj[1];
            }
            if($valEPSG != "" && is_array($tabElementAtom["formats"]) && !empty($tabElementAtom["formats"])) {
                foreach($tabElementAtom["formats"] as $keyf => $format) {
                   if($couche_type_stockage!=-3 || in_array($format, $tabTabulaireFormats)){
                      if($tabElementAtom["territoire"]["TABLE"] != null) {
                        foreach($tabElementAtom["territoire"]["LISTID"] as $keyLst => $idTerritoire) {
                            if($keyLst < 1000) { // on limite à 1000
                                $newItem = $oSyndWriter->createNewItem();
                                $newItem->setTitle($titreFluxSerie." - ".$format." - ".$projection." - ".$tabElementAtom["territoire"]["LISTNOM"][$keyLst]);
                                /*****************
                                 * link + id
                                 ********************/
                                //$strUrlAccesDonnees = PRO_CATALOGUE_URLBASE."atomfeed/atomdata.php?uuid=".$uuid."&format=".$format."&srs=".$valEPSG."&emprise=".$idTerritoire."&territoire_type=".$tabElementAtom["territoire"]["ID_TYPE_DOWNLOAD"]."&couchd_emplacement_stockage=".$couchd_emplacement_stockage."&bTerritoire=1&couche_type_stockage=".$couche_type_stockage;
                                $strUrlAccesDonnees = $this->generateUrl('catalogue_atomfeed_atomdata',array(
                                    "uuid"                  => $uuid,
                                    "format"                => $format,
                                    "srs"                   => $valEPSG,
                                    "emprise"               => $idTerritoire,
                                    "territoire_type"       => $tabElementAtom["territoire"]["ID_TYPE_DOWNLOAD"],
                                    "couchd_emplacement_stockage" => $couchd_emplacement_stockage,
                                    "bTerritoire"           => 1,
                                    "couche_type_stockage"  => $couche_type_stockage
                                ), self::ABSOLUTE_URL);
                                // taille du file.zip
                                $FilePathName = PRO_ROOT_FILE_ATOM.$uuid."/".$format."/".$valEPSG.($idTerritoire!=-1 ? "/".$idTerritoire : "");
                                $filesize = "";
                                if(file_exists($FilePathName."/file.zip")) { // si existe -> sortie standard
                                    $filesize = filesize($FilePathName."/file.zip");
                                }
    
                                //$newItem->addElement('link','', array('href'=>$strUrlAccesDonnees, 'rel'=>'alternate', 'type'=>'application/x-shapefile', 'hreflang'=>'fr', 'title'=>'accès aux fichiers sources', 'length'=>$filesize), FALSE, TRUE);
                                if($filesize!=""){
                                    $newItem->addElement('link','', array('href'=>$strUrlAccesDonnees, 'rel'=>'alternate', 'type'=>'application/x-shapefile', 'hreflang'=>'fr', 'title'=>'accès aux fichiers sources', 'length'=>$filesize), FALSE, TRUE);
                                }else{
                                    $newItem->addElement('link','', array('href'=>$strUrlAccesDonnees, 'rel'=>'alternate', 'type'=>'application/x-shapefile', 'hreflang'=>'fr', 'title'=>'accès aux fichiers sources'), FALSE, TRUE);
                                }
                                
                                $newItem->addElement('id', $oSyndWriter::uuid($uuid.$format.$projection.$idTerritoire,'urn:uuid:'));
                                /*****************
                                 * updated : date de maj de la serie de donn�es
                                 ********************/
                                $newItem->setDate($changeDate, '%Y-%m-%eT%H:%M:%S');
                                /*****************
                                 * category
                                 ********************/
                                $newItem->addElement('category', '', array('term'=>'http://www.opengis.net/def/crs/EPSG/0/'.$valEPSG, 'label'=>$nomProjection), FALSE, TRUE);
                                $oSyndWriter->addItem($newItem);
                            }
                        }
                      
                    } else { // cas où il n'y a pas de découpage par territoire
                      
                          $idTerritoire = -1;// pas de decoupage par territoire
                          $newItem = $oSyndWriter->createNewItem();
                          $newItem->setTitle($titreFluxSerie." - Format ".strtoupper($format)." - ".$projection);
                          /*****************
                           * link + id
                           ********************/
                          //$strUrlAccesDonnees = PRO_CATALOGUE_URLBASE."atomfeed/atomdata.php?uuid=".$uuid."&format=".$format."&srs=".$valEPSG."&emprise=".$idTerritoire."&territoire_type=&couchd_emplacement_stockage=".$couchd_emplacement_stockage."&bTerritoire=0&couche_type_stockage=".$couche_type_stockage;
                          $strUrlAccesDonnees = $this->generateUrl('catalogue_atomfeed_atomdata', array(
                              "uuid"                  => $uuid,
                              "format"                => $format,
                              "srs"                   => $valEPSG,
                              "emprise"               => $idTerritoire,
                              "territoire_type"       => "",
                              "couchd_emplacement_stockage" => $couchd_emplacement_stockage,
                              "bTerritoire"           => 0,
                              "couche_type_stockage"  => $couche_type_stockage
                           ), self::ABSOLUTE_URL);

                          $FilePathName = PRO_ROOT_FILE_ATOM.$uuid."/".$format."/".$valEPSG.($idTerritoire!=-1 ? "/".$idTerritoire : "");
                          $filesize = "";
                          if(file_exists($FilePathName."/file.zip")) { // si existe -> sortie standard
                              $filesize = filesize($FilePathName."/file.zip");
                          }
                          //$newItem->addElement('link','', array('href'=>$strUrlAccesDonnees, 'rel'=>'alternate', 'type'=>'application/x-shapefile', 'hreflang'=>'fr', 'title'=>'accès aux fichiers sources', 'length'=>$filesize), FALSE, TRUE);
                          if($filesize!=""){
                              $newItem->addElement('link','', array('href'=>$strUrlAccesDonnees, 'rel'=>'alternate', 'type'=>'application/x-shapefile', 'hreflang'=>'fr', 'title'=>'accès aux fichiers sources', 'length'=>$filesize), FALSE, TRUE);
                          }else{
                              $newItem->addElement('link','', array('href'=>$strUrlAccesDonnees, 'rel'=>'alternate', 'type'=>'application/x-shapefile', 'hreflang'=>'fr', 'title'=>'accès aux fichiers sources'), FALSE, TRUE);
                          }
                          $newItem->addElement('id', $oSyndWriter::uuid($uuid.$format.$projection.$idTerritoire,'urn:uuid:'));
                          /*****************
                           * updated : date de maj de la serie de donn�es
                           ********************/
                          $newItem->setDate($changeDate, '%Y-%m-%eT%H:%M:%S');
                          /*****************
                           * category
                           ********************/
                          $newItem->addElement('category', '', array('term'=>'http://www.opengis.net/def/crs/EPSG/0/'.$valEPSG, 'label'=>$nomProjection), FALSE, TRUE);
                          $oSyndWriter->addItem($newItem);
                        }
                   }
                }
            }
        }
    }

}
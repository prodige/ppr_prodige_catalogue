<?php

namespace ProdigeCatalogue\RSSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route; 
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Prodige\ProdigeBundle\Controller\BaseController;

use Prodige\ProdigeBundle\Services\GeonetworkInterface;

use ProdigeCatalogue\RSSBundle\Common\FeedWriter\Feed;
use ProdigeCatalogue\RSSBundle\Common\FeedWriter\Item;
use ProdigeCatalogue\RSSBundle\Common\FeedWriter\ATOM;

use Prodige\ProdigeBundle\DAOProxy\DAO;

/**
 * AtomData Controller
 * @author alkante <support@alkante.com>
 * @Route("/rss")
 *
 */

class AtomDataController extends BaseController {
    
    /**
     * @Route("/atomfeed/atomdata/{uuid}", name="catalogue_atomfeed_atomdata", options={"expose"=true} )
     * #Method({"GET", "POST"})
     * */
    public function atomDataAction(Request $request, $uuid="-1") {
        global $PRO_DOWNLOAD_METADATA_ID;
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
        //constante correspondant à la fiche de metadonnée de série de données
        //$uuid = (isset($_GET["uuid"]) ? $_GET["uuid"] : "-1");

        //$format = (isset($_GET["format"]) ? $_GET["format"] : "-1");
        $format = $request->query->get("format", "-1");
        //$srs = (isset($_GET["srs"]) ? $_GET["srs"] : "-1");
        $srs = $request->query->get("srs", "-1");
        //$emprise = (isset($_GET["emprise"])&& $_GET["emprise"]!=-1 ? $_GET["emprise"] : "-1");// id des elment de territoire si decoupage par territoire
        $emprise = $request->query->get("emprise", "-1");
        //$territoire_type = (isset($_GET["territoire_type"])&& $_GET["territoire_type"]!=-1 ? $_GET["territoire_type"] : "-1");
        $territoire_type = $request->query->get("territoire_type", "-1");
        //$couchd_emplacement_stockage = (isset($_GET["couchd_emplacement_stockage"])&& $_GET["couchd_emplacement_stockage"]!=-1 ? $_GET["couchd_emplacement_stockage"] : "-1");
        //$couchd_emplacement_stockage = $request->query->get("couchd_emplacement_stockage", "-1");
        //$bTerritoire = (isset($_GET["bTerritoire"])&& $_GET["bTerritoire"]!=-1 ? $_GET["bTerritoire"] : "-1");
        $bTerritoire = $request->query->get("bTerritoire", "-1");
        //$couche_type_stockage = (isset($_GET["couche_type_stockage"])&& $_GET["couche_type_stockage"]!="" ? $_GET["couche_type_stockage"] : "");
        $couche_type_stockage = $request->query->get("couche_type_stockage", "-1");

        $FilePathName = PRO_ROOT_FILE_ATOM.$uuid."/".$format."/".$srs.($emprise!=-1 ? "/".$emprise : "");

        //$dao = new DAO();
        $conn = $this->getDoctrine()->getConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        if($dao) {
            //Sécurité séries de données en téléchargement libre bien identifiées par couchd_download=1
            $dao->setSearchPath("public,catalogue");
            
            //$strSql = "SELECT id, changedate FROM metadata where uuid='".$uuid."'";
            $strSql = "SELECT id, changedate FROM metadata where uuid=?";
            $rs0= $dao->BuildResultSet($strSql, array($uuid));
            if($rs0->GetNbRows() > 0) {
                $rs0->First();
                $id_ens_serie = $rs0->Read(0);
                $changedate = $rs0->Read(1);
                $dtime = \DateTime::createFromFormat("Y-m-d\TH:i:s", $changedate);
                $metadataDatetime = $dtime->getTimestamp();
                if($couche_type_stockage == -1) { // ensemble de serie, vérification des dates des séries de données et récupération de la plus récente
                    $strMetadataId = "";
                    $relationsXml = $geonetwork->get("api/records/".urlencode($uuid)."/related?type=children", false);
                    $couchd_emplacement_stockage = "";
                    $xmlDoc = new \DOMDocument('1.0', 'UTF-8');
                    //if($xmlDoc->load($strUrl) === TRUE) {
                    if($xmlDoc->loadXML($relationsXml) === TRUE) {
                        $relations = $xmlDoc->getElementsByTagName('relation');
                        if($relations->length > 0) { // si il y a des fiche serie de donnée filles
                            for($i=0; $i<$relations->length; $i++) {
                                if($relations->item($i)->hasAttribute('type')) { // reucp des informations sur les séries de données filles
                                    $changeDate = $relations->item($i)->getElementsByTagName('changeDate')->item(0)->nodeValue;
                                    $dtime = \DateTime::createFromFormat("Y-m-d\TH:i:s", $changeDate);
                                    if($dtime->getTimestamp() > $metadataDatetime)
                                        $metadataDatetime = $dtime->getTimestamp();
                                }
                            }
                        }
                    }
                }

            }
        }

        if(file_exists($FilePathName) && $metadataDatetime > filemtime($FilePathName))
            $this->delArboTelechargementFluxAtom($FilePathName);

        if(file_exists($FilePathName."/file.zip")) { // si existe -> sortie standard et la métadonnée est plus récente que la donnée
            if($couche_type_stockage==-1) { // ensemble de serie
                // on recupere les uuid des séries de données filles pour savoir s'il y a bien encore des fiche de serie de données
                $relationsXml = $geonetwork->get("api/records/".urlencode($uuid)."/related?type=children", false);
                $couchd_emplacement_stockage = "";
                $xmlDoc = new \DOMDocument('1.0', 'UTF-8');
                //if($xmlDoc->load($strUrl) === TRUE ) {
                if($xmlDoc->loadXML($relationsXml) === TRUE ) {
                    $relations = $xmlDoc->getElementsByTagName('relation');
                    if($relations->length == 0) { // si aucune donnée fille dans ce cas on supprime le dossier
                        $dir = PRO_ROOT_FILE_ATOM.$uuid;
                        $this->delArboTelechargementFluxAtom($dir);
                        //$strUrl = PRO_CATALOGUE_URLBASE."atomfeed/atomdataset.php?uuid=".$uuid;
                        $strUrl = $this->generateUrl('catalogue_atomfeed_atomdataset', array('uuid' => $uuid), self::ABSOLUTE_URL);
                        header('location:' .$strUrl);
                        exit;
                    } else {
                        $this->affReadFile($FilePathName."/file.zip");
                    }
                }
            } else {
                $this->affReadFile($FilePathName."/file.zip");
            }
        } else { // generation avec le service prodigetelecarto 	// construction de l'url

            //$format ex. shp; tab..
            if($format=="xls"){
                $format= "xlsx";
            }
            $encodeformat = $this->encodeValue($format);
            // projection $srs ex. 2154
            $projection = $this->encodeValue($srs);
            
            
            // metadata_id
            if($couche_type_stockage == -1) { // ensemble des series de donnees
                // on recupere l'id de metadata de l'ensmeble de serie
                // on recupere les uuid des séries de données filles via service
                $relationsXml = $geonetwork->get("api/records/".urlencode($uuid)."/related?type=children", false);
                
                $couchd_emplacement_stockage = "";
                $xmlDoc = new \DOMDocument('1.0', 'UTF-8');
                
                //if($xmlDoc->load($strUrl) === TRUE ) {
                if($xmlDoc->loadXML($relationsXml) === TRUE ) {
                  
                    $relations = $xmlDoc->getElementsByTagName('item');
                  
                    $strMetadataId = "";
                    if($relations->length > 0) { // si il y a des fiche serie de donnée filles
                        for($i=0; $i<$relations->length; $i++) {
                            $uuid_item = $relations->item($i)->getElementsByTagName('id')->item(0)->nodeValue;
                            if($dao) {
                                $dao->setSearchPath("catalogue,public");
                                $strSql = "SELECT fmeta_id, couchd_emplacement_stockage FROM couche_sdom where uuid=? and couchd_download=1";
                                $rs1 = $dao->BuildResultSet($strSql, array($uuid_item));
                                if($rs1->GetNbRows() > 0) {
                                    $bMetadataFluxATOM = false;
                                    $rs1->First();
                                    //BFE : pourquoi ce test ?
                                    $strSep="";
                                    if($rs1->Read(0) != $PRO_DOWNLOAD_METADATA_ID) { // cas de la fiche de metadata utilise pourles flux atom
                                        $strMetadataId.= $rs1->Read(0)."%";
                                        $couchd_emplacement_stockage.= $rs1->Read(1)."%";
                                        $strSep .= "%";
                                    }
                                }
                            }
                        }
                        $strMetadataId = substr($strMetadataId, 0, -1);
                        
                        $metadata_id = $this->encodeValue($strMetadataId);
                        // nom physique de la table postgis porteuse des geometry
                        $couchd_emplacement_stockage = substr($couchd_emplacement_stockage, 0, -1);
                        
                        $couchd_emplacement_stockage = $this->encodeValue("POSTGIS_DATA:".$couchd_emplacement_stockage);
                    } else { // si aucune fiche fille constaté on en profite pour supprimer les dossiers stocké dans l'arborescence du dossier d'ensemble de serie
                        $dir = PRO_ROOT_FILE_ATOM.$uuid;
                        $this->delArboTelechargementFluxAtom($dir);
                        $strUrl = $this->generateUrl('catalogue_atomfeed_atomdataset', array('uuid' => $uuid), self::ABSOLUTE_URL);
                        header('location:' .$strUrl);
                        exit;
                    }
                }
            } else {
                
                if($dao) {
                    $dao->setSearchPath("catalogue,public");
                    //$strSql = "SELECT id  FROM metadata where uuid='".$uuid."'";
                    $strSql = "SELECT id, couchd_emplacement_stockage from public.metadata meta inner join fiche_metadonnees on ".
                    " fiche_metadonnees.fmeta_id::bigint = meta.id".
                    " inner join couche_donnees cd on cd.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees".
                    " where meta.data IS NOT NULL and meta.data!='' and ".
                    " meta.uuid = ? and cd.couchd_download=1 and cd.couchd_type_stockage in (1, -3, -4)";
                    $rs1 = $dao->BuildResultSet($strSql, array($uuid));
                    if($rs1->GetNbRows()==0){
                        throw new \Exception("droit d'accès refusé");
                        exit();
                    }
                    $strMetadataId = "";
                    $couchd_emplacement_stockage ="";
                    $strSep="";
                    for($rs1->First(); !$rs1->EOF(); $rs1->Next()) {
                        $strMetadataId.= $rs1->Read(0)."%";
                        $couchd_emplacement_stockage.= $rs1->Read(1)."%";
                        $strSep .= "%";
                    }

                    $strMetadataId = substr($strMetadataId, 0, -1);
                    $couchd_emplacement_stockage = substr($couchd_emplacement_stockage, 0, -1);
                }
                $metadata_id = $this->encodeValue($strMetadataId);
                // nom physique de la table postgis porteuse des geometry
                $couchd_emplacement_stockage = $this->encodeValue("POSTGIS_DATA:".$couchd_emplacement_stockage);
            }

            if($metadata_id != "") {
                $strUrlTelecarto = $this->container->getParameter('PRODIGE_URL_TELECARTO')."/?mode=prodige&"
                  . "data_type=".($couche_type_stockage == -3 ? "table" : "vector")."&"
                  . "service_idx=1&"
                  . "email=&"
                  . "format=".$encodeformat."&"
                  . "projection=".$projection."&"
                  . "metadata_id=".$metadata_id."&"
                  . "direct=MQ==&"
                  . "bTerritoire=".$bTerritoire."&"
                  . "territoire_type=".$territoire_type."&"
                  . "territoire_data=".($emprise!=-1 ? $emprise : "")."&"
                  . "extract_area=&"
                  . "buffer=0&"
                  . "restricted_area_buffer=".$strSep."&"
                  . "restricted_area_field=".$strSep."&"
                  . "territoire_area=".$strSep."&"
                  . "data=".$couchd_emplacement_stockage.
                  ($couche_type_stockage == -1 ? "&id_ens_serie=".$id_ens_serie : "");
            } else {
                throw new \Exception("erreur dans le service de téléchargement. Pas de fiche de série fille en téléchargement libre");
            }

            //execution
            $reponse = json_decode(trim(file_get_contents($strUrlTelecarto),"()"),true);
            
            if($reponse["success"] == 1) { // deplacement du fichier générer
                // reconstitution du nom du chemin
                $dstfile=$FilePathName."/file.zip";
                @mkdir($FilePathName, 0777, true);
                copy($reponse["data"], $dstfile);
                
                // ouverture du fichier
                $this->affReadFile($dstfile);
            } else {
                throw new \Exception("erreur dans le service de téléchargement");

            }
        }

    }


    protected function encodeValue($value){
        return base64_encode(urlencode($value));
    }

    protected function decodeValue($value){
        return urldecode(base64_decode($value));
    }

    protected function affReadFile($dstfile){
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename='.basename($dstfile));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($dstfile));
        readfile($dstfile);
        exit;
    }

    /**
     * #brief : fonction supprimant l'arborescence de stockage des dossiers accessible par les flux ATOM de façon recursive
     * lors de la suppresion d'une fiche de métadonnee de serie de données
     * @param $uuid : id de la fiche de meta de serie de donnees
     */
    protected function delArboTelechargementFluxAtom($dir) {

        if(is_dir($dir)) {
            $objects = scandir($dir);
            foreach($objects as $object) {
                if($object != "." && $object != "..") {
                    if(filetype($dir."/".$object) == "dir") {
                        $this->delArboTelechargementFluxAtom($dir."/".$object);
                    }
                    else {
                        unlink($dir."/".$object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }
}
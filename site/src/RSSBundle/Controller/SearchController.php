<?php

namespace ProdigeCatalogue\RSSBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route; 
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Prodige\ProdigeBundle\Controller\BaseController;

use Prodige\ProdigeBundle\DAOProxy\DAO;

/**
 * AtomData Controller
 * @author alkante <support@alkante.com>
 * @Route("/rss")
 *
 */

class SearchController extends BaseController {
    
    /**
     
     * @Route("/atomfeed/search/", name="catalogue_atomfeed_search", options={"expose"=true} )
     * #Génération des flux ATOM
     * #Method({"GET", "POST"})
     * */
    public function searchAction(Request $request) {

        // hismail - Partie commentée avant la migration
        /*
         require_once('../include/FeedWriter/Feed.php');
         require_once('../include/FeedWriter/Item.php');
         require_once('../include/FeedWriter/ATOM.php');

         //date_default_timezone_set('UTC');

         use \FeedWriter\ATOM;
         */

        //require_once('include/ClassSyndXml.php');
        // Fin de la partie commentée avant la migration

        //require_once('../parametrage.php');

        //$AdminPath = "../Administration/";
        //require_once($AdminPath."DAO/DAO/DAO.php");
        $conn = $this->getDoctrine()->getConnection('catalogue');
        $dao = new DAO($conn, 'catalogue');
        //$dao = new DAO();
        if($dao) {

            // construciton de la partie flux de la plate forme
            // recherche des info de metadonnees de flux  de service
            $PRO_DOWNLOAD_METADATA_ID = "1387"; //TODO hismail - Pour tester seulement
            /*$rs = $dao->BuildResultSet("SELECT data from public.metadata where (data IS NOT NULL and data!='') and ".
                "metadata.id = ".$PRO_DOWNLOAD_METADATA_ID);*/
            $rs = $dao->BuildResultSet("SELECT data from public.metadata where (data IS NOT NULL and data!='') and ".
                "metadata.id = ?", array($PRO_DOWNLOAD_METADATA_ID));
            $rs->First();

            $XML_FicheMetadataService = "";
            $XML_FicheMetadataService = $rs->ReadHtml(0);

            $version  = "1.0";
            $encoding = "UTF-8";
            $domMetadataService = new \DOMDocument($version, $encoding);
            $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
            $metadata_data = $entete.$XML_FicheMetadataService;
            $domMetadataService->loadXML($metadata_data);

            $xpath = new \DOMXpath($domMetadataService);

            /*****************
             //serach the titre du flux
             *******************/

            $TitreFlux = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString");
            if($TitreFlux && $TitreFlux->length > 0) {
                $titreFluxService = $TitreFlux->item(0)->nodeValue;
            } else {
                $titreFluxService = "Aucun titre identifié";
            }

            /*****************
             //resume du flux
             *******************/

            $ResumeFlux = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:abstract/gco:CharacterString");
            if($TitreFlux && $ResumeFlux->length > 0) {
                $strResumeFlux = $ResumeFlux->item(0)->nodeValue;
            } else {
                $strResumeFlux = "Aucun résumé";
            }
            //echo html_entity_decode($strResumeFlux, ENT_QUOTES, 'UTF-8'); die();
            /**********
            * email contact
            **********/
            $email = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:pointOfContact/gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:electronicMailAddress/gco:CharacterString");
            if($email && $email->length > 0) {
                $strEmail = $email->item(0)->nodeValue;
            }
            else {
                $strEmail = "Aucune email de contact identifié";
            }

            /**********
             * $strUrlService flux atom
             **********/
            //$strUrlServiceOpenSearch = PRO_CATALOGUE_URLBASE."atomfeed/search.php";
            $strUrlServiceOpenSearch = $this->generateUrl('catalogue_atomfeed_search', array(), self::ABSOLUTE_URL);
            //$strUrlServiceTopatom= PRO_CATALOGUE_URLBASE."atomfeed/topatom.php";
            $strUrlServiceTopatom = $this->generateUrl('catalogue_atomfeed_topatom', array(), self::ABSOLUTE_URL);

            /***********
             * Parametre filtre de la recherche
             ***********/

            $searchTerms ="";
            $version  = "1.0";
            $encoding = "UTF-8";
            $metaOpenSearch = new \DOMDocument($version, $encoding);
            //$metaOpenSearch->formatOutput = true;
            //$metaOpenSearch->preserveWhiteSpace= false;
            $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";

            $strXML =
            "<OpenSearchDescription xmlns=\"http://a9.com/-/spec/opensearch/1.1/\"
                xmlns:inspire_dls=\"http://inspire.ec.europa.eu/schemas/inspire_dls/1.0\"
                xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"
                xsi:schemaLocation=\"http://a9.com/-/spec/opensearch/1.1/OpenSearch.xsd\">
                <ShortName>".$titreFluxService."</ShortName>
                <Description>".$strResumeFlux."</Description>
                <Url type=\"application/opensearchdescription+xml\" rel=\"self\" template=\"".$strUrlServiceOpenSearch."\"/>
                <Url type=\"application/atom+xml\" rel=\"results\" template=\"".$strUrlServiceTopatom."?q={searchTerms}\"/>
                <Url type=\"application/atom+xml\" rel=\"describedby\" template=\"".$strUrlServiceTopatom."?spatial_dataset_identifier_code={inspire_dls:spatial_dataset_identifier_code?}&spatial_dataset_identifier_namespace={inspire_dls:spatial_dataset_identifier_namespace?}&crs={inspire_dls:crs?}&language={language?}&q={searchTerms}?\"/>
                <Contact>".$strEmail."</Contact>
                <Language>fr</Language>
                </OpenSearchDescription>";

            $metadata_data = $entete.$strXML;
            $metadata_data = str_replace("&", "&amp;", $metadata_data);

            $metaOpenSearch->loadXML($metadata_data);

            header("Content-Type: " . "text/xml");
            echo $metaOpenSearch->saveXML();

        }
        exit;
    }

}
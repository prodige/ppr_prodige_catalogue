<?php

namespace ProdigeCatalogue\ApiBundle\Controller;

use http\Client;
use OpenApi\Annotations as OA;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Prodige\ProdigeBundle\Controller\BaseController;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Proxy vers l'API https://entreprise.api.gouv.fr
 *
 * @Route("/")
 */
class ApiEntrepriseController extends BaseController
{

    /**
     * @Route("/api_entreprise/siren/{siren}")
     * 
     * @OA\Get(
     *     summary="Récupération des unités légales",
     * )
     * @OA\Tag(name="Data")
     * @OA\Parameter(
     *     name="siren",
     *     in="path",
     *     description="Numéro de SIREN",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\Response(
     *     response="401",
     *     description="Non autorisé"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )  
     * @OA\Response(
     *     response="404",
     *     description="Non trouvée"
     * )
     * @OA\Response(
     *     response="422",
     *     description="Paramètre(s) invalide(s)"
     * ),
     * @OA\Response(
     *     response="451",
     *     description="Indisponible pour raisons légales"
     * ),
     * @OA\Response(
     *     response="502",
     *     description="Erreur de fournisseur"
     * ),
     * @OA\Response(
     *     response="504",
     *     description="Erreur d'intermédiaire"
     * )  
     *
     * @param string $siren
     * @return JsonResponse
     */
    public function getUnitesLegales(string $siren = '')
    {
        $unitesLegalesPath = $this->container->getParameter('api_entreprise_unites_legales_path');
        $result = $this->callApiEnterprise($unitesLegalesPath, $siren, '/siege_social');
        if (isset($result['responseBody'])) {
            // on encode puis décode les données pour les convertir du format text/plain au format application/json
            return JsonResponse::fromJsonString(json_decode(json_encode($result['responseBody'])));
        } else {
            $httpError = $result['httpError'];
            switch ($httpError) {
                case 401:
                case 403:
                case 404:
                case 422:
                case 451:
                case 502:
                case 504:
                    http_response_code($httpError);
                    exit();
                default:
                    http_response_code(502);
                    exit();
            }
        }
    }

    /**
     * @Route("/api_entreprise/siret/{siret}")
     * @OA\Get(
     *     summary="Récupération des établissements",
     * )
     * @OA\Tag(name="Data")
     * 
     * @OA\Parameter(
     *    name="siret",
     *     in="path",
     *     description="Numéro de SIRET",
     *     required=true,
     *     @OA\Schema(type="string"),
     *  ),
     * @OA\Response(
     *    response="200",
     *    description="Succès"
     * ),
     * @OA\Response(
     *    response="401",
     *    description="Non autorisé"
     * ),
     * @OA\Response(
     *    response="403",
     *    description="Interdit"
     * ),
     * @OA\Response(
     *    response="404",
     *    description="Non trouvé"
     * ),
     * @OA\Response(
     *    response="422",
     *    description="Paramètre(s) invalide(s)"
     * ),
     * @OA\Response(
     *    response="502",
     *    description="Erreur de fournisseur"
     * ),
     * @OA\Response(
     *    response="504",
     *    description="Erreur d'intermédiaire"
     * )
     *
     * @param string $siret
     * @return JsonResponse
     */
    public function getEtablissement(string $siret = '')
    {
        $etablissementPath = $this->container->getParameter('api_entreprise_etablissements_path');
        $result = $this->callApiEnterprise($etablissementPath, $siret);
        if (isset($result['responseBody'])) {
            // on encode puis décode les données pour les convertir du format text/plain au format application/json
            return JsonResponse::fromJsonString(json_decode(json_encode($result['responseBody'])));
        } else {
            $httpError = $result['httpError'];
            switch ($httpError) {
                case 401:
                case 403:
                case 404:
                case 422:
                case 502:
                case 504:
                    http_response_code($httpError);
                    exit();
                default:
                    http_response_code(502);
                    exit();
            }
        }
    }

    /**
     * Exécute une requête en GET avec CURL et retourne les données dans l'attribut 'responseBody'.
     * Si la réponse contient un code erreur HTTP, on renvoie ce code dans l'attribut 'httpError'.
     *
     * @param $endPointPath Chemin du endpoint à interroger
     * @param $identifiant Identifiant de la ressource à interroger (SIREN ou SIRET)
     * @param $postfix postfixe du service web
     * @return mixed
     */
    private function callApiEnterprise($endPointPath, $identifiant, $postfix='')
    {
        $apiEntrepriseBaseURL = $this->container->getParameter('api_entreprise_base_url');
        $apiEntrepriseContext = $this->container->getParameter('api_entreprise_context');
        $apiEntrepriseRecipient = $this->container->getParameter('api_entreprise_recipient');
        $apiEntrepriseObjet = $this->container->getParameter('api_entreprise_object');
        $apiEntrepriseToken = $this->container->getParameter('api_entreprise_token');

        $url = $apiEntrepriseBaseURL.$endPointPath.'/'.$identifiant.$postfix
            .'?context='.urlencode($apiEntrepriseContext)
            .'&recipient='.urlencode($apiEntrepriseRecipient)
            .'&object='.urlencode($apiEntrepriseObjet);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1000);

        $authorization = "Authorization: Bearer ".$apiEntrepriseToken;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $authorization));

        $reply = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        if ($httpCode == 200) {
            return array('responseBody' => $reply);
        } else {
            return array('httpError' => $httpCode);
        }
    }


}

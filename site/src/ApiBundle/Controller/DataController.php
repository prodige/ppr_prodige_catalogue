<?php

namespace ProdigeCatalogue\ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;

use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\Controller\Annotations\QueryParam;

use Prodige\ProdigeBundle\Controller\User;
use Symfony\Component\Process\Process;


use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\EditionController;
use Prodige\ProdigeBundle\Services\Logs;
use Psr\Log\LoggerInterface;

/**
 *
 * @author alkante <support@alkante.com>
 *
 * @Route("/api")
 */
class DataController extends BaseController
{
    private $operators;

    private $column_types;

    private $authorized_columns;

    const INVALID_ATTRIBUTES = "INVALID_ATTRIBUTES";
    const INVALID_GEOMETRY = "INVALID_GEOMETRY";
    const GEOMETRY_OUTSIDE_BOUNDS = "GEOMETRY_OUTSIDE_BOUNDS";
    const UNKNOWN_ERROR = "UNKNOWN_ERROR";
    const MISSING_GID = "MISSING_GID";
    const CALCUL_AUTOMATIQUE = "CALCUL_AUTOMATIQUE";
    const INVALID_DATA_RIGHTS = "INVALID_DATA_RIGHTS";

    /**
     * Construteur.
     *
     */
    public function __construct()
    {
        //parent::__construct();
        $this->operators = [
            "and",
            "eq",
            "neq",
            "in",
            "nin",
            "le",
            "lt",
            "ge",
            "gt"
        ];
        $this->authorized_columns = [];
        $this->column_types = [];
    }

    /**
     * Récupération des données d'une ressource au format GeoJson
     *
     *
     * @Route("/data/{uuid}", methods={"GET"})
     * @OA\Get(
     *     summary="Récupération des données d'une ressource"
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="nature",
     *     in="query",
     *     description="nature des données (visibles (view) ou éditables (edit))",
     *     required=false,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="bbox",
     *     in="query",
     *     description="emprise des données souhaitées. Si srs est fourni, bbox doit être fourni dans le système de coordonnées srs. Sinon, il doit être fourni dans le système de coordonnées des données",
     *     required=false,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="gid",
     *     in="query",
     *     description="gid de la donnée souhaitée",
     *     required=false,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="filter",
     *     in="query",
     *     description="filtre sur les données, paramètre dépendant de la structure de la donnée (cf operation structure), ex : https://[CATALOGUE_URL]/api/data/[uuid]?departement=35",
     *     required=false,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="search",
     *     in="query",
     *     description="(beta)filtre sur les données, par une recherche plein texte sur les colonnes de la donnée, ex : https://[CATALOGUE_URL]/api/data/[uuid]?search=paris",
     *     required=false,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="limit",
     *     in="query",
     *     description="nombre de données maximal en retour",
     *     required=false,
     *     @OA\Schema(type="integer")
     * )
     * @OA\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Indice de départ de pagniation (URLs de pagination (prev, next, self, first, last) contenus dans le header de réponse Link)",
     *     required=false,
     *     @OA\Schema(type="integer")
     * )
     * @OA\Parameter(
     *     name="_where",
     *     in="query",
     *     description="Filtre les données (opérations supportées : and, eq, neq, in, nin, le, lt, ge, gt) <table summary=''><colgroup><col><col>        <col>        <col>       </colgroup>       <thead style='text-align:left;'>        <tr>           <th id='d23261e63'>              <div class='bx--table-header-label'>Opérateur</div>           </th>           <th id='d23261e66'>              <div class='bx--table-header-label'>Syntaxe</div>           </th>           <th id='d23261e69'>              <div class='bx--table-header-label'>Exemple</div>           </th>           <th id='d23261e72'>              <div class='bx--table-header-label'>Description</div>           </th>        </tr>       </thead>       <tbody>        <tr>          <td headers='d23261e63 '>And</td>            <td headers='d23261e66 '><code >and</code></td>           <td headers='d23261e69 '><code >?_where=and(eq(<var >com_nom</var>,Rennes),eq(<var >acc</var>,Intérieur))</code></td>           <td headers='d23261e72 '><code ><var >com_nom</var></code> vaut <code >Rennes</code> et              <code ><var >acc</var></code> vaut Intérieur.           </td>        </tr>        <tr>           <td headers='d23261e63 '>Equal</td>           <td headers='d23261e66 '><code >eq</code></td>           <td headers='d23261e69 '><code >?_where=eq(<var >com_nom</var>,Rennes)</code></td>           <td headers='d23261e72 '><code ><var >com_nom</var></code> vaut <code >Rennes</code>.</td>        </tr> 	     <tr>           <td headers='d23261e63 '>Not equal to</td>           <td headers='d23261e66 '><code >neq</code></td>           <td headers='d23261e69 '><code >?_where=neq(<var >com_nom</var>,Rennes)</code></td>           <td headers='d23261e72 '><code ><var >com_nom</var></code> vaut tout sauf              <code >Rennes</code>.           </td>        </tr>             <tr>           <td headers='d23261e63 '>Greater than or equal to</td>           <td headers='d23261e66 '><code >ge</code></td>           <td headers='d23261e69 '><code >?_where=ge(<var >date_instal</var>,2022-06-08)</code></td>           <td headers='d23261e72 '><code ><var >date_instal</var></code> est supérieure ou égale à 2022-06-08.</td>        </tr>        <tr>           <td headers='d23261e63 '>Greater than</td>           <td headers='d23261e66 '><code >gt</code></td>           <td headers='d23261e69 '><code >?_where=gt(<var >date_instal</var>,2022-06-08)</code></td>           <td headers='d23261e72 '><code ><var >date_instal</var></code> est inférieure à 2022-06-08.</td>        </tr>        <tr>           <td headers='d23261e63 '>Less than or equal to</td>           <td headers='d23261e66 '><code >le</code></td>           <td headers='d23261e69 '><code >?_where=le(<var >date_instal</var>,2022-06-08)</code></td>           <td headers='d23261e72 '><code ><var >date_instal</var></code> est inférieure ou égale à 2022-06-08.</td>        </tr>        <tr>           <td headers='d23261e63 '>Less than</td>           <td headers='d23261e66 '><code >lt</code></td>           <td headers='d23261e69 '><code >?_where=lt(<var >date_instal</var>,2022-06-08)</code></td>           <td headers='d23261e72 '><code ><var >date_instal</var></code> est inférieure than 2022-06-08.</td>        </tr>        <tr>           <td headers='d23261e63 '>In</td>           <td headers='d23261e66 '><code >in</code></td>           <td headers='d23261e69 '><code >?_where=in(<var >com_nom</var>,Rennes,Nantes)</code></td>           <td headers='d23261e72 '><code ><var >com_nom</var></code> vaut <code >Rennes</code> ou              <code >Nantes</code>.           </td>        </tr>        <tr>           <td headers='d23261e63 '>Not in</td>           <td headers='d23261e66 '><code >nin</code></td>           <td headers='d23261e69 '><code >?_where=nin(<var >com_nom</var>,Rennes,Nantes)</code></td>           <td headers='d23261e72 '><code ><var >com_nom</var></code> ne vaut pas <code >Rennes</code> ou              <code >Nantes</code>.           </td>        </tr>     </tbody>  </table>",
     *     required=false,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="sort_by",
     *     in="query",
     *     description="champ de tri des données (nom_champ:asc par ordre croissant ou nom_champ:desc par ordre décroissant)",
     *     @OA\Schema(type="string"),
     *     required=false
     * )
     * @OA\Parameter(
     *     name="spatialfilter",
     *     in="query",
     *     description="Filtre les données contenues dans le cercle de centre (x,y) dans le système de coordonnées de la donnée et de rayon 'radius' exprimé en mètres / syntaxe : spatialfilter=nearby(x,y,radius)",
     *     @OA\Schema(type="string"),
     *     required=false
     * )
     * @OA\Parameter(
     *     name="srs",
     *     in="query",
     *     description="Système de coordonnées dans lequel les données sont récupérées, au format EPSG:XXXX. Par défaut, les données sont fournies dans le système de coordonnées de la donnée",
     *     @OA\Schema(type="string"),
     *     required=false
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Tag(name="Data")
     *
     *
     * @return JsonResponse
     */
    public function getDataAction(Request $request, $uuid = "")
    {
        $type = $request->get("nature", "view");
        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => ($type == 'view' ? "NAVIGATION" : "EDITION"),
            "OBJET_TYPE" => "dataset"
        );

        // check rights
        $serv = $this->container->get('prodige.verifyrights');
        $req = new Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);

        $ipAuthorize = $this->isIpAuthorize($request, $uuid);
        if($ipAuthorize){
            $rights['NAVIGATION'] = true;
        }

        $tableName = $rights["layer_table"];
        $tabInfoTables = explode(".", $tableName);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);

        return $this->getDataOfTable($request, $uuid, $schemaName, $tableName, $rights);
    }

    /**
     * Récupération d'un brouillon de données au format GeoJson
     *
     * @Route("/data/draft/{uuid}", methods={"GET"})
     * @OA\Get(
     *     summary="Récupération d'un brouillon de données d'une ressource"
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="nature",
     *     in="query",
     *     description="nature des données (visibles (view) ou éditables (edit))",
     *     required=false,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="bbox",
     *     in="query",
     *     description="emprise des données souhaitées",
     *     required=false,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="gid",
     *     in="query",
     *     description="gid de la donnée souhaitée",
     *     required=false,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="filter",
     *     in="query",
     *     description="filtre sur les données, paramètre dépendant de la structure de la donnée (cf operation structure), ex : https://[CATALOGUE_URL]/api/data/[uuid]?departement=35",
     *     required=false,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="search",
     *     in="query",
     *     description="filtre sur les données, par une recherche plein texte sur les colonnes de la donnée, ex : https://[CATALOGUE_URL]/api/data/[uuid]?search=paris",
     *     required=false,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="limit",
     *     in="query",
     *     description="nombre de données maximal en retour",
     *     required=false,
     *     @OA\Schema(type="integer")
     * )
     * @OA\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Indice de départ de pagniation (URLs de pagination (prev, next, self, first, last) contenus dans le header de réponse Link)",
     *     required=false,
     *     @OA\Schema(type="integer")
     * )
     * @OA\Parameter(
     *     name="_where",
     *     in="query",
     *     description="Filtre les données (opérations supportées : and, eq, neq, in, nin, le, lt, ge, gt) <table summary=''><colgroup><col><col>        <col>        <col>       </colgroup>       <thead style='text-align:left;'>        <tr>           <th id='d23261e63'>              <div class='bx--table-header-label'>Opérateur</div>           </th>           <th id='d23261e66'>              <div class='bx--table-header-label'>Syntaxe</div>           </th>           <th id='d23261e69'>              <div class='bx--table-header-label'>Exemple</div>           </th>           <th id='d23261e72'>              <div class='bx--table-header-label'>Description</div>           </th>        </tr>       </thead>       <tbody>        <tr>          <td headers='d23261e63 '>And</td>            <td headers='d23261e66 '><code >and</code></td>           <td headers='d23261e69 '><code >?_where=and(eq(<var >com_nom</var>,Rennes),eq(<var >acc</var>,Intérieur))</code></td>           <td headers='d23261e72 '><code ><var >com_nom</var></code> vaut <code >Rennes</code> et              <code ><var >acc</var></code> vaut Intérieur.           </td>        </tr>        <tr>           <td headers='d23261e63 '>Equal</td>           <td headers='d23261e66 '><code >eq</code></td>           <td headers='d23261e69 '><code >?_where=eq(<var >com_nom</var>,Rennes)</code></td>           <td headers='d23261e72 '><code ><var >com_nom</var></code> vaut <code >Rennes</code>.</td>        </tr> 	     <tr>           <td headers='d23261e63 '>Not equal to</td>           <td headers='d23261e66 '><code >neq</code></td>           <td headers='d23261e69 '><code >?_where=neq(<var >com_nom</var>,Rennes)</code></td>           <td headers='d23261e72 '><code ><var >com_nom</var></code> vaut tout sauf              <code >Rennes</code>.           </td>        </tr>             <tr>           <td headers='d23261e63 '>Greater than or equal to</td>           <td headers='d23261e66 '><code >ge</code></td>           <td headers='d23261e69 '><code >?_where=ge(<var >date_instal</var>,2022-06-08)</code></td>           <td headers='d23261e72 '><code ><var >date_instal</var></code> est supérieure ou égale à 2022-06-08.</td>        </tr>        <tr>           <td headers='d23261e63 '>Greater than</td>           <td headers='d23261e66 '><code >gt</code></td>           <td headers='d23261e69 '><code >?_where=gt(<var >date_instal</var>,2022-06-08)</code></td>           <td headers='d23261e72 '><code ><var >date_instal</var></code> est inférieure à 2022-06-08.</td>        </tr>        <tr>           <td headers='d23261e63 '>Less than or equal to</td>           <td headers='d23261e66 '><code >le</code></td>           <td headers='d23261e69 '><code >?_where=le(<var >date_instal</var>,2022-06-08)</code></td>           <td headers='d23261e72 '><code ><var >date_instal</var></code> est inférieure ou égale à 2022-06-08.</td>        </tr>        <tr>           <td headers='d23261e63 '>Less than</td>           <td headers='d23261e66 '><code >lt</code></td>           <td headers='d23261e69 '><code >?_where=lt(<var >date_instal</var>,2022-06-08)</code></td>           <td headers='d23261e72 '><code ><var >date_instal</var></code> est inférieure than 2022-06-08.</td>        </tr>        <tr>           <td headers='d23261e63 '>In</td>           <td headers='d23261e66 '><code >in</code></td>           <td headers='d23261e69 '><code >?_where=in(<var >com_nom</var>,Rennes,Nantes)</code></td>           <td headers='d23261e72 '><code ><var >com_nom</var></code> vaut <code >Rennes</code> ou              <code >Nantes</code>.           </td>        </tr>        <tr>           <td headers='d23261e63 '>Not in</td>           <td headers='d23261e66 '><code >nin</code></td>           <td headers='d23261e69 '><code >?_where=nin(<var >com_nom</var>,Rennes,Nantes)</code></td>           <td headers='d23261e72 '><code ><var >com_nom</var></code> ne vaut pas <code >Rennes</code> ou              <code >Nantes</code>.           </td>        </tr>     </tbody>  </table>",
     *     required=false,
     *     @OA\Schema(type="string")
     * )
     * @OA\Parameter(
     *     name="sort_by",
     *     in="query",
     *     description="champ de tri des données (nom_champ:asc par ordre croissant ou nom_champ:desc par ordre décroissant)",
     *     @OA\Schema(type="string"),
     *     required=false
     * )
     * @OA\Parameter(
     *     name="spatialfilter",
     *     in="query",
     *     description="Filtre les données contenues dans le cercle de centre (x,y) dans le système de coordonnées de la donnée et de rayon 'radius' exprimé en mètres / syntaxe : spatialfilter=nearby(x,y,radius)",
     *     @OA\Schema(type="string"),
     *     required=false
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Tag(name="Data")
     * 
     *
     *
     * @return JsonResponse
     */
    public function getDataDraftAction(Request $request, $uuid = "")
    {
        
        $type = $request->get("nature", "view");
        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => ($type == 'view' ? "NAVIGATION" : "EDITION"),
            "OBJET_TYPE" => "dataset"
        );

        // check rights
        $serv = $this->container->get('prodige.verifyrights');
        $req = new Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);

        $ipAuthorize = $this->isIpAuthorize($request, $uuid);
        if($ipAuthorize){
            $rights['NAVIGATION'] = true;
        }

        $tableName = $rights["layer_table"];
        $tabInfoTables = explode(".", $tableName);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);

        $tableName = $this->getDraftTable($schemaName, $tableName, false);
        if (is_null($tableName)) {
            $msg = "Pas de brouillon pour cette ressource";
            return new JsonResponse($msg, 404);
        }
        $type = $request->get("nature", "view");
        
        //force rights to own drafts
        if($type==='view'){
            $rights["NAVIGATION_ATTRIBUTE_FILTER"] = array(
                "column" => "_userid_modification",
                "value"  => User::GetUserId()
            );
        }elseif($type==='edit'){
            $rights["EDITION_ATTRIBUTE_FILTER"] = array(
                "column" => "_userid_modification",
                "value"  => User::GetUserId()
            );
        }
        return $this->getDataOfTable($request, $uuid, $schemaName, $tableName, $rights);
    }

    private function getDataOfTable(Request $request, $uuid, $schemaName, $tableName, $rights)
    {
        $dataTable = $schemaName . '.' . $tableName;
        $callback = $request->get('callback');
        $PRODIGE = $this->getProdigeConnection('carmen');

        $type = $request->get("nature", "view");
        $bbox = $request->get("bbox", "");
        $gid = $request->get("gid", null);
        $limit = $request->get('limit',null);
        $offset = $request->get('offset',0);
        $_where = $request->get("_where");
        $search = $request->get('search',"");
        $spatialfilter = $request->get("spatialfilter");
        $srsDestination = $request->get("srs", "");

        $database_columns = [];

        if ($type !== "view" && $type !== "edit") {
            $msg = "type non reconnu";

            return new JsonResponse($msg, 400);
        }

        if ($type === "view" && !@$rights['NAVIGATION']) {
            $msg = "Accès interdit aux données en consultation";

            return new JsonResponse($msg, 403);
        }

        if ($type === "edit" && !(@$rights['EDITION'] || @$rights['EDITION_MODIFICATION'] || @$rights['EDITION_AJOUT'])) {
            $msg = "Accès interdit aux données en édition ";

            return new JsonResponse($msg, 403);
        }

        if ($type == 'view') {
            $territoire = isset($rights["RESTRICTION_TERRITORIALE"]) && $rights["RESTRICTION_TERRITORIALE"] ===true && isset($rights["NAVIGATION_AREA"]) ? $rights["NAVIGATION_AREA"] : array();
            $filter_data = isset($rights["NAVIGATION_ATTRIBUTE_FILTER"]) ? $rights["NAVIGATION_ATTRIBUTE_FILTER"] : false;
        } else {
            $territoire = isset($rights["EDITION_AREA"]) ? $rights["EDITION_AREA"] : array();
            $filter_data = isset($rights["EDITION_ATTRIBUTE_FILTER"]) ? $rights["EDITION_ATTRIBUTE_FILTER"] : false;
        }

        $couche_type = $rights["couche_type"];
        $tabFilterData = [];
        //filters 
        $enum = $this->_getAllDataType($dataTable);
        //print(json_encode($enum));
        foreach($enum as $enum_=> $enumInfos){
            if($request->get($enumInfos["column_name"], "")!==''){
                //TODO manage all case, depending on data type
                $tabFilterData[$enumInfos["column_name"]] = "'".$request->get($enumInfos["column_name"])."'";
            }
        }
        if ($couche_type == -3) {
            $tabSelect[] = "a.*";
            $columnTypes = $this->_getAllDataType($dataTable);
            foreach ($columnTypes as $indice => $infos) {
                $this->column_types[$infos["column_name"]] = $infos["data_type"];
            }

        } else {
            //get fields name and types
            $columnTypes = $this->_getAllDataType($dataTable);
            foreach ($columnTypes as $indice => $infos) {
                $this->column_types[$infos["column_name"]] = $infos["data_type"];
            }
            //print(json_encode($this->column_types));

            //get fields name from default representation
            //first detect if map exists in carmen database
            $map = $PRODIGE->fetchAllAssociative(
                "select field.*, layer.layer_field_url from carmen.field inner join carmen.layer on field.layer_id = layer.layer_id"
                . " inner join carmen.map on layer.map_id = map.map_id  "
                . " where map_wmsmetadata_uuid = :uuid and published_id is null "
                . " and layer.layer_metadata_uuid =:uuid "
                . " and field.field_queries=true"
                . " and field.field_duplicate=false"
                . " order by field_rank",
                array("uuid" => $uuid)
            );
            if (empty($map)) {
                $msg = "Erreur de configuration, les champs de la représentation par défaut ne sont pas définis ";

                return new JsonResponse($msg, 500);
            }

            $tabSelect = array("a.gid");
            
            foreach ($map as $key => $columns) {
                $strSelect = "";
                //for types IMAGE and URL, concat with layer_field_url
                //TODO manage all cases (cf admincarto behaviour)
                if ($columns["field_type_id"] == 2 || $columns["field_type_id"] == 3) {
                    if ($columns["layer_field_url"] != "") {
                        $strSelect .= "'".pg_escape_string($columns["layer_field_url"])."' ||";
                    }
                    if ($columns["field_url"] != "") {
                        $strSelect .= "'".pg_escape_string($columns["field_url"])."' ||";
                    }
                }
                //array_push($database_columns, $columns["field_name"]);
                foreach ($columnTypes as $indice => $infos) {
                    if ($infos["column_name"] == $columns["field_name"]) {
                        //specific format for date fields

                        switch ($infos["data_type"]) {
                            case "date":
                                $strSelect .= "to_char(a.".$columns["field_name"].", 'YYYY-MM-DD') as \"".addslashes(
                                        $columns["field_name"]
                                    )."\"";
                                break;
                            case "ARRAY":
                                $strSelect .= "to_json(a.".$columns["field_name"].") as \"".addslashes(
                                        $columns["field_name"]
                                    )."\"";
                                break;
                            default:
                                $strSelect .= "a.".$columns["field_name"]." as \"".addslashes(
                                        $columns["field_name"]
                                    )."\"";
                                break;
                        }

                        $tabSelect[] = $strSelect;
                        break;
                    }
                }
            }
            $tabSelect[] = "a.the_geom";
        }
        //$this->authorized_columns = array_filter($database_columns);
        $this->authorized_columns = array_keys($this->column_types);

        //3 stream Data as geojson
        $tmpFile = CARMEN_PATH_CACHE . $tableName . "_" . uniqid('', true). ".json";

        $desc = $this->getGeometryInfo($schemaName, $tableName);
        $srid = $desc["srid"];
        $srs = "EPSG:".$srid;
        if ($srsDestination===''){
            $srsDestination = $srs;
        } 
        $sridDestination = str_replace("EPSG:", "", $srsDestination);

        $conn = $this->getConnection('prodige');

        $cmd = $this->container->getParameter("CMD_OGR2OGR")." -gt 10000 -f GeoJSON ".$tmpFile." ".
            ($couche_type != -3 ?
                "-t_srs ".$srsDestination."  -a_srs ".$srsDestination." -s_srs ".$srs." -nlt \"PROMOTE_TO_MULTI\" "
                : "").
            "\"PG:host=".$conn->getHost()." dbname=".$conn->getDatabase()." user=".$conn->getUsername(
            )." password=".$conn->getPassword()." port=".$conn->getPort()."\" ".
            "-sql ";

        $sql = "select " . implode(",", $tabSelect) . " from " . $dataTable . " a ";

        $filter = " where 1=1 ";
        //filtre par restriction territoriale
        if (!empty($territoire)) {
            if ($territoire->filter_table) {
                $sql .= " inner join ".$territoire->filter_table." b on (a.the_geom && b.the_geom and ".
                    " st_intersects(a.the_geom, b.the_geom))";
                $filter = " where ".$territoire->filter_field." in ('".implode(
                        "','",
                        $territoire->filter_values
                    )."') ";
            }
        }
        
        //filtre par restriction attributaire
        if ($filter_data) {
            $filter .= " and a.".$filter_data["column"]." = '".($filter_data["value"])."' ";
        }

        //recherche plein texte sur l'ensemble de la table
        if($search && $search!==""){
            //TODO faire évoluer le paramétrage API pour spécifier les champs de recherche textuel
            //$filter .= " and  to_tsvector('french', COALESCE(".implode("::text,'') || ' ' || COALESCE(", $this->authorized_columns) .")) @@ plainto_tsquery('french', '".pg_escape_string($search)."') ";
            $filter.= " and a::text ilike '%".pg_escape_string($search)."%' ";
        }

        //TODO régler le problème des commandes trop longues (liste à rallonge des territoires)
        if ($bbox !== "") {
            $extent = json_decode($bbox);
            //bbox format : [99225.999999816, 6008488.9999994, 1242375, 7151637.9999996]
            if ($extent) {
                $filter .= empty($filter) ? " where " : " and ";
                if($srsDestination !== $srs){
                    $filter .= " a.the_geom && st_transform(ST_MakeEnvelope(".$extent[0].", ".$extent[1].", ".$extent[2].", ".$extent[3].", ".$sridDestination."), ".$srid.")";
                }else{
                    $filter .= " a.the_geom && ST_MakeEnvelope(".$extent[0].", ".$extent[1].", ".$extent[2].", ".$extent[3].", ".$srid.")";
                }
                
            }
        }

        //filtres API
        foreach ($tabFilterData as $filerField => $filterValue) {
            $filter .= empty($filter) ? " where " : " and ";
            $filter .= " a.".$filerField." = ".$filterValue;
        }

        //when the only right is ADD, load no data
        if ($type === "edit" && @$rights['EDITION_AJOUT'] && !(@$rights['EDITION'] || @$rights['EDITION_MODIFICATION'])) {
            $filter = "where 1=0";
        }

        //Sort by
        if($request->query->has("sort_by")){
            $order_split = explode(":",$request->query->get("sort_by"));
            if(count($order_split) != 2){
                return new JsonResponse("sort_by : doit être de la forme champ:asc ou champ:desc", Response::HTTP_BAD_REQUEST);
            }
            if(!in_array($order_split[0],$this->authorized_columns)){
                return new JsonResponse("sort_by : champ incorrect. Champs disponibles : ".json_encode($this->authorized_columns), Response::HTTP_BAD_REQUEST);
            }
            if(!in_array($order_split[1],["asc","desc"])){
                return new JsonResponse("sort_by : l'ordre doit être asc ou desc", Response::HTTP_BAD_REQUEST);
            }
            $sort_by = ["champ" => $order_split[0], "ordre" => strtoupper($order_split[1])];
        }else{
            $sort_by = ["champ" => "gid", "ordre" => "ASC"];
        }
        
        //spatialfilter
        if($spatialfilter != null){
            $matches = [];
            try{
                preg_match("/nearby\([0-9.,-]+\)/",$spatialfilter,$matches);
                if(empty($matches) || empty($matches[0])){
                    throw new \Exception();
                }
                $params = preg_replace("/nearby/","",$matches[0]);
                $params_ = explode(",",substr($params,1,strlen($params)-2));
                $spatialfilter_x = $params_[0];
                $spatialfilter_y = $params_[1];
                $spatialfilter_buffer = $params_[2];
            }catch(\Exception $e){
                return new JsonResponse("spatial_filter mal formaté", Response::HTTP_BAD_REQUEST);
            }
            //for spherical projcetion
            if($srid===4326){
                $filter .= empty($filter) ? " where " : " AND ";
                $filter .=  "st_contains(cast (st_buffer(cast (st_setsrid(st_point($spatialfilter_x,$spatialfilter_y),".strval($srid).") as geography) ,$spatialfilter_buffer) as geometry),a.the_geom)";
            }else{
                $filter .= empty($filter) ? " where " : " AND ";
                $filter .=  "st_contains(st_buffer(st_setsrid(st_point($spatialfilter_x,$spatialfilter_y),".strval($srid)."),$spatialfilter_buffer),a.the_geom)";
            }
            
            
        }

        
        try{
            $filter .= ( $_where != null && !empty($_where)) ? $this->parseWhereQuery($_where) : "";
        }catch(\Exception $e){
            return new JsonResponse("Error parsing _where parameter".$e->getMessage(), Response::HTTP_BAD_REQUEST);
        }
        

        //sort_by
        $filter .= " ORDER BY " . $sort_by["champ"] . " " . $sort_by["ordre"] . " ";

        //limit and offset
        if($limit != null){
            $filter .= " LIMIT {$limit} ";
        }
        $filter .= " OFFSET {$offset} ";
        
        $sql .= $filter;

        $sql_count = "SELECT count(*) from " . explode("ORDER",explode("from",$sql,2)[1],2)[0];
        $total_amount = $this->_getCountData($sql_count);
        //print(json_encode($sql_count));


        //write sql request in file since it can be long
        $tmpfname = tempnam(CARMEN_PATH_CACHE, 'sqlrequest');
        $fp = fopen($tmpfname, "w");
        fwrite($fp, $sql);
        fclose($fp);
        //print($sql);
        
        $cmd .= "@" . $tmpfname;

        $this->getLogger()->info("CMD : " . $cmd);
        $this->getLogger()->info($sql);

        $process = Process::fromShellCommandline($cmd);

        $process->setTimeout(10800);
        $process->run();

        unlink($tmpfname); // erase temp file

        if (!file_exists($tmpFile)) {
            $msg = "Erreur lors de la génération des données ";

            return new JsonResponse($msg, 500);
        }
        $fileData = file_get_contents($tmpFile);

        //Pagination Links
        
        $self = "<https://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ;
        if($this->str_contains($self, "offset=") && ($limit != null) ){
            $next = $offset + $limit < $total_amount ? preg_replace("/offset=[0-9]+/","offset=".strval($offset + $limit),$self) . ">; rel=\"next\"" : "";
            $first = preg_replace("/offset=[0-9]+/","offset=0",$self) . ">; rel=\"first\"";
            $last = preg_replace("/offset=[0-9]+/","offset=".strval($total_amount - ($total_amount%$limit)),$self) . ">; rel=\"last\"";
            $prev = $offset > 0 ? preg_replace("/offset=[0-9]+/","offset=".strval($offset - $limit),$self) . ">; rel=\"prev\"" : "" ;
        }else if($limit != null){
            $next = $offset + $limit < $total_amount ? $self."&offset=".strval($offset + $limit) . ">; rel=\"next\"" : "";
            $first = $self."&offset=0". ">; rel=\"first\"";
            $last = $self."&offset=".strval($total_amount - ($total_amount%$limit)) . ">; rel=\"last\"";
            $prev = $offset > 0 ? $self."&offset=".strval($offset - $limit) . ">; rel=\"prev\"" : "" ;
        }else{//sans pagination
            $next = $self . ">; rel=\"next\"";
            $first = $self. ">; rel=\"first\"";
            $last = $self.">; rel=\"last\"";
            $prev = $self.">; rel=\"prev\"";
        }
        $self = $self . ">; rel=\"self\"";
        

        $headers["Access-Control-Allow-Origin"] = "*";
        $headers["X-Total-Count"] = $total_amount;
        $headers["Link"] = implode(",",array_filter([$self,$next,$first,$last,$prev]));

        $response = new JsonResponse(json_decode($fileData, true), 200, $headers);

        $response->expire();
        if ($callback != "") {
            $response->setCallback($callback);
        }

        return $response;
    }

    /**
     * Récupération de la structure d'une ressource
     *
     *
     * @Route("/structure/{uuid}", methods={"GET"})
     *
     * https://www.postgresql.org/docs/8.0/infoschema-columns.html
     * #### Response sample with _device\_id_
     * ```
     * [
     *    {
     *    "id": "000001cb",
     *    "name": "Clos Joury - Extension Elec",
     *    "picture": "",
     *    "active": 1,
     *    "model": 24,
     *    "latitude": "48.13506",
     *    "longitude": "-1.62209",
     *    "characs":{
     *        "nom_site": "Le Rheu - Clos Joury",
     *        "date_activation": "2017-07-12 00:00:00",
     *        "period": "60",
     * ```
     *
     * @OA\Get(
     *     summary="Récupération de la structure d'une ressource"
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Tag(name="Data")
     *
     * @return JsonResponse
     */
    public function getStructureAction(Request $request, $uuid = "")
    {
        $PRODIGE = $this->getProdigeConnection('carmen');
        //1 TODO check data existence

        //2 check rights
        $serv = $this->container->get('prodige.verifyrights');

        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => "NAVIGATION|EDITION",
        );
        $getParams['OBJET_TYPE'] = "dataset";

        $req = new \Symfony\Component\HttpFoundation\Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);

        $ipAuthorize = $this->isIpAuthorize($request, $uuid);
        if($ipAuthorize){
            $rights['NAVIGATION'] = true;
        }

        //if not one right on data, no right to structure
        if (!@$rights['NAVIGATION'] && !@$rights['EDITION'] && !@$rights['EDITION_AJOUT'] && !@$rights['EDITION_MODIFICATION']) {
            $msg = "Accès interdit aux données ";

            return new JsonResponse($msg, 403);
        }

        $data = $rights["layer_table"];

        //get fields name and types
        $res = $this->_getAllDataType($data);

        return new JsonResponse($res, 200);
    }

    protected function _getCountData($sql)
    {
        $PRODIGE = $this->getProdigeConnection('public');

        return $PRODIGE->executeQuery($sql)->fetchAll()[0]["count"];
    }

    protected function _getAllDataType($tableSourceName)
    {

        $PRODIGE = $this->getProdigeConnection('public');

        $query = "SELECT  c.*, " ./*" coalesce(e.data_type, c.data_type) as data_type,"*/ 
                " e.object_type, pg_description.description , " .
                " t.constraint_type as is_unique".
            " FROM information_schema.columns c" .
            " LEFT JOIN information_schema.element_types e ON ((c.table_catalog, c.table_schema, c.table_name, 'TABLE', c.dtd_identifier) = (e.object_catalog, e.object_schema, e.object_name, e.object_type, e.collection_type_identifier)) " .
            " LEFT JOIN pg_description on (objoid=(table_schema||'.'||table_name)::regclass and objsubid=ordinal_position)" .
            " LEFT JOIN information_schema.constraint_column_usage AS ccu ON 
              ccu.constraint_schema = c.table_schema AND c.column_name = ccu.column_name AND c.table_name = ccu.table_name  ".
            " LEFT JOIN information_schema.table_constraints AS t ON t.table_schema = ccu.table_schema and t.table_name = ccu.table_name AND t.constraint_name = ccu.constraint_name AND  t.constraint_type = 'UNIQUE' ".
            " WHERE c.table_name = :table_name AND c.udt_name != 'geometry' " .
            /*TODO colonnes à choix multiple : retirer cette condition" and e.data_type is null". */
            " ORDER BY c.ordinal_position";
        //print($query);

        $tableSourceName = explode(".", $tableSourceName);
        $tableSourceName = end($tableSourceName);

        $params = array('table_name' => $tableSourceName);
        $stmt = $PRODIGE->executeQuery($query, $params);
        $res = array();

        foreach ($stmt as $row) {
            //API does not present some column which description is CALCUL_AUTOMATIQUE
            if ($row["description"] && stripos($row["description"], self::CALCUL_AUTOMATIQUE) !== false) {
                continue;
            }

            $field = array();
            $field['column_name'] = utf8_encode($row['column_name']);
            $field['data_type'] = $row['data_type'];

            $field['udt_name'] = preg_replace("!^_!", "", $row['udt_name']);
            $field['column_default'] = $row['column_default'];
            if (strpos($row['column_default'], "::".$field['udt_name']) !== false) {
                //remove quotes and :: from column_default for USER-DEFINED
                $field['column_default'] = str_replace(
                    "'",
                    "",
                    str_replace("::".$field['udt_name'], "", $field['column_default'])
                );
            }

            $field['character_maximum_length'] = $row['character_maximum_length'] ?: $row['numeric_precision'];
            $field['is_nullable'] = ($row['is_nullable'] === "YES");
            $field['domain_name'] = $row['domain_name'];
            $field["object_type"] = $row['object_type'];
            $field["description"] = $row['description'];

            $field["is_unique"] = !!$row["is_unique"];
            $res[] = $field;
        }

        return $res;
    }

    /**
     * get data for present GID to log it before update
     *
     */
    protected function _getData($tableSourceName, $gid)
    {

        $conn_prodige = $this->getConnection('prodige');

        $tableSourceName = explode(".", $tableSourceName);
        $tableSourceName = end($tableSourceName);

        $query = "SELECT * from ".$tableSourceName." where gid=:gid";

        $params = array('gid' => $gid);
        $stmt = $conn_prodige->fetchAssociative($query, $params);

        return json_encode($stmt);
    }

        /**
     * get data for present GID to log it before update
     * 
     */
    protected function _getDataByColumnName($tableSourceName, $gid, $columnId)
    {

        $conn_prodige = $this->getConnection('prodige');

        $tableSourceName = explode(".", $tableSourceName);
        $tableSourceName = end($tableSourceName);

        $query = "SELECT * from " . $tableSourceName . " where $columnId=:$columnId";

        $params = array('$olumnId' => $gid);
        $stmt = $conn_prodige->fetchAssoc($query, $params);

        return json_encode($stmt);
    }

    /**
     * Get geometry infos
     * @param schemaName : schema name
     * @param tableName : table name
     *
     */
    protected function getGeometryInfo($schemaName, $tableName)
    {

        $conn_prodige = $this->getConnection('prodige');
        $desc = $conn_prodige->fetchAssociative(
            "SELECT f_table_schema, f_geometry_column, srid, type from geometry_columns where f_table_schema=:schema and f_table_name = :table",
            array(
                "table" => strtolower($tableName),
                "schema" => strtolower($schemaName),
            )
        );

        return $desc;
    }

    /**
     * Ajout de données pour une ressource
     *
     *
     * @Route("/data/{uuid}", methods={"POST"})
     *
     * @OA\Post(
     *     summary="Ajout de données pour une ressource",
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\RequestBody(
     *     description="données au format geoJSON (featureCollection).",
     *     required=true,
     *     @OA\JsonContent(type="object"),
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Response(
     *     response="400",
     *     description="Requête invalide"
     * )
     * @OA\Tag(name="Data")
     *
     * @return JsonResponse
     */
    public function postDataAction(Request $request, $uuid = "")
    {
        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => "EDITION",
            "OBJET_TYPE" => "dataset"
        );

        $serv = $this->container->get('prodige.verifyrights');
        $req = new Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);

        $tableName = $rights["layer_table"];
        $tabInfoTables = explode(".", $tableName);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);

        return $this->postDataIntoTable($request, $schemaName, $tableName, $rights, true);
    }

    /**
     * Ajout d'un brouillon de données pour une ressource
     *
     * @Route("/data/draft/{uuid}", methods={"POST"})
     *
     * @OA\Post(
     *     summary="Ajout d'un brouillon de données pour une ressource",
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\RequestBody(
     *     description="données au format geoJSON (featureCollection).",
     *     required=true,
     *     @OA\JsonContent(type="object"),
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Response(
     *     response="400",
     *     description="Requête invalide"
     * )
     * @OA\Tag(name="Data")
     *
     *
     * @return JsonResponse
     */
    public function postDataDraftAction(Request $request, $uuid = "")
    {
        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => "EDITION",
            "OBJET_TYPE" => "dataset"
        );

        $serv = $this->container->get('prodige.verifyrights');
        $req = new Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);

        $tableName = $rights["layer_table"];
        $tabInfoTables = explode(".", $tableName);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);
        $tableName = $this->getDraftTable($schemaName, $tableName, true);

        return $this->postDataIntoTable($request, $schemaName, $tableName, $rights, false);
    }

    private function postDataIntoTable(Request $request, $schemaName, $tableName, $rights, $bCheckData=true)
    {

        $strJson = $request->getContent();
        //log post datas (date,user...)
        $logFile = "api_data";
        $user = User::GetUser();

        Logs::AddLogs($logFile, array($user->GetNom(), $user->GetPrenom(), $user->GetLogin(), $strJson));

        if (!@$rights['EDITION'] && !@$rights['EDITION_AJOUT']) {
            $msg = "Accès interdit aux données ";

            return new JsonResponse($msg, 403);
        }

        $geoJson = json_decode($strJson, true);

        if (!$geoJson || !isset($geoJson["features"])) {
            $msg = "Contenu invalide";

            return new JsonResponse($msg, 400);
        }

        $pk_couche_donnees = $rights["pk_data"];
        $donnee = $rights["couche_nom"];
        $territoire = isset($rights["EDITION_AREA"]) ? $rights["EDITION_AREA"] : array();

        $desc = $this->getGeometryInfo($schemaName, $tableName);
        $srid = $desc["srid"];
        $geomType = $desc["type"];

        //Adaptation de la structure de table si besoin
        $dataTable = $schemaName . '.' . $tableName;
        $editionController = new EditionController();
        $editionController->setContainer($this->container);
        $editionController->initialize((array)$territoire, $srid, $tableName, $pk_couche_donnees);

        $editionController->_checkLockOnTable($dataTable, false, "_userid_creation", "integer");
        $editionController->_checkLockOnTable($dataTable, false, "_userid_modification", "integer");
        $editionController->_checkLockOnTable($dataTable, false, "_edit_datemaj", "timestamp");
        $editionController->_checkLockOnTable($dataTable, false, "_edit_datecrea", "timestamp");


        //list of gid inserted
        $gids = [];
        $results = [];

        foreach ($geoJson["features"] as $key => $feature) {
            $editionController->editionTablename = $tableName;
            $geometry = $feature["geometry"];
            //control validity
            if ($bCheckData && !$editionController->_checkConstraint(json_encode($geometry), "", "isvalid", null, "Geojson")) {
                // test de validité de la géométrie
                $res = array(
                    "check" => false,
                    "msg" => "La géométrie n'est pas valide",
                    "code" => self::INVALID_GEOMETRY,
                    "reject" => true,
                );
                $results[] = $res;
                break;
            }
            if($bCheckData){
                $tabConstraints =  $editionController->_checkUniquConstraint($feature['properties']);
                if(!empty($tabConstraints)) {
                    foreach($tabConstraints as $column => $bChecked){
                        if (!$bChecked){
                            $res = array(
                                "check" => false,
                                "msg" => "la valeur de l'attribut ".$column." n'est pas unique" ,
                                "code" => self::INVALID_ATTRIBUTES,
                                "reject" => true
                            );
                            $results[] = $res;
                            break 2;
                        }
                    }
                }
            }

            //control area and attribute rights
            //if rights["EDITION_ATTRIBUTE_FILTER"] is not set, no attribute restriction

            if ($bCheckData && $rights["EDITION_ATTRIBUTE_FILTER"]&& !$editionController->_checkData(
                    $feature['properties'],
                    $rights['EDITION_ATTRIBUTE_FILTER']['column'],
                    $rights['EDITION_ATTRIBUTE_FILTER']['value']

                )) {
                $res = array(
                    "check" => false,
                    "msg" => "La saisie n'est autorisée qu'avec la valeur ". $rights['EDITION_ATTRIBUTE_FILTER']['value']. " pour le champ ".$rights['EDITION_ATTRIBUTE_FILTER']['column'],
                    "code" => self::INVALID_DATA_RIGHTS,
                    "reject" => true
                );
                $results[] = $res;
                break;
            }
            //if rights["EDITION_AREA"] is not set, no territorial restriction
            if ($bCheckData && isset($rights["EDITION_AREA"]) && !$editionController->_checkConstraint(
                    json_encode($geometry),
                    "",
                    "territoire",
                    null,
                    "Geojson"
                )) {
                $res = array(
                    "check" => false,
                    "msg" => "La saisie est en dehors du territoire de compétences",
                    "code" => self::GEOMETRY_OUTSIDE_BOUNDS,
                    "reject" => true,
                );
                $results[] = $res;
                break;
            }
            if ($feature["properties"]) {
                $properties = $feature["properties"];
                //remove specific prodige properties (should not be activated in forms)            
                if (array_key_exists("_userid_creation", $properties)) {
                    unset($properties["_userid_creation"]);
                }
                if (array_key_exists("_userid_modification", $properties)) {
                    unset($properties["_userid_modification"]);
                }
                if (array_key_exists("_edit_datemaj", $properties)) {
                    unset($properties["_edit_datemaj"]);
                }
                if (array_key_exists("_edit_datecrea", $properties)) {
                    unset($properties["_edit_datecrea"]);
                }
                if (array_key_exists("gid", $properties)) {
                    unset($properties["gid"]);
                }

                //check and specific update for attributes
                $properties = $editionController->_checkAttributes($properties);
                if (!$properties) {
                    $res = array(
                        "check" => false,
                        "msg" => "La liste des attributs ne correspond pas à la structure de données",
                        "code" => self::INVALID_ATTRIBUTES,
                        "reject" => true,
                    );
                    $results[] = $res;
                    break;
                }
            } else {
                $properties = array();
            }

            //insert data
            $editionController->editionTablename = $dataTable;
            $gid = $editionController->insertData($geometry, $srid, $properties, $geomType);

            if ($gid !== false) {
                $gids[] = $gid;
                //log success insertion
                $logFile = "api_update";
                Logs::AddLogs(
                    $logFile,
                    array($user->GetNom(), $user->GetPrenom(), $user->GetLogin(), 'CREATE', '', json_encode($feature))
                );

                $res = array(
                    "check" => true,
                    "msg" => "OK inserted",
                    "gid" => $gid,
                );
                $results[] = $res;
            } else {
                $res = array(
                    "check" => false,
                    "msg" => "Erreur lors de l'insertion du feature",
                    "code" => self::UNKNOWN_ERROR,
                    "reject" => true,
                );
                $results[] = $res;
                break;
            }
        }

        //alert Worflow
        if (!empty($gids)) {

            $userSingleton = User::GetUser();
            $request = new Request(array(
                "userEmail" => $userSingleton->getEmail(),
                "userLogin" => $userSingleton->GetLogin(),
                "userNom" => $userSingleton->GetNom(),
                "userPrenom" => $userSingleton->GetPrenom(),
                "userSignature" => $userSingleton->GetUserSignature(),
            ));

            $editionController->_alertUsersForModifiedObjects($request, $dataTable, $dataTable, $gids, $donnee);
            return new JsonResponse($results, 200);
        } else {
            return new JsonResponse($results, 200);
        }
    }

    /**
     * Ajout de données pour une ressource tabulaire
     *
     *
     * @Route("/data_tabulaire/{uuid}", methods={"POST"})
     *
     * @OA\Post(
     *     summary="Ajout de données pour une ressource",
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\RequestBody(
     *     description="données au format geoJSON (featureCollection).",
     *     required=true,
     *     @OA\JsonContent(type="object"),
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Response(
     *     response="400",
     *     description="Requête invalide"
     * )
     * @OA\Tag(name="Data")
     *
     * @return JsonResponse
     */
    public function postDataTabulaireAction($uuid = "", Request $request)
    {
        /*$strJson  = '{
            "type": "FeatureCollection",
            "name": "sql_statement",
            "crs": { "type": "name", "properties": { "name": "urn:ogc:def:crs:EPSG::2154" } },
            "features": [
            { "type": "Feature", "properties": { "gid": 103, "nom": "Kurban", "prenom": "Mustafa", "email": "k.mus69@gmail.com", "pseudo": "", "date_sb_data_1": "2018\/04\/24", "source": "appli", "photo": null, "densite": "Supérieur a 50", "code_insee": null, "commentaire": "", "code_dpt": null, "statut": "à valider", "commentaire_ref": null, "date_validation": null, "commune": null, "num_parcelle": null, "anonyme": 0, "adresse": "39 Rue de sebaco 69120 Vaulx en velin", "locked": 2, "milieu": "Champ", "_edit_datemaj": "2018\/04\/24 07:37:41.569", "x": 848741.25246347801, "y": 6522906.7465153104, "email_retour": null }, "geometry": { "type": "MultiPoint", "coordinates": [ [ 848741.25246347754728, 6522906.746515309438109 ] ] } },
            { "type": "Feature", "properties": { "gid": 203, "nom": "Majeri", "prenom": "Hamadi", "email": "hamadi-majeri@hotmail.com", "pseudo": null, "date_sb_data_1": "2018\/04\/25", "source": "appli", "photo": "", "densite": "Inférieur à 10", "code_insee": "69290", "commentaire": null, "code_dpt": "69", "statut": "signalement erroné", "commentaire_ref": null, "date_validation": null, "commune": "Saint-Priest", "num_parcelle": "CS0081", "anonyme": null, "adresse": null, "locked": 2, "milieu": "Résidentiel\/jardin", "_edit_datemaj": "2018\/05\/23 07:11:31.883", "x": 850677.67865437502, "y": 6511505.1883763801, "email_retour": null }, "geometry": { "type": "MultiPoint", "coordinates": [ [ 850677.678654375020415, 6511505.188376380130649 ] ] } },
            { "type": "Feature", "properties": { "gid": 303, "nom": "Cournac", "prenom": "Éléonore", "email": "eleonore.cournac@gmail.com", "pseudo": null, "date_sb_data_1": "2018\/04\/27", "source": "appli", "photo": "", "densite": "Supérieur a 50", "code_insee": "30077", "commentaire": null, "code_dpt": "30", "statut": "validé non détruit", "commentaire_ref": null, "date_validation": "2018\/08\/01", "commune": "Cendras", "num_parcelle": "0A2481", "anonyme": null, "adresse": null, "locked": 2, "milieu": "Autre", "_edit_datemaj": "2018\/08\/01 16:45:06.484", "x": 784483.99562949303, "y": 6340437.7541623302, "email_retour": null }, "geometry": { "type": "MultiPoint", "coordinates": [ [ 784483.995629492681473, 6340437.754162332974374 ] ] } }
            ]
                }';
        */
        $strJson = $request->getContent();
        //log post datas (date,user...)
        $logFile = "api_data";
        $user = User::GetUser();

        Logs::AddLogs($logFile, array($user->GetNom(), $user->GetPrenom(), $user->GetLogin(), $strJson));

        //1 TODO check data existence

        //2 check rights
        $serv = $this->container->get('prodige.verifyrights');
        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => "EDITION",
        );
        $getParams['OBJET_TYPE'] = "dataset";
        $req = new Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);

        if (!@$rights['EDITION'] && !@$rights['EDITION_AJOUT']) {
            $msg = "Accès interdit aux données ";

            return new JsonResponse($msg, 403);
        }

        $geoJson = json_decode($strJson, true);

        if (!$geoJson || !isset($geoJson["features"])) {
            $msg = "Contenu invalide";

            return new JsonResponse($msg, 400);
        }

        $data = $rights["layer_table"];
        $pk_couche_donnees = $rights["pk_data"];
        $donnee = $rights["couche_nom"];
        $territoire = isset($rights["EDITION_AREA"]) ? $rights["EDITION_AREA"] : array();

        $tabInfoTables = explode(".", $data);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);
        $desc = $this->getGeometryInfo($schemaName, $tableName);
        $srid = $desc["srid"];
        $geomType = $desc["type"];

        //Adaptation de la structure de table si besoin
        $editionController = new EditionController();
        $editionController->setContainer($this->container);
        $editionController->initialize((array)$territoire, $srid, $data, $pk_couche_donnees);

        $editionController->_checkLockOnTable($data, false, "_userid_creation", "integer");
        $editionController->_checkLockOnTable($data, false, "_userid_modification", "integer");
        $editionController->_checkLockOnTable($data, false, "_edit_datemaj", "timestamp");
        $editionController->_checkLockOnTable($data, false, "_edit_datecrea", "timestamp");


        //list of gid inserted
        $gids = [];
        $results = [];

        foreach ($geoJson["features"] as $key => $feature) {
            if ($feature["properties"]) {
                $properties = $feature["properties"];

                //remove specific prodige properties (should not be activated in forms)            
                unset($properties["_userid_creation"]);
                unset($properties["_userid_modification"]);
                unset($properties["_edit_datemaj"]);
                unset($properties["_edit_datecrea"]);

                unset($properties["gid"]);

                //check and specific update for attributes
                $properties = $editionController->_checkAttributes($properties);
                if (!$properties) {
                    $res = array(
                        "check" => false,
                        "msg" => "La liste des attributs ne correspond pas à la structure de données",
                        "code" => self::INVALID_ATTRIBUTES,
                        "reject" => true,
                    );
                    $results[] = $res;
                    break;
                }
                //check constraint on tables
                $tabConstraints =  $editionController->_checkUniquConstraint($feature['properties']);
                if(!empty($tabConstraints)) {
                    foreach($tabConstraints as $column => $bChecked){
                        if (!$bChecked){
                            $res = array(
                                "check" => false,
                                "msg" => "la valeur de l'attribut ".$column." n'est pas unique" ,
                                "code" => self::INVALID_ATTRIBUTES,
                                "reject" => true
                            );
                            $results[] = $res;
                            break 2;
                        }
                    }
                }


            } else {
                $properties = array();
            }

            //insert data
            $gid = $editionController->insertDataTabulaire($properties);

            if ($gid !== false) {
                $gids[] = $gid;
                //log success insertion
                $logFile = "api_update";
                Logs::AddLogs(
                    $logFile,
                    array($user->GetNom(), $user->GetPrenom(), $user->GetLogin(), 'CREATE', '', json_encode($feature))
                );

                $res = array(
                    "check" => true,
                    "msg" => "OK inserted",
                );
                $results[] = $res;
            } else {
                $res = array(
                    "gid" => $gid,
                    "check" => false,
                    "msg" => "Erreur lors de l'insertion du feature",
                    "code" => self::UNKNOWN_ERROR,
                    "reject" => true,
                );
                $results[] = $res;
                break;
            }
        }

        //alert Worflow
        if (!empty($gids)) {

            $userSingleton = User::GetUser();
            $request = new Request(array(
                "userEmail" => $userSingleton->getEmail(),
                "userLogin" => $userSingleton->GetLogin(),
                "userNom" => $userSingleton->GetNom(),
                "userPrenom" => $userSingleton->GetPrenom(),
                "userSignature" => $userSingleton->GetUserSignature(),
            ));

            $editionController->_alertUsersForModifiedObjects($request, $data, $data, $gids, $donnee);

            return new JsonResponse($results, 200);
        } else {
            return new JsonResponse($results, 200);
        }
    }


    
    /**
     * Modification de données brouillon pour une ressource
     *
     * @Route("/data/draft/{uuid}/{column_id}", methods={"PATCH"})
     *
     * @OA\Patch (
     *     summary="Modification de données brouillon pour une ressource.",
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\Parameter(
     *     name="column_id",
     *     in="path",
     *     description="colonne servant d'identifiant",
     *     required=false,
     *     @OA\Schema(type="string"),
     * )
     * @OA\RequestBody(
     *     description="données au format geoJSON (featureCollection).",
     *     required=true,
     *     @OA\JsonContent(type="object"),
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Response(
     *     response="400",
     *     description="Requête invalide"
     * )
     * @OA\Tag(name="Data")
     *
     *
     *
     * @return JsonResponse
     */
    public function patchDataDraftAction($uuid = "", $column_id = null, Request $request)
    {
        $conn_prodige = $this->getConnection('prodige');
        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => "EDITION",
            "OBJET_TYPE" => "dataset"
        );

        $serv = $this->container->get('prodige.verifyrights');
        $req = new Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);

        $tableName = $rights["layer_table"];
        $tabInfoTables = explode(".", $tableName);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);
        $tableName = $this->getDraftTable($schemaName, $tableName, true);

        if (is_null($tableName)) {
            $msg = "Pas de brouillon pour cette ressource";
            return new JsonResponse($msg, 404);
        }
        $gidsArray = [];

        $strJson = $request->getContent();

        $geoJson = json_decode(($strJson), true);

        if (!$geoJson || !isset($geoJson["features"])) {
            $msg = "Contenu invalide";
            return new JsonResponse($msg, 400);
        }
        
        foreach ($geoJson["features"] as $key => $feature) {
            $properties = $feature["properties"];
            $gidsArray[] =  $properties[$column_id ? $column_id : "gid"];
        }
        
        $res = $conn_prodige->fetchAssoc(
            "select count(gid) from " . pg_escape_string($tableName) . " where gid in (:gids) and _userid_modification=:_userid_modification ",
            array("gids" => $gidsArray,
                "_userid_modification" => User::GetUserId()),
            array("gids" => $conn_prodige::PARAM_INT_ARRAY)
        );

        //data is not user data
         if ($res['count']!==count($gidsArray)) {
            $msg = "Accès interdit aux données ";
            return new JsonResponse($msg, 403);
        }

        return $this->patchDataIntoTable($request, $schemaName, $tableName, $rights, false);
    }

    
    /**
     * Modification de données pour une ressource
     *
     *
     * @Route("/data/{uuid}/{column_id}", methods={"PATCH"})
     *
     * @OA\Patch (
     *     summary="Modification de données pour une ressource.",
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\Parameter(
     *     name="column_id",
     *     in="path",
     *     description="colonne servant d'identifiant",
     *     required=false,
     *     @OA\Schema(type="string"),
     * )
     * @OA\RequestBody(
     *     description="données au format geoJSON (featureCollection).",
     *     required=true,
     *     @OA\JsonContent(type="object"),
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Response(
     *     response="400",
     *     description="Requête invalide"
     * )
     * @OA\Tag(name="Data")
     *
     * @return JsonResponse
     */
    public function patchDataAction($uuid = "", $column_id = null, Request $request)
    {
        
      
        //1 TODO check data existence

        //2 check rights
        $serv = $this->container->get('prodige.verifyrights');
        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => "EDITION",
        );
        $getParams['OBJET_TYPE'] = "dataset";
        $req = new \Symfony\Component\HttpFoundation\Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);
        
        $tableName = $rights["layer_table"];
        $tabInfoTables = explode(".", $tableName);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);
        
        return $this->patchDataIntoTable($request, $schemaName, $tableName, $rights, true);
    
    
    }

    private function patchDataIntoTable($request, $schemaName, $tableName, $rights, $bCheckData){

        $strJson = $request->getContent();
        //log post datas (date,user...)
        $logFile = "api_data";
        $user = User::GetUser();
        Logs::AddLogs($logFile, array($user->GetNom(), $user->GetPrenom(), $user->GetLogin(), $strJson));

        //access to PATCH operation for EDITION (global rights or EDITION_MODIFICATION). EDITION_AJOUT exclued
        if (!@$rights['EDITION'] && !@$rights['EDITION_MODIFICATION']) {
            $msg = "Accès interdit aux données ";

            return new JsonResponse($msg, 403);
        }

        $geoJson = json_decode(($strJson), true);

        if (!$geoJson || !isset($geoJson["features"])) {
            $msg = "Contenu invalide";

            return new JsonResponse($msg, 400);
        }

        $dataTable = $schemaName . '.' . $tableName;
        $pk_couche_donnees = $rights["pk_data"];
        $donnee = $rights["couche_nom"];
        $territoire = isset($rights["EDITION_AREA"]) ? $rights["EDITION_AREA"] : array();

        $desc = $this->getGeometryInfo($schemaName, $tableName);
        $srid = $desc["srid"];
        $geomType = $desc["type"];

        //Adaptation de la structure de table si besoin
        $editionController = new EditionController();
        $editionController->setContainer($this->container);
        $editionController->initialize((array)$territoire, $srid, $tableName, $pk_couche_donnees);

        $editionController->_checkLockOnTable($dataTable, false, "_userid_creation", "integer");
        $editionController->_checkLockOnTable($dataTable, false, "_userid_modification", "integer");
        $editionController->_checkLockOnTable($dataTable, false, "_edit_datemaj", "timestamp");
        $editionController->_checkLockOnTable($dataTable, false, "_edit_datecrea", "timestamp");

        //list of gid updated
        $gids = [];
        $results = [];

        foreach ($geoJson["features"] as $key => $feature) {
            $geometry = $feature["geometry"];
            //control validity
            if ($bCheckData && !$editionController->_checkConstraint(json_encode($geometry), "", "isvalid", null, "Geojson")) {
                // test de validité de la géométrie
                $res = array(
                    "check" => false,
                    "msg" => "La géométrie n'est pas valide",
                    "code" => self::INVALID_GEOMETRY,
                    "reject" => true,
                );
                $results[] = $res;
                break;
            }
            //control perimetre

            //if rights["EDITION_AREA"] is not set, no territorial restriction
            if (isset($rights["EDITION_AREA"]) && !$editionController->_checkConstraint(
                    json_encode($geometry),
                    "",
                    "territoire",
                    null,
                    "Geojson"
                )) {
                $res = array(
                    "check" => false,
                    "msg" => "La saisie est en dehors du territoire de compétences",
                    "code" => self::GEOMETRY_OUTSIDE_BOUNDS,
                    "reject" => true,
                );
                $results[] = $res;
                break;
            }
            $properties = $feature["properties"];
            if ($feature["properties"]) {
                //remove specific prodige properties (should not be activated in forms)            
                if (array_key_exists("_userid_creation", $properties)) {
                    unset($properties["_userid_creation"]);
                }
                if (array_key_exists("_userid_modification", $properties)) {
                    unset($properties["_userid_modification"]);
                }
                if (array_key_exists("_edit_datemaj", $properties)) {
                    unset($properties["_edit_datemaj"]);
                }
                if (array_key_exists("_edit_datecrea", $properties)) {
                    unset($properties["_edit_datecrea"]);
                }

                //check and specific update for attributes
                $properties = $editionController->_checkAttributes($properties);
                if (!$properties) {
                    $res = array(
                        "check" => false,
                        "msg" => "La liste des attributs ne correspond pas à la structure de données",
                        "code" => self::INVALID_ATTRIBUTES,
                        "reject" => true,
                    );
                    $results[] = $res;
                    break;
                }
            }
            if (!isset($properties["gid"]) && !$column_id) {
                $res = array(
                    "check" => false,
                    "msg" => "gid manquant",
                    "code" => self::MISSING_GID,
                    "reject" => true,
                );
                $results[] = $res;
                break;
            }
            else if(!isset($properties[$column_id]) && $column_id) {
                $res = array(
                    "check" => false,
                    "msg" => "$column_id manquant",
                    "code" => self::MISSING_GID,
                    "reject" => true
                );
                $results[] = $res;
                break;
            }

            //for log, get dataTable
            $oldJsonValue = $column_id ? $this->_getdataByColumnName($dataTable, $properties[$column_id], $column_id ) : $this->_getData($dataTable, $properties["gid"]);

            //if rights["EDITION_ATTRIBUTE_FILTER"] is not set, no attribute restriction
            if ($bCheckData && $rights["EDITION_ATTRIBUTE_FILTER"] &&
                //check new value
                (!$editionController->_checkData(
                    $feature['properties'],
                    $rights['EDITION_ATTRIBUTE_FILTER']['column'],
                    $rights['EDITION_ATTRIBUTE_FILTER']['value']

                ) ||
                //also check old value, since you must have rights to modifiy it
                !$editionController->_checkData(
                    json_decode($oldJsonValue, true),
                    $rights['EDITION_ATTRIBUTE_FILTER']['column'],
                    $rights['EDITION_ATTRIBUTE_FILTER']['value']
                )
                )
            ) {
                $res = array(
                    "check" => false,
                    "msg" => "La saisie n'est autorisée qu'avec la valeur ". $rights['EDITION_ATTRIBUTE_FILTER']['value']. " pour le champ ".$rights['EDITION_ATTRIBUTE_FILTER']['column'],
                    "code" => self::INVALID_DATA_RIGHTS,
                    "reject" => true
                );
                $results[] = $res;
                break;
            }
            //check constraint on tables
            if($bCheckData){
                $tabConstraints =  $editionController->_checkUniquConstraint($feature['properties']);
                if(!empty($tabConstraints)) {
                    foreach($tabConstraints as $column => $bChecked){
                        //uniq constraint applies when new value is different from previous in data
                        if (!$bChecked && json_decode($oldJsonValue, true)[$column] !==$feature['properties'][$column]){
                            $res = array(
                                "check" => false,
                                "msg" => "la valeur de l'attribut ".$column." n'est pas unique" ,
                                "code" => self::INVALID_ATTRIBUTES,
                                "reject" => true
                            );
                            $results[] = $res;
                            break 2;
                        }
                    }
                }
            }


            //update dataTable
            if($column_id){
                $sucess = $editionController->updateData($geometry, $srid, $properties, $geomType);
            }else{
                $sucess = $editionController->updateData($geometry, $srid, $properties, $geomType, $column_id);
            }
            

            if ($sucess !== false) {
                //log success update
                $logFile = "api_update";
                Logs::AddLogs(
                    $logFile,
                    array(
                        $user->GetNom(),
                        $user->GetPrenom(),
                        $user->GetLogin(),
                        'UPDATE',
                        $oldJsonValue,
                        json_encode($feature),
                    )
                );

                $gids[] = $properties["gid"];
                $res = array(
                    "check" => true,
                    "msg" => "OK updated",
                );
                $results[] = $res;
            } else {
                $res = array(
                    "check" => false,
                    "msg" => "Erreur lors de l'insertion du feature",
                    "code" => self::UNKNOWN_ERROR,
                    "reject" => true,
                );
                $results[] = $res;
                break;
            }
        }

        //alert Worflow
        if (!empty($gids)) {
            $userSingleton = User::GetUser();
            $request = new Request(array(
                "userEmail" => $userSingleton->getEmail(),
                "userLogin" => $userSingleton->GetLogin(),
                "userNom" => $userSingleton->GetNom(),
                "userPrenom" => $userSingleton->GetPrenom(),
                "userSignature" => $userSingleton->GetUserSignature(),
            ));

            return new JsonResponse($results, 200);
        } else {
            return new JsonResponse($results, 200);
        }
    }



    /**
     * Modification de données pour une ressource
     *
     *
     * @Route("/data_tabulaire/{uuid}", methods={"PATCH"})
     *
     * @OA\Patch (
     *     summary="Modification de données pour une ressource.",
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\RequestBody(
     *     description="données au format geoJSON (featureCollection). Le champ **gid** est la clé pour les opérations de mise à jour.",
     *     required=true,
     *     @OA\JsonContent(type="object"),
     * ),
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Response(
     *     response="400",
     *     description="Requête invalide"
     * )
     * @OA\Tag(name="Data")
     *
     * @return JsonResponse
     */
    public function patchDataTabulaireAction($uuid = "", Request $request)
    {

        $strJson = $request->getContent();
        //log post datas (date,user...)
        $logFile = "api_data";
        $user = User::GetUser();
        Logs::AddLogs($logFile, array($user->GetNom(), $user->GetPrenom(), $user->GetLogin(), $strJson));

        //1 TODO check data existence

        //2 check rights
        $serv = $this->container->get('prodige.verifyrights');
        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => "EDITION",
        );
        $getParams['OBJET_TYPE'] = "dataset";
        $req = new \Symfony\Component\HttpFoundation\Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);


        //access to PATCH operation for EDITION (global rights or EDITION_MODIFICATION). EDITION_AJOUT exclued
        if (!@$rights['EDITION'] && !@$rights['EDITION_MODIFICATION']) {
            $msg = "Accès interdit aux données ";

            return new JsonResponse($msg, 403);
        }


        $geoJson = json_decode(($strJson), true);

        if (!$geoJson || !isset($geoJson["features"])) {
            $msg = "Contenu invalide";

            return new JsonResponse($msg, 400);
        }


        $data = $rights["layer_table"];
        $pk_couche_donnees = $rights["pk_data"];
        $donnee = $rights["couche_nom"];
        $territoire = isset($rights["EDITION_AREA"]) ? $rights["EDITION_AREA"] : array();

        $tabInfoTables = explode(".", $data);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);
        $desc = $this->getGeometryInfo($schemaName, $tableName);
        $srid = $desc["srid"];
        $geomType = $desc["type"];

        //Adaptation de la structure de table si besoin
        $editionController = new EditionController();
        $editionController->setContainer($this->container);
        $editionController->initialize((array)$territoire, $srid, $data, $pk_couche_donnees);

        $editionController->_checkLockOnTable($data, false, "_userid_creation", "integer");
        $editionController->_checkLockOnTable($data, false, "_userid_modification", "integer");
        $editionController->_checkLockOnTable($data, false, "_edit_datemaj", "timestamp");
        $editionController->_checkLockOnTable($data, false, "_edit_datecrea", "timestamp");


        //list of gid updated
        $gids = [];
        $results = [];

        foreach ($geoJson["features"] as $key => $feature) {
            $properties = $feature["properties"];
            if ($feature["properties"]) {
                //remove specific prodige properties (should not be activated in forms)            
                if (array_key_exists("_userid_creation", $properties)) {
                    unset($properties["_userid_creation"]);
                }
                if (array_key_exists("_userid_modification", $properties)) {
                    unset($properties["_userid_modification"]);
                }
                if (array_key_exists("_edit_datemaj", $properties)) {
                    unset($properties["_edit_datemaj"]);
                }
                if (array_key_exists("_edit_datecrea", $properties)) {
                    unset($properties["_edit_datecrea"]);
                }

                //check and specific update for attributes
                $properties = $editionController->_checkAttributes($properties);
                if (!$properties) {
                    $res = array(
                        "check" => false,
                        "msg" => "La liste des attributs ne correspond pas à la structure de données",
                        "code" => self::INVALID_ATTRIBUTES,
                        "reject" => true,
                    );
                    $results[] = $res;
                    break;
                }
            }
            if (!isset($properties["gid"])) {
                $res = array(
                    "check" => false,
                    "msg" => "gid manquant",
                    "code" => self::MISSING_GID,
                    "reject" => true,
                );
                $results[] = $res;
                break;
            }

            //for log, get data 
            $oldJsonValue = $this->_getData($data, $properties["gid"]);
            //update data
            $sucess = $editionController->updateDataTabulaire($properties);

            if ($sucess !== false) {
                //log success update
                $logFile = "api_update";
                Logs::AddLogs(
                    $logFile,
                    array(
                        $user->GetNom(),
                        $user->GetPrenom(),
                        $user->GetLogin(),
                        'UPDATE',
                        $oldJsonValue,
                        json_encode($feature),
                    )
                );

                $gids[] = $properties["gid"];
                $res = array(
                    "check" => true,
                    "msg" => "OK updated",
                );
                $results[] = $res;
            } else {
                $res = array(
                    "check" => false,
                    "msg" => "Erreur lors de l'insertion du feature",
                    "code" => self::UNKNOWN_ERROR,
                    "reject" => true,
                );
                $results[] = $res;
                break;
            }
        }

        //alert Worflow
        if (!empty($gids)) {
            $userSingleton = User::GetUser();
            $request = new Request(array(
                "userEmail" => $userSingleton->getEmail(),
                "userLogin" => $userSingleton->GetLogin(),
                "userNom" => $userSingleton->GetNom(),
                "userPrenom" => $userSingleton->GetPrenom(),
                "userSignature" => $userSingleton->GetUserSignature(),
            ));

            return new JsonResponse($results, 200);
        } else {
            return new JsonResponse($results, 200);
        }
    }

    /**
     * Suppression de données pour une ressource
     *
     *
     * @Route("/data/{uuid}/{gids}", methods={"DELETE"})
     *
     * @OA\Delete(
     *     summary="Suppression de données pour une ressource.",
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\Parameter(
     *     name="gids",
     *     in="path",
     *     description="identifiant des objets à supprimer séparés par une virgule",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Response(
     *     response="400",
     *     description="Requête invalide"
     * )
     * @OA\Tag(name="Data")
     *
     * @return JsonResponse
     */
    public function deleteDataAction($uuid = "", $gids = "")
    {
        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => "EDITION",
            "OBJET_TYPE" => "dataset"
        );

        $serv = $this->container->get('prodige.verifyrights');
        $req = new Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);

        $tableName = $rights["layer_table"];
        $tabInfoTables = explode(".", $tableName);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);

        //access to DELETE operation for EDITION (global rights). EDITION_AJOUT adn EDITION_MODIFICATION exclued
        if (!@$rights['EDITION']) {
            $msg = "Accès interdit aux données ";
            return new JsonResponse($msg, 403);
        }

        return $this->deleteDataOfTable($gids, $schemaName, $tableName, $rights);
    }

    /**
     * Suppression d'un brouillon de données pour une ressource
     *
     * @Route("/data/draft/{uuid}/{gids}", methods={"DELETE"})
     *
     * @OA\Delete(
     *     summary="Suppression d'un brouillon de données pour une ressource.",
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\Parameter(
     *     name="gids",
     *     in="path",
     *     description="identifiant des objets à supprimer séparés par une virgule",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Response(
     *     response="400",
     *     description="Requête invalide"
     * )
     * @OA\Tag(name="Data")
     * 
     *
     *
     *
     * @return JsonResponse
     */
    public function deleteDataDraftAction($uuid = "", $gids = "")
    {
        $conn_prodige = $this->getConnection('prodige');

        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => "EDITION",
            "OBJET_TYPE" => "dataset"
        );
        $gidsArray = explode(",", $gids);

        $serv = $this->container->get('prodige.verifyrights');
        $req = new Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);

        $tableName = $rights["layer_table"];
        $tabInfoTables = explode(".", $tableName);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);

        $tableName = $this->getDraftTable($schemaName, $tableName, false);
        if (is_null($tableName)) {
            $msg = "Pas de brouillon pour cette ressource";
            return new JsonResponse($msg, 404);
        }
        
        $res = $conn_prodige->fetchAssoc(
            "select count(gid) from " . pg_escape_string($tableName) . " where gid in (:gids) and _userid_modification=:_userid_modification ",
            array("gids" => $gidsArray,
                "_userid_modification" => User::GetUserId()),
            array("gids" => $conn_prodige::PARAM_INT_ARRAY)
        );

        //data is not user data
         if ($res['count']!==count($gidsArray)) {
            $msg = "Accès interdit aux données ";

            return new JsonResponse($msg, 403);
        }

        return $this->deleteDataOfTable($gids, $schemaName, $tableName, $rights, false);
    }

    private function deleteDataOfTable($gids, $schemaName, $tableName, $rights, $bCheckData=true)
    {
        //log post datas (date,user...)
        $logFile = "api_data";
        $user = User::GetUser();
        //Logs::AddLogs($logFile, array($user->GetNom(), $user->GetPrenom(), $user->GetLogin(), $strJson));

        

        $gids = explode(",", $gids);

        if (!$gids || !is_array($gids) || empty($gids)) {
            $msg = "Contenu invalide";

            return new JsonResponse($msg, 400);
        }

        if (count($gids) > 100) {
            $msg = "Nombre d'objets à supprimer trop important";

            return new JsonResponse($msg, 400);
        }

        $pk_couche_donnees = $rights["pk_data"];
        $donnee = $rights["couche_nom"];
        $territoire = isset($rights["EDITION_AREA"]) ? $rights["EDITION_AREA"] : array();

        $desc = $this->getGeometryInfo($schemaName, $tableName);
        $srid = $desc["srid"];

        //Adaptation de la structure de table si besoin
        $dataTable = $schemaName . '.' . $tableName;
        $editionController = new EditionController();
        $editionController->setContainer($this->container);
        $editionController->initialize((array)$territoire, $srid, $dataTable, $pk_couche_donnees);

        $editionController->_checkLockOnTable($dataTable, false, "_userid_creation", "integer");
        $editionController->_checkLockOnTable($dataTable, false, "_userid_modification", "integer");
        $editionController->_checkLockOnTable($dataTable, false, "_edit_datemaj", "timestamp");
        $editionController->_checkLockOnTable($dataTable, false, "_edit_datecrea", "timestamp");


        $results = [];
        //delete data

        //control perimetre
        $conn_prodige = $this->getConnection('prodige');

        $res = $conn_prodige->fetchAllAssociative(
            "select *, st_asgeojson(the_geom) as geom from ".pg_escape_string($dataTable)." where gid in (:gids)",
            array("gids" => $gids),
            array("gids" => $conn_prodige::PARAM_INT_ARRAY)
        );

        $modifiedGids = array();
        foreach ($res as $key => $properties) {

            if ($bCheckData && $rights["EDITION_ATTRIBUTE_FILTER"]&& !$editionController->_checkData(
                    $properties,
                    $rights['EDITION_ATTRIBUTE_FILTER']['column'],
                    $rights['EDITION_ATTRIBUTE_FILTER']['value']

                )) {
                $res = array(
                    "check" => false,
                    "msg" => "La saisie n'est autorisée qu'avec la valeur ". $rights['EDITION_ATTRIBUTE_FILTER']['value']. " pour le champ ".$rights['EDITION_ATTRIBUTE_FILTER']['column'],
                    "code" => self::INVALID_DATA_RIGHTS,
                    "reject" => true
                );
                $results[] = $res;
                break;
            }
            //if rights["EDITION_AREA"] is not set, no territorial restriction
            if ($bCheckData && isset($rights["EDITION_AREA"]) && !$editionController->_checkConstraint(
                    $properties["geom"],
                    "",
                    "territoire",
                    null,
                    "Geojson"
                )) {
                $res = array(
                    "check" => false,
                    "msg" => "La géométrie est hors du territoire de compétences",
                    "code" => self::GEOMETRY_OUTSIDE_BOUNDS,
                    "reject" => true,
                );
                $results[] = $res;
                break;
            }
            //for log, get data
            $oldJsonValue = $this->_getData($dataTable, $properties["gid"]);
            $sucess = $editionController->deleteData($properties["gid"]);

            if ($sucess !== false) {

                //log success update
                $logFile = "api_update";

                Logs::AddLogs(
                    $logFile,
                    array($user->GetNom(), $user->GetPrenom(), $user->GetLogin(), 'DELETE', $oldJsonValue, '')
                );

                $res = array(
                    "check" => true,
                    "msg" => "OK deleted",
                );
                $results[] = $res;
                $deletedGids[] = $properties["gid"];
            } else {
                $res = array(
                    "check" => false,
                    "msg" => "Erreur lors de la suppression du feature",
                    "code" => self::UNKNOWN_ERROR,
                    "reject" => true,
                );
                $results[] = $res;
            }
        }

        //alert Worflow
        if (!empty($deletedGids)) {
            $userSingleton = User::GetUser();
            $request = new Request(array(
                "userEmail" => $userSingleton->getEmail(),
                "userLogin" => $userSingleton->GetLogin(),
                "userNom" => $userSingleton->GetNom(),
                "userPrenom" => $userSingleton->GetPrenom(),
                "userSignature" => $userSingleton->GetUserSignature(),
            ));

            return new JsonResponse($results, 200);
        } else {
            return new JsonResponse($results, 200);
        }
    }

    /**
     * Suppression de données pour une ressource
     *
     *
     * @Route("/data_tabulaire/{uuid}", methods={"DELETE"})
     *
     * @OA\Delete(
     *     summary="Suppression de données pour une ressource.",
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\RequestBody(
     *     description="données au format geoJSON (featureCollection).",
     *     required=true,
     *     @OA\JsonContent(type="object"),
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Response(
     *     response="400",
     *     description="Requête invalide"
     * )
     * @OA\Tag(name="Data")
     *
     * @return JsonResponse
     */
    public function deleteDataTabulaireAction(Request $request, $uuid, $gids)
    {

        //log post datas (date,user...)
        $logFile = "api_data";
        $user = User::GetUser();
        //Logs::AddLogs($logFile, array($user->GetNom(), $user->GetPrenom(), $user->GetLogin(), $strJson));

        //1 TODO check data existence

        //2 check rights
        $serv = $this->container->get('prodige.verifyrights');
        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => "EDITION",
        );
        $getParams['OBJET_TYPE'] = "dataset";
        $req = new \Symfony\Component\HttpFoundation\Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);

        //access to DELETE operation for EDITION (global rights). EDITION_AJOUT adn EDITION_MODIFICATION exclued
        if (!@$rights['EDITION']) {
            $msg = "Accès interdit aux données ";

            return new JsonResponse($msg, 403);
        }

        $gids = explode(",", $gids);

        if (!$gids || !is_array($gids) || empty($gids)) {
            $msg = "Contenu invalide";

            return new JsonResponse($msg, 400);
        }

        if (count($gids) > 100) {
            $msg = "Nombre d'objets à supprimer trop important";

            return new JsonResponse($msg, 400);
        }

        $data = $rights["layer_table"];
        $pk_couche_donnees = $rights["pk_data"];
        $donnee = $rights["couche_nom"];
        $territoire = isset($rights["EDITION_AREA"]) ? $rights["EDITION_AREA"] : array();

        $tabInfoTables = explode(".", $data);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);
        $desc = $this->getGeometryInfo($schemaName, $tableName);
        $srid = $desc["srid"];

        //Adaptation de la structure de table si besoin
        $editionController = new EditionController();
        $editionController->setContainer($this->container);
        $editionController->initialize((array)$territoire, $srid, $data, $pk_couche_donnees);

        $editionController->_checkLockOnTable($data, false, "_userid_creation", "integer");
        $editionController->_checkLockOnTable($data, false, "_userid_modification", "integer");
        $editionController->_checkLockOnTable($data, false, "_edit_datemaj", "timestamp");
        $editionController->_checkLockOnTable($data, false, "_edit_datecrea", "timestamp");


        $results = [];
        //delete data

        //control perimetre
        $conn_prodige = $this->getConnection('prodige');

        $res = $conn_prodige->fetchAllAssociative(
            "select gid from ".pg_escape_string($data)." where gid in (:gids)",
            array("gids" => $gids),
            array("gids" => $conn_prodige::PARAM_INT_ARRAY)
        );

        $modifiedGids = array();
        foreach ($res as $key => $properties) {
            //for log, get data
            $oldJsonValue = $this->_getData($data, $properties["gid"]);
            $sucess = $editionController->deleteData($properties["gid"]);

            if ($sucess !== false) {

                //log success update
                $logFile = "api_update";

                Logs::AddLogs(
                    $logFile,
                    array($user->GetNom(), $user->GetPrenom(), $user->GetLogin(), 'DELETE', $oldJsonValue, '')
                );

                $res = array(
                    "check" => true,
                    "msg" => "OK deleted",
                );
                $results[] = $res;
                $deletedGids[] = $properties["gid"];
            } else {
                $res = array(
                    "check" => false,
                    "msg" => "Erreur lors de la suppression du feature",
                    "code" => self::UNKNOWN_ERROR,
                    "reject" => true,
                );
                $results[] = $res;
            }
        }

        //alert Worflow
        if (!empty($deletedGids)) {
            $userSingleton = User::GetUser();
            $request = new Request(array(
                "userEmail" => $userSingleton->getEmail(),
                "userLogin" => $userSingleton->GetLogin(),
                "userNom" => $userSingleton->GetNom(),
                "userPrenom" => $userSingleton->GetPrenom(),
                "userSignature" => $userSingleton->GetUserSignature(),
            ));


            return new JsonResponse($results, 200);
        } else {
            return new JsonResponse($results, 200);
        }
    }

    /**
     * liste des énumérations associées à une ressource
     *
     *
     *
     * @Route("/enum/{uuid}", methods={"GET"})
     *
     * @OA\GET(
     *     summary="liste des énumérations associées à une ressource",
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource inexistante"
     * )
     * @OA\Response(
     *     response="400",
     *     description="Requête invalide"
     * )
     * @OA\Tag(name="Data")
     *
     * @return JsonResponse
     */
    public function getEnumAction(Request $request, $uuid)
    {

        $PRODIGE = $this->getProdigeConnection('carmen');
        //1 TODO check data existence
        //2 check rights
        $serv = $this->container->get('prodige.verifyrights');

        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => "NAVIGATION|EDITION",
        );
        $getParams['OBJET_TYPE'] = "dataset";

        $req = new \Symfony\Component\HttpFoundation\Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);

        $ipAuthorize = $this->isIpAuthorize($request, $uuid);
        if($ipAuthorize){
            $rights['NAVIGATION'] = true;
        }

        if (!@$rights['NAVIGATION'] && !@$rights['EDITION'] && !@$rights['EDITION_AJOUT'] && !@$rights['EDITION_MODIFICATION']) {
            $msg = "Accès interdit aux données ";

            return new JsonResponse($msg, 403);
        }

        $data = $rights["layer_table"];

        $query = "select  t.typname as enum_name, string_agg(e.enumlabel, '||' order by enumsortorder ) as enum_value 
                    from pg_type t 
                    join pg_enum e on t.oid = e.enumtypid join pg_catalog.pg_namespace n ON n.oid = t.typnamespace 
                    JOIN pg_attribute a ON a.atttypid = t.oid
                    join pg_class c ON c.oid = a.attrelid 
                    WHERE c.relname = :table
                
                    group by  enum_name 
                    ".
            //hack for array enums, since relation is not done in postgres system tables
            " union
                    select t.typname as enum_name, string_agg(e.enumlabel, '||' order by enumsortorder) as enum_value 
                    from pg_type t
                    left  join pg_enum e on t.oid = e.enumtypid 
                    where typname in (
                    select ltrim(t.typname, '_') as enum_name
                                    from pg_attribute a 
                    inner JOIN pg_class c  
                    ON c.oid = a.attrelid 
                    inner join pg_type t
                    ON a.atttypid = t.oid
                    left  join pg_enum e on t.oid = e.enumtypid 
                    WHERE c.relname = :table and typinput::text= 'array_in')
                    group by enum_name 
                    ";

        $resultArray = array();
        $stmt = $PRODIGE->executeQuery($query, array("table" => $data));
        foreach ($stmt as $row) {
            $resultArray[$row['enum_name']] = explode("||", $row['enum_value']);
        }

        $query = "SELECT  c.column_name".
                 " FROM information_schema.columns c".
                 " WHERE c.table_name = :table and domain_name='image_gallery'";
                 
        $stmt = $PRODIGE->executeQuery($query, array("table" => $data));
        foreach ($stmt as $row) {
            $query = "SELECT image_texte, image_url, image_select ".
                " FROM  public.". $data."_images order by id";

            $list = $PRODIGE->fetchAllAssociative($query);

            $resultArray[$row['column_name'].'_images']  = $list;
        }
        return new JsonResponse($resultArray);
    }

     /**
     * teste l'unicité d'une valeur pour un champ
     *
     * @Route("/unique/{uuid}/{field}/{value}", methods={"GET"})
     *
     * @OA\GET(
     *     summary="teste l'unicité d'une valeur pour un champ",
     * )
     * @OA\Parameter(
     *     name="uuid",
     *     in="path",
     *     description="identifiant de la ressource",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\Parameter(
     *     name="field",
     *     in="path",
     *     description="nom du champ",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\Parameter(
     *     name="value",
     *     in="path",
     *     description="valeur testée",
     *     required=true,
     *     @OA\Schema(type="string"),
     * )
     * @OA\Parameter(
     *     name="gid",
     *     in="query",
     *     description="identifiant de l'object à exclure du test",
     *     required=false,
     *     @OA\Schema(type="string"),
     * )   
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Response(
     *     response="404",
     *     description="Ressource ou champ inexistant"
     * )
     * @OA\Response(
     *     response="400",
     *     description="Requête invalide"
     * )
     * @OA\Tag(name="Data")
     *
     * @return JsonResponse
     */
    public function getUniqueAction(Request $request, $uuid, $field, $value)
    {
        $PRODIGE = $this->getProdigeConnection('public');
        $getParams = array(
            "uuid" => $uuid,
            "TRAITEMENTS" => "NAVIGATION",
            "OBJET_TYPE" => "dataset"
        );

        $gid = $request->get("gid", null);

        // check rights
        $serv = $this->container->get('prodige.verifyrights');
        $req = new Request($getParams);
        $rights = $serv->verifyRightsAction($req, true);
        if (!@$rights['NAVIGATION']) {
            $msg = "Accès interdit aux données ";
            return new JsonResponse($msg, 403);
        }

        $tableName = $rights["layer_table"];
        $tabInfoTables = explode(".", $tableName);
        $schemaName = (isset($tabInfoTables[1]) ? $tabInfoTables[0] : "public");
        $tableName = (isset($tabInfoTables[1]) ? $tabInfoTables[1] : $tabInfoTables[0]);

        $columnTypes = $this->_getAllDataType($tableName);
        foreach ($columnTypes as $indice => $infos) {
             if ($infos["column_name"] == $field){
                $query = " select gid from ".$tableName." where ".pg_escape_string($field)."=:value ";
                $params = array("value" => $value);
        
                if($gid){
                    $query .= "and gid <> :gid";
                    $params["gid"] = intval($gid);
                }
        
                $result = $PRODIGE->fetchAll($query, $params);
                $return = new \stdClass(); 
                if (empty($result)) {
                    $return->unique = true;
                }else{
                    $return->unique = false;
                }
                return new JsonResponse($return);
             }
        }

        return new JsonResponse("unknow field", 404);

    }

    /**
     * Checks if column is accessible / correct and adjusts value
     * @param string $column the column to verify
     * @param array $value the value/values to adapt
     * @return array [entity_column_name, [adapted_value(s)]]
     */
    private function checkColumn($column, $values)
    {
        //print(json_encode($this->authorized_columns));
        $this->getLogger()->info("Auth Columns : ". json_encode($this->authorized_columns));
        if(!in_array($column,$this->authorized_columns)){
            throw new \Exception(" Column '$column' not supported",422);
        }
        $new_values = $values;

        //$new_value = $value;
        //TODO check type
        //$new_column = $this->database_entity_mapping[$column];
        //print(json_encode($this->column_types));
        //die();
        switch($this->column_types[$column])
        {
            case "character varying":
                $new_values = [];
                foreach($values as $value){
                    $new_values[] = "'".$value."'";
                }
                //$new_value = "'".$value."'";//"'::dae_etat_valid"
                break;
            case "date":
                $new_values = [];
                foreach($values as $value){
                    $new_values[] = "'".$value."'";
                }
                //$new_value = "'".$value."'";//"'::dae_etat_valid"
                break;
            case "timestamp without time zone":
                $new_values = [];
                foreach($values as $value){
                    $new_values[] = "'".$value."'";
                }
                break;    
            case "USER-DEFINED":
                $new_values = [];
                foreach($values as $value){
                    $new_values[] = "'".$value."'";
                }
            case "USER_DEFINED":
                $new_values = [];
                foreach($values as $value){
                    $new_values[] = "'".$value."'";
                }
            default:
                break;
        }

        return [$column, $new_values];
    }

    /**
     * Parse une expression de filtrage
     * @param string $operator l'opérateur de l'expression
     * @param string $params la chaîne de caractères de paramètres
     * @return Expr une expression de filtrage
     */
    private function parseOne($operator,$params)
    {
        $operation = null;
        $params_array = explode(",",substr($params,1,strlen($params)-2));

        $checked_column = $this->checkColumn($params_array[0],array_slice($params_array,1));
        $column = $checked_column[0];
        $parameters = $checked_column[1];
        $parameter = $parameters[0];

        //print("\nColumn : ".$column. " type : ". $this->column_types[$column]."\n");

        if($this->column_types[$column] == "ARRAY"){
            switch($operator){
                case "eq" :
                    $operation = " ARRAY[".implode(",",
                        array_map(function($item)
                        {
                            return "'".$item."'";
                        },array_slice($params_array,1))
                    ). "] = a.".$column."::text[]";
                    break;
                case "neq" :
                    $operation = " NOT (ARRAY[".implode(",",
                        array_map(function($item)
                        {
                            return "'".$item."'";
                        },array_slice($params_array,1))
                    ). "] = a.".$column."::text[])";
                    break;
                case "in" :
                    $operation = " ARRAY[".implode(",",
                        array_map(function($item)
                        {
                            if(is_numeric($item)){
                                return $item;
                            }else{
                                return "'".$item."'";
                            }
                        },array_slice($params_array,1))
                    )
                    ."] && a.".$column."::text[]";
                    break;
                case "nin" :
                    $operation = " NOT (ARRAY[".implode(",",
                        array_map(function($item)
                        {
                            if(is_numeric($item)){
                                return $item;
                            }else{
                                return "'".$item."'";
                            }
                        },array_slice($params_array,1))
                    )
                    ."] && a.".$column."::text[])";
                    break;
                case "le" :
                    throw new \Exception("Operator 'le' not supported for array column type",400);
                    break;
                case "lt" :
                    throw new \Exception("Operator 'lt' not supported for array column type",400);
                    break;
                case "ge" :
                    throw new \Exception("Operator 'ge' not supported for array column type",400);
                    break;
                case "gt" :
                    throw new \Exception("Operator 'gt' not supported for array column type",400);
                    break; 
            }
        }else{
            switch($operator){
                case "eq" :
                    $operation = " a.".$column."=$parameter";
                    break;
                case "neq" :
                    $operation = " a.".$column."!=$parameter";
                    break;
                case "in" :
                    $operation = " a.".$column." IN (".implode(",",$parameters).")";
                    break;
                case "nin" :
                    $operation = " a.".$column." NOT IN (".implode(",",$parameters).")";
                    break;
                case "le" :
                    $operation = " a.".$column."<=$parameter";
                    break;
                case "lt" :
                    $operation = " a.".$column."<$parameter";
                    break;
                case "ge" :
                    $operation = " a.".$column.">=$parameter";
                    break;
                case "gt" :
                    $operation = " a.".$column.">$parameter";
                    break; 
            }
        }

        

        return $operation;
    }

    /**
     * Parse une opération And, récursif
     * @param string $params la chaîne de caractères de paramètres
     * @return Expr une expression de filtrage
     */
    private function parseAnd($params)
    {
        $operation = null;
        $matches = [];
        $params_ = explode("&",str_replace("),",")&",substr($params,1,strlen($params)-2)));
        $expressions = [];

        foreach($params_ as $oper){
            $split = array_filter(preg_split("/\([^()]+\)/",$oper))[0];

            if(!in_array($split,$this->operators)){
                throw new \Exception("Operator ".$split." not supported",422);
            }

            preg_match("/\([^()]+\)/",$oper,$matches);
            
            //print("\n\nParse And : \ninput : ".json_encode($oper)."\nOperator : ".$split." \nParams : ".json_encode($matches));
            
            if($split=="and"){
                $expressions[] = $this->parseAnd( $matches[0]);
            }else{
                $expressions[] = $this->parseOne( $split, $matches[0]);
            }
            
        }

        $operation = " (" . implode(" AND ",$expressions) . ") ";
        
        return $operation;
    }

    /**
     * Parse le queryparam _where 
     * référence : https://www.ibm.com/docs/en/supply-chain-insight?topic=eccra-using-where-query-parameter-specify-advanced-search-criteria
     * opérateurs supportés : and, eq, neq, in, nint, le, lt, ge, gt
     * @param string $_where la requête de filtrage
     */
    private function parseWhereQuery($_where)
    {
        //séparation en sous-requêtes
        //print("bouh");
        $expressions = [];
        $matches = [];
        $_wherearray = explode("&",$_where);
        if(count($_wherearray) > 1){
            foreach($_wherearray as $stmt){
                
                $split = array_filter(preg_split("/\([^&]+\)/",$stmt))[0];
                if(!in_array($split,$this->operators)){
                    throw new \Exception("Operator ".$split." not supported",422);
                }
                preg_match("/\([^&]+\)/",$stmt,$matches);

                /*print("\n\nStatement : \n".$stmt."\n\n");
                
                print("Operator : ".json_encode($split)."\nParameters : ".json_encode($matches)."\n");*/

                if($split=="and"){
                    $expressions[] = $this->parseAnd($matches[0]);
                }else{
                    $expressions[] = $this->parseOne($split, $matches[0]);
                }

                
            }
        }else{
            $split = array_filter(preg_split("/\([^&]+\)/",$_wherearray[0]))[0];
            preg_match("/\([^&]+\)/",$_wherearray[0],$matches);
            if(!in_array($split,$this->operators)){
                throw new \Exception("Operator ".$split." not supported",422);
            }
            if($split=="and"){
                $expressions[] = $this->parseAnd($matches[0]);
            }else{
                $expressions[] = $this->parseOne($split, $matches[0]);
            }
        }

        return  " AND " . implode(" AND ",$expressions);
    }

    protected function str_contains($haystack, $needle) {
        return $needle !== '' && mb_strpos($haystack, $needle) !== false;
    }

    protected function quote($my_string)
    {
        return "'".$my_string."'";
    }

    /**
     * Retourne le nom de la table draft.
     * Si cette table n'existe pas, elle est créée (sans les contraintes not null).
     *
     * @param $schemaName
     * @param $tableName
     * @param $create
     * @return string|null
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getDraftTable($schemaName, $tableName, $create)
    {
        $draftTableName = 'draft_'.$tableName;
        $connection = $this->getProdigeConnection($schemaName);

        // on vérifie si la table draft existe
        $sql = "select exists(select * from information_schema.tables where table_schema = :schemaName and table_name = :draftTableName)";
        $properties = array(
            "schemaName" => $schemaName,
            "draftTableName" => $draftTableName);

        $result = $connection->fetchAll($sql, $properties);
        if (empty($result) || !$result[0]["exists"]) {
            if ($create) {
                $sequenceName = $draftTableName.'_seq';
                $fullTableName = $schemaName.'.'.$tableName;
                $fullDraftTableName = $schemaName.'.'.$draftTableName;

                // si la table draft n'existe pas, on la crée en utilisant la structure de la table d'origine
                $sql =
                      "create sequence ${sequenceName};"
                    . "create table ${fullDraftTableName} (like ${fullTableName} including indexes excluding defaults excluding constraints);"
                    . "alter table ${fullDraftTableName} alter column gid set default nextval('${sequenceName}');";
                $connection->executeUpdate($sql);

                // on supprime les contraintes not null
                $sql = "select column_name from information_schema.columns where table_schema = :schemaName and table_name = :draftTableName";
                $columns = $connection->fetchAll($sql, $properties);
                $sql = "";
                foreach ($columns as $column) {
                    if($column['column_name']!=='gid'){
                        $sql .= "alter table ".$fullDraftTableName." alter column ".$column['column_name']." drop not null;";
                    }
                }
                if (!empty($sql)) {
                    $connection->executeUpdate($sql);
                }
            } else {
                return null;
            }
        }
        return $draftTableName;
    }

    public function isIpAuthorize(Request $request, $uuid){
        $sql = "SELECT couchd_api_restricted_ip FROM public.metadata INNER JOIN catalogue.fiche_metadonnees ON metadata.id = fiche_metadonnees.fmeta_id::int".
            " INNER JOIN catalogue.couche_donnees ON fiche_metadonnees.fmeta_fk_couche_donnees = couche_donnees.pk_couche_donnees WHERE metadata.uuid = :uuid";

        $connection = $this->getCatalogueConnection("catalogue");
        $properties = array("uuid" => $uuid);
        $result = $connection->fetchAll($sql, $properties);

        if(isset($result[0]["couchd_api_restricted_ip"]) && isset($request->headers->all()["x-forwarded-for"][0])){
            $tabIp = explode(";", $result[0]["couchd_api_restricted_ip"]);
            
            $listIpClient = explode(",",  $request->headers->all()["x-forwarded-for"][0]);
            $ipClient = $listIpClient[0];
            if (filter_var($ipClient, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) !== false){
                $ipClient = $this->expandIpv6($ipClient);
                foreach($tabIp as $value){
                    $ip = trim($value);

                    if(filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) !== false){
                        $ip = $this->expandIpv6($ip);

                        if(substr($ip, 0, -19) == substr($ipClient, 0, -19)){
                            return true;
                        }
                    }
                }
            } else {
                foreach($tabIp as $ip){
                    if($ipClient == $ip){
                        return true;
                    }
                }
            }
        }

        return false;
    }

    function expandIpv6($ip){
        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) !== false) {
            $hex = unpack("H*hex", inet_pton($ip));         
            $ip = substr(preg_replace("/([A-f0-9]{4})/", "$1:", $hex['hex']), 0, -1);
        }
    
        return $ip;
    }
}

<?php

namespace ProdigeCatalogue\ApiBundle\Controller;

use http\Client;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 *
 * @author alkante <support@alkante.com>
 *
 * @Route("/api")
 */
class ApiController extends BaseController
{

    const TGT_SESSION_NAME = 'api_tgt_session';

    /**
     * Authentification au service via Basic Auth
     *
     * @Route("/login", methods={"POST"})
     *
     * @OA\Post(
     *     summary="Connexion au service",
     * )
     * @OA\Response(
     *     response="401",
     *     description="Echec de l'authentification"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Tag(name="Authentification")
     *
     * @return JsonResponse
     */
    public function loginAction(Request $request, HttpClientInterface $client)
    {

        /*
            S'authentification au CAS pour récupérer un TGT
            POST https://prodige4.alkante.com:8444/cas/v1/tickets
            Content-Type: application/x-www-form-urlencoded
            username=xxx
            password=xxx
            201 Created
    
            dans le header Location : <TGT-ID>
    
            Demander au CAS un ST pour le service démandé (url)
            POST https://prodige4.alkante.com:8444/cas/v1/tickets/<TGT-ID>
            Content-Type: application/x-www-form-urlencoded
            service=https://prodige4.alkante.com/...
            200 OK
    
            dans le body: <ST-ID>
            Appeler un premier service avec le ticket ST
            ANY https://prodige4.alkante.com/...?ticket=<ST-ID>
            Les autres appels seront connectés tant que la session est vivante
        */
        try {
            // kill previous session for this client
            $request->getSession()->invalidate();

            $cas_url = 'https://'.$this->container->getParameter('cas_host');

            $r1 = $client->request('POST', $cas_url.$this->container->getParameter('cas_context').'/v1/tickets', [
                'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
                'body' => ['username' => $request->headers->get('php-auth-user'), 'password' => $request->headers->get('php-auth-pw')],
                'verify_peer' => false
            ]);

            if ($r1->getStatusCode() === 201) {

                //2. Récupérer le header location de la réponse, l'appeler en POST avec un service interne
                $headers = $r1->getHeaders();
                $location = $headers['location'][0];
                $request->getSession()->set(self::TGT_SESSION_NAME, $location);
                $tgt = str_replace($cas_url, '', $location); // extract TGT service /cas/v1/tickets/TGTxxx from the full Location header

                $r2 = $client->request('POST', $cas_url . $tgt, [
                    'headers' => ['Content-Type' => 'application/x-www-form-urlencoded'],
                    'body' =>  ['service' => $this->generateUrl('api_dummy',[],UrlGeneratorInterface::ABSOLUTE_URL)],
                    'verify_peer' => false
                ]);

                if ($r2->getStatusCode() === 200) {

                    // 3. Récupérer le ST présent dans le body de la réponse et appeler un service en mode connecté pour initialiser l'authentification CAS en session
                    $ST = $r2->getContent();
                    $r3 = $client->request('GET', $this->generateUrl('api_dummy', ['ticket' => $ST], UrlGeneratorInterface::ABSOLUTE_URL), ['verify_peer' => false]);

                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    // Changer l'ID la session courante avec le nom du ST, comme le fait PhpCas, 
                    // pour que les prochains appels chargent la bonne session et récupère l'authentification
                    // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    $headers = $r3->getHeaders();
                    $cookie = $headers['set-cookie'];
                    if (preg_match('/PHPSESSID=(.*?);/', $cookie[0], $matches)) {
                        $cookie = $matches[1];
                    }

                    $request->getSession()->save();
                    $request->getSession()->setId($cookie);
                    $request->getSession()->migrate();
                    $request->getSession()->start();

                    if ($r3->getStatusCode() === 200) {
                        $jsonResponse = new JsonResponse([
                            'status' => 200,
                            'message' => 'Authenticated',
                        ]);
                        $jsonResponse->headers->setCookie(new Cookie('PHPSESSID', $cookie));

                        return $jsonResponse;
                    } else {
                        // cas d'utilisation anormal, le service appelé devrait répondre
                        $this->getLogger()->error("Échec d'authentification via le ST");
                    }

                } else {
                    // cas d'utilisation anormal, un problème est survenu dans la validation du TGT pour obtenir un ST
                    $this->getLogger()->error(
                        "Échec de la validation de l'authentification auprès du CAS, ST non récupéré"
                    );
                }

            } else {
                $errorJson = json_decode($r1->getContent());
                if (isset($errorJson['authentication_exceptions'])) {
                    foreach ($errorJson['authentication_exceptions'] as $message) {
                        if (\strpos('CredentialExpiredException', $message) >= 0) {
                            return new JsonResponse(array(
                                'status' => 401,
                                'message' => "Mot de passe expiré",
                            ), 401);
                        }
                    }
                }
                $this->getLogger()->info("Échec de l'authentification auprès du CAS, TGT non récupéré");
            }

        } catch (\Exception $e) {

            $this->getLogger()->error($e->getMessage());
        }

        return new JsonResponse(array(
            'status' => 401,
            'message' => "Echec de l'authentification",
        ), 401);
    }


    /**
     * Service vide
     *
     * @Route("/dummy", name="api_dummy", options={"expose"=true})
     *
     * @return JsonResponse
     */
    public function dummyAction(Request $request)
    {
        $response = new JsonResponse('OK');
        $cookie = new Cookie('PHPSESSID',  $request->cookies->get('PHPSESSID'));
        $response->headers->setCookie($cookie);
        return $response;
    }

    /**
     * Get Information about current User
     *
     * @Route("/me", methods={"GET"})
     *
     * @OA\Get(
     *     summary="Informations de l'utilisateur connecté",
     * )
     * @OA\Response(
     *     response="403",
     *     description="Accès interdit"
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Tag(name="Authentification")
     *
     * @return JsonResponse
     */
    public function meAction(Request $request)
    {
        $user = User::GetUser();
        $userId = User::GetUserId();
        if ($userId === User::getUserInternet()) {
            return new JsonResponse(array(
                'status' => 403,
                'message' => "Accès interdit",
            ), 403);
        }
        $userInfo = new \stdClass;
        $userInfo->nom = $user->GetNom();
        $userInfo->prenom = $user->GetPrenom();
        $userInfo->id = $user->GetUserId();
        //$userInfo->email = $user->GetEmail();
        //take first email
        $tabEmails = explode(",", $user->GetEmail());
        $userInfo->email = $tabEmails[0];

        return new JsonResponse($userInfo);
    }

    /**
     * Déconnexion du service
     *
     * @Route("/logout", methods={"POST"})
     *
     * @OA\Post(
     *     summary="Déconnexion du service",
     * )
     * @OA\Response(
     *     response="200",
     *     description="Succès"
     * )
     * @OA\Tag(name="Authentification")
     *
     * @return JsonResponse
     */
    public function logoutAction(Request $request)
    {
        /*
            Se déconnecter
            DELETE https://prodige4.alkante.com:8444/cas/v1/tickets/<TGT-ID>
            => attention, ne tue pas la session du service appelé, donc on est surement toujours connecté au service
            3:25 PM
            Une fois connecté, pas besoin de se balader le ticket, il ne sert qu'à la première connexion
            Par contre le DELETE pour se déconnecter ne détruit pas la session PHP, donc on reste connecté tant qu'elle est valide même si le ticket a été detruit
        */

        $cas_url = 'https://'.$this->container->getParameter('cas_host');
        $location = $request->getSession()->get(self::TGT_SESSION_NAME, false);

        if ($location) {

            try {
                $tgt = str_replace(
                    $cas_url,
                    '',
                    $location
                ); // extract TGT service /cas/v1/tickets/TGTxxx from the full Location header

                $c1 = new Client($cas_url);
                $c1->setDefaultOption('exceptions', false);
                $r1 = $c1->delete(
                    $tgt,
                    ['Content-Type' => 'application/x-www-form-urlencoded'],
                    []
                )->send();
            } catch (\Exception $e) {
                $this->getLogger()->error("Erreur de logout");
            }

        }

        // Finally, destroy the session.
        $request->getSession()->invalidate();

        return new JsonResponse();
    }

}

<?php
namespace ProdigeCatalogue\AdminBundle\Common\Components\RelationList;

//require_once($AdminPath."/Administration/AccessRights/AccessRights.php");
use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;
use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\DAOProxy\ViewObject;
use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Classe constituant les listes
 * @author Alkante
 *
 */
class DoubleRelationList
{
	public $SEPARATOR = "_";
  private $Name;
  private $RelationViewObject;
  
  private $RelationKeyOrdinal1;
  private $RelationValueOrdinal1;
  private $RelationFKTable1;
  
  private $RelationKeyOrdinal2;
  private $RelationValueOrdinal2;
  private $RelationFKTable2;
  
  private $RelationTitle;
  private $RelationPKRestrictionOrdinal;
  private $RelationPKRestrictionValue;
  
  private $TableViewObject;
  private $TableTitle;
  
  private $TableKeyOrdinal1;
  private $TableValueOrdinal1;
  
  private $TableKeyOrdinal2;
  private $TableValueOrdinal2;

  private $ViewObject;
  private $ResultSet;
  
  private $KeyOrdinal;
  private $ValueOrdinal;
  
  private $Target;
  private $Page;
  private $Width;
  private $Height;

  private $Disabled;
  
  private $InsertMode;
  private $InverseFields;
  private $bCheckBox;
  
  private $strDetectError;

  /*
  public function __construct( $name, $viewObject, $keyOrdinal, $valueOrdinal, $target, $page, $width, $height )
  {
    $this->Name = $name;
    $this->ViewObject = $viewObject;
    $this->KeyOrdinal = $keyOrdinal;
    $this->ValueOrdinal = $valueOrdinal;
    $this->Target = $target;
    $this->Page = $page;
    $this->Width = $width;
    $this->Height = $height;

    $this->FillList();
  }
  */
  // ############################################

  public function __construct(   $name,
                  $relationTitle,
                  ViewObject $relationViewObject,
  		
                  $relationKeyOrdinal1,
                  $relationValueOrdinal1,
                  $RelationFKTable1,
  		
                  $relationKeyOrdinal2,
                  $relationValueOrdinal2,
                  $RelationFKTable2,
  		
                  $relationPKRestrictionOrdinal,
                  $relationPKRestrictionValue,
  		
                  ViewObject $tableViewObject,
                  $tableTitle,
  		
                  $tableKeyOrdinal1,
                  $tableValueOrdinal1,
  		
                  $tableKeyOrdinal2,
                  $tableValueOrdinal2,
  		
                  $page,
                  $target,
                  $insertMode = true,
                  $inverseFields = false,
                  $bCheckBox = true,
                  $strDetectError = "",
                  $readOnly = false
  ) {
    //global $AdminPath;
    $this->Name = $name;
    $this->RelationTitle = $relationTitle;
    $this->RelationViewObject = $relationViewObject;
    
    $this->RelationKeyOrdinal1 = $relationKeyOrdinal1;
    $this->RelationValueOrdinal1 = $relationValueOrdinal1;
    $this->RelationFKTable1 = $RelationFKTable1;
    
    $this->RelationKeyOrdinal2 = $relationKeyOrdinal2;
    $this->RelationValueOrdinal2 = $relationValueOrdinal2;
    $this->RelationFKTable2 = $RelationFKTable2;
    
    $this->RelationPKRestrictionOrdinal = $relationPKRestrictionOrdinal;
    $this->RelationPKRestrictionValue = $relationPKRestrictionValue;

    $this->TableViewObject = $tableViewObject;
    $this->TableTitle = $tableTitle;
    
    $this->TableKeyOrdinal1 = $tableKeyOrdinal1;
    $this->TableValueOrdinal1 = $tableValueOrdinal1;
    
    $this->TableKeyOrdinal2 = $tableKeyOrdinal2;
    $this->TableValueOrdinal2 = $tableValueOrdinal2;

    //$this->Page = str_replace($AdminPath, PRO_CATALOGUE_URLBASE."droits.php?url=", $page);
    $this->Page = $page;
      $this->Page = $_SERVER['REQUEST_URI'];
    $this->Target = Ressources::$IFRAME_NAME_MAIN;

    
    $this->InsertMode = $insertMode;
    $this->InverseFields = $inverseFields;
    
    $this->bCheckBox = $bCheckBox;
    
    $this->strDetectError = $strDetectError;
    
    $this->readOnly = $readOnly;
      
    $accessRights = new AccessRights();

    $this->canUpdate = true;
    if ( !$accessRights->CanUpdate( $this->RelationViewObject, $this->RelationPKRestrictionOrdinal, $this->RelationPKRestrictionValue ) || $this->readOnly ) {
      $this->Disabled = " DISABLED=\"DISABLED\" ";
      $this->canUpdate = false;
    }

    
    $iPart = (isset($_GET["iPart"]) ? $_GET["iPart"] : 1);
    if ( $iPart==Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION ){
      $this->SaveSelection();
      return new Response("");
    }
      
    $this->FillLists();
  }
    
  private function SaveSelection()
  {
    $selection = (isset($_POST["selection"]) ? $_POST["selection"] : array());
    
    // update association in data table
    if ( !$this->InsertMode ){
      
      $ordinalList = array();
      $valueList = array();
      $ordinalList[] = ( $this->InverseFields ? $this->RelationKeyOrdinal1 : $this->RelationPKRestrictionOrdinal );
      $valueList[]   = "NULL";
      
      $ordinalRestriction = $this->RelationPKRestrictionOrdinal;
      $valueRestriction   = $this->RelationPKRestrictionValue;

      $this->RelationViewObject->Update( $ordinalList, $valueList, $ordinalRestriction, $valueRestriction );
      $this->RelationViewObject->Commit();
      
      foreach ($selection as $selection_id){
        list($PK1, $PK2) = $this->splitMultipleId($selection_id);
        $ordinalList = array();
        $valueList = array();
        
        if ( $this->InverseFields ){
          // set data_key=<data_id>
          $ordinalList[] = $this->RelationKeyOrdinal1;
          $valueList[]   = $PK1;
          $ordinalList[] = $this->RelationKeyOrdinal2;
          $valueList[]   = $PK2;
          
          // where selection_key=<selection_id>
          $ordinalRestriction = $this->RelationPKRestrictionOrdinal;
          $valueRestriction   = $this->RelationPKRestrictionValue;
        }
        else {
          // set data_key=<data_id>
          $ordinalList[] = $this->RelationPKRestrictionOrdinal;
          $valueList[]   = $this->RelationPKRestrictionValue;
          
          // where selection_key=<selection_id>
          $ordinalRestriction = $this->RelationKeyOrdinal1;
          $valueRestriction   = $selection_id;
        }

        $this->RelationViewObject->Update( $ordinalList, $valueList, $ordinalRestriction, $valueRestriction );
        $this->RelationViewObject->Commit();
      }
    }
    // update association in join table
    else{
      // suppression de tous les enregistrements relatifs à la donnée de travail
      $this->RelationViewObject->DeleteRow( $this->RelationPKRestrictionOrdinal, $this->RelationPKRestrictionValue );
      $this->RelationViewObject->Commit();
      
      foreach ($selection as $selection_id){
        list($PK1, $PK2) = $this->splitMultipleId($selection_id);
        $ordinalList = array();
        $valueList = array();
        $ordinalList[] = $this->RelationPKRestrictionOrdinal;
        $ordinalList[] = $this->RelationKeyOrdinal1;
        $ordinalList[] = $this->RelationKeyOrdinal2;
        $valueList[] = $this->RelationPKRestrictionValue;
        $valueList[] = $PK1;
        $valueList[] = $PK2;
        
        $this->RelationViewObject->InsertValues( $ordinalList, $valueList );
        $this->RelationViewObject->Commit();
      }
    }
    $strJs = "parent.reloadPanel(parent.getActiveTabId('module_details'), parent.getActiveTabId('module_details').replace(/tab_(\d+)_\d+/, '\$1'));";
    if ( $this->InverseFields )
      $strJs .= "parent.reloadGrid(parent.getActiveTabId('module_details').replace(/tab_(\d+)_\d+/, '\$1'), ".$this->RelationPKRestrictionValue.");";
    AlertSaveController::AlertSaveDone($strJs, false);
  }
    // ############################################

  private function getMultipleId($PK1,$PK2)
  {
    return sprintf("%d".$this->SEPARATOR."%d", $PK1, $PK2);
  } 
  private function splitMultipleId($ID)
  {
    return explode($this->SEPARATOR, $ID);
  } 
  /**
   * @brief Remplissage de la liste et affichage
   * @return 
   */
  private function FillLists()
  {
    $this->RelationClick();
    $this->TableClick();
    
    $tabData = array();
    if ( $this->bCheckBox && $this->canUpdate ){
        // TODO hismail modified --- A vérifier avec Vincent
        //$tabData[-1] = array("name"=>"<b>".$this->RelationTitle."</b>", "id"=>"-1", "selection"=>"<a onclick=\"SelectAll(true)\">Tous</a>&nbsp;/&nbsp;<a onclick=\"SelectAll(false)\">Aucun</a>");
        $tabData[-1] = array("name"=>(is_array($this->RelationTitle)) ? "Array" : "<b>".$this->RelationTitle."</b>", "id"=>"-1", "selection"=>"<a onclick=\"SelectAll(true)\">Tous</a>&nbsp;/&nbsp;<a onclick=\"SelectAll(false)\">Aucun</a>");
    }
    if ( false ){
      echo "<br>Relation (in) = <br><br>".$this->RelationViewObject->GetSQLStmt();
      echo "<br><br>Table (notin) = <br><br>".$this->TableViewObject->GetSQLStmt();
    }

    $tabTitle = array("name1"=>$this->TableTitle, "name2"=>"", "selection"=>$this->RelationTitle);
    $ctrl_type = ($this->bCheckBox ? "checkbox" : "radio");


    $this->RelationViewObject->AddRestriction( $this->RelationPKRestrictionOrdinal, $this->RelationPKRestrictionValue );
    $this->RelationViewObject->AddOrder($this->RelationValueOrdinal1);
    $this->RelationViewObject->AddOrder($this->RelationValueOrdinal2);
    $this->RelationViewObject->Open();
    $this->RelationViewObject->GetResultSet()->First();/*OK*/
    
    if ( !$this->RelationViewObject->GetResultSet()->EOF() ){
	    $tabData["-1"]["id"] = "-1";
	    $tabData["-1"]["name1"] = "<b>".$this->RelationTitle[0]."</b>"; 
	    $tabData["-1"]["name2"] = "<b>".$this->RelationTitle[1]."</b>"; 
	    $tabData["-1"]["selection"] = "";
    }
  
    $row = 0;
    
    
      if ( $this->RelationViewObject->GetResultSet()->GetNbRows()>ViewObject::LIMIT_GROUPING ){
          $names1 = array();
          $names2 = array();
          while ( !$this->RelationViewObject->GetResultSet()->EOF() )
          {
          $PK1 = $this->RelationViewObject->GetResultSet()->Read($this->RelationFKTable1);
          $PK2 = $this->RelationViewObject->GetResultSet()->Read($this->RelationFKTable2);
            $PK = $this->getMultipleId($PK1, $PK2);
            $names1[] = $this->RelationViewObject->GetResultSet()->Read($this->RelationValueOrdinal1);
            $names2[] = "<input type=\"".$ctrl_type."\" value=\"".$PK."\"" .
                                          " name=\"selection[]\"" .
                                          " checked=\"checked\"".
                                          " onclick=\"onClickSelection(this);\"" .
                                          $this->Disabled.
                                          "/>&nbsp;".$this->RelationViewObject->GetResultSet()->Read($this->RelationValueOrdinal2);
            $this->RelationViewObject->GetResultSet()->Next();
          }
          $tabData["in"]["id"] = "in";
          $tabData["in"]["name1"] = implode("<br/>", $names1);
          $tabData["in"]["name2"] = implode("<br/>", $names2);
          $tabData["in"]["selection"] = "";
          $row++;
      } else {
        while ( !$this->RelationViewObject->GetResultSet()->EOF() )
        {
          $PK1 = $this->RelationViewObject->GetResultSet()->Read($this->RelationFKTable1);
          $PK2 = $this->RelationViewObject->GetResultSet()->Read($this->RelationFKTable2);
          $tabData[$row]["id"] = $this->getMultipleId($PK1, $PK2);
          /*Utilisé avec LayersAlerts : Inversion des colonnes necessaire . Pourquoi ?*/
          $tabData[$row]["name1"] = $this->RelationViewObject->GetResultSet()->Read($this->RelationValueOrdinal1); 
          $tabData[$row]["name2"] = $this->RelationViewObject->GetResultSet()->Read($this->RelationValueOrdinal2); 
          $tabData[$row]["selection"] = "<input type=\"".$ctrl_type."\" value=\"".$tabData[$row]["id"]."\"" .
                                        " name=\"selection[]\"" .
                                        " checked=\"checked\"".
                                        " onclick=\"onClickSelection(this);\"" .
                                        $this->Disabled.
                                        "/>";
          $this->RelationViewObject->GetResultSet()->Next();
          $row++;
        }
      }
    
    $this->TableViewObject->AddOrder($this->TableValueOrdinal1);
    $this->TableViewObject->AddOrder($this->TableValueOrdinal2);
    $this->TableViewObject->Open();
    
    $this->TableViewObject->GetResultSet()->First();
    
    if ( ( $this->bCheckBox && !$this->TableViewObject->GetResultSet()->EOF() )
      || (!$this->bCheckBox && !$this->TableViewObject->GetResultSet()->EOF() && !empty($tabData)) 
    ){
      $tabData["-2"]["id"] = "-2";
      $tabData["-2"]["name1"] = "<b>".$this->TableTitle[0]."</b>"; 
      $tabData["-2"]["name2"] = "<b>".$this->TableTitle[1]."</b>"; 
      $tabData["-2"]["selection"] = "";
    }

    $strJsAdaptGridView = "";
      if ( $this->TableViewObject->GetResultSet()->GetNbRows()>ViewObject::LIMIT_GROUPING ){
          $strJsAdaptGridView = "
, focusCell : function(c, a, b) {
    this.syncFocusEl(this.ensureVisible(c, a, b));
}
, ensureVisible : function(t, g, e) {
    var r = this.resolveCell(t, g, e);
    if (!r || !r.row) {
      return
    }
    return this.getResolvedXY(r)
}";
          $names1 = array();
          $names2 = array();
          while ( !$this->TableViewObject->GetResultSet()->EOF() )
          {
            $PK1 = $this->TableViewObject->GetResultSet()->Read($this->TableKeyOrdinal1);
            $PK2 = $this->TableViewObject->GetResultSet()->Read($this->TableKeyOrdinal2);
            $PK = $this->getMultipleId($PK1, $PK2);
            $names1[] = $this->TableViewObject->GetResultSet()->Read($this->TableValueOrdinal1);
            $names2[] = "<input type=\"".$ctrl_type."\" value=\"".$PK."\"" .
                                          " name=\"selection[]\"" .
                                          " onclick=\"onClickSelection(this);\"" .
                                          $this->Disabled.
                                          "/>&nbsp;".$this->TableViewObject->GetResultSet()->Read($this->TableValueOrdinal2);
            $this->TableViewObject->GetResultSet()->Next();
          }
          $tabData["out"]["id"] = "out";
          $tabData["out"]["name1"] = implode("<br/>", $names1);
          $tabData["out"]["name2"] = implode("<br/>", $names2);
          $tabData["out"]["selection"] = "";
          $row++;
      } else {
        // FILL THE SELECT LIST
        while ( !$this->TableViewObject->GetResultSet()->EOF() )
        {
          $PK1 = $this->TableViewObject->GetResultSet()->Read($this->TableKeyOrdinal1);
          $PK2 = $this->TableViewObject->GetResultSet()->Read($this->TableKeyOrdinal2);
          $tabData[$row]["id"] = $this->getMultipleId($PK1, $PK2);
          $tabData[$row]["name1"] = $this->TableViewObject->GetResultSet()->Read($this->TableValueOrdinal1); 
          $tabData[$row]["name2"] = $this->TableViewObject->GetResultSet()->Read($this->TableValueOrdinal2); 
          $tabData[$row]["selection"] = "<input type=\"".$ctrl_type."\" value=\"".$tabData[$row]["id"]."\"" .
                                        " name=\"selection[]\"" .
                                        " onclick=\"onClickSelection(this);\"" .
                                        $this->Disabled.
                                        "/>";
          $this->TableViewObject->GetResultSet()->Next();
          $row++;
        }
      }
    
    $tabFields = array(array("name"=>"id"));
    $tabColumns = array();
    $bNotTitle = false;

    foreach ($tabTitle as $dataIndex=>$title){
      $tabFields[] = array("name"=>$dataIndex);
      $column = array(
      "id" => "col_".$dataIndex,
      "dataIndex" => $dataIndex,
      // TODO hismail modified --- A vérifier avec Vincent
      //"header" => "<b>".$title."</b>",
      "header" => (is_array($title)) ? "Array" : "<b>".$title."</b>",
      );
      if ( $bNotTitle ){
      	$column["align"] = "center";
      	$column["width"] = 100;
      }
      $tabColumns[] = $column;
      if ( !$bNotTitle ) $bNotTitle = true;
    }

    $strFields = json_encode($tabFields);
    $strColumns = json_encode($tabColumns);
    
    $classSelection = "x-grid3-row-alt";
    ?>
    <script type='text/javascript'>
    function onClickSelection(oCheck)
    {
      <?php if ( !$this->bCheckBox ) echo "return;" ?>
      var element = new Ext.Element(adminTabPanelMain.getActiveTab().gridList.view.findRow(oCheck), true);
      if (oCheck.checked)
        element.addClass('<?php echo $classSelection ?>');
      else
        element.removeClass('<?php echo $classSelection ?>');
    }
    function SelectAll(bSelect)
    {
      var oForm = document.getElementById('form_<?php echo $this->Name ?>');
      var oChecks = oForm.elements["selection[]"];
      if ( !oChecks.item ){
        oChecks.checked = bSelect;
        oChecks.onclick();
        return;
      }
      for (var i=0; i <?php echo "<"?> oChecks.length; i++){
        oChecks[i].checked = bSelect;
        oChecks[i].onclick();
      }
    }
    function ChangeEnabled(oManage, OBJET_PK, listTraitement)
    {
      var tabTraitement = listTraitement.split(",");
      for (var i=0; i <?php echo "<"?> tabTraitement.length; i++){
        var oTraitement = document.getElementById('traitement['+OBJET_PK+']['+tabTraitement[i]+']');
        oTraitement.disabled = !oManage.checked;
      }
    }
    
    adminTabPanelMain.getActiveTab().add(new Ext.FormPanel({
      border: false,
      autoWidth : true,
      autoHeight : true,
      refOwner : adminTabPanelMain.getActiveTab(),
      ref : "../formPanel",
      renderTo : adminTabPanelMain.getActiveTab().id,
      standardSubmit: true,
      baseParams: {
          foo: 'bar'
      },
      id : 'panelform_<?php echo $this->Name ?>',
      formId : 'form_<?php echo $this->Name ?>',
      url: '<?php echo str_replace("iPart=1", "iPart=".Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION, $_SERVER["REQUEST_URI"])?>',
      items: [
        new Ext.grid.GridPanel({
          ref : "../../gridList",
          region : 'center',
          height : 400,
          border : true,
          store : new Ext.data.Store({
            reader : new Ext.data.JsonReader({
              idProperty: 'id',
              fields: <?php echo $strFields ?>
            }),
            data : <?php echo json_encode(array_values($tabData)); ?>
          }),
          id : 'grid_<?php echo $this->Name ?>',
          colModel: new Ext.grid.ColumnModel({
              defaults: {
                sortable: false, hideable:false, menuDisabled:true, resizable: true
              },
              columns: <?php echo $strColumns ?>
          }),
          hideHeaders : true,
          
          view: new Ext.grid.GridView({
            // render rows as they come into viewable area.
            scrollDelay: false,
            //cleanDelay: 200,
            
            deferEmptyText : false,
            forceFit: true,
            showPreview: true, // custom property
            enableRowBody: true, // required to create a second, full-width row to show expanded Record data
            
            // for correlated sdrs
            getRowClass: function(record, index, rowParam, store) {
              if ( record.id=="-1" || record.id=="-2" ){
                return "x-grid3-header";
              }
              <?php if ( $this->strDetectError!="" ){ ?>
                if ( record.get("name").indexOf("<?php echo $this->strDetectError; ?>")==-1 )
                  return "data-error";
              <?php } ?>
              return "";
            }
          <?php echo $strJsAdaptGridView; ?>
          }),
          loadMask : {msg : "Chargement en cours..."},
          disableSelection : true,
          autoExpandColumn : 'col_name',
          iconCls: 'icon-grid',
          margins : '5 5 5 5',
          buttonAlign : 'center',
          border : true,
          buttons : [
            <?php if ( $this->canUpdate ) { ?>
            new Ext.Button({
              defaults : {buttonAlign:'center'}, 
              text : 'Enregistrer la configuration',
              margins : '5 5 5 5',
              handler : function(){
                var oForm = document.getElementById('form_<?php echo $this->Name ?>');
                oForm.action = '<?php echo str_replace("iPart=1", "iPart=".Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION, $_SERVER["REQUEST_URI"])?>';
                oForm.target = '<?php echo Ressources::$IFRAME_NAME_MAIN; ?>';
                oForm.submit();
              }
            })
            <?php } ?>
          ],
          listeners : {
            afterrender : {
              fn : function(grid){
                grid.resizeEnable = true;
                updateGridListSize(grid);
                grid.on("afterrender", updateGridListSize);
                Ext.getCmp('module_details').getActiveTab().gridList = grid;
                tabGridList.push(grid);
              },
              delay : 100, 
              single : true
            }
          }
        })
      ]
    }));
    
    </script>
    <?php
  }

  // ############################################

  /**
   * @brief Remplissage de la liste de relation et affichage
   * @return 
   */
  private function FillRelationList()
  {
    $returnHTML = "";

    $this->RelationViewObject->AddRestriction( $this->RelationPKRestrictionOrdinal, $this->RelationPKRestrictionValue );

    $this->RelationViewObject->Open();
    $this->RelationViewObject->GetResultSet()->First();

    $returnHTML = '
      <select style="width:'.$this->Width.';height:'.$this->Height.';"  '.$this->Disabled.'
           multiple id="'.$this->Name.'Relation"
           language="javascript"
           ondblclick="'.$this->Name.'Relation_ondblclick(this);"
      >';

    // FILL THE SELECT LIST
    while ( !$this->RelationViewObject->GetResultSet()->EOF() )
    {
      $returnHTML = $returnHTML.'
        <option value="'.$this->RelationViewObject->GetResultSet()->Read($this->RelationKeyOrdinal1).'">
          '.$this->RelationViewObject->GetResultSet()->Read($this->RelationValueOrdinal1).'
        </option>';

      $this->RelationViewObject->GetResultSet()->Next();
    }

     $returnHTML = $returnHTML.'</select>';

     return $returnHTML;
  }

  // ############################################

  /**
   * @brief Affichage de la fonction javascript qui gère le clic sur une relation
   * @return 
   */
  private function RelationClick()
  {return;
    print('
      <script language="javascript" type="text/javascript">
        <!--
          function '.$this->Name.'Relation_ondblclick(selectList)
          {
              var myString="'.$this->Page.(strpos($this->Page, "?")===false ? "?" : "&").'Action='.Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION.$this->strActionParam.'&Id=";
              myString = myString+"'.$this->RelationPKRestrictionValue.'";
              myString = myString+"&RR=";
              myString = myString + selectList.options[selectList.selectedIndex].value;

              window.open(myString,"'.$this->Target.'");
          }
        // -->
      </script>
    ');
  }

  /**
   * @brief Affichage de la fonction javascript qui gère le clic sur une table
   * @return 
   */
  private function TableClick()
  {
    return;
    print('
      <script language="javascript" type="text/javascript">
        <!--
          function '.$this->Name.'Table_ondblclick(selectList)
          {
              var myString="'.$this->Page.(strpos($this->Page, "?")===false ? "?" : "&").'Action='.Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION.$this->strActionParam.'&Id=";
              myString = myString+"'.$this->RelationPKRestrictionValue.'";
              myString = myString+"&RR=";
              myString = myString + selectList.options[selectList.selectedIndex].value;

              window.open(myString,"'.$this->Target.'");
          }
        // -->
      </script>
    ');
  }

  private function FillTableList()
  {
    $returnHTML = "";

    $this->TableViewObject->Open();
    $this->TableViewObject->GetResultSet()->First();

    $returnHTML = '
      <select style="width:'.$this->Width.';height:'.$this->Height.';"  '.$this->Disabled.'
           multiple id="'.$this->Name.'Table"
           language="javascript"
           ondblclick="'.$this->Name.'Table_ondblclick(this);"
      >';

    // FILL THE SELECT LIST
    while ( !$this->TableViewObject->GetResultSet()->EOF() )
    {
      $returnHTML = $returnHTML.'
        <option value="'.$this->TableViewObject->GetResultSet()->Read($this->TableKeyOrdinal1).'">
          '.$this->TableViewObject->GetResultSet()->Read($this->TableValueOrdinal1).'
        </option>';

      $this->TableViewObject->GetResultSet()->Next();
    }

     $returnHTML = $returnHTML.'</select>';

     return $returnHTML;
  }

  // ############################################
}

?>

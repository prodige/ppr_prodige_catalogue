<?php
  namespace ProdigeCatalogue\AdminBundle\Common\Components\EditForm;

	//require_once($AdminPath."/Administration/AccessRights/AccessRights.php");
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;
  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;

	/**
	 * Classe servant à écrire des formulaires html
	 * @author Alkante
	 *
	 */
	Class EditForm
	{
		private $Name;
		private $ViewObjectClassName;
		private $ViewObject;
		private $RestrictionOrdinal;
		private $RestrictionValue;
		
		protected $FieldOrdinalsDisplay = array();
		protected $FieldOrdinals = array();
		protected $FieldTypes = array();
		protected $FieldSize = array();
		protected $FieldReadOnly =array();
		protected $FieldOnChangeEvent =array();
		protected $DBPage;
		protected $RefreshPage;
		
		protected $MainAction;
		
		
		public static $TYPE_TEXT = 0;
		public static $TYPE_CB = 1;
		public static $TYPE_TEXT_PASSWORD = 2;
		public static $TYPE_SELECT=3;
		public static $TYPE_HIDDEN = 4;
		public static $TYPE_DOUBLE_LIST = 1;
		public static $TYPE_TEXTAREA = 5;
		public static $TYPE_FILE_UPLOAD = 6;
		
		public function __construct( $name, $viewObject, $restrictionOrdinal, $restrictionValue, $fieldOrdinals, $fieldOrdinalsDisplay, $fieldTypes, $fieldSize, $fieldReadOnly, $refreshPage, $mainAction, $FieldOnChangeEvent = null, $hideBtValidate=false, $hideBtDelete=false, $arraySelect=array(), $submitUrl=null)
		{
      //global $AdminPath;
			$this->Name = $name;
			$this->ViewObject = $viewObject;
			$this->RestrictionOrdinal = $restrictionOrdinal;
			$this->RestrictionValue = $restrictionValue;
			$this->FieldOrdinals = $fieldOrdinals;
			$this->FieldOrdinalsDisplay = $fieldOrdinalsDisplay;
			$this->FieldTypes = $fieldTypes;
			$this->FieldSize = $fieldSize;
			$this->FieldReadOnly = $fieldReadOnly;
			$this->RefreshPage = $refreshPage;
			$this->MainAction = $mainAction;
			
			if (is_null($FieldOnChangeEvent))
				$this->FieldOnChangeEvent = array();
			else
				$this->FieldOnChangeEvent = $FieldOnChangeEvent;

			$this->ArraySelect = $arraySelect;	
			$this->FieldOnChangeEvent = array_pad($this->FieldOnChangeEvent, count($this->FieldOrdinals), '');
            $this->hideBtValidate = $hideBtValidate;
            $this->hideBtDelete = $hideBtDelete;
            $clazz = new \ReflectionClass($viewObject);
            $this->ViewObjectClassName = $clazz->getShortName(); //get_class( $viewObject );
			$this->DBPage = $submitUrl; /*'PRO_CATALOGUE_URLBASE."droits.php?url=Administration/Components/EditForm/WriteBOV.php";*/

			$this->DoUI();
		}
		
		/**
		 * @brief Effectue l'affichage du formulaire
		 * @return 
		 */
		private function DoUI()
		{
			$this->ViewObject->AddRestriction( $this->RestrictionOrdinal, $this->RestrictionValue );
			$this->ViewObject->Open();
			$this->ViewObject->GetResultSet()->First();
			
			
			$this->ValidateCallback();
			$this->DeleteCallback();
			
			print (' <FORM ENCTYPE="multipart/form-data" ID="'.$this->Name.'" METHOD="POST" target="'.Ressources::$IFRAME_NAME_MAIN.'" class="administration_gen"> ');
				print ('<TABLE STYLE="width:100%;" align="center" border=0>');
					foreach ( $this->FieldOrdinals as $key=>$ordinal)
					{
						  if($this->FieldTypes[$key]!=EditForm::$TYPE_HIDDEN){
  					    print ('<TR>');
  							print ('<TH style="width:40%;" align="right">');	
  							print $this->FieldOrdinalsDisplay[$key];
  							print ('</TH>');	
  							print ('<TD>');
						  }
								$readOnly = "";
								$FieldOnChangeEvent = "";

								if(isset($this->FieldReadOnly[$key])) { //Added by hismail
								    if ($this->FieldReadOnly[$key] == "1" )
								        $readOnly = "DISABLED";
								}
								else 
									$readOnly = "";
								if ($this->FieldOnChangeEvent[$key] != "")
									$FieldOnChangeEvent = " onchange=\"".$this->FieldOnChangeEvent[$key]."\"";
								else
									$FieldOnChangeEvent = "";
                if (!empty($this->ArraySelect[$key]))  
									$arraySelect = $this->ArraySelect[$key];
								else
								  $arraySelect = array();	
                
								switch ( $this->FieldTypes[$key] )
								{
									case EditForm::$TYPE_TEXT_PASSWORD:
										print('   <INPUT 	TYPE="PASSWORD" 
															NAME="ORDINAL_'.$ordinal.'"
														 	ID="ORDINAL_'.$ordinal.'" 
														 	SIZE="'.$this->FieldSize[$key].'" 
														 	VALUE="'.$this->ViewObject->Read($ordinal).'"
														 	'.$readOnly.'
														 	'.$FieldOnChangeEvent.'
														 	>');
									break;
									case EditForm::$TYPE_TEXT:
									  
										print('   <INPUT 	TYPE="text" 
															NAME="ORDINAL_'.$ordinal.'"
														 	ID="ORDINAL_'.$ordinal.'" 
														 	SIZE="'.$this->FieldSize[$key].'" 
														 	VALUE="'.$this->ViewObject->Read($ordinal).'"
														 	'.$readOnly.'
														 	'.$FieldOnChangeEvent.'
														 	>');
									break;
									case EditForm::$TYPE_FILE_UPLOAD:
									$valueFile = (strpos($this->ViewObject->Read($ordinal), "http://")===false && strpos($this->ViewObject->Read($ordinal), "https://")===false ? substr($this->ViewObject->Read($ordinal), strrpos($this->ViewObject->Read($ordinal), "/")+1) :  $this->ViewObject->Read($ordinal) );
    								print('<INPUT 	TYPE="file"
    														NAME="ORDINAL_'.$ordinal.'"
    													 	ID="ORDINAL_'.$ordinal.'"
    													 	'.$readOnly.'
    													 	'.$FieldOnChangeEvent.'
    													 	> &nbsp;'.$valueFile);
    								break;
									case EditForm::$TYPE_TEXTAREA:
									  	
									print('   <textarea 
															NAME="ORDINAL_'.$ordinal.'"
														 	ID="ORDINAL_'.$ordinal.'"
														 	rows="3"
														 	cols="'.$this->FieldSize[$key].'"
														 	'.$readOnly.'
														 	'.$FieldOnChangeEvent.'
														 	>'.$this->ViewObject->Read($ordinal).'</textarea>');
									break;
									case EditForm::$TYPE_CB:
										if ( $this->ViewObject->Read($ordinal) == 1 )
											print('<INPUT 	TYPE="checkbox" 
															NAME="ORDINAL_'.$ordinal.'" 
															ID="ORDINAL_'.$ordinal.'" 
															VALUE="1" 
															CHECKED
															'.$readOnly.'
														 	'.$FieldOnChangeEvent.'
															>');
										else 
											print('<INPUT 	TYPE="checkbox" 
															NAME="ORDINAL_'.$ordinal.'" 
															ID="ORDINAL_'.$ordinal.'" 
															VALUE="1"
															'.$readOnly.'
														 	'.$FieldOnChangeEvent.'
															>');
									break;
									case EditForm::$TYPE_HIDDEN:
                    
                    print('   <INPUT  TYPE="hidden" 
                              NAME="ORDINAL_'.$ordinal.'"
                              ID="ORDINAL_'.$ordinal.'" 
                              VALUE="'.$this->ViewObject->Read($ordinal).'"
                              '.$readOnly.'
                              '.$FieldOnChangeEvent.'
                              >');
                  break;
									case EditForm::$TYPE_SELECT:/*select element of html form*/
  								   
  								  /*$rs=$this->ViewObject->GetResultSet();
								    for ($rs->First(); !$rs->EOF(); $rs->Next()) {
                       $selected=$rs->Read($ordinal+1);//la clé doit suivre;
                    }*/
  								  echo "<select ".$readOnly." name='ORDINAL_".$ordinal."' ID='ORDINAL_".$ordinal."' ".$FieldOnChangeEvent.">";
                    foreach($arraySelect as $key=>$value){
                      if ($key==$this->ViewObject->Read($ordinal)){
  								      echo '<option value="'. $key. '" selected="selected">'. $value. '</option>';
                      }
                      else{
                        echo '<option value="'. $key. '">'. $value. '</option>';
                      }
  								  }
                    echo '</select>';             
                  break;
									case EditForm::$TYPE_DOUBLE_LIST:/*select element of html form*/
  								   
  								  /*$rs=$this->ViewObject->GetResultSet();
								    for ($rs->First(); !$rs->EOF(); $rs->Next()) {
                       $selected=$rs->Read($ordinal+1);//la clé doit suivre;
                    }*/
  								  echo "<select ".$readOnly." name='ORDINAL_".$ordinal."' ID='ORDINAL_".$ordinal."' ".$FieldOnChangeEvent.">";
                    foreach($arraySelect as $key=>$value){
                      if ($key==$this->ViewObject->Read($ordinal)){
  								      echo '<option value="'. $key. '" selected="selected">'. $value. '</option>';
                      }
                      else{
                        echo '<option value="'. $key. '">'. $value. '</option>';
                      }
  								  }
                    echo '</select>';             
                  break;
								}
								if(isset($this->FieldTypes[$key])) { //Condition if added by hismail
								    if($this->FieldTypes[$key]!=EditForm::$TYPE_HIDDEN){
								        print ('</TD>');
								        print ('</TR>');
								    }
								}

						
					}
				print ('</TABLE>');
				
				print ('<INPUT TYPE="HIDDEN" NAME="VOClassName" ID="VOClassName" VALUE="'.$this->ViewObjectClassName.'" >');
				print ('<INPUT TYPE="HIDDEN" NAME="Ordinals" ID="Ordinals" VALUE="'.implode(";", $this->FieldOrdinals ).'" >');
				print ('<INPUT TYPE="HIDDEN" NAME="RestrictionOrdinal" ID="RestrictionOrdinal" VALUE="'.$this->RestrictionOrdinal.'" >');
				print ('<INPUT TYPE="HIDDEN" NAME="Id" ID="Id" VALUE="'.$this->RestrictionValue.'" >');
				print ('<INPUT TYPE="HIDDEN" NAME="Action" ID="Action" VALUE="0" >');
				print ('<INPUT TYPE="HIDDEN" NAME="DBPage" ID="DBPage" VALUE="test" >');
				print ('<INPUT TYPE="HIDDEN" NAME="RefreshPage" ID="RefreshPage" VALUE ="'.$this->RefreshPage.'" >');
				print ('<INPUT TYPE="HIDDEN" NAME="MainAction" ID="MainAction" VALUE="'.$this->MainAction.'" >');
				print ('<INPUT TYPE="HIDDEN" NAME="ReadOnly" ID="ReadOnly" VALUE="'.implode(";",$this->FieldReadOnly ).'" >');
				
				
			print (' </FORM> ');
			
			$this->DisplayButtons();
		}
		
		/**
		 * @brief Ecrit la fonction javascript de validation du formulaire
		 * @return 
		 */
		private function ValidateCallback()
		{
			print('
				<script language="javascript" type="text/javascript">
						function '.$this->Name.'Validate_onclick() 
						{
							var form = document.getElementById("'.$this->Name.'");
							
							var inputId = document.getElementById("Id");
							var inputAction = document.getElementById("Action");
							
							form.action = "'.$this->DBPage.'";
							form.submit();
						}
				</script>
			');			
		}
		
		/**
     * @brief Ecrit la fonction javascript de suppression
     * @return 
     */
		private function DeleteCallback()
		{
			print('
				<script language="javascript" type="text/javascript">
						function '.$this->Name.'Delete_onclick() 
						{
							var form = document.getElementById("'.$this->Name.'");
							
							var inputId = document.getElementById("Id");
							var inputAction = document.getElementById("Action");
							
							inputAction.value = "'.Ressources::$EDITFORM_DELETE.'"; 
							form.action = "'.$this->DBPage.'";
              if (confirm("'.Ressources::$EDITFORM_DELETE_CONFIRMATION.'")){;
							 form.submit();
						  }
            }
				</script>
			');			
		}
		
		/**
		 * Génère l'affichage des boutons
		 * @return unknown_type
		 */
		private function DisplayButtons()
		{
			$ValidateDISABLED = "";
			$DeleteDISABLED = "";

			$accessRights = new AccessRights();
            
			if ( !$accessRights->CanUpdate( $this->ViewObject, -2, $this->RestrictionValue ) )
				$ValidateDISABLED = "DISABLED=\"DISABLED\" class=\"DISABLED\"";
			
			if ( !$accessRights->CanDelete( $this->ViewObject, -3, $this->RestrictionValue ) || $this->RestrictionValue==Ressources::$NEWROW )
				$DeleteDISABLED = "DISABLED=\"DISABLED\" class=\"DISABLED\"";
			
			print ('<br>
					<table style="text-align: center" align="center" class="validation">
			            <tr>
			                <td style="height: 26px">'.
			                    ( $this->hideBtValidate ? '' : '<input type="BUTTON" '.$ValidateDISABLED.' Value="'.Ressources::$EDITFORM_BTN_VALIDATE.'" onclick="'.$this->Name.'Validate_onclick();"/>' ).
			                '</td>
			                <td style="height: 26px">'.
			                    ( $this->hideBtDelete ? '' : '<input type="BUTTON" '.$DeleteDISABLED.' Value="'.Ressources::$EDITFORM_BTN_DELETE.'"   onclick="'.$this->Name.'Delete_onclick();"/>' ).
			                '</td>
			            </tr>
			        </table>
			       ');
		}
		
	}
?>
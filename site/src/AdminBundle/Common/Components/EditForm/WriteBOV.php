<?php

namespace ProdigeCatalogue\AdminBundle\Common\Components\EditForm;

/**
 *
 * @author Alkante
 */
/* require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Modules/BO/DomaineVO.php");
  require_once($AdminPath."/Modules/BO/SousDomaineVO.php");
  require_once($AdminPath."/Modules/BO/CarteProjetVO.php");
  require_once($AdminPath."/Modules/BO/CoucheDonneesVO.php");
  require_once($AdminPath."/Modules/BO/FicheMetaDonneesVO.php");
  require_once($AdminPath."/Modules/BO/CommuneVO.php");
  require_once($AdminPath."/Modules/BO/GroupeProfilVO.php");
  require_once($AdminPath."/Modules/BO/TraitementVO.php");
  require_once($AdminPath."/Modules/BO/CompetenceVO.php");
  require_once($AdminPath."/Modules/BO/ZonageVO.php");
  require_once($AdminPath."/Modules/BO/PerimetreVO.php");
  require_once($AdminPath."/Modules/BO/RubricVO.php"); */

use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

// set a default action
if ( !isset($_POST["Action"]) ) {
    echo "action";
    exit;
}
else {
    $Action = $_POST["Action"];
}

if ( !isset($_POST["MainAction"]) ) {
    echo "mainAction";
    exit;
}
else {
    $MainAction = $_POST["MainAction"];
}

// set a default ID
if ( !isset($_POST["Id"]) ) {
    echo "Id";
    exit;
}
else {
    $PK = intval($_POST["Id"]);
}


if ( !isset($_POST["RefreshPage"]) ) {
    echo "RefreshPage";
    exit;
}
else {
    $RefreshPage = $_POST["RefreshPage"];
}



if ( !isset($_POST["VOClassName"]) ) {
    echo "classname";
    exit;
}
else {
    $VOClassName = $_POST["VOClassName"];
}

if ( !isset($_POST["Ordinals"]) ) {
    echo "ordinals";
    exit;
}
else {
    $OrdinalString = $_POST["Ordinals"];
    $OrdinalsList = explode(";", $OrdinalString);
}


if ( !isset($_POST["ReadOnly"]) ) {
    echo "ReadOnly";
    exit;
}
else {
    $ReadOnlyString = $_POST["ReadOnly"];
    $ReadOnlyList = explode(";", $ReadOnlyString);
}

$dao = new DAO($conn, 'catalogue');

//suppression des droits enregistrés en session pour l'ajout de couches  
unset($_SESSION["userLayerRights"]);
//suppression du cache du service verifyrights
$meminstance = new \Memcached(); 
$meminstance->addServer("visualiseur-memcached", 11211);
$meminstance->flush();

$reflectionClass = new \ReflectionClass('ProdigeCatalogue\\AdminBundle\\Common\\Modules\\BO\\' . $VOClassName);
$vo = $reflectionClass->newInstance();
$vo->setDao($dao);

$ordinalList = array();
$valueList = array();

$ordinalRestriction = $_POST["RestrictionOrdinal"];
global $valueRestriction;
$valueRestriction = $_POST["Id"];

// ####################################################
// ###################   DELETE   #####################
// ####################################################
if ( $Action == Ressources::$EDITFORM_DELETE ) {

    $vo->setCurrentValues((array)$ordinalRestriction, (array)$valueRestriction);
    
    //for delete, need ldapSync to be before user is deleted
    if ( method_exists($vo, "ldapSyncAction") )
        $vo->ldapSyncAction($valueRestriction, true);

    $beforeSaveValues = null;
    if ( method_exists($vo, "beforeSaveAction") )
        $beforeSaveValues = $vo->beforeSaveAction($controller, $conn, "DELETE");
    
    if ( $beforeSaveValues ){
        list($ordinalRestriction, $valueRestriction) = $beforeSaveValues;
    }
    if ( $vo->DeleteRow($ordinalRestriction, $valueRestriction) ) {
        $vo->Commit();

        if ( method_exists($vo, "syncThesaurus") )
            $vo->syncThesaurus($this);
        if ( method_exists($vo, "afterSaveAction") )
            $vo->afterSaveAction($controller, $conn, true);

        AlertSaveController::AlertSaveDone("parent.reloadGrid('" . $MainAction . "', -1)", true, Ressources::$EDITFORM_WAS_DELETE);
    }
    else {
        AlertSaveController::AlertSaveDone("", true, Ressources::$DAO_EXECUTE_ERROR, true);
    }
}

// ####################################################
// ##################### INSERT  ######################
// ####################################################
if ( $valueRestriction == Ressources::$NEWROW ) {
    $valueRestriction = $vo->GetNextPKSequence();

    foreach ($OrdinalsList as $key => $ordinal) {
        $PostName = "ORDINAL_" . $ordinal;
        $shouldBeInserted = true;
        if ( !isset($_POST[$PostName]) && !isset($_FILES[$PostName]) )
            $valueList[] = "0";
        else {
            if ( isset($_POST[$PostName]) ) {
                $value = $_POST[$PostName];
            }
            else {
                if ( $_FILES[$PostName]['name'] != "" ) {
                    $accepted_extension = array(
                        'avi', 'mov', 'mp3', 'mpg', 'mpeg', 'wma', 
                        'bmp', 'gif', 'jpg', 'jpeg', 'jp2', 'png', 'tif', 'tiff', 
                        'xls', 'ods', 'csv', 'doc', 'docx', 'odt', 'txt', 'pdf', 'html'
                    );                           
                    $file_extension = strtolower(pathinfo($_FILES[$PostName]['name'], PATHINFO_EXTENSION)); 
                    if( in_array($file_extension, $accepted_extension) !== false) { 
                        $value = "./ressources/" . $_FILES[$PostName]['name'];
                        $uploadfile = $uploadFolder . 'ressources/' . $_FILES[$PostName]['name'];
                        move_uploaded_file($_FILES[$PostName]['tmp_name'], $uploadfile);                        
                    } else {
                        $valueList[] = "0";
                        $shouldBeInserted = false;
                    }

                }
                else {
                    $shouldBeInserted = false;
                }
            }
            //$value = stripslashes($_POST[$PostName]);
            //$value = str_replace("'","''", $value);
            $value = htmlentities($value, ENT_QUOTES, "UTF-8");
            if ( $shouldBeInserted ) {
                if ( $value == "" )
                    $valueList[] = " NULL ";
                else
                    $valueList[] = $value;
            }
        }

        if ( $shouldBeInserted ) {
            $ordinalList[] = $ordinal;
        }
    }

    $ordinalList[] = $ordinalRestriction;
    $valueList[] = $valueRestriction;

    //echo "INSERT ".$ordinalRestriction."   ".$valueRestriction."<bR>";

    $vo->setCurrentValues($ordinalList, $valueList);
    
    $beforeSaveValues = null;
    if ( method_exists($vo, "beforeSaveAction") )
        $beforeSaveValues = $vo->beforeSaveAction($controller, $conn, "INSERT");
    
    if ( $beforeSaveValues ){
        list($ordinalList, $valueList) = $beforeSaveValues;
    }
    
    if($shouldBeInserted ) {
        if ($vo->InsertValues($ordinalList, $valueList) ) {
            $vo->Commit();

            if ( method_exists($vo, "syncThesaurus") )
                $vo->syncThesaurus($this);
            if ( method_exists($vo, "afterSaveAction") )
                $vo->afterSaveAction($controller, $conn);
            if ( method_exists($vo, "ldapSyncAction") )
                $vo->ldapSyncAction($valueRestriction);

            AlertSaveController::AlertSaveDone("parent.reloadGrid('" . $MainAction . "', " . $valueRestriction . ")");
        }
        else {
            AlertSaveController::AlertSaveDone("", true, Ressources::$DAO_EXECUTE_ERROR, true);
        }        
    } else {
        AlertSaveController::AlertSaveDone("", true, Ressources::$INVALID_FILE_EXTENSION , true);
    }

}
// ####################################################
// ##################### UPDATE  ######################
// ####################################################
else {
    
    foreach ($OrdinalsList as $key => $ordinal) {
        if ( isset($ReadOnlyList[$key]) ) { //Condition if added by hismail
            if ( $ReadOnlyList[$key] != "1" ) {
                $shouldBeInserted = true;

                $PostName = "ORDINAL_" . $ordinal;
                if ( !isset($_POST[$PostName]) && !isset($_FILES[$PostName]) ) { //for checkbox 
                    $valueList[] = "0";
                }
                else {
                    if ( isset($_POST[$PostName]) ) {
                        $value = $_POST[$PostName];
                    }
                    else {
                        if ( $_FILES[$PostName]['name'] != "" ) {
                            $accepted_extension = array(
                                'avi', 'mov', 'mp3', 'mpg', 'mpeg', 'wma', 
                                'bmp', 'gif', 'jpg', 'jpeg', 'jp2', 'png', 'tif', 'tiff', 
                                'xls', 'ods', 'csv', 'doc', 'docx', 'odt', 'txt', 'pdf', 'html'
                            );                           
                            $file_extension = strtolower(pathinfo($_FILES[$PostName]['name'], PATHINFO_EXTENSION)); 
                            if( in_array($file_extension, $accepted_extension) !== false) { 
                                $value = "./ressources/" . $_FILES[$PostName]['name'];
                                $uploadfile = $uploadFolder . 'ressources/' . $_FILES[$PostName]['name'];
                                move_uploaded_file($_FILES[$PostName]['tmp_name'], $uploadfile);                        
                            } else {
                                $valueList[] = "0";
                                $shouldBeInserted = false;
                            }
                        }
                        else {
                            $shouldBeInserted = false;
                        }
                    }

                    //$value = stripslashes($_POST[$PostName]);
                    //$value = str_replace("'","''", $value);
                    //$value = ( $value );

                    $value = htmlentities($value, ENT_QUOTES, "UTF-8");
                    if ( $shouldBeInserted ) {
                        if ( $value == "" )
                            $valueList[] = " NULL ";
                        else
                            $valueList[] = $value;
                    }
                }
                if ( $shouldBeInserted ) {
                    $ordinalList[] = $ordinal;
                }
            }
        }
    }

    $vo->setCurrentValues($ordinalList, $valueList, $ordinalRestriction, $valueRestriction);
    
    
    $beforeSaveValues = null;
    if ( method_exists($vo, "beforeSaveAction") )
        $beforeSaveValues = $vo->beforeSaveAction($controller, $conn, "INSERT");
    
    if ( $beforeSaveValues ){
        list($ordinalList, $valueList) = $beforeSaveValues;
    }
    
    if ( method_exists($vo, "beforeSaveAction") )
        $vo->beforeSaveAction($controller, $conn, "UPDATE");
    
    if ( $vo->Update($ordinalList, $valueList, $ordinalRestriction, $valueRestriction) ) {
        $vo->Commit();

        
        if ( method_exists($vo, "syncThesaurus") )
            $vo->syncThesaurus($this);
        if ( method_exists($vo, "afterSaveAction") )
            $vo->afterSaveAction($controller, $conn);
        if ( method_exists($vo, "ldapSyncAction") )
            $vo->ldapSyncAction($valueRestriction);

        AlertSaveController::AlertSaveDone("parent.reloadGrid('" . $MainAction . "', " . $valueRestriction . ")");
    }
    else {
        AlertSaveController::AlertSaveDone("", true, Ressources::$DAO_EXECUTE_ERROR, true);
    }
}
?>

<?php
namespace ProdigeCatalogue\AdminBundle\Common\Components\SelectList;

use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;
use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;

	/**
   * Classe constituant les listes de sélection html
   * @author Alkante
   *
   */
	class SelectList
	{
		private $Name;
		private $ViewObject;
		private $ResultSet;
		private $KeyOrdinal;
		private $ValueOrdinal;
		private $Target;
		private $Page;
		private $Width;
		private $Height;
    private $Disabled;
		
		public function __construct( $name, $viewObject, $keyOrdinal, $valueOrdinal, $target, $page, $width, $height, $bButtons = true, $strActionParam = "", $bDisabled=false)
		{
      if ($bDisabled)
        $this->Disabled = "DISABLED=\"disabled\"";  
      $this->Name = $name;
			$this->ViewObject = $viewObject;
			$this->KeyOrdinal = $keyOrdinal;
			$this->ValueOrdinal = $valueOrdinal;	
			$this->Target = Ressources::$IFRAME_NAME_MAIN;
			$this->Page = /*"droits.php?url=".*/$page;
			$this->Width = $width;
			$this->Height = $height-30;
      $this->strActionParam = $strActionParam; 
			$this->FillList();	
			$this->AddCallback();
			$this->DeleteCallback();
			if ($bButtons)
        $this->PlaceButtons();
      
      
		}
		
		/**
     * @brief Remplissage de la liste et affichage
     * @return 
     */
		private function FillList()
		{
		  $this->ViewObject->Open();			
			$this->ViewObject->GetResultSet()->First();			
			
			print('
				<script language="javascript" type="text/javascript">
					<!--
						function '.$this->Name.'_onclick(selectList) 
						{
		    				var myString="'.$this->Page.'?Action='.Ressources::$LIST_MODIFY_ACTION.$this->strActionParam.'&Id=";
		    				if ( selectList.selectedIndex == -1 )
		    					return;
		    					
							//myString = myString + selectList.options[selectList.selectedIndex].value; 
							myString = myString + selectList.value;
		    				
							window.open(myString,"'.$this->Target.'");
						}
					// -->
				</script>
			');
			// SELECT LIST DECLARATION
      
			print ('
				<select	style="width:'.$this->Width.'px; height: '.$this->Height.'px;" '. $this->Disabled.'
					 	multiple="multiple" id="'.$this->Name.'"
					 	language="javascript" 
					 	onchange="'.$this->Name.'_onclick(this);"
					 	>
				');
			// FILL THE SELECT LIST
			while ( !$this->ViewObject->GetResultSet()->EOF() )
			{
				//appel d'une fonction getOptionClassName pour potentiellement donner un style sur chaque objet de la liste
				$className = "";
  			if(method_exists($this->ViewObject, "getOptionClassName"))
          $className = $this->ViewObject->getOptionClassName($this->ViewObject);
				print('
					<option '.($className!="" ? 'classname="'.$className.'"' : '').' value="'.$this->ViewObject->GetResultSet()->Read($this->KeyOrdinal).'" title="'.htmlspecialchars($this->ViewObject->GetResultSet()->Read($this->ValueOrdinal), ENT_QUOTES, 'UTF-8').'" >
						'.htmlspecialchars($this->ViewObject->GetResultSet()->Read($this->ValueOrdinal), ENT_QUOTES, 'UTF-8').'
					</option>
					');

				$this->ViewObject->GetResultSet()->Next();
			}
			 print ('</select>');
		}
		
		/**
		 * @brief Ecriture de la fonction javascript
		 * @return unknown_type
		 */
		private function AddCallback()
		{
			print('
				<script language="javascript" type="text/javascript">
					<!--

						function '.$this->Name.'Add_onclick() 
						{
		    				var myString="'.$this->Page.'?Action='.Ressources::$LIST_MODIFY_ACTION.'&Id=";
		    				myString = myString + "'.Ressources::$NEWROW.'"; 
		    				
							window.open(myString,"'.$this->Target.'");
						}
					// -->
				</script>
			');			
      	
		}
		
		
		private function DeleteCallback()
		{
			
		}
		
		/**
		 * @brief Affichage des boutons
		 * @return 
		 */
		private function PlaceButtons()
		{
			$accessRights = new AccessRights();
			$disabled = "";
            
            $clazz = new \ReflectionClass($this->ViewObject); //get_class( $this->ViewObject );
            $clazz = $clazz->getShortName();
			
			if ( !$accessRights->CanInsert( $this->ViewObject) )
				$disabled = "DISABLED=\"disabled\"";
		
			print ('<TABLE STYLE="width:100%;text-align: center" border="0">');
				print('<TR>');
					print('<TD>');
						print('<input style="font-size:8pt;text-align: center" type="BUTTON" '.$disabled.' Value="'.Ressources::$SELECTLIST_ADD.'" onclick="'.$this->Name.'Add_onclick();"/>');
					print('</TD>');
      if ( $clazz=="UtilisateurVO" ){
          print('<TD>');
            print('<input style="font-size:8pt;text-align: center" type="BUTTON" Value="Importer" onclick="catalogue_administration_users_importer_form"/>');
          print('</TD>');
          print('<TD>');
            print('<input style="font-size:8pt;text-align: center" type="BUTTON" Value="Exporter" onclick="catalogue_administration_users_exporter_form"/>');
          print('</TD>');
      }
      if ( $clazz=="CoucheDonneesVO" ){
          print('<TD>');
            print('<input style="font-size:8pt;text-align: center" type="BUTTON" Value="Importer" onclick="catalogue_administration_layers_importer_form"/>');
          print('</TD>');
      }
      if( $clazz=="PerimetreVO"){
        print('<TD>');
        print('<input style="font-size:8pt;text-align: center" type="BUTTON" Value="Utilisateur/Territoire" onclick="catalogue_administration_perimetres_usersuploader"/>');
        print('</TD>');
          
      }
				//	print('<TD>');
				//		print('<input style="font-size:8pt;text-align: center" type="BUTTON" Value="'.Ressources::$SELECTLIST_ADD.'" onclick="'.$this->Name.'Add_onclick();"/>');
				//	print('</TD>');
				print('</TR>');
			print ('</TABLE>');
		}
	}

?>

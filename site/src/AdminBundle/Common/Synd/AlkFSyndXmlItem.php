<?php
namespace ProdigeCatalogue\AdminBundle\Common\Synd;

// Les "defines" sont déplacés dans prodige/prodige/Prodige/ProdigeBundle/config/parameters.yml
// RSS 0.90  officiellement obsolete depuis 1.0
// RSS 0.91, 0.92, 0.93 and 0.94  officiellement obsolete depuis 2.0
//define("ALK_RSS1", "RSS 1.0", true);
//define("ALK_RSS2", "RSS 2.0", true);
//define("ALK_ATOM", "ATOM", true);

//define("ALK_DATE_RSS", "D, d M Y G:i:s T"); // RFC-822 : Wed, 04 Feb 2008 08:00:00 EST
//define("ALK_DATE_ATOM", "c"); // ISO 860, ex.: 2003-12-13T18:30:02.25+01:00

class AlkFSyndXmlItem {

    /** ensemble des noeuds du flux */
    private $elements = array();

    /** type de flux */
    private $version;

    /**
     * Constructor
     *
     * @param    contant     (ALK_RSS1/ALK_RSS2/ALK_ATOM) ALK_RSS2 is default.
     */
    function __construct($version = ALK_RSS2) {
        $this->version = $version;
    }

    /**
     * Ajoute un élément à la collection
     *
     * @param strName     Nom du tag
     * @param strContent  Contenu du tag
     * @param attributes  tableau contenant les attributs au format 'attrName' => 'attrValue' si non null par défaut
     */
    public function addElement($strName, $strContent, $attributes = null) {
        $this->elements[$strName]['name']       = $strName;
        $this->elements[$strName]['content']    = $strContent;
        $this->elements[$strName]['attributes'] = $attributes;
    }

    /**
     * Ajout des éléments par tableau
     * Chaque élément du tableau eltArray est ajouté à la collection en appelant addElement()
     *
     * @param eltArray  tableau au format 'tagName' => 'tagContent'
     */
    public function addElementArray($eltArray) {
        if(!is_array($eltArray)) 
            return;
        foreach ($eltArray as $strName => $strContent) {
            $this->addElement($strName, $strContent);
        }
    }

    /**
     * Retourne la collection des éléments
     * @return array
     */
    public function getElements() {
        return $this->elements;
    }

    /**
     * Mémorise l'élément 'description'
     *
     * @param strDesc  Valeur du tag description
     */
    public function setDescription($strDesc) {
        $tag = ($this->version == ALK_ATOM ? 'summary' : 'description');
        $this->addElement($tag, $strDesc);
    }

    /**
     * Mémorire l'élément 'title'
     *
     * @param strTitle  valeur du titre
     */
    public function setTitle($strTitle) {
        $this->addElement('title', $strTitle);
    }

    /**
     * Mémorise l'élément 'date'
     *
     * @param dDate     valeur de la date (timestamp ou chaine)
     * @param strFormat format de la date passée en paramètre (pris en compte si dDate n'est pas numérique)
     *                  = chaine vide par défaut pour détection auto par strtotime() (fonctionne avec format anglais uniquement)
     *                  le format utilisé est celui de la fonction strftime()
     */
    public function setDate($dDate, $strFormat="") {
        $iDate = $dDate;
        if(!is_numeric($dDate)) {
            if($strFormat == "") {
                $iDate = strtotime($dDate);
            } else {
                $tabDate = strptime($dDate, $strFormat);
                if(is_array($tabDate)) {
                    $iDate = mktime($tabDate["tm_hour"], $tabDate["tm_min"],$tabDate["tm_sec"],
                        1 + $tabDate["tm_mon"], $tabDate["tm_mday"], 1900 + $tabDate["tm_year"]);
                } else {
                    $iDate = 0;
                }
            }
        }

        if($this->version == ALK_ATOM) {
            $tag    = 'updated';
            $value  = date(ALK_DATE_ATOM, $iDate);
        }
        elseif($this->version == ALK_RSS2) {
            $tag    = 'pubDate';
            $value  = date(ALK_DATE_RSS, $iDate);
        }
        else {
            $tag    = 'dc:date';
            $value  = date("Y-m-d", $iDate);
        }

        $this->addElement($tag, $value);
    }

    /**
     * Mémorise l'élémént 'link'
     *
     * @param strLink  valeur du tag
     */
    public function setLink($strLink) {
        $strLink = mb_ereg_replace("&", "&amp;", $strLink);
        if($this->version == ALK_RSS2 || $this->version == ALK_RSS1) {
            $this->addElement('link', $strLink);
        }
        else {
            $this->addElement('link','',array('href' => $strLink));
            $this->addElement('id', ALKFSyndXmlWriter::uuid($strLink,'urn:uuid:'));
        }
    }

    /**
     * Mémorise l'élément 'encloser' uniquement pour RSS 2.0
     *
     * @param url     attribut url du tag encloser
     * @param length  attribut longueur du tag encloser
     * @param type    attribut type du tag encloser
     * @param name    attribut optionnel, permettant de donner un intitulé à l'url
     */
    public function setEncloser($url, $length, $type, $name="") {
        $url = mb_ereg_replace("&", "&amp;", $url);
        $attributes = array('url' => $url, 'length' => $length, 'type' => str_replace("\"", "'", $type));
        if($name != "") {
            $attributes['name'] = $name;
        }
        $this->addElement('enclosure','',$attributes);
    }
}
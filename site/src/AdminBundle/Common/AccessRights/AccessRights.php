<?php
namespace ProdigeCatalogue\AdminBundle\Common\AccessRights;

/*
	if (!isset($AdminPath))
		  $AdminPath = realpath(dirname(__FILE__))."/../../";
	require_once($AdminPath."Ressources/Administration/Ressources.php");
	require_once($AdminPath."DAO/DAO/DAO.php");
	require_once($AdminPath."Modules/BO/DomaineVO.php");
	require_once($AdminPath."Modules/BO/GrpRegroupeUsrVO.php");
	require_once($AdminPath."Modules/BO/UtilisateurVO.php");
	require_once($AdminPath."Modules/BO/SousDomaineVO.php");
	require_once($AdminPath."../include/ClassUser.php");
*/

use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\Common\Modules\BO\UtilisateurVO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GrpRegroupeUsrVO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\DomaineVO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\SousDomaineVO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\TraitementVO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\PerimetreVO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CompetenceVO;
use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;

/**
 * @class AccessRights
 * @brief Gère les droits des utilisateurs
 * @author Alkante
 *
 */
class AccessRights
{
    protected $UserName="";
    protected $Userid="";
    protected $PKUser;
    protected $ProfileList = array();
    protected $SSDOMAdminList = array();
    protected $IsADM_PRODIGE_GROUP;

    /**
     * Constructeur de la classe
     * @return unknown_type
     */
    public function __construct( )
    {
        //if (!isset($AdminPath) && isset($GLOBALS['AdminPath']))
        //	$AdminPath = $GLOBALS['AdminPath'];
        $user = User::GetUser();
        $this->Userid = $user->GetLogin();
        $this->UserName = $user->GetNom();
        $this->IsADM_PRODIGE_GROUP = false;

        $this->PKUser = User::GetUserId();

        //$this->FillUser(); // pas besoin de le refaire, déjà fait dans User !
        // user profiles
        $this->FillProfileList();
    }

    /**
     * @brief Méthode qui renvoie l'identifiant pk de l'utilisateur
     * @return pk de l'utilisateur
     */
    public function GetUserPK()
    {
        return $this->PKUser;
    }

    /**
     * @brief Méthode qui renvoie l'identifiant id de l'utilisateur
     * @return id de l'utilisateur
     */
    public function GetUserId()
    {
        return $this->Userid;
    }

    /**
     * @brief Méthode qui renvoie le nom de l'utilisateur
     * @return nom de l'utilisateur
     */
    public function GetUserName()
    {
        return $this->UserName;
    }

    /**
     * @brief Méthode qui renvoie le premier profil de l'utilisateur
     * @return premier profil de l'utilisateur
     */
    public function GetFirstProfile()
    {
        if ( count( $this->ProfileList ) > 0 )
            return $this->ProfileList[0];
    }

    /**
     * @brief Méthode qui renvoie vrai si l'utilisateur est administrateur PRODIGE
     * @return boolean
     */
    public function IsProdigeAdmin()
    {
        return $this->IsADM_PRODIGE_GROUP;
    }

    /**
     * @brief Méthode qui renvoie vrai si l'utilisateur est dans la liste des profils
     * @param $pkvalue : pk du profil
     * @return boolean
     */
    private function IsInProfileList( $pkvalue )
    {
        foreach( $this->ProfileList as $key=>$column)
        {
            if ( $pkvalue == $column )
                return true;
        }
    }

    /**
     * @brief Méthode qui renvoie vrai si l'utilisateur a les droits d'administration sur le groupe passé
     * en paramètres
     * @param $pkvalue : pk du groupe
     * @return boolean
     */
   public function HasAdminOnGroup($pkvalue){
     $user = User::GetUser();
     if ($user->HasAdminGroup(PRO_TRT_TYPE_ADMINISTRTATION, PRO_OBJET_TYPE_GROUP, $pkvalue))
       return true;
     return false;
   }

    /**
     * @brief Méthode qui renvoie vrai si l'utilisateur a les droits d'administration sur le groupe passé
     * en paramètres
     * @param $pkvalue : pk du groupe
     * @return boolean
     */
    public function IsAdminOfSSDOM( $pkvalue )
    {
        // SSDom administrated by all user profiles
        if ( count( $this->SSDOMAdminList ) == 0 )
            $this->FillSSDOMAdminList();

        foreach( $this->SSDOMAdminList as $key=>$column)
        {
            if ( $pkvalue == $column )
                return true;
        }
        return false;
    }

    /**
     * @deprecated vlc pourquoi recharger le user avec une requête alors que c'est fait dans la classe User
     * 
     * @brief Méthode utilisée pour initialiser un utilisateur
     * @return 
     */
    private function FillUser()
    {
      /*
        $utilisateurVO = new UtilisateurVO();
        $utilisateurVO->AddRestriction( UtilisateurVO::$USR_ID, $this->Userid );

        $utilisateurVO->Open();
        $utilisateurVO->GetResultSet()->First();

        $this->PKUser = $utilisateurVO->Read( UtilisateurVO::$PK_UTILISATEUR );

        if ( $this->PKUser == "" )
            $this->PKUser = -1;
        */
    }

    /**
     * @brief Méthode utilisée pour initialiser une liste de profils
     * @return 
     */
    private function FillProfileList()
    {
        $grpRegroupeUsrVO = new GrpRegroupeUsrVO();
        $grpRegroupeUsrVO->setDao(User::getCatalogueDao());
        $grpRegroupeUsrVO->AddRestriction( GrpRegroupeUsrVO::$GRPUSR_FK_UTILISATEUR, $this->PKUser );
        $grpRegroupeUsrVO->Open();
        $grpRegroupeUsrVO->GetResultSet()->First();

        $user = User::GetUser();
        if ($user->HasTraitement("ADMINISTRATION"))
            $this->IsADM_PRODIGE_GROUP = true;
        while ( !$grpRegroupeUsrVO->GetResultSet()->EOF() )
        {
            $this->ProfileList[]= $grpRegroupeUsrVO->Read( GrpRegroupeUsrVO::$GRPUSR_FK_GROUPE_PROFIL );
            $grpRegroupeUsrVO->GetResultSet()->Next();
        }
    }

    /** 
     * @brief Get all administrated sub domains of all group of the current user.
     * @return unknown_type
     */
    private function FillSSDOMAdminList()
    {
        $sousDomaineVO = new SousDomaineVO();
        $sousDomaineVO->setDao(User::getCatalogueDao());

        foreach( $this->ProfileList as $key=>$column)
        {
            $sousDomaineVO->AddRestriction( SousDomaineVO::$SSDOM_ADMIN_FK_GROUPE_PROFIL, $column );
        }

        $sousDomaineVO->Open();
        $sousDomaineVO->GetResultSet()->First();

        while ( !$sousDomaineVO->GetResultSet()->EOF() )
        {
            $this->SSDOMAdminList[]= $sousDomaineVO->Read( SousDomaineVO::$PK_SOUS_DOMAINE );
            $sousDomaineVO->GetResultSet()->Next();
        }
    }

    /**
     * @brief Méthode qui renvoie vrai si l'utilisateur peut insérer des données.
     * @param $vo : Object
     * @return true
     */
    public function CanInsert( $vo )
    {
        $clazz = "";
        $clazz = new \ReflectionClass($vo); //get_class( $vo );
        $clazz = $clazz->getShortName();
        
        switch( $clazz )
        {
            case "GroupeProfilVO":
                // ne peut pas insérer de nouveaux profils si Prodige est intégrée dans une application externe
      if ( defined("PRO_INCLUDED") && PRO_INCLUDED )
        return false;
      // only the prodige admin can insert
      if ( $this->IsProdigeAdmin() )
                    return true;
                else
                    return false;
                break;
            case "StructureVO":    
                if ( defined("PRO_INCLUDED") && PRO_INCLUDED )
                return false;    
                if ( $this->IsProdigeAdmin() )
                    return true;
                else
                    return false;
            break;                
            // nobody can insert an action
            case "TraitementVO":
                    return false;
            break;
            case "UtilisateurVO":
      // ne peut pas insérer de nouveaux utilisateurs si Prodige est intégrée dans une application externe
      if ( defined("PRO_INCLUDED") && PRO_INCLUDED )
        return false;
                if ( $this->IsProdigeAdmin() )
                    return true;
                else
                    return true;
            break;
            case "DomaineVO":
                return true;
            break;
            case "CoucheDonneesVO":
                return false;
            break;
            case "CarteProjetVO":
                return false;
            break;
            case "SousDomaineVO":
                return true;
            break;
        }

        return true;
    }

    /**
     * @brief Méthode qui renvoie vrai si l'utilisateur peut mettre à jour des données.
     * @param $vo : object
     * @param $ordinal
     * @param $PK : clé primaire
     * @return boolean
     */
    public function CanUpdate( $vo, $ordinal, $PK )
    {
        $clazz = "";
        $clazz = new \ReflectionClass($vo); //get_class( $vo );
        $clazz = $clazz->getShortName();
        
        switch( $clazz )
        {
            case "GroupeProfilVO":
                // only the prodige admin can insert
                if ( $this->IsProdigeAdmin() )
                    return true;
                else
                {
                    return false;
                }
                break;
            case "UtilisateurVO":
                if ($this->IsProdigeAdmin())
                    return true;

                // Only the password can be modified by the user himself or prodige admin.
                if (($ordinal == UtilisateurVO::$USR_PASSWORD) || ($ordinal == UtilisateurVO::$USR_TELEPHONE2) || ($ordinal == UtilisateurVO::$USR_PWD_EXPIRE))
                {
                    if ( $this->IsProdigeAdmin() || ( $this->GetUserId() == $this->Userid ) )
                        return true;
                    else
                        return false;
                }
                else
                    return false;
                break;
            case "TraitementVO":
                if ( $this->IsProdigeAdmin() )
                    return true;
                break;
            case "CompetenceVO":
                if ( $this->IsProdigeAdmin() )
                    return true;
                break;
            case "ZonageVO":
                if ( $this->IsProdigeAdmin() ){
                    return true;
                }

                break;
            case "PerimetreVO":
                if ( $this->IsProdigeAdmin() )
                    return true;
                break;
            case "RubricVO":
                if ( $this->IsProdigeAdmin() )
                    return true;
                break;
            case "RubricAideVO":
                if ( $this->IsProdigeAdmin() )
                    return true;
                break;
            
            case "RubricAideAccedeProfilVO":
                if ( $this->IsProdigeAdmin() )
                    return true;
                break;
            case "StructureVO":
                if ( $this->IsProdigeAdmin() ){
                    return true;
                }
                break;
            case "StructureAccedeUserVO":
                if ( defined("PRO_INCLUDED") && PRO_INCLUDED )
                return false;
                if ( $this->IsProdigeAdmin() ){
                    return true;
                }
                break;
            case "TrtObjetVO":
                if ( $this->IsProdigeAdmin() )
                    return true;
                break;
            case "DomaineVO":
                if ( $PK == Ressources::$NEWROW )
                    return $this->CanInsert($vo);

                if ( $this->IsProdigeAdmin() )
                {
                    return true;
                }

                return false;
                break;
                case "CoucheDonneesVO":
                    if ( $this->IsProdigeAdmin() )
                        return true;

                    if ( $PK == Ressources::$NEWROW )
                        return $this->CanInsert($vo);

                    return false;
                break;
                case "CarteProjetVO":
                    if ( $this->IsProdigeAdmin() )
                        return true;

                    if ( $PK == Ressources::$NEWROW )
                        return $this->CanInsert($vo);

                    return false;
                break;
            case "SousDomaineVO":
                // new row always ok
                if ( $PK == Ressources::$NEWROW )
                        return $this->CanInsert($vo);

                if ( !$this->IsProdigeAdmin() && $ordinal != SousDomaineVO::$SSDOM_FK_DOMAINE )
                {
                    // OK if we are the creator
                    $sousDomaineVO = new SousDomaineVO();
                    $sousDomaineVO->setDao(User::getCatalogueDao());
                    $sousDomaineVO->AddRestriction( SousDomaineVO::$PK_SOUS_DOMAINE, $PK );
                    $sousDomaineVO->Open();
                    $sousDomaineVO->GetResultSet()->First();

                    if ( $sousDomaineVO->Read( SousDomaineVO::$SSDOM_CREATEUR_FK_GROUPE_PROFIL ) == $this->GetFirstProfile() )
                        return true;

                    // OK if current user admin of the subdomain
                    if ( $this->IsAdminOfSSDOM( $PK ) )
                    {
                        return true;
                    }
                }

                if ( $this->IsProdigeAdmin() ){
                    if ( $ordinal == SousDomaineVO::$SSDOM_ADMIN_FK_GROUPE_PROFIL ){
                        return true;
                    }

                    if ( $ordinal == SousDomaineVO::$SSDOM_FK_DOMAINE )
                    {
                        return $this->CanUpdate( new DomaineVO(), DomaineVO::$PK_DOMAINE, $PK );
                    }

                        $sousDomaineVO = new SousDomaineVO();
                        $sousDomaineVO->setDao(User::getCatalogueDao());
                        $sousDomaineVO->AddRestriction( SousDomaineVO::$PK_SOUS_DOMAINE, $PK );
                        $sousDomaineVO->Open();
                        $sousDomaineVO->GetResultSet()->First();
                        $myDomainPK = $sousDomaineVO->Read( SousDomaineVO::$SSDOM_FK_DOMAINE );

                }
                //ok si l'utilisateur a les droits d'administration sur le groupe
                if ($this->HasAdminOnGroup($PK)){
                    return true;
                }

                return true;

                break;
            case "SSDomDisposeCoucheVO":
                return true;
                break;
            case "SSDomVisualiseCarteVO":
                return true;
                break;
            case "GrpAutoriseTrtVO":
                return $this->CanUpdate( new TraitementVO(), $ordinal, $PK );
                break;
            case "GrpRegroupeUsrVO":
                return $this->CanUpdate( new UtilisateurVO(), $ordinal, $PK );
                break;
            case "GrpAccedeDomVO":
                if ($this->HasAdminOnGroup($PK)){
                    return true;
                }
                return $this->CanUpdate( new DomaineVO(), $ordinal, $PK );
                break;
            case "GrpAccedeSSDomVO":
                return true;
                break;
            case "UsrAccedePerimetreVO":
                return $this->CanUpdate( new PerimetreVO(), $ordinal, $PK );
                break;
            case "UsrAccedePerimetreEditionVO":
                return $this->CanUpdate( new PerimetreVO(), $ordinal, $PK );
                break;
            case "UsrAlertePerimetreEditionVO":
                return $this->CanUpdate( new PerimetreVO(), $ordinal, $PK );
                break;
            case "GrpAccedeCompetenceVO":
                return $this->CanUpdate( new CompetenceVO(), $ordinal, $PK );
                break;
            case "CompetenceAccedeCoucheVO":
                return $this->CanUpdate( new CompetenceVO(), $ordinal, $PK );
                break;
            case "CompetenceAccedeCarteVO":
                return $this->CanUpdate( new CompetenceVO(), $ordinal, $PK );
                break;
            case "GrpTrtObjetVO"://vérification faite en amont
                return true;
                break;
        }

        return false;
    }

    /**
     * @brief Méthode qui renvoie vrai si l'utilisateur peut effacer des données.
     * @param $vo : object
     * @param $ordinal
     * @param $PK : clé primaire
     * @return boolean
     */
    public function CanDelete( $vo, $ordinal, $PK )
    {
        return $this->CanUpdate( $vo, $ordinal, $PK );
    }

    /**
     * @brief Méthode qui renvoie vrai si l'utilisateur peut voir le mot de passe d'un autre utilisateur.
     * @param $UserPK : pk de l'utilisteur
     * @return boolean
     */
    public function CanSeePassword( $UserPK )
    {
        return true;
    }

    // ##############################################
    // ################## DOMAINS ###################
    // ##############################################

    /**
     * @brief Méthode qui renvoie vrai si l'utilisateur peut créer des domaines.
     * @return boolean
     */
    public function CanCreateDomains()
    {
        return true;
    }

    /**
     * @brief Méthode qui renvoie vrai si l'utilisateur peut mettre à jour le domaine ayant pour clé $domainPK.
     * @param $domainPK
     * @return boolean
     */
    public function CanUpdateDomains( $domainPK )
    {
        return false;
    }

    /**
     * @brief Méthode qui renvoie vrai si l'utilisateur peut mettre le domaine ayant pour clé $domainPK et qui est dans
     * le cadre de cohérence.
     * @param $domainPK
     * @return boolean
     */
    public function CanUpdateCoherenceCB( $domainPK )
    {
        return false;
    }

}
?>
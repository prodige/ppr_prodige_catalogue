<?php
  namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

/**
   * @class UtilisateurVO
   * @brief  Classe de gestion des Utilisateurs
   * @author Alkante
   */

    /*if(!isset($AdminPath)) {
        $AdminPath = "../../";
    }*/

    //require_once($AdminPath.'DAO/ViewObject/ViewObject.php');
    use Prodige\ProdigeBundle\DAOProxy\ViewObject;

    class UtilisateurVO extends ViewObject {
        static public $PK_UTILISATEUR = 0;
        static public $TS = 1;
        static public $USR_ID = 2;
        static public $USR_NOM = 3;
        static public $USR_PRENOM = 4;
        static public $USR_EMAIL = 5;
        static public $USR_TELEPHONE = 6;
        static public $USR_TELEPHONE2 = 7;
        static public $USR_SERVICE = 8;
        static public $USR_DESCRIPTION = 9;
        static public $USR_PASSWORD = 10;
        static public $USR_PWD_EXPIRE = 11;
        static public $USR_GENERIC = 12;
        static public $USR_LDAP = 13;
        static public $USR_SIGNATURE = 14;
        static public $USR_ACCOUNT_EXPIRE = 15;

        public function __construct( ) {
            $this->AddProjection(UtilisateurVO::$PK_UTILISATEUR, "UTILISATEUR", "PK_UTILISATEUR");
            $this->AddProjection(UtilisateurVO::$TS, "UTILISATEUR", "TS");
            $this->AddProjection(UtilisateurVO::$USR_ID, "UTILISATEUR", "USR_ID");
            $this->AddProjection(UtilisateurVO::$USR_NOM, "UTILISATEUR", "USR_NOM");
            $this->AddProjection(UtilisateurVO::$USR_PRENOM, "UTILISATEUR", "USR_PRENOM");
            $this->AddProjection(UtilisateurVO::$USR_EMAIL, "UTILISATEUR", "USR_EMAIL");
            $this->AddProjection( UtilisateurVO::$USR_TELEPHONE, "UTILISATEUR", "USR_TELEPHONE" );
            $this->AddProjection( UtilisateurVO::$USR_TELEPHONE2, "UTILISATEUR", "USR_TELEPHONE2" );
            $this->AddProjection( UtilisateurVO::$USR_SERVICE, "UTILISATEUR", "USR_SERVICE" );
            $this->AddProjection( UtilisateurVO::$USR_DESCRIPTION, "UTILISATEUR", "USR_DESCRIPTION" );
            $this->AddProjection( UtilisateurVO::$USR_PASSWORD, "UTILISATEUR", "USR_PASSWORD" );
            $this->AddProjection( UtilisateurVO::$USR_PWD_EXPIRE, "UTILISATEUR", "USR_PWDEXPIRE" );
            $this->AddProjection( UtilisateurVO::$USR_GENERIC, "UTILISATEUR", "USR_GENERIC" );
            $this->AddProjection( UtilisateurVO::$USR_LDAP, "UTILISATEUR", "USR_LDAP" );
            $this->AddProjection( UtilisateurVO::$USR_SIGNATURE, "UTILISATEUR", "USR_SIGNATURE" );
            $this->AddProjection( UtilisateurVO::$USR_ACCOUNT_EXPIRE, "UTILISATEUR", "DATE_EXPIRATION_COMPTE" );

            $this->NewRowSequence = "SEQ_UTILISATEUR";

            $this->AddOrder(UtilisateurVO::$USR_ID);
        }
        
        /**
         * Force la synchronisation avec le LDAP edit/add
         * @param type $conn
         * @param array $ldapConf
         * @param array $ordinalList
         * @param array $valueList
         * @param string $pk
         * @param bool $delete
         */
        public function ldapSyncAction($pk,$delete = false){
            $conn = $this->getDao()->getConnection();
            include_once(__DIR__.'/../../../Resources/templates/Administration/Users/UsersSyncLdap.php');
        }
        
        /**
         * Force la synchronisation avec le LDAP edit/add dans le cadre d'une selection
         * @param type $conn
         * @param array $ldapConf
         * @param array $ordinalList
         * @param array $valueList
         * @param string $pk
         * @param bool $delete
         */
        public function ldapSyncSelectionAction($pk,$delete = false){
            $this->ldapSyncAction($pk, $delete);
        }
    }
?>

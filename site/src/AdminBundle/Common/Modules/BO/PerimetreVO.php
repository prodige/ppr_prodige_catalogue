<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

/**
   * @class PerimetreVO
   * @brief  Classe de gestion des compétences
   */
  /*if (!isset($AdminPath))
    $AdminPath = "../../";

  require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
  require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

  class PerimetreVO extends ViewObject
  {
    static public $PK_PERIMETRE_ID = 0;
    static public $PERIMETRE_CODE = 1;
    static public $PERIMETRE_NOM = 2;
    static public $PERIMETRE_ZONAGE_NOM = 3;
    static public$PERIMETRE_ZONAGE_ID = 4;
    static public $ZONAGE_ID = 5;
    
    
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( PerimetreVO::$PK_PERIMETRE_ID, "PERIMETRE", "PK_PERIMETRE_ID" );
      $this->AddProjection( PerimetreVO::$PERIMETRE_CODE, "PERIMETRE", "PERIMETRE_CODE" );
      $this->AddProjection( PerimetreVO::$PERIMETRE_NOM, "PERIMETRE", "PERIMETRE_NOM  ||' (' || PERIMETRE_CODE || ')'"  );
      $this->AddProjection( PerimetreVO::$PERIMETRE_ZONAGE_NOM, "ZONAGE", "ZONAGE_NOM" );
      $this->AddProjection( PerimetreVO::$PERIMETRE_ZONAGE_ID, "PERIMETRE", "PERIMETRE_ZONAGE_ID" );
      $this->AddProjection( PerimetreVO::$ZONAGE_ID, "ZONAGE", "PK_ZONAGE_ID" );
      
      $ordinalColumnLeft=PerimetreVO::$PERIMETRE_ZONAGE_ID;
      $ordinalColumnRight=PerimetreVO::$ZONAGE_ID;
      
      $this->AddEqualsRelation( $ordinalColumnLeft, $ordinalColumnRight );
      
      $this->NewRowSequence = "SEQ_PERIMETRE";
      $this->AddOrder(PerimetreVO::$PERIMETRE_NOM);
    
    }

  }
?>

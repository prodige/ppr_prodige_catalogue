<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

/**
   * @class ZonageVO
   * @brief  Classe de gestion des compétences
   */
  /*if (!isset($AdminPath))
    $AdminPath = "../../";

  require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
  require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;
use Symfony\Component\HttpFoundation\Response;
 
  class ZonageVO extends ViewObject
  {
    static public $PK_ZONAGE_ID = 0;
    static public $ZONAGE_NOM = 1;
    static public $ZONAGE_MINIMAL = 2;
    static public $ZONAGE_FIELD_ID = 3;
    static public $ZONAGE_FIELD_NAME = 4;
    static public $ZONAGE_FK_COUCHE_DONNEES = 5;
    static public $ZONAGE_FIELD_LIAISON = 6;
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( ZonageVO::$PK_ZONAGE_ID, "ZONAGE", "PK_ZONAGE_ID" );
      $this->AddProjection( ZonageVO::$ZONAGE_NOM, "ZONAGE", "ZONAGE_NOM" );
      $this->AddProjection( ZonageVO::$ZONAGE_MINIMAL, "ZONAGE", "ZONAGE_MINIMAL" );
      $this->AddProjection( ZonageVO::$ZONAGE_FIELD_ID, "ZONAGE", "ZONAGE_FIELD_ID" );
      $this->AddProjection( ZonageVO::$ZONAGE_FIELD_NAME, "ZONAGE", "ZONAGE_FIELD_NAME" );
      $this->AddProjection( ZonageVO::$ZONAGE_FK_COUCHE_DONNEES, "ZONAGE", "ZONAGE_FK_COUCHE_DONNEES" );
      $this->AddProjection( ZonageVO::$ZONAGE_FIELD_LIAISON, "ZONAGE", "ZONAGE_FIELD_LIAISON" );
      
      $this->NewRowSequence = "SEQ_ZONAGE";
      $this->AddOrder(ZonageVO::$ZONAGE_NOM);
    
    }
    
    /**
     * Synchronisation des périmètres depuis le service admincarto
     * @return unknown_type
     */
    public function afterSaveAction($controller, $conn, $delete=false){
        if ( $delete ) {
            return new Response("");
        }
        //global $AdminPath;
        //include_once($AdminPath."Administration/Perimetres/PerimetreSynchronisation.php");
        include_once(__DIR__.'/../../../Resources/templates/Administration/Perimetres/PerimetreSynchronisation.php');
        return new Response("");
    }
  }
?>

<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

	/**
   * @class CarteProjetVO
   * @brief  Classe de gestion des cartes
   * @author Alkante
   */
  /*if (!isset($AdminPath))
    $AdminPath = "../../";
require_once($AdminPath."/DAO/ViewObject/ViewObject.php");*/
use Prodige\ProdigeBundle\DAOProxy\ViewObject;

	class CarteProjetVO extends ViewObject
	{
		static public $PK_CARTE_PROJET = 0;
		static public $TS = 1;
		static public $CARTP_ID = 2;
		static public $CARTP_NOM = 3;
		static public $CARTP_DESCRIPTION = 4;
		static public $CARTP_FORMAT = 5;
		static public $CARTP_FK_STOCKAGE_CARTE = 6;
		static public $CARTP_FK_FICHE_METADONNEES = 7;

		public function __construct( )
		{
			$this->AddProjection( CarteProjetVO::$PK_CARTE_PROJET, "CARTE_PROJET", "PK_CARTE_PROJET" );
			$this->AddProjection( CarteProjetVO::$TS, "CARTE_PROJET", "TS" );
			$this->AddProjection( CarteProjetVO::$CARTP_ID, "CARTE_PROJET", "CARTP_ID" );
			$this->AddProjection( CarteProjetVO::$CARTP_NOM, "CARTE_PROJET", "CARTP_NOM" );
			$this->AddProjection( CarteProjetVO::$CARTP_DESCRIPTION, "CARTE_PROJET", "CARTP_DESCRIPTION" );
			$this->AddProjection( CarteProjetVO::$CARTP_FORMAT, "CARTE_PROJET", "CARTP_FORMAT" );
			$this->AddProjection( CarteProjetVO::$CARTP_FK_STOCKAGE_CARTE, "CARTE_PROJET", "CARTP_FK_STOCKAGE_CARTE" );
			$this->AddProjection( CarteProjetVO::$CARTP_FK_FICHE_METADONNEES, "CARTE_PROJET", "CARTP_FK_FICHE_METADONNEES" );

			$this->NewRowSequence = "SEQ_CARTE_PROJET";
      $this->AddOrder(CarteProjetVO::$CARTP_NOM);
		}
	}
?>

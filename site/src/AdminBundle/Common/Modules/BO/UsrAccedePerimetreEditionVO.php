<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

  /**
   * @class UsrAccedePerimetreVO
   * @brief  Classe de gestion des compétences de groupes
   */
//require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
use Prodige\ProdigeBundle\DAOProxy\ViewObject;

class UsrAccedePerimetreEditionVO extends ViewObject
{
  static public $PK_USR_ACCEDE_PERIMETRE_EDITION = 0;
  static public $USRPERIM_FK_UTILISATEUR = 1;
  static public $USRPERIM_FK_PERIMETRE = 2;
  
  static public $USRACCPERIMETRE_PERIMETRE_ID = 3;
  static public $USRACCPERIMETRE_PERIMETRE_NOM = 4;
  
  static public $USRACCPERIMETRE_UTILISATEUR = 5;
  static public $USRACCPERIMETRE_USR_ID = 6;
  
  /**
   * @brief constructeur
   */
  public function __construct( )
  {
    $this->AddProjection( UsrAccedePerimetreEditionVO::$PK_USR_ACCEDE_PERIMETRE_EDITION, "USR_ACCEDE_PERIMETRE_EDITION", "PK_USR_ACCEDE_PERIMETRE_EDITION" );
    $this->AddProjection( UsrAccedePerimetreEditionVO::$USRPERIM_FK_UTILISATEUR, "USR_ACCEDE_PERIMETRE_EDITION", "USRPERIM_FK_UTILISATEUR" );
    $this->AddProjection( UsrAccedePerimetreEditionVO::$USRPERIM_FK_PERIMETRE, "USR_ACCEDE_PERIMETRE_EDITION", "USRPERIM_FK_PERIMETRE" );
    
    // linked fields.
    $this->AddProjection( UsrAccedePerimetreEditionVO::$USRACCPERIMETRE_PERIMETRE_ID, "PERIMETRE", "PK_PERIMETRE_ID" );
    $this->AddProjection( UsrAccedePerimetreEditionVO::$USRACCPERIMETRE_PERIMETRE_NOM, "PERIMETRE", "PERIMETRE_NOM  ||' (' || PERIMETRE_CODE || ')'" );
    $this->AddProjection( UsrAccedePerimetreEditionVO::$USRACCPERIMETRE_UTILISATEUR, "UTILISATEUR", "PK_UTILISATEUR");
    $this->AddProjection( UsrAccedePerimetreEditionVO::$USRACCPERIMETRE_USR_ID, "UTILISATEUR", "USR_ID");
    
    // relastionship building
    $this->AddEqualsRelation( UsrAccedePerimetreEditionVO::$USRPERIM_FK_PERIMETRE, UsrAccedePerimetreEditionVO::$USRACCPERIMETRE_PERIMETRE_ID );
    $this->AddEqualsRelation( UsrAccedePerimetreEditionVO::$USRPERIM_FK_UTILISATEUR, UsrAccedePerimetreEditionVO::$USRACCPERIMETRE_UTILISATEUR );
  }
}

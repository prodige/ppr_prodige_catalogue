<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

	/**
   * @class GrpAccedeDomVO
   * @brief  Classe de gestion des domaines de groupes
   * @author Alkante
   */
//require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
use Prodige\ProdigeBundle\DAOProxy\ViewObject;

	class GrpAccedeDomVO extends ViewObject
	{
		static public $PK_GRP_ACCEDE_DOM = 0;
		static public $TS = 1;
		static public $GRPDOM_FK_DOMAINE = 2;
		static public $GRPDOM_FK_GROUPE_PROFIL = 3;
		
		static public $GRPDOM_PK_DOMAINE = 4;
		static public $GRPDOM_DOM_ID = 5;
    static public $GRPDOM_DOM_NOM = 6;
		
		static public $GRPDOM_PK_GROUPE_PROFIL = 7;
		static public $GRPDOM_GRP_ID = 8;
    static public $GRPDOM_GRP_NOM = 9;
		
		public function __construct( )
		{
			$this->AddProjection( GrpAccedeDomVO::$PK_GRP_ACCEDE_DOM, "GRP_ACCEDE_DOM", "PK_GRP_ACCEDE_DOM" );
			$this->AddProjection( GrpAccedeDomVO::$TS, "GRP_ACCEDE_DOM", "TS" );
			$this->AddProjection( GrpAccedeDomVO::$GRPDOM_FK_DOMAINE, "GRP_ACCEDE_DOM", "GRPDOM_FK_DOMAINE" );
			$this->AddProjection( GrpAccedeDomVO::$GRPDOM_FK_GROUPE_PROFIL, "GRP_ACCEDE_DOM", "GRPDOM_FK_GROUPE_PROFIL" );
			
			// linked fields.
			$this->AddProjection( GrpAccedeDomVO::$GRPDOM_PK_DOMAINE, "DOMAINE", "PK_DOMAINE" );
      $this->AddProjection( GrpAccedeDomVO::$GRPDOM_DOM_ID, "DOMAINE", "DOM_ID" );
      $this->AddProjection( GrpAccedeDomVO::$GRPDOM_DOM_NOM, "DOMAINE", "DOM_NOM" );
      
			$this->AddProjection( GrpAccedeDomVO::$GRPDOM_PK_GROUPE_PROFIL, "GROUPE_PROFIL", "PK_GROUPE_PROFIL");
			$this->AddProjection( GrpAccedeDomVO::$GRPDOM_GRP_ID, "GROUPE_PROFIL", "GRP_ID");
      $this->AddProjection( GrpAccedeDomVO::$GRPDOM_GRP_NOM, "GROUPE_PROFIL", "GRP_NOM");
			
			// relastionship building
			$this->AddEqualsRelation( GrpAccedeDomVO::$GRPDOM_FK_DOMAINE, GrpAccedeDomVO::$GRPDOM_PK_DOMAINE );
			$this->AddEqualsRelation( GrpAccedeDomVO::$GRPDOM_FK_GROUPE_PROFIL, GrpAccedeDomVO::$GRPDOM_PK_GROUPE_PROFIL );
		}
	}
?>

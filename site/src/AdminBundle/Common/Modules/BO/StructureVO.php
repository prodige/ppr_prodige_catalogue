<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

/**
   * @class StructureVO
   * @brief  Classe de gestion des structures
   */
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;

  class StructureVO extends ViewObject
  {    
    const TABLE_NAME = "structure";
    const ID = 0;
    const NAME = 1;
    const SIGLE = 2;
    const SIREN = 3;
    const SIRET = 4;

    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection(StructureVO::ID,   StructureVO::TABLE_NAME, "pk_structure" );
      $this->AddProjection(StructureVO::NAME,   StructureVO::TABLE_NAME, "structure_nom" );
      $this->AddProjection(StructureVO::SIGLE,   StructureVO::TABLE_NAME, "structure_sigle" );
      $this->AddProjection(StructureVO::SIREN,   StructureVO::TABLE_NAME, "structure_siren" );
      $this->AddProjection(StructureVO::SIRET,   StructureVO::TABLE_NAME, "structure_siret" );
      
      $this->NewRowSequence = "seq_structure";
      $this->AddOrder(StructureVO::ID);
    }

  }
?>

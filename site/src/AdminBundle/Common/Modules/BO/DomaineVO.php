<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

/**
   * @class DomaineVO
   * @brief  Classe de gestion des Domaines
   * @author Alkante
   */
	//require_once($AdminPath."DAO/ViewObject/ViewObject.php");
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;
  use JMS\AopBundle\Exception\Exception;
  use ProdigeCatalogue\AdminBundle\Services\ThesaurusSerializer;

	
	class DomaineVO extends ViewObject
	{
		static public $PK_DOMAINE = 0;
		static public $TS = 1;
		static public $DOM_ID = 2;
		static public $DOM_NOM = 3;
		static public $DOM_DESCRIPTION = 4;
		static public $DOM_RUBRIC_NAME = 5;
		static public $DOM_RUBRIC_ID = 6;
		static public $RUBRIC_ID = 7;
		static public $array_select=array();/*used for select option in html forms*/
		

		public function __construct( )
		{
			$this->AddProjection( DomaineVO::$PK_DOMAINE, "DOMAINE", "PK_DOMAINE" );
			$this->AddProjection( DomaineVO::$TS, "DOMAINE", "TS" );
			$this->AddProjection( DomaineVO::$DOM_ID, "DOMAINE", "DOM_ID" );
			$this->AddProjection( DomaineVO::$DOM_NOM, "DOMAINE", "DOM_NOM" );
			$this->AddProjection( DomaineVO::$DOM_DESCRIPTION, "DOMAINE", "DOM_DESCRIPTION" );
			$this->AddProjection( DomaineVO::$DOM_RUBRIC_NAME, "RUBRIC_PARAM", "RUBRIC_NAME" );
			$this->AddProjection( DomaineVO::$DOM_RUBRIC_ID, "DOMAINE", "DOM_RUBRIC" );
			$this->AddProjection( DomaineVO::$RUBRIC_ID, "RUBRIC_PARAM", "RUBRIC_ID" );
			
			$ordinalColumnLeft=DomaineVO::$RUBRIC_ID;
			$ordinalColumnRight=DomaineVO::$DOM_RUBRIC_ID;
			
			$this->AddEqualsRelation( $ordinalColumnLeft, $ordinalColumnRight );
      $this->AddOrder(DomaineVO::$DOM_ID);
			$this->NewRowSequence = "SEQ_DOMAINE";
		}


                /**
                 * Génération Thesaurus
                 * @return unknown_type
                 */
                public function syncThesaurus($controller){
                    ThesaurusSerializer::getInstance($this->getDao()->getConnection(), $controller->getUnsecureCurlOptions(), THESAURUS_NAME)->generateDocument();
                }
                
		/**
		 * 
		 * {@inheritDoc}
		 * @see \Prodige\ProdigeBundle\DAOProxy\ViewObject::DeleteRow()
		 */
		/*public function DeleteRow($ordinalRestriction, $valueRestriction)
		{
		    $ret = parent::DeleteRow($ordinalRestriction, $valueRestriction);
		    
		    if( true == $ret ) {
		      // TODO synchro avec thesaurus geosource
		      
		    }
		    
		    return $ret;
		}
		
		public function Update($ordinalList, $valueList, $ordinalRestriction, $valueRestriction)
		{
		    //die('marche pas');
		    $ret = parent::Update($ordinalList, $valueList, $ordinalRestriction, $valueRestriction);
		    
		    if( true == $ret ) {
		        // TODO synchro avec thesaurus geosource
		    
		    }
		    
		    return $ret;
		}*/
	}
?>
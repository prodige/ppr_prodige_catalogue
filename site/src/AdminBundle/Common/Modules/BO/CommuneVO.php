<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

	/**
   * @class CommuneVO
   * @brief  Classe de gestion des communes
   * @author Alkante
   */

//require_once($AdminPath."DAO/ViewObject/ViewObject.php");
use Prodige\ProdigeBundle\DAOProxy\ViewObject;
	class CommuneVO extends ViewObject
	{
		static public $PK_COMMUNE = 0;
		static public $TS = 1;
		static public $COMM_CODE_INSEE = 2;
		static public $COMM_NOM = 3;
		static public $COMM_FK_COUCHE_DONNEES = 4;
		static public $COMM_FK_FICHE_METADONNEES = 5;

		public function __construct( )
		{
			$this->AddProjection( CommuneVO::$PK_COMMUNE, "COMMUNE", "PK_COMMUNE" );
			$this->AddProjection( CommuneVO::$TS, "COMMUNE", "TS" );
			$this->AddProjection( CommuneVO::$COMM_CODE_INSEE, "COMMUNE", "COMM_CODE_INSEE" );
			$this->AddProjection( CommuneVO::$COMM_NOM, "COMMUNE", "COMM_NOM" );
			$this->AddProjection( CommuneVO::$COMM_FK_COUCHE_DONNEES, "COMMUNE", "COMM_FK_COUCHE_DONNEES" );
			$this->AddProjection( CommuneVO::$COMM_FK_FICHE_METADONNEES, "COMMUNE", "COMM_FK_FICHE_METADONNEES" );

			$this->NewRowSequence = "SEQ_COMMUNE";
		}

		public function GetFicheMetadonnees()
		{
		}

		public function GetCoucheDonnees()
		{
		}
	}
?>
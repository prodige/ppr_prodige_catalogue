<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

/**
   * @class TrtObjetVO
   * @brief  Classe de gestion des traitements Objet
   */
  /*if (!isset($AdminPath))
    $AdminPath = "../../";
  require_once($AdminPath."DAO/ViewObject/ViewObject.php");
  require_once($AdminPath."Administration/AccessRights/AccessRights.php");*/
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

  class TrtObjetVO extends ViewObject
  {
    static public $PK_TRT_OBJET = 0;
    static public $TRT_ID = 1;
    static public $TRT_NOM = 2;
     
    static public $TRT_AUTORISE_OBJET_TRT_ID = 10;
    static public $TRT_AUTORISE_OBJET_OBJ_TYPE_ID = 11; 
    
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( TrtObjetVO::$PK_TRT_OBJET, "TRT_OBJET", "PK_TRT_OBJET" );
      $this->AddProjection( TrtObjetVO::$TRT_ID, "TRT_OBJET", "TRT_ID" );
      $this->AddProjection( TrtObjetVO::$TRT_NOM, "TRT_OBJET", "TRT_NOM" );
      
           
      $this->AddProjection( TrtObjetVO::$TRT_AUTORISE_OBJET_TRT_ID, "TRT_AUTORISE_OBJET", "TRTAUTOOBJET_FK_TRT_ID");
      $this->AddProjection( TrtObjetVO::$TRT_AUTORISE_OBJET_OBJ_TYPE_ID, "TRT_AUTORISE_OBJET", "TRTAUTOOBJET_FK_OBJ_TYPE_ID");
      
      $this->AddOrder(TrtObjetVO::$TRT_ID);
      $this->AddEqualsRelation( TrtObjetVO::$PK_TRT_OBJET, TrtObjetVO::$TRT_AUTORISE_OBJET_TRT_ID );
      
      $this->NewRowSequence = "SEQ_TRT_OBJET";
    }

  }
?>

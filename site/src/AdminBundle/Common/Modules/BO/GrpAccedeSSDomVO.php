<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

	/**
   * @class GrpAccedeSSDomVO
   * @brief  Classe de gestion des sous-domaine de groupes
   * @author Alkante
   */
//require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
use Prodige\ProdigeBundle\DAOProxy\ViewObject;

	class GrpAccedeSSDomVO extends ViewObject
	{
		static public $PK_GRP_ACCEDE_SSDOM = 0;
		static public $TS = 1;
		static public $GRPSS_FK_SOUS_DOMAINE = 2;
		static public $GRPSS_FK_GROUPE_PROFIL = 3;
		
		static public $GRPSS_PK_SOUS_DOMAINE = 4;
		static public $GRPSS_SSDOM_ID = 5;
		
		static public $GRPSS_PK_GROUPE_PROFIL = 6;
		static public $GRPSS_GRP_ID = 7;
    
    static public $GRPSS_SSDOM_NOM = 8;
		
		public function __construct( $bDomSDomNom=false )
		{
			$this->AddProjection( GrpAccedeSSDomVO::$PK_GRP_ACCEDE_SSDOM, "GRP_ACCEDE_SSDOM", "PK_GRP_ACCEDE_SSDOM" );
			$this->AddProjection( GrpAccedeSSDomVO::$TS, "GRP_ACCEDE_SSDOM", "TS" );
			$this->AddProjection( GrpAccedeSSDomVO::$GRPSS_FK_SOUS_DOMAINE, "GRP_ACCEDE_SSDOM", "GRPSS_FK_SOUS_DOMAINE" );
			$this->AddProjection( GrpAccedeSSDomVO::$GRPSS_FK_GROUPE_PROFIL, "GRP_ACCEDE_SSDOM", "GRPSS_FK_GROUPE_PROFIL" );
			
			// linked fields.
			$this->AddProjection( GrpAccedeSSDomVO::$GRPSS_PK_SOUS_DOMAINE, "SOUS_DOMAINE", "PK_SOUS_DOMAINE" );
			$this->AddProjection( GrpAccedeSSDomVO::$GRPSS_SSDOM_ID, "SOUS_DOMAINE", "SSDOM_ID" );
			$this->AddProjection( GrpAccedeSSDomVO::$GRPSS_PK_GROUPE_PROFIL, "GROUPE_PROFIL", "PK_GROUPE_PROFIL");
			$this->AddProjection( GrpAccedeSSDomVO::$GRPSS_GRP_ID, "GROUPE_PROFIL", "GRP_ID");
			
      if ( $bDomSDomNom ){
        $this->SetDomaineSubDomaineNom();
      }
      
			// relastionship building
			$this->AddEqualsRelation( GrpAccedeSSDomVO::$GRPSS_FK_SOUS_DOMAINE, GrpAccedeSSDomVO::$GRPSS_PK_SOUS_DOMAINE );
			$this->AddEqualsRelation( GrpAccedeSSDomVO::$GRPSS_FK_GROUPE_PROFIL, GrpAccedeSSDomVO::$GRPSS_PK_GROUPE_PROFIL );
		}
    public function SetDomaineSubDomaineNom()
    {
      $this->AddProjection( GrpAccedeSSDomVO::$GRPSS_SSDOM_NOM, "SOUS_DOMAINE", 
              "((CASE WHEN (select DOMAINE.DOM_NOM ||' : ' from DOMAINE where DOMAINE.PK_DOMAINE=SOUS_DOMAINE.SSDOM_FK_DOMAINE) is null" .
              " then '' " .
              " else (select DOMAINE.DOM_NOM ||' : ' from DOMAINE where DOMAINE.PK_DOMAINE=SOUS_DOMAINE.SSDOM_FK_DOMAINE) end) " .
              "||SOUS_DOMAINE.SSDOM_NOM) as DOM_SSDOM_NOM" );
      
      $this->AddOrder(GrpAccedeSSDomVO::$GRPSS_SSDOM_NOM);
    }  
	}

?>

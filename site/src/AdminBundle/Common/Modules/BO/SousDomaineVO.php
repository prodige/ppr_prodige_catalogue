<?php
namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;
use Prodige\ProdigeBundle\DAOProxy\ViewObject;
use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;
use ProdigeCatalogue\AdminBundle\Services\ThesaurusSerializer;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\DBAL\Connection;

/**
 * @class SousDomaineVO
 * @brief  Classe de gestion des SousDomaines
 * @author Alkante
 */
class SousDomaineVO extends ViewObject
{
    static public $PK_SOUS_DOMAINE = 0;
    static public $TS = 1;
    static public $SSDOM_ID = 2;
    static public $SSDOM_NOM = 3;
    static public $SSDOM_DESCRIPTION = 4;
    static public $SSDOM_FK_DOMAINE = 5;
    static public $SSDOM_SERVICE_WMS = 6;
    static public $SSDOM_SERVICE_WMS_UUID = 7;
    static public $SSDOM_ADMIN_FK_GROUPE_PROFIL = 8;
    static public $SSDOM_CREATEUR_FK_GROUPE_PROFIL = 9;
    static public $SSDOM_EDITEUR_FK_GROUPE_PROFIL = 10;

    public $SSDOM_PK_DOMAINE = 11;
    public $SSDOM_DOM_ID = 12;
    public $DOM_SSDOM_NOM = 13;

    public function __construct( $bDomSDomNom=false )
    {
        $this->AddProjection( SousDomaineVO::$PK_SOUS_DOMAINE, "SOUS_DOMAINE", "PK_SOUS_DOMAINE" );
        $this->AddProjection( SousDomaineVO::$TS, "SOUS_DOMAINE", "TS" );
        $this->AddProjection( SousDomaineVO::$SSDOM_ID, "SOUS_DOMAINE", "SSDOM_ID" );
        $this->AddProjection( SousDomaineVO::$SSDOM_NOM, "SOUS_DOMAINE", "SSDOM_NOM" );
        $this->AddProjection( SousDomaineVO::$SSDOM_DESCRIPTION, "SOUS_DOMAINE", "SSDOM_DESCRIPTION" );
        $this->AddProjection( SousDomaineVO::$SSDOM_FK_DOMAINE, "SOUS_DOMAINE", "SSDOM_FK_DOMAINE" );
        $this->AddProjection( SousDomaineVO::$SSDOM_SERVICE_WMS, "SOUS_DOMAINE", "SSDOM_SERVICE_WMS" );
        $this->AddProjection( SousDomaineVO::$SSDOM_SERVICE_WMS_UUID, "SOUS_DOMAINE", "SSDOM_SERVICE_WMS_UUID" );
        $this->AddProjection( SousDomaineVO::$SSDOM_ADMIN_FK_GROUPE_PROFIL, "SOUS_DOMAINE", "SSDOM_ADMIN_FK_GROUPE_PROFIL" );
        $this->AddProjection( SousDomaineVO::$SSDOM_EDITEUR_FK_GROUPE_PROFIL, "SOUS_DOMAINE", "SSDOM_EDITEUR_FK_GROUPE_PROFIL" );
        $this->AddProjection( SousDomaineVO::$SSDOM_CREATEUR_FK_GROUPE_PROFIL, "SOUS_DOMAINE", "SSDOM_CREATEUR_FK_GROUPE_PROFIL" );
        if ( $bDomSDomNom ){
          $this->DOM_SSDOM_NOM = $this->SSDOM_PK_DOMAINE;
          $this->SSDOM_PK_DOMAINE++;
          $this->SSDOM_DOM_ID++;
          $this->SetDomaineSubDomaineNom();
        }
        $this->AddOrder(SousDomaineVO::$SSDOM_ID);
        $this->NewRowSequence = "SEQ_SOUS_DOMAINE";
    }

    /**
     * @brief Insertion de nouvelles valeurs
     * (non-PHPdoc)
     * @see www/PRRA/Administration/DAO/ViewObject/ViewObject#InsertValues()
     */
    public function InsertValues( $ordinalList, $valueList )
    {
        $accessRights = new AccessRights();

        $ordinalList[]= SousDomaineVO::$SSDOM_CREATEUR_FK_GROUPE_PROFIL;
        $valueList[]= $accessRights->GetFirstProfile();

        //echo "value ".$accessRights->GetFirstProfile()."<bR>";

        return parent::InsertValues( $ordinalList, $valueList );
    }

    public function SetDomaineSubDomaineNom()
    {
      $this->AddProjection( $this->DOM_SSDOM_NOM, "SOUS_DOMAINE", 
              "((CASE WHEN (select DOMAINE.DOM_NOM ||' : ' from DOMAINE where DOMAINE.PK_DOMAINE=SOUS_DOMAINE.SSDOM_FK_DOMAINE) is null" .
              " then '' " .
              " else (select DOMAINE.DOM_NOM ||' : ' from DOMAINE where DOMAINE.PK_DOMAINE=SOUS_DOMAINE.SSDOM_FK_DOMAINE) end) " .
              "||SOUS_DOMAINE.SSDOM_NOM) as DOM_SSDOM_NOM" );

      $this->AddOrder($this->DOM_SSDOM_NOM);
    }
    


    /**
     * Actions pré-enregistrement de la fiche
     * @param Controller $controller
     * @param Connection $conn
     * @param string $action
     */
    public function beforeSaveAction($controller, Connection $conn, string $action){
        $viewObject = $this;
        
        if ( isset($this->currentValues) && isset($this->currentValues[self::$PK_SOUS_DOMAINE]) ) {
            $domaine_pk = $this->currentValues[self::$PK_SOUS_DOMAINE];
            $ssdom_fmeta_uuid = isset($this->currentValues[self::$SSDOM_SERVICE_WMS_UUID]) ? $this->currentValues[self::$SSDOM_SERVICE_WMS_UUID] : null;
            if ( $action=="UPDATE" ) {
                /* Suppression de l'ancien service WMS si le dom_id a changé */
                $domaine_id = $conn->fetchOne("select ssdom_id from catalogue.sous_domaine where pk_sous_domaine=:domaine_pk", compact("domaine_pk"));
                $dom_id = isset($this->currentValues[self::$SSDOM_ID]) ? $this->currentValues[self::$SSDOM_ID] : null;
                if ( $domaine_id!=$dom_id ){
                    $has_service_wms = 0;
                    include(__DIR__.'/../../../Resources/templates/Administration/SubDomains/SubsDomainsServiceWMS.php');
                    $this->currentValues[self::$SSDOM_SERVICE_WMS_UUID] = null;
                }
            }
            if ( $action=="DELETE" ) {
                /* Suppression du service WMS */
                $domaine_id = $conn->fetchOne("select ssdom_id from catalogue.sous_domaine where pk_sous_domaine=:domaine_pk", compact("domaine_pk"));
                $has_service_wms = 0;
                include(__DIR__.'/../../../Resources/templates/Administration/SubDomains/SubsDomainsServiceWMS.php');
                $this->currentValues[self::$SSDOM_SERVICE_WMS_UUID] = null;
            }
        }
    }


    /**
     * Actions post-enregistrement de la fiche
     * @param Controller $controller
     * @param Connection $conn
     * @param bool $delete
     */
    public function afterSaveAction( $controller, Connection $conn, $delete=false){
        $viewObject = $this;
        
        if ( !$delete && isset($this->currentValues) && isset($this->currentValues[self::$PK_SOUS_DOMAINE]) ) {
            $domaine_pk = $this->currentValues[self::$PK_SOUS_DOMAINE];
            $domaine_id = isset($this->currentValues[self::$SSDOM_ID]) ? $this->currentValues[self::$SSDOM_ID] : null;
            $has_service_wms = isset($this->currentValues[self::$SSDOM_SERVICE_WMS]) ? $this->currentValues[self::$SSDOM_SERVICE_WMS] : null;
            $ssdom_fmeta_uuid = isset($this->currentValues[self::$SSDOM_SERVICE_WMS_UUID]) ? $this->currentValues[self::$SSDOM_SERVICE_WMS_UUID] : null;
            
            include(__DIR__.'/../../../Resources/templates/Administration/SubDomains/SubsDomainsServiceWMS.php');
            
            
            
//            $this->currentValues[self::$SSDOM_SERVICE_WMS_UUID] = $ssdom_fmeta_uuid;
//                
//                list(, $ordinalRestriction) = array_values($this->currentValues["__ARGS__"]);
//                $ordinalRestriction = array_combine((array)$ordinalRestriction, (array)$ordinalRestriction);
//                $valueRestriction = array_intersect_key($this->currentValues, $ordinalRestriction);
//                $valueOrdinal = array_diff_key($this->currentValues, $ordinalRestriction);
//                return array(array_keys($valueOrdinal), array_values($valueOrdinal), array_keys($valueRestriction), array_values($valueRestriction));
        }
    }
    /**
     * Force la synchronisation avec le LDAP edit/add
     * @param type $conn
     * @param array $ldapConf
     * @param array $ordinalList
     * @param array $valueList
     * @param string $pk
     * @param bool $delete
     */
    public function ldapSyncAction($pk,$delete = false){
        $conn = $this->getDao()->getConnection();
        include(__DIR__.'/../../../Resources/templates/Administration/SubDomains/SubDomainsSyncLdap.php');
    }

    /**
     * Force la synchronisation avec le LDAP edit/add dans le cadre d'une selection
     * @param type $conn
     * @param array $ldapConf
     * @param array $ordinalList
     * @param array $valueList
     * @param string $pk
     * @param bool $delete
    */
    public function ldapSyncSelectionAction($pk,$delete = false){
        $this->ldapSyncAction($pk, $delete);
    }

    public function ldapSyncActionClean($ordinal,$pk) {
        $conn = $this->getDao()->getConnection();
        if ($ordinal == SousDomaineVO::$SSDOM_ADMIN_FK_GROUPE_PROFIL)
        {
            include(__DIR__.'/../../../Resources/templates/Administration/SubDomains/SubDomainsSyncLdapClean.php');
        }
    }

    /**
     * Génération Thesaurus
     * @return unknown_type
     */
    public function syncThesaurus($controller){
        ThesaurusSerializer::getInstance($this->getDao()->getConnection(),$controller->getUnsecureCurlOptions(), THESAURUS_NAME)->generateDocument();
    }
}
?>

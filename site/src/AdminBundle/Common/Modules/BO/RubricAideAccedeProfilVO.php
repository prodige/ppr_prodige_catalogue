<?php
  namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;
/**
   * @class RubricAideAccedeProfilVO
   * @brief  Classe de gestion des groupes utilisateurs associés aux groupes d'aide
   * @author Alkante
   */

	//require_once($AdminPath."DAO/ViewObject/ViewObject.php");
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;

	class RubricAideAccedeProfilVO extends ViewObject
	{
    static public $PK_RUBRIC_AIDE_ACCEDE_PROFIL = 0;
    static public $RUBRICAIDEPROFILE_FK_RUBRICAIDE_ID = 1;
    static public $RUBRICAIDEPROFILE_FK_PROFIL = 2;
    
    static public $RUBRICAIDEPROFILE_RUBRICAIDE_ID = 3;
    static public $RUBRICAIDEPROFILE_RUBRICAIDE_NOM = 4;
    
    static public $RUBRICAIDEPROFILE_PROFIL_ID = 5;
    static public $RUBRICAIDEPROFILE_PROFIL_NOM = 6;
    
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( RubricAideAccedeProfilVO::$PK_RUBRIC_AIDE_ACCEDE_PROFIL, "PRODIGE_HELP_GROUP", "PK_PRODIGE_HELP_GROUP" );
      $this->AddProjection( RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_FK_RUBRICAIDE_ID, "PRODIGE_HELP_GROUP", "HLPGRP_FK_HELP_ID" );
      $this->AddProjection( RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_FK_PROFIL, "PRODIGE_HELP_GROUP", "HLPGRP_FK_GROUPE_PROFIL" );
      
      // linked fields.
      $this->AddProjection( RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_RUBRICAIDE_ID, "PRODIGE_HELP", "PK_PRODIGE_HELP_ID" );
      $this->AddProjection( RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_RUBRICAIDE_NOM, "PRODIGE_HELP", "PRODIGE_HELP_TITLE" );
      $this->AddProjection( RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_PROFIL_ID, "GROUPE_PROFIL", "PK_GROUPE_PROFIL");
      $this->AddProjection( RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_PROFIL_NOM, "GROUPE_PROFIL", "GRP_ID");
      
      // relastionship building
      $this->AddEqualsRelation( RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_FK_RUBRICAIDE_ID, RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_RUBRICAIDE_ID );
      $this->AddEqualsRelation( RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_FK_PROFIL, RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_PROFIL_ID );
    }
  }
?>
<?php
  namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;
  /**
   * @class StructureAccedeUserVO
   * @brief  Classe de gestion des groupes utilisateurs associés aux groupes d'aide
   * @author Alkante
   */

	//require_once($AdminPath."DAO/ViewObject/ViewObject.php");
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;

	class StructureAccedeUserVO extends ViewObject
	{
    const ID = 0;
    const STRUCTURE_USER_ID = 1;
    const USER_STRUCTURE_ID = 2;
    
    const STRUCTURE_ID = 3;
    const STRUCTURE_NOM = 4;
    
    const USER_ID = 5;
    const USER_NOM = 6;
    
     const TABLE_NAME = "structure";
    /**
     * @brief constructeur
     */ //id	fk_utilisateur	fk_structure
    public function __construct( )
    {
      $this->AddProjection( StructureAccedeUserVO::ID, "utilisateur_structure", "id" );
      $this->AddProjection( StructureAccedeUserVO::STRUCTURE_USER_ID, "utilisateur_structure", "fk_structure" );
      $this->AddProjection( StructureAccedeUserVO::USER_STRUCTURE_ID, "utilisateur_structure", "fk_utilisateur" );
      
      // linked fields.
      $this->AddProjection( StructureAccedeUserVO::STRUCTURE_ID, "structure", "pk_structure" );
      $this->AddProjection( StructureAccedeUserVO::STRUCTURE_NOM, "structure", "structure_nom" );
      $this->AddProjection( StructureAccedeUserVO::USER_ID, "utilisateur", "pk_utilisateur");
      $this->AddProjection( StructureAccedeUserVO::USER_NOM, "utilisateur", "usr_nom");
      
      // relastionship building
      $this->AddEqualsRelation( StructureAccedeUserVO::USER_STRUCTURE_ID , StructureAccedeUserVO::USER_ID );
      $this->AddEqualsRelation( StructureAccedeUserVO::STRUCTURE_USER_ID , StructureAccedeUserVO::STRUCTURE_ID );
    }
  }
?>
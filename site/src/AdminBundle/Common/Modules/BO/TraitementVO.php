<?php
namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;
/**
   * @class TraitementVO
   * @brief  Classe de gestion des traitements
   * @author Alkante
   */
  /*if (!isset($AdminPath))
    $AdminPath = "../../";
	require_once($AdminPath."/DAO/ViewObject/ViewObject.php");*/
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;

	
	class TraitementVO extends ViewObject
	{
		static public $PK_TRAITEMENT = 0;
		static public $TS = 1;
		static public $TRT_ID = 2;
		static public $TRT_NOM = 3;
		static public $TRT_DESCRIPTION = 4;
		
		public function __construct( )
		{
			$this->AddProjection( TraitementVO::$PK_TRAITEMENT, "TRAITEMENT", "PK_TRAITEMENT" );
			$this->AddProjection( TraitementVO::$TS, "TRAITEMENT", "TS" );
			$this->AddProjection( TraitementVO::$TRT_ID, "TRAITEMENT", "TRT_ID" );
			$this->AddProjection( TraitementVO::$TRT_NOM, "TRAITEMENT", "TRT_NOM" );
			$this->AddProjection( TraitementVO::$TRT_DESCRIPTION, "TRAITEMENT", "TRT_DESCRIPTION" );
      $this->AddOrder(TraitementVO::$TRT_ID);
			$this->NewRowSequence = "SEQ_TRAITEMENT";
		}
	}
?>

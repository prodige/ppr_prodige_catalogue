<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

  /**
   * @class UsrAlertePerimetreVO
   * @brief  Classe de gestion des compétences de groupes
   */
//require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
use Prodige\ProdigeBundle\DAOProxy\ViewObject;

  class UsrAlertePerimetreEditionVO extends ViewObject
  {
    static public $PK_USR_ALERTE_PERIMETRE_EDITION = 0;
    
    static public $USRALERTERIMETRE_FK_PERIMETRE = 1;
    static public $USRALERTERIMETRE_FK_UTILISATEUR = 2;
    static public $USRALERTERIMETRE_FK_COUCHEDONNEES = 3;
    
    static public $USRALERTPERIMETRE_PERIMETRE_ID = 4;
    static public $USRALERTPERIMETRE_PERIMETRE_NOM = 5;
    
    static public $USRALERTPERIMETRE_USR_ID = 6;
    static public $USRALERTPERIMETRE_USR_NOM = 7;
    
    static public $USRALERTPERIMETRE_COUCHEDONNEES = 8;
    static public $USRALERTPERIMETRE_COUCHD_ID = 9;
    
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( UsrAlertePerimetreEditionVO::$PK_USR_ALERTE_PERIMETRE_EDITION, "USR_ALERTE_PERIMETRE_EDITION", "PK_USR_ALERTE_PERIMETRE_EDITION" );
      $this->AddProjection( UsrAlertePerimetreEditionVO::$USRALERTERIMETRE_FK_PERIMETRE, "USR_ALERTE_PERIMETRE_EDITION", "USRALERT_FK_PERIMETRE" );
      $this->AddProjection( UsrAlertePerimetreEditionVO::$USRALERTERIMETRE_FK_UTILISATEUR, "USR_ALERTE_PERIMETRE_EDITION", "USRALERT_FK_UTILISATEUR" );
      $this->AddProjection( UsrAlertePerimetreEditionVO::$USRALERTERIMETRE_FK_COUCHEDONNEES, "USR_ALERTE_PERIMETRE_EDITION", "USRALERT_FK_COUCHEDONNEES" );
      
      $this->AddProjection( UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_PERIMETRE_ID, "PERIMETRE", "PK_PERIMETRE_ID" );
      $this->AddProjection( UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_PERIMETRE_NOM, "PERIMETRE", "PERIMETRE_NOM  ||' (' || PERIMETRE_CODE || ')'" );
      
      $this->AddProjection( UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_USR_ID, "UTILISATEUR", "PK_UTILISATEUR");
      $this->AddProjection( UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_USR_NOM, "UTILISATEUR", "USR_ID");
      
      $this->AddProjection( UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_COUCHEDONNEES, "COUCHE_DONNEES", "PK_COUCHE_DONNEES");
      $this->AddProjection( UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_COUCHD_ID, "COUCHE_DONNEES", "COUCHD_ID");
      
         
      // relastionship building
      $this->AddEqualsRelation( UsrAlertePerimetreEditionVO::$USRALERTERIMETRE_FK_UTILISATEUR,   UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_USR_ID );
      $this->AddEqualsRelation( UsrAlertePerimetreEditionVO::$USRALERTERIMETRE_FK_PERIMETRE,     UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_PERIMETRE_ID );
      $this->AddEqualsRelation( UsrAlertePerimetreEditionVO::$USRALERTERIMETRE_FK_COUCHEDONNEES, UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_COUCHEDONNEES );
    }
  }
?>

<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

/**
   * @class CompetenceVO
   * @brief  Classe de gestion des compétences
   */
  /*if (!isset($AdminPath))
    $AdminPath = "../../";
    
  require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
  require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

  class CompetenceVO extends ViewObject
  {
    static public $PK_COMPETENCE_ID = 0;
    static public $COMPETENCE_NOM = 1;
    
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( CompetenceVO::$PK_COMPETENCE_ID, "COMPETENCE", "PK_COMPETENCE_ID" );
      $this->AddProjection( CompetenceVO::$COMPETENCE_NOM, "COMPETENCE", "COMPETENCE_NOM" );

      $this->NewRowSequence = "SEQ_COMPETENCE";
      $this->AddOrder(CompetenceVO::$COMPETENCE_NOM);
    
    }

  }
?>

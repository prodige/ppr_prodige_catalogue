<?php

  namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

  /**
   * @class CompetenceAccedeCoucheVO
   * @brief  Classe de gestion des compétences
   */
 //require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;
  class CompetenceAccedeCoucheVO extends ViewObject
  {
    static public $PK_COMPETENCE_ACCEDE_COUCHE = 0;
    static public $COMPETENCECOUCHE_FK_COMPETENCE_ID = 1;
    static public $COMPETENCECOUCHE_FK_COUCHE_DONNEES = 2;
    
    static public $COMPETENCE_COUCHE_COMPETENCE_ID = 3;
    static public $COMPETENCE_COUCHE_COMPETENCE_NOM = 4;
    
    static public $COMPETENCECOUCHE_COUCHEDONNEES = 5;
    static public $COMPETENCECOUCHE_COUCHD_ID = 6;
    static public $COMPETENCECOUCHE_COUCHD_NOM = 7;
    
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( CompetenceAccedeCoucheVO::$PK_COMPETENCE_ACCEDE_COUCHE, "COMPETENCE_ACCEDE_COUCHE", "PK_COMPETENCE_ACCEDE_COUCHE" );
      $this->AddProjection( CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_FK_COMPETENCE_ID, "COMPETENCE_ACCEDE_COUCHE", "COMPETENCECOUCHE_FK_COMPETENCE_ID" );
      $this->AddProjection( CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_FK_COUCHE_DONNEES, "COMPETENCE_ACCEDE_COUCHE", "COMPETENCECOUCHE_FK_COUCHE_DONNEES" );
      
      // linked fields.
      $this->AddProjection( CompetenceAccedeCoucheVO::$COMPETENCE_COUCHE_COMPETENCE_ID, "COMPETENCE", "PK_COMPETENCE_ID" );
      $this->AddProjection( CompetenceAccedeCoucheVO::$COMPETENCE_COUCHE_COMPETENCE_NOM, "COMPETENCE", "COMPETENCE_NOM" );
      $this->AddProjection( CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_COUCHEDONNEES, "COUCHE_DONNEES", "PK_COUCHE_DONNEES");
      $this->AddProjection( CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_COUCHD_ID, "COUCHE_DONNEES", "COUCHD_ID");
      $this->AddProjection( CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_COUCHD_NOM, "COUCHE_DONNEES", "COUCHD_NOM");
      
      $this->AddOrder(CompetenceAccedeCoucheVO::$COMPETENCE_COUCHE_COMPETENCE_NOM);
      // relastionship building
      $this->AddEqualsRelation( CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_FK_COMPETENCE_ID, CompetenceAccedeCoucheVO::$COMPETENCE_COUCHE_COMPETENCE_ID );
      $this->AddEqualsRelation( CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_FK_COUCHE_DONNEES, CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_COUCHEDONNEES );
    }
  }
?>

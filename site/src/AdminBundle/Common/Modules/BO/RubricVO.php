<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

/**
   * @class RubricVO
   * @brief  Classe de gestion des rubriques
   */
  /*if (!isset($AdminPath))
    $AdminPath = "../../";

  require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
  require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;
  use ProdigeCatalogue\AdminBundle\Services\ThesaurusSerializer;

  class RubricVO extends ViewObject
  {
    static public $RUBRIC_ID = 0;
    static public $RUBRIC_NAME = 1;
    
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( RubricVO::$RUBRIC_ID, "RUBRIC_PARAM", "RUBRIC_ID" );
      $this->AddProjection( RubricVO::$RUBRIC_NAME, "RUBRIC_PARAM", "RUBRIC_NAME" );

      $this->NewRowSequence = "RUBRIC_PARAM_RUBRIC_ID_SEQ";
      $this->AddOrder(RubricVO::$RUBRIC_NAME);
    
    }
    
    
    /**
     * Génération Thesaurus
     * @return unknown_type
     */
      public function syncThesaurus($controller){
        ThesaurusSerializer::getInstance($this->getDao()->getConnection(),$controller->getUnsecureCurlOptions(), THESAURUS_NAME)->generateDocument();
      }

  }
?>

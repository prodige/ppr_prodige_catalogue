<?php
namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

use Prodige\ProdigeBundle\DAOProxy\ViewObject;

/**
   * @class GroupeProfilVO
   * @brief  Classe de gestion des GroupeProfils
   * @author Alkante
   */
	class GroupeProfilVO extends ViewObject
	{
		static public $PK_GROUPE_PROFIL = 0;
		static public $TS = 1;
		static public $GRP_ID = 2;
		static public $GRP_NOM = 3;
		static public $GRP_DESCRIPTION = 4;
		static public $GRP_IS_DEFAULT_INSTALLATION = 5;
		static public $GRP_NOM_LDAP = 6;

		const TEMPLATE_MAIL = 7;

		public function __construct( )
		{
			$this->AddProjection( GroupeProfilVO::$PK_GROUPE_PROFIL, "GROUPE_PROFIL", "PK_GROUPE_PROFIL" );
			$this->AddProjection( GroupeProfilVO::$TS, "GROUPE_PROFIL", "TS" );
			$this->AddProjection( GroupeProfilVO::$GRP_ID, "GROUPE_PROFIL", "GRP_ID" );
			$this->AddProjection( GroupeProfilVO::$GRP_NOM, "GROUPE_PROFIL", "GRP_NOM" );
			$this->AddProjection( GroupeProfilVO::$GRP_DESCRIPTION, "GROUPE_PROFIL", "GRP_DESCRIPTION" );
			$this->AddProjection( GroupeProfilVO::$GRP_IS_DEFAULT_INSTALLATION, "GROUPE_PROFIL", "GRP_IS_DEFAULT_INSTALLATION" );
			$this->AddProjection( GroupeProfilVO::$GRP_NOM_LDAP, "GROUPE_PROFIL", "GRP_NOM_LDAP" );
			$this->AddProjection( GroupeProfilVO::TEMPLATE_MAIL, "GROUPE_PROFIL", "GRP_TEMPLATE_MAIL" );

      $this->AddOrder(GroupeProfilVO::$GRP_ID);
			$this->NewRowSequence = "SEQ_GROUPE_PROFIL";
		}
	}
?>
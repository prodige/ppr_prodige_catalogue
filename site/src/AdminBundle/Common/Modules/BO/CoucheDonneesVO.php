<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

	/**
   * @class CoucheDonneesVO
   * 
   * 
   * @brief  Classe de gestion des couches de données
   * @author Alkante
   */

  /*if (!isset($AdminPath))
    $AdminPath = "../../";
require_once($AdminPath."/DAO/ViewObject/ViewObject.php");*/
use Prodige\ProdigeBundle\DAOProxy\ViewObject;

	class CoucheDonneesVO extends ViewObject
	{
		static public $PK_COUCHE_DONNEES = 0;
		static public $TS = 1;
		static public $COUCHD_ID = 2;
		static public $COUCHD_NOM = 3;
		static public $COUCHD_DESCRIPTION = 4;
		static public $COUCHD_TYPE_STOCKAGE = 5;
		static public $COUCHD_EMPLACEMENT_STOCKAGE = 6;
		static public $COUCHD_FK_ACCES_SERVER = 7;
		static public $COUCHD_WMS = 8;
		static public $COUCHD_WFS = 9;
		static public $COUCHD_DOWNLOAD = 10;
    static public $COUCHD_RESTRICTION = 11;
		static public $COUCHD_RESTRICTION_EXCLUSIF = 12;
    static public $COUCHD_ZONAGEID_FK = 13;
    static public $COUCHD_RESTRICTION_BUFFER = 14;
    static public $COUCHD_EXTRACTION_ATTRIBUTAIRE = 15;
    static public $COUCHD_EXTRACTION_ATTRIBUTAIRE_CHAMP = 16;
    static public $COUCHD_ALERT_MSG_ID = 17;
		
		public function __construct( )
		{
			$this->AddProjection( CoucheDonneesVO::$PK_COUCHE_DONNEES, "COUCHE_DONNEES", "PK_COUCHE_DONNEES" );
			$this->AddProjection( CoucheDonneesVO::$TS, "COUCHE_DONNEES", "TS" );
			$this->AddProjection( CoucheDonneesVO::$COUCHD_ID, "COUCHE_DONNEES", "COUCHD_ID" );
			$this->AddProjection( CoucheDonneesVO::$COUCHD_NOM, "COUCHE_DONNEES", "COUCHD_NOM" );
			$this->AddProjection( CoucheDonneesVO::$COUCHD_DESCRIPTION, "COUCHE_DONNEES", "COUCHD_DESCRIPTION" );
			$this->AddProjection( CoucheDonneesVO::$COUCHD_TYPE_STOCKAGE, "COUCHE_DONNEES", "COUCHD_TYPE_STOCKAGE" );
			$this->AddProjection( CoucheDonneesVO::$COUCHD_EMPLACEMENT_STOCKAGE, "COUCHE_DONNEES", "COUCHD_EMPLACEMENT_STOCKAGE" );
			$this->AddProjection( CoucheDonneesVO::$COUCHD_FK_ACCES_SERVER, "COUCHE_DONNEES", "COUCHD_FK_ACCES_SERVER" );
			$this->AddProjection( CoucheDonneesVO::$COUCHD_WMS, "COUCHE_DONNEES", "COUCHD_WMS" );
			$this->AddProjection( CoucheDonneesVO::$COUCHD_WFS, "COUCHE_DONNEES", "COUCHD_WFS" );
			$this->AddProjection( CoucheDonneesVO::$COUCHD_DOWNLOAD, "COUCHE_DONNEES", "COUCHD_DOWNLOAD");
      $this->AddProjection( CoucheDonneesVO::$COUCHD_RESTRICTION, "COUCHE_DONNEES", "COUCHD_RESTRICTION");
			$this->AddProjection( CoucheDonneesVO::$COUCHD_RESTRICTION_EXCLUSIF, "COUCHE_DONNEES", "COUCHD_RESTRICTION_EXCLUSIF");
      $this->AddProjection( CoucheDonneesVO::$COUCHD_ZONAGEID_FK, "COUCHE_DONNEES", "COUCHD_ZONAGEID_FK");
      $this->AddProjection( CoucheDonneesVO::$COUCHD_RESTRICTION_BUFFER, "COUCHE_DONNEES", "COUCHD_RESTRICTION_BUFFER");
      $this->AddProjection( CoucheDonneesVO::$COUCHD_EXTRACTION_ATTRIBUTAIRE, "COUCHE_DONNEES", "COUCHD_EXTRACTION_ATTRIBUTAIRE");
      $this->AddProjection( CoucheDonneesVO::$COUCHD_EXTRACTION_ATTRIBUTAIRE_CHAMP, "COUCHE_DONNEES", "COUCHD_EXTRACTION_ATTRIBUTAIRE_CHAMP");
      $this->AddProjection( CoucheDonneesVO::$COUCHD_ALERT_MSG_ID, "COUCHE_DONNEES", "COUCHD_ALERT_MSG_ID");
			
			$this->NewRowSequence = "SEQ_COUCHE_DONNEES";
      $this->AddOrder(CoucheDonneesVO::$COUCHD_NOM);
		}
		/**
		 * retourne le nom de la classe à appliquer à chaque row de la liste des couches (en fonction du caractère public ou non de la donnée
		 * @param $viewObject objet viewObject
		 * @return text
		 */
		public function getOptionClassName($viewObject){
			if($viewObject->Read(CoucheDonneesVO::$COUCHD_DOWNLOAD)==1 || $viewObject->Read(CoucheDonneesVO::$COUCHD_WMS)==1 ||
			   $viewObject->Read(CoucheDonneesVO::$COUCHD_WFS)==1)
			  return "data-download";
			return "";  
		}

		public function GetCoucheDonnees()
		{
		}
	}
?>

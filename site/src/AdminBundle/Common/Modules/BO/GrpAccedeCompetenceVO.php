<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

  /**
   * @class GrpAccedeCompetenceVO
   * @brief  Classe de gestion des compétences de groupes
   */
//require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
use Prodige\ProdigeBundle\DAOProxy\ViewObject;

  class GrpAccedeCompetenceVO extends ViewObject
  {
    static public $PK_GRP_ACCEDE_COMPETENCE = 0;
    static public $FK_GRP_ID = 1;
    static public $FK_COMPETENCE_ID = 2;
    
    static public $GRPACCCOMPETENCE_COMPETENCE_ID = 3;
    static public $GRPACCCOMPETENCE_COMPETENCE_NOM = 4;
    
    static public $GRPACCCOMPETENCE_GROUPE_PROFIL = 5;
    static public $GRPACCCOMPETENCE_GRP_ID = 6;
    
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( GrpAccedeCompetenceVO::$PK_GRP_ACCEDE_COMPETENCE, "GRP_ACCEDE_COMPETENCE", "PK_GRP_ACCEDE_COMPETENCE" );
      $this->AddProjection( GrpAccedeCompetenceVO::$FK_GRP_ID, "GRP_ACCEDE_COMPETENCE", "FK_GRP_ID" );
      $this->AddProjection( GrpAccedeCompetenceVO::$FK_COMPETENCE_ID, "GRP_ACCEDE_COMPETENCE", "FK_COMPETENCE_ID" );
      
      // linked fields.
      $this->AddProjection( GrpAccedeCompetenceVO::$GRPACCCOMPETENCE_COMPETENCE_ID, "COMPETENCE", "PK_COMPETENCE_ID" );
      $this->AddProjection( GrpAccedeCompetenceVO::$GRPACCCOMPETENCE_COMPETENCE_NOM, "COMPETENCE", "COMPETENCE_NOM" );
      $this->AddProjection( GrpAccedeCompetenceVO::$GRPACCCOMPETENCE_GROUPE_PROFIL, "GROUPE_PROFIL", "PK_GROUPE_PROFIL");
      $this->AddProjection( GrpAccedeCompetenceVO::$GRPACCCOMPETENCE_GRP_ID, "GROUPE_PROFIL", "GRP_ID");
      
      // relastionship building
      $this->AddEqualsRelation( GrpAccedeCompetenceVO::$FK_COMPETENCE_ID, GrpAccedeCompetenceVO::$GRPACCCOMPETENCE_COMPETENCE_ID );
      $this->AddEqualsRelation( GrpAccedeCompetenceVO::$FK_GRP_ID, GrpAccedeCompetenceVO::$GRPACCCOMPETENCE_GROUPE_PROFIL );
    }
  }
?>

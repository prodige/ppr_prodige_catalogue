<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

/**
   * @class RubricAideVO
   * @brief  Classe de gestion des rubriques d'aide
   */
  /*if (!isset($AdminPath))
    $AdminPath = "../../";

  require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
  require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

  class RubricAideVO extends ViewObject
  {
    static public $RUBRIC_AIDE_ID = 0;
    static public $RUBRIC_AIDE_NAME = 1;
    static public $RUBRIC_AIDE_DESC = 2;
    static public $RUBRIC_AIDE_FILE = 3;
    
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( RubricAideVO::$RUBRIC_AIDE_ID, "PRODIGE_HELP", "PK_PRODIGE_HELP_ID" );
      $this->AddProjection( RubricAideVO::$RUBRIC_AIDE_NAME, "PRODIGE_HELP", "PRODIGE_HELP_TITLE" );
      $this->AddProjection( RubricAideVO::$RUBRIC_AIDE_DESC, "PRODIGE_HELP", "PRODIGE_HELP_DESC" );
      $this->AddProjection( RubricAideVO::$RUBRIC_AIDE_FILE, "PRODIGE_HELP", "PRODIGE_HELP_URL" );

      $this->NewRowSequence = "SEQ_PRODIGE_HELP";
      $this->AddOrder(RubricAideVO::$RUBRIC_AIDE_ID);
    
    }

  }
?>

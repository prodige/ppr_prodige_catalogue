<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

/**
   * @class ObjetVO
   * @brief  Classe de gestion des Objets
   */
  /*if (!isset($AdminPath))
    $AdminPath = "../../";
  require_once($AdminPath."DAO/ViewObject/ViewObject.php");
  require_once($AdminPath."Administration/AccessRights/AccessRights.php");*/
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;


  class ObjetVO extends ViewObject
  {
    static public $OBJET_ID = 0;
    static public $OBJET_NOM = 1;
    static public $OBJET_TYPE_ID = 2;
    
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( ObjetVO::$OBJET_ID, "OBJET", "OBJET_ID" );
      $this->AddProjection( ObjetVO::$OBJET_NOM, "OBJET", "OBJET_NOM" );
      $this->AddProjection( ObjetVO::$OBJET_TYPE_ID, "OBJET", "OBJET_TYPE_ID" );
      $this->NewRowSequence = "SEQ_OBJET";
      $this->AddOrder(ObjetVO::$OBJET_NOM);
    }

  }
?>

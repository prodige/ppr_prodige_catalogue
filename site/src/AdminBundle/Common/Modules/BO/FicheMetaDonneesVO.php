<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

	/**
   * @class FicheMetaDonneesVO
   * @brief  Classe de gestion des Fiches de MetaDonnees
   * @author Alkante
   */
//require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
use Prodige\ProdigeBundle\DAOProxy\ViewObject;
	class FicheMetaDonneesVO extends ViewObject
	{
		static public $PK_FICHE_METADONNEES = 0;
		static public $TS = 1;
		static public $FMETA_ID = 2;
		static public $FMETA_DESCRIPTION = 3;
		static public $FMETA_FK_COUCHE_DONNEES = 4;

		public function __construct( )
		{
			$this->AddProjection( FicheMetaDonneesVO::$PK_FICHE_METADONNEES, "FICHE_METADONNEES", "PK_FICHE_METADONNEES" );
			$this->AddProjection( FicheMetaDonneesVO::$TS, "FICHE_METADONNEES", "TS" );
			$this->AddProjection( FicheMetaDonneesVO::$FMETA_ID, "FICHE_METADONNEES", "FMETA_ID" );
			$this->AddProjection( FicheMetaDonneesVO::$FMETA_DESCRIPTION, "FICHE_METADONNEES", "FMETA_DESCRIPTION" );
			$this->AddProjection( FicheMetaDonneesVO::$FMETA_FK_COUCHE_DONNEES, "FICHE_METADONNEES", "FMETA_FK_COUCHE_DONNEES" );

			$this->NewRowSequence = "SEQ_FICHE_METADONNEES";
		}

		public function GetCoucheDonnees()
		{
		}
	}
?>
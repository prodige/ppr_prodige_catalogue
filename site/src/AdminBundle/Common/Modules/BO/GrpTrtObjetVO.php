<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

  /**
   * @class GrpTrtObjetVO
   * @brief  Classe de gestion des traitements des groupes sur les objets
   */
//require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
use Prodige\ProdigeBundle\DAOProxy\ViewObject;

  class GrpTrtObjetVO extends ViewObject
  {
    static public $PK_GPR_TRT_OBJET = 0;
    static public $FK_GRP_ID = 1;
    static public $FK_TRT_ID = 2;
    static public $FK_OBJET_ID = 3;
    static public $FK_OBJ_TYPE_ID = 4;
    static public $GRP_TRT_OBJET_STATUS = 5;
    
    static public $GRPTRTOBJET_PK_TRAITEMENT = 6;
    static public $GRPTRTOBJET_TRT_ID = 7;
    
    
    static public $GRPTRTOBJET_GROUPE_PROFIL = 8;
    static public $GRPTRTOBJET_GRP_ID = 9;
    static public $GRPTRTOBJET_OBJET_ID = 10;
    static public $GRPTRTOBJET_OBJETTYPE = 11;
    
   
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( GrpTrtObjetVO::$PK_GPR_TRT_OBJET, "GRP_TRT_OBJET", "PK_GRP_TRT_OBJET" );
      $this->AddProjection( GrpTrtObjetVO::$FK_GRP_ID, "GRP_TRT_OBJET", "GRPTRTOBJ_FK_GRP_ID" );
      $this->AddProjection( GrpTrtObjetVO::$FK_TRT_ID, "GRP_TRT_OBJET", "GRPTRTOBJ_FK_TRT_ID" );
      $this->AddProjection( GrpTrtObjetVO::$FK_OBJET_ID, "GRP_TRT_OBJET", "GRPTRTOBJ_FK_OBJET_ID" );
      $this->AddProjection( GrpTrtObjetVO::$FK_OBJ_TYPE_ID, "GRP_TRT_OBJET", "GRPTRTOBJ_FK_OBJ_TYPE_ID" );
      $this->AddProjection( GrpTrtObjetVO::$GRP_TRT_OBJET_STATUS, "GRP_TRT_OBJET", "GRP_TRT_OBJET_STATUS" );
      
      // linked fields.
      $this->AddProjection( GrpTrtObjetVO::$GRPTRTOBJET_PK_TRAITEMENT, "TRT_OBJET", "PK_TRT_OBJET" );
      $this->AddProjection( GrpTrtObjetVO::$GRPTRTOBJET_TRT_ID, "TRT_OBJET", "TRT_ID" );
      $this->AddProjection( GrpTrtObjetVO::$GRPTRTOBJET_GROUPE_PROFIL, "GROUPE_PROFIL", "PK_GROUPE_PROFIL");
      $this->AddProjection( GrpTrtObjetVO::$GRPTRTOBJET_GRP_ID, "GROUPE_PROFIL", "GRP_ID");
      $this->AddProjection( GrpTrtObjetVO::$GRPTRTOBJET_OBJET_ID, "OBJET", "OBJET_ID");
      $this->AddProjection( GrpTrtObjetVO::$GRPTRTOBJET_OBJETTYPE, "OBJET", "OBJET_TYPE_ID");
 
      $this->AddOrder(GrpTrtObjetVO::$GRPTRTOBJET_GRP_ID);
      // relastionship building
      $this->AddEqualsRelation( GrpTrtObjetVO::$FK_TRT_ID, GrpTrtObjetVO::$GRPTRTOBJET_PK_TRAITEMENT );
      $this->AddEqualsRelation( GrpTrtObjetVO::$FK_GRP_ID, GrpTrtObjetVO::$GRPTRTOBJET_GROUPE_PROFIL );
      $this->AddEqualsRelation( GrpTrtObjetVO::$FK_OBJET_ID, GrpTrtObjetVO::$GRPTRTOBJET_OBJET_ID );
      
      
    }
  }
?>

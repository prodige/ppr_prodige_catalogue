<?php
namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;
/**
   * @class GrpAutoriseTrtVO
   * @brief  Classe de gestion des autorisations sur traitements
   * @author Alkante
   */
//require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
use Prodige\ProdigeBundle\DAOProxy\ViewObject;

	class GrpAutoriseTrtVO extends ViewObject
	{
		static public $PK_GRP_AUTORISE_TRT = 0;
		static public $TS = 1;
		static public $GRPTRT_FK_TRAITEMENT = 2;
		static public $GRPTRT_FK_GROUPE_PROFIL = 3;
		
		static public $GRPTRT_PK_TRAITEMENT = 4;
		static public $GRPTRT_TRT_ID = 5;
		
		static public $GRPTRT_PK_GROUPE_PROFIL = 6;
		static public $GRPTRT_GRP_ID = 7;
		
		public function __construct( )
		{
			$this->AddProjection( GrpAutoriseTrtVO::$PK_GRP_AUTORISE_TRT, "GRP_AUTORISE_TRT", "PK_GRP_AUTORISE_TRT" );
			$this->AddProjection( GrpAutoriseTrtVO::$TS, "GRP_AUTORISE_TRT", "TS" );
			$this->AddProjection( GrpAutoriseTrtVO::$GRPTRT_FK_TRAITEMENT, "GRP_AUTORISE_TRT", "GRPTRT_FK_TRAITEMENT" );
			$this->AddProjection( GrpAutoriseTrtVO::$GRPTRT_FK_GROUPE_PROFIL, "GRP_AUTORISE_TRT", "GRPTRT_FK_GROUPE_PROFIL" );
			
			// linked fields.
			$this->AddProjection( GrpAutoriseTrtVO::$GRPTRT_PK_TRAITEMENT, "TRAITEMENT", "PK_TRAITEMENT" );
			$this->AddProjection( GrpAutoriseTrtVO::$GRPTRT_TRT_ID, "TRAITEMENT", "TRT_ID" );
			$this->AddProjection( GrpAutoriseTrtVO::$GRPTRT_PK_GROUPE_PROFIL, "GROUPE_PROFIL", "PK_GROUPE_PROFIL");
			$this->AddProjection( GrpAutoriseTrtVO::$GRPTRT_GRP_ID, "GROUPE_PROFIL", "GRP_ID");
			
			// relastionship building
			$this->AddEqualsRelation( GrpAutoriseTrtVO::$GRPTRT_FK_TRAITEMENT, GrpAutoriseTrtVO::$GRPTRT_PK_TRAITEMENT );
			$this->AddEqualsRelation( GrpAutoriseTrtVO::$GRPTRT_FK_GROUPE_PROFIL, GrpAutoriseTrtVO::$GRPTRT_PK_GROUPE_PROFIL );
			$this->AddOrder(GrpAutoriseTrtVO::$GRPTRT_GRP_ID);
		}
                
                
                /**
                * Force la synchronisation avec le LDAP edit/add
                * @param type $conn
                * @param array $ldapConf
                * @param array $ordinalList
                * @param array $valueList
                * @param string $pk
                * @param bool $delete
                */
                public function ldapSyncAction($pk,$delete = false){
                   $conn = $this->getDao()->getConnection();
                   include(__DIR__.'/../../../Resources/templates/Administration/Profiles/ProfilesTraitementsSyncLdap.php');
                }
	}
?>

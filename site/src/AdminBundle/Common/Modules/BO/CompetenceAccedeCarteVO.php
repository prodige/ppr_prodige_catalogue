<?php

namespace ProdigeCatalogue\AdminBundle\Common\Modules\BO;

  /**
   * @class CompetenceAccedeCarteVO
   * @brief  Classe de gestion des compétences de cartes
   */
//require_once($AdminPath."/DAO/ViewObject/ViewObject.php");
use Prodige\ProdigeBundle\DAOProxy\ViewObject;

  class CompetenceAccedeCarteVO extends ViewObject
  {
    static public $PK_COMPETENCE_ACCEDE_CARTE = 0;
    static public $COMPETENCECARTE_FK_COMPETENCE_ID = 1;
    static public $COMPETENCECARTE_FK_CARTE_PROJET = 2;
    
    static public $COMPETENCECARTE_COMPETENCE_ID = 3;
    static public $COMPETENCECARTE_COMPETENCENOM = 4;
    
    static public $COMPETENCECARTE_CARTEPROJET = 5;
    static public $COMPETENCECARTE_CARTP_ID = 6;
    static public $COMPETENCECARTE_CARTP_NOM = 7;
    
    /**
     * @brief constructeur
     */
    public function __construct( )
    {
      $this->AddProjection( CompetenceAccedeCarteVO::$PK_COMPETENCE_ACCEDE_CARTE, "COMPETENCE_ACCEDE_CARTE", "PK_COMPETENCE_ACCEDE_CARTE" );
      $this->AddProjection( CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_COMPETENCE_ID, "COMPETENCE_ACCEDE_CARTE", "COMPETENCECARTE_FK_COMPETENCE_ID" );
      $this->AddProjection( CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_CARTE_PROJET, "COMPETENCE_ACCEDE_CARTE", "COMPETENCECARTE_FK_CARTE_PROJET" );
      
      // linked fields.
      $this->AddProjection( CompetenceAccedeCarteVO::$COMPETENCECARTE_COMPETENCE_ID, "COMPETENCE", "PK_COMPETENCE_ID" );
      $this->AddProjection( CompetenceAccedeCarteVO::$COMPETENCECARTE_COMPETENCENOM, "COMPETENCE", "COMPETENCE_NOM" );
      $this->AddProjection( CompetenceAccedeCarteVO::$COMPETENCECARTE_CARTEPROJET, "CARTE_PROJET", "PK_CARTE_PROJET");
      $this->AddProjection( CompetenceAccedeCarteVO::$COMPETENCECARTE_CARTP_ID, "CARTE_PROJET", "CARTP_ID");
      $this->AddProjection( CompetenceAccedeCarteVO::$COMPETENCECARTE_CARTP_NOM, "CARTE_PROJET", "CARTP_NOM");
      
      $this->AddOrder(CompetenceAccedeCarteVO::$COMPETENCECARTE_COMPETENCENOM);
      // relastionship building
      $this->AddEqualsRelation( CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_COMPETENCE_ID, CompetenceAccedeCarteVO::$COMPETENCECARTE_COMPETENCE_ID );
      $this->AddEqualsRelation( CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_CARTE_PROJET, CompetenceAccedeCarteVO::$COMPETENCECARTE_CARTEPROJET );
    }
  }
?>

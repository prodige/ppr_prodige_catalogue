<?php
namespace ProdigeCatalogue\AdminBundle\Common\Ressources;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Prodige\ProdigeBundle\Controller\BaseController;

/**
 * Classe utilisé pour configurer l'IHM(menu, bouton, actions, etc...) extjs à  partir des variables et valeur par défaut
 *
 */
class Ressources extends BaseController {

    // #################################
    static public $CONNECTION_CONNECTION_FAILED = "Impossible de se connecter à la Base.\n";
    static public $DAO_RESULSTSET_NOT_GOOD = "La requête n'a pu être exécutée.";
    static public $INVALID_FILE_EXTENSION = "L'extension du fichier téléchargée n'est pas valide, veuillez modifier votre fichier"; 
    static public $DAO_EXECUTE_ERROR = "Il y a eu une erreur à l'exécution de la requête.";
    // #################################
    static public $GRAPHIC_BG_COLOR ="#FFFFFF";
    static public $GRAPHIC_FG_COLOR ="#000055";

    // #################################
    static public $ACTION_RANGE             = "RANGE";
    static public $ACTION_OPEN_INITIAL      = "INITIAL";
    static public $ACTION_OPEN_PROFILES     = "PROFILES";
    static public $ACTION_OPEN_STRUCTURE          = "STRUCTURE";
    static public $ACTION_OPEN_USERS        = "USERS";
    static public $ACTION_OPEN_RUBRICS      = "RUBRICS";
    

    static public $ACTION_OPEN_DOMAINS      = "DOMAINS";
    static public $ACTION_OPEN_SUBDOMAINS   = "SUBDOMAINS";
    static public $ACTION_OPEN_LAYERS       = "LAYERS";
    static public $ACTION_OPEN_MAPS         = "MAPS";

    static public $ACTION_OPEN_COMPETENCES  = "COMPETENCES";
    static public $ACTION_OPEN_ZONAGES      = "ZONAGES";
    static public $ACTION_OPEN_PERIMETRES   = "PERIMETRES";
    static public $ACTION_OPEN_GROUPING     = "GROUPING";
    static public $ACTION_OPEN_OBJECTS      = "OBJECTS";//6ème onglet

    static public $ACTION_OPEN_GROUPING_PROFILES = "GROUPING_PROFILES";
    static public $ACTION_OPEN_GROUPING_SUBDOMAINS = "GROUPING_SUBDOMAINS";

    static public $ACTION_OPEN_OBJECTS_DONNEE     = "OBJECTS_DONNEE";
    static public $ACTION_OPEN_OBJECTS_CARTE      = "OBJECTS_CARTE";
    static public $ACTION_OPEN_OBJECTS_METADONNEE = "OBJECTS_METADONNEE";
    static public $ACTION_OPEN_OBJECTS_MODELE     = "OBJECTS_MODELE";
    static public $ACTION_OPEN_OBJECTS_GROUPS     = "OBJECTS_GROUPS";
    static public $ACTION_OPEN_RUBRICS_AIDE       = "RUBRICS_AIDE";

    static public $ACTION_OPEN_EMPTY              = "EMPTY";

    static public $BTN_INITIAL      = "Accueil";
    static public $BTN_PROFILES     = "Profils";
    static public $BTN_MEMBERS      = "Membres";
    static public $BTN_ACTIONS      = "Traitements";
    static public $BTN_ADMIN        = "Administre";
    static public $BTN_EDITE         = "Edite";
    static public $BTN_DOMAINS      = "Domaines";
    static public $BTN_SUBDOMAINS   = "Sous-Domaines";
    static public $BTN_OBJECTS      = "Données accessibles";
    static public $BTN_COMPETENCES  = "Compétences";
    static public $BTN_ALERTS       = "Alertes sur édition";
    static public $BTN_ZONAGES      = "Zonages";
    static public $BTN_PERIMETRES   = "Territoires (consultation/téléchargement)";
    static public $BTN_PERIMETRES_EDITION   = "Territoires (Edition)";
    static public $BTN_COMPOSTION = "Composition";
    static public $BTN_LAYERS       = "Couches";
    static public $BTN_MAPS         = "Cartes";
    static public $BTN_RUBRICS      = "Rubriques";
    static public $BTN_IMPORT_DATA  = "Importation";
    static public $BTN_GROUPPROFILS = "Groupes-profil";
    static public $BTN_GROUPSUBDOMS = "Sous-domaines";
    static public $BTN_RUBRICS_AIDE      = "Rubriques aide";
    static public $BTN_STRUCTURE      = "Structures";
    static public $BTN_PROFILS_AUTORISES        = "Profils autorisés";
    //static public $BTN_USERS_AUTORISES = "Utilisateurs";
    static public $BTN_STRUCTURE_AUTORISE = "Structure";

    static public $BTN_OBJECTS_DONNEE     = "Données";
    static public $BTN_OBJECTS_CARTE      = "Cartes";
    static public $BTN_OBJECTS_METADONNEE = "Métadonnées";
    static public $BTN_OBJECTS_MODELE     = "Modèles";
    static public $BTN_OBJECTS_GROUPS     = "Profils";

    // #################################

    static public $RELATIONLIST_PROFILES_MEMBERS_RELATION = "A pour Utilisateurs";
    static public $RELATIONLIST_PROFILES_MEMBERS_TABLE = "Utilisateurs Disponibles";

    static public $RELATIONLIST_PROFILES_ADMIN_RELATION = "Administre";
    static public $RELATIONLIST_PROFILES_ADMIN_TABLE = "Sous-Domaines Disponibles";

    static public $RELATIONLIST_PROFILES_EDITE_RELATION = "Edite";
    static public $RELATIONLIST_PROFILES_EDITE_TABLE = "Sous-Domaines Disponibles";

    static public $RELATIONLIST_PROFILES_DOM_RELATION = "Accède à";
    static public $RELATIONLIST_PROFILES_DOM_TABLE = "N'accède pas à";

    static public $RELATIONLIST_PROFILES_SSDOM_RELATION = "Accède à";
    static public $RELATIONLIST_PROFILES_SSDOM_TABLE = "N'accède pas à";

    static public $RELATIONLIST_PROFILES_COMPETENCE_RELATION = "Accède à";

    static public $RELATIONLIST_PROFILES_COMPETENCE_TABLE = "N'accède pas à";

    static public $RELATIONLIST_PROFILES_PERIMETRE_RELATION = "Consulte / télécharge sur le territoire";
    static public $RELATIONLIST_PROFILES_PERIMETRE_TABLE = "Ne peut pas consulter / télécharger sur le territoire";

    static public $RELATIONLIST_PROFILES_PERIMETRE_EDITION_RELATION = "Edite sur le territoire";
    static public $RELATIONLIST_PROFILES_PERIMETRE_EDITION_TABLE = "Ne peut pas éditer sur le territoire";

    static public $RELATIONLIST_PROFILES_TRT_RELATION = "Peut utiliser";
    static public $RELATIONLIST_PROFILES_TRT_TABLE = "Traitements Disponibles";

    static public $RELATIONLIST_PROFILES_ACTIONS_RELATION = "Droits activés";
    static public $RELATIONLIST_PROFILES_ACTIONS_TABLE = "Droits Disponibles";

    static public $PROFILES_EMPTY_SELECT = "Veuillez choisir un profil dans la liste.";

    // #################################

    static public $RELATIONLIST_OBJECTS_TRT_RELATION = "Accède à";
    static public $RELATIONLIST_OBJECTS_TRT_TABLE = "Traitements disponibles";

    // #################################

    static public $RELATIONLIST_USERS_PROFILES_RELATION = "A pour Profil";
    static public $RELATIONLIST_USERS_PROFILES_TABLE = "Profils Disponibles";

    static public $USERS_EMPTY_SELECT = "Veuillez choisir un utilisateur dans la liste.";

    // #################################

    static public $RELATIONLIST_DOMAINS_SSDOM_RELATION = "A pour Sous-Domaines";
    static public $RELATIONLIST_DOMAINS_SSDOM_TABLE = "Sous-Domaines Disponibles";

    static public $DOMAINS_EMPTY_SELECT = "Veuillez choisir un domaine dans la liste.";

    // #################################

    static public $RELATIONLIST_ACTIONS_PROFILES_RELATION = "Droit activés sur Profils";
    static public $RELATIONLIST_ACTIONS_PROFILES_TABLE = "Profils Disponibles";

    static public $ACTIONS_EMPTY_SELECT = "Veuillez choisir un traitement dans la liste.";

    // #################################

    static public $RELATIONLIST_LAYERS_SSDOM_RELATION = "Sous-Domaines Accédants";
    static public $RELATIONLIST_LAYERS_SSDOM_TABLE = "Sous-Domaines Disponibles";

    static public $RELATIONLIST_LAYERS_COMPETENCE_RELATION = "Téléchargée par la compétence";
    static public $RELATIONLIST_LAYERS_COMPETENCE_TABLE = "Compétences disponibles";

    static public $RELATIONLIST_LAYERS_ALERTPERIM_RELATION = array("Lors de l'édition du territoire", "Alerter les utilisateurs");
    static public $RELATIONLIST_LAYERS_ALERTPERIM_TABLE = array("Territoires concernés", "Utilisateurs avertis");

    static public $RELATIONLIST_COMPETENCE_LAYERS_RELATION = "Couches téléchargées par la compétence";
    static public $RELATIONLIST_COMPETENCE_LAYERS_TABLE = "Couches disponibles";

    static public $RELATIONLIST_MAPS_COMPETENCE_RELATION = "Téléchargée par la compétence";
    static public $RELATIONLIST_MAPS_COMPETENCE_TABLE = "Compétences disponibles";

    static public $RELATIONLIST_COMPETENCE_MAPS_RELATION = "Cartes téléchargées par la compétence";
    static public $RELATIONLIST_COMPETENCE_MAPS_TABLE = "Cartes disponibles";
    static public $LAYERS_EMPTY_SELECT = "Veuillez choisir une couche dans la liste.";

    // #################################

    static public $RELATIONLIST_MAPS_SSDOM_RELATION = "Sous-Domaines Visualisants";
    static public $RELATIONLIST_MAPS_SSDOM_TABLE = "Sous-Domaines Disponibles";

    static public $MAPS_EMPTY_SELECT = "Veuillez choisir une carte dans la liste.";

    // #################################

    static public $COMPETENCE_EMPTY_SELECT = "Veuillez choisir une compétence dans la liste.";

    // #################################

    static public $ZONAGES_EMPTY_SELECT = "Veuillez choisir un zonage dans la liste.";

    // #################################

    static public $PERIMETRES_EMPTY_SELECT = "Veuillez choisir un territoire dans la liste.";


    static public $RELATIONLIST_SUBDOMAINS_DOM_RELATION = "A pour Domaine";
    static public $RELATIONLIST_SUBDOMAINS_DOM_TABLE = "Domaines Disponibles";
    static public $RELATIONLIST_SUBDOMAINS_LAYERS_RELATION = "A pour Couches";
    static public $RELATIONLIST_SUBDOMAINS_LAYERS_TABLE = "Couches Disponibles";
    static public $RELATIONLIST_SUBDOMAINS_MAPS_RELATION = "A pour Cartes";
    static public $RELATIONLIST_SUBDOMAINS_MAPS_TABLE = "Cartes Disponibles";

    static public $SUBDOMAINS_EMPTY_SELECT = "Veuillez choisir un sous-domaine dans la liste.";

    // #################################
    
    static public $RELATIONLIST_PROFILES_RUBRICS_AIDE_RELATION = "Accède à";
    static public $RELATIONLIST_PROFILES_RUBRICS_AIDE_TABLE = "N'accède pas à";
    
    // #################################
    static public $IFRAME_NAME_MAIN = "AdminDroits";
    // #################################
    static public $ROUTE_UPLOAD_TEMP = "catalogue_web_uploadtemp";

    // #################################
    static public $PAGE_TITLE_MAIN = "Gestion des Droits Prodige";
    // #################################
    static public $MAIN_SIZE_X = "100%";//640
    static public $MAIN_SIZE_Y = 440;
    static public $MODULE_SIZE_X ="99%";//640
    static public $MODULE_SIZE_Y =420;//428
    static public $LIST_SIZE_X = 150;
    static public $LIST_SIZE_Y = 420;
    static public $SELECT_LIST_SIZE_X = 145;
    static public $SELECT_LIST_SIZE_Y = 400;
    static public $DETAIL_SIZE_X = 700;
    static public $DETAIL_SIZE_Y = 420;
    static public $SUB_DETAILS_SIZE_X ="100%";
    static public $SUB_DETAILS_SIZE_Y =380;
    static public $SUB_OBJECTS_SIZE_X =500;
    static public $SUB_OBJECTS_SIZE_Y =350;
    // #################################
    static public $LIST_ADD_ACTION = 10;
    static public $LIST_MODIFY_ACTION = 20;
    static public $LIST_DELETE_ACTION = 30;

    static public $SELECTLIST_ADD = "Ajouter";
    static public $SELECTLIST_DELETE = "Supprimer";

    static public $NEWROW = -999;
    // #################################
    static public $RELATIONLIST_ADD_TO_RELATION_ACTION = 111;
    static public $RELATIONLIST_REMOVE_FROM_RELATION_ACTION = 222;
    static public $RELATIONLIST_UPDATE_TO_RELATION_ACTION = 333;

    // #################################

    static public $BTN_NAV_INITIAL = "Accueil";
    static public $BTN_NAV_PROFILES = "Profils";
    static public $BTN_NAV_STRUCTURE = "Structures";
    static public $BTN_NAV_USERS = "Utilisateurs";
    static public $BTN_NAV_RUBRICS = "Rubriques";
    static public $BTN_NAV_DOMAINS = "Domaines";
    static public $BTN_NAV_SUBDOMAINS = "Sous-Domaines";
    //static public $BTN_NAV_ACTIONS = "Traitements";
    static public $BTN_NAV_LAYERS = "Couches";
    static public $BTN_NAV_MAPS = "Cartes";
    static public $BTN_NAV_ZONAGES = "Zonages";
    static public $BTN_NAV_PERIMETRES = "Territoires";
    static public $BTN_NAV_COMPETENCES = "Compétences";
    static public $BTN_NAV_GROUPING = "Regroupements";
    static public $BTN_NAV_RUBRICS_AIDE = "Rubriques aide";

    // #################################
    static public $EDITFORM_BTN_VALIDATE = "Valider";
    static public $EDITFORM_BTN_CANCEL = "Annuler";
    static public $EDITFORM_BTN_DELETE = "Supprimer";
    static public $EDITFORM_WAS_DELETE = "Enregistrement supprimé.";
    static public $EDITFORM_DELETE_CONFIRMATION = "Êtes-vous sûr de vouloir supprimer cet enregistrement ?";
    static public $EDITFORM_VALIDATE = 9111;
    static public $EDITFORM_CANCEL = 9222;
    static public $EDITFORM_DELETE = 9333;

    // #################################
    // ########### PROFILES ############
    // #################################
    static public $MODULE_PROFILES_ACCUEIL =
    "<BR><BR><BR>
        <CENTER>
            Gestion des Profils <BR><BR><BR><BR><BR><BR>
            Veuillez choisir un profil dans la liste de gauche.
        </CENTER>";

    // #################################
    // ###########   USERS  ############
    // #################################

    static public function getTabModuleSheet() {
        $TAB_MODULE_SHEETS = array(
            "default" => array(
                array(
                    "title" => "Ma fiche utilisateur",      "url" => "catalogue_administration_utilisateur"),
                    array("title" => "Droits",              "url" => "catalogue_administration_compte_utilisateur")
            ),
            self::$ACTION_OPEN_PROFILES => array(
                array("title" => self::$BTN_INITIAL,    "url" => "catalogue_administration_profiles_form"),
                array("title" => self::$BTN_MEMBERS,    "url" => "catalogue_administration_profiles_membres_form"),
                array("title" => self::$BTN_ACTIONS,    "url" => "catalogue_administration_profiles_traitements_form"),
                array("title" => self::$BTN_ADMIN,      "url" => "catalogue_administration_profiles_administre_form"),
                array("title" => self::$BTN_DOMAINS,    "url" => "catalogue_administration_profiles_domaines_form"),
                array("title" => self::$BTN_SUBDOMAINS, "url" => "catalogue_administration_profiles_sousdomaines_form"),
                array("title" => self::$BTN_OBJECTS,    "url" => "catalogue_administration_profiles_objects_form"),
                array("title" => self::$BTN_COMPETENCES,"url" => "catalogue_administration_profiles_competences_form")
            ),

            self::$ACTION_OPEN_STRUCTURE => array(
                array("title" => self::$BTN_INITIAL,      "url" => "catalogue_administration_structure_form"),
                //array("title" => self::$BTN_USERS_AUTORISES,      "url" => "catalogue_administration_structure_users_form"),
            ),
            self::$ACTION_OPEN_USERS => array(
                array("title" => self::$BTN_INITIAL,  "url" => "catalogue_administration_users_form"),
                array("title" => self::$BTN_STRUCTURE_AUTORISE,      "url" => "catalogue_administration_structure_users_form"),
                array("title" => self::$BTN_PROFILES, "url" => "catalogue_administration_users_profiles_form"),
                array("title" => "Droits",            "url" => "catalogue_administration_compte_utilisateur"),
                array("title" => self::$BTN_PERIMETRES ,"url" => "catalogue_administration_users_perimetres_form")
            ),

            self::$ACTION_OPEN_DOMAINS => array(
                array("title" => self::$BTN_INITIAL,  "url" => "catalogue_administration_domains_form"),
                array("title" => self::$BTN_SUBDOMAINS,    "url" => "catalogue_administration_domains_subdomains_form"),
            ),

            self::$ACTION_OPEN_SUBDOMAINS => array(
                array("title" => self::$BTN_INITIAL, "url" => "catalogue_administration_subdomains_form"),
                array("title" => self::$BTN_DOMAINS,  "url" => "catalogue_administration_subdomains_subdomainsdomains_form"),
            ),

            self::$ACTION_OPEN_LAYERS => array(
                array("title" => self::$BTN_INITIAL,      "url" => "catalogue_administration_layers_form"),
                array("title" => self::$BTN_COMPETENCES,  "url" => "catalogue_administration_layers_competences_form"),
            ),

            self::$ACTION_OPEN_MAPS => array(
                array("title" => self::$BTN_INITIAL,      "url" => "catalogue_administration_maps_form"),
                array("title" => self::$BTN_COMPETENCES,  "url" => "catalogue_administration_maps_competences_form"),
            ),

            self::$ACTION_OPEN_COMPETENCES => array(
                array("title" => self::$BTN_INITIAL,      "url" => "catalogue_administration_competences_form"),
                array("title" => self::$BTN_PROFILES,      "url" => "catalogue_administration_competences_profiles_form"),
                array("title" => self::$BTN_OBJECTS_DONNEE, "url" => "catalogue_administration_competences_layers_form"),
                array("title" => self::$BTN_OBJECTS_CARTE, "url" => "catalogue_administration_competences_maps_form"),
            ),

            self::$ACTION_OPEN_ZONAGES => array(
                array("title" => self::$BTN_INITIAL,      "url" => "catalogue_administration_zonages_form")
            ),

            self::$ACTION_OPEN_PERIMETRES => array(
                array("title" => self::$BTN_INITIAL,      "url" => "catalogue_administration_perimetres_form"),
                array("title" => self::$BTN_COMPOSTION,   "url" => "catalogue_administration_perimetres_composition_form")
            ),

            self::$ACTION_OPEN_RUBRICS => array(
                array("title" => self::$BTN_INITIAL,      "url" => "catalogue_administration_rubrics_form"),
            ),
            
            self::$ACTION_OPEN_RUBRICS_AIDE => array(
                array("title" => self::$BTN_INITIAL,      "url" => "catalogue_administration_rubrics_aide_form"),
                array("title" => self::$BTN_PROFILS_AUTORISES,      "url" => "catalogue_administration_rubrics_aide_profiles_form"),
            ),


            self::$ACTION_OPEN_GROUPING_PROFILES => array(
                array("title" => self::$BTN_PROFILES,      "url" => "catalogue_administration_grouping_profile_form"),
            ),

            self::$ACTION_OPEN_GROUPING_SUBDOMAINS => array(
                array("title" => self::$BTN_SUBDOMAINS,      "url" => "catalogue_administration_grouping_subdomains_form"),
            ),
            
            self::$ACTION_OPEN_EMPTY => array(
                array("title" => "Sélectionnez une valeur dans la liste de gestion", "url" => "catalogue_administration_empty"),
            )

        );

        
        if(defined("PRO_EDITION") && (PRO_EDITION == "on")) {
            array_push($TAB_MODULE_SHEETS[self::$ACTION_OPEN_USERS], array("title" => self::$BTN_PERIMETRES_EDITION ,"url" => "catalogue_administration_users_perimetres_edition_form"));
            array_push($TAB_MODULE_SHEETS[self::$ACTION_OPEN_PROFILES], array("title" => self::$BTN_EDITE,      "url" => "catalogue_administration_profiles_edit_form"));
            array_push($TAB_MODULE_SHEETS[self::$ACTION_OPEN_LAYERS], array("title" => self::$BTN_ALERTS,       "url" => "catalogue_administration_layers_alerts_form"));
        }
        return $TAB_MODULE_SHEETS;
    }

    private function __construct() {}

    /**
     * Configuration du menu extjs à partir des variable et valeur par défaut de la classe Ressources
     * @param bool $userDetails
     * @return string
     */
    public function getVars($class, $userDetails=false): string
    {
        $pageUrl = $class->generateUrl('catalogue_web_page');
        
        $tabMainMenu = array();
        $strJs = "<script type='text/javascript'>";
        $strJs .= "var defaultPage = '".$pageUrl."?Action=default&iPart=1';";
        $strJs .= "var emptyPage = '".$pageUrl."?Action=".Ressources::$ACTION_OPEN_EMPTY."&iPart=1';";
        
        if(get_class_vars(self::class)) {
            foreach(get_class_vars(self::class) as $var_name=>$var_value ) {
                if(preg_match("!^ACTION_OPEN_!", $var_name)) {
                    $tabMainMenu[$var_name] = "";
                }
                if(preg_match("!^BTN_NAV_!", $var_name)) {
                    $tabMainMenu[str_replace("BTN_NAV_", "ACTION_OPEN_", $var_name)] = $var_name;
                }
                if(preg_match("!^ACTION_OPEN_!", $var_name)
                    || preg_match("!^BTN_NAV_!", $var_name)
                    || preg_match("!^NEWROW!", $var_name)
                    ) {
                        $strJs .= "\n var ".$var_name." = '".addslashes(str_replace("\n", "\\n", $var_value))."';";
                }
            }
        }

        $strJs .= "\n var disableLeftPanel = ".($userDetails ? 'true' : 'false').";";
        $strJs .= "\n var tabMainMenu = {};";
        foreach ($tabMainMenu as $menu_id=>$menu_title){
            if ( $menu_title!="" )
                $strJs .= "\n tabMainMenu[".$menu_id."] = ".$menu_title.";";
        }
        $strJs .= "\n var tabPageAdd = {};";
        $tabSheets = Ressources::getTabModuleSheet();
        foreach ($tabSheets as $menu_id=>$tabMenuDesc){
            $strJs .= "\n tabPageAdd['".$menu_id."'] = \"".(array_key_exists("addurl", $tabMenuDesc[0]) ? $tabMenuDesc[0]["addurl"] : $tabMenuDesc[0]["url"])."\";";
        }
        $strJs .= "</script>";
        return $strJs;
    }

}
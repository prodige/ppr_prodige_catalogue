<?php
/**
 * Onglet compétences dans la gestion des données
 * @author Alkante
 */
  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Components/RelationList/RelationList.php");
  require_once($AdminPath."/Components/SelectList/SelectList.php"); 
  require_once($AdminPath."/Modules/BO/CompetenceVO.php");
  require_once($AdminPath."/Modules/BO/CompetenceAccedeCoucheVO.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CompetenceVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CompetenceAccedeCoucheVO;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php     
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_LAYERS_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if ( $PK==-1 )
  {
    exit;
  }

  // set a default ID
  if ( !isset($_GET["RR"]) )
  {
    $RELATIONLIST_RESULT = -1;
  }
  else
  {
    $RELATIONLIST_RESULT = $_GET["RR"]; 
  }

  $dao = new DAO($conn, 'catalogue');

  // #################################################################
  
  if ( $RELATIONLIST_RESULT != -1 )
  {
    switch( $Action )
    {
      case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
        $ordinalList = array();
        $valueList = array();
        $ordinalList[]=CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_FK_COMPETENCE_ID;
        $ordinalList[]=CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_FK_COUCHE_DONNEES;
        $valueList[]=$RELATIONLIST_RESULT;
        $valueList[]=$PK;

        $CompetenceAccedeCoucheVO_RL = new CompetenceAccedeCoucheVO();
        $CompetenceAccedeCoucheVO_RL->setDao($dao);
        $CompetenceAccedeCoucheVO_RL->InsertValues( $ordinalList, $valueList );
        $CompetenceAccedeCoucheVO_RL->Commit();
      break;
      
      case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
        $CompetenceAccedeCoucheVO_RL = new CompetenceAccedeCoucheVO();
        $CompetenceAccedeCoucheVO_RL->setDao($dao);
        $CompetenceAccedeCoucheVO_RL->DeleteRow( CompetenceAccedeCoucheVO::$PK_COMPETENCE_ACCEDE_COUCHE, $RELATIONLIST_RESULT );
        $CompetenceAccedeCoucheVO_RL->Commit();
      break;
    }
    AlertSaveController::AlertSaveDone("", false);
  }

  // #################################################################

  $CompetenceAccCoucheVO = new CompetenceAccedeCoucheVO();
  $CompetenceAccCoucheVO->setDao($dao);

  $CompetenceAccedeCoucheVO2 = new CompetenceAccedeCoucheVO();
  $CompetenceAccedeCoucheVO2->setDao($dao);
  $CompetenceAccedeCoucheVO2->AddRestriction( CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_FK_COUCHE_DONNEES, $PK );
  $CompetenceAccedeCoucheVO2->AddOnlyKeyProjection( CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_FK_COMPETENCE_ID );

  $competenceVO = new CompetenceVO();
  $competenceVO->setDao($dao);
  $competenceVO->AddNotInRestriction( CompetenceVO::$PK_COMPETENCE_ID, $CompetenceAccedeCoucheVO2 );

  $relationList = new RelationList( "RelationListLayerAccCompetence",
                   Ressources::$RELATIONLIST_LAYERS_COMPETENCE_RELATION,
                   $CompetenceAccCoucheVO,
                   CompetenceAccedeCoucheVO::$PK_COMPETENCE_ACCEDE_COUCHE,
                   CompetenceAccedeCoucheVO::$COMPETENCE_COUCHE_COMPETENCE_NOM,
                   CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_FK_COUCHE_DONNEES,
                   $PK,
                   CompetenceAccedeCoucheVO::$COMPETENCECOUCHE_FK_COMPETENCE_ID,
                   $competenceVO,
                   Ressources::$RELATIONLIST_LAYERS_COMPETENCE_TABLE,
                   CompetenceVO::$PK_COMPETENCE_ID,
                   CompetenceVO::$COMPETENCE_NOM,
                   '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Layers/LayersCompetences.php",*/
                   Ressources::$IFRAME_NAME_MAIN,
                   $this
                   );
  
?>
  </body>
</html>
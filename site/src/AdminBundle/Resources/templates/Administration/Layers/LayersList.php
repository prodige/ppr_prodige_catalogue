<?php
/**
 * Liste des couches (haut) dans l'adminsitration des couches (administration des droits)
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/CoucheDonneesVO.php");
	require_once($AdminPath."/Components/SelectList/SelectList.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CoucheDonneesVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;

  header("Content-type: application/xml");
  echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 

  $dao = new DAO($conn, 'catalogue');

  $coucheDonneesVO = new CoucheDonneesVO();
  $coucheDonneesVO->setDao($dao);

	//ne pas afficher les ensembles de séries de données
	$coucheDonneesVO2 = new CoucheDonneesVO();
	$coucheDonneesVO2->setDao($dao);
	$coucheDonneesVO2->AddRestriction( $coucheDonneesVO2::$COUCHD_TYPE_STOCKAGE, -1 );
	$coucheDonneesVO2->AddOnlyKeyProjection( $coucheDonneesVO2:: $PK_COUCHE_DONNEES );
	
	$coucheDonneesVO->AddNotInRestriction( CoucheDonneesVO::$PK_COUCHE_DONNEES, $coucheDonneesVO2 );
	
	$selectList = new SelectList(	 "SelectListCoucheDonneesVO",
									 $coucheDonneesVO,
									 CoucheDonneesVO::$PK_COUCHE_DONNEES,
									 CoucheDonneesVO::$COUCHD_NOM,
									 Ressources::$IFRAME_NAME_MAIN,
									 '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Layers/LayersDetail.php",*/
									 "800",
									 "100"
									 );

?>
	</body>
</html>
<?php
/**
 * Onglet compétences dans la gestion des données
 * @author Alkante
 */
  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Components/RelationList/DoubleRelationList.php");
  require_once($AdminPath."/Components/SelectList/SelectList.php"); 
  require_once($AdminPath."/Modules/BO/UserAlertePerimetreEditionVO.php");
  require_once($AdminPath."/Modules/BO/UserAccedePerimetreVO.php");
  require_once($AdminPath."/Modules/BO/PerimetreVO.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\DoubleRelationList;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UsrAlertePerimetreEditionVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UsrAccedePerimetreVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\PerimetreVO;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;
  use Prodige\ProdigeBundle\DAOProxy\ViewObject;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php     
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_LAYERS_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if ( $PK==-1 )
  {
    exit;
  }

  // set a default ID
  if ( !isset($_GET["RR"]) )
  {
    $RELATIONLIST_RESULT = -1;
  }
  else
  {
    $RELATIONLIST_RESULT = $_GET["RR"]; 
  }

  $dao = new DAO($conn, 'catalogue');

  // #################################################################

  if ( $RELATIONLIST_RESULT != -1 )
  {

    switch( $Action )
    {
      case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
        $ordinalList = array();
        $valueList = array();
        $ordinalList[]=UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_FK_ALERTEDITION_ID;
        $ordinalList[]=UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_FK_COUCHE_DONNEES;
        $valueList[]=$RELATIONLIST_RESULT;
        $valueList[]=$PK;

        $UsrAlertePerimetreEditionVO_RL = new UsrAlertePerimetreEditionVO();
        $UsrAlertePerimetreEditionVO_RL->setDao($dao);
        $UsrAlertePerimetreEditionVO_RL->InsertValues( $ordinalList, $valueList );
        $UsrAlertePerimetreEditionVO_RL->Commit();
      break;
      
      case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
        $UsrAlertePerimetreEditionVO_RL = new UsrAlertePerimetreEditionVO();
        $UsrAlertePerimetreEditionVO_RL->setDao($dao);
        $UsrAlertePerimetreEditionVO_RL->DeleteRow( UsrAlertePerimetreEditionVO::$PK_ALERTEDITION_ACCEDE_COUCHE, $RELATIONLIST_RESULT );
        $UsrAlertePerimetreEditionVO_RL->Commit();
      break;
    }
    AlertSaveController::AlertSaveDone("", false);
  }

  // #################################################################

  //global $var_dump; $var_dump = true;
  $UsrAlertePerimetreEditionVO2 = new UsrAlertePerimetreEditionVO();
  $UsrAlertePerimetreEditionVO2->setDao($dao);

  $filter_UsrAlertePerimetreEditionVO2 = new UsrAlertePerimetreEditionVO();
  $filter_UsrAlertePerimetreEditionVO2->setDao($dao);
  $filter_UsrAlertePerimetreEditionVO2->AddRestriction( UsrAlertePerimetreEditionVO::$USRALERTERIMETRE_FK_COUCHEDONNEES, $PK );
  $filter_UsrAlertePerimetreEditionVO2->AddOnlyKeyProjection( array(UsrAlertePerimetreEditionVO::$USRALERTERIMETRE_FK_UTILISATEUR, UsrAlertePerimetreEditionVO::$USRALERTERIMETRE_FK_PERIMETRE) );

  
  $usrAccedePerimetreVO = new UsrAccedePerimetreVO();
  $usrAccedePerimetreVO->setDao($dao);
  $usrAccedePerimetreVO->AddNotInRestriction( array(UsrAccedePerimetreVO::$USRPERIM_FK_PERIMETRE, UsrAccedePerimetreVO::$USRPERIM_FK_UTILISATEUR), $filter_UsrAlertePerimetreEditionVO2 );


  $relationList = new DoubleRelationList( "RelationListLayerAccAlertEdition",
                   Ressources::$RELATIONLIST_LAYERS_ALERTPERIM_RELATION,
                   $UsrAlertePerimetreEditionVO2,
  		
                   UsrAlertePerimetreEditionVO::$USRALERTERIMETRE_FK_UTILISATEUR, // $relationKeyOrdinal2,
                   UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_USR_NOM,       // $relationValueOrdinal2,
                   UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_USR_ID,        // $RelationFKTable2,
  		
  		             UsrAlertePerimetreEditionVO::$USRALERTERIMETRE_FK_PERIMETRE,   // $relationKeyOrdinal1,
                   UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_PERIMETRE_NOM, // $relationValueOrdinal1,
                   UsrAlertePerimetreEditionVO::$USRALERTPERIMETRE_PERIMETRE_ID,  // $RelationFKTable1,
  		
                   UsrAlertePerimetreEditionVO::$USRALERTERIMETRE_FK_COUCHEDONNEES, // $relationPKRestrictionOrdinal
                   $PK,   // $relationPKRestrictionValue
                   
                   $usrAccedePerimetreVO,                                    // $tableViewObject
                   Ressources::$RELATIONLIST_LAYERS_ALERTPERIM_TABLE,
  		
                   UsrAccedePerimetreVO::$USRPERIM_FK_UTILISATEUR,
                   UsrAccedePerimetreVO::$USRACCPERIMETRE_PERIMETRE_NOM,
  		
                   UsrAccedePerimetreVO::$USRPERIM_FK_PERIMETRE,
                   UsrAccedePerimetreVO::$USRACCPERIMETRE_USR_ID,
  		
                   '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Layers/LayersAlerts.php",*/
                   Ressources::$IFRAME_NAME_MAIN
                   );
  
?>
  </body>
</html>
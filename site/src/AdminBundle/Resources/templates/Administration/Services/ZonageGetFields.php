<?php 

//$AdminPath = "../../";
//require_once($AdminPath."DAO/DAO/DAO.php");

//require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php"); //TODO hismail - A voir avec Vincent
use Prodige\ProdigeBundle\DAOProxy\DAO;

$callback = isset($_GET["callback"]) ? $_GET["callback"] : '';
$table = isset ($_GET["table"]) ? $_GET["table"] : "";

//ConnectionFactory::BeginTransaction(); //TODO hismail - A voir avec Vincent
//$dao = new DAO();
$dao = new DAO($conn, 'public');

if ($dao)
{
  $schemaName = "public";
  $tabInfoTables = explode(".", $table);
  if (isset($tabInfoTables[1])){
    $table = $tabInfoTables[1];
    $schemaName = $tabInfoTables[0];
  }
  $query = "SELECT a.attnum, a.attname ".
     " FROM pg_class c, pg_attribute a, pg_type t, pg_namespace n " .
     " WHERE c.relname = '".($table!="" ? $table : "prodige_perimetre" )."'".
     " AND n.nspname = '".mb_strtolower($schemaName)."'".
     " and a.attnum > 0".
     " and a.attname <> 'the_geom'".
     " and a.attrelid = c.oid".
     " and a.atttypid = t.oid". 
     " and n.oid = c.relnamespace".
     " ORDER BY attnum";
  $rs = $dao->BuildResultSet($query);
  $lastType = '';
  $lastSDom = '';
  $param = "[";
  for ($rs->First(); !$rs->EOF(); $rs->Next()){
    if($rs->read(1)!="gid" && $rs->read(1)!="the_geom")
      $param.= "'".$rs->read(1)."',";
  }
  $param = substr($param,0, -1);
  if($param!="")
    $param.= "]";
  
  //ConnectionFactory::CloseConnection(); //TODO hismail - A voir avec Vincent
  echo $callback."(".$param.")";
}





?>
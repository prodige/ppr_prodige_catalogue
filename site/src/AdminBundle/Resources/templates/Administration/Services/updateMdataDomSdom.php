<?php

/**
 * @brief service qui permet de mettre à jour les métadonnées lors d'un changement de domaine/sous domaine
 * @author Alkante
 * @param id    identifiant du contact (attribut id, balise CI_ResponsibleParty du fichier XML)
 * @param file  chemin vers le fichier des contacts, à partir de "xml/codelist/", sans l'extension du fichier (fichier xml uniquement)
 */

define("WRITE_LOG", false);

write_log("****************************************************************************************************************");
write_log("DEBUT : mise à jour des métadonnées");

if ( isset($_REQUEST['mode']) &&  $_REQUEST['mode'] != "") {
  $mode = $_REQUEST['mode'];
} else {
  write_log("ERREUR : paramètre 'mode' manquant.");
  $result['success'] = false;
  $result['msg']     = "Le param&egrave;tre 'mode' est manquant.";
  echo json_encode($result);
  exit();
}

if ( isset($_REQUEST['id']) &&  $_REQUEST['id'] != "") {
  $id = $_REQUEST['id'];
} else {
  write_log("ERREUR : paramètre 'id' manquant.");
  $result['success'] = false;
  $result['msg']     = "Le param&egrave;tre 'id' est manquant.";
  echo json_encode($result);
  exit();
}

use Prodige\ProdigeBundle\DAOProxy\DAO;

write_log("paramètres :" .
          "\n\t\t\t\t"."id = ".$id .
          "\n\t\t\t\t"."mode = ".$mode);

$tab_metadata_index_success = array();
$tab_metadata_index_failed = array();
$tab_metadata = array();



//$dao = new DAO();
//$dao->setSearchPath("public,catalogue");
$dao = new DAO($conn, 'public,catalogue');

$bUpdateMetadata = true;
switch($mode){
    case "delsubdomain":
        $filterField = "pk_sous_domaine";
        $nameField = "ssdom_nom";
        $bUpdate = false;
    break;
    case "deldomain":
        $filterField = "pk_domaine";
        $nameField = "dom_nom";
        $bUpdate = false;
    break;
    case "updatedomain": 
        $filterField = "pk_domaine";
        $nameField = "dom_nom";
        $newName = $_REQUEST["newDomain"];
        
        $rs = $dao->BuildResultSet("select 1 from domaine where ".$filterField." = :id and $nameField<>:name", array('id'=>$id, "name"=>$newName));
        $rs->First();
        if ( $rs->EOF() ) {//no changes
            $bUpdateMetadata = false;
        }
        $bUpdate = true;
    break;
    case "updatesubdomain": 
        $filterField = "pk_sous_domaine";
        $nameField = "ssdom_nom";
        $newName = $_REQUEST["newSubDomain"];
        $rs = $dao->BuildResultSet("select 1 from sous_domaine where ".$filterField." = :id and $nameField<>:name", array('id'=>$id, "name"=>$newName));
        $rs->First();
        if ( $rs->EOF() ) {//no changes
            $bUpdateMetadata = false;
        }
        $bUpdate = true;
    break;
}

// A voir avec Vincent
/*$strSql = "SELECT id, data , ".$nameField." FROM metadata ".
          " inner join couche_sdom on  metadata.id = int8(couche_sdom.fmeta_id)".
          " where ".$filterField." = ".$id.
          " union SELECT id, data , ".$nameField." FROM metadata ".
          " inner join cartes_sdom on  metadata.id = int8(cartes_sdom.fmeta_id)".
          " where ".$filterField." = ".$id;*/
$strSql = "SELECT id, data , ".$nameField.", 'couche' as mode FROM metadata ".
    " inner join couche_sdom on  metadata.id = int8(couche_sdom.fmeta_id)".
    " where ".$filterField." = :id".
    " union SELECT id, data , ".$nameField.", 'carte' as mode FROM metadata ".
    " inner join cartes_sdom on  metadata.id = int8(cartes_sdom.fmeta_id)".
    " where ".$filterField." = :id";


$rs     = $dao->BuildResultSet($strSql, array('id'=>$id));

for ( $rs->First(); $bUpdateMetadata && !$rs->EOF(); $rs->Next() ) {
  $metadata_id   = $rs->Read(0);
  $metadata_data = $rs->Read(1);
  $name = $rs->Read(2);
  $mode = $rs->Read(3);
  
  //chargement XML
  $version  = "1.0";
  $encoding = "UTF-8";
  $metadata_doc = new \DOMDocument($version, $encoding);
  
  $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
  $metadata_data = $entete.$metadata_data;
  $metadata_data = str_replace("&", "&amp;", $metadata_data);
  $metadata_doc->loadXML(($metadata_data));
  
  //Modification XML
  $this->forward('Prodige\ProdigeBundle\Controller\UpdateDomSdomController::updateDomSdomAction', array(
      'fmeta_id'  => $metadata_id,
      'modeData' => $mode
  ));
  /*
  if($bUpdate)
    $bOK = update_domsdom($metadata_doc, $name, $newName); 
  else
    $bOK = del_domsdom($metadata_doc, $name);
  if ( $bOK ) {
    // save new metadata_data
    $new_metadata_data = $metadata_doc->saveXML();
    $new_metadata_data = str_replace($entete, "", $new_metadata_data);
    $strSql = "UPDATE metadata SET data='".(str_replace("'", "''",$new_metadata_data))."'".
              " WHERE id=".$metadata_id;
    if ( pg_result_status($dao->Execute($strSql)) == 1 ) {
      $dao->Execute("COMMIT");
      write_log("SUCCESS : Mise à jour de la métadonnée ".$metadata_id." réussie ( $nameField : ".$name.")");
    } else {
      write_log("ECHEC : Mise à jour de la métadonnée ".$metadata_id." échouée\n".
                $strSql);
    }
    // reindex metadata
    if ( metadata_index($metadata_id) ) {
      $tab_metadata_index_success[] = $metadata_id;
    } else {
      $tab_metadata_index_failed[] = $metadata_id;
    }
    $tab_metadata[] = $metadata_id;
  }*/
}


unset($dao);

write_log("FIN : mise à jour terminée (métadonnées : ".implode(", ", $tab_metadata).")");
write_log("****************************************************************************************************************");

$result['success']     = true;
$result['msg']         = ("Les métadonnées ont bien été mises à jour");
$result['metadata_index_success'] = implode(", ", $tab_metadata_index_success);
$result['metadata_index_failed'] = implode(", ", $tab_metadata_index_failed);
echo json_encode($result);
exit();


/*******************************************************************************************************
 * @brief supprime un tag Dom/sDom
 * @param doc             DOMDocument du XML de la métadonnée
 * @param domSdom_name    nom du domaine ou du sous-domaine
 * @return true si la métadonnée a été mis à jour, false sinon
 *******************************************************************************************************/
function del_domsdom(&$doc, $domSdom_name){
  $bUpdate = false;
  
  $xpath = new \DOMXpath($doc);
  //serach the data
  $keyword_list = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/fra:FRA_DataIdentification/gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:keyword/gco:CharacterString");
  if($keyword_list){
	  for ($i = 0; $i < $keyword_list->length; $i++)
	  { 
	    $keyword = $keyword_list->item($i)->nodeValue;
	    //valeur trouvée, on enlève le parent du parent... (gmd:descriptiveKeywords)
	    if ($keyword==$domSdom_name){
	    	$keyword_list->item($i)->parentNode->parentNode->parentNode->parentNode->removeChild($keyword_list->item($i)->parentNode->parentNode->parentNode);
	    }
	  }
  }        
  //ISO 19139 metadatas
  $keyword_list = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:keyword/gco:CharacterString");
  if($keyword_list){
	  for ($i = 0; $i < $keyword_list->length; $i++)
	  { 
	    $keyword = $keyword_list->item($i)->nodeValue;
	    //valeur trouvée, on enlève le parent du parent (gmd:descriptiveKeywords)
	    if ($keyword==$domSdom_name){
	      $keyword_list->item($i)->parentNode->parentNode->parentNode->parentNode->removeChild($keyword_list->item($i)->parentNode->parentNode->parentNode);
	    }
	  }
  }
  //services metadata
  $keyword_list = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:keyword/gco:CharacterString");
  if($keyword_list){
    for ($i = 0; $i < $keyword_list->length; $i++)
    { 
      $keyword = $keyword_list->item($i)->nodeValue;
      //valeur trouvée, on enlève le parent du parent (gmd:descriptiveKeywords)
      if ($keyword==$domSdom_name){
        $keyword_list->item($i)->parentNode->parentNode->parentNode->parentNode->removeChild($keyword_list->item($i)->parentNode->parentNode->parentNode);
      }
    }
  }
  
  $bUpdate = true;
  return $bUpdate;
}

/*******************************************************************************************************
 * @brief modifie un tag Dom/sDom
 * @param doc             DOMDocument du XML de la métadonnée
 * @param domSdom_name    nom du domaine ou du sous-domaine
 * @param newDomSdom_name nom du nouveau domaine ou sous-domaine
 * @return true si la métadonnée a été mis à jour, false sinon
 *******************************************************************************************************/
function update_domsdom(&$doc, $domSdom_name, $newDomSdom_name){
  $bUpdate = false;
  
  $xpath = new \DOMXpath($doc);
  //serach the data
  $keyword_list = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/fra:FRA_DataIdentification/gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:keyword/gco:CharacterString");
  if($keyword_list){
	  for ($i = 0; $i < $keyword_list->length; $i++)
	  { 
	    $keyword = $keyword_list->item($i)->nodeValue;
	    //valeur trouvée, on la remplace
	    if ($keyword==$domSdom_name){
	      $keyword_list->item($i)->nodeValue = $newDomSdom_name;
	    }
	  }
  }        
  //ISO 19139 metadatas
  $keyword_list = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:keyword/gco:CharacterString");
  if($keyword_list){
	  for ($i = 0; $i < $keyword_list->length; $i++)
	  { 
	    $keyword = $keyword_list->item($i)->nodeValue;
	    //valeur trouvée, on enlève le parent du parent (gmd:descriptiveKeywords)
	    if ($keyword==$domSdom_name){
	      $keyword_list->item($i)->nodeValue = $newDomSdom_name;
	    }
	  }
  }
  //services metadata
  $keyword_list = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:keyword/gco:CharacterString");
  if($keyword_list){
    for ($i = 0; $i < $keyword_list->length; $i++)
    { 
      $keyword = $keyword_list->item($i)->nodeValue;
      //valeur trouvée, on enlève le parent du parent (gmd:descriptiveKeywords)
      if ($keyword==$domSdom_name){
        $keyword_list->item($i)->nodeValue = $newDomSdom_name;
      }
    }
  }
  $bUpdate = true;
  return $bUpdate;
}
/*******************************************************
 * @deprecated v3.2 - 5 mars 12
 * @todo le service n'existe plus, utiliser un autre moyen de réindexer la métadonnée
 * @brief réindexe une métadonnée
 * @param metadata_id   identifiant de la métadonnée
 * @return true si l'indexation a réussi, false sinon
 *******************************************************/
function metadata_index($metadata_id){
  $bRes = false;

  return $bRes;
}

/******************************************
 * @brief écrit dans le fichier de log
 * @param msg message à écrire
 ******************************************/
function write_log($msg){
  if ( defined("WRITE_LOG") && WRITE_LOG ) {
    if ( $log = fopen("updateDomSdom.log", "a") ) {
      fwrite($log, "[".date('Y-m-d')."T".date('G:i:s')."] ".$msg."\n");
      fclose($log);
    }
  }
}

?>
<?php

//$AdminPath = "../Administration/";

//require_once($AdminPath."DAO/DAO/DAO.php");
//require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php");
use Prodige\ProdigeBundle\DAOProxy\DAO;

//ConnectionFactory::BeginTransaction();

//$dao = new DAO();
$dao = new DAO($conn, 'parametrage');
$dao->beginTransaction();

$callback = isset($_GET["callback"]) ? $_GET["callback"] : '';

$res = array();

$layer_name = isset($_GET["layer_name"]) ? $_GET["layer_name"] : '';

if((isset($_GET["layer_name"])) && (trim($_GET["layer_name"]) != "")) {

    // for test
    //$layer_name = "ign_metropole_communes_geofla_2013_z";

    //Récupération des modèles d'une vue en json;
    //$strSql = "SELECT pk_modele_id, modele_name FROM parametrage.prodige_msg_template WHERE layer_name='".$layer_name."';";
    $strSql = "SELECT pk_modele_id, modele_name FROM parametrage.prodige_msg_template WHERE layer_name=?;";
    //die($strSql);

    //$rs = $dao->Execute($strSql);
    $rs     = $dao->BuildResultSet($strSql, array($layer_name));

    if($rs) {
        // TODO hismail - A vérifier avec Vincent
        //while($row = pg_fetch_assoc($rs)) {
        for ( $rs->First(); !$rs->EOF(); $rs->Next() ) {
            $field = array();
            //$field['pk_modele_id'] = $row['pk_modele_id'];
            $field['pk_modele_id'] = $rs->Read(0);
            //$field['modele_name'] = $row['modele_name'];
            $field['modele_name'] = $rs->Read(1);
            array_push($res, $field);
        }
    }
}

echo $callback."(".json_encode($res).")";

//ConnectionFactory::CloseConnection();
$dao->commit();

?>
<?php

use Prodige\ProdigeBundle\DAOProxy\DAO;

// hismail
//Source Module admincarto - PRA/Administration/Administration/Perimetres/perimetreComposition.php
//$AdminPath = "../../";
//require_once($AdminPath."DAO/DAO/DAO.php");

//require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php");

//ConnectionFactory::BeginTransaction();

//$dao = new DAO();

//$init_zonage_field_id = (isset($_POST["init_zonage_field_id"]) ? $_POST["init_zonage_field_id"] : $_GET["init_zonage_field_id"]) ;
$init_zonage_field_id = (isset($tabParam["init_zonage_field_id"]) ? $tabParam["init_zonage_field_id"] : (isset($_GET["init_zonage_field_id"]) ? $_GET["init_zonage_field_id"] : null));
//$init_zonage_field_name = (isset($_POST["init_zonage_field_name"]) ? $_POST["init_zonage_field_name"] : $_GET["init_zonage_field_name"]) ;
$init_zonage_field_name = (isset($tabParam["init_zonage_field_name"]) ? $tabParam["init_zonage_field_name"] : (isset($_GET["init_zonage_field_name"]) ? $_GET["init_zonage_field_name"] : null));

//$zonage_field_id = (isset($_POST["zonage_field_id"]) ? $_POST["zonage_field_id"] : $_GET["zonage_field_id"]) ;
$zonage_field_id = (isset($tabParam["zonage_field_id"]) ? $tabParam["zonage_field_id"] : (isset($_GET["zonage_field_id"]) ? $_GET["zonage_field_id"] : null));
//$perimetre_code = (isset($_POST["perimetre_code"]) ? $_POST["perimetre_code"] : $_GET["perimetre_code"]) ;
$perimetre_code = (isset($tabParam["perimetre_code"]) ? $tabParam["perimetre_code"] : (isset($_GET["perimetre_code"]) ? $_GET["perimetre_code"] : null));

//Added by hismail
if(!$init_zonage_field_id || !$init_zonage_field_name || !$zonage_field_id || !$perimetre_code) {
    exit();
}

//$strParam = "?perimetre_code=".$perimetre_code."&zonage_field_id=".$zonage_field_id."&init_zonage_field_id=".$init_zonage_field_id."&init_zonage_field_name=".$init_zonage_field_name;
$strParam = "/".$perimetre_code."/".$zonage_field_id."/".$init_zonage_field_id."/".$init_zonage_field_name;

if(isset($_POST["selection"]) && !empty($_POST["selection"])) {

    $strListValues ="";
    foreach($_POST["selection"] as $key => $value)
        $strListValues .= "'".$value."',";

    $strListValues = substr($strListValues, 0, -1);
    $query = "update prodige_perimetre set ".$zonage_field_id." = null where ".$zonage_field_id." = '".$perimetre_code."'";
    // echo $query."<br>";
    $dao = new DAO($conn_prodige, 'public');
    $dao->Execute($query);

    $query = "update prodige_perimetre set ".$zonage_field_id."='".$perimetre_code."' where ".$init_zonage_field_id." in (".$strListValues.")";

    $dao->Execute($query);
    //ConnectionFactory::CommitTransaction();
    //header("location:http://".$PRO_SITE_URL."/PRRA/Administration/Administration/Perimetres/PerimetresReload.php");
    header($perimetresReloadUrl);
    exit();
}

$dao_prodige = new DAO($conn_prodige, 'public');

if($dao_prodige) {
 $rs = $dao_prodige->BuildResultSet('select '.$init_zonage_field_id.', '.$init_zonage_field_name.', '.$zonage_field_id.' from prodige_perimetre  '.
     ' order by '.$init_zonage_field_name);
 $strJs="";
 for ($rs->First(); !$rs->EOF(); $rs->Next())
 {
     if ($perimetre_code == $rs->Read(2)){
         $strJs .= '{"id":"'.$rs->Read(0).'","name":"'.$rs->Read(1).'", "insee" : "'.$rs->Read(0).'","selection":"<input type=\"checkbox\" checked value=\"'.$rs->Read(0).'\" name=\"selection[]\" onclick=\"onClickSelection(this);\"\/>"},';
     }else if($rs->Read(2) == ""){
         $strJs .= '{"id":"'.$rs->Read(0).'","name":"'.$rs->Read(1).'", "insee" : "'.$rs->Read(0).'","selection":"<input type=\"checkbox\"  value=\"'.$rs->Read(0).'\" name=\"selection[]\" onclick=\"onClickSelection(this);\"\/>"},';
     }
 }
 $strJs = substr($strJs, 0, -1);
 /*
  $rs = $dao->BuildResultSet('select '.$init_zonage_field_id.', '.$init_zonage_field_name.', '.$zonage_field_id.' from prodige_perimetre where '.
  $zonage_field_id.' is null  order by '.$init_zonage_field_name);
  for ($rs->First(); !$rs->EOF(); $rs->Next())
  {
  $strJsDispo .= '{"id":"'.$rs->Read(0).'","name":"'.$rs->Read(1).'('.$rs->Read(0).')","selection":"<input type=\"checkbox\"  value=\"'.$rs->Read(0).'\" name=\"selection[]\" onclick=\"onClickSelection(this);\"\/>"},';
  }
  $strJs = substr($strJs, 0, -1);
  */
}
//ConnectionFactory::CloseConnection();

 ?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Gestion des Droits Prodige</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
  </head>
  <body style="margin:0px;">
      <script type='text/javascript'>
      function onClickSelection(oCheck)
      {
        var element = new Ext.Element(adminTabPanelMain.getActiveTab().gridList.view.findRow(oCheck), true);
        if (oCheck.checked)
          element.addClass('x-grid3-row-alt');
        else
          element.removeClass('x-grid3-row-alt');
      }
      function SelectAll(bSelect)
      {
        var oForm = document.getElementById('form_RelationListPerimetreComposition');
        var oChecks = oForm.elements["selection[]"];
        if ( !oChecks.item ){
          oChecks.checked = bSelect;
          oChecks.onclick();
          return;
        }
        for (var i=0; i < oChecks.length; i++){
          oChecks[i].checked = bSelect;
          oChecks[i].onclick();
        }
      }
      function ChangeEnabled(oManage, OBJET_PK, listTraitement)
      {
        var tabTraitement = listTraitement.split(",");
        for (var i=0; i < tabTraitement.length; i++){
          var oTraitement = document.getElementById('traitement['+OBJET_PK+']['+tabTraitement[i]+']');
          oTraitement.disabled = !oManage.checked;
        }
      }

      adminTabPanelMain.getActiveTab().add(new Ext.FormPanel({
        border: false,
        autoWidth : true,
        autoHeight : true,
        refOwner : adminTabPanelMain.getActiveTab(),
        ref : "../formPanel",
        renderTo : adminTabPanelMain.getActiveTab().id,
        standardSubmit: true,
        baseParams: {
            foo: 'bar'
        },
        id : 'panelform_RelationListPerimetreComposition',
        formId : 'form_RelationListPerimetreComposition',
        url: '',
        items: [
          new Ext.grid.GridPanel({
            ref : "../../gridList",
            region : 'center',
            height : 400,
            border : true,
            store : new Ext.data.Store({
                reader : new Ext.data.JsonReader({
                idProperty: 'id',
                fields: [
                  {name : 'id'}, {name : 'name'}, {name : 'insee'}, {name : 'selection'}                ]
              }),
              data : [
                      <?php echo $strJs;?>
                      ]     }),
            id : 'grid_RelationListPerimetreComposition',
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                  sortable: true, hideable:false, menuDisabled:true, resizable: true
                },
                columns: [
                  { id: 'col_name', dataIndex: 'name',  header: "<b>Nom</b>"},
                  { id: 'col_insee', dataIndex: 'insee',  header: "<b>Code INSEE</b>"},
                  { id: 'col_selection', sortable: false, dataIndex: 'selection',  header: "<a onclick=\"SelectAll(true)\">Tous<\/a>&nbsp;\/&nbsp;<a onclick=\"SelectAll(false)\">Aucun<\/a>", align:'center', width:100}  
                ]
            }),
            hideHeaders : false,

            view: new Ext.grid.GridView({
              // render rows as they come into viewable area.
              scrollDelay: false,

              deferEmptyText : false,
              forceFit: true,
              showPreview: true, // custom property
              enableRowBody: true, // required to create a second, full-width row to show expanded Record data

              // for correlated sdrs
              getRowClass: function(record, index, rowParam, store) {
                if ( record.id=="-1" || record.id=="-2" ){
                  return "x-grid3-header";
                }
                                return "";
              }
            }),
            loadMask : {msg : "Chargement en cours..."},
            disableSelection : true,
            autoExpandColumn : 'col_name',
            iconCls: 'icon-grid',
            margins : '5 5 5 5',
            buttonAlign : 'center',
            border : true,
            buttons : [
                            new Ext.Button({
                defaults : {buttonAlign:'center'}, 
                text : 'Enregistrer la configuration',
                margins : '5 5 5 5',
                handler : function(){
                  var oForm = document.getElementById('form_RelationListPerimetreComposition');
                  //oForm.action = '---php echo $CARMEN_URL_SERVER_BACK. "/PRRA/Administration/Administration/Perimetres/perimetreComposition.php".$strParam;---php';
                  oForm.action = '<?php echo $perimetresCompositionServiceUrl.$strParam; ?>';
                  oForm.target = 'AdminDroits';
                  oForm.submit();
                }
              })
                          ],
            listeners : {
              beforeRender : function() {
                myMask = new Ext.LoadMask(Ext.getBody(), {msg:"Merci de patienter..."});
                myMask.show();
              },
              afterrender : {
                fn : function(grid){
                  myMask.hide();
                  grid.resizeEnable = true;
                  updateGridListSize(grid);
                  grid.on("afterrender", updateGridListSize);
                  Ext.getCmp('module_details').getActiveTab().gridList = grid;
                  tabGridList.push(grid);
                },
                delay : 100, 
                single : true
              }
            }
          })
        ]
      }));

      </script>
        </body>

</html>
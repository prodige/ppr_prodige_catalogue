<?php 

//$AdminPath = "../../";
//require_once($AdminPath."DAO/DAO/DAO.php");
use Prodige\ProdigeBundle\DAOProxy\DAO;

//require_once($AdminPath."DAO/ConnectionFactory/ConnectionFactory.php"); //TODO hismail - A voir avec Vincent

$conn instanceof \Doctrine\DBAL\Connection;
$callback = (isset($_GET["callback"]) ? $_GET["callback"] : '');
$fieldId = (isset($_GET["fieldId"]) ? $_GET["fieldId"] : "");
$fieldName = (isset($_GET["fieldName"]) ? $_GET["fieldName"] : "");
$fieldLiaison = (isset($_GET["fieldLiaison"]) ? $_GET["fieldLiaison"] : "");
$fieldIdNiveau1 = (isset($_GET["fieldIdNiveau1"]) ? $_GET["fieldIdNiveau1"] : "");
$table = (isset($_GET["table"]) ? $_GET["table"] : "");

if ($fieldId !="" && $fieldName!=""){
  
    $conn->beginTransaction(); //TODO hismail - A voir avec Vincent
    //$dao = new DAO();
    $dao = new DAO($conn, 'public');
    if ($dao) {
        try {
            $query = "SELECT a.attnum, a.attname ".
               " FROM pg_class c, pg_attribute a, pg_type t " .
               " WHERE c.relname = 'prodige_perimetre'".
               " and a.attnum > 0".
               " and a.attname <> 'the_geom'".
               " and a.attrelid = c.oid".
               " and a.atttypid = t.oid". 
               " ORDER BY attnum";
            $rs = $dao->BuildResultSet($query);
            $lastType = '';
            $lastSDom = '';
            $param = "[";
            $bHasFieldId = false;
            $bHasFieldName = false;
            for ($rs->First(); !$rs->EOF(); $rs->Next()){
              $bHasFieldId = $bHasFieldId || ($fieldId ==$rs->Read(1));
              $bHasFieldName = $bHasFieldName || ($fieldName ==$rs->Read(1));
            }
            if(!$bHasFieldName && !$bHasFieldId){
              $query = "alter table prodige_perimetre add column ".$fieldId." character varying;".
                       "alter table prodige_perimetre add column ".$fieldName." character varying;";
              $query .= " update prodige_perimetre set ".$fieldId." =  ".$table.".".$fieldId." from ".$table." where prodige_perimetre.".$fieldIdNiveau1."::text = ".$table.".".$fieldLiaison."::text;";
              $query .= " update prodige_perimetre set ".$fieldName." =  ".$table.".".$fieldName." from ".$table." where prodige_perimetre.".$fieldIdNiveau1."::text = ".$table.".".$fieldLiaison."::text;";
              
              $dao->Execute($query);
            }
            $conn->commit();
            echo $callback."({success:true})";
        } catch (Exception $exception){
            $conn->rollBack();
            echo $callback."({success:false})";
        }
    }
  
}




?>
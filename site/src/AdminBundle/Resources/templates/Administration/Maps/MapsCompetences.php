<?php
/**
 * Onglet compétences dans la gestion des cartes
 * @author Alkante
 */
  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Components/RelationList/RelationList.php");
  require_once($AdminPath."/Components/SelectList/SelectList.php"); 
  require_once($AdminPath."/Modules/BO/CompetenceVO.php");
  require_once($AdminPath."/Modules/BO/CompetenceAccedeCarteVO.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CompetenceVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CompetenceAccedeCarteVO;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php     
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_MAPS_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if ( $PK==-1 )
  {
    exit;
  }

  // set a default ID
  if ( !isset($_GET["RR"]) )
  {
    $RELATIONLIST_RESULT = -1;
  }
  else
  {
    $RELATIONLIST_RESULT = $_GET["RR"]; 
  }

  $dao = new DAO($conn, 'catalogue');

  if ( $RELATIONLIST_RESULT != -1 )
  {
    switch( $Action )
    {
      case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
        $ordinalList = array();
        $valueList = array();
        $ordinalList[]=CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_COMPETENCE_ID;
        $ordinalList[]=CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_CARTE_PROJET;
        $valueList[]=$RELATIONLIST_RESULT;
        $valueList[]=$PK;

        $CompetenceAccedeCarteVO_RL = new CompetenceAccedeCarteVO();
        $CompetenceAccedeCarteVO_RL->setDao($dao);
        $CompetenceAccedeCarteVO_RL->InsertValues( $ordinalList, $valueList );
        $CompetenceAccedeCarteVO_RL->Commit();
      break;
      
      case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
        $CompetenceAccedeCarteVO_RL = new CompetenceAccedeCarteVO();
        $CompetenceAccedeCarteVO_RL->setDao($dao);
        $CompetenceAccedeCarteVO_RL->DeleteRow( CompetenceAccedeCarteVO::$PK_COMPETENCE_ACCEDE_CARTE, $RELATIONLIST_RESULT );
        $CompetenceAccedeCarteVO_RL->Commit();
      break;
    }
    AlertSaveController::AlertSaveDone("", false);
  }

  // #################################################################

  $CompetenceAccCarteVO = new CompetenceAccedeCarteVO();
  $CompetenceAccCarteVO->setDao($dao);

  $CompetenceAccedeCarteVO2 = new CompetenceAccedeCarteVO();
  $CompetenceAccedeCarteVO2->setDao($dao);
  $CompetenceAccedeCarteVO2->AddRestriction( CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_CARTE_PROJET, $PK );
  $CompetenceAccedeCarteVO2->AddOnlyKeyProjection( CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_COMPETENCE_ID );
  
  $competenceVO = new CompetenceVO();
  $competenceVO->setDao($dao);
  $competenceVO->AddNotInRestriction( CompetenceVO::$PK_COMPETENCE_ID, $CompetenceAccedeCarteVO2 );

  $relationList = new RelationList( "RelationListMapAccCompetence",
                   Ressources::$RELATIONLIST_MAPS_COMPETENCE_RELATION,
                   $CompetenceAccCarteVO,
                   CompetenceAccedeCarteVO::$PK_COMPETENCE_ACCEDE_CARTE,
                   CompetenceAccedeCarteVO::$COMPETENCECARTE_COMPETENCENOM,
                   CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_CARTE_PROJET,
                   $PK,
                   CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_COMPETENCE_ID,
                   $competenceVO,
                   Ressources::$RELATIONLIST_MAPS_COMPETENCE_TABLE,
                   CompetenceVO::$PK_COMPETENCE_ID,
                   CompetenceVO::$COMPETENCE_NOM,
                   '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Maps/MapsCompetences.php",*/
                   Ressources::$IFRAME_NAME_MAIN,
                   $this
                   );

?>
  </body>
</html>
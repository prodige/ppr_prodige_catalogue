<?php
/**
 * Liste des cartes (haut) dans l'adminsitration des cartes (administration des droits)
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/CarteProjetVO.php");
	require_once($AdminPath."/Components/SelectList/SelectList.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CarteProjetVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;

  header("Content-type: application/xml");
  echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 

  $dao = new DAO($conn, 'catalogue');

	$carteProjetVO = new CarteProjetVO();
	$carteProjetVO->setDao($dao);

	$selectList = new SelectList(	 "SelectListCarteProjetVO",
									 $carteProjetVO,
									 CarteProjetVO::$PK_CARTE_PROJET,
									 CarteProjetVO::$CARTP_NOM,
									 Ressources::$IFRAME_NAME_MAIN,
									 '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Maps/MapsDetail.php",*/
									 "800",
                   "100"
									 );

?>
	</body>
</html>
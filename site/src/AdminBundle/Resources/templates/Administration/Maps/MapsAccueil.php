<?php
/**
 * Accueil de la page cartes (Administration des droits)
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/CarteProjetVO.php");
	require_once($AdminPath."/Components/EditForm/EditForm.php");
	require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CarteProjetVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 	
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_MAPS_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}

  $dao = new DAO($conn, 'catalogue');

	$carteProjetVO = new CarteProjetVO();
	$carteProjetVO->setDao($dao);

	$accessRights = new AccessRights( );

	$ordinalList = array();
	$ordinalListDisp = array();
	$ordinalType = array();
	$ordinalSize = array();		
	$ordinalReadOnly = array();
	
	$ordinalList[]=CarteProjetVO::$CARTP_ID;
	$ordinalListDisp[]="ID :";
	$ordinalType[]=EditForm::$TYPE_TEXT;
	$ordinalSize[]=20;
	$ordinalReadOnly[]= true;

	$ordinalList[]=CarteProjetVO::$CARTP_NOM;
	$ordinalListDisp[]="Nom :";
	$ordinalType[]=EditForm::$TYPE_TEXT;
	$ordinalSize[]=30;
	$ordinalReadOnly[]= true;

	$ordinalList[]=CarteProjetVO::$CARTP_DESCRIPTION;
	$ordinalListDisp[]="Description :";
	$ordinalType[]=EditForm::$TYPE_TEXT;
	$ordinalSize[]=50;
	$ordinalReadOnly[]= true;

	$ordinalList[]=CarteProjetVO::$CARTP_FORMAT;
	$ordinalListDisp[]="Format carte :";
	$ordinalType[]=EditForm::$TYPE_CB;
	$ordinalSize[]=0;
	$ordinalReadOnly[]= true;

	print('<br>');
	
	$editForm = new EditForm(	"EditFormCarteProjetVO",
								$carteProjetVO,
								CarteProjetVO::$PK_CARTE_PROJET,
								$PK,
								$ordinalList,
								$ordinalListDisp,
								$ordinalType,
								$ordinalSize,
								$ordinalReadOnly,
								'', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Maps/MapsAccueil.php",*/
								$Action,
								null, 
								true,
								true,
            	  array(),
            	  $submitUrl);

?>
	</body>
</html>
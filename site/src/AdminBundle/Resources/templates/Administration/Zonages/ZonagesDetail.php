<?php
/**
 * Page de détail des compétences (Administration des droits)
 * @author Alkante
 */
  require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");  
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css">
      a {color:#000000;text-decoration:none}
      a:hover {text-decoration:underline}
    </style>
  </head>
  <body style="margin:0px;">
<?php   
  
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_ZONAGES_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if ( $PK==-1 )
  {
    $Action = Ressources::$ACTION_OPEN_ZONAGES_ACCUEIL;
    print('
      <TABLE style="text-align: center" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">      
        <TR width="100%" >
          <TD>
            '.Ressources::$ZONAGES_EMPTY_SELECT.'
          </TD>
        </TR>
      </TABLE>
      ');
    
    exit;
  }

  if ( $Action == Ressources::$LIST_MODIFY_ACTION )
  {
    $Action = Ressources::$ACTION_OPEN_ZONAGES_ACCUEIL;
  }
  
  print('
    <TABLE style="text-align: justify" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
      <TR width="100%" height="32">
        <TD>
          '.DrawHeader( $Action, $PK ).'
        </TD>
      </TR>
      <TR width="100%" height="100%">
        <TD><IFRAME width="'.Ressources::$SUB_DETAILS_SIZE_X.'"
            height="'.Ressources::$SUB_DETAILS_SIZE_Y.'" 
            MARGINHEIGHT="0"
            MARGINWIDTH="0" 
            FRAMEBORDER="0"
            name="'.Ressources::$IFRAME_NAME_MAIN.'"
            ALLOWTRANSPARENCY="true" 
            SCROLLING="auto" 
            SRC="'.GetModulePageDetail($Action).'?Action='.$Action.'&Id='.$PK.'">
          </IFRAME>
        </TD>
      </TR>
      
    </TABLE>
  ');
    

  // #################################################################
  
  function DrawHeader( $action, $pk )
  {
    $header ='
      <TABLE style="text-align: center" width="100" height="32" border="0" cellpadding="0" cellspacing="0">
        <TR>
          <TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_ZONAGES_ACCUEIL, true ).'">
            <A HREF="ZonagesDetail.php?Action='.Ressources::$ACTION_OPEN_ZONAGES_ACCUEIL.'&Id='.$pk.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
              '.Ressources::$BTN_ZONAGES_INITIAL.'
            </A>
          </TD>
        </TR>
      </TABLE>';
    
    return $header;
  }

  // #################################################################
  
  function GetHeaderTableBackground( $action, $celAction, $bLast=false )
  {
    $strStyle = "border:1px solid #000000;".(!$bLast ? "border-right:none;" : "")."padding:5px;background-color:";
    if ( $action == $celAction )
    {
      return $strStyle."#FFFFFF;border-bottom:none;";
    }

    return $strStyle."#dddddd;";
  }
  
  function GetModulePageDetail( $action )
  {
    global $AdminPath;
    switch( $action )
    {
      case Ressources::$ACTION_OPEN_ZONAGES_ACCUEIL:
        return $AdminPath."/Administration/Zonages/ZonagesAccueil.php";
      break;
    }

    return $AdminPath."/Administration/Zonages/Empty.php";
  }
  
?>

  </body>
</html>
<?php
/**
 * Accueil de la page domaines (Administration des droits)
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/DomaineVO.php");
	require_once($AdminPath."/Components/EditForm/EditForm.php");
	require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\DomaineVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 	
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_DOMAINS_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}

	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}

	$dao = new DAO($conn, 'catalogue');

	$accessRights = new AccessRights( );
	
	$domaineVO = new DomaineVO();
	$domaineVO->setDao($dao);
	$ordinalList = array();
	$ordinalListDisp = array();
	$ordinalType = array();
	$ordinalSize = array();		
	$ordinalReadOnly = array();	
	$arraySelect = array();
	
	$ordinalList[]=DomaineVO::$DOM_ID;
	$ordinalListDisp[]="ID :";
	$ordinalType[]=EditForm::$TYPE_TEXT;
	$ordinalSize[]=20;
	$ordinalReadOnly[]= !$accessRights->CanUpdate( $domaineVO, DomaineVO::$DOM_ID, $PK);
	$arraySelect[] = array();

	$ordinalList[]=DomaineVO::$DOM_NOM;
	$ordinalListDisp[]="Nom :";
	$ordinalType[]=EditForm::$TYPE_TEXT;
	$ordinalSize[]=30;
	$ordinalReadOnly[]= !$accessRights->CanUpdate( $domaineVO, DomaineVO::$DOM_NOM, $PK);
	$arraySelect[] = array();
	
	$ordinalList[]=DomaineVO::$DOM_DESCRIPTION;
	$ordinalListDisp[]="Description :";
	$ordinalType[]=EditForm::$TYPE_TEXT;
	$ordinalSize[]=50;
	$ordinalReadOnly[]= !$accessRights->CanUpdate( $domaineVO, DomaineVO::$DOM_DESCRIPTION, $PK);
	$arraySelect[] = array();
	
	$ordinalList[]=DomaineVO::$DOM_RUBRIC_ID;
	
  //$dao = new DAO();
  if ($dao)
  { 
    $query = 'SELECT rubric_id, rubric_name FROM rubric_param;';
    $rs = $dao->BuildResultSet($query);
    for ($rs->First(); !$rs->EOF(); $rs->Next())
    {
      $rubric_id = $rs->Read(0);
      $rubric_name = $rs->Read(1);
      $rubrics[$rubric_id]=$rubric_name;
    } 

  }
  
  unset($dao);
  
	
	$ordinalListDisp[]="Rubrique :";
	$ordinalType[]=EditForm::$TYPE_SELECT;
	$ordinalSize[]=10;
  $arraySelect[] = $rubrics;
	//$ordinalReadOnly[]= !$accessRights->CanUpdate($domaineVO, DomaineVO::$DOM_RUBRIC, $PK);
	
	print('<br><br>');
	
	$editForm = new EditForm(	"EditFormDomains",
								$domaineVO,
								DomaineVO::$PK_DOMAINE,
								$PK,
								$ordinalList,
								$ordinalListDisp,
								$ordinalType,
								$ordinalSize,
								$ordinalReadOnly,
								'', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Domains/DomainsAccueil.php",*/
								$Action, null, false, false,
                $arraySelect,
	              $submitUrl);
	
	
?>
<script type="text/javascript">

//surcharge de la fonction de validation / modification des métadonnées
function EditFormDomainsValidate_onclick() 
{
  var form = document.getElementById("EditFormDomains");
  var inputId = document.getElementById("Id");
  /*form.action = "php --- echo PRO_CATALOGUE_URLBASE; --- php droits.php?url=Administration/Components/EditForm/WriteBOV.php";*/
  form.action = '<?php echo $submitUrl; ?>';
  parent.Ext.Ajax.timeout = 900000;
  parent.Ext.Ajax.request({
  //url : 'php --- echo PRO_CATALOGUE_URLBASE --- php Services/updateMdataDomSdom.php',
  url : '<?php echo $ajaxRequestUrl; ?>',
      method : 'GET',
      params : {
        'mode' : 'updatedomain',
        'newDomain' : document.getElementById("ORDINAL_3").value,
        'id' : inputId.value
      },
  success: function(result){
    form.submit();
  },
  failure: function(result){
    parent.Ext.MessageBox.alert('Failed','erreur lors de la mise à jour de l\'information des métadonnées'); 
  } 
  });
}
function EditFormDomainsDelete_onclick() 
{
    var form = document.getElementById("EditFormDomains");
    
    var inputId = document.getElementById("Id");
    var inputAction = document.getElementById("Action");
    
    inputAction.value = '<?php echo Ressources::$EDITFORM_DELETE;?>'; 
    /*form.action = "php --- echo PRO_CATALOGUE_URLBASE; --- php droits.php?url=Administration/Components/EditForm/WriteBOV.php";*/
    form.action = '<?php echo $submitUrl; ?>';
    if (confirm("Êtes-vous sûr de vouloir supprimer cet enregistrement (test)?")){
      parent.Ext.Ajax.timeout = 900000;
      parent.Ext.Ajax.request({
        /*url : 'php -- echo PRO_CATALOGUE_URLBASE --- php Services/updateMdataDomSdom.php',*/
        url : '<?php echo $ajaxRequestUrl; ?>',
        method : 'GET',
        params : {
          'mode' : 'deldomain',
          'id' : inputId.value
        },
        success: function(result){
          form.submit();
        },
        failure: function(result){
          parent.Ext.MessageBox.alert('Failed', 'erreur lors de la suppression de l\'information des métadonnées'); 
        } 
        });
    }
  }
</script>
	</body>
</html>
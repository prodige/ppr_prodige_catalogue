<?php
/**
 * Onglet domaines dans la gestion des profils
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/GrpAccedeDomVO.php");
	require_once($AdminPath."/Modules/BO/DomaineVO.php");
	require_once($AdminPath."/Components/RelationList/RelationList.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GrpAccedeDomVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\DomaineVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 	
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_PROFILES_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}

	// set a default ID
	if ( !isset($_GET["RR"]) )
	{
		$RELATIONLIST_RESULT = -1;
	}
	else
	{
		$RELATIONLIST_RESULT = $_GET["RR"];	
	}

	$dao = new DAO($conn, 'catalogue');

	if ( $RELATIONLIST_RESULT != -1 )
	{
		switch( $Action )
		{
			case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
				$ordinalList = array();
				$valueList = array();
				$ordinalList[]=GrpAccedeDomVO::$GRPDOM_FK_GROUPE_PROFIL;
				$ordinalList[]=GrpAccedeDomVO::$GRPDOM_FK_DOMAINE;
				$valueList[]=$PK;
				$valueList[]=$RELATIONLIST_RESULT;

				$grpAccedeDomVO_RL = new GrpAccedeDomVO();
				$grpAccedeDomVO_RL->setDao($dao);
				$grpAccedeDomVO_RL->InsertValues( $ordinalList, $valueList );
				$grpAccedeDomVO_RL->Commit();
 			break;
			
			case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
				$grpAccedeDomVO_RL = new GrpAccedeDomVO();
				$grpAccedeDomVO_RL->setDao($dao);
				$grpAccedeDomVO_RL->DeleteRow( GrpAccedeDomVO::$PK_GRP_ACCEDE_DOM, $RELATIONLIST_RESULT );
				$grpAccedeDomVO_RL->Commit();
			break;
		}
        AlertSaveController::AlertSaveDone("", false);
	}
	
	// #################################################################


	$grpAccedeDomVO = new GrpAccedeDomVO();
	$grpAccedeDomVO->setDao($dao);
	$domaineVO = new DomaineVO();
	$domaineVO->setDao($dao);
	
	$grpAccedeDomVO2 = new GrpAccedeDomVO();
	$grpAccedeDomVO2->setDao($dao);
	$grpAccedeDomVO2->AddRestriction( GrpAccedeDomVO::$GRPDOM_FK_GROUPE_PROFIL, $PK );
	$grpAccedeDomVO2->AddOnlyKeyProjection( GrpAccedeDomVO::$GRPDOM_FK_DOMAINE );
	
	$domaineVO->AddNotInRestriction( DomaineVO::$PK_DOMAINE, $grpAccedeDomVO2 );
	
	$relationList = new RelationList( "RelationListProfileDom",
									 Ressources::$RELATIONLIST_PROFILES_DOM_RELATION,
									 $grpAccedeDomVO,
									 GrpAccedeDomVO::$PK_GRP_ACCEDE_DOM,
									 GrpAccedeDomVO::$GRPDOM_DOM_NOM,
									 GrpAccedeDomVO::$GRPDOM_FK_GROUPE_PROFIL,
									 $PK,
									 GrpAccedeDomVO::$GRPDOM_FK_DOMAINE,
									 $domaineVO,
									 Ressources::$RELATIONLIST_PROFILES_DOM_TABLE,
									 DomaineVO::$PK_DOMAINE,
									 DomaineVO::$DOM_NOM,
									 '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Profiles/ProfilesDomains.php",*/
									 Ressources::$IFRAME_NAME_MAIN,
	                                 $this
									 );
	
	
?>
	</body>
</html>
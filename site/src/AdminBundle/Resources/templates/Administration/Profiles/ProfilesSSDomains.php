<?php
/**
 * Onglet sous-domaines dans la gestion des profils
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/GrpAccedeSSDomVO.php");
	require_once($AdminPath."/Modules/BO/SousDomaineVO.php");
	require_once($AdminPath."/Components/RelationList/RelationList.php");*/
  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GrpAccedeSSDomVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\SousDomaineVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 	
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_PROFILES_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}

	// set a default ID
	if ( !isset($_GET["RR"]) )
	{
		$RELATIONLIST_RESULT = -1;
	}
	else
	{
		$RELATIONLIST_RESULT = $_GET["RR"];	
	}

	$dao = new DAO($conn, 'catalogue');

	if ( $RELATIONLIST_RESULT != -1 )
	{
		switch( $Action )
		{
			case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
				$ordinalList = array();
				$valueList = array();
				$ordinalList[]=GrpAccedeSSDomVO::$GRPSS_FK_GROUPE_PROFIL;
				$ordinalList[]=GrpAccedeSSDomVO::$GRPSS_FK_SOUS_DOMAINE;
				$valueList[]=$PK;
				$valueList[]=$RELATIONLIST_RESULT;

				$grpAccedeSSDomVO_RL = new GrpAccedeSSDomVO();
				$grpAccedeSSDomVO_RL->setDao($dao);
				$grpAccedeSSDomVO_RL->InsertValues( $ordinalList, $valueList );
				$grpAccedeSSDomVO_RL->Commit();
 			break;
			
			case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
				$grpAccedeSSDomVO_RL = new GrpAccedeSSDomVO();
				$grpAccedeSSDomVO_RL->setDao($dao);
				$grpAccedeSSDomVO_RL->DeleteRow( GrpAccedeSSDomVO::$PK_GRP_ACCEDE_SSDOM, $RELATIONLIST_RESULT );
				$grpAccedeSSDomVO_RL->Commit();
			break;
		}
        AlertSaveController::AlertSaveDone("", false);
	}
	
	// #################################################################


	$grpAccedeSSDomVO = new GrpAccedeSSDomVO(true);
	$grpAccedeSSDomVO->setDao($dao);
	
	$grpAccedeSSDomVO2 = new GrpAccedeSSDomVO(true);
	$grpAccedeSSDomVO2->setDao($dao);
	
	$grpAccedeSSDomVO2->AddRestriction( GrpAccedeSSDomVO::$GRPSS_FK_GROUPE_PROFIL, $PK );
	$grpAccedeSSDomVO2->AddOnlyKeyProjection( GrpAccedeSSDomVO::$GRPSS_FK_SOUS_DOMAINE );
	
  $sousDomaineVO = new SousDomaineVO(true);
  $sousDomaineVO->setDao($dao);
	$sousDomaineVO->AddNotInRestriction( SousDomaineVO::$PK_SOUS_DOMAINE, $grpAccedeSSDomVO2 );
	
	$relationList = new RelationList( "RelationListProfileSSDom",
									 Ressources::$RELATIONLIST_PROFILES_SSDOM_RELATION,
									 $grpAccedeSSDomVO,
									 GrpAccedeSSDomVO::$PK_GRP_ACCEDE_SSDOM,
									 GrpAccedeSSDomVO::$GRPSS_SSDOM_NOM,
									 GrpAccedeSSDomVO::$GRPSS_FK_GROUPE_PROFIL,
									 $PK,
									 GrpAccedeSSDomVO::$GRPSS_FK_SOUS_DOMAINE,
									 $sousDomaineVO,
									 Ressources::$RELATIONLIST_PROFILES_SSDOM_TABLE,
									 SousDomaineVO::$PK_SOUS_DOMAINE,
									 $sousDomaineVO->DOM_SSDOM_NOM,
									 '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Profiles/ProfilesSSDomains.php",*/
									 Ressources::$IFRAME_NAME_MAIN,
	                                 $this
									 );
	
?>
	</body>
</html>
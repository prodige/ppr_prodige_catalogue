<?php
/**
 * Onglet administration dans la gestion des profils
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/GroupeProfilVO.php");
	require_once($AdminPath."/Modules/BO/SousDomaineVO.php");
	require_once($AdminPath."/Components/RelationList/RelationList.php");
	require_once($AdminPath."/Components/SelectList/SelectList.php");	
	require_once($AdminPath."/Modules/BO/UtilisateurVO.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GroupeProfilVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\SousDomaineVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 	
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_PROFILES_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}

	// set a default ID
	if ( !isset($_GET["RR"]) )
	{
		$RELATIONLIST_RESULT = -1;
	}
	else
	{
		$RELATIONLIST_RESULT = $_GET["RR"];	
	}

	$dao = new DAO($conn, 'catalogue');

	if ( $RELATIONLIST_RESULT != -1 )
	{
		switch( $Action )
		{
			case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
				$ordinalList = array();
				$valueList = array();
				$ordinalList[]=SousDomaineVO::$SSDOM_ADMIN_FK_GROUPE_PROFIL;
				$valueList[]=$PK;
				$ordinalRestriction = SousDomaineVO::$PK_SOUS_DOMAINE;
				$valueRestriction = $RELATIONLIST_RESULT;

				$sousDomaineVO_RL = new SousDomaineVO();
				$sousDomaineVO_RL->setDao($dao);
				$sousDomaineVO_RL->Update( $ordinalList, $valueList, $ordinalRestriction, $valueRestriction );
				$sousDomaineVO_RL->Commit();
 			break;
			
			case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
				$ordinalList = array();
				$valueList = array();
				$ordinalList[]=SousDomaineVO::$SSDOM_ADMIN_FK_GROUPE_PROFIL;
				$valueList[]="NULL";
				$ordinalRestriction = SousDomaineVO::$PK_SOUS_DOMAINE;
				$valueRestriction = $RELATIONLIST_RESULT;

				$sousDomaineVO_RL = new SousDomaineVO();
				$sousDomaineVO_RL->setDao($dao);
				$sousDomaineVO_RL->Update( $ordinalList, $valueList, $ordinalRestriction, $valueRestriction );
				$sousDomaineVO_RL->Commit();
			break;
		}
        AlertSaveController::AlertSaveDone("", false);
	}
	
	// #################################################################

	$groupeProfilVO = new GroupeProfilVO();
	$groupeProfilVO->setDao($dao);

	// ALL SUBDOMAINS FOR THE COMPONENT (they will be filtered by it)
	$sousDomaineVO = new SousDomaineVO(true);
	$sousDomaineVO->setDao($dao);

	// SUBDOMAINS OF THE ACTUAL PROFILE
	$sousDomaineVO2 = new SousDomaineVO();
	$sousDomaineVO2->setDao($dao);
	$sousDomaineVO2->AddRestriction( SousDomaineVO::$SSDOM_ADMIN_FK_GROUPE_PROFIL, $PK );
	$sousDomaineVO2->AddOnlyKeyProjection( SousDomaineVO::$PK_SOUS_DOMAINE );
	
	// ALL OTHER SUBDOMAINS 
	$sousDomaineVO3 = new SousDomaineVO(true);
	$sousDomaineVO3->setDao($dao);
	$sousDomaineVO3->AddNotInRestriction( SousDomaineVO::$PK_SOUS_DOMAINE, $sousDomaineVO2 );


	$relationList = new RelationList( "RelationListProfileAdmin",
									 Ressources::$RELATIONLIST_PROFILES_ADMIN_RELATION,
									 $sousDomaineVO,
									 SousDomaineVO::$PK_SOUS_DOMAINE,//keyord
									 $sousDomaineVO->DOM_SSDOM_NOM,//valord
									 SousDomaineVO::$SSDOM_ADMIN_FK_GROUPE_PROFIL,//pkrestord
									 $PK,//pkrestval
									 SousDomaineVO::$PK_SOUS_DOMAINE,//fktable
									 
									 $sousDomaineVO3,
									 Ressources::$RELATIONLIST_PROFILES_ADMIN_TABLE,
									 SousDomaineVO::$PK_SOUS_DOMAINE,
									 $sousDomaineVO3->DOM_SSDOM_NOM,
									 '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Profiles/ProfilesAdmin.php",*/
									 Ressources::$IFRAME_NAME_MAIN,
	                                 $this,
                   false
									 );
	
	
?>
	</body>
</html>
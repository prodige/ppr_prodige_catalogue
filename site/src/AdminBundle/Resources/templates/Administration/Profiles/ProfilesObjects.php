<?php
/**
 * Onglet traitements dans la gestion des profils
 * @author Alkante
 */

  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Modules/BO/ObjetVO.php");
  require_once($AdminPath."/Components/SelectList/SelectList.php");
  
  require_once($AdminPath."/Modules/BO/GrpTrtObjetVO.php");
  require_once($AdminPath."/Modules/BO/TrtObjetVO.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\ObjetVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;

  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GrpTrtObjetVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\TrtObjetVO;

  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_OBJECTS_DONNEE;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  //redirection par défaut sur l'onglet donnée
  if ($Action == Ressources::$ACTION_OPEN_OBJECTS )
    $Action = Ressources::$ACTION_OPEN_OBJECTS_DONNEE;
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = 1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );
  }
  if ( !isset($_GET["iPart"]) )
  {
    $iPart = 1;
  }
  else
  {
    $iPart = intval( $_GET["iPart"] );
  }

  $dao = new DAO($conn, 'catalogue');

  $objet_type_id = getObjetType($Action);
  
  $accessRights = new AccessRights();
  $bDisabled = !($accessRights->IsProdigeAdmin() || ($accessRights->HasAdminOnGroup($PK)));

  if ( $PK==-1 )
  {
    $Action = Ressources::$ACTION_OPEN_OBJECTS_DONNEE;
    print('
      <TABLE style="text-align: center" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">      
        <TR width="100%" >
          <TD>
            '.Ressources::$PROFILES_EMPTY_SELECT.'
          </TD>
        </TR>
      </TABLE>
      ');
    
    exit();
  }
  
  if ( $Action == Ressources::$LIST_MODIFY_ACTION )
  {
    $Action = Ressources::$ACTION_OPEN_OBJECTS_DONNEE;
  }
  $PROFIL = $PK;

  switch($iPart){
    case "1" :

      $tabSheet = array(
        Ressources::$ACTION_OPEN_OBJECTS_DONNEE     => array("title" => Ressources::$BTN_OBJECTS_DONNEE,    "url" => "Objects/ObjectsDonnee.php"),
        Ressources::$ACTION_OPEN_OBJECTS_CARTE      => array("title" => Ressources::$BTN_OBJECTS_CARTE,     "url" => "Objects/ObjectsCarte.php"),
        //Ressources::$ACTION_OPEN_OBJECTS_METADONNEE => array("title" => Ressources::$BTN_OBJECTS_METADONNEE,"url" => "Objects/ObjectsMetadonnee.php"),
        //Ressources::$ACTION_OPEN_OBJECTS_MODELE     => array("title" => Ressources::$BTN_OBJECTS_MODELE,    "url" => "Objects/ObjectsModele.php"),
        //Ressources::$ACTION_OPEN_OBJECTS_GROUPS     => array("title" => Ressources::$BTN_OBJECTS_GROUPS,    "url" => "Objects/ObjectsGroup.php"),
      
      );
      $idElt = "tab_".Ressources::$ACTION_OPEN_PROFILES."_".Ressources::$ACTION_OPEN_OBJECTS;
      $firstSheet = "-1";
      foreach ($tabSheet as $idSheet=>$titleSheet){
        $firstSheet = $idSheet;
        break;
      }
      ?>
      <script type='text/javascript'>
      adminTabPanelMain.getActiveTab().add(new Ext.TabPanel(
        {
          renderTo : adminTabPanelMain.getActiveTab().id,
          border :false,
          id :'data_profiles',
          autoHeight : true,
          autoWidth : true,
          activeTab :'<?php echo $idElt."_".$firstSheet ?>',
          items : [
      <?php
      $glue = "";
      foreach ($tabSheet as $idSheet=>$descSheet){
        echo $glue; 
      ?>
        {
          title :'<?php echo $descSheet["title"] ?>',
          id :'<?php echo $idElt."_".$idSheet ?>',
          autoHeight : true,
          autoWidth : true,
          autoLoad : {
            //url : "droits.php?url=Administration/Administration/Profiles/ProfilesObjects.php&iPart=2&Action=<?php echo $idSheet ?>&Id=<?php echo $PROFIL ?>",
            url : Routing.generate('catalogue_administration_profiles_objects_form', {
              iPart: 2,
              Action: '<?php  echo $idSheet ?>',
              Id: <?php echo $PROFIL ?>
            }),
            method : 'GET', 
            scripts: true,
            text : "Chargement en cours..."
          },
          listeners : {
            show : function(_this){
              if ( typeof _this.gridList != "undefined" ){
                _this.gridList.fireEvent("afterrender", _this.gridList);
                Ext.getCmp('module_details').getActiveTab().gridList = _this.gridList;
              }
            }
          }
        }
      <?php
        $glue = ",";
      }
      ?>
      ]}));
      </script>
      <?php
    break;
    
    
    case "2" :
      $idElt = "tab_".Ressources::$ACTION_OPEN_PROFILES."_".Ressources::$ACTION_OPEN_OBJECTS;
      
      $objetVO = new ObjetVO();
      $objetVO->setDao($dao);
      $objetVO->AddRestriction( ObjetVO::$OBJET_TYPE_ID, $objet_type_id );
      $objetVO->Open();      
      $objetVO->GetResultSet()->First();   
      
      $tabData = array(
        -1=>array("name"=>"", "id"=>"-1")
      );
      $tabTitle = array("name"=>getObjetTypeIntitule($Action), "status"=>"Gestion à l'objet");
      
      $tabTraitement = array();
      $tabTraitementId = array();
      $TrtObjetVO = new TrtObjetVO();
      $TrtObjetVO->setDao($dao);
      $TrtObjetVO->AddRestriction( TrtObjetVO::$TRT_AUTORISE_OBJET_OBJ_TYPE_ID, $objet_type_id);
      $TrtObjetVO->Open();      
      $TrtObjetVO->GetResultSet()->First();  
      while ( !$TrtObjetVO->GetResultSet()->EOF() )
      {
      	$TRT_PK = $TrtObjetVO->GetResultSet()->Read(TrtObjetVO::$TRT_ID);
      	//champs visibles pour le traitement edition
      	if(($TRT_PK != "EDITION" && $TRT_PK != "EDITION (AJOUT)") || PRO_EDITION){
	      	
	        $tabTitle[$TRT_PK] = $TRT_PK;
	        $tabTraitementId[$TRT_PK] = $TrtObjetVO->GetResultSet()->Read(TrtObjetVO::$PK_TRT_OBJET);
	        
	        $tabTraitement[$TRT_PK] = false;
	        $tabData[-1][$TRT_PK] = "<a onclick=\"SelectAll('".$TRT_PK."', true)\">Tous</a>&nbsp;/&nbsp;<a onclick=\"SelectAll('".$TRT_PK."', false)\">Aucun</a>";
	        
      	}
      	$TrtObjetVO->GetResultSet()->Next();
      }
      $strTraitement = implode(",", array_keys($tabTraitement));
      $strObject = "";
      
      $tabDataSelected = $tabData;
      $tabDataNotSelected = array();
      // FILL THE SELECT LIST
      while ( !$objetVO->GetResultSet()->EOF() )
      {
        $OBJET_PK = $objetVO->GetResultSet()->Read(ObjetVO::$OBJET_ID);
        $strObject .= ($strObject=="" ? "" : ",").$OBJET_PK;
        
        
        //$bManage = IsObjectManaged($OBJET_PK, $objet_type_id, $PROFIL)!=-1;
        $bManage = IsObjectManaged($conn, $OBJET_PK, $objet_type_id, $PROFIL)!=-1;
        $tabData[$OBJET_PK]["name"] = $objetVO->GetResultSet()->Read(ObjetVO::$OBJET_NOM);
        $tabData[$OBJET_PK]["id"] = $OBJET_PK;
        $tabData[$OBJET_PK]["status"] = "<input type=\"checkbox\" value=\"1\"" .
              " name=\"manage[".$OBJET_PK."]\"" .
              " id=\"manage[".$objet_type_id."][".$OBJET_PK."]\"" .
              ($bManage ? " checked=\"checked\"" : "").
              " onclick=\"ChangeEnabled(this, ".$objet_type_id.", ".$OBJET_PK.", '".$strTraitement."')\"/>";

        $tabTraitementObj = $tabTraitement;
        
        
        $GrpTrtObjetVO2 = new GrpTrtObjetVO();
        $GrpTrtObjetVO2->setDao($dao);
        $GrpTrtObjetVO2->AddRestriction( GrpTrtObjetVO::$GRPTRTOBJET_GROUPE_PROFIL, $PROFIL );
        $GrpTrtObjetVO2->AddRestriction( GrpTrtObjetVO::$FK_OBJ_TYPE_ID, $objet_type_id);
        $GrpTrtObjetVO2->AddRestriction( GrpTrtObjetVO::$GRPTRTOBJET_OBJETTYPE, $objet_type_id);
        $GrpTrtObjetVO2->AddRestriction( GrpTrtObjetVO::$GRP_TRT_OBJET_STATUS, 1);
        $GrpTrtObjetVO2->AddRestriction( GrpTrtObjetVO::$GRPTRTOBJET_OBJET_ID, $OBJET_PK);
        $GrpTrtObjetVO2->Open();      
        $GrpTrtObjetVO2->GetResultSet()->First();  
        while ( !$GrpTrtObjetVO2->GetResultSet()->EOF() )
        {
          $TRT_PK = $GrpTrtObjetVO2->GetResultSet()->Read( GrpTrtObjetVO::$GRPTRTOBJET_TRT_ID );
          $tabTraitementObj[$TRT_PK] = true;
          $GrpTrtObjetVO2->GetResultSet()->Next();
        }
        
        $bOneTraitement = false;
        foreach ($tabTraitementObj as $TRT_PK=>$bTraite){
            if(isset($tabTraitementId[$TRT_PK])) {
                $tabData[$OBJET_PK][$TRT_PK] = "<input type=\"checkbox\" value=\"".$tabTraitementId[$TRT_PK]."\"" .
                    " name=\"traitement[".$OBJET_PK."][".$TRT_PK."]\"" .
                    " id=\"traitement[".$objet_type_id."][".$OBJET_PK."][".$TRT_PK."]\"" .
                    ($bTraite ? " checked=\"checked\"" : "").
                    ($bManage ? "" : " disabled=\"disabled\"").
                    "/>";
                    $bOneTraitement |= $bManage;
            }
        }
     
        if ( $bOneTraitement ){
          $tabDataSelected[$OBJET_PK] =  $tabData[$OBJET_PK];
        }
        else {
          $tabDataNotSelected[$OBJET_PK] =  $tabData[$OBJET_PK];
        }
        $objetVO->GetResultSet()->Next();
        
      }
      
      $tabSeparator = (count($tabDataSelected)>1 && !empty($tabDataNotSelected)
                      ? array("-"=>array("id"=>"-", "name"=>"", "selection"=>""))
                      : array());
      
      $tabData = array_merge($tabDataSelected, $tabSeparator, $tabDataNotSelected);
      
      $strFields = "{name : 'id'}";
      $strColumns = "";
      $bNotTitle = false;
      foreach ($tabTitle as $dataIndex=>$title){
        $strFields .= ($strFields=="" ? "" : ", ").  "{name : '".$dataIndex."'}";
        $strColumns .= ($strColumns=="" ? "" : ", "). 
                       "{" .
                         " id: 'col_".$dataIndex."', dataIndex: '".$dataIndex."', " .
                         " header: \"".addslashes($title)."\"" .
                         ($bNotTitle ? ", align:'center', width:100" : "").
                       "}";
        if ( !$bNotTitle ) $bNotTitle = true;
      }
      $classSelection = "x-grid3-row-alt";
      ?>
      
      <script type='text/javascript'>
      function SelectAll(TRT_ID, bSelect)
      {
        var oForm = document.getElementById('form_<?php echo $idElt."_".$Action ?>');
        for (var i=0; i <?php echo "<"?> oForm.elements.length; i++){
          if ( oForm.elements[i].id.indexOf('['+TRT_ID+']')!=-1 ){
            oForm.elements[i].checked = bSelect;
          }
        }
      }
      function ChangeEnabled(oManage, OBJET_TYPE, OBJET_PK, listTraitement)
      {
        var tabTraitement = listTraitement.split(",");
        for (var i=0; i <?php echo "<"?> tabTraitement.length; i++){
          var oTraitement = document.getElementById('traitement['+OBJET_TYPE+']['+OBJET_PK+']['+tabTraitement[i]+']');
          oTraitement.disabled = !oManage.checked;
        }
      }

      Ext.getCmp('data_profiles').getActiveTab().add(new Ext.FormPanel({
        border: false,
        autoWidth : true,
        autoHeight : true,
        renderTo : Ext.getCmp('data_profiles').getActiveTab().id,
        standardSubmit: true,
        baseParams: {
            foo: 'bar'
        },
        id : 'formpanel_<?php echo $idElt."_".$Action ?>',
        formId : 'form_<?php echo $idElt."_".$Action ?>',
        url: Routing.generate('catalogue_administration_profiles_objects_form', {'iPart':3, 'Action':'<?php echo $Action ?>', 'Id':'<?php echo $PK ?>'}),
        items: [
          <?php foreach ($tabTraitementId as $TRT_PK=>$TRT_ID){
            echo "{xtype:'hidden', name:'listTraitement[".$TRT_PK."]', value:'".$TRT_ID."'},\n";
          }?> 
          new Ext.grid.GridPanel({
            region : 'center',
            height : 400,
            store : new Ext.data.Store({
              reader : new Ext.data.JsonReader({
                idProperty: 'id',
                fields: [
                  <?php echo $strFields ?>
                ]
              }),
              data : <?php echo json_encode(array_values($tabData)); ?>
            }),
            id : 'grid_<?php echo $idElt."_".$Action ?>',
            colModel: new Ext.grid.ColumnModel({
                defaults: {
                  sortable: false, hideable:false, menuDisabled:true, resizable: true
                },
                columns: [
                  <?php echo $strColumns ?>
                ]
            }),
            view: new Ext.grid.GridView({
              // render rows as they come into viewable area.
              scrollDelay: false,
              //cleanDelay: 200,
              
              deferEmptyText : false,
              forceFit: true,
              showPreview: true, // custom property
              enableRowBody: true, // required to create a second, full-width row to show expanded Record data
              
              // for correlated sdrs
              getRowClass: function(record, index, rowParam, store) {
                if ( index==0 ){
                  return "x-grid3-header";
                }
                if ( store.findExact("id", "-")==index ){
                  return "<?php echo $classSelection ?>";//"x-grid3-row-selected";
                }
                return "";
              }
            }),
            loadMask : {msg : "Chargement en cours..."},
            disableSelection : true,
            autoExpandColumn : 'col_name',
            iconCls: 'icon-grid',
            margins : '5 5 5 5',
            buttonAlign : 'center',
            border : true,
            buttons : [
              new Ext.Button({
                defaults : {buttonAlign:'center'}, 
                text : 'Enregistrer la configuration',
                margins : '5 5 5 5',
                handler : function(){
                  var oForm = document.getElementById('form_<?php echo $idElt."_".$Action ?>');
                  oForm.target = "<?php echo Ressources::$IFRAME_NAME_MAIN; ?>";
                  oForm.action = Routing.generate('catalogue_administration_profiles_objects_form', {'iPart':3, 'Action':'<?php echo $Action ?>', 'Id':'<?php echo $PK ?>'});
                  oForm.submit();
                }
              })
            ],
            listeners : {
              afterrender : {
                fn : function(grid){
                  grid.resizeEnable = true;
                  updateGridListSize(grid);
                  grid.on("afterrender", updateGridListSize);
                  Ext.getCmp('data_profiles').getActiveTab().gridList = grid;
                  Ext.getCmp('module_details').getActiveTab().gridList = grid;
                  tabGridList.push(grid);
                },
                delay : 100, 
                single : true
              }
            }
          })
        ]
      }));
      </script>
      <?php
    break;
    
    
    case "3" :
    
      $GrpTrtObjetVO_RL = new GrpTrtObjetVO();
      $GrpTrtObjetVO_RL->setDao($dao);
      $GrpTrtObjetVO_RL->DeleteRow( GrpTrtObjetVO::$FK_GRP_ID, $PROFIL, " and GRPTRTOBJ_FK_OBJ_TYPE_ID = ".$objet_type_id );
      $GrpTrtObjetVO_RL->Commit();
      
      $listTraitement = (isset($_POST["listTraitement"]) ? $_POST["listTraitement"] : array());
      $manage = (isset($_POST["manage"]) ? $_POST["manage"] : array());
      $traitement = (isset($_POST["traitement"]) ? $_POST["traitement"] : array());
      
      $bOk = true;
      foreach ($manage as $OBJECT_PK=>$checked){
        foreach ($listTraitement as $TRT_PK=>$TRT_ID){
          
          $status = (array_key_exists($OBJECT_PK, $traitement) && array_key_exists($TRT_PK, $traitement[$OBJECT_PK]) ? 1 : 0);
          $ordinalList = array();
          $valueList = array();
          $ordinalList[] = GrpTrtObjetVO::$FK_GRP_ID;
          $ordinalList[] = GrpTrtObjetVO::$FK_OBJET_ID;
          $ordinalList[] = GrpTrtObjetVO::$FK_TRT_ID;
          $ordinalList[] = GrpTrtObjetVO::$FK_OBJ_TYPE_ID;
          $ordinalList[] = GrpTrtObjetVO::$GRP_TRT_OBJET_STATUS;
          $valueList[] = $PROFIL;
          $valueList[] = $OBJECT_PK;
          $valueList[] = $TRT_ID;
          $valueList[] = $objet_type_id;
          $valueList[] = $status;
          
          $GrpTrtObjetVO_RL = new GrpTrtObjetVO();
          $GrpTrtObjetVO_RL->setDao($dao);
          $bOk &= $GrpTrtObjetVO_RL->InsertValues( $ordinalList, $valueList );
          $GrpTrtObjetVO_RL->Commit();
        }
      }

      $strJs = "";
      if ( $bOk ) 
        $strJs = "parent.reloadPanel(parent.getActiveTabId('data_profiles'));";
      AlertSaveController::AlertSaveDone($strJs, true);
    break;
  }

  /**
   * @brief retourne le droit de traitement sur un objet 
   * @param objet identifiant de l'objet
   * @param objet_type identifiant du type d'objet
   * @param grp identifiant du groupe de l'utilisateur courant
   */
  //function IsObjectManaged($objet, $objet_type, $grp){
  function IsObjectManaged($conn, $objet, $objet_type, $grp){
    
    $query = 'SELECT GRP_TRT_OBJET_STATUS FROM GRP_TRT_OBJET ' .
             'WHERE GRPTRTOBJ_FK_GRP_ID = '.$grp.
             ' AND GRPTRTOBJ_FK_OBJET_ID = '.$objet.
             ' AND GRPTRTOBJ_FK_OBJ_TYPE_ID = '.$objet_type;
    //$dao = new DAO();
    $dao = new DAO($conn, 'catalogue');
    $status = -1;
    if ($dao){
      $rs = $dao->BuildResultSet($query);
      for ($rs->First(); !$rs->EOF(); $rs->Next())
        $status = $rs->Read(0);
    }
    return $status;
  }
  
  /**
   * @brief retourne le type de l'objet en fonction de l'onglet sélectionné
   * @param action identifiant de l'onglet courant
   */
  function getObjetType($action){
    
    switch( $action )
    {
      case Ressources::$ACTION_OPEN_OBJECTS_DONNEE:
        return PRO_OBJET_TYPE_COUCHE;
      case Ressources::$ACTION_OPEN_OBJECTS_CARTE:
        return PRO_OBJET_TYPE_CARTE;
      case Ressources::$ACTION_OPEN_OBJECTS_METADONNEE:
        return PRO_OBJET_TYPE_METADONNEE;
      case Ressources::$ACTION_OPEN_OBJECTS_MODELE:
        return PRO_OBJET_TYPE_MODELE;
      case Ressources::$ACTION_OPEN_OBJECTS_GROUPS:
        return PRO_OBJET_TYPE_GROUP;
    }
    return PRO_OBJET_TYPE_COUCHE;
  }
  
  function getObjetTypeIntitule($action){
    switch( $action )
    {
      case Ressources::$ACTION_OPEN_OBJECTS_DONNEE:
        return "Couches";
      case Ressources::$ACTION_OPEN_OBJECTS_CARTE:
        return "Cartes";
      case Ressources::$ACTION_OPEN_OBJECTS_METADONNEE:
        return "Métadonnées";
      case Ressources::$ACTION_OPEN_OBJECTS_MODELE:
        return "Modèles";
      case Ressources::$ACTION_OPEN_OBJECTS_GROUPS:
        return "Profils";
    }
    return "Couches";
  }
?>
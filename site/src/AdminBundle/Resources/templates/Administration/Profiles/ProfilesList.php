<?php
/**
 * Liste des profils (gauche) dans l'adminsitration des profils (administration des droits)
 * @author Alkante
 */

	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/GroupeProfilVO.php");
	require_once($AdminPath."/Components/SelectList/SelectList.php");*/

  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GroupeProfilVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;
  

  header("Content-type: application/xml");
  echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php 

	$dao = new DAO($conn, 'catalogue');

	$groupeProfilVO = new GroupeProfilVO();
	$groupeProfilVO->setDao($dao);

	$selectList = new SelectList(	 "SelectListGroupeProfilVO",
									 $groupeProfilVO,
									 GroupeProfilVO::$PK_GROUPE_PROFIL,
									 GroupeProfilVO::$GRP_ID,
									 Ressources::$IFRAME_NAME_MAIN,
									 '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Profiles/ProfilesDetail.php",*/
									 Ressources::$SELECT_LIST_SIZE_X,
									 Ressources::$SELECT_LIST_SIZE_Y
									 );

?>

  </body>
</html>
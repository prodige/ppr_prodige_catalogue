<?php
/**
 * Accueil de l'administration des profils (administration des droits)
 * @author Alkante
 */
  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GroupeProfilVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 	
	// set a default action
	$action =  !isset($_GET["Action"]) ? Ressources::$ACTION_OPEN_PROFILES_ACCUEIL : $_GET["Action"];
	
	// set a default ID
	$id = !isset($_GET["Id"]) ? -1  : intval( $_GET["Id"] );

	if ( $id == -1 ){
		exit();
	}

	$accessRights = new AccessRights( );
	
	$dao = new DAO($conn, 'catalogue');
	$groupeProfilVO = new GroupeProfilVO();
	$groupeProfilVO->setDao($dao);

	$ordinalList = array();
	$ordinalListDisp = array();
	$ordinalType = array();
	$ordinalSize = array();		
	$ordinalReadOnly = array();	

	// Id
	$ordinalList[] = GroupeProfilVO::$GRP_ID;
	$ordinalListDisp[] = "ID :";
	$ordinalType[] = EditForm::$TYPE_TEXT;
	$ordinalSize[] = 20;
	$ordinalReadOnly[] = ( defined("PRO_INCLUDED") && PRO_INCLUDED ? true : false);

	// Nom
	$ordinalList[] = GroupeProfilVO::$GRP_NOM;
	$ordinalListDisp[] = "Nom :";
	$ordinalType[] = EditForm::$TYPE_TEXT;
	$ordinalSize[] = 30;
	$ordinalReadOnly[] = ( defined("PRO_INCLUDED") && PRO_INCLUDED ? true : false);

	// description
	$ordinalList[] = GroupeProfilVO::$GRP_DESCRIPTION;
	$ordinalListDisp[] = "Description :";
	$ordinalType[] = EditForm::$TYPE_TEXT;
	$ordinalSize[] = 50;
	$ordinalReadOnly[] = ( defined("PRO_INCLUDED") && PRO_INCLUDED ? true : false);

	// nom ldap
	$ordinalList[] = GroupeProfilVO::$GRP_NOM_LDAP;
	$ordinalListDisp[] = "Nom dans l'annuaire LDAP :";
	$ordinalType[] = EditForm::$TYPE_TEXT;
	$ordinalSize[] = 50;
	$ordinalReadOnly[] = ( defined("PRO_INCLUDED") && PRO_INCLUDED ? true : false);
	

	// template mail
	$ordinalList[] = GroupeProfilVO::TEMPLATE_MAIL;
	$ordinalListDisp[] = "Messages d'expiration du compte :";
	$ordinalType[] = EditForm::$TYPE_TEXTAREA;
	$ordinalSize[] = 50;
	$ordinalReadOnly[] = ( defined("PRO_INCLUDED") && PRO_INCLUDED ? true : false);

	print('<br><br>');
	
	$editForm = new EditForm(	"EditFormDomains",
								$groupeProfilVO,
								GroupeProfilVO::$PK_GROUPE_PROFIL,
								$id,
								$ordinalList,
								$ordinalListDisp,
								$ordinalType,
								$ordinalSize,
								$ordinalReadOnly,
								'',
								$action,
                null,
                defined("PRO_INCLUDED") && PRO_INCLUDED,
                defined("PRO_INCLUDED") && PRO_INCLUDED,
	              array(),
	              $submitUrl
							);
?>
	</body>
</html>
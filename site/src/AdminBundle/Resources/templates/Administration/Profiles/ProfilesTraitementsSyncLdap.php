<?php

use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Services\LdapUtils;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

/**
 * Author : Alkante
 * Service de synchronisation des  Utilisateurs vers le ldap
 * se base sur les données postées au service writeBOV.php
 * 
 */


    // instance de ldapUtils
    $ldapUtils = LdapUtils::getInstance(LDAP_HOST, LDAP_PORT, LDAP_DN_ADMIN, LDAP_PASSWORD, LDAP_BASE_DN);
    // Si $ordinalList est null on est en mode delete
    $dao = new DAO($conn, 'catalogue');
    $sqlUser = "SELECT usr.usr_id as uid ,"
        . "usr.pk_utilisateur as uidnumber,"
        . "usr.usr_nom as sn,"
        . "usr.usr_prenom as gn"
        . " from utilisateur as usr,grp_regroupe_usr as grp where grp.grpusr_fk_utilisateur = usr.pk_utilisateur and grp.GRPUSR_FK_GROUPE_PROFIL =$pk";
    
    $rs = $dao->BuildResultSet($sqlUser);
    for ($rs->First(); !$rs->EOF(); $rs->Next())
    {

        $tabUser['uid'] = $rs->Read(0);
        $tabUser['uidnumber'] = $rs->Read(1);
        $tabUser['sn'] = $rs->Read(2);
        $tabUser['gn'] = $rs->Read(3);
        // Sous domaines
        $sqlSousDomaine = "select sd.ssdom_id as alkMemberOfGroup "
                        . "from administrateurs_sous_domaine as asd, sous_domaine as sd "
                        . "where pk_utilisateur = ".$tabUser['uidnumber']." and asd.pk_sous_domaine = sd.pk_sous_domaine";
        $rsSousDomaine = $dao->BuildResultSet($sqlSousDomaine);
        $tabUser['alkmemberofgroup'] = [];
        for ($rsSousDomaine->First(); !$rsSousDomaine->EOF(); $rsSousDomaine->Next())
        {
            $tabUser['alkmemberofgroup'][] = $rsSousDomaine->Read(0);
        }
        //Profile
        $tabUser['alkmemberofprofile'] = 'RegisteredUser';
        // Si l'utilisateur est Reviewer on le met
        $sql = "select 'Reviewer' as traitement from traitements_utilisateur where pk_utilisateur = ".$tabUser['uidnumber']." and trt_id='CMS'";
        $rsReview = $dao->BuildResultSet($sql);
        for ($rsReview->First(); !$rsReview->EOF(); $rsReview->Next())
        {
            $tabUser['alkmemberofprofile'] = $rsReview->Read(0);
        }
        $sql = "select 'Administrator' as traitement from traitements_utilisateur where pk_utilisateur = ".$tabUser['uidnumber']." and trt_id='ADMINISTRATION'";
        $rsAdmin = $dao->BuildResultSet($sql);
        for ($rsAdmin->First(); !$rsAdmin->EOF(); $rsAdmin->Next())
        {
            $tabUser['alkmemberofprofile'] = $rsAdmin->Read(0);
        }
        $tabUser = $ldapUtils->decode_data($tabUser);
        
        //maj geonetwork
        $headers = array(CURLOPT_HTTPHEADER, array(
            'Accept: application/json, text/plain, */*'
        ));
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/', $headers);
        
        //list groupIds
        $groupArray = array();
        $sql = "select id from public.groups where name = ANY(:name)";
        
        $rs2 = $dao->BuildResultSet($sql, array("name" => '{' . implode(', ', $tabUser["alkmemberofgroup"]) . '}' ));
        for ($rs2->First(); !$rs2->EOF(); $rs2->Next()) {
            $groupArray[] = $rs2->Read(0);
        }
        
        $params = array(
            //"emailAddresses" => array($tabUser["mail"]),
            "username" => $tabUser["uid"],
            "name" => $tabUser["gn"],
            "surname" => $tabUser["sn"],
            "profile" => $tabUser["alkmemberofprofile"],
            "enabled" => true,
            "groupsRegisteredUser" => ($tabUser["alkmemberofprofile"] == "RegisteredUser" ? $groupArray : array()),
            "groupsEditor" => array(),
            "groupsReviewer" => ($tabUser["alkmemberofprofile"] == "Reviewer" ? $groupArray : array()),
            "groupsUserAdmin" => array()
        );
        
        $sql = "select id from public.users where username = :name";
        
        $rs3 = $dao->BuildResultSet($sql, array("name" => $tabUser["uid"] ));
        for ($rs3->First(); !$rs3->EOF(); $rs3->Next()) {
            $userId = $rs3->Read(0);
            $jsonResp = $geonetwork->put('api/users/'.$userId, $params, false, true);
        }
        
        
        // Mise à jour
        $ldapUtils->editUser($tabUser);
   
    }

    
    
?>
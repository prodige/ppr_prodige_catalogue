<?php

use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Services\LdapUtils;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

/**
 * Author : Alkante
 * Service de synchronisation des  Utilisateurs vers le ldap
 * se base sur les données postées au service writeBOV.php
 * 
 */

    $ldapUtils = LdapUtils::getInstance(LDAP_HOST, LDAP_PORT, LDAP_DN_ADMIN, LDAP_PASSWORD, LDAP_BASE_DN);
    // Si $ordinalList est null on est en mode delete
    $dao = new DAO($conn, 'catalogue');
    $sqlUser = "SELECT CONCAT(usr.usr_prenom, ' ', usr.usr_nom) as cn, "
        . "usr.usr_email as mail, "
        . "usr.usr_id as uid ,"
        . "usr.usr_id as o ,"
        . "usr.usr_nom as sn , "
        . "usr.usr_description as description , "
        . "usr.usr_prenom as gn , "
        . "usr.pk_utilisateur as uidnumber , "
        . "CONCAT('{md5}', usr.usr_password) as userpassword , "
        . "usr.usr_telephone as telephonenumber "
        . " from utilisateur as usr,grp_regroupe_usr as grp where grp.grpusr_fk_utilisateur = usr.pk_utilisateur and grp.GRPUSR_FK_GROUPE_PROFIL =$pk";

    $rs = $dao->BuildResultSet($sqlUser);
    for ($rs->First(); !$rs->EOF(); $rs->Next())
    {
        $tabUser['cn'] = $rs->Read(0);
        $tabUser['mail'] = $rs->Read(1);
        $tabUser['uid'] = $rs->Read(2);
        $tabUser['o'] = $rs->Read(3);
        $loginValue = $ldapUtils->decode_data($rs->Read(3));
        $tabUser['sn'] = $rs->Read(4);
        $tabUser['description'] = $rs->Read(5);
        $tabUser['gn'] = $rs->Read(6);
        $tabUser['uidnumber'] = $rs->Read(7);
        $tabUser['userpassword'] = $rs->Read(8);
        $tabUser['telephonenumber'] = $rs->Read(9);
        
        // Sous domaines
        $sqlSousDomaine = "select sd.ssdom_id as alkMemberOfGroup "
                        . "from administrateurs_sous_domaine as asd, sous_domaine as sd "
                        . "where pk_utilisateur = ".$tabUser['uidnumber']." and asd.pk_sous_domaine = sd.pk_sous_domaine and ssdom_admin_fk_groupe_profil <> $pk";
        $rsSousDomaine = $dao->BuildResultSet($sqlSousDomaine);
        $tabUser['alkmemberofgroup'] = [];
        for ($rsSousDomaine->First(); !$rsSousDomaine->EOF(); $rsSousDomaine->Next())
        {
            $tabUser['alkmemberofgroup'][] = $rsSousDomaine->Read(0);
        }

        //Profile
        $tabUser['alkmemberofprofile'] = 'RegisteredUser';
        // Si l'utilisateur est Reviewer on le met
        $sql = "select 'Reviewer' as traitement from traitements_utilisateur where pk_utilisateur = $pk and trt_id='CMS'";
        $rsReview = $dao->BuildResultSet($sql);
        for ($rsReview->First(); !$rsReview->EOF(); $rsReview->Next())
        {
            $tabUser['alkmemberofprofile'] = $rsReview->Read(0);
        }

        $sql = "select 'Administrator' as traitement from traitements_utilisateur where pk_utilisateur = $pk and trt_id='ADMINISTRATION'";
        $rsAdmin = $dao->BuildResultSet($sql);
        for ($rsAdmin->First(); !$rsAdmin->EOF(); $rsAdmin->Next())
        {
            $tabUser['alkmemberofprofile'] = $rsAdmin->Read(0);
        }
        $tabUser = $ldapUtils->decode_data($tabUser);
        
        
        //maj geonetwork
        $headers = array(CURLOPT_HTTPHEADER, array(
            'Accept: application/json, text/plain, */*'
        ));
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/', $headers);
        
        //list groupIds
        $groupArray = array();
        $sql = "select id from public.groups where name = ANY(:name)";
        
        $rs = $dao->BuildResultSet($sql, array("name" => '{' . implode(', ', $tabUser["alkmemberofgroup"]) . '}' ));
        for ($rs->First(); !$rs->EOF(); $rs->Next()) {
            $groupArray[] = $rs->Read(0);
        }
        
        $params = array(
            "emailAddresses" => array($tabUser["mail"]),
            "username" => $tabUser["uid"],
            "name" => $tabUser["gn"],
            "enabled" => true,
            "surname" => $tabUser["sn"],
            "profile" => $tabUser["alkmemberofprofile"],
            "groupsRegisteredUser" => ($tabUser["alkmemberofprofile"] == "RegisteredUser" ? $groupArray : array()),
            "groupsEditor" => array(),
            "groupsReviewer" => ($tabUser["alkmemberofprofile"] == "Reviewer" ? $groupArray : array()),
            "groupsUserAdmin" => array()
        );
        
        $sql = "select id from public.users where username = :name";
        
        $rs = $dao->BuildResultSet($sql, array("name" => $tabUser["uid"] ));
        for ($rs->First(); !$rs->EOF(); $rs->Next()) {
            $userId = $rs->Read(0);
            $jsonResp = $geonetwork->put('api/users/'.$userId, $params, false, true);
        }
        
        // Mise à jour
        $ldapUtils->editUser($tabUser);
    }

?>
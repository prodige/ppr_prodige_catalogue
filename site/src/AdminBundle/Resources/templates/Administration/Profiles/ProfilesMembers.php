<?php
/**
 * Onglet membres dans la gestion des profils
 * @author Alkante
 */

	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/GroupeProfilVO.php");
	require_once($AdminPath."/Modules/BO/GrpRegroupeUsrVO.php");
	require_once($AdminPath."/Components/RelationList/RelationList.php");
	require_once($AdminPath."/Components/SelectList/SelectList.php");	
  require_once($AdminPath."/Modules/BO/UtilisateurVO.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GroupeProfilVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GrpRegroupeUsrVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;
  
  set_time_limit(300);
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php

	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_PROFILES_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}

	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );
	}

	if ( $PK==-1 )
	{
		exit;
	}

	// set a default ID
	if ( !isset($_GET["RR"]) )
	{
		$RELATIONLIST_RESULT = -1;
	}
	else
	{
		$RELATIONLIST_RESULT = $_GET["RR"];
	}

	$dao = new DAO($conn, 'catalogue');

	if ( $RELATIONLIST_RESULT != -1 )
	{
	  switch( $Action )
		{
			case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
				$ordinalList = array();
				$valueList = array();
				$ordinalList[]=GrpRegroupeUsrVO::$GRPUSR_FK_GROUPE_PROFIL;
				$ordinalList[]=GrpRegroupeUsrVO::$GRPUSR_FK_UTILISATEUR;
				$valueList[]=$PK;
				$valueList[]=$RELATIONLIST_RESULT;

				$grpRegroupeUsrVO_RL = new GrpRegroupeUsrVO();
				$grpRegroupeUsrVO_RL->setDao($dao);
				$grpRegroupeUsrVO_RL->InsertValues( $ordinalList, $valueList );
				$grpRegroupeUsrVO_RL->Commit();
 			break;

			case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
				$grpRegroupeUsrVO_RL = new GrpRegroupeUsrVO();
				$grpRegroupeUsrVO_RL->setDao($dao);
				$grpRegroupeUsrVO_RL->DeleteRow( GrpRegroupeUsrVO::$PK_GRP_REGROUPE_USR, $RELATIONLIST_RESULT );
				$grpRegroupeUsrVO_RL->Commit();
			break;
		}
        AlertSaveController::AlertSaveDone("", false);
	}

	// #################################################################

	$grpRegroupeUsrVO = new GrpRegroupeUsrVO();
	$grpRegroupeUsrVO->setDao($dao);
	$utilisateurVO = new UtilisateurVO();
	$utilisateurVO->setDao($dao);

	// Build $utilisateurVO = ( UTILISATEUR(all) - GRPREGROUPEUSR(actual group) )
	$grpRegroupeUsrVO2 = new GrpRegroupeUsrVO();
	$grpRegroupeUsrVO2->setDao($dao);
	$grpRegroupeUsrVO2->AddRestriction( GrpRegroupeUsrVO::$GRPUSR_FK_GROUPE_PROFIL, $PK );
	$grpRegroupeUsrVO2->AddOnlyKeyProjection( GrpRegroupeUsrVO::$GRPUSR_FK_UTILISATEUR );
	$utilisateurVO->AddNotInRestriction( UtilisateurVO::$PK_UTILISATEUR, $grpRegroupeUsrVO2 );

	$relationList = new RelationList( "RelationListProfileUser",
									 Ressources::$RELATIONLIST_PROFILES_MEMBERS_RELATION,
									 $grpRegroupeUsrVO,
									 GrpRegroupeUsrVO::$PK_GRP_REGROUPE_USR,
									 GrpRegroupeUsrVO::$GRPUSR_USR_ID,
									 GrpRegroupeUsrVO::$GRPUSR_FK_GROUPE_PROFIL,
									 $PK,
									 GrpRegroupeUsrVO::$GRPUSR_FK_UTILISATEUR,
									 $utilisateurVO,
									 Ressources::$RELATIONLIST_PROFILES_MEMBERS_TABLE,
									 UtilisateurVO::$PK_UTILISATEUR,
									 UtilisateurVO::$USR_ID,
									 '' /*TODO hismail - argument ne sera pas utilisé ...  $AdminPath."/Administration/Profiles/ProfilesMembers.php"*/,
									 Ressources::$IFRAME_NAME_MAIN,
	                                 $this,
                   true,
                   false,
                   true,
                   "",
                   ( defined("PRO_INCLUDED") && PRO_INCLUDED ? true : false ) // formulaire un lecture seule si intégration de Prodige dans une application externe => profils non modifiables
									 );

?>
	</body>
</html>

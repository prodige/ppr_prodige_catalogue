<?php
/**
 * Onglet profil dans la gestion des traitements
 * @author Alkante
 */

    // TODO @vlc n'a pas été testé !
	require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Components/RelationList/RelationList.php");
	require_once($AdminPath."/Components/SelectList/SelectList.php");	
	require_once($AdminPath."/Modules/BO/GroupeProfilVO.php");
	require_once($AdminPath."/Modules/BO/GrpAutoriseTrtVO.php");
    require_once($AdminPath."/Modules/BO/TraitementVO.php");
    
    use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;
  
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 		
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_ACTIONS_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}

	// set a default ID
	if ( !isset($_GET["RR"]) )
	{
		$RELATIONLIST_RESULT = -1;
	}
	else
	{
		$RELATIONLIST_RESULT = $_GET["RR"];	
	}

	// #################################################################
	
	if ( $RELATIONLIST_RESULT != -1 )
	{
		switch( $Action )
		{
			case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
				$ordinalList = array();
				$valueList = array();
				$ordinalList[]=GrpAutoriseTrtVO::$GRPTRT_FK_GROUPE_PROFIL;
				$ordinalList[]=GrpAutoriseTrtVO::$GRPTRT_FK_TRAITEMENT;
				$valueList[]=$RELATIONLIST_RESULT;
				$valueList[]=$PK;

				$grpAutoriseTrtVO_RL = new GrpAutoriseTrtVO();
				$grpAutoriseTrtVO_RL->InsertValues( $ordinalList, $valueList );
				$grpAutoriseTrtVO_RL->Commit();
 			break;
			
			case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
				$grpAutoriseTrtVO_RL = new GrpAutoriseTrtVO();
				$grpAutoriseTrtVO_RL->DeleteRow( GrpAutoriseTrtVO::$PK_GRP_AUTORISE_TRT, $RELATIONLIST_RESULT );
				$grpAutoriseTrtVO_RL->Commit();
			break;
		}
    AlertSaveController::AlertSaveDone("", false);
	}

	// #################################################################

	$grpAutoriseTrtVO = new GrpAutoriseTrtVO();

	$grpAutoriseTrtVO2 = new GrpAutoriseTrtVO();
	$grpAutoriseTrtVO2->AddRestriction( GrpAutoriseTrtVO::$GRPTRT_FK_TRAITEMENT, $PK );
	$grpAutoriseTrtVO2->AddOnlyKeyProjection( GrpAutoriseTrtVO:: $GRPTRT_FK_GROUPE_PROFIL );

	$groupeProfilVO3 = new GroupeProfilVO();
	$groupeProfilVO3->AddNotInRestriction( GroupeProfilVO::$PK_GROUPE_PROFIL, $grpAutoriseTrtVO2 );
	
	$relationList = new RelationList( "RelationListActionProfiles",
									 Ressources::$RELATIONLIST_ACTIONS_PROFILES_RELATION,
									 $grpAutoriseTrtVO,
									 GrpAutoriseTrtVO::$PK_GRP_AUTORISE_TRT,
									 GrpAutoriseTrtVO::$GRPTRT_GRP_ID,
									 GrpAutoriseTrtVO::$GRPTRT_FK_TRAITEMENT,
									 $PK,
									 GrpAutoriseTrtVO::$GRPTRT_FK_GROUPE_PROFIL,
									 $groupeProfilVO3,
									 Ressources::$RELATIONLIST_ACTIONS_PROFILES_TABLE,
									 GroupeProfilVO::$PK_GROUPE_PROFIL,
									 GroupeProfilVO::$GRP_ID,
									 $AdminPath."/Administration/Actions/ActionsProfiles.php",
									 Ressources::$IFRAME_NAME_MAIN,
	                                 $this
									 );
	
?>
	</body>
</html>
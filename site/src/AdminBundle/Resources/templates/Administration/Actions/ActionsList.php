<?php
/**
 * Liste des traitements (gauche) dans l'adminsitration des traitements (administration des droits)
 * @author Alkante
 */
	require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/TraitementVO.php");
	require_once($AdminPath."/Components/SelectList/SelectList.php");
  
  header("Content-type: application/xml");
  echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 

	$traitementVO = new TraitementVO();

	//crée une liste html select
	$selectList = new SelectList(	 "SelectListTraitementVO",
									 $traitementVO,
									 TraitementVO::$PK_TRAITEMENT,
									 TraitementVO::$TRT_ID,
									 Ressources::$IFRAME_NAME_MAIN,
									 $AdminPath."/Administration/Actions/ActionsDetail.php",
									 Ressources::$SELECT_LIST_SIZE_X,
									 Ressources::$SELECT_LIST_SIZE_Y
									 );

?>

	</body>
</html>
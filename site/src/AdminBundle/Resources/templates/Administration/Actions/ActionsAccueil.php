<?php
/**
 * Accueil de la page traitements (Administration des droits)
 * @author Alkante
 */
	require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/TraitementVO.php");
	require_once($AdminPath."/Components/EditForm/EditForm.php");
	require_once($AdminPath."/Administration/AccessRights/AccessRights.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 		
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_ACTIONS_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}
	
	$traitementVO = new TraitementVO();
	$accessRights = new AccessRights( );

	$ordinalList = array();
	$ordinalListDisp = array();
	$ordinalType = array();
	$ordinalSize = array();		
	$ordinalReadOnly = array();
	
	$ordinalList[]=TraitementVO::$TRT_ID;
	$ordinalListDisp[]="ID :";
	$ordinalType[]=EditForm::$TYPE_TEXT;
	$ordinalSize[]=20;
	$ordinalReadOnly[]= !$accessRights->CanUpdate( $traitementVO, TraitementVO::$TRT_ID, $PK);
	
	$ordinalList[]=TraitementVO::$TRT_NOM;
	$ordinalListDisp[]="Nom :";
	$ordinalType[]=EditForm::$TYPE_TEXT;
	$ordinalSize[]=30;
	$ordinalReadOnly[]= !$accessRights->CanUpdate( $traitementVO, TraitementVO::$TRT_NOM, $PK);

	$ordinalList[]=TraitementVO::$TRT_DESCRIPTION;
	$ordinalListDisp[]="Description :";
	$ordinalType[]=EditForm::$TYPE_TEXT;
	$ordinalSize[]=50;
	$ordinalReadOnly[]= !$accessRights->CanUpdate( $traitementVO, TraitementVO::$TRT_DESCRIPTION, $PK);

	print('<br>');
	
	$editForm = new EditForm(	"EditFormTraitementVO",
								$traitementVO,
								TraitementVO::$PK_TRAITEMENT,
								$PK,
								$ordinalList,
								$ordinalListDisp,
								$ordinalType,
								$ordinalSize,
								$ordinalReadOnly,
								$AdminPath."/Administration/Actions/ActionsAccueil.php",
								$Action );
?>
	</body>
</html>
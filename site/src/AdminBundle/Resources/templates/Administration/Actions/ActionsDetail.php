<?php
/**
 * Page englobante les détails des traitements (Administration des droits)
 * @author Alkante
 */
	require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css">
      a {color:#000000;text-decoration:none}
      a:hover {text-decoration:underline}
    </style>
	</head>
	<body style="margin:0px;">
<?php 		
	
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_ACTIONS_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		$Action = Ressources::$ACTION_OPEN_ACTIONS_ACCUEIL;
		print('
			<TABLE style="text-align: center" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">			
				<TR width="100%" >
					<TD>
						'.Ressources::$ACTIONS_EMPTY_SELECT.'
					</TD>
				</TR>
			</TABLE>
			');
		
		exit;
	}

	if ( $Action == Ressources::$LIST_MODIFY_ACTION )
	{
		$Action = Ressources::$ACTION_OPEN_ACTIONS_ACCUEIL;
	}
	
	print('
		<TABLE style="text-align: justify" width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
			<TR width="100%" height="32">
				<TD>
					'.DrawHeader( $Action, $PK ).'
				</TD>
			</TR>
			<TR width="100%" height="100%">
				<TD><IFRAME width="'.Ressources::$SUB_DETAILS_SIZE_X.'"
						height="'.Ressources::$SUB_DETAILS_SIZE_Y.'" 
						MARGINHEIGHT="0"
						MARGINWIDTH="0" 
						FRAMEBORDER="0"
						name="'.Ressources::$IFRAME_NAME_MAIN.'"
						ALLOWTRANSPARENCY="true" 
						SCROLLING="no" 
						SRC="'.GetModulePageDetail($Action).'?Action='.$Action.'&Id='.$PK.'">
					</IFRAME>
				</TD>
			</TR>
		</TABLE>
	');
		

	// #################################################################
	
	/**
	 * Ecrit un tableau dans la page html
	 * @param $action
	 * @param $pk
	 * @return unknown_type
	 */
	function DrawHeader( $action, $pk )
	{
		$header ='
			<TABLE style="text-align: center" width="200" height="32" border="0" cellpadding="0" cellspacing="0">
				<TR>
					<TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_ACTIONS_ACCUEIL ).'">
						<A HREF="ActionsDetail.php?Action='.Ressources::$ACTION_OPEN_ACTIONS_ACCUEIL.'&Id='.$pk.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
							'.Ressources::$BTN_ACTIONS_INITIAL.'
						</A>
					</TD>
					<TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_ACTIONS_PROFILES, true ).'">
						<A HREF="ActionsDetail.php?Action='.Ressources::$ACTION_OPEN_ACTIONS_PROFILES.'&Id='.$pk.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
							'.Ressources::$BTN_ACTIONS_PROFILES.'
						</A>
					</TD>
				</TR>
			</TABLE>';
		
		return $header;
	}

	// #################################################################
	
	/**
	 * Renvoie la chaîne de caractères correspondant à l'attribut html style d'un tableau
	 * @param $action
	 * @param $celAction
	 * @param $bLast
	 * @return $strStyle : style du tableau
	 */
  function GetHeaderTableBackground( $action, $celAction, $bLast=false )
  {
    $strStyle = "border:1px solid #000000;".(!$bLast ? "border-right:none;" : "")."padding:5px;background-color:";
    if ( $action == $celAction )
    {
      return $strStyle."#FFFFFF;border-bottom:none;";
    }

    return $strStyle."#dddddd;";
  }
	
  /**
   * Renvoie l'url de la page
   * @param $action
   * @return url de la page
   */
	function GetModulePageDetail( $action )
	{
		switch( $action )
		{
			case Ressources::$ACTION_OPEN_ACTIONS_ACCUEIL:
				return "../../Administration/Actions/ActionsAccueil.php";
			break;
			case Ressources::$ACTION_OPEN_ACTIONS_PROFILES:
				return "../../Administration/Actions/ActionsProfiles.php";
			break;
		}

		return "../../Administration/Actions/Empty.php";
	}
	
?>
	</body>
</html>
<?php

use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Services\LdapUtils;

use Prodige\ProdigeBundle\Services\GeonetworkInterface;
/**
 * Author : Alkante
 * Service de synchronisation des  Utilisateurs vers le ldap
 * se base sur les données postées au service writeBOV.php
 * 
 */



    // instance de ldapUtils
    $ldapUtils = LdapUtils::getInstance(LDAP_HOST, LDAP_PORT, LDAP_DN_ADMIN, LDAP_PASSWORD, LDAP_BASE_DN);
    $found = false;
    $dao = new DAO($conn, 'catalogue,public');
    if (!$delete) 
    {    
        
        $sqlGroup = "SELECT ssdom_id as ou, "
                . "ssdom_nom as alkprofilename , "
                . "ssdom_description as description ,"
                . "pk_sous_domaine as gidnumber "
                . " from sous_domaine where pk_sous_domaine=$pk";

        $rs = $dao->BuildResultSet($sqlGroup);
        for ($rs->First(); !$rs->EOF(); $rs->Next())
        {
            $found = true;
            $tabSousGroupe['ou'] = $rs->Read(0);
            $loginValue = $ldapUtils->decode_data($rs->Read(0));
            $tabSousGroupe['alkprofilename'] = $rs->Read(1);
            $tabSousGroupe['description'] = $rs->Read(2);
            $tabSousGroupe['gidnumber'] = $rs->Read(3);
        }
    
        // User
        $sqlUser = "select asd.usr_id as alkmember "
                . "from administrateurs_sous_domaine as asd, sous_domaine as sd "
                . "where sd.pk_sous_domaine = $pk and asd.pk_sous_domaine = sd.pk_sous_domaine";
        
        $rs = $dao->BuildResultSet($sqlUser);
        $tabSousGroupe['alkmember'] = [];
        for ($rs->First(); !$rs->EOF(); $rs->Next())
        {
            if ($rs->Read(0) != null)
            {
                $tabSousGroupe['alkmember'][] = $rs->Read(0);
            }
        }
        
        $tabSousGroupe = $ldapUtils->decode_data($tabSousGroupe);
    }

    if ($ldapUtils->groupExistFromPk($pk))
    {
        if ($delete)
        {
            // Supprimer
            //update metadata groupowner set to 1 (ALL/Internet)
            $strSql = "update metadata set groupowner=1 where groupowner=:pk;";
            $dao->Execute($strSql, array("pk"=>$pk));
            $headers = array(CURLOPT_HTTPHEADER, array(
                'Accept: application/json, text/plain, */*'
            ));
            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/', $headers);
            $jsonResp = $geonetwork->delete('api/groups/'.$pk, false);
            
            $ldapUtils->deleteGroup($pk);
        }
        else
        {
            if (!$ldapUtils->groupExist($loginValue))
            {
                // Renommer
                $ldapUtils->renameGroup($pk,$loginValue);
            }
            // Mise à jour SQL (TODO geonetwork would be better)
            $strSql = "update public.groups set name=:name,  description=:description where id = :id" ;
            $dao->Execute($strSql, array("id"=>$tabSousGroupe["gidnumber"], 
                                         "name" => substr($tabSousGroupe["ou"], 0, 32), 
                                         "description" => $tabSousGroupe["alkprofilename"]));
            $strSql = "update public.groupsdes set label =:name where iddes=:id " ;
            $dao->Execute($strSql, array("id"=>$tabSousGroupe["gidnumber"], 
                                         "name" => substr($tabSousGroupe["ou"], 0, 96)));
            
            $ldapUtils->editGroup($tabSousGroupe);
        }

    } else {
        // Create
        if ($found) {
            //since synchro LDAP does not work properly in geonetwork (miss groupsdes)
            $sqlGroup = "select id from public.groups where name=:group";
            $rs = $dao->BuildResultSet($sqlGroup, array("group"=>$loginValue));
            if($rs->GetNbRows()==0){
                $strSql = "insert into public.groups (id, name, description, email, referrer, logo, website)" .
                    "select  :pk, substr(ssdom_id,0,32), ssdom_description, '', NULL, '', '' from catalogue.sous_domaine ".
                    "where pk_sous_domaine=:pk;";
                $dao->Execute($strSql, array("pk"=>$pk));
                $strSql = "insert into public.groupsdes (iddes,	langid, 	label) " .
                    "select  :pk, languages.id,  substr(sous_domaine.ssdom_nom,0,96) from catalogue.sous_domaine, languages where pk_sous_domaine=:pk ;";
                $dao->Execute($strSql, array("pk"=>$pk));
            
            }
            $ldapUtils->addGroup($tabSousGroupe);
            
        }
        
    }

    
?>
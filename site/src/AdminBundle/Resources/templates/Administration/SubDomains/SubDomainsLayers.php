<?php
/**
 * Onglet couches dans la gestion des sous-domaines
 * @author Alkante
 */

    // TODO @vlc n'a pas été testé
	require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/SSDomDisposeCoucheVO.php");
	require_once($AdminPath."/Modules/BO/CoucheDonneesVO.php");
	
	require_once($AdminPath."/Components/RelationList/RelationList.php");
    
    use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 	
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_SUBDOMAINS_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}

	// set a default ID
	if ( !isset($_GET["RR"]) )
	{
		$RELATIONLIST_RESULT = -1;
	}
	else
	{
		$RELATIONLIST_RESULT = $_GET["RR"];	
	}

	
	if ( $RELATIONLIST_RESULT != -1 )
	{
		switch( $Action )
		{
			case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
				$ordinalList = array();
				$valueList = array();
				$ordinalList[]=SSDomDisposeCoucheVO::$SSDCOUCH_FK_SOUS_DOMAINE;
				$ordinalList[]=SSDomDisposeCoucheVO::$SSDCOUCH_FK_COUCHE_DONNEES;
				$valueList[]=$PK;
				$valueList[]=$RELATIONLIST_RESULT;

				$sSDomDisposeCoucheVO_RL = new SSDomDisposeCoucheVO();
				$sSDomDisposeCoucheVO_RL->InsertValues( $ordinalList, $valueList );
				$sSDomDisposeCoucheVO_RL->Commit();
 			break;
			
			case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
				$sSDomDisposeCoucheVO_RL = new SSDomDisposeCoucheVO();
				$sSDomDisposeCoucheVO_RL->DeleteRow( SSDomDisposeCoucheVO::$PK_SSDOM_DISPOSE_COUCHE, $RELATIONLIST_RESULT );
				$sSDomDisposeCoucheVO_RL->Commit();
			break;
		}
        AlertSaveController::AlertSaveDone("", false);
	}
	
	// #################################################################

	$sSDomDisposeCoucheVO = new SSDomDisposeCoucheVO();
	
	$sSDomDisposeCoucheVO2 = new SSDomDisposeCoucheVO();
	$sSDomDisposeCoucheVO2->AddRestriction( SSDomDisposeCoucheVO::$SSDCOUCH_FK_SOUS_DOMAINE, $PK );
	$sSDomDisposeCoucheVO2->AddOnlyKeyProjection( SSDomDisposeCoucheVO::$SSDCOUCH_FK_COUCHE_DONNEES );

	$coucheDonneesVO = new CoucheDonneesVO();
	$coucheDonneesVO->AddNotInRestriction( CoucheDonneesVO::$PK_COUCHE_DONNEES, $sSDomDisposeCoucheVO2 );
		
	$relationList = new RelationList( "RelationListSSdomLayer",
									 Ressources::$RELATIONLIST_SUBDOMAINS_LAYERS_RELATION,
									 $sSDomDisposeCoucheVO,
									 SSDomDisposeCoucheVO::$PK_SSDOM_DISPOSE_COUCHE,
									 SSDomDisposeCoucheVO::$SSDCOUCH_COUCHD_ID,
									 SSDomDisposeCoucheVO::$SSDCOUCH_FK_SOUS_DOMAINE,
									 $PK,
									 SSDomDisposeCoucheVO::$PK_SSDOM_DISPOSE_COUCHE,
									 $coucheDonneesVO,
									 Ressources::$RELATIONLIST_SUBDOMAINS_LAYERS_TABLE,
									 CoucheDonneesVO::$PK_COUCHE_DONNEES,
									 CoucheDonneesVO::$COUCHD_ID,
									 $AdminPath."/Administration/SubDomains/SubDomainsLayers.php",
									 Ressources::$IFRAME_NAME_MAIN,
	                                 $this
									 );
	
?>
	</body>
</html>
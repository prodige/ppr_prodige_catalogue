<?php
/**
 * Accueil de la page sous-domaines (Administration des droits)
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/SousDomaineVO.php");
	require_once($AdminPath."/Components/EditForm/EditForm.php");
	require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\SousDomaineVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;


?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 	
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_SUBDOMAINS_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}

	$dao = new DAO($conn, 'catalogue');

	$sousDomaineVO = new SousDomaineVO();
	$sousDomaineVO->setDao($dao);
	$accessRights = new AccessRights( );

	$ordinalList = array();
	$ordinalListDisp = array();
	$ordinalType = array();
	$ordinalSize = array();		
	$ordinalReadOnly = array();
	$fieldOnChange = array();
	

	$ordinalList[]=SousDomaineVO::$SSDOM_ID;
	$ordinalListDisp[]="ID :";
	$ordinalType[]=EditForm::$TYPE_TEXT;
	$ordinalSize[]=20;
	$ordinalReadOnly[]= !$accessRights->CanUpdate( $sousDomaineVO, SousDomaineVO::$SSDOM_ID, $PK);
        $fieldOnChange[] = null;

	$ordinalList[]=SousDomaineVO::$SSDOM_NOM;
	$ordinalListDisp[]="Nom :";
	$ordinalType[]=EditForm::$TYPE_TEXT;
	$ordinalSize[]=30;
	$ordinalReadOnly[]= !$accessRights->CanUpdate( $sousDomaineVO, SousDomaineVO::$SSDOM_NOM, $PK);
        $fieldOnChange[] = null;

	$ordinalList[]=SousDomaineVO::$SSDOM_DESCRIPTION;
	$ordinalListDisp[]="Description :";
	$ordinalType[]=EditForm::$TYPE_TEXT;
	$ordinalSize[]=50;
	$ordinalReadOnly[]= !$accessRights->CanUpdate( $sousDomaineVO, SousDomaineVO::$SSDOM_DESCRIPTION, $PK);
        $fieldOnChange[] = null;

	$ordinalList[]=SousDomaineVO::$SSDOM_SERVICE_WMS_UUID;
	$ordinalListDisp[]="<!--hidden-->";
	$ordinalType[]=EditForm::$TYPE_HIDDEN;
	$ordinalSize[]=50;
	$ordinalReadOnly[]= false;
	$arraySelect[] = array();
        $fieldOnChange[] = null;
        
	$ordinalList[]=SousDomaineVO::$SSDOM_SERVICE_WMS;
	$ordinalListDisp[]="Service WMS dédié au sous-domaine :";
	$ordinalType[]=EditForm::$TYPE_CB;
	$ordinalSize[]=50;
	$ordinalReadOnly[]= !$accessRights->CanUpdate( $sousDomaineVO, SousDomaineVO::$SSDOM_SERVICE_WMS, $PK);
	$arraySelect[] = array();
        $fieldOnChange[] = "onChangeWMS(this, 'ORDINAL_".SousDomaineVO::$SSDOM_SERVICE_WMS_UUID."')";
        
        
	print('<br>');
	
	$editForm = new EditForm(	"EditFormSubDomains",
								$sousDomaineVO,
								SousDomaineVO::$PK_SOUS_DOMAINE,
								$PK,
								$ordinalList,
								$ordinalListDisp,
								$ordinalType,
								$ordinalSize,
								$ordinalReadOnly,
								'', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/SubDomains/SubDomainsAccueil.php",*/
								$Action,
	              $fieldOnChange, false, false, array(),
	              $submitUrl);

?>
<script type="text/javascript">

function onChangeWMS(checkbox, wms_uuid_fieldname) 
{
  var hidden = Ext.DomQuery.selectNode('input[type=hidden][name='+wms_uuid_fieldname+']');
  if ( hidden ) {
    if ( hidden.value && !checkbox.checked ){
      if ( !confirm("Attention, la désactivation du service WMS entraînera la suppression du service et de sa métadonnée."+
                    "\n\nConfirmez-vous la désactivation ?" )) {
        checkbox.checked = true;
      }
    }
  }
}
//surcharge de la fonction de validation / modification des métadonnées
function EditFormSubDomainsValidate_onclick() 
{
var form = document.getElementById("EditFormSubDomains");
var inputId = document.getElementById("Id");
/*form.action = "php--- echo PRO_CATALOGUE_URLBASE; ---php droits.php?url=Administration/Components/EditForm/WriteBOV.php";*/
form.action = "<?php  echo $submitUrl; ?>";
parent.Ext.Ajax.timeout = 900000;
parent.Ext.Ajax.request({
/*url : 'php--- echo PRO_CATALOGUE_URLBASE ---php Services/updateMdataDomSdom.php',*/
url : '<?php echo $ajaxRequestUrl; ?>',
    method : 'GET',
    params : {
      'mode' : 'updatesubdomain',
      'newSubDomain' : document.getElementById("ORDINAL_3").value,
      'id' : inputId.value
    },
success: function(result){
  form.submit();
},
failure: function(result){
  parent.Ext.MessageBox.alert('Failed','erreur lors de la mise à jour de l\'information des métadonnées'); 
} 
});
}
function EditFormSubDomainsDelete_onclick() 
{
    var form = document.getElementById("EditFormSubDomains");
    
    var inputId = document.getElementById("Id");
    var inputAction = document.getElementById("Action");
    
    inputAction.value = '<?php echo Ressources::$EDITFORM_DELETE;?>'; 
    /*form.action = "php--- echo PRO_CATALOGUE_URLBASE; ---php droits.php?url=Administration/Components/EditForm/WriteBOV.php";*/
    form.action = "<?php  echo $submitUrl; ?>";
    if (confirm("Êtes-vous sûr de vouloir supprimer cet enregistrement ?")){
      parent.Ext.Ajax.timeout = 900000;
      parent.Ext.Ajax.request({
        /*url : 'php--- echo PRO_CATALOGUE_URLBASE ---php Services/updateMdataDomSdom.php',*/
        url : '<?php echo $ajaxRequestUrl; ?>',
        method : 'GET',
        params : {
          'userId' : <?php  echo $PK;?>,
          'mode' : 'delsubdomain',
          'id' : inputId.value
        },
        success: function(result){
          form.submit();
        },
        failure: function(result){
          parent.Ext.MessageBox.alert('Failed', 'erreur lors de la suppression de l\'information des métadonnées'); 
        } 
        });
    }
  }
</script>
	</body>
</html>
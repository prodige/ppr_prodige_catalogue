<?php

use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Services\LdapUtils;

/**
 * Author : Alkante
 * Service de synchronisation des  Utilisateurs vers le ldap
 * se base sur les données postées au service writeBOV.php
 * 
 */


    // instance de ldapUtils
    $ldapUtils = LdapUtils::getInstance(LDAP_HOST, LDAP_PORT, LDAP_DN_ADMIN, LDAP_PASSWORD, LDAP_BASE_DN);
    // Si $ordinalList est null on est en mode delete
    $dao = new DAO($conn, 'catalogue');

    $sqlGroup = "SELECT ssdom_id as ou, "
            . "ssdom_nom as alkprofilename , "
            . "ssdom_description as description ,"
            . "pk_sous_domaine as gidnumber "
            . " from sous_domaine where ssdom_admin_fk_groupe_profil=$pk";

    $rs = $dao->BuildResultSet($sqlGroup);
    for ($rs->First(); !$rs->EOF(); $rs->Next())
    {
        $tabSousGroupe['ou'] = $rs->Read(0);
        $loginValue = $ldapUtils->decode_data($rs->Read(0));
        $tabSousGroupe['alkprofilename'] = $rs->Read(1);
        $tabSousGroupe['description'] = $rs->Read(2);
        $tabSousGroupe['gidnumber'] = $rs->Read(3);
        $tabSousGroupe['alkmember'] = [];
        $tabSousGroupe = $ldapUtils->decode_data($tabSousGroupe);
        $ldapUtils->editGroup($tabSousGroupe);
    }

    
?>
<?php
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
$conn instanceof \Doctrine\DBAL\Connection;
$dao = new DAO($conn, 'catalogue');

if ( !function_exists('findMetadataWMS') ) {
    function findMetadataWMS($dao, $urlWebservice, $ssdom_fmeta_uuid=null){
        $metadataId = null;
        $uuid = null;
        $data = null;
        
        if ( $ssdom_fmeta_uuid ){
            $rs = $dao->BuildResultSet("select id, uuid, data from public.metadata where uuid=:ssdom_fmeta_uuid", compact("ssdom_fmeta_uuid"));
            if ( $rs->GetNbRows() > 0 ) {
                $rs->First();
                $metadataId = $rs->Read(0);
                $uuid = $rs->Read(1);
                $data = $rs->ReadHtml(2);
                return array($metadataId, $uuid, $data);
            }
        }
        
        $namespaces = "ARRAY[ARRAY['gmd', 'http://www.isotc211.org/2005/gmd'], ARRAY['gco','http://www.isotc211.org/2005/gco'], ARRAY['srv', 'http://www.isotc211.org/2005/srv']]"; 
        $xpath = "'/".implode("/", array(
                    "gmd:MD_Metadata", 
                    "gmd:identificationInfo",  "srv:SV_ServiceIdentification", 
                    "srv:containsOperations",  "srv:SV_OperationMetadata", 
                    "srv:connectPoint", 
                    "gmd:CI_OnlineResource",   "gmd:linkage",  "gmd:URL"
                 ))."[text()='||quote_literal(?)||']'";
        $rs = $dao->BuildResultSet("select id, uuid, data from public.metadata "
              . " where data like '<gmd:MD_Metadata%'"
              . " and xpath_exists((".$xpath."), data::xml, ".$namespaces.")", array (
              html_entity_decode($urlWebservice)
        ));

        if ( $rs->GetNbRows() > 0 ) {
            $rs->First();
            $metadataId = $rs->Read(0);
            $uuid = $rs->Read(1);
            $data = $rs->ReadHtml(2);

        }
        return array($metadataId, $uuid, $data);
    }
}//function_exists


if ( !function_exists('deleteServiceWMSFiche') ) {
    /**
     * création de la métadonnée de service de carte et remplissage automatique de son contenu
     * 
     * @param unknown $data            
     * @param unknown $tabLayer            
     * @param unknown $pk_carte_projet            
     * @param unknown $dao            
     * @param unknown $cartp_emplacement_stockage            
     */
    function deleteServiceWMSFiche($controller, $dao, $urlWebservice, $ssdom_fmeta_uuid=null) {

        list($metadataId, $uuid, $metadata_data) = findMetadataWMS($dao, $urlWebservice, $ssdom_fmeta_uuid);
        global $kernel;
        if($uuid!=""){
          $urlCatalogue = rtrim($kernel->getContainer()->getParameter('PRODIGE_URL_CATALOGUE'), "/");
          $deleteUrl = $urlCatalogue.'/deleteMetaData/'.$uuid;
          $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($deleteUrl);
          $jsonResp = $controller->curl($deleteUrl, 'GET', array('ticket'=>$ticket),  array());
        }
        return array(null, null);
    }
}//function_exists

if ( !function_exists('createServiceWMSFiche') ) {
    /**
     * création de la métadonnée de service de carte et remplissage automatique de son contenu
     * 
     * @param unknown $data            
     * @param unknown $tabLayer            
     * @param unknown $pk_carte_projet            
     * @param unknown $dao            
     * @param unknown $cartp_emplacement_stockage            
     */
    function createServiceWMSFiche( $controller, $dao, $domaine_id, $ssdom_fmeta_uuid, array $labels, $thesaurus, $urlWebservice) {

        $title = "Service WMS - ".html_entity_decode(urldecode(implode(" / ", $labels)));
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
        // global $PRO_GEONETWORK_DIRECTORY, $accs_adresse_data;

        //groupowner is All(Internet)
        $group_id = 1;
        global $kernel;
        //if not groupowner for metadata, take first on wich user has rights to edit
        $query = "select groupid from public.users left join public.usergroups on usergroups.userid = users.id ".
            " where usergroups.profile >1 and username=:username limit 1";
        $userSingleton = User::GetUser($kernel->getContainer());
        $rs = $dao->BuildResultSet($query, array (
            "username" => $userSingleton->getLogin()
        ));
        if ( $rs->GetNbRows() > 0 ) {
            $rs->First();
            $group_id = $rs->Read(0);
        }

        $metadataId = null;
        $uuid = null;
        $metadata_data = null;

        list($metadataId, $uuid, $metadata_data) = findMetadataWMS($dao, $urlWebservice, $ssdom_fmeta_uuid);

        /* Création de la métadonnée de service */
        if ( $metadataId===null ) {
            
            $template_id = PRO_WMS_METADATA_ID;

            $jsonResp = $geonetwork->get('md.create?_content_type=json&id='.$template_id.'&group=' . $group_id . "&title=".urlencode($title)."&template=n&child=n&fullPrivileges=false");
            $jsonObj = json_decode($jsonResp);
            if ( $jsonObj && $jsonObj->id ) {
                $metadataId = $jsonObj->id;
            } else {
                throw new \Exception("Impossible de créer la métadonnée de service WMS de ".$domaine_id);
            }

            // load metadata
            $metadata_data = "";
            $query = 'SELECT data, uuid
                      FROM public.metadata' . ' where id = ?';
            $rs = $dao->BuildResultSet($query, array (
                    $metadataId 
            ));
            if ( $rs->GetNbRows() > 0 ) {
                $rs->First();
                $metadata_data = $rs->ReadHtml(0);
                $uuid = $rs->Read(1);
            }
        }


        $version = "1.0";
        $encoding = "UTF-8";
        $metadata_doc = new \DOMDocument($version, $encoding);
        $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
        $metadata_data = $entete . $metadata_data;
        if ( @$metadata_doc->loadXML($metadata_data) !== false ) {
            $xpath = new \DOMXPath($metadata_doc);
            $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString");
            if ( $objXPath->length > 0 ) {
                $element = $objXPath->item(0);
                $newelement = $metadata_doc->createElement('gco:CharacterString', $title);
                $element->parentNode->replaceChild($newelement, $element);
            }
            $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:abstract/gco:CharacterString");
            if ( $objXPath->length > 0 ) {
                $element = $objXPath->item(0);
                $newelement = $metadata_doc->createElement('gco:CharacterString', "Service WMS du domaine ".html_entity_decode(urldecode(implode(" / ", $labels))));
                $element->parentNode->replaceChild($newelement, $element);
            }
            $serviceType = $metadata_doc->getElementsByTagName("serviceType");
            $objXPath = @$xpath->query("//gco:LocalName", $serviceType->item(0));
            if ( $objXPath && $objXPath->length > 0 ) {
                $element = $objXPath->item(0);
                $newelement = $metadata_doc->createElement('gco:LocalName', "view");
                $element->parentNode->replaceChild($newelement, $element);
            }

            $strXml = '<srv:coupledResource xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">';
            $strXmlOp = "<tag xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:srv=\"http://www.isotc211.org/2005/srv\">";
            $strXmlOp .= "</tag>";
            $strXml .= "</srv:coupledResource>";
            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXml);
            $nodesToImport = $tagRepport->getElementsByTagName("SV_CoupledResource");
            $coupledResource = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
            if ( $coupledResource && $coupledResource->length > 0 ) {

                for($c = 0; $c < $nodesToImport->length; $c ++) {
                    $newNodeDesc = $metadata_doc->importNode($nodesToImport->item($c ), true);

                    if($metadata_doc->getElementsByTagName("coupledResource") && $metadata_doc->getElementsByTagName("coupledResource")->length>0){
                        $firstCoupledResource = $metadata_doc->getElementsByTagName("coupledResource")->item(0);
                        $firstCoupledResource->parentNode->insertBefore($newNodeDesc, $firstCoupledResource);
                    }else{
                        $coupledResource->item(0)->appendChild($newNodeDesc);
                    }

                }
            }

            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXmlOp);
            $nodes = $tagRepport->getElementsByTagName("operatesOn");
            $serviceIdentification = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");

            if ( $serviceIdentification && $serviceIdentification->length > 0 ) {
                for($c = 0; $c < $nodes->length; $c ++) {
                    $nodeToImport = $nodes->item($c);
                    $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);

                    if($metadata_doc->getElementsByTagName("operatesOn") && $metadata_doc->getElementsByTagName("operatesOn")->length>0){
                        $firstOperatesOn = $metadata_doc->getElementsByTagName("operatesOn")->item(0);
                        $firstOperatesOn->parentNode->insertBefore($newNodeDesc, $firstOperatesOn);
                    }else{
                        $serviceIdentification->item(0)->appendChild($newNodeDesc);
                    }

                }
            }

            $strXml = '<test xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">
             <srv:containsOperations>
              <srv:SV_OperationMetadata>
                <srv:operationName>
                  <gco:CharacterString>GetCapabilities</gco:CharacterString>
                </srv:operationName>
                <srv:DCP>
                  <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                </srv:DCP>
                <srv:connectPoint>
                  <gmd:CI_OnlineResource>
                    <gmd:linkage>
                      <gmd:URL>' . $urlWebservice . '</gmd:URL>
                    </gmd:linkage>
                    <gmd:protocol>
                      <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>
                    </gmd:protocol>
                  </gmd:CI_OnlineResource>
                </srv:connectPoint>
              </srv:SV_OperationMetadata>
            </srv:containsOperations>
            <srv:containsOperations>
              <srv:SV_OperationMetadata>
                <srv:operationName>
                  <gco:CharacterString>GetMap</gco:CharacterString>
                </srv:operationName>
                <srv:DCP>
                  <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                </srv:DCP>
                <srv:connectPoint>
                  <gmd:CI_OnlineResource>
                    <gmd:linkage>
                      <gmd:URL>' . $urlWebservice . '</gmd:URL>
                    </gmd:linkage>
                    <gmd:protocol>
                      <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>
                    </gmd:protocol>
                  </gmd:CI_OnlineResource>
                </srv:connectPoint>
              </srv:SV_OperationMetadata>
            </srv:containsOperations>
            <srv:containsOperations>
              <srv:SV_OperationMetadata>
                <srv:operationName>
                  <gco:CharacterString>GetFeatureInfo</gco:CharacterString>
                </srv:operationName>
                <srv:DCP>
                  <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                </srv:DCP>
                <srv:connectPoint>
                  <gmd:CI_OnlineResource>
                    <gmd:linkage>
                      <gmd:URL>' . $urlWebservice . '</gmd:URL>
                    </gmd:linkage>
                    <gmd:protocol>
                      <gco:CharacterString>OGC:WMS-1.1.1-http-get-map</gco:CharacterString>
                    </gmd:protocol>
                  </gmd:CI_OnlineResource>
                </srv:connectPoint>
              </srv:SV_OperationMetadata>
            </srv:containsOperations></test>';

            $tagRepport = new \DOMDocument();
            $tagRepport->loadXML($strXml);

            // suppression des tags containsOperations existant
            $nodeToDelete = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/srv:containsOperations");
            for($c = 0; $c < $nodeToDelete->length; $c ++) {
                $nodeToDelete->item($c )->parentNode->removeChild($nodeToDelete->item($c));
            }
            
            $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource");
            if ( $objXPath->length > 0 ) {
                foreach($objXPath as $item){
                    $item->parentNode->removeChild($item);
                }
            }
            $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn");
            if ( $objXPath->length > 0 ) {
                foreach($objXPath as $item){
                    $item->parentNode->removeChild($item);
                }
            }
            $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine");
            if ( $objXPath->length > 0 ) {
                foreach($objXPath as $item){
                    $item->parentNode->removeChild($item);
                }
            }
            $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:date/gmd:CI_Date/gmd:date/gco:Date");
            if ( $objXPath->length > 0 ) {
                $item = $objXPath->item(0);
                $item->nodeValue = date("Y-m-d");
            }

            // création des tags containsOperations nécessaire (WMS protocole)
            $coupledResource = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
            if ( $coupledResource && $coupledResource->length > 0 ) {
                $nodes = $tagRepport->getElementsByTagName("containsOperations");
                for($c = 0; $c < $nodes->length; $c ++) {
                    $nodeToImport = $nodes->item($c);
                    $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                    $coupledResource->item(0 )->appendChild($newNodeDesc);
                }
            }
        }

        $strXmlDistributionInfo = "<tag  xmlns:gmd=\"http://www.isotc211.org/2005/gmd\" xmlns:gco=\"http://www.isotc211.org/2005/gco\">
        <gmd:onLine>
            <gmd:CI_OnlineResource>
                <gmd:linkage>
                    <gmd:URL>" . $urlWebservice . "</gmd:URL>
                </gmd:linkage>
                <gmd:protocol>
                    <gco:CharacterString>application/vnd.ogc.wms_xml</gco:CharacterString>
                </gmd:protocol>
                <gmd:description>
                    <gco:CharacterString>Accès au service WMS</gco:CharacterString>
                </gmd:description>
            </gmd:CI_OnlineResource>
        </gmd:onLine></tag>";

        $tagDistInfo = new \DOMDocument();
        $tagDistInfo->loadXML($strXmlDistributionInfo);

        //first delete all onlineResource links
        $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine");
        if ( $objXPath->length > 0 ) {
            foreach($objXPath as $item){
                $item->parentNode->removeChild($item);
            }
        }

        $transferOptions = $metadata_doc->getElementsByTagName("MD_DigitalTransferOptions");
        if ( $transferOptions && $transferOptions->length > 0 ) {
            $nodes = $tagDistInfo->getElementsByTagName("onLine");
            for($c = 0; $c < $nodes->length; $c ++) {
                $nodeToImport = $nodes->item($c);
                $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
                $transferOptions->item(0 )->appendChild($newNodeDesc);
            }
        }

        $new_metadata_data = $metadata_doc->saveXML();
        $new_metadata_data = str_replace($entete, "", $new_metadata_data);

        // Prodige4
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
        $urlUpdateData = "md.edit.save";
        $formData = array (
                "id" => $metadataId,
                "data" => $new_metadata_data 
        );
        // send to geosource
        $geonetwork->post($urlUpdateData, $formData);

        // publish metadata
        $jsonResp = $geonetwork->get('md.privileges.update?_content_type=json&_1_0=on&id=' . $metadataId);
        $jsonObj = json_decode($jsonResp, true);
        if ( !($jsonObj && $jsonObj [0]) ) {
            throw new \Exception("Impossible de publier la métadonnée WMS");
        }

        /*$response = $controller->forward('ProdigeProdigeBundle:UpdateDomSdom:delDomSdomKeywords', array(
            'fmeta_id'  => $metadataId
        ));*/
        
        $urlCatalogue = rtrim($kernel->getContainer()->getParameter('PRODIGE_URL_CATALOGUE'), "/");
        $delDomSdomKeywords = $urlCatalogue.'/prodige/delDomSdomKeywords/'.$metadataId;
        $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($delDomSdomKeywords);
        $jsonResp = $controller->curl($delDomSdomKeywords, 'GET', array('ticket'=>$ticket),  array());
  
        /*
        $response = $controller->forward('ProdigeProdigeBundle:UpdateDomSdom:addDomSdomKeywords', array(
            'fmeta_id'  => $metadataId,
            'thesaurus' => $thesaurus
        ));*/
        $addDomSdomKeywords = $urlCatalogue.'/prodige/addDomSdomKeywords/'.$metadataId."/".$thesaurus;
        $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($addDomSdomKeywords);
        $jsonResp = $controller->curl($addDomSdomKeywords, 'GET', array('ticket'=>$ticket),  array());


        /*$response = $controller->forward('ProdigeProdigeBundle:UpdateMetadata:updateDateValidity', array(
            'metadata_id'  => $metadataId
        ));*/
        $updateDateValidity = $urlCatalogue.'/prodige/updateDateValidity/'.$metadataId;
        $ticket = \Alk\Common\CasBundle\Security\Authentication\Provider\CasProvider::getPGT($updateDateValidity);
        $jsonResp = $controller->curl($updateDateValidity, 'GET', array('ticket'=>$ticket),  array());


        return array (
            $metadataId,
            $uuid 
        );
    }
}//function_exists


###########################################################################################

if ( !isset($domaine_pk) ) {
    return;
}
if ( !isset($domaine_id) ) {
    $query = "select SSDOM_ID from catalogue.sous_domaine where pk_sous_domaine=:domaine_pk and SSDOM_ID not in (select DOM_ID from catalogue.domaine)";
    $domaine_id = $conn->fetchOne($query, compact("domaine_pk"));
}


$labels = array();
$query =  "select RUBRIC_NAME, DOM_NOM, SSDOM_NOM "
        . " from catalogue.sous_domaine"
        . " join catalogue.domaine on (domaine.pk_domaine=sous_domaine.ssdom_fk_domaine)"
        . " join rubric_param on (domaine.dom_rubric=rubric_param.rubric_id)"
        . " where pk_sous_domaine=:domaine_pk";
$labels = $conn->fetchArray($query, compact("domaine_pk"));
if ( $labels===false ) {
    //end if the labels are not found
    return;
}
$alabels = $labels ?: array();
if ( $labels ) {
    $labels = implode(" / ", $labels);
} else {
    $labels = "";
}
$labels = html_entity_decode($labels);

$urlGeonetwork = PRO_GEONETWORK_URLBASE;
$primary_keys = array();
$query = "select RUBRIC_ID, PK_DOMAINE, PK_SOUS_DOMAINE "
        . " from catalogue.sous_domaine"
        . " join catalogue.domaine on (domaine.pk_domaine=sous_domaine.ssdom_fk_domaine)"
        . " join rubric_param on (domaine.dom_rubric=rubric_param.rubric_id)"
        . " where pk_sous_domaine=:domaine_pk";
$primary_keys = $conn->fetchArray($query, compact("domaine_pk")) ?: array();

$thesaurus = array(
    $urlGeonetwork . "/thesaurus/theme/domaine%23rubric_",
    $urlGeonetwork . "/thesaurus/theme/domaine%23domaine_",
    $urlGeonetwork . "/thesaurus/theme/domaine%23sousdomaine_",
);
$thesaurus = array_slice($thesaurus, 0, count($primary_keys));
$thesaurus = array_combine($thesaurus, $primary_keys);
$t = array();
foreach ($thesaurus as $url=>$pk){ $t = $url.$pk; }
$thesaurus = $t;

$CARMEN_PATH_CACHE = CARMEN_PATH_CACHE;
$CARMEN_URL_SERVER_DATA = CARMEN_URL_SERVER_DATA;
$PRODIGE_DEFAULT_EPSG = PRODIGE_DEFAULT_EPSG;
    

if ( isset($has_service_wms) ) {
    $partition = "wms_sdom";
    
    $domaine_id = preg_replace("!\W!", "_", $domaine_id);
    $rootUrlWebservice = CARMEN_URL_SERVER_DATA. "/".($partition)."/" . $domaine_id;
    $urlWebservice = $rootUrlWebservice."?service=WMS&amp;request=GetCapabilities";
    
    $labels .= " - Données WMS";
    $mapfile = PRO_MAPFILE_PATH . "/".$partition."_" .$domaine_id.".map";
    if ( $has_service_wms ) {
        if ( !file_exists($mapfile) ) {
            
            $handler = fopen($mapfile, "w");
            $data = 
<<<MAPFILE
MAP
  EXTENT -1870.33947299157 6100048.8679937 1120552.77850281 7222471.9859695
  FONTSET "./etc/fonts.txt"
  IMAGETYPE "png"
  MAXSIZE 10000
  NAME "{$labels}"
  SHAPEPATH "./"
  SIZE 400 400
  STATUS ON
  SYMBOLSET "./etc/symbols.sym"
  TRANSPARENT TRUE
  UNITS METERS

  OUTPUTFORMAT
    NAME "png"
    MIMETYPE "image/png"
    DRIVER "AGG/PNG"
    EXTENSION "png"
    IMAGEMODE RGBA
    TRANSPARENT TRUE
  END # OUTPUTFORMAT

  PROJECTION
    "init=epsg:{$PRODIGE_DEFAULT_EPSG}"
  END # PROJECTION
  LEGEND
    KEYSIZE 20 10
    KEYSPACING 5 5
    LABEL
      FONT "Arial"
      SIZE 10
      ENCODING "utf-8"
      OFFSET 0 0
      SHADOWSIZE 1 1
    END # LABEL
    STATUS OFF
  END # LEGEND

  QUERYMAP
    SIZE -1 -1
    STATUS OFF
    STYLE HILITE
  END # QUERYMAP

  SCALEBAR
    IMAGECOLOR 255 255 255
    INTERVALS 1
    LABEL
      SIZE 5
      OFFSET -3 0
      POSITION LR
      SHADOWSIZE 1 1
    END # LABEL
    OUTLINECOLOR 0 0 0
    SIZE 100 4
    STATUS OFF
    UNITS KILOMETERS
  END # SCALEBAR

  WEB
    IMAGEPATH "{$CARMEN_PATH_CACHE}"
    IMAGEURL "/mapimage/"
    LOG "log_itx.txt"
    MAXSCALEDENOM 1e+07
    METADATA
      "wms_srs"	"EPSG:{$PRODIGE_DEFAULT_EPSG}"
      "wms_onlineresource"	"{$rootUrlWebservice}"
      "wms_enable_request"	"*"
      "wms_title"	"{$labels}"
    END # METADATA
    MINSCALEDENOM 1
    VALIDATION
      "queryfile"	"[a-z]+"
    END # VALIDATION
  END # WEB
END # MAP
MAPFILE;
      
            fwrite($handler, $data);
            fclose($handler);
        } else {
            $map = ms_newMapObj($mapfile);
            if ( $map ) {
                $map->set('name', $labels);
                $map->setMetadata("wms_onlineresource", $rootUrlWebservice);
                $map->save($mapfile);
            }
        }
        list($ssdom_fmeta_id, $ssdom_fmeta_uuid) = createServiceWMSFiche($controller, $dao, $domaine_id, $ssdom_fmeta_uuid, $alabels, $thesaurus, $urlWebservice);
    } else  {
        if ( file_exists($mapfile) ) {
            @unlink($mapfile);
        }
        list($ssdom_fmeta_id, $ssdom_fmeta_uuid) = deleteServiceWMSFiche($controller, $dao, $urlWebservice, $ssdom_fmeta_uuid);
    }
    
    $conn->executeQuery('update sous_domaine set ssdom_service_wms_uuid=:ssdom_fmeta_uuid where pk_sous_domaine=:domaine_pk', compact("domaine_pk", "ssdom_fmeta_uuid"));
}

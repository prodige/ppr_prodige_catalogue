<?php
/**
 * Liste des sous-domaines (gauche) dans l'adminsitration des sous-domaines (administration des droits)
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/SousDomaineVO.php");
	require_once($AdminPath."/Components/SelectList/SelectList.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\SousDomaineVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;

  header("Content-type: application/xml");
  echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 

  $dao = new DAO($conn, 'catalogue');

	$sousDomaineVO = new SousDomaineVO(true);
	$sousDomaineVO->setDao($dao);
	
	$selectList = new SelectList(	 "SelectListSubDomaineVO",
									 $sousDomaineVO,
									 SousDomaineVO::$PK_SOUS_DOMAINE,
									 $sousDomaineVO->DOM_SSDOM_NOM,
									 Ressources::$IFRAME_NAME_MAIN,
									 '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/SubDomains/SubDomainsDetail.php",*/
									 Ressources::$SELECT_LIST_SIZE_X,
									 Ressources::$SELECT_LIST_SIZE_Y
									 );
?>
	</body>
</html>
<?php
/**
 * Onglet cartes dans la gestion des sous-domaines
 * @author Alkante
 */

    // TODO @vlc n'a pas été testé
	require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	
	require_once($AdminPath."/Modules/BO/SSDomVisualiseCarteVO.php");
	require_once($AdminPath."/Modules/BO/CarteProjetVO.php");
	
	require_once($AdminPath."/Components/RelationList/RelationList.php");
    
    use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 	
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_SUBDOMAINS_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}

	// set a default ID
	if ( !isset($_GET["RR"]) )
	{
		$RELATIONLIST_RESULT = -1;
	}
	else
	{
		$RELATIONLIST_RESULT = $_GET["RR"];	
	}

	
	if ( $RELATIONLIST_RESULT != -1 )
	{
		switch( $Action )
		{
			case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
				$ordinalList = array();
				$valueList = array();
				$ordinalList[]=SSDomVisualiseCarteVO::$SSDCART_FK_SOUS_DOMAINE;
				$ordinalList[]=SSDomVisualiseCarteVO::$SSDCART_FK_CARTE_PROJET;
				$valueList[]=$PK;
				$valueList[]=$RELATIONLIST_RESULT;

				$sSDomVisualiseCarteVO_RL = new SSDomVisualiseCarteVO();
				$sSDomVisualiseCarteVO_RL->InsertValues( $ordinalList, $valueList );
				$sSDomVisualiseCarteVO_RL->Commit();
 			break;
			
			case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
				$sSDomVisualiseCarteVO_RL = new SSDomVisualiseCarteVO();
				$sSDomVisualiseCarteVO_RL->DeleteRow( SSDomVisualiseCarteVO::$PK_SSDOM_VISUALISE_CARTE, $RELATIONLIST_RESULT );
				$sSDomVisualiseCarteVO_RL->Commit();
			break;
		}
        AlertSaveController::AlertSaveDone("", false);
	}
	
	// #################################################################

	$sSDomVisualiseCarteVO = new SSDomVisualiseCarteVO();
	
	$sSDomVisualiseCarteVO2 = new SSDomVisualiseCarteVO();
	$sSDomVisualiseCarteVO2->AddRestriction( SSDomVisualiseCarteVO::$SSDCART_FK_SOUS_DOMAINE, $PK );
	$sSDomVisualiseCarteVO2->AddOnlyKeyProjection( SSDomVisualiseCarteVO::$SSDCART_FK_CARTE_PROJET );

		
	$carteProjetVO = new CarteProjetVO();;
	$carteProjetVO->AddNotInRestriction( CarteProjetVO::$PK_CARTE_PROJET, $sSDomVisualiseCarteVO2 );
		
	$relationList = new RelationList( "RelationListSSdomMaps",
									 Ressources::$RELATIONLIST_SUBDOMAINS_MAPS_RELATION,
									 $sSDomVisualiseCarteVO,
									 SSDomVisualiseCarteVO::$PK_SSDOM_VISUALISE_CARTE,
									 SSDomVisualiseCarteVO::$SSDCART_CARTP_ID,
									 SSDomVisualiseCarteVO::$SSDCART_FK_SOUS_DOMAINE,
									 $PK,
									 SSDomVisualiseCarteVO::$PK_SSDOM_VISUALISE_CARTE,
									 $carteProjetVO,
									 Ressources::$RELATIONLIST_SUBDOMAINS_MAPS_TABLE,
									 CarteProjetVO::$PK_CARTE_PROJET,
									 CarteProjetVO::$CARTP_ID,
									 $AdminPath."/Administration/SubDomains/SubDomainsMaps.php",
									 Ressources::$IFRAME_NAME_MAIN,
	                                 $this
									 );
	
	
	// #################################################################
?>
	</body>
</html>
<?php
/**
 * Onglet domaines dans la gestion des sous-domaines
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Components/RelationList/RelationList.php");
	require_once($AdminPath."/Components/SelectList/SelectList.php");	
	require_once($AdminPath."/Modules/BO/DomaineVO.php");
	require_once($AdminPath."/Modules/BO/SousDomaineVO.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\DomaineVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\SousDomaineVO;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 		
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_SUBDOMAINS_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}

	// set a default ID
	if ( !isset($_GET["RR"]) )
	{
		$RELATIONLIST_RESULT = -1;
	}
	else
	{
		$RELATIONLIST_RESULT = $_GET["RR"];	
	}

	$dao = new DAO($conn, 'catalogue');

	if ( $RELATIONLIST_RESULT != -1 )
	{
		switch( $Action )
		{
			case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
				$ordinalList = array();
				$valueList = array();
				$ordinalList[]=SousDomaineVO::$SSDOM_FK_DOMAINE;
				$valueList[]=$RELATIONLIST_RESULT;
				$ordinalRestriction = SousDomaineVO::$PK_SOUS_DOMAINE;
				$valueRestriction = $PK;

				$sousDomaineVO_RL = new SousDomaineVO();
				$sousDomaineVO_RL->setDao($dao);
				$sousDomaineVO_RL->Update( $ordinalList, $valueList, $ordinalRestriction, $valueRestriction );
				$sousDomaineVO_RL->Commit();
 			break;
			
			case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
				$ordinalList = array();
				$valueList = array();
				$ordinalList[]=SousDomaineVO::$SSDOM_FK_DOMAINE;
				$valueList[]="NULL";
				$ordinalRestriction = SousDomaineVO::$PK_SOUS_DOMAINE;
				$valueRestriction = $PK;

				$sousDomaineVO_RL = new SousDomaineVO();
				$sousDomaineVO_RL->setDao($dao);
				$sousDomaineVO_RL->Update( $ordinalList, $valueList, $ordinalRestriction, $valueRestriction );
				$sousDomaineVO_RL->Commit();
			break;
		}
        AlertSaveController::AlertSaveDone("", false);
	}
	
	// #################################################################

	$sousDomaineVO  = new SousDomaineVO();
	$sousDomaineVO->setDao($dao);
	$sousDomaineVO->AddProjection( $sousDomaineVO->SSDOM_PK_DOMAINE, "DOMAINE", "PK_DOMAINE" );
	$sousDomaineVO->AddProjection( $sousDomaineVO->SSDOM_DOM_ID, "DOMAINE", "DOM_NOM" );
	$sousDomaineVO->AddEqualsRelation( SousDomaineVO::$SSDOM_FK_DOMAINE, $sousDomaineVO->SSDOM_PK_DOMAINE );

	$sousDomaineVO2 = new SousDomaineVO();
	$sousDomaineVO2->setDao($dao);
	$sousDomaineVO2->AddRestriction( SousDomaineVO::$PK_SOUS_DOMAINE, $PK );
	$sousDomaineVO2->AddOnlyKeyProjection( SousDomaineVO::$SSDOM_FK_DOMAINE );
	$sousDomaineVO2->EnableCOALESCE();
	
	// ALL OTHER SUBDOMAINS 
	$domaineVO3 = new DomaineVO();
	$domaineVO3->setDao($dao);
	$domaineVO3->AddNotInRestriction( DomaineVO::$PK_DOMAINE, $sousDomaineVO2 );
  
  
	$relationList = new RelationList( "RelationListDomainSSDOM",
									 Ressources::$RELATIONLIST_SUBDOMAINS_DOM_RELATION,
									 $sousDomaineVO,
									 SousDomaineVO::$SSDOM_FK_DOMAINE,
									 $sousDomaineVO->SSDOM_DOM_ID,
									 SousDomaineVO::$PK_SOUS_DOMAINE,
									 $PK,
									 SousDomaineVO::$SSDOM_FK_DOMAINE,
									 $domaineVO3,
									 Ressources::$RELATIONLIST_SUBDOMAINS_DOM_TABLE,
									 DomaineVO::$PK_DOMAINE,
									 DomaineVO::$DOM_NOM,
									 '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/SubDomains/SubDomainsDomains.php",*/
									 Ressources::$IFRAME_NAME_MAIN,
	                                 $this,
                   false, true, false
									 );
	
?>
	</body>
</html>
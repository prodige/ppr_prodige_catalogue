<?php
/**
 * Enregistrement des regroupements
 * @author Alkante
 * 
 */
unset($user);
//$AdminPath= realpath(dirname(__FILE__))."/../../";

//encodage des paramètres de formulaires passés en base de données
foreach($_REQUEST as $key => $value){
    $_REQUEST[$key] = htmlentities($value, ENT_QUOTES, "UTF-8");
}

//require_once(PRO_CATALOGUE_ROOT.'/include/ClassUser.php'); //TODO hismail

//require_once($AdminPath."/Ressources/Administration/Ressources.php");
//require_once($AdminPath."/DAO/DAO/DAO.php");  

use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Controller\User;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\SousDomaineVO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;
use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;
use Prodige\ProdigeBundle\Controller\UpdatedomSdomController;
use ProdigeCatalogue\AdminBundle\Services\ThesaurusSerializer;

$user = User::GetUser();
$userId = User::GetUserId();
/* @deprecated prodige4 *
if (! is_null($userId))
  User::SetUserCookie($userId);
  
if (is_null($user))
{
  header('location:connexion.php');
  exit(0);
}
if (!$user->PasswordIsValid())
{
  header('location:administration_utilisateur.php');
  exit(0);
}*/

$iMode   = $_GET["iMode"];

switch ($iMode){
  
  case "1"://groupes-profil

    $gridReload = Ressources::$ACTION_OPEN_PROFILES;
    $gridSelectedId = -1;
    $reloadForm = "groupes_profil";
    $bAutoClose = true;

    //initialisation
    $result_profil_name   = (isset($_POST["result_profil_name"]) ? $_POST["result_profil_name"] : '');
    $result_profil_id     = (isset($_POST["result_profil_id"])   ? $_POST["result_profil_id"]   : '');
    $result_profil_desc   = (isset($_POST["result_profil_desc"]) ? $_POST["result_profil_desc"] : '');
    $pk_groupProfil1      = (isset($_POST["pk_groupProfil1"])    ? $_POST["pk_groupProfil1"]    : '');
    $pk_groupProfil2      = (isset($_POST["pk_groupProfil2"])    ? $_POST["pk_groupProfil2"]    : '');
    $mode_droits          = (isset($_POST["mode_droits"])        ? $_POST["mode_droits"]        : '');
    $new_group_pk         = "";
    $grp_pk_is_default    = "";
    $alerte               = "";

    //$dao = new DAO();
    $dao = new DAO($conn, 'catalogue');

    if($dao) {
      if($result_profil_name != "" && $pk_groupProfil1 != "" && $pk_groupProfil2 != "" && $mode_droits != "") {

        //verify unicity of $result_profil_id and $result_profil_name
        //$query="SELECT grp_id, grp_nom from groupe_profil where grp_id='".$result_profil_id."' or grp_nom='".$result_profil_name."';";
        $query="SELECT grp_id, grp_nom from groupe_profil where grp_id=? or grp_nom=?";
        $rs_select=$dao->BuildResultSet($query, array($result_profil_id, $result_profil_name));
        if ($rs_select->GetNbRows() > 0){
          $alerte="Le regroupement a échoué :<br>Veuillez entrer un identifiant et un nom uniques. ";
        }
        else{
          //create new groupe-profil
          //$query = "INSERT INTO groupe_profil(grp_id, grp_nom, grp_description) VALUES ('".$result_profil_id."','".$result_profil_name."','".$result_profil_desc."');";
          $query = "INSERT INTO groupe_profil(grp_id, grp_nom, grp_description) VALUES (?, ?, ?);";
          $dao->Execute($query, array($result_profil_id, $result_profil_name, $result_profil_desc));
          $query="COMMIT";
          $dao->Execute($query);


          //select new groupe_profil id
          //$query="SELECT pk_groupe_profil from groupe_profil where grp_id='".$result_profil_id."'";
          $query="SELECT pk_groupe_profil from groupe_profil where grp_id=?";
          $rs_select=$dao->BuildResultSet($query, array($result_profil_id));
          for ($rs_select->First(); !$rs_select->EOF();$rs_select->Next()){
            $gridSelectedId = $new_group_pk=$rs_select->Read(0);
          }
          //see if grp_is_default_installation
          $query="SELECT pk_groupe_profil,grp_is_default_installation from groupe_profil where grp_is_default_installation=1";
          $rs_select=$dao->BuildResultSet($query);
          for ($rs_select->First(); !$rs_select->EOF();$rs_select->Next()){
            $grp_pk_is_default=$rs_select->Read(0);
          }
            
          //select all sdom admin by profils
          $query = "select pk_sous_domaine from sous_domaine where ssdom_admin_fk_groupe_profil in (:pk_groupProfil1, :pk_groupProfil2)";
          $rs_select = $dao->BuildResultSet($query, array("pk_groupProfil1" => $pk_groupProfil1, "pk_groupProfil2" => $pk_groupProfil2));
          $tabSdom = array();
          for ($rs_select->First(); !$rs_select->EOF();$rs_select->Next()){
            $tabSdom[] = $rs_select->Read(0);
          }
                                   
          
          //select all sdom user from profils
          $query = "select grpusr_fk_utilisateur from grp_regroupe_usr where grpusr_fk_groupe_profil in (:pk_groupProfil1, :pk_groupProfil2)";
          $rs_select = $dao->BuildResultSet($query, array("pk_groupProfil1" => $pk_groupProfil1, "pk_groupProfil2" => $pk_groupProfil2));
          $tabUsers = array();
          for ($rs_select->First(); !$rs_select->EOF();$rs_select->Next()){
              $tabUsers[] = $rs_select->Read(0);
          }
          
          //update depending tables
          switch($mode_droits){

            case "droits_profil1":

              $query = array();
              $params = array();

              /*$query="UPDATE grp_accede_dom set grpdom_fk_groupe_profil=".$new_group_pk.
                     " where grpdom_fk_groupe_profil=".$pk_groupProfil1.";";*/
              $query[] = "UPDATE grp_accede_dom set grpdom_fk_groupe_profil=:new_grpdom_fk_groupe_profil".
                         " where grpdom_fk_groupe_profil=:grpdom_fk_groupe_profil;";
              $params[] = array(
                            "new_grpdom_fk_groupe_profil"=>$new_group_pk,
                            "grpdom_fk_groupe_profil"=>$pk_groupProfil1
              );

              /*$query.="UPDATE grp_accede_ssdom set grpss_fk_groupe_profil=".$new_group_pk.
                     " where grpss_fk_groupe_profil=".$pk_groupProfil1.";";*/
              $query[] = "UPDATE grp_accede_ssdom set grpss_fk_groupe_profil=:new_grpss_fk_groupe_profil".
                         " where grpss_fk_groupe_profil=:grpss_fk_groupe_profil;";
              $params[] = array(
                  "new_grpss_fk_groupe_profil"=>$new_group_pk,
                  "grpss_fk_groupe_profil"=>$pk_groupProfil1
              );

              /*$query.="UPDATE grp_accede_competence set fk_grp_id=".$new_group_pk.
                     " where fk_grp_id=".$pk_groupProfil1.";";*/
              $query[] = "UPDATE grp_accede_competence set fk_grp_id=:new_fk_grp_id".
                         " where fk_grp_id=:fk_grp_id;";
              $params[] = array(
                  "new_fk_grp_id"=>$new_group_pk,
                  "fk_grp_id"=>$pk_groupProfil1
              );

              /*$query.="UPDATE grp_accede_perimetre set  grpperim_fk_groupe_profil=".$new_group_pk.
                     " where  grpperim_fk_groupe_profil=".$pk_groupProfil1.";";
              */

              /*$query.="UPDATE grp_autorise_trt set grptrt_fk_groupe_profil=".$new_group_pk.
                     " where grptrt_fk_groupe_profil=".$pk_groupProfil1.";";*/
              $query[] = "UPDATE grp_autorise_trt set grptrt_fk_groupe_profil=:new_grptrt_fk_groupe_profil".
                         " where grptrt_fk_groupe_profil=:grptrt_fk_groupe_profil;";
              $params[] = array(
                  "new_grptrt_fk_groupe_profil"=>$new_group_pk,
                  "grptrt_fk_groupe_profil"=>$pk_groupProfil1
              );

              /*$query.="UPDATE grp_regroupe_usr set grpusr_fk_groupe_profil=".$new_group_pk.
                     " where grpusr_fk_groupe_profil=".$pk_groupProfil1.";";*/
              $query[] = "UPDATE grp_regroupe_usr set grpusr_fk_groupe_profil=:new_grpusr_fk_groupe_profil".
                         " where grpusr_fk_groupe_profil=:grpusr_fk_groupe_profil;";
              $params[] = array(
                  "new_grpusr_fk_groupe_profil"=>$new_group_pk,
                  "grpusr_fk_groupe_profil"=>$pk_groupProfil1
              );

              /*$query.="UPDATE grp_trt_objet set grptrtobj_fk_grp_id=".$new_group_pk.
                     " where grptrtobj_fk_grp_id=".$pk_groupProfil1.";";*/
              $query[] ="UPDATE grp_trt_objet set grptrtobj_fk_grp_id=:new_grptrtobj_fk_grp_id".
                    " where grptrtobj_fk_grp_id=:grptrtobj_fk_grp_id;";
              $params[] = array(
                  "new_grptrtobj_fk_grp_id"=>$new_group_pk,
                  "grptrtobj_fk_grp_id"=>$pk_groupProfil1
              );

              /*$query.="UPDATE sous_domaine set ssdom_admin_fk_groupe_profil=".$new_group_pk.
                     " where ssdom_admin_fk_groupe_profil=".$pk_groupProfil1.";";*/
              $query[] = "UPDATE sous_domaine set ssdom_admin_fk_groupe_profil=:new_ssdom_admin_fk_groupe_profil".
                         " where ssdom_admin_fk_groupe_profil=:ssdom_admin_fk_groupe_profil;";
              $params[] = array(
                  "new_ssdom_admin_fk_groupe_profil"=>$new_group_pk,
                  "ssdom_admin_fk_groupe_profil"=>$pk_groupProfil1
              );

              /*$query.="UPDATE sous_domaine set ssdom_createur_fk_groupe_profil=".$new_group_pk.
                     " where ssdom_createur_fk_groupe_profil=".$pk_groupProfil1.";";*/
              $query[] = "UPDATE sous_domaine set ssdom_createur_fk_groupe_profil=:new_ssdom_createur_fk_groupe_profil".
                         " where ssdom_createur_fk_groupe_profil=:ssdom_createur_fk_groupe_profil";
              $params[] = array(
                  "new_ssdom_createur_fk_groupe_profil"=>$new_group_pk,
                  "ssdom_createur_fk_groupe_profil"=>$pk_groupProfil1
              );
              /*
              if ($grp_pk_is_default==$pk_groupProfil1){
                $query.="UPDATE groupe_profil set grp_is_default_installation=1 where pk_groupe_profil=".$new_group_pk.";";
              }
              */
              foreach($query as $i=>$a_query) {
                  $dao->Execute($a_query, $params[$i]);
              }
              $query="COMMIT";
              $dao->Execute($query);

              break;

            case "droits_profil2":

              $query = array();
              $params = array();

              /*$query="UPDATE grp_accede_dom set grpdom_fk_groupe_profil=".$new_group_pk.
               " where grpdom_fk_groupe_profil=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE grp_accede_dom set grpdom_fk_groupe_profil=:new_grpdom_fk_groupe_profil".
                         " where grpdom_fk_groupe_profil=:grpdom_fk_groupe_profil;";
              $params[] = array(
                  "new_grpdom_fk_groupe_profil"=>$new_group_pk,
                  "grpdom_fk_groupe_profil"=>$pk_groupProfil2
              );
              /*$query.="UPDATE grp_accede_ssdom set grpss_fk_groupe_profil=".$new_group_pk.
               " where grpss_fk_groupe_profil=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE grp_accede_ssdom set grpss_fk_groupe_profil=:new_grpss_fk_groupe_profil".
                         " where grpss_fk_groupe_profil=:grpss_fk_groupe_profil;";
              $params[] = array(
                  "new_grpss_fk_groupe_profil"=>$new_group_pk,
                  "grpss_fk_groupe_profil"=>$pk_groupProfil2
              );
              /*$query.="UPDATE grp_accede_competence set fk_grp_id=".$new_group_pk.
               " where fk_grp_id=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE grp_accede_competence set fk_grp_id=:new_fk_grp_id".
                         " where fk_grp_id=:fk_grp_id;";
              $params[] = array(
                  "new_fk_grp_id"=>$new_group_pk,
                  "fk_grp_id"=>$pk_groupProfil2
              );
              /*$query.="UPDATE grp_accede_perimetre set  grpperim_fk_groupe_profil=".$new_group_pk.
               " where  grpperim_fk_groupe_profil=".$pk_groupProfil2.";";
               */

              /*$query.="UPDATE grp_autorise_trt set grptrt_fk_groupe_profil=".$new_group_pk.
               " where grptrt_fk_groupe_profil=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE grp_autorise_trt set grptrt_fk_groupe_profil=:new_grptrt_fk_groupe_profil".
                         " where grptrt_fk_groupe_profil=:grptrt_fk_groupe_profil;";
              $params[] = array(
                  "new_grptrt_fk_groupe_profil"=>$new_group_pk,
                  "grptrt_fk_groupe_profil"=>$pk_groupProfil2
              );
              /*$query.="UPDATE grp_regroupe_usr set grpusr_fk_groupe_profil=".$new_group_pk.
               " where grpusr_fk_groupe_profil=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE grp_regroupe_usr set grpusr_fk_groupe_profil=:new_grpusr_fk_groupe_profil".
                         " where grpusr_fk_groupe_profil=:grpusr_fk_groupe_profil;";
              $params[] = array(
                  "new_grpusr_fk_groupe_profil"=>$new_group_pk,
                  "grpusr_fk_groupe_profil"=>$pk_groupProfil2
              );
              /*$query.="UPDATE grp_trt_objet set grptrtobj_fk_grp_id=".$new_group_pk.
               " where grptrtobj_fk_grp_id=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE grp_trt_objet set grptrtobj_fk_grp_id=:new_grptrtobj_fk_grp_id".
                         " where grptrtobj_fk_grp_id=:grptrtobj_fk_grp_id;";
              $params[] = array(
                  "new_grptrtobj_fk_grp_id"=>$new_group_pk,
                  "grptrtobj_fk_grp_id"=>$pk_groupProfil2
              );
              /*$query.="UPDATE sous_domaine set ssdom_admin_fk_groupe_profil=".$new_group_pk.
               " where ssdom_admin_fk_groupe_profil=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE sous_domaine set ssdom_admin_fk_groupe_profil=:new_ssdom_admin_fk_groupe_profil".
                         " where ssdom_admin_fk_groupe_profil=:ssdom_admin_fk_groupe_profil;";
              $params[] = array(
                  "new_ssdom_admin_fk_groupe_profil"=>$new_group_pk,
                  "ssdom_admin_fk_groupe_profil"=>$pk_groupProfil2
              );
              /*$query.="UPDATE sous_domaine set ssdom_createur_fk_groupe_profil=".$new_group_pk.
               " where ssdom_createur_fk_groupe_profil=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE sous_domaine set ssdom_createur_fk_groupe_profil=:new_ssdom_createur_fk_groupe_profil".
                         " where ssdom_createur_fk_groupe_profil=:ssdom_createur_fk_groupe_profil";
              $params[] = array(
                  "new_ssdom_createur_fk_groupe_profil"=>$new_group_pk,
                  "ssdom_createur_fk_groupe_profil"=>$pk_groupProfil2
              );
              /*
              if ($grp_pk_is_default==$pk_groupProfil2){
                $query.="UPDATE groupe_profil set grp_is_default_installation=1 where pk_groupe_profil=".$new_group_pk.";";
              }
              */
              foreach($query as $i=>$a_query) {
                  $dao->Execute($a_query, $params[$i]);
              }
              $query="COMMIT";
              $dao->Execute($query);
              break;

            case "droits_profil12":
              /*$query="UPDATE grp_accede_dom set grpdom_fk_groupe_profil=".$new_group_pk.
                     " where grpdom_fk_groupe_profil=".$pk_groupProfil1;*/
              $query="UPDATE grp_accede_dom set grpdom_fk_groupe_profil=:new_grpdom_fk_groupe_profil".
                    " where grpdom_fk_groupe_profil=:grpdom_fk_groupe_profil";
              $dao->Execute($query, array(
                                          "new_grpdom_fk_groupe_profil"=>$new_group_pk,
                                          "grpdom_fk_groupe_profil"=>$pk_groupProfil1,
                                          )
                  );
              
              
              $query="UPDATE grp_accede_ssdom set grpss_fk_groupe_profil=:new_grpdom_fk_groupe_profil".
                  " where grpss_fk_groupe_profil=:grpdom_fk_groupe_profil";
              $dao->Execute($query, array(
                  "new_grpdom_fk_groupe_profil"=>$new_group_pk,
                  "grpdom_fk_groupe_profil"=>$pk_groupProfil1,
              )
              );
              

              //probleme de contrainte unique (grp_dom_fk_domaine, grp_dom_fk_grpprofil)
              //on efface les enregistrements qui ont le meme grp_dom_fk_domaine que ceux qui viennent d'etre mis
              //a jour et qui ont la grp_dom_fk_grpprofil qui doit etre mise à jour cad $pk_groupProfil2
              /*$query="delete from grp_accede_dom where grpdom_fk_domaine in (select grpdom_fk_domaine from grp_accede_dom where grpdom_fk_groupe_profil=".
                     $new_group_pk.") and ".
                     " grpdom_fk_groupe_profil=".$pk_groupProfil2;*/
              $query="delete from grp_accede_dom where grpdom_fk_domaine in (select grpdom_fk_domaine from grp_accede_dom where grpdom_fk_groupe_profil=:new_grpdom_fk_groupe_profil".
                  ") and ".
                  " grpdom_fk_groupe_profil=:grpdom_fk_groupe_profil";
              $dao->Execute($query, array(
                                          "new_grpdom_fk_groupe_profil"=>$new_group_pk,
                                          "grpdom_fk_groupe_profil"=>$pk_groupProfil2,
                                         )
                  );
              
              

              $query="delete from grp_accede_ssdom where grpss_fk_sous_domaine in (select grpss_fk_sous_domaine from grp_accede_ssdom where grpss_fk_groupe_profil=:new_grpdom_fk_groupe_profil".
                  ") and ".
                  " grpss_fk_groupe_profil=:grpdom_fk_groupe_profil";
              $dao->Execute($query, array(
                  "new_grpdom_fk_groupe_profil"=>$new_group_pk,
                  "grpdom_fk_groupe_profil"=>$pk_groupProfil2,
              )
              );
              
              
              $query = array();
              $params = array();

              /*$query="UPDATE grp_accede_dom set grpdom_fk_groupe_profil=".$new_group_pk.
                     " where grpdom_fk_groupe_profil=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE grp_accede_dom set grpdom_fk_groupe_profil=:new_grpdom_fk_groupe_profil".
                         " where grpdom_fk_groupe_profil=:grpdom_fk_groupe_profil;";
              $params[] = array(
                  "new_grpdom_fk_groupe_profil"=>$new_group_pk,
                  "grpdom_fk_groupe_profil"=>$pk_groupProfil2
              );
              
              /*$query.="UPDATE grp_accede_ssdom set grpss_fk_groupe_profil=".$new_group_pk.
                     " where grpss_fk_groupe_profil=".$pk_groupProfil1." or grpss_fk_groupe_profil=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE grp_accede_ssdom set grpss_fk_groupe_profil=:new_grpss_fk_groupe_profil".
                         " where grpss_fk_groupe_profil=:grpss_fk_groupe_profil1 or grpss_fk_groupe_profil=:grpss_fk_groupe_profil2;";
              $params[] = array(
                  "new_grpss_fk_groupe_profil"=>$new_group_pk,
                  "grpss_fk_groupe_profil1"=>$pk_groupProfil1,
                  "grpss_fk_groupe_profil2"=>$pk_groupProfil2
              );

              /*$query.="UPDATE grp_accede_competence set fk_grp_id=".$new_group_pk.
                     " where fk_grp_id=".$pk_groupProfil1." or fk_grp_id=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE grp_accede_competence set fk_grp_id=:new_fk_grp_id".
                         " where fk_grp_id=:fk_grp_id1 or fk_grp_id=:fk_grp_id2;";
              $params[] = array(
                  "new_fk_grp_id"=>$new_group_pk,
                  "fk_grp_id1"=>$pk_groupProfil1,
                  "fk_grp_id2"=>$pk_groupProfil2
              );

             
              foreach($query as $i=>$a_query) {
                  $dao->Execute($a_query, $params[$i]);
              }

              
              $query = array();
              $params = array();
              /*
              $query.="UPDATE grp_accede_perimetre set grpperim_fk_groupe_profil=".$new_group_pk.
                     " where grpperim_fk_groupe_profil=".$pk_groupProfil1." or grpperim_fk_groupe_profil=".$pk_groupProfil2.";";
              */
              /*$query.="UPDATE grp_autorise_trt set grptrt_fk_groupe_profil=".$new_group_pk.
                     " where grptrt_fk_groupe_profil=".$pk_groupProfil1." or grptrt_fk_groupe_profil=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE grp_autorise_trt set grptrt_fk_groupe_profil=:new_grptrt_fk_groupe_profil".
                         " where grptrt_fk_groupe_profil in(:grptrt_fk_groupe_profil, :grptrt_fk_groupe_profil2);";
              $params[] = array(
                  "new_grptrt_fk_groupe_profil"=>$new_group_pk,
                  "grptrt_fk_groupe_profil"=>$pk_groupProfil1,
                  "grptrt_fk_groupe_profil2"=>$pk_groupProfil2
              );
              foreach($query as $i=>$a_query) {
                  $dao->Execute($a_query, $params[$i]);
              }
              
              
              $query = array();
              $params = array();

              /*$query.="UPDATE grp_regroupe_usr set grpusr_fk_groupe_profil=".$new_group_pk.
                     " where grpusr_fk_groupe_profil=".$pk_groupProfil1.";";*/
              $query[] = "UPDATE grp_regroupe_usr set grpusr_fk_groupe_profil=:new_grpusr_fk_groupe_profil".
                         " where grpusr_fk_groupe_profil=:grpusr_fk_groupe_profil;";
              $params[] = array(
                  "new_grpusr_fk_groupe_profil"=>$new_group_pk,
                  "grpusr_fk_groupe_profil"=>$pk_groupProfil1
              );

              //probleme de contrainte unique (grpusr_fk_groupe_profil, grpusr_fk_utilisateur)
              //on efface les enregistrements qui ont le meme grp_dom_fk_domaine que ceux qui viennent d'etre mis
              //a jour et qui ont la grp_dom_fk_grpprofil qui doit etre mise à jour cad $pk_groupProfil2
              /*$query.="delete from grp_regroupe_usr where grpusr_fk_utilisateur in (select grpusr_fk_utilisateur from grp_regroupe_usr where grpusr_fk_groupe_profil=".
                     $new_group_pk.") and ".
                     " grpusr_fk_groupe_profil=".$pk_groupProfil2;*/
              $query[] = "delete from grp_regroupe_usr where grpusr_fk_utilisateur in (select grpusr_fk_utilisateur from grp_regroupe_usr where grpusr_fk_groupe_profil=:new_grpusr_fk_groupe_profil".
                         ") and ".
                         " grpusr_fk_groupe_profil=:grpusr_fk_groupe_profil";
              $params[] = array(
                  "new_grpusr_fk_groupe_profil"=>$new_group_pk,
                  "grpusr_fk_groupe_profil"=>$pk_groupProfil2
              );

              foreach($query as $i=>$a_query) {
                  $dao->Execute($a_query, $params[$i]);
              }


              $query = array();
              $params = array();

              /*$query="UPDATE grp_regroupe_usr set grpusr_fk_groupe_profil=".$new_group_pk.
                     " where grpusr_fk_groupe_profil=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE grp_regroupe_usr set grpusr_fk_groupe_profil=:new_grpusr_fk_groupe_profil".
                         " where grpusr_fk_groupe_profil=:grpusr_fk_groupe_profil;";
              $params[] = array(
                  "new_grpusr_fk_groupe_profil"=>$new_group_pk,
                  "grpusr_fk_groupe_profil"=>$pk_groupProfil2
              );
              /*$query.="UPDATE grp_trt_objet set grptrtobj_fk_grp_id=".$new_group_pk.
                     " where grptrtobj_fk_grp_id=".$pk_groupProfil1." or grptrtobj_fk_grp_id=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE grp_trt_objet set grptrtobj_fk_grp_id=:new_grptrtobj_fk_grp_id".
                         " where grptrtobj_fk_grp_id=:grptrtobj_fk_grp_id1 or grptrtobj_fk_grp_id=:grptrtobj_fk_grp_id2;";
              $params[] = array(
                  "new_grptrtobj_fk_grp_id"=>$new_group_pk,
                  "grptrtobj_fk_grp_id1"=>$pk_groupProfil1,
                  "grptrtobj_fk_grp_id2"=>$pk_groupProfil2
              );
              /*$query.="UPDATE sous_domaine set ssdom_admin_fk_groupe_profil=".$new_group_pk.
                     " where ssdom_admin_fk_groupe_profil=".$pk_groupProfil1." or ssdom_admin_fk_groupe_profil=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE sous_domaine set ssdom_admin_fk_groupe_profil=:new_ssdom_admin_fk_groupe_profil".
                         " where ssdom_admin_fk_groupe_profil=:ssdom_admin_fk_groupe_profil1 or ssdom_admin_fk_groupe_profil=:ssdom_admin_fk_groupe_profil2;";
              $params[] = array(
                  "new_ssdom_admin_fk_groupe_profil"=>$new_group_pk,
                  "ssdom_admin_fk_groupe_profil1"=>$pk_groupProfil1,
                  "ssdom_admin_fk_groupe_profil2"=>$pk_groupProfil2
              );
              /*$query.="UPDATE sous_domaine set ssdom_createur_fk_groupe_profil=".$new_group_pk.
                     " where ssdom_createur_fk_groupe_profil=".$pk_groupProfil1." or ssdom_createur_fk_groupe_profil=".$pk_groupProfil2.";";*/
              $query[] = "UPDATE sous_domaine set ssdom_createur_fk_groupe_profil=:new_ssdom_createur_fk_groupe_profil".
                         " where ssdom_createur_fk_groupe_profil=:ssdom_createur_fk_groupe_profil1 or ssdom_createur_fk_groupe_profil=:ssdom_createur_fk_groupe_profil2;";
              $params[] = array(
                  "new_ssdom_createur_fk_groupe_profil"=>$new_group_pk,
                  "ssdom_createur_fk_groupe_profil1"=>$pk_groupProfil1,
                  "ssdom_createur_fk_groupe_profil2"=>$pk_groupProfil2
              );
              /*if ($grp_pk_is_default==$pk_groupProfil2 || $grp_pk_is_default==$pk_groupProfil1 ){
                $query.="UPDATE groupe_profil set grp_is_default_installation=1 where pk_groupe_profil=".$new_group_pk.";";
              }
              */


              foreach($query as $i=>$a_query) {
                  $dao->Execute($a_query, $params[$i]);
              }

              
              break;

            case "aucun":

              break;
          }

          $query = array();
          $params = array();

          /*$query="UPDATE sous_domaine set ssdom_admin_fk_groupe_profil=".$new_group_pk.
                 " where ssdom_admin_fk_groupe_profil=".$pk_groupProfil1." or ssdom_admin_fk_groupe_profil=".$pk_groupProfil2.";";*/
          $query[] = "UPDATE sous_domaine set ssdom_admin_fk_groupe_profil=:new_ssdom_admin_fk_groupe_profil".
                     " where ssdom_admin_fk_groupe_profil=:ssdom_admin_fk_groupe_profil1 or ssdom_admin_fk_groupe_profil=:ssdom_admin_fk_groupe_profil2;";
          $params[] = array(
              "new_ssdom_admin_fk_groupe_profil"=>$new_group_pk,
              "ssdom_admin_fk_groupe_profil1"=>$pk_groupProfil1,
              "ssdom_admin_fk_groupe_profil2"=>$pk_groupProfil2
          );
          /*$query.="UPDATE sous_domaine set ssdom_createur_fk_groupe_profil=".$new_group_pk.
                 " where ssdom_createur_fk_groupe_profil=".$pk_groupProfil1." or ssdom_createur_fk_groupe_profil=".$pk_groupProfil2.";";*/
          $query[] = "UPDATE sous_domaine set ssdom_createur_fk_groupe_profil=:new_ssdom_createur_fk_groupe_profil".
                     " where ssdom_createur_fk_groupe_profil=:ssdom_createur_fk_groupe_profil1 or ssdom_createur_fk_groupe_profil=:ssdom_createur_fk_groupe_profil2;";
          $params[] = array(
              "new_ssdom_createur_fk_groupe_profil"=>$new_group_pk,
              "ssdom_createur_fk_groupe_profil1"=>$pk_groupProfil1,
              "ssdom_createur_fk_groupe_profil2"=>$pk_groupProfil2
          );

          foreach($query as $i=>$a_query) {
              $dao->Execute($query[$i], $params[$i]);
          }

          $query="COMMIT";
          $dao->Execute($query);

          $query = array();
          $params = array();

          //delete old groupes-profil fk from everywhere
          //$query="DELETE from grp_accede_dom where grpdom_fk_groupe_profil=".$pk_groupProfil1." or grpdom_fk_groupe_profil=".$pk_groupProfil2.";";
          $query[] = "DELETE from grp_accede_dom where grpdom_fk_groupe_profil=:grpdom_fk_groupe_profil1 or grpdom_fk_groupe_profil=:grpdom_fk_groupe_profil2;"; 
          $params[] = array(
              "grpdom_fk_groupe_profil1"=>$pk_groupProfil1,
              "grpdom_fk_groupe_profil2"=>$pk_groupProfil2
          );
          //$query.="DELETE from grp_accede_ssdom where grpss_fk_groupe_profil=".$pk_groupProfil1." or grpss_fk_groupe_profil=".$pk_groupProfil2.";";
          $query[] = "DELETE from grp_accede_ssdom where grpss_fk_groupe_profil=:grpss_fk_groupe_profil1 or grpss_fk_groupe_profil=:grpss_fk_groupe_profil2;";
          $params[] = array(
              "grpss_fk_groupe_profil1"=>$pk_groupProfil1,
              "grpss_fk_groupe_profil2"=>$pk_groupProfil2
          );
          //$query.="DELETE from grp_accede_competence where fk_grp_id=".$pk_groupProfil1." or fk_grp_id=".$pk_groupProfil2.";";
          $query[] = "DELETE from grp_accede_competence where fk_grp_id=:fk_grp_id1 or fk_grp_id=:fk_grp_id2;";
          $params[] = array(
              "fk_grp_id1"=>$pk_groupProfil1,
              "fk_grp_id2"=>$pk_groupProfil2
          );
          //$query.="DELETE from grp_accede_perimetre where grpperim_fk_groupe_profil=".$pk_groupProfil1." or grpperim_fk_groupe_profil=".$pk_groupProfil2.";";  
          //$query.="DELETE from grp_autorise_trt where grptrt_fk_groupe_profil=".$pk_groupProfil1." or grptrt_fk_groupe_profil=".$pk_groupProfil2.";";  
          $query[] = "DELETE from grp_autorise_trt where grptrt_fk_groupe_profil=:grptrt_fk_groupe_profil1 or grptrt_fk_groupe_profil=:grptrt_fk_groupe_profil2;";
          $params[] = array(
              "grptrt_fk_groupe_profil1"=>$pk_groupProfil1,
              "grptrt_fk_groupe_profil2"=>$pk_groupProfil2
          );
          //$query.="DELETE from grp_regroupe_usr where grpusr_fk_groupe_profil=".$pk_groupProfil1." or grpusr_fk_groupe_profil=".$pk_groupProfil2.";";
          $query[] = "DELETE from grp_regroupe_usr where grpusr_fk_groupe_profil=:grpusr_fk_groupe_profil1 or grpusr_fk_groupe_profil=:grpusr_fk_groupe_profil2;";
          $params[] = array(
              "grpusr_fk_groupe_profil1"=>$pk_groupProfil1,
              "grpusr_fk_groupe_profil2"=>$pk_groupProfil2
          );
          //$query.="DELETE from grp_trt_objet where grptrtobj_fk_grp_id=".$pk_groupProfil1." or grptrtobj_fk_grp_id=".$pk_groupProfil2;
          $query[] = "DELETE from grp_trt_objet where grptrtobj_fk_grp_id=:grptrtobj_fk_grp_id1 or grptrtobj_fk_grp_id=:grptrtobj_fk_grp_id2";
          $params[] = array(
              "grptrtobj_fk_grp_id1"=>$pk_groupProfil1,
              "grptrtobj_fk_grp_id2"=>$pk_groupProfil2
          );

          foreach($query as $i=>$a_query) {
              $dao->Execute($a_query, $params[$i]);
          }

          $query="COMMIT";
          $dao->Execute($query);

          //delete old groupes-profil
          //$query="DELETE from groupe_profil where pk_groupe_profil=".$pk_groupProfil1." or pk_groupe_profil=".$pk_groupProfil2;
          $query="DELETE from groupe_profil where pk_groupe_profil=:pk_groupe_profil1 or pk_groupe_profil=:pk_groupe_profil2";
          $dao->Execute($query, array(
                                      "pk_groupe_profil1"=>$pk_groupProfil1,
                                      "pk_groupe_profil2"=>$pk_groupProfil2
                                )
              );
          $query="COMMIT";
          $dao->Execute($query);
          
          
          //mise à jour LDAP des groupes concernés
          $sousDomaineVO = new SousDomaineVO();
          $sousDomaineVO->setDao($dao);
          
          foreach($tabSdom as $key => $id){
            $sousDomaineVO->ldapSyncAction($id);
          }
          //mise à jour LDAP des user concernés
          $utilisateurVO = new UtilisateurVO();
          $utilisateurVO->setDao($dao);
          
          foreach($tabUsers as $key => $id){
              $utilisateurVO->ldapSyncAction($id);
          }
        }
      }

    }
    else{
      $alerte = 'Erreur de connexion à la base de données.';
    }

    break;

  /********************************************/
  case "2"://sous-domaines

    $gridReload = Ressources::$ACTION_OPEN_SUBDOMAINS;
    $gridSelectedId = -1;
    $reloadForm = "regrpt_ssdom";
    $bAutoClose = false;

    //initialisation
    $result_ssdom_name   = (isset($_POST["result_ssdom_name"]) ? $_POST["result_ssdom_name"] : "");
    $result_ssdom_id     = (isset($_POST["result_ssdom_id"])   ? $_POST["result_ssdom_id"]   : "");
    $result_ssdom_desc   = (isset($_POST["result_ssdom_desc"]) ? $_POST["result_ssdom_desc"] : "");
    $pk_dom              = (isset($_POST["pk_dom"])            ? $_POST["pk_dom"]            : "");
    $pk_ssdom1           = (isset($_POST["pk_ssdom1"])         ? $_POST["pk_ssdom1"]         : "");
    $pk_ssdom2           = (isset($_POST["pk_ssdom2"])         ? $_POST["pk_ssdom2"]         : "");
    $pk_groupProfil      = (isset($_POST["pk_groupProfil"])    ? $_POST["pk_groupProfil"]    : "");
    $new_ssdom_pk        = "";
    $alerte              = "";
    $ssdom1_name         = "";
    $ssdom2_name         = "";
    $new_dom_name        = "";
    $old_pkdom_ssdom1    = "";
    $old_pkdom_ssdom2    = "";
    $old_dom_name_ssdom1 = "";
    $old_dom_name_ssdom2 = "";

    //$dao = new DAO();
    $dao = new DAO($conn, 'catalogue');

    if($dao) {
      if($result_ssdom_name != "" && $pk_ssdom1 != "" && $pk_ssdom2 != "" && $pk_dom != "" && $pk_groupProfil != "") {

        //select old sous-domaine 1 name
        //$query="SELECT ssdom_nom, ssdom_fk_domaine from sous_domaine where pk_sous_domaine='".$pk_ssdom1."'";
        $query="SELECT ssdom_nom, ssdom_fk_domaine from sous_domaine where pk_sous_domaine=?";
        $rs_select=$dao->BuildResultSet($query, array($pk_ssdom1));
        for ($rs_select->First(); !$rs_select->EOF();$rs_select->Next()){
          $ssdom1_name=$rs_select->Read(0);
          $old_pkdom_ssdom1=$rs_select->Read(1);
        }
        //select old sous-domaine 2 name
        //$query="SELECT ssdom_nom, ssdom_fk_domaine from sous_domaine where pk_sous_domaine='".$pk_ssdom2."'";
        $query="SELECT ssdom_nom, ssdom_fk_domaine from sous_domaine where pk_sous_domaine=?";
        $rs_select=$dao->BuildResultSet($query, array($pk_ssdom2));
        for ($rs_select->First(); !$rs_select->EOF();$rs_select->Next()){
          $ssdom2_name=$rs_select->Read(0);
          $old_pkdom_ssdom2=$rs_select->Read(1);
        }

       
        //verify unicity of $result_profil_id and $result_profil_name
        //$query="SELECT ssdom_id, ssdom_nom from sous_domaine where ssdom_id='".$result_ssdom_id."' or ssdom_nom='".$result_ssdom_name."';";
        $query="SELECT ssdom_id, ssdom_nom from sous_domaine where ssdom_id=? or ssdom_nom=?;";
        $rs_select=$dao->BuildResultSet($query, array($result_ssdom_id, $result_ssdom_name));
        if ($rs_select->GetNbRows() > 0){
          $alerte="Le regroupement a échoué :<br>Veuillez entrer un identifiant et un nom uniques. ";
        }
        else{

          //create new sous-domaine
          /*$query = "INSERT INTO sous_domaine(ssdom_id, ssdom_nom, ssdom_description, ssdom_fk_domaine, ssdom_admin_fk_groupe_profil, ssdom_createur_fk_groupe_profil)".
                    " VALUES ('".$result_ssdom_id."','".$result_ssdom_name."','".$result_ssdom_desc."',".$pk_dom.",".$pk_groupProfil.",".$pk_groupProfil." );";*/
          $query = "INSERT INTO sous_domaine(ssdom_id, ssdom_nom, ssdom_description, ssdom_fk_domaine, ssdom_admin_fk_groupe_profil, ssdom_createur_fk_groupe_profil)".
                  " VALUES (?, ?, ?, ?, ?, ?);";
          $dao->Execute($query, array($result_ssdom_id, $result_ssdom_name, $result_ssdom_desc, $pk_dom, $pk_groupProfil, $pk_groupProfil));
          $query="COMMIT";
          $dao->Execute($query);

          //select new groupe_profil id
          //$query="SELECT pk_sous_domaine from sous_domaine where ssdom_id='".$result_ssdom_id."'";
          $query="SELECT pk_sous_domaine from sous_domaine where ssdom_id=?";
          $rs_select=$dao->BuildResultSet($query, array($result_ssdom_id));
          for ($rs_select->First(); !$rs_select->EOF();$rs_select->Next()){
            $gridSelectedId = $new_ssdom_pk=$rs_select->Read(0);
          }

          //select all users impacted
          $tabUsers = array();
          $query = "select grpusr_fk_utilisateur from grp_regroupe_usr where grpusr_fk_groupe_profil =:pk_groupProfil";
          $rs_select = $dao->BuildResultSet($query, array("pk_groupProfil" => $pk_groupProfil));
          $tabUsers = array();
          for ($rs_select->First(); !$rs_select->EOF();$rs_select->Next()){
              $tabUsers[] = $rs_select->Read(0);
          }
          
          //change keywords in metadata
          //select affected metadata_id
          $dao->setSearchPath("catalogue,public");
          $query_mdata = "SELECT metadata.id FROM metadata inner join ssdom_dispose_metadata on metadata.uuid = ssdom_dispose_metadata.uuid where ssdcouch_fk_sous_domaine in(?,?) ";
          $rs_select_mdata=$dao->BuildResultSet($query_mdata, array($pk_ssdom1, $pk_ssdom2));
          $tabMetadata = array();
          for ($rs_select_mdata->First(); !$rs_select_mdata->EOF();$rs_select_mdata->Next()){

            $tabMetadata[]=$rs_select_mdata->Read(0);
            
                /*
            $mdata_data=$rs_select_mdata->Read(1);
            //lookup for the keyword

            //manipulate the xml document metadata
            $metadata_doc = new \DOMDocument('1.0', 'UTF-8');

            //echo "mdata_data=".$mdata_data;

            //$metadata_doc->loadXML(($mdata_data));
            $myXML=($mdata_data);

            $myXML=preg_replace("!&!", "&amp;", $myXML);
            $metadata_doc->loadXML($myXML);

            $xpath = new \DOMXpath($metadata_doc);

            //Intervention de Marie
            $namespaces = array(
                "gfc"=>"http://www.isotc211.org/2005/gfc",
                "xsi"=>"http://www.w3.org/2001/XMLSchema-instance", 
                "gco"=>"http://www.isotc211.org/2005/gco", 
                "gmd"=>"http://www.isotc211.org/2005/gmd",
                "fra"=>"http://www.cnig.gouv.fr/2005/fra", 
                "gts"=>"http://www.isotc211.org/2005/gts",
                "gmx"=>"http://www.isotc211.org/2005/gmx", 
                "xlink"=>"http://www.w3.org/1999/xlink",
            );
            foreach($namespaces as $name=>$path) {
                $xpath->registerNamespace($name, $path);
            }
            //replace the data
            $keyword_list = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/fra:FRA_DataIdentification/gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:keyword/gco:CharacterString");
            for ($i = 0; $i < $keyword_list->length; $i++)
            {
              $keyword=$keyword_list->item($i)->nodeValue;
              if ($keyword==$ssdom1_name || $keyword==$ssdom2_name ){
                $keyword_list->item($i)->nodeValue=$result_ssdom_name;
              }
              if ($keyword==$old_dom_name_ssdom1 || $keyword==$old_dom_name_ssdom2 ){
                $keyword_list->item($i)->nodeValue=$new_dom_name;
              }
            }

            //ISO 19139 metadatas
            $keyword_list = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:keyword/gco:CharacterString");

            for ($i = 0; $i < $keyword_list->length; $i++)
            { 
              $keyword=$keyword_list->item($i)->nodeValue;
              if ($keyword==$ssdom1_name || $keyword==$ssdom2_name ){
                $keyword_list->item($i)->nodeValue=$result_ssdom_name;
              }
              if ($keyword==$old_dom_name_ssdom1 || $keyword==$old_dom_name_ssdom2 ){
                $keyword_list->item($i)->nodeValue=$new_dom_name;
              }
            }
            $new_mdata_data=$metadata_doc->saveXML();
            $new_mdata_data=str_replace('<?xml version="1.0"?>',"",$new_mdata_data);//delete xml version="1.0" otherwise geosource crashes

            //TODO à tester - depuis la version 3.4
            $new_mdata_data=preg_replace("!&!", "&amp;", $new_mdata_data);

            //$query_update = "update metadata set data='".addslashes(html_entity_decode($new_mdata_data, ENT_QUOTES, 'UTF-8'))."' where id=".$mdata_id;
            $params = array('data'=>html_entity_decode($new_mdata_data, ENT_QUOTES, 'UTF-8'), 'id'=>$mdata_id);
            $query_update = "update metadata set data=:data where id=:id";
            $dao->Execute($query_update, $params);

            $sqlStmt_update = "COMMIT";
            $dao->Execute( $sqlStmt_update );*/


        }

          //update depending tables
        $dao->setSearchPath("CATALOGUE");
          
        $query = array();
        $params = array();
          
        /*$query="UPDATE grp_accede_ssdom set grpss_fk_sous_domaine=".$new_ssdom_pk.
                 " where grpss_fk_sous_domaine=".$pk_ssdom1.";";*/
        $query[] = "UPDATE grp_accede_ssdom set grpss_fk_sous_domaine=:new_grpss_fk_sous_domaine".
                   " where grpss_fk_sous_domaine=:grpss_fk_sous_domaine;";
          //probleme de contrainte unique (grpss_fk_sous_domaine, grpss_fk_groupe_profil)
          //on efface les enregistrements qui ont le meme grpss_fk_groupe_profil que ceux qui viennent d'etre mis
          //a jour et qui ont la grpss_fk_sous_domaine qui doit etre mise à jour cad $pk_ssdom2
          /*$query.="delete from grp_accede_ssdom where grpss_fk_groupe_profil in (select grpss_fk_groupe_profil from grp_accede_ssdom where grpss_fk_sous_domaine=".
                  $new_ssdom_pk.") and ".
                  " grpss_fk_sous_domaine=".$pk_ssdom2;*/
        $query[] = "delete from grp_accede_ssdom ".
                   " where grpss_fk_groupe_profil in (".
                     " select grpss_fk_groupe_profil from grp_accede_ssdom ".
                     " where grpss_fk_sous_domaine=:new_grpss_fk_sous_domaine".
                   ") and grpss_fk_sous_domaine=:grpss_fk_sous_domaine";
        foreach($query as $a_query){
            $dao->Execute($a_query, array(
                                      "new_grpss_fk_sous_domaine"=>$new_ssdom_pk,
                                      "grpss_fk_sous_domaine"=>$pk_ssdom1,
                                      "new_grpss_fk_sous_domaine"=>$new_ssdom_pk,
                                      "grpss_fk_sous_domaine"=>$pk_ssdom2,
                                )
              );
        }
        $query="COMMIT";
        $dao->Execute($query);

          $query = array();
          $params = array();

          /*$query="UPDATE grp_accede_ssdom set grpss_fk_sous_domaine=".$new_ssdom_pk.
                     " where grpss_fk_sous_domaine=".$pk_ssdom2.";";*/
          $query[] = "UPDATE grp_accede_ssdom set grpss_fk_sous_domaine=:new_grpss_fk_sous_domaine".
                     " where grpss_fk_sous_domaine=:grpss_fk_sous_domaine;";
          $params[] = array(
              "new_grpss_fk_sous_domaine"=>$new_ssdom_pk,
              "grpss_fk_sous_domaine"=>$pk_ssdom2
          );
          //////////
          /*$query.="UPDATE ssdom_dispose_couche set ssdcouch_fk_sous_domaine=".$new_ssdom_pk.
                 " where ssdcouch_fk_sous_domaine=".$pk_ssdom1.";";*/
          $query[] = "UPDATE ssdom_dispose_couche set ssdcouch_fk_sous_domaine=:new_ssdcouch_fk_sous_domaine".
                     " where ssdcouch_fk_sous_domaine=:ssdcouch_fk_sous_domaine;";
          $params[] = array(
              "new_ssdcouch_fk_sous_domaine"=>$new_ssdom_pk,
              "ssdcouch_fk_sous_domaine"=>$pk_ssdom1
          );
          
          $query[] = "UPDATE ssdom_dispose_metadata set ssdcouch_fk_sous_domaine=:new_ssdcouch_fk_sous_domaine".
              " where ssdcouch_fk_sous_domaine=:ssdcouch_fk_sous_domaine;";
          $params[] = array(
              "new_ssdcouch_fk_sous_domaine"=>$new_ssdom_pk,
              "ssdcouch_fk_sous_domaine"=>$pk_ssdom1
          );
          
          //probleme de contrainte unique (ssdcouch_fk_couche_donnees, ssdcouch_fk_sous_domaine)
          //on efface les enregistrements qui ont le meme ssdcouch_fk_couche_donnees que ceux qui viennent d'etre mis
          //a jour et qui ont la ssdcouch_fk_sous_domaine qui doit etre mise à jour cad $pk_ssdom2
          /*$query.="delete from ssdom_dispose_couche where ssdcouch_fk_couche_donnees in (select ssdcouch_fk_couche_donnees from ssdom_dispose_couche where ssdcouch_fk_sous_domaine=".
                  $new_ssdom_pk.") and ".
                  " ssdcouch_fk_sous_domaine=".$pk_ssdom2;*/
          $query[] = "delete from ssdom_dispose_couche where ssdcouch_fk_couche_donnees in (select ssdcouch_fk_couche_donnees from ssdom_dispose_couche where ssdcouch_fk_sous_domaine=:new_ssdcouch_fk_sous_domaine".
                     ") and ".
                     " ssdcouch_fk_sous_domaine=:ssdcouch_fk_sous_domaine";
          $params[] = array(
              "new_ssdcouch_fk_sous_domaine"=>$new_ssdom_pk,
              "ssdcouch_fk_sous_domaine"=>$pk_ssdom2
          );
          
          $query[] = "delete from ssdom_dispose_metadata where uuid in (select uuid from ssdom_dispose_metadata where ssdcouch_fk_sous_domaine=:new_ssdcouch_fk_sous_domaine".
              ") and ".
              " ssdcouch_fk_sous_domaine=:ssdcouch_fk_sous_domaine";
          $params[] = array(
              "new_ssdcouch_fk_sous_domaine"=>$new_ssdom_pk,
              "ssdcouch_fk_sous_domaine"=>$pk_ssdom2
          );

          foreach($query as $i=>$a_query) {
              $dao->Execute($a_query, $params[$i]);
          }

          $query="COMMIT";
          $dao->Execute($query);

          $query = array();
          $params = array();

          /*$query="UPDATE ssdom_dispose_couche set ssdcouch_fk_sous_domaine=".$new_ssdom_pk.
                 " where ssdcouch_fk_sous_domaine=".$pk_ssdom2.";";*/

          $query[] = "UPDATE ssdom_dispose_metadata set ssdcouch_fk_sous_domaine=:new_ssdcouch_fk_sous_domaine".
              " where ssdcouch_fk_sous_domaine=:ssdcouch_fk_sous_domaine;";
          $params[] = array(
              "new_ssdcouch_fk_sous_domaine"=>$new_ssdom_pk,
              "ssdcouch_fk_sous_domaine"=>$pk_ssdom2
          );
          
          
          $query[] = "UPDATE ssdom_dispose_couche set ssdcouch_fk_sous_domaine=:new_ssdcouch_fk_sous_domaine".
                     " where ssdcouch_fk_sous_domaine=:ssdcouch_fk_sous_domaine;";
          $params[] = array(
              "new_ssdcouch_fk_sous_domaine"=>$new_ssdom_pk,
              "ssdcouch_fk_sous_domaine"=>$pk_ssdom2
          );

          /*$query.="UPDATE ssdom_visualise_carte set ssdcart_fk_sous_domaine=".$new_ssdom_pk.
                 " where ssdcart_fk_sous_domaine=".$pk_ssdom1.";";*/
          $query[] = "UPDATE ssdom_visualise_carte set ssdcart_fk_sous_domaine=:new_ssdcart_fk_sous_domaine".
                     " where ssdcart_fk_sous_domaine=:ssdcart_fk_sous_domaine;";
          $params[] = array(
              "new_ssdcart_fk_sous_domaine"=>$new_ssdom_pk,
              "ssdcart_fk_sous_domaine"=>$pk_ssdom1
          );
          //probleme de contrainte unique (ssdcart_fk_carte_projet, ssdcart_fk_sous_domaine)
          //on efface les enregistrements qui ont le meme ssdcart_fk_carte_projet que ceux qui viennent d'etre mis
          //a jour et qui ont la ssdcart_fk_sous_domaine qui doit etre mise à jour cad $pk_ssdom2
          /*$query.="delete from ssdom_visualise_carte where ssdcart_fk_carte_projet in (select ssdcart_fk_carte_projet from ssdom_visualise_carte where ssdcart_fk_sous_domaine=".
                  $new_ssdom_pk.") and ".
                  " ssdcart_fk_sous_domaine=".$pk_ssdom2;*/
          $query[] = "delete from ssdom_visualise_carte where ssdcart_fk_carte_projet in (select ssdcart_fk_carte_projet from ssdom_visualise_carte where ssdcart_fk_sous_domaine=:new_ssdcart_fk_sous_domaine".
                     ") and ".
                     " ssdcart_fk_sous_domaine=:ssdcart_fk_sous_domaine";
          $params[] = array(
              "new_ssdcart_fk_sous_domaine"=>$new_ssdom_pk,
              "ssdcart_fk_sous_domaine"=>$pk_ssdom2
          );

          foreach($query as $i=>$a_query) {
              $dao->Execute($a_query, $params[$i]);
          }

          $query="COMMIT";
          $dao->Execute($query);

          /*$query="UPDATE ssdom_visualise_carte set ssdcart_fk_sous_domaine=".$new_ssdom_pk.
                 " where ssdcart_fk_sous_domaine=".$pk_ssdom2.";";*/
          $query="UPDATE ssdom_visualise_carte set ssdcart_fk_sous_domaine=:new_ssdcart_fk_sous_domaine".
                " where ssdcart_fk_sous_domaine=:ssdcart_fk_sous_domaine;";

          $dao->Execute($query, array(
                                  "new_ssdcart_fk_sous_domaine"=>$new_ssdom_pk,
                                  "ssdcart_fk_sous_domaine"=>$pk_ssdom2,
                                )
              );
          $query="COMMIT";
          $dao->Execute($query);

          $query = array();
          $params = array();

          //delete old sous-domaine fk from everywhere
          //$query="DELETE from grp_accede_ssdom where grpss_fk_sous_domaine=".$pk_ssdom1." or grpss_fk_sous_domaine=".$pk_ssdom2.";";
          $query[] = "DELETE from grp_accede_ssdom where grpss_fk_sous_domaine=:grpss_fk_sous_domaine1 or grpss_fk_sous_domaine=:grpss_fk_sous_domaine2;";
          $params[] = array(
              "grpss_fk_sous_domaine1"=>$pk_ssdom1,
              "grpss_fk_sous_domaine2"=>$pk_ssdom2
          );
          //$query.="DELETE from ssdom_dispose_couche where ssdcouch_fk_sous_domaine=".$pk_ssdom1." or ssdcouch_fk_sous_domaine=".$pk_ssdom2.";";
          $query[] = "DELETE from ssdom_dispose_couche where ssdcouch_fk_sous_domaine=:ssdcouch_fk_sous_domaine1 or ssdcouch_fk_sous_domaine=:ssdcouch_fk_sous_domaine2;";
          $params[] = array(
              "ssdcouch_fk_sous_domaine1"=>$pk_ssdom1,
              "ssdcouch_fk_sous_domaine2"=>$pk_ssdom2
          );
          //$query.="DELETE from ssdom_visualise_carte where ssdcart_fk_sous_domaine=".$pk_ssdom1." or ssdcart_fk_sous_domaine=".$pk_ssdom2.";";
          $query[] = "DELETE from ssdom_visualise_carte where ssdcart_fk_sous_domaine=:ssdcart_fk_sous_domaine1 or ssdcart_fk_sous_domaine=:ssdcart_fk_sous_domaine2;";
          $params[] = array(
              "ssdcart_fk_sous_domaine1"=>$pk_ssdom1,
              "ssdcart_fk_sous_domaine2"=>$pk_ssdom2
          );

          $query[] = "DELETE from ssdom_dispose_metadata where ssdcouch_fk_sous_domaine=:ssdcouch_fk_sous_domaine1 or ssdcouch_fk_sous_domaine=:ssdcouch_fk_sous_domaine2;";
          $params[] = array(
              "ssdcouch_fk_sous_domaine1"=>$pk_ssdom1,
              "ssdcouch_fk_sous_domaine2"=>$pk_ssdom2
          );
          
          foreach($query as $i=>$a_query) {
              $dao->Execute($a_query, $params[$i]);
          }

          $query="COMMIT";
          $dao->Execute($query);

          //delete old sous-domaine
          //$query="DELETE from sous_domaine where pk_sous_domaine=".$pk_ssdom1." or pk_sous_domaine=".$pk_ssdom2;
          $query="DELETE from sous_domaine where pk_sous_domaine=:pk_sous_domaine1 or pk_sous_domaine=:pk_sous_domaine2";
          $dao->Execute($query, array(
                                  "pk_sous_domaine1" =>$pk_ssdom1,
                                  "pk_sous_domaine2"=>$pk_ssdom2
                                )
              );
          $query="COMMIT";
          $dao->Execute($query);
          
          //delete sdom from LDAP GROUP
          $sousDomaineVO = new SousDomaineVO();
          $sousDomaineVO->setDao($dao);
          
          $sousDomaineVO->ldapSyncAction($pk_ssdom1,true);
          $sousDomaineVO->ldapSyncAction($pk_ssdom2,true);
          //add new LDAP GROUP
          $sousDomaineVO->ldapSyncAction($new_ssdom_pk);
          
          //mise à jour LDAP des user concernés
          $utilisateurVO = new UtilisateurVO();
          $utilisateurVO->setDao($dao);
          
          foreach($tabUsers as $key => $id){
              $utilisateurVO->ldapSyncAction($id);
          }
          
          
          //update thesaurus
          ThesaurusSerializer::getInstance($dao->getConnection(), array(), THESAURUS_NAME)->generateDocument();
          
          //maj metadata with new keywords
          foreach($tabMetadata as $key => $id){
              $response = $this->forward('Prodige\ProdigeBundle\Controller\UpdateDomSdomController::updateDomSdomAction', array(
                  'fmeta_id'  => $id,
                  'modeData' => 'couche' //works for all types
              ));
          }

        }
      }

    }

    else{
      $alerte = 'Erreur de connexion à la base de données.';
    }

    break;

  default:
    break;
}

$bError = ($alerte!="");

$strJs = ($bError
          ? "" 
          : "if (parent.document.".$reloadForm.") parent.document.".$reloadForm.".reset();" .
            "parent.reloadGrid(".$gridReload.", ".$gridSelectedId.", false);");

switch ( $iMode ){
  case "1" :
    if ( $bError ){
      AlertSaveController::AlertSaveProfil("", true, $alerte, $gridReload, $gridSelectedId);
    }
    else {
      AlertSaveController::AlertSaveProfil($strJs, false, "", $gridReload, $gridSelectedId);
    }
  break;
  case "2" :
    if ( $bError ){
      AlertSaveController::AlertSaveSSDom("", true, $alerte, $gridReload, $gridSelectedId);
    }
    else {
      AlertSaveController::AlertSaveSSDom($strJs, false, "", $gridReload, $gridSelectedId);
    }
  break;
}
exit();

$strhtml = '
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title> </title>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
    <script  src="/Scripts/administration_grouping.js"> </script>
    <!-- <?php echo addScriptCss($AdminPath."/../CSS/administration_database.php");?> -->
    <link rel="stylesheet" type="text/css" href="">
  </head>
  <body onload="">
    <p align="center">'.
    (($alerte != '')
      ? $alerte 
      : 'Regroupement effectué.').
     '<br>
      <br>
      <input type="button" value="Fermer" onclick="window.close(); window.opener.document.ReloadForm.submit();">
      </p>
 </body>
</html>';

        echo $strhtml;

<?php
/*
 * 13-01-2009
 * sert à modifier la liste select (contenant les id des sous-domaines) dynamiquement
 */
//require_once($AdminPath."/../parametrage.php"); //TODO hismail
/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
require_once($AdminPath."/DAO/DAO/DAO.php");*/
use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\DAOProxy\DAO;

$no_ssdom        = (isset($_GET["no_ssdom"])     ? $_GET["no_ssdom"]     : '');
$value_to_del    = (isset($_GET["value_to_del"]) ? $_GET["value_to_del"] : '');
$pk_dom_selected = (isset($_GET["pk_dom_value"]) ? $_GET["pk_dom_value"] : '');
$iMode           = (isset($_GET["iMode"])        ? $_GET["iMode"]        : '');
$pk_to_del      = "";
$strScript      = "";

//$dao = new DAO();
$dao = new DAO($conn, 'catalogue');

if ($dao)
{ 
  switch ($iMode){
    
    case "1":
    
    $strScript = " var oWind = window.parent;".
                 " var pk_ssdom".$no_ssdom." = oWind.document.regrpt_ssdom.elements['pk_ssdom".$no_ssdom."'];".
                 " var selected_index=pk_ssdom".$no_ssdom.".selectedIndex;".
                 " var selected_value=pk_ssdom".$no_ssdom.".options[selected_index].value;".
                 " var i=0;";
  
    $query="select PK_SOUS_DOMAINE, SSDOM_ID from SOUS_DOMAINE where PK_SOUS_DOMAINE <> ".$value_to_del.
           " order by SSDOM_ID";
    $rs_select=$dao->BuildResultSet($query);
    for($rs_select->First();!$rs_select->EOF();$rs_select->Next()){
      $pk_ssdom=$rs_select->Read(0);
      $ssdom_id=$rs_select->Read(1);
      $strScript .= " if (selected_value==".$pk_ssdom.") {pk_ssdom".$no_ssdom.".options[i].selected=true;}";
      $strScript .= " else {pk_ssdom".$no_ssdom.".options[i].selected=false};";
      $strScript .= " pk_ssdom".$no_ssdom.".options[i].value=".$pk_ssdom.";";
      $strScript .= " pk_ssdom".$no_ssdom.".options[i].text='".$ssdom_id."';";
      $strScript .= " i++;";
    }
    $strScript .= " pk_ssdom".$no_ssdom.".length=i;";
    
      break;
    case "2":
      break;
    default:
      break;
    
  }

}

echo "<html><head>" .
 "<script language='JavaScript'>".
 " function onLoadExec() { ".$strScript." }".
 "</script></head>".
 " <body onload='onLoadExec()'></body>".
 "</html>";
exit();


 
 ?>
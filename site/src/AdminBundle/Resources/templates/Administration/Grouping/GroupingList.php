<?php
/**
 * Liste des couches (haut) dans l'adminsitration des couches (administration des droits)
 * @author Alkante
 */
	//require_once($AdminPath."/Ressources/Administration/Ressources.php");
	use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;

  header("Content-type: application/xml");
  echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 
  $tabRegroupement = array(
    Ressources::$ACTION_OPEN_GROUPING_PROFILES    => "De profils",
    Ressources::$ACTION_OPEN_GROUPING_SUBDOMAINS  => "De sous-domaines"
  );


  print ('
    <select
        multiple="multiple" id="SelectGrouping"
        language="javascript" 
        onchange="SelectGrouping_onclick(this);"
        >
    ');
  // FILL THE SELECT LIST
  foreach ($tabRegroupement as $idRegroupement=>$strRegroupement)
  {
    print('
      <option value="'.$idRegroupement.'" title="'.$strRegroupement.'" >
        '.$strRegroupement.'
      </option>
      ');
  }
  print ('</select>');

?>
	</body>
</html>
<?php
/*
 * 13-01-2009
 * Regroupement des sous-domaines
 * 
 * Tables affectées par le regroupement des sous-domaines :
 * grp_accede_ssdom
 * sous_domaine
 * ssdom_dispose_couche
 * ssdom_visualise_carte
 *  
 */

header("Content-Type: text/html; charset=UTF-8;");
/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
require_once($AdminPath."/DAO/DAO/DAO.php");*/

use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\DAOProxy\DAO;

$pk_dom_selected   = (isset($_POST["pk_dom"])           ? $_POST["pk_dom"]          : "");
$result_dom_id     = (isset($_POST["result_dom_id"])    ? $_POST["result_dom_id"]   : "");
$result_dom_nom    = (isset($_POST["result_dom_nom"])   ? $_POST["result_dom_nom"]  : "");
$result_dom_desc   = (isset($_POST["result_dom_desc"])  ? $_POST["result_dom_desc"] : "");;
$pk_ssdom1         = (isset($_POST["pk_ssdom1"])        ? $_POST["pk_ssdom1"]       : "");
$pk_ssdom1         = (isset($_POST["pk_ssdom2"])        ? $_POST["pk_ssdom2"]       : "");
$pk_groupProfil    = (isset($_POST["pk_groupProfil"])   ? $_POST["pk_groupProfil"]  : "");
$new_dom_pk="";

//$dao = new DAO();
$dao = new DAO($conn, 'catalogue');
if ($dao)
{ 
}
else{
  $errormsg = 'Erreur de connexion à la base de données.';
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Regroupement des sous-domaines</title>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
    <script  src="/Scripts/administration_grouping.js"> </script>
  </head>
  <body onload="administration_init();">
    <div class="errormsg" id="errormsg">
      <?php
        if (isset($errormsg))
          echo $errormsg;
      ?>
    </div>
    <div class='administration_gen'>
      <br/>
      <br/>
      <form name="ReloadForm" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
      </form>
      <form name="regrpt_ssdom" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST" target="<?php echo Ressources::$IFRAME_NAME_MAIN;?>">
        <table align="center">
          <!-- <tr>
            <th>Sélectionnez un domaine</th>
            <td>
              <select name="pk_dom" ID="pk_dom" size="1" onchange="onChangeDom()">
                <option  value="-1" >Sélectionnez</option>
              <?php 
              /*if ($dao)
              {
                $query = 'SELECT PK_DOMAINE, DOM_ID FROM DOMAINE order by DOM_ID';
                $rs_dom = $dao->BuildResultSet($query);
                for ($rs_dom->First(); !$rs_dom->EOF(); $rs_dom->Next())
                {
                  $pk_dom = $rs_dom->Read(0);  
                  $dom_id = $rs_dom->Read(1);
                  if ($pk_dom==$pk_dom_selected){
                    echo '<option  value="'. $pk_dom. '" selected>'. ($dom_id).'</option>';
                  } 
                  else{
                    echo '<option  value="'. $pk_dom. '" >'. ($dom_id).'</option>';
                  }
                } 
              } 
              */?>
              </select>
            </td>
          </tr> -->


          <tr>
            <th>Sous-domaine 1</th>
            <td>
              <select name="pk_ssdom1" ID="pk_ssdom1" size="1" onchange="onChangeSSDom(1)">
                <option  value="-1" >Sélectionnez</option>
              <?php 
              if ($dao)
              {
                $query = 'SELECT PK_SOUS_DOMAINE, SSDOM_ID FROM SOUS_DOMAINE where ssdom_fk_domaine is not null order by SSDOM_ID ';
                $rs_ssdom1 = $dao->BuildResultSet($query);
                for ($rs_ssdom1->First(); !$rs_ssdom1->EOF(); $rs_ssdom1->Next())
                {
                  $pk_ssdom1 = $rs_ssdom1->Read(0);  
                  $ssdom1_id = $rs_ssdom1->Read(1);
                    echo '<option  value="'. $pk_ssdom1. '" >'. $ssdom1_id.'</option>'; 
                  }
                }  
              ?>
              </select>
            </td>
          </tr>
          <tr>
            <th>Sous-domaine 2</th>
            <td>
              <select name="pk_ssdom2" ID="pk_ssdom2" size="1" onchange="onChangeSSDom(2)">
                <option  value="-1" >Sélectionnez</option>
              <?php 
              if ($dao)
              {
                $query = 'SELECT PK_SOUS_DOMAINE, SSDOM_ID FROM SOUS_DOMAINE where ssdom_fk_domaine is not null order by SSDOM_ID';
                $rs_ssdom2 = $dao->BuildResultSet($query);
                for ($rs_ssdom2->First(); !$rs_ssdom2->EOF(); $rs_ssdom2->Next())
                {
                  $pk_ssdom2 = $rs_ssdom2->Read(0);  
                  $ssdom2_id = $rs_ssdom2->Read(1);
                    echo '<option  value="'. $pk_ssdom2. '" >'. $ssdom2_id.'</option>'; 
                  }
                }
              ?>
              </select>
            </td>
          </tr>

          <tr>
            <th>Identifiant du nouveau sous-domaine</th>
            <td>
              <input type="text" name="result_ssdom_id" value="<?php echo (isset($result_profil_id) ? $result_profil_id : ''); ?>" maxlength="30" size="30">
            </td>
          </tr>

          <tr>
            <th>Nom du nouveau sous-domaine</th>
            <td>
              <input type="text" name="result_ssdom_name" value="<?php echo (isset($result_profil_name) ? $result_profil_name : ''); ?>" maxlength="30" size="30">
            </td>
          </tr>

          <tr>
            <th>Description du nouveau sous-domaine</th>
            <td>
              <input type="text" name="result_ssdom_desc" value="<?php echo (isset($result_profil_desc) ? $result_profil_desc : ''); ?>" maxlength="60" size="60">
            </td>     
          </tr>
         <tr>
            <th>Domaine du nouveau sous-domaine</th>
            <td>
              <select name="pk_dom" ID="pk_dom" size="1" onchange="">
                <option  value="-1" >Sélectionnez</option>
              <?php 
              if ($dao)
              {
                $query = 'SELECT PK_DOMAINE, DOM_ID FROM DOMAINE order by DOM_ID';
                $rs_dom = $dao->BuildResultSet($query);
                for ($rs_dom->First(); !$rs_dom->EOF(); $rs_dom->Next())
                {
                  $pk_dom = $rs_dom->Read(0);  
                  $dom_id = $rs_dom->Read(1);
                  if ($pk_dom==$pk_dom_selected){
                    echo '<option  value="'. $pk_dom. '" selected>'. $dom_id.'</option>';
                  } 
                  else{
                    echo '<option  value="'. $pk_dom. '" >'. $dom_id.'</option>';
                  }
                } 
              } 
              ?>
              </select>
            </td>          
          </tr>
          <tr>
            <th>Groupe-profil administrateur</th>
            <td>
              <select name="pk_groupProfil" ID="pk_groupProfil" size="1" onchange="">
              <?php 
              if ($dao)
              {
                $query = 'SELECT PK_GROUPE_PROFIL, GRP_ID FROM GROUPE_PROFIL ORDER BY GRP_ID';
                $rs_group_profil1 = $dao->BuildResultSet($query);
                for ($rs_group_profil1->First(); !$rs_group_profil1->EOF(); $rs_group_profil1->Next())
                {
                  $group_profil1_id = $rs_group_profil1->Read(0);  
                  $group_profil1_name = $rs_group_profil1->Read(1);
                  echo '<option  value="'. $group_profil1_id. '" >'. $group_profil1_name.'</option>'; 
                  
                } 
              } 
              ?>
              </select>
            </td>
          </tr>
          <tr><td>&nbsp;</td></tr>
          <tr>
            <td colspan="2" class="validation" align="center"><input type="button" name="add" value="Valider" onclick="addSSDom()">
            </td>
          </tr>
          <?//}//fin de if pk_dom_selected!="" ?>
        </table>
      </form>
    </div>
    <!-- <iframe name='footerExec' width='200' height='200' frameborder='1' src=''></iframe> -->
    <iframe name='footerExec' width='0' height='0' frameborder='0' src=''></iframe> 
  </body>
</html>
<?php
/*
 * 13-01-2009
 * sert à modifier la liste select (contenant les id des groupes-profil) dynamiquement
 */
//require_once($AdminPath."/../parametrage.php"); //TODO hismail

/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
require_once($AdminPath."/DAO/DAO/DAO.php");*/
use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\DAOProxy\DAO;

$no_groupProfil = (isset($_GET["groupProfil"])  ? $_GET["groupProfil"] : '');
$value_to_del   = (isset($_GET["value_to_del"]) ? $_GET["value_to_del"]: '');
$pk_to_del      = "";

//$dao = new DAO();
$dao = new DAO($conn, 'catalogue');

if ($dao)
{ 
  $strScript = " var oWind = window.parent;".
               " var pk_groupProfil".$no_groupProfil." = oWind.document.groupes_profil.elements['pk_groupProfil".$no_groupProfil."'];".
               " var selected_index=pk_groupProfil".$no_groupProfil.".selectedIndex;".
               " var selected_value=pk_groupProfil".$no_groupProfil.".options[selected_index].value;".
               " var i=0;";

  $query="select pk_groupe_profil, grp_id from groupe_profil where pk_groupe_profil <>".$value_to_del ." and GRP_IS_DEFAULT_INSTALLATION=0";
  $rs_select=$dao->BuildResultSet($query);
  for($rs_select->First();!$rs_select->EOF();$rs_select->Next()){
    $pk_groupe_profil=$rs_select->Read(0);
    $groupe_profil_id=$rs_select->Read(1);
    
    $strScript .= " if (selected_value==".$pk_groupe_profil.") {pk_groupProfil".$no_groupProfil.".options[i].selected=true;}";
    $strScript .= " else {pk_groupProfil".$no_groupProfil.".options[i].selected=false};";
    $strScript .= " pk_groupProfil".$no_groupProfil.".options[i].value=".$pk_groupe_profil.";";
    $strScript .= " pk_groupProfil".$no_groupProfil.".options[i].text='".$groupe_profil_id."';";
    $strScript .= " i++;";
  }
  $strScript .= " pk_groupProfil".$no_groupProfil.".length=i;";
}


echo "<html><head>" .
 "<script language='JavaScript'>".
 " function onLoadExec() { ".$strScript." }".
 "</script></head>".
 " <body onload='onLoadExec()'></body>".
 "</html>";
exit();


 
 ?>
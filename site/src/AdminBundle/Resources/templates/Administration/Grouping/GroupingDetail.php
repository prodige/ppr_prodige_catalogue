<?php 
require_once("../../Ressources/Administration/Ressources.php");
require_once("../../DAO/DAO/DAO.php");  
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css">
      a {color:#000000;text-decoration:none}
      a:hover {text-decoration:underline}
    </style>
    <?php echo addScriptJs("Scripts/administration_grouping.js");?>
  </head>
  <body onload="" style="margin:0px;">
<?php
  // #################################################################
  // #################################################################
  // #################################################################
  
  // set a default action
  if ( !isset( $_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_GROUPING_PROFILES;
  }
  else
  {
    // get the action parameter
    $Action = $_GET["Action"];
  }
  // get the main action
   $mainAction = GetMainAction( $Action );

  print('
    <TABLE width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
      <TR width="100%" height="32" border=0>
        <TD>
          '.DrawHeader($Action).'
        </TD>
      </TR>
      <TR>
        <TD>
          <IFRAME width="100%"
            height="600px"
            MARGINHEIGHT="0"
            MARGINWIDTH="0"
            FRAMEBORDER="0"
            name="'.Ressources::$IFRAME_NAME_MAIN.'"
            ALLOWTRANSPARENCY="true"
            SCROLLING="no"
            SRC="'.GetModulePageDetail($Action).'?Action='.$Action.'&Id='.$PK.'">
          </IFRAME>
        </TD>
      </TR>
    </TABLE>
  ');

 

  // #################################################################
  // #################################################################
  // #################################################################

  /**
   * Ecrit un tableau dans la page html
   * @param $action
   * @param $pk
   * @return unknown_type
   */
  function DrawHeader($action)
  {
    $header ='
      <TABLE style="font-size:8pt;text-align: center;margin-bottom:4px;" width="200px" height="32" border="0" cellpadding="0" cellspacing="0">
        <TR>
          <TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_GROUPING_PROFILES ).'">

            <A HREF="GroupingDetail.php?Action='.Ressources::$ACTION_OPEN_GROUPING_PROFILES.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
              '.Ressources::$BTN_NAV_GROUPING_PROFILES.'
            </A>
          </TD>
          <TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_GROUPING_SUBDOMAINS, true ).'">
            <A HREF="GroupingDetail.php?Action='.Ressources::$ACTION_OPEN_GROUPING_SUBDOMAINS.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
              '.Ressources::$BTN_NAV_GROUPING_SUBDOMAINS.'
            </A>
          </TD>
        </TR>
      </TABLE>';
    
    return $header;
  }

  // #################################################################

  /**
   * Renvoie la chaîne de caractères correspondant à l'attribut html style d'un tableau
   * @param $action
   * @param $celAction
   * @param $bLast
   * @return $strStyle : style du tableau
   */
  function GetHeaderTableBackground( $action, $celAction, $bLast=false )
  {
    $strStyle = "border:1px solid #000000;".(!$bLast ? "border-right:none;" : "")."padding:5px;background-color:";
    if ( $action == $celAction )
    {
      return $strStyle."#FFFFFF;border-bottom:none;";
    }

    return $strStyle."#dddddd;";
  }

  // #################################################################
  
  
  // #################################################################
  /**
   * Renvoie l'action demandée
   * @param $action
   * @return $returnAction : numéro de l'action
   */
  function GetMainAction( $action )
  {

    if ( $action >= Ressources::$ACTION_OPEN_GROUPING_SUBDOMAINS)
    {
      $returnAction = Ressources::$ACTION_OPEN_GROUPING_SUBDOMAINS;
    }

    return $returnAction;
  }
  
  
  function GetModulePageDetail( $action )
  {
    switch( $action )
    {
      case Ressources::$ACTION_OPEN_GROUPING_PROFILES:
        return "../../Administration/Grouping/GroupingGroupesProfil.php";
      break;
      case Ressources::$ACTION_OPEN_GROUPING_SUBDOMAINS:
        return "../../Administration/Grouping/GroupingSubDomains.php";
      break;
    }

    return "../../Administration/Layers/Empty.php";
  }
  
?>
  </body>
</html>
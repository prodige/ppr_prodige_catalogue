<?php
/*
 * 12-01-2009
 * Regroupement des groupes-profil
 * 
 * Tables affectées par le regroupement des groupes-profil :
 * grp_accede_dom
 * grp_accede_ssdom
 * grp_autorise_trt
 * grp_regroupe_usr
 * grp_trt_objet
 * sous_domaine
 * 
 * 
 * 
delete from groupe_profil; delete from grp_accede_dom;
delete from grp_accede_dom;
delete from grp_accede_ssdom;
delete from grp_autorise_trt;
delete from grp_regroupe_usr;
delete from grp_trt_objet;
delete from sous_domaine;
 * 
 */

header("Content-Type: text/html; charset=UTF-8;");
/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
require_once($AdminPath."/DAO/DAO/DAO.php");*/

use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\DAOProxy\DAO;

$result_profil_name   = (isset($_POST["result_profil_name"]) ?  $_POST["result_profil_name"] : "");
$result_profil_id     = (isset($_POST["result_profil_id"])   ?  $_POST["result_profil_id"]   : "");
$result_profil_desc   = (isset($_POST["result_profil_desc"]) ?  $_POST["result_profil_desc"] : "");
$pk_groupProfil1      = (isset($_POST["pk_groupProfil1"])    ?  $_POST["pk_groupProfil1"]    : "");
$pk_groupProfil2      = (isset($_POST["pk_groupProfil2"])    ?  $_POST["pk_groupProfil2"]    : "");
$mode_droits          = (isset($_POST["mode_droits"])        ?  $_POST["mode_droits"]        : "");
$new_group_pk="";
$grp_pk_is_default="";

//$dao = new DAO();
$dao = new DAO($conn, 'catalogue'); //Added by hismail
if ($dao)
{ 
}
else{
  $errormsg = 'Erreur de connexion à la base de données.';
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Regroupement des groupes-profil</title>
    <META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
     <script  src="/Scripts/administration_grouping.js"> </script>
  </head>
  <body onload="administration_init();onChangeGroupeProfil(1);setSizeIFrame();">
    <div class="errormsg" id="errormsg">
      <?php
        if (isset($errormsg))
          echo $errormsg;
      ?>
    </div>
    <div class='administration_gen'>
      <br/>
      <br/>
      <form name="ReloadForm" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST">
      </form>
      <form name="groupes_profil" action="<?php echo $_SERVER['PHP_SELF'];?>" method="POST" target="<?php echo Ressources::$IFRAME_NAME_MAIN;?>">
        <table align="center">
          
          <tr>
            <th>Groupe Profil 1</th>
            <td colspan="2">
              <select name="pk_groupProfil1" ID="pk_groupProfil1" size="1" onchange="onChangeGroupeProfil(1)">
              <?php 
              if ($dao)
              {
                $query = 'SELECT PK_GROUPE_PROFIL, GRP_ID FROM GROUPE_PROFIL where GRP_IS_DEFAULT_INSTALLATION=0 order by GRP_ID';
                $rs_group_profil1 = $dao->BuildResultSet($query);
                for ($rs_group_profil1->First(); !$rs_group_profil1->EOF(); $rs_group_profil1->Next())
                {
                  $pk_group_profil1 = $rs_group_profil1->Read(0);  
                  $group_profil1_id = $rs_group_profil1->Read(1);
                    echo '<option  value="'. $pk_group_profil1. '" >'. $group_profil1_id.'</option>'; 
                  }
                }  
              ?>
              </select>
            </td>          
          </tr>
          <tr>
            <th>Groupe Profil 2</th>
            <td colspan="2">
              <select name="pk_groupProfil2" ID="pk_groupProfil2" size="1" onchange="onChangeGroupeProfil(2)">
              <?php 
              if ($dao)
              {
                $query = 'SELECT PK_GROUPE_PROFIL, GRP_ID FROM GROUPE_PROFIL where GRP_IS_DEFAULT_INSTALLATION=0 order by GRP_ID';
                $rs_group_profil2 = $dao->BuildResultSet($query);
                for ($rs_group_profil2->First(); !$rs_group_profil2->EOF(); $rs_group_profil2->Next())
                {
                  $pk_group_profil2 = $rs_group_profil2->Read(0);  
                  $group_profil2_id = $rs_group_profil2->Read(1);
                    echo '<option  value="'. $pk_group_profil2. '" >'. $group_profil2_id.'</option>'; 
                  }
                }  
              ?>
              </select>
            </td>          
          </tr>
          <tr>
            <th>Identifiant du nouveau groupe-profil</th>
            <td colspan="2">
              <input type="text" name="result_profil_id" value="<?php echo (isset($result_profil_id) ? $result_profil_id : ''); ?>" maxlength="30" size="30">
            </td>
          </tr>
          <tr>
            <th>Nom du nouveau groupe-profil</th>
            <td colspan="2">
              <input type="text" name="result_profil_name" value="<?php echo (isset($result_profil_name) ? $result_profil_name : ''); ?>" maxlength="30" size="30">
            </td>     
          </tr>
          <tr>
            <th>Description du nouveau groupe-profil</th>
            <td colspan="2">
              <input type="text" name="result_profil_desc" value="<?php echo (isset($result_profil_desc) ? $result_profil_desc : ''); ?>" maxlength="60" size="60">
            </td>     
          </tr>
          <tr>
            <th>Mode de récupération des droits</th>
            <td colspan="2">
              <select name="mode_droits" ID="mode_droits" size="1">
                <option  value="droits_profil1" >Droits du premier profil</option>
                <option  value="droits_profil2" >Droits du second profil</option>
                <option  value="droits_profil12" >Droits du premier profil + droits du second profil</option>
                <option  value="aucun" >Aucun droit</option>
              </select>
            </td>
          </tr>
          <tr><td>&nbsp;</td></tr>
          <tr>
            <td colspan="3" class="validation" align="center"><input type="button" name="add" value="Valider" onclick="addGroupProfil()">
            </td>
          </tr>
        </table>
      </form>
    </div>
    <iframe name='footerExec' width='0' height='0' frameborder='0' src=''></iframe>
  </body>
</html>
<?php
  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;
  
	$Action = Ressources::$LIST_MODIFY_ACTION;
	$PK = -1;
	
	if (isset($_GET["Action"])) $Action = intval($_GET["Action"]);
	if (isset($_GET["Id"])) 	$PK 	= intval($_GET["Id"]);	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<script>

		function uploadFile(form, post_name, callback){
			var action = form.action;
			form.target = '<?php echo Ressources::$IFRAME_NAME_MAIN ?>';
			var win = window.frames[form.target];
			if ( !win ) {form.target = ""; win = window;};
			window.callbackUpload = callback;
			form.action = Routing.generate("<?php echo Ressources::$ROUTE_UPLOAD_TEMP ?>", {post_name : post_name});
			form.submit();
			form.action = action;
	  }
		function UserAdding_ImportUser(){
			var elt;
			var err1;
			
			err1 = "Vous devez définir un fichier d'import de type LDIF.";
			
			elt = document.getElementById("FILE_LDIF");
			if (elt){
				if (elt.value == "") {
					alert(err1);
					return ;
				}
				var form = document.getElementById('importLdap');
				var callback = function(file){
					loadMainPanel("Gestion des utilisateurs : Import Ldap", form.action+(form.action.indexOf('?')!=-1 ? '&' : "?")+"FILE_LDIF="+escape(file));
				};
				uploadFile(form, elt.name, callback);
			}
			else {
				alert("eErreur interne:\nImpossible de récupérer l'élément File_LDIF");
			}
		}

		
	    function UserPerimetre_Import(formId, fieldId, title){
				var elt;
				var err1;

	      err1 = "Vous devez définir un fichier d'import de type csv.";
				
				elt = document.getElementById(fieldId);
				if (elt){
					if (elt.value == "") {
						alert(err1);
						return ;
					}
					var form = document.getElementById(formId);
					var callback = function(file){
						var params = {};
		        var box = Ext.MessageBox.wait('Traitement du fichier...', 'Intégration de la liaison '+title);
		        parent.Ext.Ajax.request({
		          url :  form.action, 
		          method : 'GET',
		          params : {
		            'CSV_FILE' : escape(file)
		          }, 
		          timeout : 0,
		          success: function(result){   
		            box.hide();  
		            response = parent.Ext.decode(result.responseText);  
		            if(response.success){
		              parent.Ext.MessageBox.alert('Succès', "L'import a été réalisé avec succès");
		            }else{
		              parent.Ext.MessageBox.alert('Erreur', response.msg);
		            }
		          },failure: function(result){
		              box.hide();
		              parent.Ext.MessageBox.alert('Erreur', 'Erreur lors de la lecture du fichier'); 
		          } 
		         });
		         
					};
					uploadFile(form, elt.name, callback);
				}
				else {
	        alert("Erreur interne:\nImpossible de récupérer l'élément Fichier");
				}
	    }
		</script>
	</head>
	<body style="margin:0px;">
<br><br>	

<?php 
// ne peut pas insérer de nouveaux utilisateurs si Prodige est intégrée dans une application externe
if ( !(defined("PRO_INCLUDED") && PRO_INCLUDED) ){
?>
	<form name="importLdap" id="importLdap" method="POST" action="<?php echo $importLDAP; ?>"  enctype="multipart/form-data">
	<div align="center">
		<table border="0" width="60%">
			<tr>
				<td colspan="2" style="font-size:16px;font-weight:bold" align="center">
					Importation des utilisateurs depuis
					le fichier d'export de l'annuaire LDAP
					<br>
				</td>
			</tr>
			<tr>
				<td>
					Fichier à importer :
				</td>
				<td align="left" valign="top">
					<input id="FILE_LDIF" name="FILE_LDIF" type="File" accept=".ldif,.txt" width="100" >
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<br>
					<input id="FILE_LDIF" name="FILE_LDIF" type="button" value="Importer les utilisateurs" onclick="UserAdding_ImportUser()">
				</td>
			</tr>
		</table>
	</div>
	</form>
<br><hr width="90%"><br>	
<?php } ?>
	<form name="importTerritoire" id="importTerritoire" method="POST" action="<?php echo $importTerritoire; ?>"  enctype="multipart/form-data">
<div align="center">
<table border="0" width="60%">
  <tr>
    <td colspan="2" style="font-size: 16px; font-weight: bold" align="center">
    Importation de la liaison entre les utilisateurs et les territoires</td>
  </tr>
  <tr>
    <td colspan="2">Cette interface permet l'import en masse de
    l'association entre les utilisateurs et leur territoires associés.<br>
    L'import se fait à partir d'un fichier CSV (séparateur virgule)
    contenant deux colonnes :
    <ul>
      <li>-<b> usr_id</b> : identifiant de l'utilisateur</li>
      <li>-<b> territoire_code</b> : identifiant du territoire concerné</li>
    </ul>
    <a style="text-decoration:underline" target="_blank" href="<?php echo str_replace("/app_dev.php", "", PRO_CATALOGUE_URLBASE);?>/modeles/importTerritoire.csv">Exemple de fichier CSV</a>
    <br/><br/>
    <i><b>Note</b> : Le nettoyage des données est relatif aux identifiants d'utilisateurs présents dans le fichier.</i>
    
    </td>
  </tr>
  <tr>
    <td>Fichier à importer :</td>
    <td valign="top">
					<input id="FILE_PERIMETRE" name="FILE_PERIMETRE" type="File" accept=".csv,.txt" width="100" >
  </tr>
  <tr>
    <td colspan="2" align="center"><br>
    <input type="button" value="Importer le fichier"
      onclick="UserPerimetre_Import('importTerritoire', 'FILE_PERIMETRE', 'utilisateur/territoire')"></td>
  </tr>
  
</table>
</div>
	
	</form>
<br><hr width="90%"><br>	
	<form name="importEdition" id="importEdition" method="POST" action="<?php echo $importEdition; ?>"  enctype="multipart/form-data">
<div align="center">
<table border="0" width="60%">
  <tr>
    <td colspan="2" style="font-size: 16px; font-weight: bold" align="center">
    Importation des territoires <u>éditables</u> par utilisateurs</td>
  </tr>
  <tr>
    <td colspan="2">Cette interface permet l'import en masse des territoires éditables par utilisateurs.<br>
    L'import se fait à partir d'un fichier CSV (séparateur virgule)
    contenant deux colonnes :
    <ul>
      <li>-<b> usr_id</b> : identifiant de l'utilisateur</li>
      <li>-<b> territoire_code</b> : identifiant du territoire concerné</li>
    </ul>
    <a style="text-decoration:underline" target="_blank" href="<?php echo str_replace("/app_dev.php", "", PRO_CATALOGUE_URLBASE);?>/modeles/importTerritoire.csv">Exemple de fichier CSV</a>
    <br/><br/>
    <i><b>Note</b> : Le nettoyage des données est relatif aux identifiants d'utilisateurs présents dans le fichier.</i>
    
    </td>
  </tr>
  <tr>
    <td>Fichier à importer :</td>
    <td valign="top">
					<input id="FILE_EDITION" name="FILE_EDITION" type="File" accept=".csv,.txt" width="100" >
  </tr>
  <tr>
    <td colspan="2" align="center"><br>
    <input type="button" value="Importer le fichier"
      onclick="UserPerimetre_Import('importEdition', 'FILE_EDITION', 'utilisateur/territoire')"></td>
  </tr>
  
</table>
</div>
	
	</form>
<br><hr width="90%"><br>	
	<form name="importAlertEdition" id="importAlertEdition" method="POST" action="<?php echo $importAlertEdition; ?>"  enctype="multipart/form-data">
<div align="center">
<table border="0" width="60%">
  <tr>
    <td colspan="2" style="font-size: 16px; font-weight: bold" align="center">
    <u>Droits d'alerte sur l'édition</u> en ligne de couches cartographiques par utilisateur et territoire</td>
  </tr>
  <tr>
    <td colspan="2">Cette interface permet l'import en masse des droits d'alerte sur l'édition en ligne de couches cartographiques par utilisateur et territoire.<br>
    L'import se fait à partir d'un fichier CSV (séparateur virgule)
    contenant trois colonnes :
    <ul>
      <li>-<b> usr_id</b> : identifiant de l'utilisateur</li>
      <li>-<b> territoire_code</b> : identifiant du territoire concerné</li>
      <li>-<b> couche_id</b> : identifiant de la couche</li>
    </ul>
    <a style="text-decoration:underline" target="_blank" href="<?php echo str_replace("/app_dev.php", "", PRO_CATALOGUE_URLBASE);?>/modeles/importAlertEdition.csv">Exemple de fichier CSV</a>
    <br/><br/>
    <i><b>Note</b> : Le nettoyage des données est relatif aux identifiants d'utilisateurs présents dans le fichier.</i>
    
    </td>
  </tr>
  <tr>
    <td>Fichier à importer :</td>
    <td valign="top">
					<input id="FILE_ALERT" name="FILE_ALERT" type="File" accept=".csv,.txt" width="100" >
  </tr>
  <tr>
    <td colspan="2" align="center"><br>
    <input type="button" value="Importer le fichier"
      onclick="UserPerimetre_Import('importAlertEdition', 'FILE_ALERT', 'utilisateur/territoire/couche')"></td>
  </tr>
  
</table>
</div>
	
	</form>
	</body>
</html>

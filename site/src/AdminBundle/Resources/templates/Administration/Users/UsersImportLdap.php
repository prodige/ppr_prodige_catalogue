<?php
/**
 * Ajout d'un utilisateur dans la liste à partir de LDAP
 * @author Alkante
 */

// TODO vlc à migrer en prodige4 ?

use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;
use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;
use ProdigeCatalogue\AdminBundle\Services\LdapUser;
  
  
	
  header("Content-Type: text/html; charset=UTF-8;");
	if (isset($_GET["FILE_LDIF"])) $m_fileLdif = $_GET["FILE_LDIF"];
	if (is_null($m_fileLdif)){
		echo 'Aucun fichier d\'importation de type LDIF n\'défini.';
		echo '<br>';
		echo 'L\'opération d\'importation a été abandonnée.';
		exit;
	}
	
	$dao = new DAO($conn);
	$m_lstLdapUsers = LdapUser::getUsersFromFile($dao, $m_fileLdif);
	
	function utilisateur_integrateLdap_presenterServeur(LdapUser $p_LdapUser, $exist){
		$result = "";
		if ($exist)
			$result .= "\n<table bgcolor=\"lime\" style=\"font-size:12px;float:left;margin:10px\">";
		else
			$result .= "\n<table bgcolor=\"red\" style=\"font-size:12px;float:left;margin:10px\">";
		
		
		
		$result .= "\n<tr><td><strong>Id					</strong></td><td>:</td><td>".$p_LdapUser->GetId()."</td></tr>";
		$result .= "\n<tr><td><strong>Nom					</strong></td><td>:</td><td>".$p_LdapUser->GetNom()."</td></tr>";
		$result .= "\n<tr><td><strong>Prénom				</strong></td><td>:</td><td>".$p_LdapUser->GetPrenom()."</td></tr>";
		if (!$exist) $result .= "\n<tr><td><strong>Mail					</strong></td><td>:</td><td>".$p_LdapUser->GetMail()."</td></tr>";
		$result .= "\n<tr><td><strong>Téléphone				</strong></td><td>:</td><td>".$p_LdapUser->GetPhone1()."</td></tr>";
		//$result .= "\n<tr><td><strong>Téléphone 2			</strong></td><td>:</td><td>".$p_LdapUser->GetPhone2()."</td></tr>";
		$result .= "\n<tr><td><strong>Service				</strong></td><td>:</td><td>".$p_LdapUser->GetService()."</td></tr>";
		//$result .= "\n<tr><td><strong>Description			</strong></td><td>:</td><td>".$p_LdapUser->GetDescription()."</td></tr>";
		$tabProfilIdLDAP = $p_LdapUser->GetUserLDAPProfilId();
		if(isset($tabProfilIdLDAP)&& !empty($tabProfilIdLDAP)){
			$strIdProfil = "Identifiant(s) des profils LDAP mis à jour : ";
			foreach($tabProfilIdLDAP as $k => $profilId){
				$strIdProfil.=$profilId.",";
			}
			$strIdProfil = substr($strIdProfil,0, -1);
			$result .= "\n<tr><td><strong>Profil(s) 			</strong></td><td>:</td><td>".$strIdProfil."</td></tr>";		
		}
		if (!$exist) $result .= "\n<tr><td><strong>Mot de passe			</strong></td><td>:</td><td>*******</td></tr>";
		if (!$exist) $result .= "\n<tr><td><strong>Expiration de l'accès	</strong></td><td>:</td><td>".$p_LdapUser->GetPasswordExpire()."</td></tr>";
		$result .= "\n</table>";
		
		return $result;
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Intégration des utilisateurs LDAP</title>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
		<script>

			function UserImportLdap_updateList(){
				var l_url;
				var l_fenetre;
				l_url = "UsersList.php";
				l_fenetre = "<?php echo Ressources::$IFRAME_NAME_MAIN;?>";
				window.open(l_url, l_fenetre, "");
			}
		</script>
	</head>
	<body>
		<div align="center">
			<table border="0">
				<tr>
					<td colspan="2" style="font-size:16px;font-weight:bold">
						Importation des utilisateurs depuis
						<br>
						le fichier d'export de l'annuaire LDAP
						<br>
					</td>
				</tr>
			</table>
		</div>
		<br>
		<table bgcolor="red" style="font-size:12px;font-weight:bold">
			<tr>
				<td>Utilisateur LDAP créé dans la base des droits</td>
			</tr>
		</table>
		<table bgcolor="lime" style="font-size:12px;font-weight:bold">
			<tr>
				<td>Utilisateur LDAP mis à jour dans la base des droits</td>
			</tr>
		</table>
			<?php
				if (!is_null($m_lstLdapUsers)){
					echo '<hr width="70%">';
					for ($idxUser=0;$idxUser<count($m_lstLdapUsers);$idxUser++){
						$m_UserLdap = $m_lstLdapUsers[$idxUser];

						echo (utilisateur_integrateLdap_presenterServeur($m_UserLdap, $m_UserLdap->Existe()));
						//echo '<hr width="70%">';
						if ($m_UserLdap->Existe())
							$m_UserLdap->Update();
						else
							$m_UserLdap->Create();
					}
				}
			?>
		<br>
		<br>
		<script>
			UserImportLdap_updateList();
		</script>
	</body>
</html>
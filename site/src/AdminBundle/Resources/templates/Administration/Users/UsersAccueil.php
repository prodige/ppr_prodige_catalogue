<?php
/**
 * Accueil de la page utilisateurs (Administration des droits)
 * @author Alkante
 */
  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Modules/BO/UtilisateurVO.php");
  require_once($AdminPath."/Components/EditForm/EditForm.php");
  require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title> <?php echo Ressources::$PAGE_TITLE_MAIN; ?> </title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <script language="javascript">
          var regex = <?php echo $regex ?> ?  <?php echo $regex ?> : null;
          regex = regex ? regex :  /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,16}$/g;
          
          console.log(regex, "aA1;aA1;;;;;".match( new RegExp(regex)),  new RegExp(regex));
          
          function AdminUser_changePassword(password, expireName) {
            bHasChangedPwd = true;

            var expire = document.getElementById('ORDINAL_' + expireName);
            var hier = new Date();
            var btns = document.getElementsByTagName("input");
            var valid;

            for(var i = 0; (i < btns.length) && (!valid); i++) {
              if((btns[i].type == 'button') && (btns[i].value == '<?php echo Ressources::$EDITFORM_BTN_VALIDATE;?>')) {
                valid = btns[i];
              }
            }
            if(password) {
              if(AdminUser_validPassword(password)) {
                if(valid) {
                  valid.disabled = false;
                }
                if(expire) {
                  expire.disabled = false;
                  expire.value = '<?php echo date("Y-m-d"); ?>';
                }
              }
              else {
                if(valid) {
                  valid.disabled = true;
                }
              }
            }
          }

          function	AdminUser_validPassword(password) {
            valid = true;
            msg = "";
            if(!password || !(password.value).match( new RegExp(regex)) ) {
              msg += "Le mot de passe doit comporter au minimum 12 caractères, au moins un chiffre, une majuscule, une minuscule et un caractère spécial !\n";
              valid = false;
            }

            if(!valid) {
              alert(msg);
              password.value='';
            }
            return valid;
          }
        </script>
  </head>

  <body style="margin:0px;">
      <?php
        // set a default action
        if(!isset($_GET["Action"]) ) {
            $Action = Ressources::$ACTION_OPEN_USERS_ACCUEIL;
        }
        else {
            $Action = intval($_GET["Action"]);
        }

        // set a default ID
        if(!isset($_GET["Id"])) {
            $PK = -1;
        }
        else {
            $PK = intval( $_GET["Id"] );
        }
        if($PK==-1) {
            exit;
        }

        //$dao = new DAO();
        $dao = new DAO($conn, 'catalogue');

        $sql = "select metadata.id from utilisateur inner join public.users on utilisateur.usr_id = users.username inner join public.metadata on metadata.owner = users.id where pk_utilisateur =:pk_utilisateur ";
        $rs = $dao->BuildResultSet($sql, array("pk_utilisateur" => $PK));
        //user is owner of metadata, can't delete it
        $bCantDelete = false;
        if($rs->GetNbRows()>0){
            $bCantDelete = true;
        }
        
        
        $utilisateurVO = new UtilisateurVO();
        $utilisateurVO->setDao($dao);

        $accessRights = new AccessRights();

        $ordinalList = array();
        $ordinalListDisp = array();
        $ordinalType = array();
        $ordinalSize = array();
        $ordinalReadOnly = array();
        $FieldOnChangeEvent = array();
        
        $bReadOnly = false;
        if(defined("PRO_INCLUDED") && PRO_INCLUDED) {
            $bReadOnly = true;
        }
        else {
            if(isset($_SESSION["userIdInternet"])) { //Condition if added by hismail
                if($PK == $_SESSION["userIdInternet"]) {
                    $bReadOnly = true;
                }
            }
            else {
                $bReadOnly = false;
            }
        }

        $ordinalList[]= UtilisateurVO::$USR_ID;
        $ordinalListDisp[]= "Identifiant *:";
        $ordinalType[]= EditForm::$TYPE_TEXT;
        $ordinalSize[]= 50;
        $ordinalReadOnly[]= ($bReadOnly? true : !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_ID, $PK));
        $FieldOnChangeEvent[]= "";

        $ordinalList[]= UtilisateurVO::$USR_NOM;
        $ordinalListDisp[]= "Nom *:";
        $ordinalType[]= EditForm::$TYPE_TEXT;
        $ordinalSize[]= 30;
        $ordinalReadOnly[]= ($bReadOnly ? true : !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_NOM, $PK));
        $FieldOnChangeEvent[]= "";

        $ordinalList[]= UtilisateurVO::$USR_PRENOM;
        $ordinalListDisp[]= "Prénom *:";
        $ordinalType[]= EditForm::$TYPE_TEXT;
        $ordinalSize[]= 30;
        $ordinalReadOnly[]= ($bReadOnly ? true : !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_PRENOM, $PK));
        $FieldOnChangeEvent[]= "";

        $ordinalList[]= UtilisateurVO::$USR_SERVICE;
        $ordinalListDisp[]= "Service :";
        $ordinalType[]= EditForm::$TYPE_TEXT;
        $ordinalSize[]= 30;
        $ordinalReadOnly[]= ($bReadOnly ? true : !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_SERVICE, $PK));
        $FieldOnChangeEvent[]= "";

        $ordinalList[]= UtilisateurVO::$USR_TELEPHONE;
        $ordinalListDisp[]= "Téléphone 1 :";
        $ordinalType[]= EditForm::$TYPE_TEXT;
        $ordinalSize[]= 12;
        $ordinalReadOnly[]= ($bReadOnly ? true : !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_TELEPHONE, $PK));
        $FieldOnChangeEvent[]= "";

        $ordinalList[]= UtilisateurVO::$USR_TELEPHONE2;
        $ordinalListDisp[]= "Téléphone 2 :";
        $ordinalType[]= EditForm::$TYPE_TEXT;
        $ordinalSize[]= 12;
        $ordinalReadOnly[]= ($bReadOnly ? true : !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_TELEPHONE2, $PK));
        $FieldOnChangeEvent[]= "";

        $ordinalList[]= UtilisateurVO::$USR_EMAIL;
        $ordinalListDisp[]= "Mél *:";
        $ordinalType[]= EditForm::$TYPE_TEXT;
        $ordinalSize[]= 50;
        $ordinalReadOnly[]= ($bReadOnly ? true : !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_EMAIL, $PK));
        $FieldOnChangeEvent[]= "";

        if($accessRights->CanSeePassword($PK) && (!defined("PRO_INCLUDED") || !PRO_INCLUDED)) {
            $ordinalList[]= UtilisateurVO::$USR_PASSWORD;
            $ordinalListDisp[]= "Mot de Passe *:";
            $ordinalType[]= EditForm::$TYPE_TEXT_PASSWORD;
            $ordinalSize[]= 12;
            $ordinalReadOnly[]= !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_PASSWORD, $PK);
            $FieldOnChangeEvent[]= "AdminUser_changePassword(this, '".UtilisateurVO::$USR_PWD_EXPIRE."')";
        }

        $ordinalList[]= UtilisateurVO::$USR_PWD_EXPIRE;
        $ordinalListDisp[]= "Expiration du mot de passe *:";
        $ordinalType[]= EditForm::$TYPE_HIDDEN;
        $ordinalSize[]= 10;
        $ordinalReadOnly[]= !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_PWD_EXPIRE, $PK);
        $FieldOnChangeEvent[]= "";

        $ordinalList[]= UtilisateurVO::$USR_SIGNATURE;
        $ordinalListDisp[]= "Signature :";
        $ordinalType[]= EditForm::$TYPE_TEXT;
        $ordinalSize[]= 50;
        $ordinalReadOnly[]= ($bReadOnly ? true : !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_SIGNATURE, $PK));
        $FieldOnChangeEvent[]= "";

        $ordinalList[]= UtilisateurVO::$USR_GENERIC;
        $ordinalListDisp[]= "Compte générique :";
        $ordinalType[]= EditForm::$TYPE_CB;
        $ordinalSize[]= 0;
        $ordinalReadOnly[]= ($bReadOnly ? true : !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_GENERIC, $PK));
        $FieldOnChangeEvent[]= "";

        $ordinalList[]= UtilisateurVO::$USR_LDAP;
        $ordinalListDisp[]= "Synchronisation LDAP :";
        $ordinalType[]= EditForm::$TYPE_CB;
        $ordinalSize[]= 0;
        $ordinalReadOnly[]= ($bReadOnly ? true : !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_LDAP, $PK));
        $FieldOnChangeEvent[]= "";

        $ordinalList[]= UtilisateurVO::$USR_ACCOUNT_EXPIRE;
        $ordinalListDisp[]= "Expiration du compte :";
        $ordinalType[]= EditForm::$TYPE_TEXT;
        $ordinalSize[]= 10;
        $ordinalReadOnly[]= !$accessRights->CanUpdate($utilisateurVO, UtilisateurVO::$USR_ACCOUNT_EXPIRE, $PK);
        $FieldOnChangeEvent[]= "";
        
        print('<br>');
        
        
        $editForm = new EditForm("EditFormUsers",
            $utilisateurVO,
            UtilisateurVO::$PK_UTILISATEUR,
            $PK,
            $ordinalList,
            $ordinalListDisp,
            $ordinalType,
            $ordinalSize,
            $ordinalReadOnly,
            '' /* TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Users/UsersAccueil.php"*/,
            $Action,
            $FieldOnChangeEvent,
            $bReadOnly,
            $bReadOnly || $bCantDelete,
            array(),
            $submitUrl
        );

        if($bCantDelete){
            print('<div style="text-align:left;margin:10px;"><p><i>Cet utilisateur est propriétaire de métadonnées, il ne peut être supprimé.<br>Pour le supprimer, il est nécessaire de changer l\'affectation des métadonnées au préalable.</i></p></div>');
        }
    //surcharge de la fonction validation pour inclure les contrôles.
    ?>

  <script>

    <?php echo "var bHasChangedPwd = false;" ?>;

    function EditFormUsersValidate_onclick() {
      var form = document.getElementById("EditFormUsers");
      if(document.getElementById("ORDINAL_<?php echo UtilisateurVO::$USR_ID;?>").value == ""
        || document.getElementById("ORDINAL_<?php echo UtilisateurVO::$USR_NOM;?>").value == ""
        || document.getElementById("ORDINAL_<?php echo UtilisateurVO::$USR_PRENOM;?>").value == ""
        || document.getElementById("ORDINAL_<?php echo UtilisateurVO::$USR_EMAIL;?>").value == ""
        || document.getElementById("ORDINAL_<?php echo UtilisateurVO::$USR_PASSWORD;?>").value == ""
        || document.getElementById("ORDINAL_<?php echo UtilisateurVO::$USR_PWD_EXPIRE;?>").value == ""
      ) {
        Ext.Msg.alert("Fiche utilisateur", "Les champs marqués d'un astérisque sont obligatoires");
        return;
      }

      if( !bHasChangedPwd ) {
        // disable password input if it has not changed
        document.getElementById("ORDINAL_<?php echo UtilisateurVO::$USR_PASSWORD;?>").disabled = "disabled";
        // remove password ordinal index from submitted ordinals
        ords = document.getElementById("Ordinals").value.split(';');
        ords.splice(ords.indexOf('<?php echo UtilisateurVO::$USR_PASSWORD;?>'), 1);
        document.getElementById("Ordinals").value = ords.join(';');
      }

      var inputId = document.getElementById("Id");
      var inputAction = document.getElementById("Action");
    
      form.action = "<?php echo $submitUrl; ?>";
      form.submit();
    }

    function EditFormUsersDelete_onclick() {

      var form = document.getElementById("EditFormUsers");
      var inputId = document.getElementById("Id");
      var inputAction = document.getElementById("Action");

      inputAction.value = '<?php echo Ressources::$EDITFORM_DELETE;?>'; 
      form.action = "<?php echo $submitUrl; ?>";

      if(confirm("Êtes-vous sûr de vouloir supprimer cet enregistrement ?")) {
        parent.Ext.Ajax.request({
          url : '<?php echo $delUserUrl; ?>',
          method : 'GET',
          params : {
            'userId' : <?php  echo $PK;?>
          },
          success: function(result) {
            form.submit();
          },
          failure: function(result) {
            parent.Ext.MessageBox.alert('Failed', 'erreur d\'enregistrement de la table'); 
          }
        });
      }
    }

  </script>

 </body>

</html>
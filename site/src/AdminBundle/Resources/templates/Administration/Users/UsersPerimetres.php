<?php
/**
 * Onglet périmètres dans la gestion des utilisateurs
 * @author Alkante
 */
  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Modules/BO/UserAccedePerimetreVO.php");
  require_once($AdminPath."/Modules/BO/PerimetreVO.php");
  require_once($AdminPath."/Components/RelationList/RelationList.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UsrAccedePerimetreVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\PerimetreVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php   
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_PROFILES_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if ( $PK==-1 )
  {
    exit;
  }

  // set a default ID
  if ( !isset($_GET["RR"]) )
  {
    $RELATIONLIST_RESULT = -1;
  }
  else
  {
    $RELATIONLIST_RESULT = $_GET["RR"]; 
  }

  $dao = new DAO($conn, 'catalogue');

  if ( $RELATIONLIST_RESULT != -1 )
  {
    switch( $Action )
    {
      case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
        $ordinalList = array();
        $valueList = array();
        $ordinalList[]=UsrAccedePerimetreVO::$USRPERIM_FK_UTILISATEUR;
        $ordinalList[]=UsrAccedePerimetreVO::$USRPERIM_FK_PERIMETRE;
        $valueList[]=$PK;
        $valueList[]=$RELATIONLIST_RESULT;

        $grpAccedeSSDomVO_RL = new UsrAccedePerimetreVO();
        $grpAccedeSSDomVO_RL->setDao($dao);
        $grpAccedeSSDomVO_RL->InsertValues( $ordinalList, $valueList );
        $grpAccedeSSDomVO_RL->Commit();
      break;
      
      case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
        $UsrAccedePerimetreVO_RL = new UsrAccedePerimetreVO();
        $UsrAccedePerimetreVO_RL->setDao($dao);
        $UsrAccedePerimetreVO_RL->DeleteRow( UsrAccedePerimetreVO::$PK_USR_ACCEDE_PERIMETRE, $RELATIONLIST_RESULT );
        $UsrAccedePerimetreVO_RL->Commit();
      break;
    }
    AlertSaveController::AlertSaveDone("", false);
  }
  
  // #################################################################

  $UsrAccedePerimetreVO = new UsrAccedePerimetreVO();
  $UsrAccedePerimetreVO->setDao($dao);
  $PerimetreVO = new PerimetreVO();
  $PerimetreVO->setDao($dao);

  $UsrAccedePerimetreVO2 = new UsrAccedePerimetreVO();
  $UsrAccedePerimetreVO2->setDao($dao);
  $UsrAccedePerimetreVO2->AddRestriction( UsrAccedePerimetreVO::$USRPERIM_FK_UTILISATEUR, $PK );
  $UsrAccedePerimetreVO2->AddOnlyKeyProjection( UsrAccedePerimetreVO::$USRPERIM_FK_PERIMETRE );

  $PerimetreVO->AddNotInRestriction( PerimetreVO::$PK_PERIMETRE_ID, $UsrAccedePerimetreVO2 );

  $relationList = new RelationList("RelationListProfilePerimetre",
                   Ressources::$RELATIONLIST_PROFILES_PERIMETRE_RELATION,
                   $UsrAccedePerimetreVO,
                   UsrAccedePerimetreVO::$PK_USR_ACCEDE_PERIMETRE,
                   UsrAccedePerimetreVO::$USRACCPERIMETRE_PERIMETRE_NOM,
                   UsrAccedePerimetreVO::$USRPERIM_FK_UTILISATEUR,
                   $PK,
                   UsrAccedePerimetreVO::$USRPERIM_FK_PERIMETRE,
                   $PerimetreVO,
                   Ressources::$RELATIONLIST_PROFILES_PERIMETRE_TABLE,
                   PerimetreVO::$PK_PERIMETRE_ID,
                   PerimetreVO::$PERIMETRE_NOM,
                   '' /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Users/UsersPerimetres.php"*/,
                   Ressources::$IFRAME_NAME_MAIN,
                   $this
                   );

?>
  </body>
</html>
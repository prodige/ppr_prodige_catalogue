<?php

/**
 * Author Alkante
 * Service d'import de liaison territoire / utilisateur
 *
 */
use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;
use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;
use Symfony\Component\Process\Process;
$dao = new DAO($conn);

set_time_limit(0);
$file = urldecode($_GET["CSV_FILE"]);

$DELIMITER = ",";

$row = 1;
$strMsg="";
if (($handle = fopen($file, "r")) !== FALSE) {
    $tempTable = uniqid("temp_import_usr_alerte_perimetre_edition_");
    $copyFilename = $file.".temp";
    $copyFile = fopen($copyFilename, "w");
  $bCreated = false;
	while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
		$num = count($data);
		if($num!="3"){
      $strMsg = "Le fichier doit contenir trois colonnes";
      break;
    }
		 
		if($row == 1){
			$territoire_codeIdx = array_search("territoire_code", $data);
			$usr_idIdx = array_search("usr_id", $data);
			$couche_idIdx = array_search("couche_id", $data);

			if($territoire_codeIdx===false || $usr_idIdx===false || $couche_idIdx===false){
				$strMsg = "Le fichier doit contenir les colonnes perimetre_code, couche_id et usr_id";
        break;
			} 
			$data[$territoire_codeIdx] = "perimetre_code";
			$data[$couche_idIdx] = "couchd_id";
      fputcsv($copyFile, array_merge(array("rownumber"), $data), $DELIMITER);
      
			$data[$territoire_codeIdx] .= " text";
			$data[$usr_idIdx]          .= " text";
			$data[$couche_idIdx]       .= " text";
      $bCreated = $dao->execute("create table ".$tempTable." (rownumber int, ".implode(", ", $data).")");
    
		}else{
      fputcsv($copyFile, array_merge(array($row), $data), $DELIMITER);
		}
		$row++;
	}
	fclose($handle);
	fclose($copyFile);
	
	if ( $bCreated ){
    	$connectionParams = $dao->getConnection()->getParams();
    	unset($connectionParams["driver"]);
    	unset($connectionParams["charset"]);
    	$res = array();
    	$cmd = 'psql "'.http_build_query($connectionParams, null, " ").'" -c "\\copy catalogue.'.$tempTable.' from \''.$copyFilename.'\' delimiter as \''.$DELIMITER.'\' csv header"';
    	$process = Process::fromShellCommandline($cmd);
        $process->run(); 
        $res = explode (" ", $process->getOutput()); 
        //exec($cmd, $res);
    	
    	@unlink($copyFilename);
    	$strMsg .= treatData($dao, $tempTable); 
    	$dao->execute("drop table if exists ".$tempTable);
	} else {
	    $strMsg = "Impossible d'ouvrir le fichier source.";
	}
}else{
	$strMsg = "Impossible d'ouvrir le fichier source.";
}
echo "{success:".($strMsg!="" ? "false" : "true").", msg:\"".$strMsg."\"}";

/**
 * Traitement d'une ligne du fichier csv
 * @param $territoire_code
 * @param $usr_id
 * @param $row
 * @return message d'erreur
 */
function treatData(DAO $dao, $tempTable){
  $strMsg = array();
  
	//Rejet des lignes dont l'identifiant de territoire n'est pas connu
	$query = "select rownumber, perimetre_code from ".$tempTable." where perimetre_code not in (select perimetre_code from perimetre)";
	$rs = $dao->getConnection()->fetchAllAssociative($query);
	foreach($rs as $reject){
	  $strMsg[] = "L'identifiant de territoire ".$reject["perimetre_code"]." est absent dans la table des territoires. La ligne ".$reject["rownumber"]." est rejetée.";
	}
	$query = "delete from ".$tempTable." where perimetre_code not in (select perimetre_code from perimetre)";
	$rs = $dao->getConnection()->fetchAllAssociative($query);
	
	
	//Rejet des lignes dont l'identifiant d'utilisateur n'est pas connu
	$query = "select rownumber, usr_id from ".$tempTable." where usr_id not in (select usr_id from utilisateur)";
	$rs = $dao->getConnection()->fetchAllAssociative($query);
	foreach($rs as $reject){
	  $strMsg[] = "L'identifiant d'utilisateur ".$reject["usr_id"]." est absent dans la table des utilisateurs. La ligne ".$reject["rownumber"]." est rejetée.";
	}
	$query = "delete from ".$tempTable." where usr_id not in (select usr_id from utilisateur)";
	$rs = $dao->getConnection()->fetchAllAssociative($query);
	
	//Rejet des lignes dont l'identifiant de couche n'est pas connu
	$query = "select rownumber, couchd_id from ".$tempTable." where couchd_id not in (select couchd_id from couche_donnees)";
	$rs = $dao->getConnection()->fetchAllAssociative($query);
	foreach($rs as $reject){
	  $strMsg[] = "L'identifiant de couche ".$reject["usr_id"]." est absent dans la table des couches. La ligne ".$reject["rownumber"]." est rejetée.";
	}
	$query = "delete from ".$tempTable." where couchd_id not in (select couchd_id from couche_donnees)";
	$rs = $dao->getConnection()->fetchAllAssociative($query);
	
	
	
  //suppression de l'association puis recréation
  $query = "delete from usr_alerte_perimetre_edition ".
           " where usralert_fk_utilisateur in ( select pk_utilisateur from ".$tempTable." inner join utilisateur using(usr_id) ) "
           ;
  $rs = $dao->Execute($query);
  
  $query = "insert into usr_alerte_perimetre_edition (usralert_fk_utilisateur, usralert_fk_perimetre, usralert_fk_couchedonnees) ".
           " select pk_utilisateur, pk_perimetre_id, pk_couche_donnees ".
           " from ".$tempTable.
           " inner join utilisateur using(usr_id) ".
           " inner join perimetre using(perimetre_code) ".
           " inner join couche_donnees using(couchd_id) "
           ;
  $rs = $dao->Execute($query);
  $rs = $dao->Execute("COMMIT");
	return implode("<br>", $strMsg);
}

?>
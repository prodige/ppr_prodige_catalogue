<?php
/**
 * Liste des utilisateurs (gauche) dans l'adminsitration des utilisateurs (administration des droits)
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/UtilisateurVO.php");
	require_once($AdminPath."/Components/SelectList/SelectList.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;

  header("Content-type: application/xml");
  echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php 
  $dao = new DAO($conn, 'catalogue');
	$utilisateurVO = new UtilisateurVO();
	$utilisateurVO->setDao($dao);

	$selectList = new SelectList(	 "SelectListUtilisateurVO",
									 $utilisateurVO,
									 UtilisateurVO::$PK_UTILISATEUR,
									 UtilisateurVO::$USR_ID,
									 Ressources::$IFRAME_NAME_MAIN,
									 '' /*TODO hismail - argument ne sera pas utilisé ... "Administration/Administration/Users/UsersDetail.php"*/,
									 Ressources::$SELECT_LIST_SIZE_X,
									 Ressources::$SELECT_LIST_SIZE_Y
									 );

  
?>

  </body>
</html>
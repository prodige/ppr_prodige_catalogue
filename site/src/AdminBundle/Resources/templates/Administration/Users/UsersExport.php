<?php
  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

  $Action = Ressources::$LIST_MODIFY_ACTION;
  $PK = -1;
  
  if (isset($_GET["Action"])) $Action = intval($_GET["Action"]);
  if (isset($_GET["Id"]))   $PK   = intval($_GET["Id"]);  
  //if ($PK == -1)  exit;
  
  
  //récupération des profils
  
  
  $dao = new DAO($conn);
  if ($dao)
  { 
    $sqlStmt = "select  pk_groupe_profil, grp_nom from groupe_profil";
    $rs = $dao->BuildResultSet($sqlStmt);
    
  ?>
  <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
  <html>
    <head>
      <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <script>
      function User_Export(){
        document.formExport.submit();
      }
      </script>
    </head>
    <body style="margin:0px;">
    <form name="formExport" target ="<?php echo Ressources::$IFRAME_NAME_MAIN;?>" action="<?php echo $route; ?>" method="POST">
    <div align="center">
      <table class="administration_gen" border="0">
        <tr>
          <td colspan="2" style="font-size:16px;font-weight:bold">
            Export des utilisateurs
            <br>
          </td>
        </tr>
        <tr>
          <th>Profil</th>
          <td>  
            <select id="EXPORT_PROFIL" name="EXPORT_PROFIL" value="-1">
            <option value='-1'>Tous</option>
            <?php 
            for ($rs->First(); !$rs->EOF(); $rs->Next())
            {
              echo "<option value='".$rs->Read(0)."'>".$rs->Read(1)."</option>";
            }
            ?>
            </select>
          </td>
        </tr>
        <tr>
          <th>Type d'export</th>
          <td>
            <select id="EXPORT_TYPE" name="EXPORT_TYPE" value="csv">
            <option value='csv'>CSV</option>
            <option value='ldif'>LDIF</option>
            </select>
          </td>  
        </tr>
        <tr>
        <td colspan="2" align="center">
          <br>
          <input id="FILE_EXPORT" name="FILE_EXPORT" type="button" value="Exporter les utilisateurs" onclick="User_Export()">
        </td>
      </tr>
      </table>
    </div>
    </form>
    </body>
  </html>
<?php 
  }?>
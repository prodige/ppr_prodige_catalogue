<?php
/**
 * Onglet profils dans la gestion des utilisateurs
 * @author Alkante
 */
	/*require_once($AdminPath."/Ressources/Administration/Ressources.php");
	require_once($AdminPath."/DAO/DAO/DAO.php");
	require_once($AdminPath."/Modules/BO/GroupeProfilVO.php");
	require_once($AdminPath."/Modules/BO/GrpRegroupeUsrVO.php");
	require_once($AdminPath."/Components/RelationList/RelationList.php");
	require_once($AdminPath."/Components/SelectList/SelectList.php");	
	require_once($AdminPath."/Modules/BO/GroupeProfilVO.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GroupeProfilVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GrpRegroupeUsrVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 	
	// set a default action
	if ( !isset($_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_USERS_ACCUEIL;
	}
	else
	{
		$Action = $_GET["Action"];
	}
	
	// set a default ID
	if ( !isset($_GET["Id"]) )
	{
		$PK = -1;
	}
	else
	{
		$PK = intval( $_GET["Id"] );	
	}
	
	if ( $PK==-1 )
	{
		exit;
	}

	// set a default ID
	if ( !isset($_GET["RR"]) )
	{
		$RELATIONLIST_RESULT = -1;
	}
	else
	{
		$RELATIONLIST_RESULT = $_GET["RR"];	
	}

	$dao = new DAO($conn, 'catalogue');

	if ( $RELATIONLIST_RESULT != -1 )
	{
		switch( $Action )
		{
			case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
				$ordinalList = array();
				$valueList = array();
				$ordinalList[]=GrpRegroupeUsrVO::$GRPUSR_FK_GROUPE_PROFIL;
				$ordinalList[]=GrpRegroupeUsrVO::$GRPUSR_FK_UTILISATEUR;
				$valueList[]=$RELATIONLIST_RESULT;
				$valueList[]=$PK;

				$grpRegroupeUsrVO_RL = new GrpRegroupeUsrVO();
				$grpRegroupeUsrVO_RL->setDao($dao);
				$grpRegroupeUsrVO_RL->InsertValues( $ordinalList, $valueList );
				$grpRegroupeUsrVO_RL->Commit();
 			break;
			
			case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
				$grpRegroupeUsrVO_RL = new GrpRegroupeUsrVO();
				$grpRegroupeUsrVO_RL->setDao($dao);
				$grpRegroupeUsrVO_RL->DeleteRow( GrpRegroupeUsrVO::$PK_GRP_REGROUPE_USR, $RELATIONLIST_RESULT );
				$grpRegroupeUsrVO_RL->Commit();
			break;
		}
        AlertSaveController::AlertSaveDone("", false);
	}

	// #################################################################

	$grpRegroupeUsrVO = new GrpRegroupeUsrVO();
	$grpRegroupeUsrVO->setDao($dao);
	$groupeProfilVO = new GroupeProfilVO();
	$groupeProfilVO->setDao($dao);
	
	$grpRegroupeUsrVO2 = new GrpRegroupeUsrVO();
	$grpRegroupeUsrVO2->setDao($dao);
	$grpRegroupeUsrVO2->AddRestriction( GrpRegroupeUsrVO::$GRPUSR_FK_UTILISATEUR, $PK );
	$grpRegroupeUsrVO2->AddOnlyKeyProjection( GrpRegroupeUsrVO::$GRPUSR_FK_GROUPE_PROFIL );
	$groupeProfilVO->AddNotInRestriction( GroupeProfilVO::$PK_GROUPE_PROFIL, $grpRegroupeUsrVO2 );
	
	$relationList = new RelationList("RelationListProfileUser",
									 Ressources::$RELATIONLIST_USERS_PROFILES_RELATION,
									 $grpRegroupeUsrVO,
									 GrpRegroupeUsrVO::$PK_GRP_REGROUPE_USR,
									 GrpRegroupeUsrVO::$GRPUSR_GRP_ID,
									 GrpRegroupeUsrVO::$GRPUSR_FK_UTILISATEUR,
									 $PK,
									 GrpRegroupeUsrVO::$GRPUSR_FK_GROUPE_PROFIL,
									 $groupeProfilVO,
									 Ressources::$RELATIONLIST_USERS_PROFILES_TABLE,
									 GroupeProfilVO::$PK_GROUPE_PROFIL,
									 GroupeProfilVO::$GRP_ID,
									 '' /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Users/UsersProfiles.php"*/,
									 Ressources::$IFRAME_NAME_MAIN,
	                                 $this,
                   true,
                   false,
                   true,
                   "",
                   ( defined("PRO_INCLUDED") && PRO_INCLUDED ? true : false ) // formulaire un lecture seule si intégration de Prodige dans une application externe => profils non modifiables
									 );
	
?>
	</body>
</html>
<?php

use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Services\LdapUtils;

use Prodige\ProdigeBundle\Services\GeonetworkInterface;

/**
 * Author : Alkante
 * Service de synchronisation des  Utilisateurs vers le ldap
 * se base sur les données postées au service writeBOV.php
 * 
 */
// instance de ldapUtils
$ldapUtils = LdapUtils::getInstance(LDAP_HOST, LDAP_PORT, LDAP_DN_ADMIN, LDAP_PASSWORD, LDAP_BASE_DN);
$found = false;
$loginValue = null;
if (!$delete) {
    $dao = new DAO($conn, 'catalogue');
    $sqlUser = "SELECT CONCAT(usr_prenom, ' ', usr_nom) as cn, "
            . "usr_email as mail, "
            . "usr_id as uid ,"
            . "usr_id as o ,"
            . "usr_nom as sn , "
            . "usr_description as description , "
            . "usr_prenom as gn , "
            . "pk_utilisateur as uidnumber , "
            . "usr_password as userpassword , "
            . "usr_telephone as telephonenumber "
            . " from utilisateur where pk_utilisateur=$pk";

    $rs = $dao->BuildResultSet($sqlUser);
    for ($rs->First(); !$rs->EOF(); $rs->Next()) {
        $found = true;
        $tabUser['cn'] = $rs->Read(0);
        $tabUser['mail'] = $rs->Read(1);
        $tabUser['uid'] = $rs->Read(2);
        $tabUser['o'] = $rs->Read(3);
        $loginValue = $ldapUtils->decode_data($rs->Read(3));
        $tabUser['sn'] = $rs->Read(4);
        $tabUser['description'] = $rs->Read(5);
        $tabUser['gn'] = $rs->Read(6);
        $tabUser['uidnumber'] = $rs->Read(7);
        $tabUser['userpassword'] = $rs->Read(8);
        $tabUser['telephonenumber'] = $rs->Read(9);
    }
    
    // Sous domaines
    $sqlSousDomaine = "select sd.ssdom_id as alkMemberOfGroup "
            . "from administrateurs_sous_domaine as asd, sous_domaine as sd "
            . "where pk_utilisateur = $pk and asd.pk_sous_domaine = sd.pk_sous_domaine";
    $rs = $dao->BuildResultSet($sqlSousDomaine);
    $tabUser['alkmemberofgroup'] = [];
    for ($rs->First(); !$rs->EOF(); $rs->Next()) {
        $tabUser['alkmemberofgroup'][] = $rs->Read(0);
    }

    //Profile
    $tabUser['alkmemberofprofile'] = 'RegisteredUser';
    // Si l'utilisateur est Reviewer on le met
    $sql = "select 'Reviewer' as traitement from traitements_utilisateur where pk_utilisateur = $pk and trt_id='CMS'";
    $rs = $dao->BuildResultSet($sql);
    for ($rs->First(); !$rs->EOF(); $rs->Next()) {
        $tabUser['alkmemberofprofile'] = $rs->Read(0);
    }

    $sql = "select 'Administrator' as traitement from traitements_utilisateur where pk_utilisateur = $pk and trt_id='ADMINISTRATION'";
    $rs = $dao->BuildResultSet($sql);
    for ($rs->First(); !$rs->EOF(); $rs->Next()) {
        $tabUser['alkmemberofprofile'] = $rs->Read(0);
    }
    $tabUser = $ldapUtils->decode_data($tabUser);
}

if ($ldapUtils->userExistFromPk($pk)) {
    
    if ($delete) {
        //delete from geonetwork   
        $dao = new DAO($conn, 'catalogue');
        
        //get uid from ldap since in catalogue database it's deleted
        $userlogin = $ldapUtils->getUidFromPk($pk);
        
        $sql = "select users.id from public.users where users.username =:username";
        $rs = $dao->BuildResultSet($sql, array("username" => $userlogin));
        for ($rs->First(); !$rs->EOF(); $rs->Next()) {
            $userId = $rs->Read(0);
            $headers = array(CURLOPT_HTTPHEADER, array(
                        'Accept: application/json, text/plain, */*'
            ));
            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/', $headers);
        
            $jsonResp = $geonetwork->delete('api/users/'.$userId, false);
        }
        
        // delete from ldap
        $ldapUtils->deleteUser($pk);
        
    } else {
        if ($loginValue && !$ldapUtils->userExist($loginValue)) {
            // Renommer
            $ldapUtils->renameUser($pk, $loginValue);
        }

        $headers = array(CURLOPT_HTTPHEADER, array(
            'Accept: application/json, text/plain, */*'
        ));
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/', $headers);
        
        //list groupIds
        $groupArray = array();
        $sql = "select id from public.groups where name = ANY(:name)";

        $rs = $dao->BuildResultSet($sql, array("name" => '{' . implode(', ', $tabUser["alkmemberofgroup"]) . '}' ));
        for ($rs->First(); !$rs->EOF(); $rs->Next()) {
            $groupArray[] = $rs->Read(0);
        }

        $params = array(
            "emailAddresses" => array($tabUser["mail"]),
            "username" => $tabUser["uid"],
            "name" => $tabUser["gn"],
            "enabled" => true,
            "surname" => $tabUser["sn"],
            "profile" => $tabUser["alkmemberofprofile"],
            "groupsRegisteredUser" => ($tabUser["alkmemberofprofile"] == "RegisteredUser" ? $groupArray : array()),
            "groupsEditor" => array(),
            "groupsReviewer" => ($tabUser["alkmemberofprofile"] == "Reviewer" ? $groupArray : array()),
            "groupsUserAdmin" => array()
        );
        
        $sql = "select id from public.users where username = :name";
        $rs = $dao->BuildResultSet($sql, array("name" => $tabUser["uid"] ));
        //TODO when login is modified this can't be good way to do
        for ($rs->First(); !$rs->EOF(); $rs->Next()) {
            $userId = $rs->Read(0);
            $jsonResp = $geonetwork->put('api/users/'.$userId, $params, false, true);
        }
        
        // Mise à jour
        $ldapUtils->editUser($tabUser);
        
    }
} else {
    // Create
    if ($found) {
        
        $headers = array(CURLOPT_HTTPHEADER, array(
            'Accept: application/json, text/plain, */*'
        ));
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/', $headers);
        
        //list groupIds
        $groupArray = array();
        $sql = "select id from public.groups where name = ANY(:name)";
        
        $rs = $dao->BuildResultSet($sql, array("name" => '{' . implode(', ', $tabUser["alkmemberofgroup"]) . '}' ));
        for ($rs->First(); !$rs->EOF(); $rs->Next()) {
            $groupArray[] = $rs->Read(0);
        }
        
        $params = array(
            "password" =>"",
            "emailAddresses" => array($tabUser["mail"]),
            "username" => $tabUser["uid"],
            "name" => $tabUser["gn"],
            "enabled" => true,
            "surname" => $tabUser["sn"],
            "profile" => $tabUser["alkmemberofprofile"],
            "groupsRegisteredUser" => ($tabUser["alkmemberofprofile"] == "RegisteredUser" ? $groupArray : array()),
            "groupsEditor" => array(),
            "groupsReviewer" => ($tabUser["alkmemberofprofile"] == "Reviewer" ? $groupArray : array()),
            "groupsUserAdmin" => array()
        );
        
        $jsonResp = $geonetwork->put('api/users', $params, false, true);
        
        //{"id":"","username":"login","password":"test123","name":"prenom","surname":"nom","profile":"Editor","addresses":[{"address":"","city":"","state":"","zip":"","country":""}],"emailAddresses":["test@test.fr"],"organisation":"","enabled":true,"groupsRegisteredUser":[],"groupsEditor":["2"],"groupsReviewer":[],"groupsUserAdmin":[]}
        
        $ldapUtils->addUser($tabUser);
    }
    // Sinon ce n'est pas un id correct d'utilisateur
}
?>
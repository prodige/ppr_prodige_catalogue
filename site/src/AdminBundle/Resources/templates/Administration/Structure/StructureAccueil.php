<?php
/**
 * Accueil de la page rubriques d'aide (Administration des droits)
 * @author Alkante
 */

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\StructureVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
    <?php   
      // set a default action
      $Action = !isset($_GET["Action"]) ? Ressources::$ACTION_OPEN_COMPETENCES_ACCUEIL : $_GET["Action"] ;
      
      // set a default ID
      $id = !isset($_GET["Id"]) ? -1 : intval( $_GET["Id"] );
      
      if ( $id === -1 ){
        exit();
      }
      
      $submitUrl = $this->generateUrl('catalogue_administration_submit_form');
      $conn    = $this->getCatalogueConnection('catalogue');
      $dao = new DAO($conn, 'catalogue');

      $accessRights = new AccessRights( );

      $structureVO = new StructureVO();
      $structureVO->setDao($dao);

      $ordinalList = array();
      $ordinalListDisp = array();
      $ordinalType = array();
      $ordinalSize = array();   
      $ordinalReadOnly = array(); 
      $bReadOnly = false;
      if(defined("PRO_INCLUDED") && PRO_INCLUDED) {
          $bReadOnly = true;
      }
      // édition du nom
      $ordinalList[] = StructureVO::NAME;
      $ordinalListDisp[] ="Nom :";
      $ordinalType[] = EditForm::$TYPE_TEXT;
      $ordinalSize[] = 79;
      $ordinalReadOnly[] = ($bReadOnly ? true : !$accessRights->CanUpdate( $structureVO, StructureVO::NAME, $id));
     
      // sigle
      $ordinalList[] = StructureVO::SIGLE;
      $ordinalListDisp[] ="Sigle :";
      $ordinalType[] = EditForm::$TYPE_TEXT;
      $ordinalSize[] = 79;
      $ordinalReadOnly[] =($bReadOnly ? true: !$accessRights->CanUpdate( $structureVO, StructureVO::SIGLE, $id));

      // siren
      $ordinalList[] = StructureVO::SIREN;
      $ordinalListDisp[] ="Siren :";
      $ordinalType[] = EditForm::$TYPE_TEXT;
      $ordinalSize[] = 79;
      $ordinalReadOnly[] = ($bReadOnly ? true:!$accessRights->CanUpdate( $structureVO, StructureVO::SIREN, $id));

      // siret
      $ordinalList[] = StructureVO::SIRET;
      $ordinalListDisp[] ="Siren :";
      $ordinalType[] = EditForm::$TYPE_TEXT;
      $ordinalSize[] = 79;
      $ordinalReadOnly[] = ($bReadOnly ? true:!$accessRights->CanUpdate( $structureVO, StructureVO::SIRET, $id));

      print('<br><br>');
      
      $editForm = new EditForm( "EditFormStructureAide",
                    $structureVO,
                    StructureVO::ID,
                    $id,
                    $ordinalList,
                    $ordinalListDisp,
                    $ordinalType,
                    $ordinalSize,
                    $ordinalReadOnly,
                    '',
                    $Action,
                    null, 
                    $bReadOnly, 
                    $bReadOnly, 
                    array(),
                    $submitUrl
                  );
    ?>
  </body>
</html>
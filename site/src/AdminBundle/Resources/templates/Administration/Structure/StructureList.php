<?php
/**
 * Liste des rubriques d'aide (gauche) dans l'adminsitration des rubriques d'aide (administration des droits)
 * @author Alkante
 */

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\StructureVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;

  header("Content-type: application/xml");
  echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php 
  $conn    = $this->getCatalogueConnection('catalogue');
  $dao = new DAO($conn, 'catalogue');

  $structureVo = new StructureVO();
  $structureVo->setDao($dao);

  $selectList = new SelectList(  "SelectListStrucutureVO",
                   $structureVo,
                   StructureVO::ID,
                   StructureVO::NAME,
                   Ressources::$IFRAME_NAME_MAIN,
                   '',
                   Ressources::$SELECT_LIST_SIZE_X,
                   Ressources::$SELECT_LIST_SIZE_Y
                   );

?>
  </body>
</html>
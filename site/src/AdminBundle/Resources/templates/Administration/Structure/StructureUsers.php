<?php

/**
 * Onglet gestion des utilisateurs liers au structures
 * @author Alkante
 */


  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\StructureVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\StructureAccedeUserVO;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
    <?php   
      $conn    = $this->getCatalogueConnection('catalogue');  

      // set a default action
      $Action = !isset($_GET["Action"]) ? Ressources::$ACTION_OPEN_STRUCTURES_ACCUEIL : $_GET["Action"] ;
      
      // set a default ID
      $id = !isset($_GET["Id"]) ? -1 : intval( $_GET["Id"] );
      
      if ( $id === -1 ){
        exit();
      }
      
      // set a default ID
      $relationResult = !isset($_GET["RR"]) ? -1 : $_GET["RR"];
      // #################################################################

      $dao = new DAO($conn, 'catalogue');

      if ( $relationResult != -1 ){
        switch( $Action ){
          case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
            $ordinalList = array();
            $valueList = array();
            $ordinalList[] = StructureAccedeUserVO::STRUCTURE_USER_ID;
            $ordinalList[] = StructureAccedeUserVO::USER_STRUCTURE_ID;
            $valueList[] = $relationResult;
            $valueList[] = $id;
   
            $structureAccedeUserVO = new StructureAccedeUserVO();
            $structureAccedeUserVO->setDao($dao);
            $structureAccedeUserVO->InsertValues( $ordinalList, $valueList );
            $structureAccedeUserVO->Commit();
          break;

          case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
            $structureAccedeUserVO = new StructureAccedeUserVO();
            $structureAccedeUserVO->setDao($dao);
            $structureAccedeUserVO->DeleteRow( StructureAccedeUserVO::ID, $relationResult );
            $structureAccedeUserVO->Commit();
          break;
        }
        AlertSaveController::AlertSaveDone("", false);
      }

      // #################################################################
      $structureAccedeUserVO = new StructureAccedeUserVO();
      $structureAccedeUserVO->setDao($dao);

      $structureAccedeUserVO2 = new StructureAccedeUserVO();
      $structureAccedeUserVO2->setDao($dao);
      $structureAccedeUserVO2->AddRestriction( StructureAccedeUserVO::USER_ID, $id );
      $structureAccedeUserVO2->AddOnlyKeyProjection( StructureAccedeUserVO::STRUCTURE_ID );
            
      $structureVO = new StructureVO();
      $structureVO->setDao($dao);

      $structureVO->AddNotInRestriction( StructureVO::ID, $structureAccedeUserVO2 );
      

      $relationList = new RelationList( 
                        "RelationListUserAccedeStructure", //1
                        Ressources::$RELATIONLIST_PROFILES_RUBRICS_AIDE_RELATION,//2
                        $structureAccedeUserVO,                     //3
                        StructureAccedeUserVO::STRUCTURE_USER_ID,        //4
                        StructureAccedeUserVO::STRUCTURE_NOM,       //5
                        StructureAccedeUserVO::USER_STRUCTURE_ID,   //6
                        $id,                                        //7
                        StructureAccedeUserVO::STRUCTURE_USER_ID,   //8
                        $structureVO,                               //9
                        Ressources::$RELATIONLIST_PROFILES_RUBRICS_AIDE_TABLE,//10
                        StructureVO::ID,                            //11
                        StructureVO::NAME,                          //12
                        '',                                         //13
                        Ressources::$IFRAME_NAME_MAIN,              //14
                        $this,
                        true,
                        false,
                        false,
                        "",
                        false
                      );
    ?>
  </body>
</html>
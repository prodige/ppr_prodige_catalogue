<?php
/**
 * Accueil de la page d'administration des droits
 * @author Alkante
 */

	/*require_once($AdminPath."/Administration/AccessRights/AccessRights.php");
  require_once(__DIR__."/AccessRights/AccessRights.php");*/
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

	$accessRights = new AccessRights();
	
	print ('<H2>Bienvenue dans l\'outil d\'administration</H2>');
  print ("Sur la gauche, cliquez sur l'un des menus de gestion et sélectionnez une valeur dans la liste proposée pour gérer cette information." .
      "<br/>" .
      "Au centre, vous pourrez naviguer dans les différentes propriétés à gérer par un menu à onglet.");
	print ('<BR><BR>');
	print ('<BR><BR>');
	print ('<BR>');

	print ('<br>
		<table class="administration_gen" style="width:auto;height:auto;position:absolute; bottom:0px" >
			<tr>
				<td>
					UserID : '.$accessRights->GetUserId().'
				</td>
      </tr>' .
      ( $accessRights->IsProdigeAdmin() 
      ? '<tr>
            <td>
    					Droits : ADMINISTRATEUR PRODIGE
    				</td>
          </tr>'
      : '').
      '<tr>
        <td>
					UserName : '.$accessRights->GetUserName().'
				</td>
			</tr>
		</table>
	   ');
?>
	</body>
</html>
<?php
/**
 * Page d'administration des utilisateurs
 * @author Alkante
 */

use Prodige\ProdigeBundle\Controller\User;
use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

header("Content-Type: text/html; charset=UTF-8;");
unset($errormsg);

$user = User::GetUser();
$userId = User::GetUserId();
/* @deprecated prodige4 *
  if (! is_null($userId))
    User::SetUserCookie($userId);
  if (!$user->isConnected())
	{
	  header('location:Erreurs/NonConnecte.php');
	  exit(0);
	}

	if (is_null($user))
		$errormsg = 'Vous n\'&ecirc;tes pas identifi&eacute; !<br>';
*/
  if (isset($_POST['IDENTIFIANT']) && !is_null($user))
	{
		//$user->SetLogin(htmlentities($_POST['LOGIN'], ENT_QUOTES));
		//$user->SetNom(htmlentities($_POST['NOM'], ENT_QUOTES));
		//$user->SetPrenom(htmlentities($_POST['PRENOM'], ENT_QUOTES));
		$user->SetEmail(htmlentities($_POST['EMAIL'], ENT_QUOTES, "UTF-8"));
		//$user->SetTelephone1(htmlentities($_POST['TELEPHONE1'], ENT_QUOTES));
		$user->SetTelephone2(htmlentities($_POST['TELEPHONE2'], ENT_QUOTES, "UTF-8"));
		//$user->SetService(htmlentities($_POST['SERVICE'], ENT_QUOTES));
		if ($_POST['CHANGEMOTDEPASSE'] == '1')
		{
			if (!$user->IsPassword(htmlentities($_POST['MOTDEPASSE1_hidden'], ENT_QUOTES, "UTF-8")))
			{
				$user->SetPassword(htmlentities($_POST['MOTDEPASSE1_hidden'], ENT_QUOTES, "UTF-8"));
				$user->SetPasswordExpire();
			}
			else
			{
        if (!$user->PasswordIsValid())
					$errormsg = 'Votre mot de passe a expir&eacute;, vous devez le changer !<br>';
			}
		}
		else
		{
			if (!$user->PasswordIsValid())
				$errormsg = 'Votre mot de passe a expir&eacute;, vous devez le changer !<br>';
		}
		if (!isset($errormsg))
		{
			$user->Save();
			$errormsg = 'Donn&eacute;es enregistr&eacute;es !<br>';
            AlertSaveController::AlertSaveDone("", true, $errormsg);
		}
	}
	else
	{
        if (!is_null($user)) {
            $userDetails = $user->getUserAccountExpirationDetails();
            if( $userDetails['alert'] == true ) {
                $errormsg = $userDetails['message'];
            }
        }
		if (!is_null($user) && !$user->PasswordIsValid())
			$errormsg = 'Votre mot de passe a expir&eacute; !<br>';		
	}
  /*
  $formAction = $_SERVER['PHP_SELF'].
                ( $_SERVER['QUERY_STRING']!=""
                  ? "?".$_SERVER['QUERY_STRING']
                  : "");
  */
  $formAction = $submit_form;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Administration du profil utilisateur</title>
		<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
    <link rel='stylesheet' type='text/css' href="<?php echo $admin_css; ?>">
    <script  src="/Scripts/administration_utilisateur.js"> </script>
    <script  src="/Scripts/lib_crypt.js"> </script>
	</head>
	<body onload="administration_utilisateur_init();">
		<div class="errormsg" id="errormsg">
			<?php
				if (isset($errormsg))
					echo $errormsg;
			?>
		</div>
		<div id="administration_utilisateur" class="administration_utilisateur" align="center">
			<form action="<?php echo $formAction;?>" method="POST">
				<input type='hidden' name='EXPIRE' id='EXPIRE' value='<?php echo (!$user->PasswordIsValid() ? "1" : "0"); ?>'>
				<table>
					<tr>
						<th>Identifiant</th>
						<td>
							<input type='hidden' name='IDENTIFIANT' id='IDENTIFIANT' value='<?php echo $user->GetUserId(); ?>'>
							<input class="lock" type='text' name='LOGIN' id='LOGIN' readonly value="<?php echo $user->GetLogin(); ?>" size="30" maxlength="128">
						</td>
					</tr>
          <!-- masque l'affichage du mot de passe en mode intégration -->
          <?php
            if ( !defined("PRO_INCLUDED") || !PRO_INCLUDED ) {
          ?>
					<tr>
						<th>Mot de passe</th>
						<td><input type='hidden' name='MOTDEPASSE1_hidden' id='MOTDEPASSE1_hidden' value=''>
						<input <?php echo ( defined("PRO_INCLUDED") && PRO_INCLUDED ? "class=\"lock\"" : "" ) ?> type='password' name='MOTDEPASSE1' id='MOTDEPASSE1' value='fauxmdp' size="30" maxlength="128" onchange="administration_utilisateur_onchangepwd(this)" onfocus="administration_utilisateur_ongetfocus(this)"><input type='hidden' name='CHANGEMOTDEPASSE' id='CHANGEMOTDEPASSE' value='0'></td>
					</tr>
					<tr>
						<th>Confirmation du mot de passe</th>
						<td><input type='hidden' name='MOTDEPASSE2_hidden' id='MOTDEPASSE2_hidden' value=''>
						<input <?php echo ( defined("PRO_INCLUDED") && PRO_INCLUDED ? "class=\"lock\"" : "" ) ?> type='password' name='MOTDEPASSE2' id='MOTDEPASSE2' value='fauxmdp' size="30" maxlength="128" onchange="administration_utilisateur_onchangepwd(this)" onfocus="administration_utilisateur_ongetfocus(this)"></td>
					</tr>
					<tr>
          <?php
            }
          ?>
						<th>Nom</th>
						<td><input class="lock" type='text' name='NOM' id='NOM' readonly value="<?php echo $user->GetNom(); ?>" size="30" maxlength="128"></td>
					</tr>
					<tr>
						<th>Pr&eacute;nom</th>
						<td><input class="lock" type='text' name='PRENOM' id='PRENOM' readonly value="<?php $user->GetPrenom(); ?>" size="30" maxlength="128"></td>
					</tr>
					<tr>
						<th>Email</th>
						<td><input  type='text' name='EMAIL' id='EMAIL'  value="<?php echo $user->GetEmail(); ?>" size="30" maxlength="128"></td>
					</tr>
					<tr>
						<th>T&eacute;l&eacute;phone 1</th>
						<td><input class="lock" type='text' name='TELEPHONE1' id='TELEPHONE1' readonly value="<?php $user->GetTelephone1(); ?>" size="30" maxlength="30"></td>
					</tr>
					<tr>
						<th>T&eacute;l&eacute;phone 2</th>
            <!-- champ en lecture en mode intégration -->
						<td><input <?php echo ( defined("PRO_INCLUDED") && PRO_INCLUDED ? "class=\"lock\"" : "" ) ?> type='text' name='TELEPHONE2' id='TELEPHONE2' value="<?php /*TODO hismail //echo $user->GetTelephone2();*/ ?>" size="30" maxlength="30"></td>
					</tr>
					<tr>
						<th>Service</th>
						<td><input class="lock" type='text' name='SERVICE' id='SERVICE' readonly value="<?php echo $user->GetService(); ?>" size="30" maxlength="128"></td>
					</tr>
					<tr>
					  <td class="validation" colspan="2" style="text-align:center;padding:5px">
					    <!-- masquage du bouton valider en mode intégration -->
                        <?php
                          if ( !defined("PRO_INCLUDED") || !PRO_INCLUDED ) {
                        ?>
                        <input type="button" name="VALIDATION" value="Valider" onclick="administration_utilisateur_valider()">
                        <?php
                          }
                        ?>
				      </td>  
                      <!--
                      <td class="validation">
                      <?php if (!isset($_GET["url"])){?> <input type="button" name="DETAIL" value="Droits" onclick="detail_compte()"><?php }?>
                      </td>
                      -->
	        </tr>
        </table>
			</form>
		</div>
	</body>
</html>
<?php
/**
 * Affichage des informations resumees du catalogue de metadonnees
 * @author Alkante
 */

/*require_once('parametrage.php'); //TODO hismail
require_once('include/ClassUser.php'); //TODO hismail

require_once($AdminPath."DAO/DAO/DAO.php");
if ( !isset($AdminPath) )
$AdminPath = PRO_CATALOGUE_ROOT."/Administration/";
*/

use Prodige\ProdigeBundle\DAOProxy\DAO;
use Prodige\ProdigeBundle\Controller\User;

$id = (isset($_GET["Id"])&& $_GET["Id"]!="-1" ? $_GET["Id"] : User::GetUserId());


//$dao = new DAO();
$dao = new DAO($conn, 'catalogue');
if ($dao)
{

  // recup de l'identifiant
  $queryNom = "SELECT * from utilisateur where pk_utilisateur=:id" ;
  $rsnom = $dao->BuildResultSet($queryNom, array('id'=>$id));
  $strHtmlidentifiant = "";
  for ($rsnom->First(); !$rsnom->EOF(); $rsnom->Next())
  {
    $identifiant = $rsnom->Read(2);
    $strHtmlidentifiant.= $identifiant.",";
  }

  // selct des profils
  $query = "SELECT * from grp_regroupe_usr gru LEFT JOIN groupe_profil phg ".
           "on gru.grpusr_fk_groupe_profil  = phg.pk_groupe_profil  ".
           "WHERE grpusr_fk_utilisateur=:id ORDER BY grp_nom ";

  $rs = $dao->BuildResultSet($query, array('id'=>$id));

  $strHtmlNomProfil= '';
  $tabIDProfil = array();
  if($rs->GetNbRows()==0){
    $strHtmlNomProfil = '<li class="puce1">aucun</li>';

  }else{
    for ($rs->First(); !$rs->EOF(); $rs->Next())
    {
      if(!in_array($rs->Read(2), $tabIDProfil)){
        $tabIDProfil[] = $rs->Read(2);
      }
      $strHtmlNomProfil.= '<li class="puce1">'.$rs->Read(7).'</li>';
    }
  }
  $strHtmlTrt="";

  if(empty($tabIDProfil)){
    $strHtmlTrt = "aucun";
  }else{
    // selct des traitements
    $queryTrt = "SELECT * from grp_autorise_trt gr LEFT JOIN traitement tr ".
             "on gr.grptrt_fk_traitement  = tr.pk_traitement  ".
             "WHERE grptrt_fk_groupe_profil IN (:profiles) ORDER BY trt_nom ";

    $rsTrt = $dao->BuildResultSet($queryTrt, array('profiles'=>$tabIDProfil));
    if($rsTrt->GetNbRows()==0){
      $strHtmlTrt = '<li class="puce1">aucun</li>';
    } else{
      $strHtmlTrt.= '';
      $tabTrt = array();
      for ($rsTrt->First(); !$rsTrt->EOF(); $rsTrt->Next())
      {
        $nomTrt = $rsTrt->Read(7);
        if(!in_array($nomTrt, $tabTrt)){
          $tabTrt[] = $nomTrt;
        }

      }
      foreach($tabTrt as $key => $value){
        $strHtmlTrt.= '<li class="puce1">'.$value.'</li>';
      }
    }
  }
}


$strHtmlidentifiant = substr($strHtmlidentifiant, 0, -1);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>Détail du compte</title>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
<link rel='stylesheet' type='text/css' href="<?php echo $admin_css; ?>">
</head>
<body onload="parent.IHM_Chargement_stop();">

<div class="back">
<h2>Caractéristiques du compte  <!-- <input style="vertical-align: middle;" type="button"  name="RETOUR" value="Retour" onclick="window.location='administration.php'"> --> </h2> 
</div>
<h2 class="rss-title">Résumé des droits</h2>
<div id="resume_catalogue_compte">
<p>Identifiant : <b><?php echo $strHtmlidentifiant;?></b></p>
Liste des profils : <ul><b><?php echo $strHtmlNomProfil;?></b></ul>
Liste des traitements autorisés :<ul><b><?php echo $strHtmlTrt;?></b></ul>
</div>

<?php if( !empty($tabIDProfil) ){?>

<h2>Domaines accessibles</h2>
<div class="resume">
<table class="tableau" align='left'>
	<tr>
		<td colspan='4'>
		<hr />
		</td>
	</tr>
	<tr>
		<th>Rubrique</th>
		<th>Domaine</th>
		<th>Sous domaine</th>
		<th>Type d'accès</th>
	</tr>
	<?php
	/*$AdminPath = "Administration/";
	require_once($AdminPath."DAO/DAO/DAO.php");*/
	

	//$dao = new DAO();
	$dao = new DAO($conn, 'catalogue');
	if ($dao)
	{// permet de récupérer les ss domaines accessibles par nos profils et liste en + les profil_id (ssdom_admin_fk_groupe_profil) qui permettent l'admin de chaque ss-domm
	  $rs = $dao->BuildResultSet("SELECT distinct
                              RUBRIC_PARAM.RUBRIC_NAME,DOM_SDOM.DOM_NOM, DOM_SDOM.SSDOM_NOM, ssdom_admin_fk_groupe_profil
                              FROM DOM_SDOM
                              LEFT JOIN RUBRIC_PARAM ON DOM_SDOM.DOM_RUBRIC=RUBRIC_PARAM.RUBRIC_ID
                              LEFT JOIN grp_accede_ssdom ON DOM_SDOM.pk_sous_domaine = grp_accede_ssdom.grpss_fk_sous_domaine
                              LEFT JOIN grp_accede_dom ON DOM_SDOM.pk_domaine = grp_accede_dom.grpdom_fk_domaine
                              Left join sous_domaine on sous_domaine.pk_sous_domaine=DOM_SDOM.pk_sous_domaine
                              where     grpss_fk_groupe_profil IN (:profiles) or grpdom_fk_groupe_profil IN (:profiles)
                              ORDER BY  RUBRIC_PARAM.RUBRIC_NAME, DOM_NOM, SSDOM_NOM", array('profiles'=>$tabIDProfil));
	  $lastRubric = '';
	  $lastDomain = '';

	  //loop on all
	  for ($rs->First(); !$rs->EOF(); $rs->Next())
	  {


	    //  echo "-->".$rs->Read(4);

	    $bfirstRub=true;
	    $bfirstDom=true;
	    $bfirstSDom=true;
	    if ($rs->Read(0) != $lastRubric)
	    {
	      $lastRubric = $rs->Read(0);
	      echo "<tr><td colspan='4'><hr/></td></tr>";
	      echo "<tr>\n";
	      echo '<td class="rubric">', $rs->Read(0), '</td>';
	    }
	    else{
	      echo '<td></td>';
	    }

	    if ($rs->Read(1) != $lastDomain)
	    {
	      if ($bfirstDom==true){
	        $bfirstDom=false;
	        echo '<td class="domaine">', $rs->Read(1), '</td>';
	        echo "<td colspan='2'/>\n";
	        echo "</tr>\n";
	      }
	      else{
	        echo '<td></td>';
	      }
	    }
	    echo "<tr onmouseover=\"this.className = 'onover'\" onmouseout=\"this.className = ''\">\n";
	    echo "<td colspan='2'/>\n";
	    echo '<td class="sousdomaine">', $rs->Read(2), '</a></td>';

	    if(in_array($rs->Read(3), $tabIDProfil)){// si admin
	      $stylecolor = "typeaccesadmin";
	      $str = "Administration";
	    }else{//si consult
	      $stylecolor = "typeaccesconsult";
	      $str = "Consultation";
	    };

	    echo "<td class='".$stylecolor."' >".$str."</td>";
	    echo "</tr>\n";
	    $lastDomain = $rs->Read(1);
	  }
	}
	
	?>
	<tr>
		<td colspan='4'>
		<hr />
		</td>
	</tr>
</table></div>
</div><div class="separateur">
	<?php }?>
</div>
</body>
</html>

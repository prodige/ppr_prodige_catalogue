<?php
/**
 * Accueil de la page rubriques d'aide (Administration des droits)
 * @author Alkante
 */

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\RubricAideVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php   
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_COMPETENCES_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if ( $PK==-1 )
  {
    exit;
  }

  $dao = new DAO($conn, 'catalogue');

  $accessRights = new AccessRights( );

  $rubricAideVO = new RubricAideVO();
  $rubricAideVO->setDao($dao);

  $ordinalList = array();
  $ordinalListDisp = array();
  $ordinalType = array();
  $ordinalSize = array();   
  $ordinalReadOnly = array(); 
  

  $ordinalList[]=RubricAideVO::$RUBRIC_AIDE_NAME;
  $ordinalListDisp[]="Intitulé de la rubrique :";
  $ordinalType[]=EditForm::$TYPE_TEXT;
  $ordinalSize[]=79;
  $ordinalReadOnly[]= !$accessRights->CanUpdate( $rubricAideVO, RubricAideVO::$RUBRIC_AIDE_NAME, $PK);
  
  $ordinalList[]=RubricAideVO::$RUBRIC_AIDE_DESC;
  $ordinalListDisp[]="Description :";
  $ordinalType[]=EditForm::$TYPE_TEXTAREA;
  $ordinalSize[]=70;
  $ordinalReadOnly[]= !$accessRights->CanUpdate( $rubricAideVO, RubricAideVO::$RUBRIC_AIDE_DESC, $PK);
  
  $ordinalList[]=RubricAideVO::$RUBRIC_AIDE_FILE;
  $ordinalListDisp[]="Lien :";
  $ordinalType[]=EditForm::$TYPE_FILE_UPLOAD;
  $ordinalSize[]=70;
  $ordinalReadOnly[]= !$accessRights->CanUpdate( $rubricAideVO, RubricAideVO::$RUBRIC_AIDE_FILE, $PK);
  
  
  print('<br><br>');
  
  $editForm = new EditForm( "EditFormRubricsAide",
                $rubricAideVO,
                RubricAideVO::$RUBRIC_AIDE_ID,
                $PK,
                $ordinalList,
                $ordinalListDisp,
                $ordinalType,
                $ordinalSize,
                $ordinalReadOnly,
                '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Rubrics/RubricsAccueil.php",*/
                $Action,
                null, false, false, array(),
                $submitUrl);
?>
  </body>
</html>
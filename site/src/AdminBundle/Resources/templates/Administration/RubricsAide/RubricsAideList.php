<?php
/**
 * Liste des rubriques d'aide (gauche) dans l'adminsitration des rubriques d'aide (administration des droits)
 * @author Alkante
 */

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\RubricAideVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;

  header("Content-type: application/xml");
  echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php 

  $dao = new DAO($conn, 'catalogue');

  $rubricAideVO = new RubricAideVo();
  $rubricAideVO->setDao($dao);

  $selectList = new SelectList(  "SelectListRubricAideVO",
                   $rubricAideVO,
                   RubricAideVO::$RUBRIC_AIDE_ID,
                   RubricAideVO::$RUBRIC_AIDE_NAME,
                   Ressources::$IFRAME_NAME_MAIN,
                   '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Competences/CompetencesDetail.php",*/
                   Ressources::$SELECT_LIST_SIZE_X,
                   Ressources::$SELECT_LIST_SIZE_Y
                   );

?>
  </body>
</html>
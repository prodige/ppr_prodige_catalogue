<?php
/**
 * Onglet gestion des profils des rubriques d'aide
 * @author Alkante
 */


  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\RubricAideVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GroupeProfilVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\RubricAideAccedeProfilVO;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php     
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_RUBRIC_AIDE_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if ( $PK==-1 )
  {
    exit;
  }

  // set a default ID
  if ( !isset($_GET["RR"]) )
  {
    $RELATIONLIST_RESULT = -1;
  }
  else
  {
    $RELATIONLIST_RESULT = $_GET["RR"]; 
  }

  // #################################################################

  $dao = new DAO($conn, 'catalogue');

  if ( $RELATIONLIST_RESULT != -1 )
  {
    switch( $Action )
    {
      case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
        $ordinalList = array();
        $valueList = array();
        $ordinalList[]=RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_FK_RUBRICAIDE_ID;
        $ordinalList[]=RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_FK_PROFIL;
        $valueList[]=$RELATIONLIST_RESULT;
        $valueList[]=$PK;

        $CompetenceAccedeCarteVO_RL = new RubricAideAccedeProfilVO();
        $CompetenceAccedeCarteVO_RL->setDao($dao);
        $CompetenceAccedeCarteVO_RL->InsertValues( $ordinalList, $valueList );
        $CompetenceAccedeCarteVO_RL->Commit();
      break;

      case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
        $CompetenceAccedeCarteVO_RL = new RubricAideAccedeProfilVO();
        $CompetenceAccedeCarteVO_RL->setDao($dao);
        $CompetenceAccedeCarteVO_RL->DeleteRow( RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_FK_PROFIL, $RELATIONLIST_RESULT );
        $CompetenceAccedeCarteVO_RL->Commit();
      break;
    }
    AlertSaveController::AlertSaveDone("", false);
  }

  // #################################################################
  
  
  
  
  $RubricAideAccedeProfilVO = new RubricAideAccedeProfilVO();
  $RubricAideAccedeProfilVO->setDao($dao);
  $groupeProfilVO = new GroupeProfilVO();
  $groupeProfilVO->setDao($dao);

  $rubricAideAccedeProfilVO2 = new RubricAideAccedeProfilVO();
  $rubricAideAccedeProfilVO2->setDao($dao);
  $rubricAideAccedeProfilVO2->AddRestriction( RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_FK_RUBRICAIDE_ID, $PK );
  $rubricAideAccedeProfilVO2->AddOnlyKeyProjection( RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_FK_PROFIL );

  $groupeProfilVO->AddNotInRestriction( GroupeProfilVO::$PK_GROUPE_PROFIL, $rubricAideAccedeProfilVO2 );
  

  $relationList = new RelationList( "RelationListProfilAccedeRubricAide",
                   Ressources::$RELATIONLIST_PROFILES_RUBRICS_AIDE_RELATION,
                   $RubricAideAccedeProfilVO,
                   RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_PROFIL_ID,
                   RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_PROFIL_NOM,
                   RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_FK_RUBRICAIDE_ID,
                   $PK,
                   RubricAideAccedeProfilVO::$RUBRICAIDEPROFILE_FK_PROFIL,
                   $groupeProfilVO,
                   Ressources::$RELATIONLIST_PROFILES_RUBRICS_AIDE_TABLE,
                   $groupeProfilVO::$PK_GROUPE_PROFIL,
                   $groupeProfilVO::$GRP_ID,
                   '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Competences/CompetencesMaps.php",*/
                   Ressources::$IFRAME_NAME_MAIN,
                   $this
                   );
  
?>
  </body>
</html>
<?php
/**
 * Liste des rubriques (gauche) dans l'adminsitration des rubriques (administration des droits)
 * @author Alkante
 */
  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Modules/BO/RubricVO.php");
  require_once($AdminPath."/Components/SelectList/SelectList.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\RubricVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;

  header("Content-type: application/xml");
  echo '<?xml version="1.0" encoding="UTF-8"?>';
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php 

  $dao = new DAO($conn, 'catalogue');

  $rubricVO = new RubricVO();
  $rubricVO->setDao($dao);

  $selectList = new SelectList(  "SelectListRubricVO",
                   $rubricVO,
                   RubricVO::$RUBRIC_ID,
                   RubricVO::$RUBRIC_NAME,
                   Ressources::$IFRAME_NAME_MAIN,
                   '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Competences/CompetencesDetail.php",*/
                   Ressources::$SELECT_LIST_SIZE_X,
                   Ressources::$SELECT_LIST_SIZE_Y
                   );

?>
  </body>
</html>
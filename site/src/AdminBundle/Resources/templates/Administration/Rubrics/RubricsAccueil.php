<?php
/**
 * Accueil de la page rubriques (Administration des droits)
 * @author Alkante
 */
  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Modules/BO/RubricVO.php");
  require_once($AdminPath."/Components/EditForm/EditForm.php");
  require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\RubricVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php   
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_COMPETENCES_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if ( $PK==-1 )
  {
    exit;
  }

  $dao = new DAO($conn, 'catalogue');

  $accessRights = new AccessRights( );

  $rubricVO = new RubricVO();
  $rubricVO->setDao($dao);

  $ordinalList = array();
  $ordinalListDisp = array();
  $ordinalType = array();
  $ordinalSize = array();   
  $ordinalReadOnly = array(); 
  

  $ordinalList[]=RubricVO::$RUBRIC_NAME;
  $ordinalListDisp[]="Nom :";
  $ordinalType[]=EditForm::$TYPE_TEXT;
  $ordinalSize[]=30;
  $ordinalReadOnly[]= !$accessRights->CanUpdate( $rubricVO, RubricVO::$RUBRIC_NAME, $PK);
  print('<br><br>');
  
  $editForm = new EditForm( "EditFormRubrics",
                $rubricVO,
                RubricVO::$RUBRIC_ID,
                $PK,
                $ordinalList,
                $ordinalListDisp,
                $ordinalType,
                $ordinalSize,
                $ordinalReadOnly,
                '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Rubrics/RubricsAccueil.php",*/
                $Action,
                null, false, false, array(),
                $submitUrl);
?>
  </body>
</html>
<?php
/**
 * Page (cadre) principale de l'administration des droits
 * @author Alkante
 */
	require_once("../../Ressources/Administration/Ressources.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style type="text/css">
      a {color:#000000;text-decoration:none}
      a:hover {text-decoration:underline}
    </style>
	</head>
	<body style="margin:0px;">
<?php
	// #################################################################
	// #################################################################
	// #################################################################

	// set a default action
	if ( !isset( $_GET["Action"]) )
	{
		$Action = Ressources::$ACTION_OPEN_INITIAL_PAGE;
	}
	else
	{
		// get the action parameter
		$Action = $_GET["Action"];
	}
	// get the main action
	$mainAction = GetMainAction( $Action );

	print('
		<TABLE width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
			<TR width="100%" height="32" border=0>
				<TD>
					'.DrawHeader( $mainAction ).'
				</TD>
			</TR>
			<TR>
				<TD>
					<IFRAME width="'.Ressources::$MODULE_SIZE_X.'"
						height="'.Ressources::$MODULE_SIZE_Y.'"
						MARGINHEIGHT="0"
						MARGINWIDTH="0"
						FRAMEBORDER="0"
						name="'.Ressources::$IFRAME_NAME_MAIN.'"
						ALLOWTRANSPARENCY="true"
						SCROLLING="no"
						SRC="Modules.php?Action='.$mainAction.'">
					</IFRAME>
				</TD>
			</TR>
		</TABLE>
	');

	// DO Specific actions
	switch( $Action )
	{
		case Ressources::$ACTION_OPEN_INITIAL_PAGE:
		break;
	}


	// #################################################################
	// #################################################################
	// #################################################################

	/**
   * Ecrit un tableau dans la page html
   * @param $action
   * @param $pk
   * @return unknown_type
   */
	function DrawHeader( $action )
	{
		$header ='
			<TABLE style="font-size:8pt;text-align: center;margin-bottom:4px;" width="600" height="32" border="0" cellpadding="0" cellspacing="0">
				<TR>
					<TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_INITIAL_PAGE ).'">

						<A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_INITIAL_PAGE.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
							'.Ressources::$BTN_NAV_INITIAL.'
						</A>
					</TD>
					<TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_PROFILES ).'">
						<A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_PROFILES.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
							'.Ressources::$BTN_NAV_PROFILES.'
						</A>
					</TD>
					<TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_USERS ).'">
						<A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_USERS.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
							'.Ressources::$BTN_NAV_USERS.'
						</A>
					</TD>
					<TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_RUBRICS).'">
            <A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_RUBRICS.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
              '.Ressources::$BTN_NAV_RUBRICS.'
            </A>
          </TD>
					<TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_DOMAINS ).'">
						<A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_DOMAINS.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
							'.Ressources::$BTN_NAV_DOMAINS.'
						</A>
					</TD>
					<TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_SUBDOMAINS ).'">
						<A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_SUBDOMAINS.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
							'.Ressources::$BTN_NAV_SUBDOMAINS.'
						</A>
					</TD>
          <TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_ACTIONS ).'">
            <A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_ACTIONS.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
              '.Ressources::$BTN_NAV_ACTIONS.'
            </A>
          </TD>
          <TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_LAYERS ).'">
            <A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_LAYERS.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
              '.Ressources::$BTN_NAV_LAYERS.'
            </A>
          </TD>
          <TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_MAPS ).'">
            <A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_MAPS.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
              '.Ressources::$BTN_NAV_MAPS.'
            </A>
          </TD>
          <TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_ZONAGES).'">
            <A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_ZONAGES.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
              '.Ressources::$BTN_NAV_ZONAGES.'
            </A>
          </TD>
          <TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_PERIMETRES).'">
            <A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_PERIMETRES.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
              '.Ressources::$BTN_NAV_PERIMETRES.'
            </A>
          </TD>
          <TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_COMPETENCES).'">
            <A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_COMPETENCES.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
              '.Ressources::$BTN_NAV_COMPETENCES.'
            </A>
          </TD>
          <TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_GROUPING, true).'">
            <A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_GROUPING.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
              '.Ressources::$BTN_NAV_GROUPING.'
            </A>
          </TD>
				</TR>
			</TABLE>';
/*
					<TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_ACTIONS ).'">
						<A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_ACTIONS.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
							'.Ressources::$BTN_NAV_ACTIONS.'
						</A>
					</TD>
					<TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_LAYERS ).'">
						<A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_LAYERS.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
							'.Ressources::$BTN_NAV_LAYERS.'
						</A>
					</TD>
					<TD width="100" style="'.GetHeaderTableBackground($action, Ressources::$ACTION_OPEN_MAPS ).'">
						<A HREF="AdminMain.php?Action='.Ressources::$ACTION_OPEN_MAPS.'" TARGET="'.Ressources::$IFRAME_NAME_MAIN.'">
							'.Ressources::$BTN_NAV_MAPS.'
						</A>
					</TD>
*/
		return $header;
	}

	// #################################################################

	/**
   * Renvoie la chaîne de caractères correspondant à l'attribut html style d'un tableau
   * @param $action
   * @param $celAction
   * @param $bLast
   * @return $strStyle : style du tableau
   */
	function GetHeaderTableBackground( $action, $celAction, $bLast=false )
	{
    $strStyle = "border:1px solid #000000;".(!$bLast ? "border-right:none;" : "")."padding:5px;background-color:";
		if ( $action == $celAction )
		{
			return $strStyle."#FFFFFF;border-bottom:none;";
		}

		return $strStyle."#dddddd;";
	}

	// #################################################################
	/**
   * Renvoie l'action demandée
   * @param $action
   * @return $returnAction : numéro de l'action
   */
	function GetMainAction( $action )
	{
		$returnAction = 1000;
	  if( $action >= Ressources::$ACTION_OPEN_GROUPING)
    {
      $returnAction = Ressources::$ACTION_OPEN_GROUPING;
    }
    elseif( $action >= Ressources::$ACTION_OPEN_ZONAGES)
    {
      $returnAction = Ressources::$ACTION_OPEN_ZONAGES;
    }
	  elseif( $action >= Ressources::$ACTION_OPEN_PERIMETRES)
    {
      $returnAction = Ressources::$ACTION_OPEN_PERIMETRES;
    }
    elseif( $action >= Ressources::$ACTION_OPEN_COMPETENCES)
    {
      $returnAction = Ressources::$ACTION_OPEN_COMPETENCES;
    }
		elseif ( $action >= Ressources::$ACTION_OPEN_MAPS)
		{
			$returnAction = Ressources::$ACTION_OPEN_MAPS;
		}
		else if ( $action >= Ressources::$ACTION_OPEN_LAYERS)
		{
			$returnAction = Ressources::$ACTION_OPEN_LAYERS;
		}
		else if ( $action >= Ressources::$ACTION_OPEN_ACTIONS)
		{
			$returnAction = Ressources::$ACTION_OPEN_ACTIONS;
		}
		else if ( $action >= Ressources::$ACTION_OPEN_SUBDOMAINS)
		{
			$returnAction = Ressources::$ACTION_OPEN_SUBDOMAINS;
		}
		else if ( $action >= Ressources::$ACTION_OPEN_DOMAINS)
		{
			$returnAction = Ressources::$ACTION_OPEN_DOMAINS;
		}
	  else if ( $action >= Ressources::$ACTION_OPEN_RUBRICS)
    {
      $returnAction = Ressources::$ACTION_OPEN_RUBRICS;
    }
		else if ( $action >= Ressources::$ACTION_OPEN_USERS)
		{
			$returnAction = Ressources::$ACTION_OPEN_USERS;
		}
		else if ( $action >= Ressources::$ACTION_OPEN_PROFILES)
		{
			$returnAction = Ressources::$ACTION_OPEN_PROFILES;
		}
		else if ( $action >= Ressources::$ACTION_OPEN_INITIAL_PAGE)
		{
			$returnAction = Ressources::$ACTION_OPEN_INITIAL_PAGE;
		}
		else if ( $action >= Ressources::$ACTION_OPEN_RUBRICS_AIDE)
		{
		  $returnAction = Ressources::$ACTION_OPEN_RUBRICS_AIDE;
		}

		return $returnAction;
	}
?>
	</body>
</html>

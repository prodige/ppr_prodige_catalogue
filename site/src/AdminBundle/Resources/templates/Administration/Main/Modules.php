<?php
/**
 * Page définissant les onglets de l'administration des droits
 * @author Alkante
 */
	require_once("../../Ressources/Administration/Ressources.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	</head>
	<body style="margin:0px;">
<?php 
	// get the action parameter
	$Action = $_GET["Action"];
	// set a default action
	if ( !isset($Action) )
	{
		$Action = Ressources::$ACTION_OPEN_INITIAL_PAGE;
	}
  
  if ($Action == Ressources::$ACTION_OPEN_LAYERS || $Action == Ressources::$ACTION_OPEN_MAPS ) {
    print('
      <TABLE width="100%" height="100%" border="1" cellpadding="0" cellspacing="0">
        <TR>
          <TD width="100%">
            <IFRAME width="100%"
              height="100"
              MARGINHEIGHT="0"
              MARGINWIDTH="0"
              FRAMEBORDER="0"
              name="'.Ressources::$IFRAME_NAME_MAIN.'"
              id="'.Ressources::$IFRAME_NAME_MAIN.'"
              ALLOWTRANSPARENCY="true"
              SCROLLING="no"
              SRC="'.GetModulePageList($Action).'">
            </IFRAME>
          </TD></TR><TR>
          <TD width="100%">
            <IFRAME width="'.Ressources::$DETAIL_SIZE_X.'"
              height="280"
              MARGINHEIGHT="0"
              MARGINWIDTH="0"
              FRAMEBORDER="0"
              name="'.Ressources::$IFRAME_NAME_MAIN.'"
              id="'.Ressources::$IFRAME_NAME_MAIN.'"
              ALLOWTRANSPARENCY="true"
              SCROLLING="no"
              SRC="'.GetModulePageDetail($Action).'">
            </IFRAME>
          </TD>
        </TR>
      </TABLE>');
  }elseif($Action != Ressources::$ACTION_OPEN_GROUPING){
    
  	print('
  			<TABLE width="100%" height="100%" border="1" cellpadding="0" cellspacing="0">
  				<TR>
  					<TD width='.Ressources::$LIST_SIZE_X.'>
  						<IFRAME width="'.Ressources::$LIST_SIZE_X.'"
  							height="'.Ressources::$LIST_SIZE_Y.'"
  							MARGINHEIGHT="0"
  							MARGINWIDTH="0"
  							FRAMEBORDER="0"
  							name="'.Ressources::$IFRAME_NAME_MAIN.'"
  							id="'.Ressources::$IFRAME_NAME_MAIN.'"
  							ALLOWTRANSPARENCY="true"
  							SCROLLING="no"
  							SRC="'.GetModulePageList($Action).'">
  						</IFRAME>
  					</TD>
  					<TD width="100%">
  						<IFRAME width="'.Ressources::$DETAIL_SIZE_X.'"
  							height="'.Ressources::$DETAIL_SIZE_Y.'"
  							MARGINHEIGHT="0"
  							MARGINWIDTH="0"
  							FRAMEBORDER="0"
  							name="'.Ressources::$IFRAME_NAME_MAIN.'"
  							id="'.Ressources::$IFRAME_NAME_MAIN.'"
  							ALLOWTRANSPARENCY="true"
  							SCROLLING="no"
  							SRC="'.GetModulePageDetail($Action).'">
  						</IFRAME>
  					</TD>
  				</TR>
  			</TABLE>
  		');
  }elseif($Action != Ressources::$ACTION_OPEN_RUBRICS){
    
    print('
        <TABLE width="100%" height="100%" border="1" cellpadding="0" cellspacing="0">
          <TR>
            <TD width='.Ressources::$LIST_SIZE_X.'>
              <IFRAME width="'.Ressources::$LIST_SIZE_X.'"
                height="'.Ressources::$LIST_SIZE_Y.'"
                MARGINHEIGHT="0"
                MARGINWIDTH="0"
                FRAMEBORDER="0"
                name="'.Ressources::$IFRAME_NAME_MAIN.'"
                id="'.Ressources::$IFRAME_NAME_MAIN.'"
                ALLOWTRANSPARENCY="true"
                SCROLLING="no"
                SRC="'.GetModulePageList($Action).'">
              </IFRAME>
            </TD>
            <TD width="100%">
              <IFRAME width="'.Ressources::$DETAIL_SIZE_X.'"
                height="'.Ressources::$DETAIL_SIZE_Y.'"
                MARGINHEIGHT="0"
                MARGINWIDTH="0"
                FRAMEBORDER="0"
                name="'.Ressources::$IFRAME_NAME_MAIN.'"
                id="'.Ressources::$IFRAME_NAME_MAIN.'"
                ALLOWTRANSPARENCY="true"
                SCROLLING="no"
                SRC="'.GetModulePageDetail($Action).'">
              </IFRAME>
            </TD>
          </TR>
        </TABLE>
      ');
  }
  else{
    print('
        <TABLE width="100%" height="100%" border="1" cellpadding="0" cellspacing="0">
          <TR>
            <TD width="100%">
              <IFRAME style:"width=100%;
                height=600px;"
                MARGINHEIGHT="0"
                MARGINWIDTH="0"
                FRAMEBORDER="0"
                name="'.Ressources::$IFRAME_NAME_MAIN.'"
                id="'.Ressources::$IFRAME_NAME_MAIN.'"
                ALLOWTRANSPARENCY="true"
                SCROLLING="no"
                SRC="'.GetModulePageDetail($Action).'">
              </IFRAME>
            </TD>
          </TR>
        </TABLE>
      ');
  }

  /**
   * @brief Renvoie la page détail correspondant à l'onglet sur lequel on a cliqué
   * @param $action
   * @return url de la page onglet
   */
	function GetModulePageDetail( $action )
	{
		switch( $action )
		{
			case Ressources::$ACTION_OPEN_INITIAL_PAGE:
				return "../../Administration/Accueil/Accueil.php";
			break;
			case Ressources::$ACTION_OPEN_PROFILES:
				return "../../Administration/Profiles/ProfilesDetail.php";
			break;
			case Ressources::$ACTION_OPEN_USERS:
				return "../../Administration/Users/UsersDetail.php";
			break;
		 case Ressources::$ACTION_OPEN_RUBRICS:
        return "../../../Administration/Appearance/RubricsDetail.php";
      break;
			case Ressources::$ACTION_OPEN_DOMAINS:
				return "../../Administration/Domains/DomainsDetail.php";
			break;
			case Ressources::$ACTION_OPEN_ACTIONS:
				return "../../Administration/Actions/ActionsDetail.php";
			break;
			case Ressources::$ACTION_OPEN_SUBDOMAINS:
				return "../../Administration/SubDomains/SubDomainsDetail.php";
			break;
			case Ressources::$ACTION_OPEN_LAYERS:
				return "../../Administration/Layers/LayersDetail.php";
			break;
			case Ressources::$ACTION_OPEN_MAPS:
				return "../../Administration/Maps/MapsDetail.php";
			break;
      case Ressources::$ACTION_OPEN_COMPETENCES:
        return "../../Administration/Competences/CompetencesDetail.php";
      break;
      case Ressources::$ACTION_OPEN_ZONAGES:
        return "../../Administration/Zonages/ZonagesDetail.php";
      break;
      case Ressources::$ACTION_OPEN_PERIMETRES:
        return "../../Administration/Perimetres/PerimetresDetail.php";
      break;
      case Ressources::$ACTION_OPEN_GROUPING:
        return "../../Administration/Grouping/GroupingDetail.php";
      break;
      
		}

		return "../../Administration/Main/Empty.php";
	}

	/**
   * @brief Renvoie la liste de gauche correspondant à l'onglet sur lequel on a cliqué
   * @param $action
   * @return url de la page onglet
   */
	function GetModulePageList( $action )
	{
		switch( $action )
		{
			case Ressources::$ACTION_OPEN_INITIAL_PAGE:
				return "../../Administration/Main/Empty.php";
			break;
			case Ressources::$ACTION_OPEN_PROFILES:
				return "../../Administration/Profiles/ProfilesList.php";
			break;
			case Ressources::$ACTION_OPEN_USERS:
				return "../../Administration/Users/UsersList.php";
			break;
			case Ressources::$ACTION_OPEN_RUBRICS:
        return "../../Administration/Rubrics/Empty.php";
      break;
			case Ressources::$ACTION_OPEN_DOMAINS:
				return "../../Administration/Domains/DomainsList.php";
			break;
			case Ressources::$ACTION_OPEN_ACTIONS:
				return "../../Administration/Actions/ActionsList.php";
			break;
			case Ressources::$ACTION_OPEN_SUBDOMAINS:
				return "../../Administration/SubDomains/SubDomainsList.php";
			break;
			case Ressources::$ACTION_OPEN_LAYERS:
				return "../../Administration/Layers/LayersList.php";
			break;
			case Ressources::$ACTION_OPEN_MAPS:
				return "../../Administration/Maps/MapsList.php";
			break;
      case Ressources::$ACTION_OPEN_COMPETENCES:
        return "../../Administration/Competences/CompetencesList.php";
      break;
      case Ressources::$ACTION_OPEN_ZONAGES:
        return "../../Administration/Zonages/ZonagesList.php";
      break;
      case Ressources::$ACTION_OPEN_PERIMETRES:
        return "../../Administration/Perimetres/PerimetresList.php";
      break;
      case Ressources::$ACTION_OPEN_GROUPING:
        return "../../Administration/Grouping/Empty.php";
      break;
  
		}

		return "../../Administration/Main/Empty.php";
	}
?>
	</body>
</html>

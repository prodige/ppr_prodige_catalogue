<?php
/**
 * Page d'adminsitration des droits, redirige vers AdminMain.php
 * @author Alkante
 */

  require_once($AdminPath."/Ressources/Administration/Ressources.php");
  
  $iPart = (isset($_GET["iPart"]) ? $_GET["iPart"] : -1); 
  $Action = (isset($_GET["Action"]) ? $_GET["Action"] : -1);
  switch ($iPart){
    case "0": // list
      include_once(GetModulePageList( $Action ));
      exit();
    break;
    case "1": // contents
      $id = (isset($_GET["id"]) ? $_GET["id"] : -1);
      if ( $Action==Ressources::$ACTION_OPEN_GROUPING ){
        $Action = $id;
        $id = -1;
      }
      GetModulePageDetail( $Action, $id );
      exit();
    break;
  }
  /**
   * @brief Renvoie la liste de gauche correspondant à l'onglet sur lequel on a cliqué
   * @param $action
   * @return url de la page onglet
   */
  function GetModulePageList( $action )
  {
    global $AdminPath;
    switch( $action )
    {
      case Ressources::$ACTION_OPEN_INITIAL:
        return $AdminPath."/Administration/Accueil/Accueil.php";
      break;
      case Ressources::$ACTION_OPEN_PROFILES:
        return $AdminPath."/Administration/Profiles/ProfilesList.php";
      break;
      case Ressources::$ACTION_OPEN_USERS:
        return $AdminPath."/Administration/Users/UsersList.php";
      break;
      case Ressources::$ACTION_OPEN_RUBRICS:
        return $AdminPath."/Administration/Rubrics/RubricsList.php";
      break;
      case Ressources::$ACTION_OPEN_DOMAINS:
        return $AdminPath."/Administration/Domains/DomainsList.php";
      break;
      case Ressources::$ACTION_OPEN_SUBDOMAINS:
        return $AdminPath."/Administration/SubDomains/SubDomainsList.php";
      break;
      case Ressources::$ACTION_OPEN_LAYERS:
        return $AdminPath."/Administration/Layers/LayersList.php";
      break;
      case Ressources::$ACTION_OPEN_MAPS:
        return $AdminPath."/Administration/Maps/MapsList.php";
      break;
      case Ressources::$ACTION_OPEN_COMPETENCES:
        return $AdminPath."/Administration/Competences/CompetencesList.php";
      break;
      case Ressources::$ACTION_OPEN_ZONAGES:
        return $AdminPath."/Administration/Zonages/ZonagesList.php";
      break;
      case Ressources::$ACTION_OPEN_PERIMETRES:
        return $AdminPath."/Administration/Perimetres/PerimetresList.php";
      break;
      case Ressources::$ACTION_OPEN_GROUPING:
        return $AdminPath."/Administration/Grouping/GroupingList.php";
      break;
      case Ressources::$ACTION_OPEN_RUBRICS_AIDE:
        return $AdminPath."/Administration/Rubrics/RubricsAideList.php";
      break;
    }

    return $AdminPath."/Administration/Main/Empty.php";
  }
  
  /**
   * @brief Renvoie la page détail correspondant à l'onglet sur lequel on a cliqué
   * @param $action
   * @return url de la page onglet
   */
  function GetModulePageDetail( $action, $idSelected )
  {
    global $AdminPath;
    $tabSheet = Ressources::getTabModuleSheet();
    $idElt = "tab_".$action;
    $firstSheet = "-1";
    foreach ($tabSheet[$action] as $idSheet=>$titleSheet){
      $firstSheet = $idSheet;
      break;
    }
    ?>
    tabGridList = [];
    adminTabPanelMain = new Ext.TabPanel(
      {
        border :false,
        id :'module_details',
        activeTab :'<?php echo $idElt."_".$firstSheet ?>',
        renderTo : Ext.getCmp("contAdmin").body,
        autoScroll : true,
          listener : {
            bodyresize : function(panel, width, height){
              alert(panel.xtype+" "+height);
              /*
              var cHeight = Ext.get('contAdmin').dom.lastChild.offsetHeight - Ext.get('module_details').dom.firstChild.offsetHeight-5;
              if ( Ext.get('data_profiles') ){
                cHeight -= Ext.get('data_profiles').dom.firstChild.offsetHeight;
              }
              grid.setHeight(Math.min(grid.view.scroller.dom.firstChild.offsetHeight+50, cHeight));*/
            }
          },
        autoHeight : true,
        autoWidth : true,
        items : [
    <?php
    $glue = "";
    foreach ($tabSheet[$action] as $idSheet=>$descSheet){
      echo $glue; 
    ?>
      {
        title :'<?php echo $descSheet["title"] ?>',
        id :'<?php echo $idElt."_".$idSheet ?>',
        autoScroll : true,
        autoHeight : true,
        autoWidth : true,
        autoLoad : {
          url : "droits.php?url=<?php echo $descSheet["url"] ?>&iPart=1&Action=<?php echo $action ?>&Id=<?php echo $idSelected ?>",
          method : 'GET', 
          scripts: true,
          text : "Chargement en cours..."
        },
        listeners : {
          show : function(_this){
            if ( typeof _this.gridList != "undefined" ){
              _this.gridList.fireEvent("afterrender", _this.gridList);
            }
          }
        }
      }
    <?php
      $glue = ",";
    }
    ?>
    ]});
    <?php
  }
  
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<!-- IE specific : forcing IE9 Document model because ext 3.0 incompatibility with IE10 -->
    <META HTTP-EQUIV="X-UA-Compatible" CONTENT="IE=9"/>
    <!-- ExtJS css -->   
    <link rel="stylesheet" type="text/css" href="<?php echo PRO_CATALOGUE_URLBASE;?>CSS/administration_droits.php" />     
    <link rel='stylesheet' type='text/css' href="<?php echo PRO_CATALOGUE_URLBASE;?>CSS/administration_utilisateur.php">
    <?php echo addScriptCss("Scripts/ext3.0/resources/css/ext-all.css");?>     
    <?php 
    if(defined("PRO_CSS_FILE") && PRO_CSS_FILE !="")
      echo '<link rel="stylesheet" type="text/css" href="'.PRO_CSS_FILE.'" /> ' ;
    else
      echo "";
    ?>
	<?php echo addScriptJs("Scripts/administration.js");?>
	<?php echo addScriptJs("Scripts/ext3.0/adapter/ext/ext-base.js");?>
	<?php echo addScriptJs("Scripts/ext3.0/ext-all.js");?>
	<?php echo addScriptJs("Scripts/extjs_overload.js");?>
	<?php echo addScriptJs("Scripts/extjs_plugins/BufferView.js");?>		
	<?php echo addScriptJs("Scripts/extadminrender.js");?>
	<?php echo addScriptJs("Scripts/lib_crypt.js");?>				
    <?php echo Ressources::getVars() ?>
	</head>
	<body style="margin:0px;" onload="administration_init()">
		<IFRAME <?php echo "style='position:absolute;bottom:0;right:0;z-index:100;display:none'" ?>
			name="<?php echo Ressources::$IFRAME_NAME_MAIN; ?>"
		</IFRAME>
	</body>
</html>

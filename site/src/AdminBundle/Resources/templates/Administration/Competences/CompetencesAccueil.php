<?php
/**
 * Accueil de la page compétences (Administration des droits)
 * @author Alkante
 */
  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Modules/BO/CompetenceVO.php");
  require_once($AdminPath."/Components/EditForm/EditForm.php");
  require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CompetenceVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">

<?php
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = "";//Ressources::$ACTION_OPEN_COMPETENCES_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }

  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if ( $PK==-1 )
  {
    exit;
  }

  $dao = new DAO($conn, 'catalogue');

  $accessRights = new AccessRights( );

  $competenceVO = new CompetenceVO();
  $competenceVO->setDao($dao);

  $ordinalList = array();
  $ordinalListDisp = array();
  $ordinalType = array();
  $ordinalSize = array();   
  $ordinalReadOnly = array(); 
  

  $ordinalList[]=CompetenceVO::$COMPETENCE_NOM;
  $ordinalListDisp[]="Nom :";
  $ordinalType[]=EditForm::$TYPE_TEXT;
  $ordinalSize[]=30;
  $ordinalReadOnly[]= !$accessRights->CanUpdate( $competenceVO, CompetenceVO::$COMPETENCE_NOM, $PK);
  print('<br><br>');

  $editForm = new EditForm( "EditFormCompetences",
                $competenceVO,
                CompetenceVO::$PK_COMPETENCE_ID,
                $PK,
                $ordinalList,
                $ordinalListDisp,
                $ordinalType,
                $ordinalSize,
                $ordinalReadOnly,
                '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Competences/CompetencesAccueil.php",*/
                $Action,
                null,
                false,
                false,
                array(),
                $submitUrl
              );

?>
  </body>
</html>
<?php
/**
 * Onglet compétences dans la gestion des cartes
 * @author Alkante
 */
  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Components/RelationList/RelationList.php");
  require_once($AdminPath."/Components/SelectList/SelectList.php"); 
  require_once($AdminPath."/Modules/BO/CompetenceVO.php");
  require_once($AdminPath."/Modules/BO/CarteProjetVO.php");
  require_once($AdminPath."/Modules/BO/CompetenceAccedeCarteVO.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Common\Components\SelectList\SelectList;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CompetenceVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CarteProjetVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CompetenceAccedeCarteVO;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php     
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_MAPS_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if ( $PK==-1 )
  {
    exit;
  }

  // set a default ID
  if ( !isset($_GET["RR"]) )
  {
    $RELATIONLIST_RESULT = -1;
  }
  else
  {
    $RELATIONLIST_RESULT = $_GET["RR"]; 
  }

  // #################################################################

  $dao = new DAO($conn, 'catalogue');

  if ( $RELATIONLIST_RESULT != -1 )
  {
    switch( $Action )
    {
      case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
        $ordinalList = array();
        $valueList = array();
        $ordinalList[]=CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_COMPETENCE_ID;
        $ordinalList[]=CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_CARTE_PROJET;
        $valueList[]=$RELATIONLIST_RESULT;
        $valueList[]=$PK;

        $CompetenceAccedeCarteVO_RL = new CompetenceAccedeCarteVO();
        $CompetenceAccedeCarteVO_RL->setDao($dao);
        $CompetenceAccedeCarteVO_RL->InsertValues( $ordinalList, $valueList );
        $CompetenceAccedeCarteVO_RL->Commit();
      break;

      case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
        $CompetenceAccedeCarteVO_RL = new CompetenceAccedeCarteVO();
        $CompetenceAccedeCarteVO_RL->setDao($dao);
        $CompetenceAccedeCarteVO_RL->DeleteRow( CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_COMPETENCE_ID, $RELATIONLIST_RESULT );
        $CompetenceAccedeCarteVO_RL->Commit();
      break;
    }
    AlertSaveController::AlertSaveDone("", false);
  }

  // #################################################################

  $CompetenceAccCoucheVO = new CompetenceAccedeCarteVO();
  $CompetenceAccCoucheVO->setDao($dao);
  $carteProjetVO = new CarteProjetVO();
  $carteProjetVO->setDao($dao);

  $CompetenceAccedeCarteVO2 = new CompetenceAccedeCarteVO();
  $CompetenceAccedeCarteVO2->setDao($dao);
  $CompetenceAccedeCarteVO2->AddRestriction( CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_COMPETENCE_ID, $PK );
  $CompetenceAccedeCarteVO2->AddOnlyKeyProjection( CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_CARTE_PROJET );

  $carteProjetVO->AddNotInRestriction( CarteProjetVO::$PK_CARTE_PROJET, $CompetenceAccedeCarteVO2 );
  

  $relationList = new RelationList( "RelationListMapAccCompetence",
                   Ressources::$RELATIONLIST_COMPETENCE_MAPS_RELATION,
                   $CompetenceAccCoucheVO,
                   CompetenceAccedeCarteVO::$COMPETENCECARTE_CARTEPROJET,
                   CompetenceAccedeCarteVO::$COMPETENCECARTE_CARTP_NOM,
                   CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_COMPETENCE_ID,
                   $PK,
                   CompetenceAccedeCarteVO::$COMPETENCECARTE_FK_CARTE_PROJET,
                   $carteProjetVO,
                   Ressources::$RELATIONLIST_COMPETENCE_MAPS_TABLE,
                   CarteProjetVO::$PK_CARTE_PROJET,
                   CarteProjetVO::$CARTP_NOM,
                   '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Competences/CompetencesMaps.php",*/
                   Ressources::$IFRAME_NAME_MAIN,
                   $this
                   );
  
?>
  </body>
</html>
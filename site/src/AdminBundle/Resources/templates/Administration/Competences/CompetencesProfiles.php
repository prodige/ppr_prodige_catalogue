<?php
/**
 * Onglet compétence dans la gestion des profils
 * @author Alkante
 */
  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  
  require_once($AdminPath."/Modules/BO/GrpAccedeCompetenceVO.php");
  require_once($AdminPath."/Modules/BO/CompetenceVO.php");
  require_once($AdminPath."/Modules/BO/GroupeProfilVO.php");
  require_once($AdminPath."/Components/RelationList/RelationList.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;

  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GrpAccedeCompetenceVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\CompetenceVO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GroupeProfilVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\RelationList\RelationList;
  use ProdigeCatalogue\AdminBundle\Controller\AlertSaveController;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php   
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_PROFILES_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if ( $PK==-1 )
  {
    exit;
  }

  // set a default ID
  if ( !isset($_GET["RR"]) )
  {
    $RELATIONLIST_RESULT = -1;
  }
  else
  {
    $RELATIONLIST_RESULT = $_GET["RR"]; 
  }

  $dao = new DAO($conn, 'catalogue');

  // #################################################################
  
  if ( $RELATIONLIST_RESULT != -1 )
  {
    switch( $Action )
    {
      case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
        $ordinalList = array();
        $valueList = array();
        $ordinalList[]=GrpAccedeCompetenceVO::$FK_COMPETENCE_ID;
        $ordinalList[]=GrpAccedeCompetenceVO::$FK_GRP_ID;
        $valueList[]=$PK;
        $valueList[]=$RELATIONLIST_RESULT;

        $grpAccedeSSDomVO_RL = new GrpAccedeCompetenceVO();
        $grpAccedeSSDomVO_RL->setDao($dao);
        $grpAccedeSSDomVO_RL->InsertValues( $ordinalList, $valueList );
        $grpAccedeSSDomVO_RL->Commit();
      break;
      
      case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
        $GrpAccedeCompetenceVO_RL = new GrpAccedeCompetenceVO();
        $GrpAccedeCompetenceVO_RL->setDao($dao);
        $GrpAccedeCompetenceVO_RL->DeleteRow( GrpAccedeCompetenceVO::$FK_COMPETENCE_ID, $RELATIONLIST_RESULT );
        $GrpAccedeCompetenceVO_RL->Commit();
      break;
    }
    AlertSaveController::AlertSaveDone("", false);
  }
  
  // #################################################################


  $GrpAccedeCompetenceVO = new GrpAccedeCompetenceVO();
  $GrpAccedeCompetenceVO->setDao($dao);

  $groupeProfilVO = new GroupeProfilVO();
  $groupeProfilVO->setDao($dao);

  $GrpAccedeCompetenceVO2 = new GrpAccedeCompetenceVO();
  $GrpAccedeCompetenceVO2->setDao($dao);
  $GrpAccedeCompetenceVO2->AddRestriction( GrpAccedeCompetenceVO::$FK_COMPETENCE_ID, $PK );
  $GrpAccedeCompetenceVO2->AddOnlyKeyProjection( GrpAccedeCompetenceVO::$FK_GRP_ID );
  
  $groupeProfilVO->AddNotInRestriction( GroupeProfilVO::$PK_GROUPE_PROFIL, $GrpAccedeCompetenceVO2 );
  
  $relationList = new RelationList( "RelationListProfileCOMPETENCE",
                   Ressources::$RELATIONLIST_PROFILES_COMPETENCE_RELATION,
                   $GrpAccedeCompetenceVO,
                   GrpAccedeCompetenceVO::$FK_GRP_ID,
                   GrpAccedeCompetenceVO::$GRPACCCOMPETENCE_GRP_ID,
                   
                   GrpAccedeCompetenceVO::$FK_COMPETENCE_ID,
                   $PK,
                   GrpAccedeCompetenceVO::$FK_GRP_ID,
                   $groupeProfilVO,
                   Ressources::$RELATIONLIST_PROFILES_COMPETENCE_TABLE,
                   GroupeProfilVO::$PK_GROUPE_PROFIL,
                   GroupeProfilVO::$GRP_ID,
                   '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Profiles/ProfilesCompetences.php",*/
                   Ressources::$IFRAME_NAME_MAIN,
                   $this
                   );
  
?>
  </body>
</html>
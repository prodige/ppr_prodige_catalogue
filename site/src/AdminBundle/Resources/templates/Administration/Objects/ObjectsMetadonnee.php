<?php
/**
 * Onglet métadonnée dans la gestion des traitements par profil
 * @author Alkante
 */
  require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Modules/BO/GrpTrtObjetVO.php");
  require_once($AdminPath."/Modules/BO/ObjetVO.php");
  require_once($AdminPath."/Modules/BO/TrtObjetVO.php");
  require_once($AdminPath."/Components/RelationList/RelationList.php");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php   
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_OBJECTS_METADONNEE;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if ( !isset($_GET["Profil"]) )
  {
    $PROFIL = -1;
  }
  else
  {
    $PROFIL = intval( $_GET["Profil"] );  
  }
  
  if ( $PK==-1 ||$PROFIL ==-1 )
  {
    exit;
  }
 
  // set a default ID
  if ( !isset($_GET["RR"]) )
  {
    $RELATIONLIST_RESULT = -1;
  }
  else
  {
    $RELATIONLIST_RESULT = $_GET["RR"]; 
  }
  
  //gestion à l'objet
  $bdisabled = true;
  if ( !isset($_GET["bObjTrt"]) )
  {
    $status = IsObjectManaged($PK, PRO_OBJET_TYPE_METADONNEE, $PROFIL);
    if ($status!=-1)
      $bdisabled = false;  
  }
  else
  {
    $bdisabled = intval( $_GET["bObjTrt"] );
    if ($bdisabled){
      //insertion de status = 0 dans la table grp_trt_objet 
      $ordinalList = array();
      $valueList = array();
      $ordinalList[]=GrpTrtObjetVO::$FK_GRP_ID;
      $ordinalList[]=GrpTrtObjetVO::$FK_OBJET_ID;
      $ordinalList[]=GrpTrtObjetVO::$FK_TRT_ID;
      $ordinalList[]=GrpTrtObjetVO::$FK_OBJ_TYPE_ID;
      $valueList[]=$PROFIL;
      $valueList[]=$PK;
      $valueList[]=PRO_TRT_TYPE_MODIFICATION;
      $valueList[]=PRO_OBJET_TYPE_METADONNEE;
      
      $valueList2[]=$PROFIL;
      $valueList2[]=$PK;
      $valueList2[]=PRO_TRT_TYPE_PROPOSITION;
      $valueList2[]=PRO_OBJET_TYPE_METADONNEE;
      
      $valueList3[]=$PROFIL;
      $valueList3[]=$PK;
      $valueList3[]=PRO_TRT_TYPE_PUBLICATION;
      $valueList3[]=PRO_OBJET_TYPE_METADONNEE;
      
      $valueList4[]=$PROFIL;
      $valueList4[]=$PK;
      $valueList4[]=PRO_TRT_TYPE_SUPRESSION;
      $valueList4[]=PRO_OBJET_TYPE_METADONNEE;
     
      $valueList5[]=$PROFIL;
      $valueList5[]=$PK;
      $valueList5[]=PRO_TRT_TYPE_VALIDATION;
      $valueList5[]=PRO_OBJET_TYPE_METADONNEE;
      
      $GrpTrtObjetVO_RL = new GrpTrtObjetVO();
      $GrpTrtObjetVO_RL->InsertValues( $ordinalList, $valueList );
      $GrpTrtObjetVO_RL->InsertValues( $ordinalList, $valueList2 );
      $GrpTrtObjetVO_RL->InsertValues( $ordinalList, $valueList3 );
      $GrpTrtObjetVO_RL->InsertValues( $ordinalList, $valueList4 );
      $GrpTrtObjetVO_RL->InsertValues( $ordinalList, $valueList5 );
      $GrpTrtObjetVO_RL->Commit();
      $bdisabled = false;
    }else{
      //supression de la gestion sur cet objet pour tous les traitements
      $GrpTrtObjetVO_RL = new GrpTrtObjetVO();
      $GrpTrtObjetVO_RL->DeleteRow( GrpTrtObjetVO::$FK_OBJET_ID, $PK." and GRPTRTOBJ_FK_OBJ_TYPE_ID = ".PRO_OBJET_TYPE_METADONNEE );
      $GrpTrtObjetVO_RL->Commit();
      $bdisabled = true;
    }
  }
  
  print('<table align =\'center\'><tr><td>Gestion à l\'objet</td><td><INPUT   TYPE="checkbox" 
                              NAME="check_objet" 
                              VALUE="0" 
                              ' . ($bdisabled ? '' : 'CHECKED ').
                              'onchange="javascript:GestionObjet('.$bdisabled.')";
                              ></td></tr></table>');
  print('
        <script language="javascript" type="text/javascript">
          
            function GestionObjet(bdisabled) 
            {
              var bObjTrt = 0; 
              if (bdisabled)
                var bObjTrt = 1;
              document.location.href =" '.$_SERVER["REQUEST_URI"].'&bObjTrt="+bObjTrt;
            
            }
        </script>
      ');     
  if ( $RELATIONLIST_RESULT != -1 )
  {
    switch( $Action )
    {
      case Ressources::$RELATIONLIST_ADD_TO_RELATION_ACTION :
        $ordinalRetriction= GrpTrtObjetVO::$FK_GRP_ID;
        $valueRestriction = $PROFIL. " and ".
                            "GRPTRTOBJ_FK_OBJET_ID = ". $PK." and ".
                            "GRPTRTOBJ_FK_TRT_ID = ".$RELATIONLIST_RESULT."  and ".
                            "GRPTRTOBJ_FK_OBJ_TYPE_ID = ".PRO_OBJET_TYPE_METADONNEE;

        $ordinalList[]= GrpTrtObjetVO::$GRP_TRT_OBJET_STATUS;
        $valueList[]=1; 
        
        $GrpTrtObjetVO_RL = new GrpTrtObjetVO();
        $GrpTrtObjetVO_RL->Update( $ordinalList, $valueList, $ordinalRetriction, $valueRestriction );
        $GrpTrtObjetVO_RL->Commit();
      break;
      
      case Ressources::$RELATIONLIST_REMOVE_FROM_RELATION_ACTION :
        $ordinalRetriction= GrpTrtObjetVO::$PK_GPR_TRT_OBJET;
        $valueRestriction = $RELATIONLIST_RESULT;
        $ordinalList[]= GrpTrtObjetVO::$GRP_TRT_OBJET_STATUS;
        $valueList[]=0; 
        
        $GrpTrtObjetVO_RL = new GrpTrtObjetVO();
        $GrpTrtObjetVO_RL->Update( $ordinalList, $valueList, $ordinalRetriction, $valueRestriction );
        $GrpTrtObjetVO_RL->Commit();
      break;
    }
  }
  
  // #################################################################
  $Action = Ressources::$ACTION_OPEN_OBJECTS_METADONNEE;

  $GrpTrtObjetVO = new GrpTrtObjetVO();
  $GrpTrtObjetVO->AddRestriction( GrpTrtObjetVO::$GRPTRTOBJET_GROUPE_PROFIL, $PROFIL);
  $GrpTrtObjetVO->AddRestriction( GrpTrtObjetVO::$FK_OBJ_TYPE_ID, PRO_OBJET_TYPE_METADONNEE);
  $GrpTrtObjetVO->AddRestriction( GrpTrtObjetVO::$GRPTRTOBJET_OBJETTYPE, PRO_OBJET_TYPE_METADONNEE);
  $GrpTrtObjetVO->AddRestriction( GrpTrtObjetVO::$GRP_TRT_OBJET_STATUS, 1);
  
  $TrtObjetVO = new TrtObjetVO();
  $TrtObjetVO->AddRestriction( TrtObjetVO::$TRT_AUTORISE_OBJET_OBJ_TYPE_ID, PRO_OBJET_TYPE_METADONNEE);
  
  $GrpTrtObjetVO2 = new GrpTrtObjetVO();
  $GrpTrtObjetVO2->AddRestriction( GrpTrtObjetVO::$GRPTRTOBJET_GROUPE_PROFIL, $PROFIL );
  $GrpTrtObjetVO2->AddRestriction( GrpTrtObjetVO::$GRPTRTOBJET_OBJET_ID, $PK);
  $GrpTrtObjetVO2->AddRestriction( GrpTrtObjetVO::$FK_OBJ_TYPE_ID, PRO_OBJET_TYPE_METADONNEE);
  $GrpTrtObjetVO2->AddRestriction( GrpTrtObjetVO::$GRPTRTOBJET_OBJETTYPE, PRO_OBJET_TYPE_METADONNEE);
  $GrpTrtObjetVO2->AddRestriction( GrpTrtObjetVO::$GRP_TRT_OBJET_STATUS, 1);
  
  
  $GrpTrtObjetVO2->AddOnlyKeyProjection( GrpTrtObjetVO::$FK_TRT_ID );
  
  $TrtObjetVO->AddNotInRestriction( TrtObjetVO::$PK_TRT_OBJET, $GrpTrtObjetVO2 );
  
  $relationList = new RelationList( "RelationListObjectMetadonnee",
                   Ressources::$RELATIONLIST_OBJECTS_TRT_RELATION,
                   $GrpTrtObjetVO,
                   GrpTrtObjetVO::$PK_GPR_TRT_OBJET,
                   GrpTrtObjetVO::$GRPTRTOBJET_TRT_ID,
                   GrpTrtObjetVO::$GRPTRTOBJET_OBJET_ID,
                   $PK,
                   GrpTrtObjetVO::$FK_TRT_ID,
                   $TrtObjetVO,
                   Ressources::$RELATIONLIST_OBJECTS_TRT_TABLE,
                   TrtObjetVO::$PK_TRT_OBJET,
                   TrtObjetVO::$TRT_ID,
                   "droits.php?url=Administration/Objects/ObjectsMetadonnee.php",
                   Ressources::$IFRAME_NAME_MAIN.$Action,
                   $this,
                   "&Profil=".$PROFIL,
                   $bdisabled
                   );
  
    /**
       * @brief retourne le droit de traitement sur un objet 
       * @param objet identifiant de l'objet
       * @param objet_type identifiant du type d'objet
       * @param grp identifiant du groupe de l'utilisateur courant
       */
      function IsObjectManaged($objet, $objet_type, $grp){
        
        $query = 'SELECT GRP_TRT_OBJET_STATUS FROM GRP_TRT_OBJET ' .
                 'WHERE GRPTRTOBJ_FK_GRP_ID = '.$grp.
                 ' AND GRPTRTOBJ_FK_OBJET_ID = '.$objet.
                 ' AND GRPTRTOBJ_FK_OBJ_TYPE_ID = '.$objet_type;
        $dao = new DAO();
        $status = -1;
        if ($dao){
          $rs = $dao->BuildResultSet($query);
          for ($rs->First(); !$rs->EOF(); $rs->Next())
            $status = $rs->Read(0);
        }
        return $status;
      }
?>
  </body>
</html>
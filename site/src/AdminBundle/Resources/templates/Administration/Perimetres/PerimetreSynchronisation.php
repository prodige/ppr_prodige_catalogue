<?php 

/**
 * Author : Alkante
 * Service de synchronisation des  périmètres d'un zonage
 * se base sur les données postées au service writeBOV.php
 * 
 */

//require_once($AdminPath."/DAO/DAO/DAO.php");
use Prodige\ProdigeBundle\DAOProxy\DAO;

// set a default ID
if ( !isset($_POST["Id"]) )
{
  $PK = -1;
}
else
{
  $PK = intval( $_POST["Id"] );  
}

if ( $PK==-1 )
{
	exit;
}

if($PK == -999){
	//variable assignée pour la nouvelle valeur de séquence
  global  $valueRestriction;
	$PK = $valueRestriction;
}

//$dao = new DAO();
$dao = new DAO($conn, 'catalogue');

$query = 'SELECT PK_ACCES_SERVEUR, ACCS_ID, ACCS_ADRESSE_ADMIN FROM ACCES_SERVEUR ORDER BY ACCS_ID';
$rs = $dao->BuildResultSet($query);
if($rs->GetNbRows()>1){
  echo "erreur, plus d'un enregistrement dans la table acces_serveur";
  die();
}
for ($rs->First(); !$rs->EOF(); $rs->Next())
{
  $acces_adress_admin = $rs->Read(2);
}

$query = 'SELECT zonage_field_id, zonage_field_name from zonage where pk_zonage_id   = '.$PK;
$rs = $dao->BuildResultSet($query);

for ($rs->First(); !$rs->EOF(); $rs->Next())
{
  $fieldId = $rs->Read(0);
  $fieldName = $rs->Read(1);
}


$query = "select distinct ".$fieldId.", ".$fieldName." from public.prodige_perimetre ";

global $conn_prodige;
$dao = new DAO($conn_prodige, 'public');
$rs = $dao->BuildResultSet($query);
for ($rs->First(); !$rs->EOF(); $rs->Next()){
    $tabPerimetre[$rs->Read(0)] = $rs->Read(1);
}

$dao = new DAO($conn, 'catalogue');

$query = 'delete from perimetre where perimetre_zonage_id='.$PK.";";
foreach($tabPerimetre as $key =>$value){
	$query.="insert into perimetre(perimetre_code, perimetre_nom, perimetre_zonage_id) values ('".str_replace("'","''", $key)."','".str_replace("'","''", $value)."',".$PK.");"; 
}
$dao->Execute($query);


?>
<?php
/**
 * Accueil de la page composition
 * @author Alkante
 */
//require_once($AdminPath."/DAO/DAO/DAO.php");
use Prodige\ProdigeBundle\DAOProxy\DAO;

// set a default ID
if ( !isset($_GET["Id"]) )
{
  $PK = -1;
}
else
{
  $PK = intval( $_GET["Id"] );  
}

if ( $PK==-1 )
{
  exit;
}

//$dao = new DAO();
$dao_catalogue = new DAO($conn_catalogue, 'catalogue');

$query = 'SELECT zonage_field_id, zonage_field_name from zonage where zonage_minimal = 1';
$rs = $dao_catalogue->BuildResultSet($query);
if($rs->GetNbRows()>1){
  echo "erreur, plus d'un enregistrement dans la table acces_serveur";
  die();
}

$tabParam = array();

for ($rs->First(); !$rs->EOF(); $rs->Next())
{
  $tabParam["init_zonage_field_id"] = $rs->Read(0);
  $tabParam["init_zonage_field_name"] = $rs->Read(1);
}

$query = 'SELECT perimetre_code, zonage_field_id from perimetre inner join zonage on zonage.pk_zonage_id = perimetre.perimetre_zonage_id where pk_perimetre_id = '.$PK;
$rs = $dao_catalogue->BuildResultSet($query);

for ($rs->First(); !$rs->EOF(); $rs->Next())
{
  $tabParam["perimetre_code"] = $rs->Read(0);
  $tabParam["zonage_field_id"] = $rs->Read(1);
}

$tabParam["perimetre_id"] = $PK;

// Modified by hismail
/*$postdata = http_build_query(
    $tabParam
);

$opts = array
(
  'http'=>array
  (
    'method'  => 'POST',
    'header'  => 'Content-type: application/x-www-form-urlencoded',
    'content' => $postdata
  )
);

$context  = stream_context_create($opts);

$result = file_get_contents((isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"]=="on" ? "https" : "http")."://".$acces_adress_admin."/PRRA/Administration/Administration/Perimetres/perimetreComposition.php", false, $context);
echo $result;*/

include_once(__DIR__.'/../Services/perimetreComposition.php');

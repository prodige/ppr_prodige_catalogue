<?php
/**
 * Accueil de la page compétences (Administration des droits)
 * @author Alkante
 */
  /*require_once($AdminPath."/Ressources/Administration/Ressources.php");
  require_once($AdminPath."/DAO/DAO/DAO.php");
  require_once($AdminPath."/Modules/BO/PerimetreVO.php");
  require_once($AdminPath."/Components/EditForm/EditForm.php");
  require_once($AdminPath."/Administration/AccessRights/AccessRights.php");*/

  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\PerimetreVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  </head>
  <body style="margin:0px;">
<?php   
  // set a default action
  if ( !isset($_GET["Action"]) )
  {
    $Action = Ressources::$ACTION_OPEN_PERIMETRES_ACCUEIL;
  }
  else
  {
    $Action = $_GET["Action"];
  }
  
  // set a default ID
  if ( !isset($_GET["Id"]) )
  {
    $PK = -1;
  }
  else
  {
    $PK = intval( $_GET["Id"] );  
  }
  
  if(!isset($_GET["bReturnFromCarto"]))
  {
    $bReturnFromCarto = false;
  }
  else
  {
    $bReturnFromCarto = true;
  }
  
  if ( $PK==-1 )
  {
    exit;
  }

  $dao = new DAO($conn, 'catalogue');

  $accessRights = new AccessRights( );

  $PerimetreVO = new PerimetreVO();
  $PerimetreVO->setDao($dao);
  $ordinalList = array();
  $ordinalListDisp = array();
  $ordinalType = array();
  $ordinalSize = array();   
  $ordinalReadOnly = array(); 
  

  $ordinalList[]=PerimetreVO::$PERIMETRE_NOM;
  $ordinalListDisp[]="Nom :";
  $arraySelect[] = array();
  $ordinalType[]=EditForm::$TYPE_TEXT;
  $ordinalSize[]=30;
  $ordinalReadOnly[]= !$accessRights->CanUpdate( $PerimetreVO, PerimetreVO::$PERIMETRE_NOM, $PK);
  
  $ordinalList[]=PerimetreVO::$PERIMETRE_CODE;
  $ordinalListDisp[]="Code :";
  $arraySelect[] = array();
  $ordinalType[]=EditForm::$TYPE_TEXT;
  $ordinalReadOnly[]=  ($PK!=-999);
  
  $ordinalList[]=PerimetreVO::$PERIMETRE_ZONAGE_ID;
  
  //$dao = new DAO();
  if ($dao)
  { 
    $query = 'SELECT pk_zonage_id, zonage_nom, zonage_field_id FROM zonage where zonage_minimal=0;';
    $rs = $dao->BuildResultSet($query);
    $bHasOneZonage = false;
    //tableau de correspondance id => zonage_field_id (utilisé à la validation)
    $strJs = "var tabZonage=new Array();";
    $tabZonages = array();
    for ($rs->First(); !$rs->EOF(); $rs->Next())
    {
      $bHasOneZonage = true;
    	$pk_zonage_id = $rs->Read(0);
      $zonage_nom = $rs->Read(1);
      $zonage_field_id = $rs->Read(2);
      $strJs.= "tabZonage['".$pk_zonage_id."']='".$zonage_field_id."';";
      $tabZonages[$pk_zonage_id] = $zonage_nom;
    } 
    $query = 'SELECT perimetre_code, zonage_field_id from perimetre inner join zonage on zonage.pk_zonage_id = perimetre.perimetre_zonage_id where pk_perimetre_id = '.$PK;
    $rs = $dao->BuildResultSet($query);
    
    for ($rs->First(); !$rs->EOF(); $rs->Next())
    {
      $perimetre_code = $rs->Read(0);
      $zonage_field_id = $rs->Read(1);
    }

  }
  
  unset($dao);
  
  if(!$bHasOneZonage){
  	echo "<script language='javascript' type='text/javascript'>parent.Ext.Msg.alert('Création d\'un territoire', 'Veuillez créer au préalable des zonages.');</script></body></html>";exit();
  	
  }
  
  $ordinalListDisp[]="Zonage :";
  $ordinalType[]=EditForm::$TYPE_SELECT;
  $ordinalSize[]=10;
  $arraySelect[] = $tabZonages;
  $ordinalReadOnly[]=  ($PK!=-999);
  print('<br><br>');
  
  $editForm = new EditForm( "EditFormPerimetres",
                $PerimetreVO,
                PerimetreVO::$PK_PERIMETRE_ID,
                $PK,
                $ordinalList,
                $ordinalListDisp,
                $ordinalType,
                $ordinalSize,
                $ordinalReadOnly,
                '', /*TODO hismail - argument ne sera pas utilisé ... $AdminPath."/Administration/Perimetres/PerimetresAccueil.php",*/
                $Action,null, false, false, $arraySelect,
                $submitUrl);
  
  
  echo "<script language='javascript' type='text/javascript'>
            ".(isset($strJs) ? $strJs : "")."
            //vérification de l'unicité du champ code
            function EditFormPerimetresValidate_onclick() 
            {
              parent.Ext.Ajax.request({
                //url :  window.location.protocol+'//".$acces_adress_admin."/PRRA/Administration/Administration/Perimetres/PerimetresUnique.php', 
                url :  '$perimetresUniqueUrl',
  	            method : 'GET',
                params : {
                  'perimetre_code' : document.getElementById('ORDINAL_1').value,
                  'zonage_field_id' : tabZonage[document.getElementById('ORDINAL_4').value]
                }, 
                success: function(result){
                 var result = setResponseObject(result);
                 if(result.responseObject.success){
	                  var form = document.getElementById('EditFormPerimetres');
			              var inputId = document.getElementById('Id');
			              var inputAction = document.getElementById('Action');

			              //form.action = 'PRO_CATALOGUE_URLBASE/droits.php?url=Administration/Components/EditForm/WriteBOV.php';
			              form.action = '$submitUrl';
                    form.submit();
		              }else{
		                 parent.Ext.MessageBox.alert('Erreur', 'Le code doit être unique pour le zonage'); 
		              }
	              },failure: function(result){
                  parent.Ext.MessageBox.alert('Erreur', 'erreur d\'enregistrement de la table'); 
                } 
              });
            }
  
  
  
            function EditFormPerimetresDelete_onclick() 
            {
              parent.Ext.Ajax.request({
                //url :  window.location.protocol+'//".$acces_adress_admin."/PRRA/Administration/Administration/Perimetres/PerimetresDelete.php', 
                url :  '$perimetresDeleteUrl',
                method : 'GET',
                params : {
                  'perimetre_code' : '".$perimetre_code."',
                  'zonage_field_id' : '".$zonage_field_id."'
                }, 
                success: function(result){  
                  var form = document.getElementById('EditFormPerimetres');
                
                  var inputId = document.getElementById('Id');
                  var inputAction = document.getElementById('Action');
                  
                  inputAction.value = '".Ressources::$EDITFORM_DELETE."'; 
	                //form.action = 'PRO_CATALOGUE_URLBASE/droits.php?url=Administration/Components/EditForm/WriteBOV.php';
                  form.action = '$submitUrl';
                  if (confirm('Êtes-vous sûr de vouloir supprimer cet enregistrement ?')){;
                   form.submit();
                  }
                },failure: function(result){
                  parent.Ext.MessageBox.alert('Failed', 'erreur d\'enregistrement de la table'); 
                }  
              });
            }
        </script>
  ";                
?>
  </body>
</html>
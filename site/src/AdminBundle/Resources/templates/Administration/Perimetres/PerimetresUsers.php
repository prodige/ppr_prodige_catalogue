<?php
  use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
  use Prodige\ProdigeBundle\DAOProxy\DAO;
  use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;
  use ProdigeCatalogue\AdminBundle\Common\Components\EditForm\EditForm;
  use ProdigeCatalogue\AdminBundle\Common\AccessRights\AccessRights;
  
$Action = Ressources::$LIST_MODIFY_ACTION;
$PK = -1;

if (isset($_GET["Action"])) $Action = intval($_GET["Action"]);
if (isset($_GET["Id"])) 	$PK 	= intval($_GET["Id"]);
if ($PK == -1)  exit;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><?php echo Ressources::$PAGE_TITLE_MAIN; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script>


    function uploadFile(form, post_name, callback){
    	var action = form.action;
    	form.target = '<?php echo Ressources::$IFRAME_NAME_MAIN ?>';
    	var win = window.frames[form.target];
    	if ( !win ) {form.target = ""; win = window;};
    	window.callbackUpload = callback;
    	form.action = Routing.generate("<?php echo Ressources::$ROUTE_UPLOAD_TEMP ?>", {post_name : post_name});
    	form.submit();
    	form.action = action;
    }
		
		function UserPerimetre_Import(){
			var elt;
			var err1;

      err1 = "Vous devez définir un fichier d'import de type csv.";
			
			elt = document.getElementById("CSV_FILE");
			if (elt){
				if (elt.value == "") {
					alert(err1);
					return ;
				}
				var form = document.getElementById('importTerritoire');
				var callback = function(file){
	        var box = Ext.MessageBox.wait('Traitement du fichier...', 'Intégration de la liaison utilisateur/territoire');
	        parent.Ext.Ajax.request({
	          url :  form.action, 
	          method : 'GET',
	          params : {
	            'CSV_FILE' : escape(file)
	          }, 
	          timeout : 0,
	          success: function(result){   
	            box.hide();  
	            response = parent.Ext.decode(result.responseText);  
	            if(response.success){
	              parent.Ext.MessageBox.alert('Succès', "L'import a été réalisé avec succès");
	            }else{
	              parent.Ext.MessageBox.alert('Erreur', response.msg);
	            }
	          },failure: function(result){
	              box.hide();
	              parent.Ext.MessageBox.alert('Erreur', 'Erreur lors de la lecture du fichier'); 
	          } 
	         });
	         
				};
				uploadFile(form, elt.name, callback);
			}
			else {
        alert("Erreur interne:\nImpossible de récupérer l'élément Fichier");
			}
    }
		</script>
</head>
<body style="margin: 0px;">

<br><br>	

	<form name="importTerritoire" id="importTerritoire" method="POST" action="<?php echo $importTerritoire; ?>"  enctype="multipart/form-data">
<div align="center">
<table border="0" width="550px">
  <tr>
    <td colspan="2" style="font-size: 16px; font-weight: bold">
    Importation de la liaison entre les utiliateurs et les territoires</td>
  </tr>
  <tr>
    <td colspan="2">Cette interface permet l'import en masse de
    l'association entre les utilisateurs et leur territoires associés.<br>
    L'import se fait à partir d'un fichier CSV (séparateur virgule)
    contenant deux colonnes :
    <ul>
      <li>-<b> usr_id</b> : identifiant de l'utilisateur</li>
      <li>-<b> territoire_code</b> : identifiant du territoire concerné</li>
    </ul>
    <a style="text-decoration:underline" target="_blank" href="<?php echo str_replace("/app_dev.php", "", PRO_CATALOGUE_URLBASE);?>/modeles/importTerritoire.csv">Exemple de fichier CSV</a>

    </td>
  </tr>
  <tr>
    <td>Fichier à importer :</td>
    <td valign="top">
					<input id="CSV_FILE" name="CSV_FILE" type="File" accept=".csv,.txt" width="100" >
  </tr>
  <tr>
    <td colspan="2" align="center"><br>
    <input type="button" value="Importer le fichier"
      onclick="UserPerimetre_Import()"></td>
  </tr>
  
</table>
</div>
	
	</form>
</body>
</html>

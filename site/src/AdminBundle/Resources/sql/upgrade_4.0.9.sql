--update xlinks, needs to restard tomcat
update metadata set data =  replace(replace (data, 'thesaurus=external.theme.prodige&amp;amp;id', 'thesaurus=external.theme.prodige&amp;id'), '&amp;amp;multiple=true&amp;amp;lang=fre', '&amp;multiple=true&amp;lang=fre')  where data like '%thesaurus=external.theme.prodige&amp;amp;id%' and isharvested='n' ;
-- SPECIFIC update metadata set changedate = '2017-06-12T00:00:00' where isharvested='n' and    schemaid ='iso19139' and istemplate='n' and changedate < '2017-06-12';
-- update metadata set data = replace (data, 'https://www.picto-occitanie.fr:80', 'https://www.picto-occitanie.fr')  where data like '%https://www.picto-occitanie.fr:80%' and isharvested='n';
update metadata set data = replace(data, '&amp;amp;fname=', '&amp;fname=') WHERE "data" ILIKE '%&amp;amp;fname=%' and isharvested='n';
update metadata set data = replace(data, '<error id="bad-parameter" xlink:href="local://fre/xml.keyword.get?thesaurus=&amp;amp;id=&amp;amp;multiple=false&amp;amp;lang=fre" xlink:show="replace" />', '') WHERE "data" ILIKE '%<error id="bad-parameter" xlink:href="local://fre/xml.keyword.get?thesaurus=&amp;amp;id=&amp;amp;multiple=false&amp;amp;lang=fre" xlink:show="replace" />%' and isharvested='n';
update metadata set data = replace(data, '&amp;amp;access=', '&amp;access=') WHERE "data" ILIKE '%&amp;amp;access=%' and isharvested='n';  

--DATE_EXPIRATION_COMPTE
alter table catalogue.utilisateur add column date_expiration_compte date;
  
-- clean users
update public.users set enabled=true;  
update metadata set data =  replace(data, '&amp;amp;amp;', '&amp;amp;')  where data like '%&amp;amp;amp;%' and isharvested='n';
update metadata set data = replace(data, '/geonetwork//srv/fre/xml.keyword.get?', '/geonetwork/srv/fre/xml.keyword.get?') where  isharvested='n';
update metadata set data = replace(data, '<gfc:featureCatalogue xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#" />', '') where  isharvested='n';
update metadata set data = replace(data, '<gfc:featureCatalogue xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#"/>', '') where  isharvested='n';
-- suppression des xlink locaux, non fonctionnels
update public.settings set value='false' where name='system/xlinkResolver/localXlinkEnable';
--update metadata set data = replace(data, 'local://fre/xml.keyword.get?', 'https://www-test.datara.gouv.fr/geonetwork/srv/fre/xml.keyword.get?') where  isharvested='n';
-- synchroniser les tables catalogue.utilisateur et public.users
update users set surname = utilisateur.usr_prenom , name = utilisateur.usr_nom from catalogue.utilisateur where username = utilisateur.usr_id



drop view catalogue.statistique_liste_metadata_publiees;
drop view catalogue.statistique_dernieres_mises_a_jour;
drop view catalogue.statistique_dernieres_nouveautes;
drop view catalogue.metadata_list;
drop view catalogue.statistique_liste_couche_publiees;
drop view catalogue.statistique_liste_carte_publiees;

create view catalogue.statistique_liste_couche_publiees as  SELECT couche_donnees.pk_couche_donnees,
    couche_donnees.couchd_nom AS "nom de la couche",
    couche_donnees.couchd_description AS "résumé",
    couche_donnees.couchd_type_stockage AS "couche vecteur ?",
    couche_donnees.couchd_emplacement_stockage AS "emplacement de stockage",
    couche_donnees.couchd_wms AS "Publiée en WMS ?",
    couche_donnees.couchd_wfs AS "Publiée en WFS ?",
    metadata.changedate,
    metadata.createdate,
    metadata.id AS "id metadata",
    metadata.uuid AS "uuid metadata"
   FROM catalogue.couche_donnees
     JOIN catalogue.fiche_metadonnees ON couche_donnees.pk_couche_donnees = fiche_metadonnees.fmeta_fk_couche_donnees
     JOIN metadata ON fiche_metadonnees.fmeta_id::bigint = metadata.id
     JOIN operationallowed ON metadata.id = operationallowed.metadataid AND operationallowed.operationid = 0 AND operationallowed.groupid = 1;

create view catalogue.statistique_liste_carte_publiees as SELECT carte_projet.pk_carte_projet,
    carte_projet.cartp_nom AS "nom de la carte",
    carte_projet.cartp_description AS "résumé",
    carte_projet.cartp_format AS format,
    stockage_carte.stkcard_path AS stockage,
    metadata.changedate,
    metadata.createdate,
    metadata.id AS "id metadata",
    metadata.uuid AS "uuid metadata"
   FROM catalogue.carte_projet
     JOIN catalogue.stockage_carte ON carte_projet.cartp_fk_stockage_carte = stockage_carte.pk_stockage_carte
     JOIN catalogue.fiche_metadonnees ON carte_projet.cartp_fk_fiche_metadonnees = fiche_metadonnees.pk_fiche_metadonnees
     JOIN metadata ON fiche_metadonnees.fmeta_id::bigint= metadata.id
     JOIN operationallowed ON metadata.id = operationallowed.metadataid AND operationallowed.operationid = 0 AND operationallowed.groupid = 1;;


create view catalogue.metadata_list as SELECT statistique_liste_couche_publiees."nom de la couche" AS nom,
    statistique_liste_couche_publiees."uuid metadata" AS id
   FROM catalogue.statistique_liste_couche_publiees
UNION
 SELECT statistique_liste_carte_publiees."nom de la carte" AS nom,
    statistique_liste_carte_publiees."uuid metadata" AS id
   FROM catalogue.statistique_liste_carte_publiees;


create view catalogue.statistique_dernieres_mises_a_jour as 
 SELECT statistique_liste_couche_publiees."nom de la couche" AS nom,
    statistique_liste_couche_publiees."résumé",
    statistique_liste_couche_publiees.changedate AS date,
    'donnée'::text AS type,
    statistique_liste_couche_publiees."uuid metadata" AS id,
    statistique_liste_couche_publiees."couche vecteur ?" AS data_type
   FROM catalogue.statistique_liste_couche_publiees
UNION
 SELECT statistique_liste_carte_publiees."nom de la carte" AS nom,
    statistique_liste_carte_publiees."résumé",
    statistique_liste_carte_publiees.changedate AS date,
    'carte'::text AS type,
    statistique_liste_carte_publiees."uuid metadata" AS id,
    statistique_liste_carte_publiees.format AS data_type
   FROM catalogue.statistique_liste_carte_publiees
  ORDER BY 3 DESC
 LIMIT 30;


create view catalogue.statistique_dernieres_nouveautes as
 SELECT statistique_liste_couche_publiees."nom de la couche" AS nom,
    statistique_liste_couche_publiees."résumé",
    statistique_liste_couche_publiees.changedate AS date,
    statistique_liste_couche_publiees.createdate,
    'donnée'::text AS type,
    statistique_liste_couche_publiees."uuid metadata" AS id,
    statistique_liste_couche_publiees."couche vecteur ?" AS data_type
   FROM catalogue.statistique_liste_couche_publiees
UNION
 SELECT statistique_liste_carte_publiees."nom de la carte" AS nom,
    statistique_liste_carte_publiees."résumé",
    statistique_liste_carte_publiees.changedate AS date,
    statistique_liste_carte_publiees.createdate,
    'carte'::text AS type,
    statistique_liste_carte_publiees."uuid metadata" AS id,
    statistique_liste_carte_publiees.format AS data_type
   FROM catalogue.statistique_liste_carte_publiees
  ORDER BY 4 DESC
 LIMIT 30;

create view catalogue.statistique_liste_metadata_publiees as 
SELECT statistique_liste_couche_publiees."nom de la couche" AS nom,
    statistique_liste_couche_publiees."résumé",
    statistique_liste_couche_publiees.changedate AS date,
    statistique_liste_couche_publiees.createdate,
    'donnée'::text AS type,
    statistique_liste_couche_publiees."id metadata" AS id,
    statistique_liste_couche_publiees."couche vecteur ?" AS data_type
   FROM catalogue.statistique_liste_couche_publiees
UNION
 SELECT statistique_liste_carte_publiees."nom de la carte" AS nom,
    statistique_liste_carte_publiees."résumé",
    statistique_liste_carte_publiees.changedate AS date,
    statistique_liste_carte_publiees.createdate,
    'carte'::text AS type,
    statistique_liste_carte_publiees."id metadata" AS id,
    statistique_liste_carte_publiees.format AS data_type
   FROM catalogue.statistique_liste_carte_publiees;
ALTER TABLE catalogue.grp_restriction_attributaire add column grprat_fk_trt_id integer;
ALTER TABLE ONLY catalogue.grp_restriction_attributaire ADD CONSTRAINT grprat_fk_traitement_fkey FOREIGN KEY (grprat_fk_trt_id) REFERENCES catalogue.traitement(pk_traitement) MATCH FULL ON DELETE CASCADE;
update catalogue.grp_restriction_attributaire set grprat_fk_trt_id= (select pk_traitement from catalogue.traitement where trt_id='EDITION EN LIGNE');
insert into catalogue.grp_restriction_attributaire (grprat_fk_couche_donnees, grprat_fk_groupe_profil, grprat_champ, grprat_fk_trt_id) select grprat_fk_couche_donnees, grprat_fk_groupe_profil, grprat_champ,2 from catalogue.grp_restriction_attributaire;

        
         
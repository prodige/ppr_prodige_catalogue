ALTER TABLE catalogue.groupe_profil ADD grp_template_mail TEXT;
ALTER TABLE catalogue.couche_donnees ADD column couchd_help_edition_msg TEXT;
DROP view catalogue.v_acces_couche;
CREATE view catalogue.v_acces_couche as 
        SELECT cd.pk_couche_donnees AS couche_pk,
        cd.couchd_id AS couche_id,
        cd.couchd_nom AS couche_nom,
        cd.couchd_description AS couche_desc,
        cd.couchd_emplacement_stockage AS couche_table,
        cd.couchd_type_stockage AS couche_type,
        cd.couchd_help_edition_msg AS couchd_help_edition_msg,
        acces_serveur.accs_adresse AS couche_srv,
        acces_serveur.accs_service_admin AS couche_service_admin,
        acces_serveur.accs_adresse_admin AS couche_srv_admin,
        acces_serveur.accs_login_admin,
        acces_serveur.accs_pwd_admin
       FROM catalogue.couche_donnees cd
         LEFT JOIN catalogue.acces_serveur ON cd.couchd_fk_acces_server = acces_serveur.pk_acces_serveur;

-- Ajout champ tampon sur lots
ALTER TABLE bdterr.bdterr_lot ADD COLUMN lot_buffer integer DEFAULT 0;

-- Ajout champs gestion type de découpage
ALTER TABLE bdterr.bdterr_lot ADD COLUMN lot_cutting_type text DEFAULT 'ref';
ALTER TABLE bdterr.bdterr_lot ADD COLUMN lot_mapping text;


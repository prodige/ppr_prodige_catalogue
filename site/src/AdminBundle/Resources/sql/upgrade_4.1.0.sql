/** @DATABASE CATALOGUE **/

/*
-- Each domain may have an associated service WMS
*/
alter table catalogue.sous_domaine add column ssdom_service_wms integer default 0;
alter table catalogue.sous_domaine add column ssdom_service_wms_uuid character varying(250);
/*
-- The column couchd_wms_sdom will manage the IDs of the subdomain of WMS publication. 
-- The global service WMS has the ID=-1. 
-- The value NULL means "no publication" 
*/
alter table catalogue.couche_donnees add column couchd_wms_sdom int[];
update catalogue.couche_donnees set couchd_wms_sdom=couchd_wms_sdom || -1 where couchd_wms=1;


/* last edit 24 août 2018
 */

delete from catalogue.trt_autorise_objet where trtautoobjet_fk_obj_type_id=3;
delete from catalogue.trt_autorise_objet where trtautoobjet_fk_trt_id in (select pk_trt_objet 	from catalogue.trt_objet where trt_id in ('MODIFICATION', 'PROPOSITION','VALIDATION', 'PUBLICATION'));

delete from catalogue.grp_trt_objet where  grptrtobj_fk_trt_id in (select pk_trt_objet 	from catalogue.trt_objet where trt_id in ('MODIFICATION', 'PROPOSITION','VALIDATION', 'PUBLICATION'));
delete from catalogue.grp_trt_objet where  grptrtobj_fk_obj_type_id = 3;
delete from catalogue.objet_type where pk_objet_type_id=3;-- suppression des métadonnées de la gestion à l'objet
delete from catalogue.trt_objet where trt_id in ('MODIFICATION', 'PROPOSITION','VALIDATION', 'PUBLICATION');

delete from catalogue.grp_autorise_trt where grptrt_fk_traitement 	 in (select pk_traitement from catalogue.traitement where trt_id in ('PROPOSITION', 'PUBLICATION'));
delete from catalogue.traitement where  trt_id in ('PROPOSITION', 'PUBLICATION');


--pgDump
ALTER TABLE catalogue.fiche_metadonnees ADD COLUMN schema varchar(128);

drop view catalogue.v_desc_metadata;
create view catalogue.v_desc_metadata as  SELECT m.id AS metadataid,
    cd.couchd_nom AS couchenom,
    cd.couchd_type_stockage AS couchetype,
    acs.accs_adresse AS couchesrv,
    acs.accs_service_admin AS couchesrv_service,
    acs.accs_adresse_tele AS couchesrv_tele,
    cd.couchd_emplacement_stockage AS couchetable,
    m.data AS metadataxml,
    fm.schema as schema
   FROM metadata m
     LEFT JOIN catalogue.fiche_metadonnees fm ON m.id::text = fm.fmeta_id::text
     JOIN catalogue.couche_donnees cd ON fm.fmeta_fk_couche_donnees = cd.pk_couche_donnees
     JOIN catalogue.acces_serveur acs ON cd.couchd_fk_acces_server = acs.pk_acces_serveur;

--Rawgraph

CREATE SEQUENCE catalogue.seq_rawgraph_couche_config
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE catalogue.rawgraph_couche_config (
    id integer DEFAULT nextval('catalogue.seq_rawgraph_couche_config'::regclass) NOT NULL,
    titre character varying(255) NOT NULL,
    resume text NOT NULL,
    serie_donnees_uuid character varying(255) NOT NULL,
    couche character varying(255) NOT NULL,
    colonnes text NOT NULL,
    uuid character varying(255) NOT NULL,
    config json
);

ALTER TABLE ONLY catalogue.rawgraph_couche_config
    ADD CONSTRAINT pk_rawgraph_couche_config PRIMARY KEY (id);

--update datacarto

set search_path to catalogue;

create sequence seq_utilisateur_carte;

create table utilisateur_carte (
  id                bigint      not null default nextval('seq_utilisateur_carte'::regclass),
  fk_utilisateur    integer     not null,
  fk_stockage_carte integer   not null,
  primary key (id)
);

create index idx_utilisateur_carte_fk_utilisateur on utilisateur_carte(fk_utilisateur);
create index idx_utilisateur_carte_fk_stockage_carte on utilisateur_carte(fk_stockage_carte);

alter table utilisateur_carte add CONSTRAINT fk_utilisateur_carte_fk_utilisateur FOREIGN KEY (fk_utilisateur) REFERENCES utilisateur (pk_utilisateur);
alter table utilisateur_carte add CONSTRAINT fk_utilisateur_carte_fk_stockage_carte FOREIGN KEY (fk_stockage_carte) REFERENCES stockage_carte (pk_stockage_carte);

create view v_utilisateur as 
select pk_utilisateur, usr_id, usr_nom, usr_prenom, usr_email
from utilisateur
where date_expiration_compte is null or date_expiration_compte is not null and date_expiration_compte>now();


-- Activation of the standards module

insert into catalogue.prodige_settings (prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type) 
values ('PRO_MODULE_STANDARDS', 'Module qualité activé', 'off', 'Module qualité activé', 'checkboxfield');

create table catalogue.standards_fournisseur(
    fournisseur_id serial not null, 
    fournisseur_name text not null, 
    fournisseur_url text not null,
    CONSTRAINT pk_standards_fournisseur PRIMARY KEY (fournisseur_id)
);
create table catalogue.standards_standard(
    standard_id serial not null, 
    fournisseur_id int not null, 
    standard_name text not null, 
    standard_url text not null,
    standard_database text,
 	  standard_schema text,
    fmeta_id int,
    CONSTRAINT pk_standards_standard   PRIMARY KEY (standard_id),
    CONSTRAINT fk_standard_fournisseur FOREIGN KEY (fournisseur_id) REFERENCES catalogue.standards_fournisseur (fournisseur_id),
    CONSTRAINT fk_standard_metadata    FOREIGN KEY (fmeta_id)       REFERENCES public.metadata (id)
);
create table catalogue.standards_conformite(
    metadata_id int not null,
    standard_id int not null,
    conformite_prefix varchar(100),
    conformite_suffix varchar(100),
    conformite_success boolean default true,
    conformite_report_url text,
    CONSTRAINT pk_standards_conformite  PRIMARY KEY (metadata_id),
    CONSTRAINT fk_conformite_metadata   FOREIGN KEY (metadata_id)   REFERENCES public.metadata (id),
    CONSTRAINT fk_conformite_standard   FOREIGN KEY (standard_id)   REFERENCES catalogue.standards_standard (standard_id)
);

insert into catalogue.standards_fournisseur(fournisseur_name, fournisseur_url) values ('Prodige', 'prodige.org');


-- Base territoriale module

insert into catalogue.prodige_settings (prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type) 
values ('PRO_MODULE_BASE_TERRITORIALE', 'Module Base territoriale activé', 'off', 'Module Base territoriale activé', 'checkboxfield');

CREATE SCHEMA bdterr;
set search_path='bdterr';
CREATE SEQUENCE bdterr.referentiel_recherche_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE bdterr.champtype_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE bdterr.rapport_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE bdterr.contenu_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE bdterr.lot_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE bdterr.bdterr_referentiel_carto_referentiel_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE bdterr.referentiel_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE bdterr.champ_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE bdterr.donnee_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE bdterr.theme_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE bdterr.rapports_profils_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE catalogue.scheduled_command_id_seq INCREMENT BY 1 MINVALUE 1 START 1;
CREATE SEQUENCE catalogue.execution_report_id_seq INCREMENT BY 1 MINVALUE 1 START 1;


CREATE TABLE bdterr.bdterr_referentiel_recherche (referentiel_id INT NOT NULL, referentiel_table VARCHAR(255) DEFAULT NULL, referentiel_insee VARCHAR(255) DEFAULT NULL, referentiel_insee_delegue VARCHAR(255) DEFAULT NULL, referentiel_commune VARCHAR(255) DEFAULT NULL, referentiel_nom TEXT DEFAULT NULL, referentiel_api_champs TEXT DEFAULT NULL, PRIMARY KEY(referentiel_id));
CREATE TABLE bdterr.bdterr_champ_type (champtype_id INT NOT NULL, champtype_nom VARCHAR(255) DEFAULT NULL, champtype_inContenuTier BOOLEAN DEFAULT 'true', PRIMARY KEY(champtype_id));
CREATE TABLE bdterr.bdterr_rapports (rapport_id INT NOT NULL, rapport_nom VARCHAR(255) DEFAULT NULL, rapport_gabarit_html VARCHAR(255) DEFAULT NULL, rapport_gabarit_odt VARCHAR(255) DEFAULT NULL, PRIMARY KEY(rapport_id));
CREATE TABLE bdterr.bdterr_contenus_tiers (contenu_id INT NOT NULL, lot_id INT DEFAULT NULL, contenu_type INT DEFAULT NULL, contenu_nom VARCHAR(255) DEFAULT NULL, contenu_ordre INT DEFAULT NULL, contenu_fiche BOOLEAN DEFAULT NULL, contenu_resultats BOOLEAN DEFAULT NULL, contenu_url VARCHAR(255) NOT NULL, PRIMARY KEY(contenu_id));                                                                                                                                               
CREATE INDEX IDX_4819E6AFA8CBA5F7 ON bdterr.bdterr_contenus_tiers (lot_id);                                                                                                                                                                  
CREATE INDEX IDX_4819E6AF8AE06949 ON bdterr.bdterr_contenus_tiers (contenu_type);                                                                                                                                                            
CREATE TABLE bdterr.bdterr_lot (lot_id INT NOT NULL, lot_theme_id INT DEFAULT NULL, lot_referentiel_id INT DEFAULT NULL, lot_uuid VARCHAR(255) DEFAULT NULL, lot_table VARCHAR(255) DEFAULT NULL, lot_alias VARCHAR(255) DEFAULT NULL, lot_carte VARCHAR(255) DEFAULT NULL, lot_avertissement_message VARCHAR(255) DEFAULT NULL, lot_statistiques BOOLEAN DEFAULT NULL, lot_visualiseur BOOLEAN DEFAULT NULL, lot_date_maj TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, lot_statistic TEXT DEFAULT NULL, PRIMARY KEY(lot_id));                                                                                                                                                                                                         
CREATE INDEX IDX_4674D3DFFBC885B3 ON bdterr.bdterr_lot (lot_theme_id);                                                                                                                                                                       
CREATE INDEX IDX_4674D3DFA9EE0A03 ON bdterr.bdterr_lot (lot_referentiel_id);                                                                                                                                                                 
CREATE TABLE bdterr.bdterr_referentiel_carto (referentiel_id INT NOT NULL, referentiel_table TEXT NOT NULL, referentiel_champs TEXT DEFAULT NULL, PRIMARY KEY(referentiel_id));                               
CREATE TABLE bdterr.bdterr_referentiel_intersection (referentiel_id INT NOT NULL, referentiel_table VARCHAR(255) DEFAULT NULL, referentiel_insee VARCHAR(255) DEFAULT NULL, referentiel_insee_deleguee VARCHAR(255) DEFAULT NULL, referentiel_nom VARCHAR(255) DEFAULT NULL, PRIMARY KEY(referentiel_id));                                                                                                                                                                                
CREATE TABLE bdterr.bdterr_couche_champ (champ_id INT NOT NULL, champ_lot_id INT NOT NULL, champ_type INT DEFAULT NULL, champ_nom VARCHAR(150) NOT NULL, champ_alias VARCHAR(255) DEFAULT NULL, champ_ordre INT DEFAULT NULL, champ_format VARCHAR(255) DEFAULT NULL, champ_fiche BOOLEAN DEFAULT NULL, champ_resultats BOOLEAN DEFAULT NULL, champ_identifiant BOOLEAN DEFAULT NULL, champ_label BOOLEAN DEFAULT NULL, PRIMARY KEY(champ_id));                                           
CREATE INDEX IDX_52A0CA003DC30ACD ON bdterr.bdterr_couche_champ (champ_lot_id);                                                                                                                                                              
CREATE INDEX IDX_52A0CA00D43AA457 ON bdterr.bdterr_couche_champ (champ_type);                                                                                                                                                                
CREATE TABLE bdterr.bdterr_donnee (donnee_id INT NOT NULL, donnee_lot_id INT DEFAULT NULL, donnee_objet_id TEXT DEFAULT NULL, donnee_objet_nom TEXT DEFAULT NULL, donnee_geom TEXT DEFAULT NULL, donnee_geom_json TEXT DEFAULT NULL, donnee_attributs_fiche TEXT DEFAULT NULL, donnee_attributs_resultat TEXT DEFAULT NULL, donnee_ressources_resultat TEXT DEFAULT NULL, donnee_ressources_fiche TEXT DEFAULT NULL, donnee_insee TEXT DEFAULT NULL, donnee_insee_delegue TEXT DEFAULT NULL, PRIMARY KEY(donnee_id));                                                                                                                                                                                                                  
CREATE INDEX IDX_30055129141F7BF0 ON bdterr.bdterr_donnee (donnee_lot_id);                                                                                                                                                                   
CREATE TABLE bdterr.bdterr_themes (theme_id INT NOT NULL, theme_parent INT DEFAULT NULL, theme_nom VARCHAR(255) NOT NULL, theme_alias TEXT DEFAULT NULL, theme_ordre INT DEFAULT NULL, PRIMARY KEY(theme_id));                               
CREATE INDEX IDX_A06003ABEDB65433 ON bdterr.bdterr_themes (theme_parent);                                                                                                                                                                    
CREATE TABLE bdterr.bdterr_rapports_profils (id INT NOT NULL, bterr_rapport_id INT DEFAULT NULL, prodige_profil_id INT DEFAULT NULL, PRIMARY KEY(id));                                                                                       
CREATE INDEX IDX_51FAF077D1EAE64C ON bdterr.bdterr_rapports_profils (bterr_rapport_id);
CREATE TABLE catalogue.scheduled_command (id INT NOT NULL, name TEXT DEFAULT NULL, command TEXT DEFAULT NULL, arguments TEXT DEFAULT NULL, cron_expression TEXT DEFAULT NULL, last_execution TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_return_code INT DEFAULT 0, log_file TEXT DEFAULT NULL, priority INT DEFAULT 0, execute_immediately BOOLEAN DEFAULT 'false', disabled BOOLEAN DEFAULT 'false', locked BOOLEAN DEFAULT 'false', is_periodic BOOLEAN NOT NULL, year INT DEFAULT NULL, frequency TEXT DEFAULT NULL, PRIMARY KEY(id));
CREATE TABLE catalogue.execution_report (id INT NOT NULL, scheduled_command_id INT DEFAULT NULL, date_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, file VARCHAR(255) NOT NULL, status VARCHAR(255) default NULL, PRIMARY KEY(id));
CREATE INDEX IDX_8E1B7BBCAD93E023 ON catalogue.execution_report (scheduled_command_id);
ALTER TABLE bdterr.bdterr_contenus_tiers ADD CONSTRAINT FK_4819E6AFA8CBA5F7 FOREIGN KEY (lot_id) REFERENCES bdterr.bdterr_lot (lot_id) NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE bdterr.bdterr_contenus_tiers ADD CONSTRAINT FK_4819E6AF8AE06949 FOREIGN KEY (contenu_type) REFERENCES bdterr.bdterr_champ_type (champtype_id) NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE bdterr.bdterr_lot ADD CONSTRAINT FK_4674D3DFFBC885B3 FOREIGN KEY (lot_theme_id) REFERENCES bdterr.bdterr_themes (theme_id) NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE bdterr.bdterr_lot ADD CONSTRAINT FK_4674D3DFA9EE0A03 FOREIGN KEY (lot_referentiel_id) REFERENCES bdterr.bdterr_referentiel_intersection (referentiel_id) NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE bdterr.bdterr_couche_champ ADD CONSTRAINT FK_52A0CA003DC30ACD FOREIGN KEY (champ_lot_id) REFERENCES bdterr.bdterr_lot (lot_id) NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE bdterr.bdterr_couche_champ ADD CONSTRAINT FK_52A0CA00D43AA457 FOREIGN KEY (champ_type) REFERENCES bdterr.bdterr_champ_type (champtype_id) NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE bdterr.bdterr_donnee ADD CONSTRAINT FK_30055129141F7BF0 FOREIGN KEY (donnee_lot_id) REFERENCES bdterr.bdterr_lot (lot_id) NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE bdterr.bdterr_themes ADD CONSTRAINT FK_A06003ABEDB65433 FOREIGN KEY (theme_parent) REFERENCES bdterr.bdterr_themes (theme_id) NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE bdterr.bdterr_rapports_profils ADD CONSTRAINT FK_51FAF077D1EAE64C FOREIGN KEY (bterr_rapport_id) REFERENCES bdterr.bdterr_rapports (rapport_id) NOT DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE catalogue.execution_report ADD CONSTRAINT FK_8E1B7BBCAD93E023 FOREIGN KEY (scheduled_command_id) REFERENCES catalogue.scheduled_command (id) NOT DEFERRABLE INITIALLY IMMEDIATE;

/** ajout des types **/
INSERT INTO bdterr.bdterr_champ_type VALUES (2, 'text', true);
INSERT INTO bdterr.bdterr_champ_type VALUES (4, 'url', true);
INSERT INTO bdterr.bdterr_champ_type VALUES (5, 'Image', true);
INSERT INTO bdterr.bdterr_champ_type VALUES (6, 'vidéo', true);
INSERT INTO bdterr.bdterr_champ_type VALUES (3, 'date', false);

/** Référentiel recherche */
INSERT INTO bdterr_referentiel_recherche VALUES (1, '', '', '', '', '', '');

/** Thème racine */
INSERT INTO bdterr.bdterr_themes (theme_id, 	theme_nom, 	theme_alias, 	theme_ordre, 	theme_parent) VALUES (0, 'root', NULL, 0, NULL);

/**  Ajout layer name dans lot **/
ALTER TABLE bdterr.bdterr_lot ADD lot_layer_name TEXT DEFAULT NULL;


--Opendata module


--
-- Name: harves_opendata_node; Type: TABLE; Schema: catalogue; Owner: user_admin
--
CREATE TABLE catalogue.harves_opendata_node (
    pk_node_id integer NOT NULL,
    node_name text,
    node_logo text,
    api_url text,
    node_last_modification text,
    url_dcat text,
    node_log_file text,
    command_id integer DEFAULT 0 NOT NULL
);

--
-- Name: TABLE harves_opendata_node; Type: COMMENT; Schema: catalogue; Owner: user_admin
--
COMMENT ON TABLE catalogue.harves_opendata_node IS 'moissonnage DCAT';
--
-- Name: harves_opendata_node_pk_node_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_admin
--
CREATE SEQUENCE catalogue.harves_opendata_node_pk_node_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--
-- Name: harves_opendata_node_pk_node_id_seq; Type: SEQUENCE OWNED BY; Schema: catalogue; Owner: user_admin
--
ALTER SEQUENCE catalogue.harves_opendata_node_pk_node_id_seq OWNED BY catalogue.harves_opendata_node.pk_node_id;
--
-- Name: harves_opendata_node pk_node_id; Type: DEFAULT; Schema: catalogue; Owner: user_admin
--
ALTER TABLE ONLY catalogue.harves_opendata_node ALTER COLUMN pk_node_id SET DEFAULT nextval('catalogue.harves_opendata_node_pk_node_id_seq'::regclass);
--
-- Name: harves_opendata_node harves_opendata_node_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_admin
--
ALTER TABLE ONLY catalogue.harves_opendata_node
    ADD CONSTRAINT harves_opendata_node_pkey PRIMARY KEY (pk_node_id);


-- Name: harves_opendata_node_params; Type: TABLE; Schema: catalogue; Owner: user_admin
--
CREATE TABLE catalogue.harves_opendata_node_params (
    pk_params_id integer NOT NULL,
    fk_node_id integer NOT NULL,
    key text,
    value text
);
--
-- Name: TABLE harves_opendata_node_params; Type: COMMENT; Schema: catalogue; Owner: user_admin
--
COMMENT ON TABLE catalogue.harves_opendata_node_params IS 'paramètres nœuds de moissonnage DCAT';
--
-- Name: harves_opendata_node_params_FK_node_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_admin
--
CREATE SEQUENCE catalogue."harves_opendata_node_params_FK_node_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
--
-- Name: harves_opendata_node_params_FK_node_id_seq; Type: SEQUENCE OWNED BY; Schema: catalogue; Owner: user_admin
--
ALTER SEQUENCE catalogue."harves_opendata_node_params_FK_node_id_seq" OWNED BY catalogue.harves_opendata_node_params.fk_node_id;
--
-- Name: harves_opendata_node_params_pk_params_id_seq; Type: SEQUENCE; Schema: catalogue; Owner: user_admin
--
CREATE SEQUENCE catalogue.harves_opendata_node_params_pk_params_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
--
-- Name: harves_opendata_node_params_pk_params_id_seq; Type: SEQUENCE OWNED BY; Schema: catalogue; Owner: user_admin
--
ALTER SEQUENCE catalogue.harves_opendata_node_params_pk_params_id_seq OWNED BY catalogue.harves_opendata_node_params.pk_params_id;
--
-- Name: harves_opendata_node_params pk_params_id; Type: DEFAULT; Schema: catalogue; Owner: user_admin
--
ALTER TABLE ONLY catalogue.harves_opendata_node_params ALTER COLUMN pk_params_id SET DEFAULT nextval('catalogue.harves_opendata_node_params_pk_params_id_seq'::regclass);
--
-- Name: harves_opendata_node_params fk_node_id; Type: DEFAULT; Schema: catalogue; Owner: user_admin
--
ALTER TABLE ONLY catalogue.harves_opendata_node_params ALTER COLUMN fk_node_id SET DEFAULT nextval('catalogue."harves_opendata_node_params_FK_node_id_seq"'::regclass);
--
-- Name: harves_opendata_node_params harves_opendata_node_params_pkey; Type: CONSTRAINT; Schema: catalogue; Owner: user_admin
--
ALTER TABLE ONLY catalogue.harves_opendata_node_params
    ADD CONSTRAINT harves_opendata_node_params_pkey PRIMARY KEY (pk_params_id);
--
-- Name: harves_opendata_node_params harves_opendata_node_params_FK_node_id_fkey; Type: FK CONSTRAINT; Schema: catalogue; Owner: user_admin
--
ALTER TABLE ONLY catalogue.harves_opendata_node_params
    ADD CONSTRAINT "harves_opendata_node_params_FK_node_id_fkey" FOREIGN KEY (fk_node_id) REFERENCES catalogue.harves_opendata_node(pk_node_id);

--
-- PostgreSQL database dump complete
--
INSERT INTO  catalogue.prodige_settings (prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES ( 'PRO_HARVES_DCAT_ORGANIZATIONS', NULL, 'https://www.data.gouv.fr/api/1/organizations/', 'Définit l''url d''API fournissant la liste des organisations disponibles', 'textfield', NULL);
INSERT INTO  catalogue.prodige_settings (prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES ( 'PRO_IS_OPENDATA_ACTIF', NULL, 'off', 'Module opendata activé', 'checkboxfield', NULL);
INSERT INTO  catalogue.prodige_settings (prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES ( 'PRO_IS_RAWGRAPH_ACTIF', NULL, 'off', 'Module graphe activé', 'checkboxfield', NULL);


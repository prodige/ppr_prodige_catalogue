
--
-- Name: utilisateur_contexte; Type: TABLE; Schema: catalogue;
--

CREATE SEQUENCE catalogue.seq_utilisateur_contexte
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE catalogue.utilisateur_contexte (
    id bigint DEFAULT nextval('catalogue.seq_utilisateur_contexte'::regclass) NOT NULL,
    fk_utilisateur integer NOT NULL,
    context text
);

ALTER TABLE ONLY catalogue.utilisateur_contexte
    ADD CONSTRAINT utilisateur_contexte_pkey PRIMARY KEY (id);

CREATE INDEX idx_utilisateur_contexte_fk_utilisateur ON catalogue.utilisateur_contexte USING btree (fk_utilisateur);

ALTER TABLE ONLY catalogue.utilisateur_contexte
    ADD CONSTRAINT fk_utilisateur_contexte_fk_utilisateur FOREIGN KEY (fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE;

--
-- Name: utilisateur_zone_favorite; Type: TABLE; Schema: catalogue;
--

CREATE SEQUENCE catalogue.seq_utilisateur_zone_favorite
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE catalogue.utilisateur_zone_favorite (
    id bigint DEFAULT nextval('catalogue.seq_utilisateur_zone_favorite'::regclass) NOT NULL,
    fk_utilisateur integer NOT NULL,
    zone_favorite text
);

ALTER TABLE ONLY catalogue.utilisateur_zone_favorite
    ADD CONSTRAINT utilisateur_zone_favorite_pkey PRIMARY KEY (id);

CREATE INDEX idx_utilisateur_zone_favorite_fk_utilisateur ON catalogue.utilisateur_zone_favorite USING btree (fk_utilisateur);

ALTER TABLE ONLY catalogue.utilisateur_zone_favorite
    ADD CONSTRAINT fk_utilisateur_zone_favorite_fk_utilisateur FOREIGN KEY (fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE;

---
--- Ajout des structures
--- 

CREATE SEQUENCE catalogue.seq_structure
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE TABLE catalogue.structure (
    pk_structure integer DEFAULT nextval('catalogue.seq_structure'::regclass) NOT NULL,
    ts timestamp without time zone DEFAULT now(),
    structure_nom character varying(255) ,
    structure_sigle character varying(40) ,
    structure_siren character varying(9)
);

ALTER TABLE ONLY catalogue.structure
    ADD CONSTRAINT pk_pk_structure PRIMARY KEY (pk_structure);


--
-- Name: utilisateur_structure; Type: TABLE; Schema: catalogue;
--

CREATE SEQUENCE catalogue.seq_utilisateur_structure
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE catalogue.utilisateur_structure (
    id integer DEFAULT nextval('catalogue.seq_utilisateur_structure'::regclass) NOT NULL,
    fk_utilisateur integer NOT NULL,
    fk_structure integer NOT NULL
);

ALTER TABLE ONLY catalogue.utilisateur_structure
    ADD CONSTRAINT utilisateur_structure_pkey PRIMARY KEY (id);

CREATE INDEX idx_utilisateur_structure_fk_utilisateur ON catalogue.utilisateur_structure USING btree (fk_utilisateur);

ALTER TABLE ONLY catalogue.utilisateur_structure
    ADD CONSTRAINT fk_utilisateur_structure_fk_utilisateur FOREIGN KEY (fk_utilisateur) REFERENCES catalogue.utilisateur(pk_utilisateur) MATCH FULL ON DELETE CASCADE;

CREATE INDEX idx_utilisateur_structure_fk_structure ON catalogue.utilisateur_structure USING btree (fk_structure);

ALTER TABLE ONLY catalogue.utilisateur_structure
    ADD CONSTRAINT fk_utilisateur_structure_fk_structure FOREIGN KEY (fk_structure) REFERENCES catalogue.structure(pk_structure) MATCH FULL ON DELETE CASCADE;

-- restriction attributaire des droits
alter table catalogue.couche_donnees add column couchd_restriction_attributaire integer default 0;
alter table catalogue.couche_donnees add column couchd_restriction_attributaire_propriete varchar(255);

CREATE SEQUENCE catalogue.seq_grp_restriction_attributaire
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


CREATE TABLE catalogue.grp_restriction_attributaire (
    pk_grp_restriction_attributaire integer DEFAULT nextval('catalogue.seq_grp_restriction_attributaire'::regclass) NOT NULL,
    grprat_fk_couche_donnees integer,
    grprat_fk_groupe_profil integer,
    grprat_champ varchar(255)
      
);

ALTER TABLE ONLY catalogue.grp_restriction_attributaire
    ADD CONSTRAINT pk_grp_restriction_attributaire PRIMARY KEY (pk_grp_restriction_attributaire);


ALTER TABLE ONLY catalogue.grp_restriction_attributaire
    ADD CONSTRAINT grprat_fk_groupe_profil_fkey FOREIGN KEY (grprat_fk_groupe_profil) REFERENCES catalogue.groupe_profil(pk_groupe_profil) MATCH FULL ON DELETE CASCADE;


ALTER TABLE ONLY catalogue.grp_restriction_attributaire
    ADD CONSTRAINT grprat_fk_couche_donnees_fkey FOREIGN KEY (grprat_fk_couche_donnees) REFERENCES catalogue.couche_donnees(pk_couche_donnees) MATCH FULL ON DELETE CASCADE;


insert into catalogue.prodige_settings (prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type) 
values ('PRO_MODULE_TABLE_EDITION', 'Module édition de table activé', 'off', 'Module édition de table activé', 'checkboxfield');

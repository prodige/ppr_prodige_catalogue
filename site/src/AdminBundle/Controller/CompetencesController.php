<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

//use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

class CompetencesController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/competences/competences_list", name="catalogue_administration_competences_list", options={"expose"=true})
     */
    public function catalogueAdministration_CompetencesListAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Competences/CompetencesList.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/competences/competences_form", name="catalogue_administration_competences_form", options={"expose"=true})
     */
    public function catalogueAdministration_CompetencesAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        $submitUrl = $this->generateUrl('catalogue_administration_submit_form');
        include_once(__DIR__.'/../Resources/templates/Administration/Competences/CompetencesAccueil.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/competences/profiles_form", name="catalogue_administration_competences_profiles_form", options={"expose"=true})
     */
    public function catalogueAdministration_CompetencesProfilesAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Competences/CompetencesProfiles.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/competences/layers_form", name="catalogue_administration_competences_layers_form", options={"expose"=true})
     */
    public function catalogueAdministration_CompetencesLayersAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Competences/CompetencesLayers.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/competences/maps_form", name="catalogue_administration_competences_maps_form", options={"expose"=true})
     */
    public function catalogueAdministration_CompetencesMapsAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Competences/CompetencesMaps.php');
        exit();
    }
}

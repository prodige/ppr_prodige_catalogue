<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route; 

use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;

class SubmitFormController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/submit_form", name="catalogue_administration_submit_form", options={"expose"=true})
     */
    public function submitFormAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        global $conn_prodige;
        $conn_prodige    = $this->getProdigeConnection('public');
        $ldapConf['hostLdap'] = $this->container->getParameter("ldap_host");
        $ldapConf['portLdap'] = $this->container->getParameter("ldap_host");
        $ldapConf['dnAdminLdap'] = $this->container->getParameter("ldap_search_dn");
        $ldapConf['passwordLdap'] = $this->container->getParameter("ldap_password");
        $ldapConf['baseDnLdap'] = $this->container->getParameter("ldap_base_dn");
        $uploadFolder = $this->container->getParameter("PRODIGE_PATH_CATALOGUE").'/public/upload/';
        
        if( $request->query->get('from',null)==='mafiche' ) {
            // ce formulaire est différent, faire mapping des champs vers ORDINAL
            $pk = $_POST['IDENTIFIANT'];
            $_POST['ORDINAL_'.UtilisateurVO::$USR_EMAIL]        = $_POST['EMAIL'];
            $_POST['ORDINAL_'.UtilisateurVO::$USR_TELEPHONE2]   = $_POST['TELEPHONE2'];
            
            $ords = array(UtilisateurVO::$USR_EMAIL, UtilisateurVO::$USR_TELEPHONE2);
            
            if( $_POST['CHANGEMOTDEPASSE']=='1' ) {
                $_POST['ORDINAL_'.UtilisateurVO::$USR_PASSWORD] = $_POST['MOTDEPASSE1'];
                $_POST['ORDINAL_'.UtilisateurVO::$USR_PWD_EXPIRE] = \ProdigeCatalogue\WebBundle\Controller\ForgottenPasswordController::getNewPdwExpireDate();
                $ords[] = UtilisateurVO::$USR_PASSWORD;
                $ords[] = UtilisateurVO::$USR_PWD_EXPIRE;
            }
            
            $_POST["Id"]                 = $pk;
            $_POST["Ordinals"]           = join(';', $ords);
            $_POST["Action"]             = Ressources::$EDITFORM_VALIDATE; // update ?
            $_POST["MainAction"]         = '0';
            $_POST["VOClassName"]        = 'UtilisateurVO';
            $_POST["RestrictionOrdinal"] = '0';
            $_POST["RefreshPage"]        = '';
            $_POST["ReadOnly"]           = join(';', array_fill(0, count($ords), ''));
        }
        
        // gestion du mot de passe utilisateur en POST
        $pwdOrdinal = 'ORDINAL_'.UtilisateurVO::$USR_PASSWORD;
        if( isset($_POST[$pwdOrdinal]) ) {
            $encoder = $this->container->get('prodige.passwordencoder');
            $encoder instanceof \Prodige\ProdigeBundle\Services\PasswordEncoder;
            $encoded  = $encoder->encode( $_POST[$pwdOrdinal]);
            
            $_POST[$pwdOrdinal] = $encoded;
        }
        
        $controller = $this;
        include_once(__DIR__.'/../Common/Components/EditForm/WriteBOV.php');
        exit();
    }

}

<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Prodige\ProdigeBundle\Controller\BaseController;

class AccueilController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/accueil", name="catalogue_administration_accueil", options={"expose"=true})
     */
    public function catalogueAdministration_Accueil(Request $request) {
        include_once(__DIR__.'/../Resources/templates/Administration/Accueil/Accueil.php');
        return new Response("");
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/administration_utilisateur", name="catalogue_administration_utilisateur", options={"expose"=true})
     */
    public function catalogueAdministration_AdministrationUtilisateur(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        $admin_css = $this->generateUrl('catalogue_get_css_file', array('css_file'=>'administration_utilisateur'));
        
        $submit_form = $this->generateUrl('catalogue_administration_submit_form', array('from'=>'mafiche'));
        
        include_once(__DIR__.'/../Resources/templates/Administration/Accueil/administration_utilisateur.php');
        return new Response("");
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/administration_compte_utilisateur", name="catalogue_administration_compte_utilisateur", options={"expose"=true})
     */
    public function catalogueAdministration_AdministrationCompteUtilisateur(Request $request) {
        
        // cas mafiche - autoriser quiconque à accéder à ce service 
        $request->query->set('userDetails', true);
        
        $conn    = $this->getCatalogueConnection('catalogue');
        $admin_css = $this->generateUrl('catalogue_get_css_file', array('css_file'=>'administration_utilisateur'));
        include_once(__DIR__.'/../Resources/templates/Administration/Accueil/administration_compte_utilisateur.php');
        return new Response("");
    }

}

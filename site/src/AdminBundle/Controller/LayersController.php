<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route; 
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

//use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

class LayersController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/layers/list", name="catalogue_administration_layers_list", options={"expose"=true})
     */
    public function catalogueAdministration_LayersListAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Layers/LayersList.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/layers/layers_form", name="catalogue_administration_layers_form", options={"expose"=true})
     */
    public function catalogueAdministration_LayersAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        $submitUrl = $this->generateUrl('catalogue_administration_submit_form');
        $getAlertsModelesUrl = $this->generateUrl('catalogue_administration_services_getAlertsModeles');
        $zonageGetFieldsUrl = $this->generateUrl('catalogue_administration_services_ZonageGetFields');
        include_once(__DIR__.'/../Resources/templates/Administration/Layers/LayersAccueil.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/layers/layers_competences", name="catalogue_administration_layers_competences_form", options={"expose"=true})
     */
    public function catalogueAdministration_CompetencesAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Layers/LayersCompetences.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/layers/layers_alerts", name="catalogue_administration_layers_alerts_form", options={"expose"=true})
     */
    public function catalogueAdministration_AlertsAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Layers/LayersAlerts.php');
        exit();
    }


    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/layers/importer_form", name="catalogue_administration_layers_importer_form", options={"expose"=true})
     */
    public function catalogueAdministration_LayersImporterAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        $importAlertEdition = $this->generateUrl('catalogue_administration_layers_alertimport', $request->query->all());
        include_once (__DIR__ . '/../Resources/templates/Administration/Layers/Import.php');
        exit();
    }
    
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/layers/alertimport", name="catalogue_administration_layers_alertimport", options={"expose"=true})
     */
    public function catalogueAdministration_AlertImporterAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        include_once (__DIR__ . '/../Resources/templates/Administration/Users/AlertEditionImport.php');
        exit();
    }
}

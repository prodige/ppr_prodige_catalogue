<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route; 
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

//use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

class MapsController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/maps/maps_list", name="catalogue_administration_maps_list", options={"expose"=true})
     */
    public function catalogueAdministration_MapsListAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Maps/MapsList.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/maps/maps_form", name="catalogue_administration_maps_form", options={"expose"=true})
     */
    public function catalogueAdministration_MapsAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        $submitUrl = $this->generateUrl('catalogue_administration_submit_form');
        include_once(__DIR__.'/../Resources/templates/Administration/Maps/MapsAccueil.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/maps/maps_competences", name="catalogue_administration_maps_competences_form", options={"expose"=true})
     */
    public function catalogueAdministration_MapsCompetencesAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Maps/MapsCompetences.php');
        exit();
    }

}

<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route; 
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

//use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

class DomainesController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/domains/domains_list", name="catalogue_administration_domains_list", options={"expose"=true})
     */
    public function catalogueAdministration_DomainsListAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Domains/DomainsList.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/domains/domains_form", name="catalogue_administration_domains_form", options={"expose"=true})
     */
    public function catalogueAdministration_DomainsFormAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        $submitUrl = $this->generateUrl('catalogue_administration_submit_form');
        $ajaxRequestUrl = $this->generateUrl('catalogue_administration_services_updateMdataDomSdom');
        include_once(__DIR__.'/../Resources/templates/Administration/Domains/DomainsAccueil.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/domains/sub_domains_form", name="catalogue_administration_domains_subdomains_form", options={"expose"=true})
     */
    public function catalogueAdministration_SubDomainsAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Domains/DomainsSubDomains.php');
        exit();
    }
}

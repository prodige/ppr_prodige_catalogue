<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route; 
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

//use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

class GroupingController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/grouping/grouping_list", name="catalogue_administration_grouping_list", options={"expose"=true})
     */
    public function catalogueAdministration_GroupingListAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Grouping/GroupingList.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/grouping/grouping_form", name="catalogue_administration_grouping_profile_form", options={"expose"=true})
     */
    public function catalogueAdministration_GroupingProfileAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Grouping/GroupingGroupesProfil.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/grouping/grouping_subdomains_form", name="catalogue_administration_grouping_subdomains_form", options={"expose"=true})
     */
    public function catalogueAdministration_GroupingSubdomainsAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Grouping/GroupingSubDomains.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/grouping/grouping_save", name="catalogue_administration_grouping_save", options={"expose"=true})
     */
    public function catalogueAdministration_GroupingSaveAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Grouping/Grouping_save.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/grouping/grouping_profile_sql", name="catalogue_administration_grouping_profile_sql", options={"expose"=true})
     */
    public function catalogueAdministration_GroupingProfileSQLAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Grouping/GroupingGroupesProfil_sql.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/grouping/grouping_subdomains_sql", name="catalogue_administration_grouping_subdomains_sql", options={"expose"=true})
     */
    public function catalogueAdministration_GroupingSubdomainsSQLAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Grouping/GroupingSubDomains_sql.php');
        exit();
    }
}

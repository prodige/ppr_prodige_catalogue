<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ProdigeCatalogue\BdcomBundle\Controller\CatalogueController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Common\SecurityExceptions;

/**
 * @Route("/catalogue/administration")
 * 
 * @author alkante
 */
class DefaultController extends BaseController {

    /**
     * Fonction de lecture des entêtes
     * @param $url
     * @return $headers : entêtes html
     */
    protected function build_response_headers($url_info)
    {
       $tabHeaders = array();
       foreach($url_info as $values){
           $splitValue = preg_split("#: #", $values);
           $data = "";
           if(count($splitValue)==2){
               $key = $splitValue[0];
               $data = $splitValue[1];
               if(isset($tabHeaders[$key])){
                   $tabHeaders[$key] = $tabHeaders[$key]."; ".$data;
               }else{
                   $tabHeaders[$key] =$data;
               }
           }
       }
       return $tabHeaders;
    }
    
    /**
     * Charge l'admin pour l'affichage de la fiche utilisateur uniquement
     * @Route("/mafiche", name="catalogue_web_userdetails", options={"expose"=true})
     */
    public function maFicheAction(Request $request) {
        $request->query->set('userDetails', true);
        return $this->indexAction($request, true);
    }
    
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/", name="catalogue_web_index", options={"expose"=true})
     */
    public function indexAction(Request $request, $userDetails=false) {
        /* Récupérer des paramètres du app/parameters.yml */
        $user = User::GetUser();
        // si on est pas sur la fiche utilisateur et qu'on est pas admin prodige -> accès refusé
        if( !$userDetails && !$user->IsProdigeAdmin() ) SecurityExceptions::throwAccessDeniedException();
        
        $title = Ressources::$PAGE_TITLE_MAIN;
        $getVars = Ressources::getVars($this, $userDetails);
        $iframe_name_main = Ressources::$IFRAME_NAME_MAIN;
        return $this->render('AdminBundle/Default/index.html.twig', array('title' => $title, 'getVars' => $getVars, 'iframe_name_main' => $iframe_name_main));
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/page", name="catalogue_web_page", options={"expose"=true})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderPageAction(Request $request) {

        /**
         * Page d'adminsitration des droits, redirige vers AdminMain.php
         * @author Alkante
         */

        $iPart = $request->get("iPart", -1);
        $Action = $request->get("Action", -1);

        switch ($iPart){
            case "0": // list
                return $this->GetModulePageList($Action);
                break;
            case "1": // contents
                $id = $request->get("Id", -1);
                if($Action == Ressources::$ACTION_OPEN_GROUPING ){
                    $Action = $id;
                    $id = -1;
                }
                $strjs = $this->GetModulePageDetail($Action, $id);
                return new Response($strjs);
                break;
        }
    }

    /**
     * #brief Renvoie la liste de gauche correspondant à l'onglet sur lequel on a cliqué
     * #param $action
     * #return url de la page onglet
     */
    public function GetModulePageList($action) {
        switch($action) {
            case Ressources::$ACTION_OPEN_INITIAL:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\AccueilController::catalogueAdministration_Accueil');
                break;
            case Ressources::$ACTION_OPEN_PROFILES:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\ProfilsController::profilesList');
                break;
            case Ressources::$ACTION_OPEN_STRUCTURE:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\StructureController::catalogueAdministration_structureListAction');
                break;
            case Ressources::$ACTION_OPEN_USERS:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\UsersController::catalogueAdministration_UsersListAction');
                break;
            case Ressources::$ACTION_OPEN_RUBRICS:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\RubricsController::catalogueAdministration_RubricsListAction');
                break;
            case Ressources::$ACTION_OPEN_DOMAINS:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\DomainesController::catalogueAdministration_DomainsListAction');
                break;
            case Ressources::$ACTION_OPEN_SUBDOMAINS:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\SubDomainesController::catalogueAdministration_SubDomainesListAction');
                break;
            case Ressources::$ACTION_OPEN_LAYERS:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\LayersController::catalogueAdministration_LayersListAction');
                break;
            case Ressources::$ACTION_OPEN_MAPS:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\MapsController::catalogueAdministration_MapsListAction');
                break;
            case Ressources::$ACTION_OPEN_COMPETENCES:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\CompetencesController::catalogueAdministration_CompetencesListAction');
                break;
            case Ressources::$ACTION_OPEN_ZONAGES:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\ZonagesController::catalogueAdministration_ZonagesListAction');
                break; 
            case Ressources::$ACTION_OPEN_PERIMETRES:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\PerimetresController::catalogueAdministration_PerimetresListAction');
                break;
            case Ressources::$ACTION_OPEN_GROUPING:
                return $this->forward('ProdigeCatalogue\AdminBundle\Controller\GroupingController::catalogueAdministration_GroupingListAction');
                break;
            case Ressources::$ACTION_OPEN_RUBRICS_AIDE:
              return $this->forward('ProdigeCatalogue\AdminBundle\Controller\RubricsAideController::catalogueAdministration_RubricsAideListAction');
              break;
        }
        return $this->forward('ProdigeCatalogue\AdminBundle\Controller\EmptyController::catalogueAdministration_EmptyAction');
    }

    /**
     * #brief Renvoie la page détail correspondant à l'onglet sur lequel on a cliqué
     * #param $action
     * #return url de la page onglet
     */
    public function GetModulePageDetail($action, $idSelected) {
        
        if( $action == Ressources::$ACTION_OPEN_EMPTY ) {
            return $this->forward('ProdigeCatalogue\AdminBundle\Controller\EmptyController::catalogueAdministration_EmptyAction');
        }
        
        //global $AdminPath;
        $strjs = "";
        $tabSheet = Ressources::getTabModuleSheet();
        $idElt = "tab_".$action;
        $firstSheet = "-1";
        foreach($tabSheet[$action] as $idSheet => $titleSheet) {
            $firstSheet = $idSheet;
            break;
        }
        $strjs .= '
        tabGridList = [];
        adminTabPanelMain = new Ext.TabPanel(
          {
            border :false,
            id :"module_details",
            activeTab :"'.$idElt.'_'.$firstSheet.'",
            renderTo : Ext.getCmp("contAdmin").body,
            autoScroll : true,
              listener : {
                bodyresize : function(panel, width, height){
                  alert(panel.xtype + " " + height);
                }
              },
            autoHeight : true,
            autoWidth : true,
            items : [';
        $glue = ""; 
        foreach ($tabSheet[$action] as $idSheet=>$descSheet) {
          $strjs .= $glue.' {
            title : "'.$descSheet["title"].'",
            id : "'.$idElt.'_'.$idSheet.'" ,
            autoScroll : true,
            autoHeight : true,
            autoWidth : true,
            autoLoad : {
              //url : "droits.php?url=<?php echo $descSheet["url"] ?>&iPart=1&Action=<?php echo $action ?>&Id=<?php echo $idSelected ?>",
              url: Routing.generate(\''.$descSheet["url"].'\', {
                iPart: 1,
                Action: \''.$action.'\',
                Id: '.$idSelected.'
              }),
              method : "GET",
              scripts: true,
              text : "Chargement en cours..."
            },
            listeners : {
              show : function(_this){
                if ( typeof _this.gridList != "undefined" ){
                  _this.gridList.fireEvent("afterrender", _this.gridList);
                }
              }
            }
          }';
          $glue = ", "; 
        }
        $strjs .= '
        ]});
        ';
        return $strjs;
      }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/css/get_css_file/{css_file}", name="catalogue_get_css_file", options={"expose"=true} )
     * #Method({"GET", "POST"})
     * */
    public function getCssFileAction(Request $request, $css_file) {
        $response = new Response();
        $response->headers->add(array(
            'Content-type' => 'text/css'
        ));
        return $this->render('AdminBundle/css/'.$css_file.'.css.twig', array(), $response);
        
    }
    
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/upload/temp/{post_name}", name="catalogue_web_uploadtemp", options={"expose"=true})
     */
    public function uploadTempAction(Request $request, $post_name) 
    {
        $uploadFolder = $this->container->getParameter("PRODIGE_PATH_CATALOGUE").'/public/upload/';
	if (isset($_FILES[$post_name]['name']) &&  $_FILES[$post_name]['name'] != "") {
            $file_extension = strtolower(pathinfo($_FILES[$post_name]['name'], PATHINFO_EXTENSION)); 
            if($file_extension === "csv" || $file_extension === "ldif" ) {
                $value = "./ressources/".$_FILES[$post_name]['name'];
                $uploadfile = $uploadFolder.'ressources/'.$_FILES[$post_name]['name'];
                move_uploaded_file($_FILES[$post_name]['tmp_name'], $uploadfile);
                return new Response(
                    "<html>
                        <head>
                            <script type='text/javascript'>
                                function onWinLoad(file){
                                   if ( parent.callbackUpload ) {
                                       parent.callbackUpload(file);
                                       parent.callbackUpload = null;
                                   }
                                }
                            </script>
                        </head>
                    <body onload='onWinLoad(\"".$uploadfile."\")'></body>
                    </html>"
                );                
            } else {
                return new Response("not a valid file");
            }
        }
        return new Response("");
    }
    
    
}

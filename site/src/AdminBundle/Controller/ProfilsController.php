<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route; 

//use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

use Prodige\ProdigeBundle\DAOProxy\DAO;


class ProfilsController extends BaseController {

    
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/profiles/set_profil", name="catalogue_administration_profiles_setprofil", options={"expose"=true})
     */
    public function setProfilAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        
        $mode = $request->get("mode", 0);
        
        if ($mode==0 ) {
            $result['success'] = false;
            $result['msg']     = htmlentities("Le paramètre 'mode' est manquant.", ENT_QUOTES, "UTF-8");
            Return new JsonResponse($result);
        }
        // get profil parameter
        $pk_groupe_profil = $request->get("profil_id", "");
        $field = $request->get("field", array());
        $grp_id = "";
        $grp_description = "";
        if(! empty($field)){
            $grp_id           = $field["PROFIL_INTITULE"][1];
            $grp_description  =  $field["PROFIL_DESC"][1];
        }
        // verify parameter
        $bParamOk = true;
        if ( !ctype_digit($pk_groupe_profil) ) $bParamOk = false;
        if ( !$bParamOk ) {
            $result['success'] = false;
            $result['msg']     = htmlentities("Paramètres non valides.", ENT_QUOTES, "UTF-8");
            Return new JsonResponse($result);
        }
        
        $grp_id = (mb_substr($grp_id, 0, 30));
        $grp_description = (mb_substr($grp_description, 0, 1024));
        $dao = new DAO($conn, 'catalogue');
        
        switch ( $mode ) {
          case 1 :  // création
              if($grp_id!=""){
                  $result['msg'] = htmlentities("Le profil a été créé.", ENT_QUOTES, "UTF-8");
                  $query = "INSERT INTO groupe_profil (pk_groupe_profil, grp_id, grp_nom, grp_description)" .
                      " VALUES (:pk_groupe_profil, :grp_id, :grp_id, :grp_description);";
                  
                  if($dao->Execute($query, array("pk_groupe_profil" => $pk_groupe_profil,
                                              "grp_id" => $grp_id,
                                              "grp_description" =>$grp_description))){ 
                      
                      $result['success'] = true;
                      return new JsonResponse($result);
                  } else {
                      $result['success'] = false;
                      $result['msg']     = htmlentities("Impossible d'exécuter la requête : $query.", ENT_QUOTES, "UTF-8");
                      return new JsonResponse($result);
                  }
              }
              break;
          case 2 :  // modification
              if($grp_id!=""){
                  $result['msg'] = htmlentities("Le profil a été mis à jour.", ENT_QUOTES, "UTF-8");
                  $query = "UPDATE groupe_profil SET grp_id=:grp_id, grp_nom=:grp_id, grp_description=:grp_description WHERE pk_groupe_profil=:pk_groupe_profil";
                  if($dao->Execute($query, array("pk_groupe_profil" => $pk_groupe_profil,
                      "grp_id" => $grp_id,
                      "grp_description" =>$grp_description))){
                  
                      $result['success'] = true;
                      return new JsonResponse($result);
                  } else {
                      $result['success'] = false;
                      $result['msg']     = htmlentities("Impossible d'exécuter la requête : $query.", ENT_QUOTES, "UTF-8");
                      return new JsonResponse($result);
                  }
              }
              break;
          case 3 :  // suppression
              
              $result['msg'] = htmlentities("Le profil a été supprimé.", ENT_QUOTES, "UTF-8");
              $query = "DELETE FROM groupe_profil WHERE pk_groupe_profil=:pk_groupe_profil";
              if($dao->Execute($query, array("pk_groupe_profil" => $pk_groupe_profil))){
                  $result['success'] = true;
                  return new JsonResponse($result);
              } else {
                  $result['success'] = false;
                  $result['msg']     = htmlentities("Impossible d'exécuter la requête : $query.", ENT_QUOTES, "UTF-8");
                  return new JsonResponse($result);
              }
              break;
          default :
              $result['success'] = false;
              $result['msg']     = htmlentities("Le paramètre 'mode' est incorrect.", ENT_QUOTES, "UTF-8");
              return new JsonResponse($result);
              break;
        }
        $result['success'] = false;
        return new JsonResponse($result);
        
    }
    
    
    
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/profiles/profiles_list", name="catalogue_administration_profiles_list", options={"expose"=true})
     */
    public function profilesListAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Profiles/ProfilesList.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/profiles/accueil_form", name="catalogue_administration_profiles_form", options={"expose"=true})
     */
    public function profilesAccueilAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        $submitUrl = $this->generateUrl('catalogue_administration_submit_form');
        include_once(__DIR__.'/../Resources/templates/Administration/Profiles/ProfilesAccueil.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/profiles/members_form", name="catalogue_administration_profiles_membres_form", options={"expose"=true})
     */
    public function profilesMembersAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Profiles/ProfilesMembers.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/profiles/traitements_form", name="catalogue_administration_profiles_traitements_form", options={"expose"=true})
     */
    public function profilesTraitementsAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Profiles/ProfilesTraitements.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/profiles/administre_form", name="catalogue_administration_profiles_administre_form", options={"expose"=true})
     */
    public function profilesAdministreAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Profiles/ProfilesAdmin.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/profiles/domaines_form", name="catalogue_administration_profiles_domaines_form", options={"expose"=true})
     */
    public function profilesDomainesAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Profiles/ProfilesDomains.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/profiles/sousdomaines_form", name="catalogue_administration_profiles_sousdomaines_form", options={"expose"=true})
     */
    public function profilesSousDomainesAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Profiles/ProfilesSSDomains.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/profiles/objects_form", name="catalogue_administration_profiles_objects_form", options={"expose"=true})
     */
    public function profilesObjectsAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Profiles/ProfilesObjects.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/profiles/competences_form", name="catalogue_administration_profiles_competences_form", options={"expose"=true})
     */
    public function profilesCompetencesAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Profiles/ProfilesCompetences.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/profiles/edit_form", name="catalogue_administration_profiles_edit_form", options={"expose"=true})
     */
    public function profilesEditAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Profiles/ProfilesEdite.php');
        return new Response("");
    }

}

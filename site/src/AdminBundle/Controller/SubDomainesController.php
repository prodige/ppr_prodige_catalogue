<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route; 

//use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

class SubDomainesController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/subdomaines/sub_domaines_list", name="catalogue_administration_subdomaines_list", options={"expose"=true})
     */
    public function catalogueAdministration_SubDomainesListAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/SubDomains/SubDomainsList.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/subdomains/subdomains_form", name="catalogue_administration_subdomains_form", options={"expose"=true})
     */
    public function catalogueAdministration_SubDomainsFormAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        $ajaxRequestUrl = $this->generateUrl('catalogue_administration_services_updateMdataDomSdom');
        $submitUrl = $this->generateUrl('catalogue_administration_submit_form');
        include_once(__DIR__.'/../Resources/templates/Administration/SubDomains/SubDomainsAccueil.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/subdomains/subdomainsdomains_form", name="catalogue_administration_subdomains_subdomainsdomains_form", options={"expose"=true})
     */
    public function catalogueAdministration_SubDomainsDomainsFormAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/SubDomains/SubDomainsDomains.php');
        exit();
    }

}

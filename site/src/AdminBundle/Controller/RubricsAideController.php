<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route; 

//use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

class RubricsAideController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/rubrics_aide/rubrics_aide_list", name="catalogue_administration_rubrics_aide_list", options={"expose"=true})
     */
    public function catalogueAdministration_RubricsAideListAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/RubricsAide/RubricsAideList.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/rubrics_aide/rubrics_form", name="catalogue_administration_rubrics_aide_form", options={"expose"=true})
     */
    public function catalogueAdministration_RubricsAideFormAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        $submitUrl = $this->generateUrl('catalogue_administration_submit_form');
        include_once(__DIR__.'/../Resources/templates/Administration/RubricsAide/RubricsAideAccueil.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/rubrics_aide/profiles_form", name="catalogue_administration_rubrics_aide_profiles_form", options={"expose"=true})
     */
    public function catalogueAdministration_RubricsAideProfilesFormAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/RubricsAide/RubricsAideProfiles.php');
        exit();
    }
    
}

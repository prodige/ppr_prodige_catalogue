<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route; 
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

//use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

class ZonagesController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/zonages/zonages_list", name="catalogue_administration_zonages_list", options={"expose"=true})
     */
    public function catalogueAdministration_ZonagesListAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Zonages/ZonagesList.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/zonages/zonages_form", name="catalogue_administration_zonages_form", options={"expose"=true})
     */
    public function catalogueAdministration_ZonagesAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        $submitUrl = $this->generateUrl('catalogue_administration_submit_form');
        $zonageGetFieldsUrl = $this->generateUrl('catalogue_administration_services_ZonageGetFields');
        $zonageAddTableUrl = $this->generateUrl('catalogue_administration_services_ZonageAddTable');
        $ZonageDelFieldsUrl = $this->generateUrl('catalogue_administration_services_ZonageDelFields');
        $ZonageDelTableUrl = $this->generateUrl('catalogue_administration_services_ZonageDelTable');
        $ZonageAddFieldsUrl = $this->generateUrl('catalogue_administration_services_ZonageAddFields');
        include_once(__DIR__.'/../Resources/templates/Administration/Zonages/ZonagesAccueil.php');
        exit();
    }

}

<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
//use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
//
//use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

class AlertSaveController extends BaseController {

    public static function AlertSaveDone($strJs="", $bAlert=true, $strAlert="Enregistrement effectué", $bError=false, $bAutoClose=true) {
        $strhtml = '';

        if($bError && $strAlert=="Enregistrement effectué")
            $strAlert = "Erreur lors de l'enregistrement";

        $strhtml .= '
            <html>
                <head>
                    <script type="text/javascript">
                        function onLoadExec() {'.
                          $strJs.';'.
                          ($bAlert
                           ?
                           'parent.msgBox = parent.Ext.Msg.show({' .
                                             ' buttons: parent.Ext.MessageBox.OK,' .
                                             ' width: 400,' .
                                             ' title : "'.($bError ? "Erreur" : "Statut").'",' .
                                             ' msg : "'.$strAlert.'", ' .
                                             ' icon : '.($bError ? "parent.Ext.MessageBox.ERROR" : "parent.Ext.MessageBox.INFO").
                                             '});' .
                          ($bAutoClose ? ' setTimeout("if (parent.msgBox.isVisible()) parent.msgBox.hide()", 2000);' : '')
                          :
                          '')
                .'}
                </script>
                </head>
                <body onload=\'onLoadExec();\'>
                </body>
            </html>
          ';

        echo $strhtml;
    }

    public static function AlertSaveProfil($strJs="", $bError=false, $strAlert="", $gridReload, $gridSelectedId) {

        $strhtml = '';
        //global $gridReload, $gridSelectedId;
        if($strAlert == '') {
            $strAlert = 'Regroupement effectué.';
        }

         $strhtml .= '
             <html>
                <head>
                    <!-- IE specific : forcing IE9 Document model because ext 3.0 incompatibility with IE10 -->
                    <META HTTP-EQUIV="X-UA-Compatible" CONTENT="IE=9"/>
                    <script  src="/Scripts/ext3.0/adapter/ext/ext-base.js"> </script>
                    <script  src="/Scripts/ext3.0/ext-all.js"> </script>
                    <script type="text/javascript">
                        function GotoData() {
                            parent.reloadGrid('.$gridReload.', '.$gridSelectedId.'); 
                         }
                         function callbackFn(buttonId) {
                            if(buttonId == \'cancel\') 
                                GotoData();
                         }
                         function onLoadExec() {
                            Ext.MessageBox.buttonText.yes = \'OK\';
                            Ext.MessageBox.buttonText.no = \'Réindexer Géosource\';
                            Ext.MessageBox.buttonText.cancel = \'Accéder à la nouvelle fiche\';
                            var msgBox = parent.Ext.Msg.show({
                                                //buttons: '.($bError ? 'Ext.MessageBox.OK' : 'Ext.MessageBox.YESNOCANCEL').',
                                                width: 400,
                                                title : "'.($bError ? 'Erreur' : 'Statut').'",
                                                msg : "'.$strAlert.'",'.
                                                (!$bError ? 'fn : callbackFn,' : '').'
                                                icon : '.($bError ? 'Ext.MessageBox.ERROR' : 'Ext.MessageBox.INFO').'
                                       });
                        }
                    </script>
                </head>
                <body onload=\'onLoadExec();\'>
                </body>
            </html>';

        echo $strhtml;
     }

     public static function AlertSaveSSDom($strJs="", $bError=false, $strAlert="", $gridReload, $gridSelectedId) {
         //global $gridReload, $gridSelectedId;

         $strhtml = '';

         if($strAlert == '') {
             $strAlert = 'Regroupement effectué.';
         }

       $strhtml .=
        '<html>
            <head>
            <!-- IE specific : forcing IE9 Document model because ext 3.0 incompatibility with IE10 -->
            <META HTTP-EQUIV="X-UA-Compatible" CONTENT="IE=9"/>
            <script  src="/Scripts/ext3.0/adapter/ext/ext-base.js"> </script>
            <script  src="/Scripts/ext3.0/ext-all.js"> </script>
            <script type="text/javascript">
                function ReindexerGeosource() {
                    parent.reBuildIndex();
                }
                function GotoData() {
                    parent.reloadGrid('.$gridReload.', '.$gridSelectedId.');
                }
                function callbackFn(buttonId) {
                    if(buttonId == \'no\') 
                        ReindexerGeosouce();
                    if(buttonId == \'cancel\') 
                        GotoData();
                }
               function onLoadExec() {
                  Ext.MessageBox.buttonText.yes = \'OK\';
                  Ext.MessageBox.buttonText.no = \'Réindexer Géosource\';
                  Ext.MessageBox.buttonText.cancel = \'Accéder à la nouvelle fiche\';
                  var msgBox = parent.Ext.Msg.show({
                                        //buttons: '.($bError ? 'Ext.MessageBox.OK' : 'Ext.MessageBox.YESNOCANCEL').',
                                        width: 400,
                                        title : "'.($bError ? 'Erreur' : 'Statut').'",
                                        msg : "'.$strAlert.'",'.
                                        (!$bError ? 'fn : callbackFn,' : '').'
                                        icon : '.($bError ? 'Ext.MessageBox.ERROR' : 'Ext.MessageBox.INFO').'
                               });
                }
            </script>
       </head>
       <body onload=\'onLoadExec();\'>
       </body>
    </html>';

    echo $strhtml;
  }

}
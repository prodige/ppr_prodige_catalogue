<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route; 

use Prodige\ProdigeBundle\Controller\BaseController;

use Prodige\ProdigeBundle\DAOProxy\DAO;


class StructureController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/structure/structure_list", name="catalogue_administration_structure_list", options={"expose"=true})
     */
    public function catalogueAdministration_structureListAction(Request $request) {
        include_once(__DIR__.'/../Resources/templates/Administration/Structure/StructureList.php');
        exit();
    }
   
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/structure/structure_form", name="catalogue_administration_structure_form", options={"expose"=true})
     */
    public function catalogueAdministration_StructureFormAction(Request $request) {
        include_once(__DIR__.'/../Resources/templates/Administration/Structure/StructureAccueil.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/structures/setstructure", name="catalogue_administration_structure_setstructure", options={"expose"=true})
     */
    public function setStructureAction(Request $request) {
        $conn = $this->getCatalogueConnection('catalogue');
        
        $mode = $request->get("mode", 0);

        if ($mode==0 ) {
            $result['success'] = false;
            $result['msg']     = htmlentities("Le paramètre 'mode' est manquant.", ENT_QUOTES, "UTF-8");
            return new JsonResponse($result);
        }

        //TODO: gestion des cas suivants :
        // - ajout de structure
        // - update de structure
        // - delete de structure (suppression possible si aucun utilisateur attaché)

        //TODO: dans UsersController
        // - association utilisateur / structure (attention l'association d'un utilisateur à une structure est une relation 1-1, il faut supprimer l'association existante
        // - un utilisateur est nécessairement associé à une structure
        
        //le code ci-dessous est un copier-coller pour la gestion des profils pour inspiration...

        //TODO: gérer dans le fichier de migration sql (upgrade_4.2.1.sql) la migration des structures d'alkanet vers prodige
        
        $field = $request->get("field", array());
        //var_dump($_REQUEST);die;
        $pk_structure = $request->get("service_id", -1);
        $pk_structure = ( !is_numeric($pk_structure) ? -1 : $pk_structure);
        $structure_nom = "";
        $structure_sigle = "";
        $structure_siren = "";
        $structure_siret = "";
        $structure_etat = "";
        $bParamOk = true;
        if( !empty($field) && $mode!=3 ){
            $pk_structure    = $field["SERVICE_ID"][1];
            $structure_nom   = $field["SERVICE_INTITULE"][1];
            $structure_sigle = $field["SERVICE_INTITULE_COURT"][1];
            $structure_siren = $field["SERVICE_SIREN"][1];
            $structure_siret = $field["SERVICE_SIRET"][1];
            $structure_etat  = $field["SERVICE_VALIDE"][1];
        } else {
          $bParamOk = ( $pk_structure>0 && $mode == 3 ? true : false );
        }

        if ( !$bParamOk ) {
            $result['success'] = false;
            $result['msg']     = htmlentities("Paramètres non valides.", ENT_QUOTES, "UTF-8");
            return new JsonResponse($result);
        }
        
        $dao = new DAO($conn, 'catalogue');
        
        switch ( $mode ) {
          case 1 :  // création
              $result['msg'] = htmlentities("La structure a été créé.", ENT_QUOTES, "UTF-8");

              $query = "INSERT INTO structure (pk_structure, structure_nom, structure_sigle, structure_siren, structure_siret)" .
                  " VALUES (:pk_structure, :structure_nom, :structure_sigle, :structure_siren, :structure_siret);";
              
              if($dao->Execute($query, array("pk_structure" => $pk_structure,
                                            "structure_nom" => $structure_nom,
                                          "structure_sigle" => $structure_sigle,
                                          "structure_siren" => $structure_siren,
                                          "structure_siret" => $structure_siret))){
                  
                  $result['success'] = true;
                  return new JsonResponse($result);
              } else {
                  $result['success'] = false;
                  $result['msg']     = htmlentities("Impossible d'exécuter la requête : $query.", ENT_QUOTES, "UTF-8");
                  return new JsonResponse($result);
              }
              break;
          case 2 :  // modification
              $result['msg'] = htmlentities("La structure a été mis à jour.", ENT_QUOTES, "UTF-8");

              $query = "UPDATE structure SET structure_nom=:structure_nom, structure_sigle=:structure_sigle, structure_siren=:structure_siren, structure_siret=:structure_siret WHERE pk_structure=:pk_structure";

              if($dao->Execute($query, array("pk_structure" => $pk_structure,
                                            "structure_nom" => $structure_nom,
                                          "structure_sigle" => $structure_sigle,
                                          "structure_siren" => $structure_siren,
                                          "structure_siret" => $structure_siret))){ 
              
                  $result['success'] = true;
                  return new JsonResponse($result);
              } else {
                  $result['success'] = false;
                  $result['msg']     = htmlentities("Impossible d'exécuter la requête : $query.", ENT_QUOTES, "UTF-8");
                  return new JsonResponse($result);
              }
              break;
          case 3 :  // suppression
              
              $result['msg'] = htmlentities("La structure a été supprimé.", ENT_QUOTES, "UTF-8");
              $query1 = "DELETE FROM utilisateur_structure WHERE fk_structure=:pk_structure";
              $query2 = "DELETE FROM structure WHERE pk_structure=:pk_structure";
              if( $dao->Execute($query1, array("pk_structure" => $pk_structure)) && 
                  $dao->Execute($query2, array("pk_structure" => $pk_structure)) ) {
                  $result['success'] = true;
                  return new JsonResponse($result);
              } else {
                  $result['success'] = false;
                  $result['msg']     = htmlentities("Impossible d'exécuter la requête : $query.", ENT_QUOTES, "UTF-8");
                  return new JsonResponse($result);
              }
              break;
          default :
              $result['success'] = false;
              $result['msg']     = htmlentities("Le paramètre 'mode' est incorrect.", ENT_QUOTES, "UTF-8");
              return new JsonResponse($result);
              break;
        }
        $result['success'] = false;
        return new JsonResponse($result);
        
    }
    
    
    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/structure/profiles_form", name="catalogue_administration_structure_users_form", options={"expose"=true})
     */
    public function catalogueAdministration_StructureUsersFormAction(Request $request) {
        include_once(__DIR__.'/../Resources/templates/Administration/Structure/StructureUsers.php');
        exit();
    }

}

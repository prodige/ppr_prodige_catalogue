<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route; 

//use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

class ServicesController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/services/services_updateMdataDomSdom", name="catalogue_administration_services_updateMdataDomSdom", options={"expose"=true})
     */
    public function catalogueAdministration_ServicesUpdateMdataDomSdomAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Services/updateMdataDomSdom.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/services/services_getAlertsModeles", name="catalogue_administration_services_getAlertsModeles", options={"expose"=true})
     */
    public function catalogueAdministration_ServicesGetAlertsModelesAction(Request $request) {
        $conn    = $this->getProdigeConnection('parametrage');
        include_once(__DIR__.'/../Resources/templates/Administration/Services/getAlertsModeles.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/services/services_ZonageGetFields", name="catalogue_administration_services_ZonageGetFields", options={"expose"=true})
     */
    public function catalogueAdministration_ServicesZonageGetFieldsAction(Request $request) {
        $conn    = $this->getProdigeConnection('public');
        include_once(__DIR__.'/../Resources/templates/Administration/Services/ZonageGetFields.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/services/services_ZonageAddFields", name="catalogue_administration_services_ZonageAddFields", options={"expose"=true})
     */
    public function catalogueAdministration_ServicesZonageAddFieldsAction(Request $request) {
        $conn    = $this->getProdigeConnection('parametrage');
        include_once(__DIR__.'/../Resources/templates/Administration/Services/ZonageAddFields.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/services/services_ZonageAddTable", name="catalogue_administration_services_ZonageAddTable", options={"expose"=true})
     */
    public function catalogueAdministration_ServicesZonageAddTableAction(Request $request) {
        $conn    = $this->getProdigeConnection('parametrage');
        include_once(__DIR__.'/../Resources/templates/Administration/Services/ZonageAddTable.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/services/services_ZonageDelFields", name="catalogue_administration_services_ZonageDelFields", options={"expose"=true})
     */
    public function catalogueAdministration_ServicesZonageDelFieldsAction(Request $request) {
        $conn    = $this->getProdigeConnection('parametrage');
        include_once(__DIR__.'/../Resources/templates/Administration/Services/ZonageDelFields.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/services/services_ZonageDelTable", name="catalogue_administration_services_ZonageDelTable", options={"expose"=true})
     */
    public function catalogueAdministration_ServicesZonageDelTableAction(Request $request) {
        $conn    = $this->getProdigeConnection('parametrage');
        include_once(__DIR__.'/../Resources/templates/Administration/Services/ZonageDelTable.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/services/services_PerimetresUnique", name="catalogue_administration_services_PerimetresUnique", options={"expose"=true})
     */
    public function catalogueAdministration_ServicesPerimetresUniqueAction(Request $request) {
        $conn    = $this->getProdigeConnection('parametrage');
        include_once(__DIR__.'/../Resources/templates/Administration/Services/PerimetresUnique.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/services/services_PerimetresDelete", name="catalogue_administration_services_PerimetresDelete", options={"expose"=true})
     */
    public function catalogueAdministration_ServicesZonagePerimetresDeleteAction(Request $request) {
        $conn    = $this->getProdigeConnection('parametrage');
        include_once(__DIR__.'/../Resources/templates/Administration/Services/PerimetresDelete.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/services/services_delUser", name="catalogue_administration_services_delUser", options={"expose"=true})
     */
    public function catalogueAdministration_ServicesDelUserAction(Request $request) {
        $conn    = $this->getProdigeConnection('parametrage');
        $pro_mapfile_path = PRO_MAPFILE_PATH;
        include_once(__DIR__.'/../Resources/templates/Administration/Services/delUser.php');
        exit();
    }
}

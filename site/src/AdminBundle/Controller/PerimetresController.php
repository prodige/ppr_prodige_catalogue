<?php

namespace ProdigeCatalogue\AdminBundle\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route; 

//use ProdigeCatalogue\WebBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\Controller\BaseController;

class PerimetresController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/perimetres/perimetres_list", name="catalogue_administration_perimetres_list", options={"expose"=true})
     */
    public function catalogueAdministration_PerimetresListAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        include_once(__DIR__.'/../Resources/templates/Administration/Perimetres/PerimetresList.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/perimetres/perimetres_form", name="catalogue_administration_perimetres_form", options={"expose"=true})
     */
    public function catalogueAdministration_PerimetresAction(Request $request) {
        $conn    = $this->getCatalogueConnection('catalogue');
        $submitUrl = $this->generateUrl('catalogue_administration_submit_form');
        $perimetresUniqueUrl = $this->generateUrl('catalogue_administration_services_PerimetresUnique');
        $perimetresDeleteUrl = $this->generateUrl('catalogue_administration_services_PerimetresDelete');
        include_once(__DIR__.'/../Resources/templates/Administration/Perimetres/PerimetresAccueil.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/perimetres/composition_form", name="catalogue_administration_perimetres_composition_form", options={"expose"=true})
     */
    public function catalogueAdministration_PerimetresCompositionAction(Request $request) {
        $conn_catalogue    = $this->getCatalogueConnection('catalogue');
        $conn_prodige    = $this->getProdigeConnection('public');
        $perimetresCompositionServiceUrl = $this->generateUrl('catalogue_administration_perimetre_composition_service');
        include_once(__DIR__.'/../Resources/templates/Administration/Perimetres/PerimetresComposition.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/perimetres/perimetre_composition_service/{perimetre_code}/{zonage_field_id}/{init_zonage_field_id}/{init_zonage_field_name}", name="catalogue_administration_perimetre_composition_service", options={"expose"=true})
     */
    public function catalogueAdministration_PerimetresCompositionServiceAction(Request $request, $perimetre_code = null, $zonage_field_id = null, $init_zonage_field_id = null, $init_zonage_field_name = null) {
        $conn_prodige    = $this->getProdigeConnection('public');
        $perimetresReloadUrl = $this->generateUrl('catalogue_administration_perimetres_reload_form');
        include_once(__DIR__.'/../Resources/templates/Administration/Services/perimetreComposition.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/perimetres/reload_form", name="catalogue_administration_perimetres_reload_form", options={"expose"=true})
     */
    public function catalogueAdministration_PerimetresReloadAction(Request $request) {
        include_once(__DIR__.'/../Resources/templates/Administration/Perimetres/PerimetresReload.php');
        exit();
    }
    


    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/perimetres/usersuploader", name="catalogue_administration_perimetres_usersuploader", options={"expose"=true})
     */
    public function catalogueAdministration_PerimetresUsersUploaderAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        $importTerritoire = $this->generateUrl('catalogue_administration_perimetres_usersimport', $request->query->all());
        include_once (__DIR__ . '/../Resources/templates/Administration/Perimetres/PerimetresUsers.php');
        exit();
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/administration/perimetres/usersimport", name="catalogue_administration_perimetres_usersimport", options={"expose"=true})
     */
    public function catalogueAdministration_PerimetresUsersImportAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        include_once (__DIR__ . '/../Resources/templates/Administration/Perimetres/PerimetresUsersImport.php');
        exit();
    }
}

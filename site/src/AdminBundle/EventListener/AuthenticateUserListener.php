<?php

namespace ProdigeCatalogue\AdminBundle\EventListener;


use Alk\Common\CasBundle\Event\AuthenticateUserEvent;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

use Symfony\Component\HttpFoundation\RequestStack;

use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;

/**
 * Listen to CAS authentication
 */
class AuthenticateUserListener {

    use ContainerAwareTrait;
    
    protected $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }
    
    /**
     * This function is called after a user is authenticated through the cas. 
     * It's up to you to load a custom user entity or throw an exception by using $event->setUser(...)
     * 
     * @param AuthenticateUserEvent $event
     */
    public function onAuthenticateUser(AuthenticateUserEvent $event)
    {
        $user = \Prodige\ProdigeBundle\Controller\User::GetUser();
        
        $request = $this->requestStack->getCurrentRequest();
        
        // cas particulier des css
        if( false !== strpos($request->getRequestUri(), '/catalogue/administration/css') ) return;
        
        // cas particulier de "mafiche" qui doit être ouvert à tout le monde
        if( false !== strpos($request->getRequestUri(), '/catalogue/administration/mafiche') ) return;
        if( false !== strpos($request->getRequestUri(), '/catalogue/administration/administration_utilisateur') ) return;
        if( false !== strpos($request->getRequestUri(), '/catalogue/administration/administration_compte_utilisateur') ) return;
        if( false !== strpos($request->getRequestUri(), '/catalogue/administration/submit_form') && 'mafiche'==$request->query->get('from') ) return;
        if( false !== strpos($request->getRequestUri(), '/catalogue/administration/page') 
            && in_array($request->query->get('Action'), array('default', Ressources::$ACTION_OPEN_INITIAL)) ) return;
        
        // si on est dans le module d'administration et que l'on est pas un ADM => sortir
        if( false !== strpos($request->getRequestUri(), '/catalogue/administration')) {
            if ( !$user->IsProdigeAdmin() ) {
                $event->setException( new \Exception(\Prodige\ProdigeBundle\Common\SecurityExceptions::MSG_DEFAULT_ACCESS_DENIED));
            }
        }
        
    }

}

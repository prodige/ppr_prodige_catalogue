<?php
namespace ProdigeCatalogue\AdminBundle\Services;
/**
 * @class LdapUser
 * @brief Classe d'acces au fichier de communication LDIF des utilisateurs declarés 
 * dans l'annuaire LDAP et a integrés dans la table POSTGRES "utilisateur"
 * @author Alkante
 */

/**
 * Classe d'acces au fichier de communication LDIF des utilisateurs declarés
 * dans l'annuaire LDAP et a integrés dans la table POSTGRES "utilisateur"
 *
 * Chaque utilisateur est declaré dans le fichier au travers
 * des description de lignes suivantes:
 * dn: ...
 * changetype: add
 * uid: carole.mayet.isere.pref.gouv.fr
 * givenName: Carole
 * mail: Carole.MAYET@isere.pref.gouv.fr
 * departmentNumber: interieur/PREFECTURE/38 ISERE/SG
 * telephoneNumber: +33 4 76 60 40 00
 * sn: MAYET
 * objectclass: frGovPerson
 * objectclass: euGovPerson
 * objectclass: inetOrgPerson
 * objectclass: organizationalPerson
 * objectclass: Person
 * objectclass: ndsLoginProperties
 * objectclass: Top
 *
 * Chaque utilisateur est declaré dans dans la table POSTGRES "utilisateur"
 * a l'aide des champs suivants:
 * usr_id <--> mail
 * usr_nom <--> givenName
 * usr_prenom <--> sn
 * usr_email <--> mail
 * usr_telephone <--> telephoneNumber
 * usr_telephone2 <--> ???
 * usr_service <--> departementNumber
 * usr_description <--> ???
 * usr_password <--> mail en CREATE
 *
 * La clé utilisée pour le CREATE/UPDATE dans la table "Utilisateur"
 * est la valeur de "mail" sur le champs "usr_mail"
 * Pour un CREATE, la valeur de "usr_password" est renseignée avec la
 * valeur "mail"
 * Pour un UPDATE, la valeur de "usr_password" n'est pas mise a jour
 */

use ProdigeCatalogue\AdminBundle\Common\Ressources\Ressources;
use Prodige\ProdigeBundle\DAOProxy\DAO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\UtilisateurVO;
use ProdigeCatalogue\AdminBundle\Common\Modules\BO\GrpRegroupeUsrVO;
use Doctrine\DBAL\Connection;

class RessourcesLdapUser {
    // Debut de la ligne qui permet de detecter le
    // debut de declaration d'un utilisateur LDAP
    public static $LDIF_FIRSTLINE_USER = 'dn:';
    
    // debut de la ligne qui permet de detecter le NOM d'unutilisateur LDAP
    public static $FILE_CHAMPS_ID = 'uid: ';
    // debut de la ligne qui permet de detecter le NOM d'unutilisateur LDAP
    public static $FILE_CHAMPS_NOM = 'sn: ';
    // debut de la ligne qui permet de detecter le PRENOM d'unutilisateur LDAP
    public static $FILE_CHAMPS_PRENOM = 'givenName: ';
    // debut de la ligne qui permet de detecter le MAIL d'unutilisateur LDAP
    public static $FILE_CHAMPS_MAIL = 'mail: ';
    // debut de la ligne qui permet de detecter le TELEPHONE d'unutilisateur LDAP
    public static $FILE_CHAMPS_PHONE1 = 'telephoneNumber: ';
    // debut de la ligne qui permet de detecter le SERVICE d'unutilisateur LDAP
    public static $FILE_CHAMPS_SERVICE = 'departmentNumber: ';
    public static $SQL_IS_EXISTE_USER = "select usr_email from utilisateur where usr_id = :usr_id";
    public static $SQL_PATTERN_CONDITION = "{1}";
    
    // format de la date a mettre lors de la creation
    // format disponible de la fct PHP: date()
    public static $DATE_FORMAT = "Y-m-j H:i:s";
}

class LdapUser {
    /**
     * @var DAO
     */
    private $dao;
    
    private $m_UserId;
    private $m_UserNom;
    private $m_UserPrenom;
    private $m_UserMail;
    private $m_UserPhone1;
    private $m_UserPhone2;
    private $m_UserService;
    private $m_UserDesc;
    private $m_UserPwd;
    private $m_UserLDAP;
    
    public static function getUsersFromFile(DAO $dao, $file) {
        $ldapUsers = null;
        $lignes4oneUser = null;
        
        if (file_exists ( $file )) {
            // ouverture du fichier
            $handleLDIF = @fopen ( $file, "r" );
            if ($handleLDIF) {
                // lecture de chaque ligne du fichier
                while ( ! feof ( $handleLDIF ) ) {
                    // lecture de la ligne courante
                    $buffer = fgets ( $handleLDIF, 4096 );
                    // test si la ligne correspond au debut d'un new userLdap
                    $isFirstLine = strpos ( $buffer, RessourcesLdapUser::$LDIF_FIRSTLINE_USER );
                    if (! ($isFirstLine === FALSE) && ($isFirstLine == 0)) {
                        // la ligne correspond a la declaration d'un nouveau userLdap
                        if (! is_null ( $lignes4oneUser )) {
                            // creation du userLdap a partir des lignes detectees precedemment
                            $ldapUsers [count ( $ldapUsers )] = LdapUser::createUserLdap ($dao,  $lignes4oneUser );
                        }
                        // re init du tableau des lignes descriptives d'un userLdap
                        $lignes4oneUser = array ();
                    }
                    // stockage de la ligne dans le but de creer un userLdap
                    $lignes4oneUser [count ( $lignes4oneUser )] = $buffer;
                }
                // on cree le dernier userLdap detecte (creation non declenche par la
                // detection d'un nouveau userLdap dans le fichier)
                if (! is_null ( $lignes4oneUser ))
                    $ldapUsers [count ( $ldapUsers )] = LdapUser::createUserLdap ($dao,  $lignes4oneUser );
                    // fermeture du fichier
                fclose ( $handleLDIF );
            }
            // print_r($ldapUsers); die();
        } else {
            echo "Erreur interne: Impossible d'ouvrir le fichier LDIF [" . $file . "]";
        }
        return $ldapUsers;
    }
    
    /**
     * @brief Crée un utilisateur
     * 
     * @param
     *            $ligne4user
     * @return new user
     */
    public static function createUserLdap(DAO $dao, $ligne4user) {
        $id = "ID";
        $nom = "Nom";
        $prenom = "Prenom";
        $mail = "Mail";
        $phone1 = "Phone";
        $phone2 = "";
        $service = "Service";
        $desc = "";
        $pwd = "Password";
        $usr_ldap = 1; // initialisé à 1 si synchronisation ldap
        $tabProfilIdLDAP = array();
        
        for($idxLigneInTab = 0; $idxLigneInTab < count ( $ligne4user ); $idxLigneInTab ++) {
            $buffer = $ligne4user [$idxLigneInTab];
            $isChampsLine = strpos ( $buffer, RessourcesLdapUser::$FILE_CHAMPS_ID );
            if (! ($isChampsLine === FALSE) && ($isChampsLine == 0)) {
                $value = substr ( $buffer, $isChampsLine + strlen ( RessourcesLdapUser::$FILE_CHAMPS_ID ) );
                $value = rtrim ( $value );
                $id = $value;
            }
            $isChampsLine = strpos ( $buffer, RessourcesLdapUser::$FILE_CHAMPS_NOM );
            if (! ($isChampsLine === FALSE) && ($isChampsLine == 0)) {
                $value = substr ( $buffer, $isChampsLine + strlen ( RessourcesLdapUser::$FILE_CHAMPS_NOM ) );
                $value = rtrim ( $value );
                $nom = $value;
            }
            $isChampsLine = strpos ( $buffer, RessourcesLdapUser::$FILE_CHAMPS_PRENOM );
            if (! ($isChampsLine === FALSE) && ($isChampsLine == 0)) {
                $value = substr ( $buffer, $isChampsLine + strlen ( RessourcesLdapUser::$FILE_CHAMPS_PRENOM ) );
                $value = rtrim ( $value );
                $prenom = $value;
            }
            $isChampsLine = strpos ( $buffer, RessourcesLdapUser::$FILE_CHAMPS_MAIL );
            if (! ($isChampsLine === FALSE) && ($isChampsLine == 0)) {
                $value = substr ( $buffer, $isChampsLine + strlen ( RessourcesLdapUser::$FILE_CHAMPS_MAIL ) );
                $value = rtrim ( $value );
                $mail = $value;
                $pwd = $value;
            }
            $isChampsLine = strpos ( $buffer, RessourcesLdapUser::$FILE_CHAMPS_PHONE1 );
            if (! ($isChampsLine === FALSE) && ($isChampsLine == 0)) {
                $value = substr ( $buffer, $isChampsLine + strlen ( RessourcesLdapUser::$FILE_CHAMPS_PHONE1 ) );
                $value = rtrim ( $value );
                $phone1 = $value;
            }
            $isChampsLine = strpos ( $buffer, RessourcesLdapUser::$FILE_CHAMPS_SERVICE );
            if (! ($isChampsLine === FALSE) && ($isChampsLine == 0)) {
                $value = substr ( $buffer, $isChampsLine + strlen ( RessourcesLdapUser::$FILE_CHAMPS_SERVICE ) );
                $value = rtrim ( $value );
                $service = $value;
            }
            
            // recherche des id de profil sur la ligne dn ex. dn: mail=nic.martin@alpes.pref.gouv.fr,ou=dreal,ou=rectorat
            $isChampsLine = strpos ( $buffer, RessourcesLdapUser::$LDIF_FIRSTLINE_USER );
            $tabProfilId = array ();
            
            if (! ($isChampsLine === FALSE) && ($isChampsLine == 0)) {
                $tabNameLdapGp = array ();
                $Tabvalue = explode ( ",ou=", $buffer );
                if (isset ( $Tabvalue ) && count ( $Tabvalue ) > 1) {
                    for($cpt = 1; $cpt < count ( $Tabvalue ); $cpt ++) {
                        // recherche des noms de grp ldap dans la table des profils groupe_profil
                        $tabNameLdapGp [] = $Tabvalue [$cpt];
                    }
                    // recuperation des id des profils ldap en fonction des noms des profils ldap
                    $tabProfilIdLDAP = GetGrpIdByLdapGpName ($dao, $tabNameLdapGp );
                }
            }
        }
        
        return new LdapUser ($dao, $id, $nom, $prenom, $mail, $phone1, $phone2, $service, $desc, $pwd, $usr_ldap, $tabProfilIdLDAP );
    }
    private function __construct(DAO $dao, $id, $nom, $prenom, $mail, $phone1, $phone2, $service, $desc, $pwd, $usr_ldap, $tabProfilIdLDAP=array()) {
        $this->dao = $dao;
        $this->m_UserId = $id;
        $this->m_UserNom = $nom;
        $this->m_UserPrenom = $prenom;
        $this->m_UserMail = $mail;
        $this->m_UserPhone1 = $phone1;
        $this->m_UserPhone2 = $phone2;
        $this->m_UserService = $service;
        $this->m_UserDesc = $desc;
        $this->m_UserPwd = $pwd;
        $this->m_UserPwdExpire = date ( RessourcesLdapUser::$DATE_FORMAT );
        $this->m_UserLDAP = $usr_ldap;
        $this->m_UserLDAPProfilId = $tabProfilIdLDAP;
    }
    /**
     * @brief renvoie l'identifiant de l'utilisateur
     * 
     * @return m_UserId
     */
    public function GetId() {
        return $this->m_UserId;
    }
    /**
     * @brief renvoie le nom de l'utilisateur
     * 
     * @return m_UserId
     */
    public function GetNom() {
        return $this->m_UserNom;
    }
    /**
     * @brief renvoie le prénom de l'utilisateur
     * 
     * @return m_UserPrenom
     */
    public function GetPrenom() {
        return $this->m_UserPrenom;
    }
    /**
     * @brief renvoie l'email de l'utilisateur
     * 
     * @return m_UserMail
     */
    public function GetMail() {
        return $this->m_UserMail;
    }
    /**
     * @brief renvoie le téléphone1 de l'utilisateur
     * 
     * @return m_UserPhone1
     */
    public function GetPhone1() {
        return $this->m_UserPhone1;
    }
    /**
     * @brief renvoie le téléphone2 de l'utilisateur
     * 
     * @return m_UserPhone2
     */
    public function GetPhone2() {
        return $this->m_UserPhone2;
    }
    /**
     * @brief renvoie le service de l'utilisateur
     * 
     * @return m_UserService
     */
    public function GetService() {
        return $this->m_UserService;
    }
    /**
     * @brief renvoie la description de l'utilisateur
     * 
     * @return m_UserDesc
     */
    public function GetDescription() {
        return $this->m_UserDesc;
    }
    /**
     * @brief renvoie le mot de passe de l'utilisateur
     * 
     * @return m_UserPwd
     */
    public function GetPassword() {
        return $this->m_UserPwd;
    }
    /**
     * @brief renvoie la date d'expiration du mot de passe de l'utilisateur
     * 
     * @return m_UserPwdExpire
     */
    public function GetPasswordExpire() {
        return $this->m_UserPwdExpire;
    }
    
    /**
     * @brief renvoie l'identifiant de l'utilisateur LDAP, si a fait l'objet d'une synchronisation LDAP
     * 
     * @return m_UserLDAP
     */
    public function GetUserLDAP() {
        return $this->m_UserLDAP;
    }
    
    /**
     * @brief renvoie les noms des profils LDAP sous la forme d'un tableau
     * 
     * @return m_UserProfilLdapName
     */
    public function GetUserLDAPProfilId() {
        return $this->m_UserLDAPProfilId;
    }
    
    /**
     * @brief renvoie l'id primaire coorespondant dans la table utilisateur (pk_utilisateur)
     * 
     * @return m_UserLDAP_Pk
     */
    public function GetUserLDAP_PK() {
        return $this->m_UserLDAP_Pk;
    }
    
    /**
     * @brief Renvoie vrai si l'utilisateur existe
     * 
     * @return $existe : boolean
     */
    public function Existe() {
        $existe = false;
        if ($this->dao) {
            $m_sSql = RessourcesLdapUser::$SQL_IS_EXISTE_USER;
            $rs = $this->dao->BuildResultSet ( $m_sSql, array("usr_id"=>$this->m_UserId) );
            if ($rs->GetNbRows () > 0) {
                $existe = true;
            }
        }
        
        return $existe;
    }
    
    /**
     * @brief Crée un utilisateur
     */
    public function create() {
        if ($this->dao) {
            $utilisateurVO = new UtilisateurVO ();
            $utilisateurVO->setDao($this->dao);
            $champs = array ();
            $values = array ();
            $champs [0] = UtilisateurVO::$USR_ID;
            $values [0] = $this->m_UserId;
            $champs [1] = UtilisateurVO::$USR_NOM;
            $values [1] = $this->m_UserNom;
            $champs [2] = UtilisateurVO::$USR_PRENOM;
            $values [2] = $this->m_UserPrenom;
            $champs [3] = UtilisateurVO::$USR_TELEPHONE;
            $values [3] = $this->m_UserPhone1;
            $champs [4] = UtilisateurVO::$USR_SERVICE;
            $values [4] = $this->m_UserService;
            $champs [5] = UtilisateurVO::$USR_PASSWORD;
            $values [5] = $this->m_UserPwd;
            $champs [6] = UtilisateurVO::$USR_EMAIL;
            $values [6] = $this->m_UserMail;
            $champs [7] = UtilisateurVO::$USR_PWD_EXPIRE;
            $values [7] = $this->m_UserPwdExpire;
            $champs [8] = UtilisateurVO::$USR_LDAP;
            $values [8] = $this->m_UserLDAP;
            
            for($idx = 0; $idx < count ( $values ); $idx ++) {
                if ($champs[$idx] == UtilisateurVO::$USR_PASSWORD) // encrypt password
                    $values [$idx] = md5($values [$idx]);
            }
            $utilisateurVO->InsertValues ( $champs, $values );
            $utilisateurVO->Commit ();
            
            $pk_utilisateur_LDAP = GetPk_utilisateurByIdLdap ( $this->dao,  $this->m_UserId );
            updateCorrespondanceProfilLdap ($this->dao, $pk_utilisateur_LDAP, $this->m_UserLDAPProfilId );
        }
    }
    
    /**
     * Met à jour l'utilisateur
     */
    public function update() {
        if ($this->dao) {
            $utilisateurVO = new UtilisateurVO ();
            $utilisateurVO->setDao($this->dao);
            $champs = array ();
            $values = array ();
            $champs [0] = UtilisateurVO::$USR_ID;
            $values [0] = $this->m_UserId;
            $champs [1] = UtilisateurVO::$USR_NOM;
            $values [1] = $this->m_UserNom;
            $champs [2] = UtilisateurVO::$USR_PRENOM;
            $values [2] = $this->m_UserPrenom;
            $champs [3] = UtilisateurVO::$USR_TELEPHONE;
            $values [3] = $this->m_UserPhone1;
            $champs [4] = UtilisateurVO::$USR_SERVICE;
            $values [4] = $this->m_UserService;
            // $champs[5] = UtilisateurVO::$USR_PASSWORD;
            // $values[5] = $this->m_UserPwd;
            // $champs[6] = UtilisateurVO::$USR_EMAIL;
            // $values[6] = $this->m_UserMail;
            $champs [5] = UtilisateurVO::$USR_LDAP;
            $values [5] = $this->m_UserLDAP;
            $utilisateurVO->Update ( $champs, $values, UtilisateurVO::$USR_ID, "'" . str_replace ( "'", "''", $this->m_UserId ) . "'" );
            $utilisateurVO->Commit ();
            
            $pk_utilisateur_LDAP = GetPk_utilisateurByIdLdap ( $this->dao,  $this->m_UserId );
            deleteCorrespondanceProfilLdap ( $this->dao,  $pk_utilisateur_LDAP );
            updateCorrespondanceProfilLdap ( $this->dao,  $pk_utilisateur_LDAP, $this->m_UserLDAPProfilId );
        }
    }
}

/**
 * @brief Recherche la listes des identifiant des profils en fonctions de noms () de groupes fournit lros d'un import ldap
 * 
 * @param $tabNameLdapGp :
 *            tableau listant les noms de groupes lors d'un import ldap
 * @return $tableau des identifiants primaires des profils depuis la table groupe_profil
 */
function GetGrpIdByLdapGpName(DAO $dao, $tabNameLdapGp) {
    $tabIdProfil = array ();
    if ($dao) {
        $tabCorrespIdName = array ();
        // recuperation des id des profils ldap en fonction des nom des profils ldap
        $m_sSql = "select pk_groupe_profil, grp_nom_ldap from groupe_profil where grp_nom_ldap is not null";
        $rs = $dao->BuildResultSet ( $m_sSql );
        for($rs->First (); ! $rs->EOF (); $rs->Next ()) {
            foreach ( $tabNameLdapGp as $k => $nameLdap ) {
                if (trim ( $nameLdap ) == trim ( $rs->Read ( 1 ) )) {
                    $tabIdProfil [] = $rs->Read ( 0 );
                }
            }
        }
    }
    return $tabIdProfil;
}

/**
 * @brief Recherche l'id primaire dans la table utilisateur en fonction de usr_id (email) fourni lors de l'import ldap
 * 
 * @param
 *            $usr_id
 * @return $pk_utilisateur
 */
function GetPk_utilisateurByIdLdap(DAO $dao, $usr_id) {
    $pk_utilisateur = - 1;
    if ($dao) {
        $m_sSql = "select pk_utilisateur from utilisateur where usr_id=:usr_id";
        $rs = $dao->BuildResultSet ( $m_sSql, array("usr_id"=>$usr_id) );
        for($rs->First (); ! $rs->EOF (); $rs->Next ()) {
            $pk_utilisateur = $rs->Read ( 0 );
        }
    }
    return $pk_utilisateur;
}

/**
 * @brief pour la maj de la table de correspondance profils "grp_regroupe_usr", il faut recupérer le pk_utilisateur correspondant dans utilisateur
 * 
 * @param $pk_utilisateur_LDAP :
 *            identifiant de l'utilisateur PK dans la table utilisateur
 * @param $m_UserLDAPProfilId :
 *            tableau des nom de profil fournit dans le fichier d'import ldap
 * @return
 *
 */
function updateCorrespondanceProfilLdap(DAO $dao, $pk_utilisateur_LDAP, $m_UserLDAPProfilId) {
    
    // Crée la correspondance entre profil ldap et utilisateur
    $GrpRegroupeUsrVO = new GrpRegroupeUsrVO ();
    $GrpRegroupeUsrVO->setDao($dao);
    $champsGrp = array (
            $GrpRegroupeUsrVO::$GRPUSR_FK_UTILISATEUR,
            $GrpRegroupeUsrVO::$GRPUSR_FK_GROUPE_PROFIL 
    );
    
    foreach ( $m_UserLDAPProfilId as $keyGp => $pk_groupe_profil ) {
        $valeursGp = array ($pk_utilisateur_LDAP ,  $pk_groupe_profil);
        
        $GrpRegroupeUsrVO->InsertValues ( $champsGrp, $valeursGp );
        $GrpRegroupeUsrVO->Commit ();
    }
}

/**
 * @brief pour la maj des profils fournis par l'import ldap, suppression de tous les profils identifiés ldap dans la table des profils
 * @brief (groupe_profil) dans la table grp_regroupe_usr pour l'utilisateur importé
 * @brief RQ.
 * Ne concerne que les profils "LDAP" car where grp_nom_ldap is not null
 * 
 * @param $pk_utilisateur_LDAP :
 *            identifiant de l'utilisateur PK dans la table utilisateur
 * @return
 *
 */
function deleteCorrespondanceProfilLdap(DAO $dao, $pk_utilisateur_LDAP) {
    if ($dao) {
        $m_sSql = "select pk_groupe_profil, grp_nom_ldap from groupe_profil where grp_nom_ldap is not null";
        $rs = $dao->BuildResultSet ( $m_sSql );
        $query = "";
        for($rs->First (); ! $rs->EOF (); $rs->Next ()) {
            $query .= " DELETE FROM GRP_REGROUPE_USR WHERE GRPUSR_FK_UTILISATEUR =" . $pk_utilisateur_LDAP . " AND GRPUSR_FK_GROUPE_PROFIL =" . $rs->Read ( 0 ) . ";";
        }
        if ($query != "") {
            $dao->Execute ( $query );
        }
    }
}
	
	

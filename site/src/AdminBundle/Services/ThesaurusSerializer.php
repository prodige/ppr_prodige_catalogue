<?php

namespace ProdigeCatalogue\AdminBundle\Services;

use Prodige\ProdigeBundle\Services\GeonetworkInterface;

/**
 * Cette classe permet de générer un fichier Thésaurus et de le mettre à jour sur la plateforme Geosource.
 *
 * @author alkante
 */
class ThesaurusSerializer
{
  
  /**
   *
   * @var Singleton
   * @access private
   * @static
   *
   */
  private static $_instance = null;
  private $urlGeonetwork;
  private $urlThesaurus;
  private $urlThesaurusUpload;
  private $urlThesaurusRemove;
  private $name;
  private $path;
  private $conn;
  private $curlOpts;
  private $fullPath;
  private $theme;

  function __construct($conn, $curlOpts, $url, $name)
  {
    $this->name = $name;
    $this->theme = "theme";
    
    $this->urlGeonetwork = $url;
    $this->path = sys_get_temp_dir();
    $this->conn = $conn;
    $this->urlThesaurus = $this->urlGeonetwork . "/thesaurus/theme/domaine";
    $this->urlThesaurusUpload = "thesaurus.upload?ref=external." .$this->theme. "." .$this->name."&_content_type=json";
    $this->urlThesaurusRemove = "thesaurus.remove?ref=external." .$this->theme. "." .$this->name;
    $this->curlOpts = $curlOpts;
    $this->fullPath = $this->path . '/' . $this->name . ".rdf";
  }


  
  /**
   * Méthode qui crée l'unique instance de la classe
   * si elle n'existe pas encore puis la retourne.
   *
   * @param
   *          void
   * @return ThesaurusSerializer
   */
  public static function getInstance($conn, $curlOpts, $name)
  {
    if (is_null(self::$_instance))
    {
      self::$_instance = new ThesaurusSerializer($conn, $curlOpts, PRO_GEONETWORK_URLBASE, $name);
    }
    
    return self::$_instance;
  }
  
  /**
   * Méthode qui crée l'unique instance de la classe
   * si elle n'existe pas encore puis la retourne.
   * Dans un context "hors" contrôlleur
   *
   * @param
   *          void
   * @return ThesaurusSerializer
   */
  public static function getStandaloneInstance($conn, $curlOpts,$url, $name)
  {
    if (is_null(self::$_instance))
    {
      self::$_instance = new ThesaurusSerializer($conn, $curlOpts,$url, $name);
    }
    
    return self::$_instance;
  }
  
  /**
   * Génère un fichier Thesaurus à partir des Rubriques,domaines et sous domaines et le transmet à Geosource
   * Si le Thesaurus est déjà présent sur Geosource il est supprimé, puis recréer.
   */
  public function generateDocument($bUpload = true)
  {
    $graph = new \EasyRdf\Graph();
    $this->processConceptScheme($graph);
    $this->processSousDomaine($graph);
    $this->processDomaine($graph);
    $this->processRubric($graph);
    
    $data = $graph->serialise('rdf');
    if (! is_scalar($data))
    {
      $data = var_export($data, true);
    }
    
    
    //upload it to geonetwork with geonetwork tools
    if($bUpload){
        @unlink($this->fullPath);
        @file_put_contents($this->fullPath, $data);
        @$this->deleteExistingThesaurus();
        @$this->uploadThesaurus();
        
    }else{
        //TODO put in in param directory;
        @file_put_contents($this->fullPath, $data);
    }
    
  }

  /**
   * Upload d'un Thésaurus sur geosource
   * - un appel post est effectué avec un fichier dans le répertoire theme
   */
  private function uploadThesaurus()
  {
    $geonetwork = new GeonetworkInterface($this->urlGeonetwork, "srv/fre", array(CURLOPT_HTTPHEADER => array('Content-Type: multipart/form-data')));
    
    $formData = array(
        'url' => '',
        'dir' => $this->theme,
    );
    $geonetwork->upload($this->urlThesaurusUpload, $formData, array("fname" => $this->fullPath), "application/rdf+xml");
  }

  /**
   * Suppression d'un Thésaurus sur geosource
   *  - un appel get est effectué
   */
  private function deleteExistingThesaurus()
  {
    $geonetwork = new GeonetworkInterface($this->urlGeonetwork, 'srv/fre/');
    $geonetwork->get($this->urlThesaurusRemove);
  }

  /**
   * Génération du header du Thésaurus
   * @param unknown $graph
   */
  private function processConceptScheme($graph)
  {
    $me = $graph->resource('http://geonetwork-opensource.org/domaines', 'skos:ConceptScheme');
    $me->set('dc:title', 'Domaines');
    $me->set('dc:description', 'Dictionnaire des domaines de la plateforme');
    $orga = $graph->newBnode('foaf:organization');
    $orga->set('foaf:name', '');
    $me->set('dc:creator', $orga);
    $me->set('dcterms:issued', date("Y-m-d"));
    $me->set('dcterms:modified', date("Y-m-d"));
  }

  /**
   * Process des rubriques
   * @param unknown $graph
   */
  private function processRubric($graph)
  {
    $tabRubric = $this->conn->fetchAllAssociative("select rubric_id, rubric_name from catalogue.rubric_param ");
    foreach ( $tabRubric as $key => $tabInfo )
    {
      $tabInfo ["rubric_name"] = html_entity_decode($tabInfo ["rubric_name"], ENT_QUOTES, 'UTF-8');
      $rubric = $graph->resource($this->urlThesaurus . '#rubric_' . $tabInfo ["rubric_id"], 'skos:Concept');
      $rubric->addLiteral('skos:scopeNote', $tabInfo ["rubric_name"], 'fr');
      $rubric->addLiteral('skos:scopeNote', $tabInfo ["rubric_name"], 'en');
      $rubric->addLiteral('skos:prefLabel', $tabInfo ["rubric_name"], 'fr');
      $rubric->addLiteral('skos:prefLabel', $tabInfo ["rubric_name"], 'en');
      $tabDom = $this->conn->fetchAllAssociative("select pk_domaine from catalogue.domaine where dom_rubric=:id ", array(
          "id" => $tabInfo ["rubric_id"]
      ));
      foreach ( $tabDom as $tabInfoDom )
      {
        $rubric->add('skos:narrower', $graph->resource($this->urlThesaurus . '#domaine_' . $tabInfoDom ["pk_domaine"]));
      }
    }
  }

  /**
   * Process des domaines
   * @param unknown $graph
   */
  private function processDomaine($graph)
  {
    $tabDomaine = $this->conn->fetchAllAssociative("select pk_domaine, dom_nom, dom_description, dom_rubric from catalogue.domaine ");
    foreach ( $tabDomaine as $key => $tabInfo )
    {
      $tabInfo ["dom_nom"] = html_entity_decode($tabInfo ["dom_nom"], ENT_QUOTES, 'UTF-8');
      $tabInfo ["dom_description"] = html_entity_decode($tabInfo ["dom_description"], ENT_QUOTES, 'UTF-8');
      $dom = $graph->resource($this->urlThesaurus . '#domaine_' . $tabInfo ["pk_domaine"], 'skos:Concept');
      $dom->addLiteral('skos:scopeNote', $tabInfo ["dom_description"], 'fr');
      $dom->addLiteral('skos:scopeNote', $tabInfo ["dom_description"], 'en');
      $dom->addLiteral('skos:prefLabel', $tabInfo ["dom_nom"], 'fr');
      $dom->addLiteral('skos:prefLabel', $tabInfo ["dom_nom"], 'en');
      $dom->add('skos:broader', $graph->resource($this->urlThesaurus . '#rubric_' . $tabInfo ["dom_rubric"]));
      $tabSdom = $this->conn->fetchAllAssociative("select pk_sous_domaine from catalogue.sous_domaine where  ssdom_fk_domaine=:id ", array(
          "id" => $tabInfo ["pk_domaine"]
      ));
      foreach ( $tabSdom as $keySdom => $tabInfoSdom )
      {
        $dom->add('skos:narrower', $graph->resource($this->urlThesaurus . '#sousdomaine_' . $tabInfoSdom ["pk_sous_domaine"]));
      }
    }
  }

  /**
   * Process des sous domaines
   * @param unknown $graph
   */
  private function processSousDomaine($graph)
  {
    $tabSousDomaine = $this->conn->fetchAllAssociative("select pk_sous_domaine, ssdom_nom, ssdom_description, ssdom_fk_domaine from catalogue.sous_domaine");
    foreach ( $tabSousDomaine as $key => $tabInfo )
    {
      $tabInfo ["ssdom_nom"] = html_entity_decode($tabInfo ["ssdom_nom"], ENT_QUOTES, 'UTF-8');
      $tabInfo ["ssdom_description"] = html_entity_decode($tabInfo ["ssdom_description"], ENT_QUOTES, 'UTF-8');
      $sdom = $graph->resource($this->urlThesaurus . '#sousdomaine_' . $tabInfo ["pk_sous_domaine"], 'skos:Concept');
      $sdom->addLiteral('skos:scopeNote', $tabInfo ["ssdom_description"], 'fr');
      $sdom->addLiteral('skos:scopeNote', $tabInfo ["ssdom_description"], 'en');
      $sdom->addLiteral('skos:prefLabel', $tabInfo ["ssdom_nom"], 'fr');
      $sdom->addLiteral('skos:prefLabel', $tabInfo ["ssdom_nom"], 'en');
      $sdom->add('skos:broader', $graph->resource($this->urlThesaurus . '#domaine_' . $tabInfo ["ssdom_fk_domaine"]));
    }
  }
}

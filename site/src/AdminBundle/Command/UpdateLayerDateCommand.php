<?php

namespace ProdigeCatalogue\AdminBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelInterface;


class UpdateLayerDateCommand extends Command
{
    protected static $defaultName = 'prodige:update_layer_date';

    /** @var ParameterBagInterface */
    protected $params;

    /** @var RequestStack */
    protected $requestStack;


    public function __construct(
        ParameterBagInterface $params,
        RequestStack $stack,
        KernelInterface $kernel,
        ContainerInterface $container
    ) {
        parent::__construct();
        $this->params = $params;
        $this->requestStack = $stack;
        $this->kernel = $kernel;
        $this->container = $container;
    }

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this->setName('prodige:update_layer_date')
            ->setDescription('Update changedate value for table couche_donnees from layer table last commit date');
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
//        dump($this->container);
//        exit();
        $catalogue = $this->getCatalogueConnection('public,catalogue');
        $prodige = $this->getProdigeConnection();

        // Get vector / table data
        $strSql = "SELECT pk_couche_donnees, couchd_emplacement_stockage FROM couche_donnees WHERE couchd_type_stockage IN ('1', '-3')";
        $results = $catalogue->fetchAllAssociative($strSql);
        $output->writeln("Nombre de couches trouvées : " . count($results));

        foreach ($results as $result) {
            $couchdEmplacementStockage = $result['couchd_emplacement_stockage'];
            $coucheDonnees = $result['pk_couche_donnees'];
            if ($couchdEmplacementStockage != '') {
                $strSql = "SELECT pg_xact_commit_timestamp(xmin) as timestamp from $couchdEmplacementStockage where pg_xact_commit_timestamp(xmin) is not null order by pg_xact_commit_timestamp(xmin) desc limit 1";
                try {
                    $timestamps = $prodige->fetchAllAssociative($strSql);
                    foreach ($timestamps as $timestamp) {
                        $output->writeln(
                            "Mise à jour de la couche de données $coucheDonnees avec la valeur suivante : " . $timestamp['timestamp']
                        );
                        $strSql = "UPDATE couche_donnees SET changedate = '" . $timestamp['timestamp'] . "' WHERE pk_couche_donnees = $coucheDonnees";
                        $catalogue->executeUpdate($strSql);
                    }
                } catch (\Exception $e) {
                    $output->writeln("Erreur de mise à jour pour la couche " . $couchdEmplacementStockage);
                    return Command::FAILURE;
                }
            }
        }
        return Command::SUCCESS;
    }

    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Get Doctrine connection
     *
     * @param string $connection_name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public")
    {
        $conn = $this->container->get("doctrine")->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);

        return $conn;
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }

}

<?php

namespace ProdigeCatalogue\AdminBundle\Command;

use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Services\LdapUtils;
use Prodige\ProdigeBundle\Common\AlkRequest;
use Prodige\ProdigeBundle\EventListener\RequestListener;

//use Symfony\Component\Console\Input\InputArgument;

class MigrationPasswordCommand extends Command
{

    protected static $_DEFAULT_PWD = 'alkante';
    protected static $_DEBUG_EMAIL_TO = 'v.legloahec@alkante.com';

    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;


    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this->setName('prodige:migrationPassword')
            ->setDescription('Migration of users password from version 3.4.x to 4.0')
            ->addOption(
                'check-mail',
                'c',
                InputOption::VALUE_NONE,
                "vérifier les adresses email de tous les comptes utilisateur (ne fait rien d'autre)"
            )
            ->addOption(
                'debug',
                'd',
                InputOption::VALUE_NONE,
                "active le mode debug, le mot de passe '".self::$_DEFAULT_PWD."' est affecté à chaque compte, l'email destinaire est ".self::$_DEBUG_EMAIL_TO
            )
            ->addOption(
                'user',
                'u',
                InputOption::VALUE_OPTIONAL,
                "uniquement en debug, ne traiter qu'un seul utilisateur, ex. user=<user_id>"
            )
            ->addOption(
                'send-to',
                't',
                InputOption::VALUE_OPTIONAL,
                "changer l'adresse destinataire du mode debug",
                self::$_DEBUG_EMAIL_TO
            )
            ->addOption(
                'force-send',
                'f',
                InputOption::VALUE_NONE,
                "forcer l'envoie d'email aux comptes utilisateurs (false par défaut)"
            );
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // call the prodige.configreader service
        $this->container->get('prodige.configreader');

        $io = new SymfonyStyle($input, $output);

        // init config
        $conn = $this->getCatalogueConnection('catalogue');
        $this->container->get('prodige.configreader');

        // simulates a call to onKernelController
        $c = new RequestListener();
        $c->setContainer($this->container);
        $c->onKernelRequest(
            new \Symfony\Component\HttpKernel\Event\GetResponseEvent(
                $this->container->get('kernel'),
                new \Symfony\Component\HttpFoundation\Request(),
                \Symfony\Component\HttpKernel\HttpKernelInterface::MASTER_REQUEST
            )
        );

        // get the password encoder
        $encoder = $this->container->get('prodige.passwordencoder');
        $encoder instanceof \Prodige\ProdigeBundle\Services\PasswordEncoder;

        // test password encoding
        /*
        $raw = '123456';
        $encoded = $encoder->encode($raw);
        $output->writeln(sprintf('raw=%s, encoded=%s, isValid=%s', $raw, $encoded, intval($encoder->isValid($encoded, $raw))));
        */

        $debug = $input->getOption('debug');
        $debugUser = $input->getOption('user');
        $forceSend = $input->getOption('force-send');
        $sendTo = $input->getOption('send-to');
        $checkMail = $input->getOption('check-mail');

        $password = self::$_DEFAULT_PWD;
        $encoded = $encoder->encode($password);

        // config LDAP
        $ldap_host = $this->container->getParameter('ldap_host');
        $ldap_port = $this->container->getParameter('ldap_port');
        $ldap_baseDn = $this->container->getParameter('ldap_base_dn');
        $ldap_dnAdmin = $this->container->getParameter('ldap_search_dn');
        $ldap_password = $this->container->getParameter('ldap_password');

        $ldap = LdapUtils::getInstance($ldap_host, $ldap_port, $ldap_dnAdmin, $ldap_password, $ldap_baseDn);

        if ($debug && $debugUser) {
            $users = $conn->fetchAllAssociative(
                'select pk_utilisateur, usr_id, usr_nom, usr_prenom, usr_email, usr_password from utilisateur where usr_id=:usr',
                array('usr' => $debugUser)
            );
        } else {
            $users = $conn->fetchAllAssociative(
                'select pk_utilisateur, usr_id, usr_nom, usr_prenom, usr_email, usr_password from utilisateur'
            );
        }

        foreach ($users as $user) {
            $io->title(
                sprintf(
                    'Utilisateur : %s, %s, %s, %s',
                    $user['pk_utilisateur'],
                    $user['usr_id'],
                    $user['usr_email'],
                    $user['usr_password']
                )
            );
            $usrEmailIsValid = \Swift_Validate::email($user['usr_email']);
            if (!$usrEmailIsValid) {
                $io->warning('> Attention, l\'adresse email contient des caractères non valides');
            }
            if ($checkMail) {
                continue;
            }

            // générer nouveau password
            if (false === $debug) {
                $password = bin2hex(random_bytes(3)); // random password 6 car. length
                $encoded = $encoder->encode($password);
            }

            $usrToken = base64_encode(time().uniqid());
            // mettre à jour le mdp dans la base catalogue
            $conn->update('utilisateur',
                array(
                    'usr_password' => $encoded,
                    'usr_description' => $usrToken,
                    'usr_pwdexpire' => \ProdigeCatalogue\WebBundle\Controller\ForgottenPasswordController::getNewPdwExpireDate(
                    ),
                ),
                array('pk_utilisateur' => $user['pk_utilisateur'])
            );
            $io->text(sprintf('> BDD  : mot de passe ré-initialisé, raw=%s, crypt=%s', $password, $encoded));

            // mettre à jour le compte sur LDAP
            $ldapOk = false;
            if ($ldap->userExist($user['usr_id'])) {
                $ldapOk = $ldap->changeUserPassword($user['usr_id'], $encoded);
                $io->text(
                    sprintf(
                        '> LDAP : mot de passe ré-initialisé, raw=%s, crypt=%s -> %s',
                        $password,
                        $encoded,
                        $ldapOk ? 'OK' : 'Échec!'
                    )
                );
            } else {
                $io->text(sprintf('> LDAP : le compte uid=%s n\'existe pas', $user['usr_id']));
            }

            // envoyer les emails
            if ($ldapOk) {
                $mailFrom = PRO_CATALOGUE_EMAIL_AUTO;
                $nameFrom = PRO_CATALOGUE_NOM_EMAIL_AUTO;
                $mailTo = $debug ? $sendTo : $user['usr_email'];
                $nameTo = $debug ? 'DEBUG' : $user['usr_prenom'].' '.$user['usr_nom'];

                $strTokenPwd = AlkRequest::getEncodeParam("l=".$user['usr_id']."&p=".$usrToken);
                $url = $this->container->getParameter('PRODIGE_URL_CATALOGUE').
                    $this->container->get('router')->generate(
                        'catalogue_web_chgpwd',
                        array('chgpwdToken' => $strTokenPwd)
                    );

                $html = $this->container->get('templating')->render(
                    'AdminBundle:mail:migration.html.twig',
                    array('identifiant' => $user['usr_id'], 'url' => $url)
                );

                $message = \Swift_Message::newInstance();
                $message
                    ->setSubject("Mise à jour de Prodige - ré-initialisation de mot de passe")
                    ->setFrom($mailFrom, $nameFrom)
                    ->setTo($mailTo, $nameTo)
                    ->setBody($html, 'text/html', 'UTF-8');

                if ($forceSend) {
                    if ($this->container->get('mailer')->send($message)) {
                        $io->text('> message envoyé');
                    } else {
                        $io->warning('Le message n\a pas été envoyé');
                    }
                }
            }
        }
    }


    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->container->get('doctrine')->getManager();
    }

    /**
     * Get Doctrine connection
     *
     * @param string $connection_name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public")
    {
        $conn = $this->container->get("doctrine")->getConnection($connection_name);
        $conn->exec('set search_path to '.$schema);

        return $conn;
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }
}
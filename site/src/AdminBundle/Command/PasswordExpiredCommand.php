<?php

namespace ProdigeCatalogue\AdminBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Services\LdapUtils;
use Prodige\ProdigeBundle\EventListener\RequestListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Cette commande s'occupe de scanner les comptes utilisateur pour désactiver ceux dont le mot de passe a expiré
 */
class PasswordExpiredCommand extends Command
{
    protected static $defaultName = 'prodige:password:lockexpired';

    /** @var ContainerInterface */
    protected $container;

    /** @var KernelInterface */
    protected $kernel;


    public function __construct(ContainerInterface $container, KernelInterface $kernel)
    {
        parent::__construct();
        $this->container = $container;
        $this->kernel = $kernel;
    }

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this->setDescription(
            'Cette commande s\'occupe de scanner les comptes utilisateur pour désactiver ceux dont le mot de passe a expiré'
        );
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // call the prodige.configreader service
        $this->container->get('prodige.configreader');

        $io = new SymfonyStyle($input, $output);

        // init config
        $conn = $this->getCatalogueConnection('catalogue');

        // simulates a call to onKernelController
        $c = new RequestListener();
        $c->setContainer($this->container);
        $c->onKernelRequest(
            new \Symfony\Component\HttpKernel\Event\RequestEvent(
                $this->kernel,
                new \Symfony\Component\HttpFoundation\Request(),
                \Symfony\Component\HttpKernel\HttpKernelInterface::MASTER_REQUEST
            )
        );

        // get the password encoder
        $encoder = $this->container->get('prodige.passwordencoder');
        $encoder instanceof \Prodige\ProdigeBundle\Services\PasswordEncoder;

        // config LDAP
        $ldap_host = $this->container->getParameter('ldap_host');
        $ldap_port = $this->container->getParameter('ldap_port');
        $ldap_baseDn = $this->container->getParameter('ldap_base_dn');
        $ldap_dnAdmin = $this->container->getParameter('ldap_search_dn');
        $ldap_password = $this->container->getParameter('ldap_password');

        $ldap = LdapUtils::getInstance($ldap_host, $ldap_port, $ldap_dnAdmin, $ldap_password, $ldap_baseDn);

        if (defined("PRO_CATALOGUE_EMAIL_ADMIN") && defined('PRODIGE_CATALOG_INTRO')) {
            $days = 30;
            // chercher tous les utilisateurs dont le compte va expirer d'ici 30 jours
            $query = "SELECT 
                        r.pk_utilisateur,
                        r.grpusr_fk_groupe_profil,
                        r.usr_id, 
                        r.usr_nom, 
                        r.usr_prenom, 
                        r.usr_email, 
                        r.usr_password,
                        r.grp_template_mail as template_mail,
                        r.date_expiration_compte
                        from (
                            SELECT 
                                pk_utilisateur,
                                gru.grpusr_fk_groupe_profil,
                                usr_id, 
                                usr_nom, 
                                usr_prenom, 
                                usr_email, 
                                usr_password,
                                gp.grp_template_mail,
                                date_expiration_compte::date,
                                rank() OVER (
                                PARTITION BY pk_utilisateur ORDER BY gru.grpusr_is_principal DESC, gp.grp_template_mail is not null DESC, gru.grpusr_fk_groupe_profil ASC
                                ) AS rank
                                from utilisateur 
                                inner join grp_regroupe_usr as gru on gru.grpusr_fk_utilisateur = utilisateur.pk_utilisateur
                                inner join groupe_profil as gp on  gru.grpusr_fk_groupe_profil = gp.pk_groupe_profil
                                where ( 
                                    date_expiration_compte IS NOT NULL 
                                and 
                                    date_expiration_compte::date - integer '{$days}' = now()::date 
                                )
                            ) as r
                        where r.rank = 1;";

            $users = $conn->fetchAllAssociative($query);

            $output->writeln(count($users) . " compte va expirer d'ici 30 jours");
            foreach ($users as $user) {
                $user_email = $user["usr_email"];
                $user_fullname = trim(sprintf("%s %s", $user["usr_prenom"], $user["usr_nom"]));


                $body = implode("%0d%0a", array(
                    "Bonjour,",
                    "",
                    "Mon compte " . PRODIGE_CATALOG_INTRO . " arrive à expiration le " . date('d/m/Y') . ". ",
                    "",
                    "Je souhaiterais prolonger mon accès au site " . PRODIGE_CATALOG_INTRO . ". " . sprintf(
                        "Mon identifiant est %s. ",
                        $user["usr_id"]
                    ),
                    "",
                    "Cordialement, ",
                    "",
                    sprintf("%s %s – %s", $user["usr_prenom"], $user["usr_nom"], $user["usr_email"]),
                ));

                $output->writeln("Compte de " . $user_fullname . " arrive à expiration le " . date('d/m/Y'));

                $mailto = "mailto:" . PRO_CATALOGUE_EMAIL_ADMIN . "?subject=Demande de prolongation des droits d'accès à " . PRODIGE_CATALOG_INTRO .
                    "&body=" . $body;


                if ($user_email) {
                    if (isset($user["template_mail"]) && $user["template_mail"]) {
                        $user["template_mail"] = str_replace('{$days}', $days, $user["template_mail"]);
                        $user["template_mail"] = str_replace(
                            '{$date_expiration}',
                            $user["date_expiration_compte"],
                            $user["template_mail"]
                        );
                        $user["template_mail"] = str_replace(
                            '{$usr_prenom}',
                            $user["usr_prenom"],
                            $user["template_mail"]
                        );
                        $user["template_mail"] = str_replace('{$usr_nom}', $user["usr_nom"], $user["template_mail"]);
                        $user["template_mail"] = str_replace(
                            '{$usr_email}',
                            $user["usr_email"],
                            $user["template_mail"]
                        );
                        $user["template_mail"] = str_replace('{$usr_id}', $user["usr_id"], $user["template_mail"]);
                    }

                    $body = isset($user["template_mail"]) ? html_entity_decode($user["template_mail"]) : implode(
                        "<br>",
                        array(
                            $user_fullname . ",",
                            "",
                            "Votre compte " . PRODIGE_CATALOG_INTRO . " expire dans {$days} jour" . ($days > 1 ? "s" : "") . ".",
                            "",
                            "Pour demander une prolongation de vos droits d'accès, contactez un administrateur du site " . PRODIGE_CATALOG_INTRO . " à l'adresse suivante : ",
                            '<a href="' . $mailto . '">' . PRO_CATALOGUE_EMAIL_ADMIN . "</a>.",
                            "",
                            "Cordialement,",
                            "",
                            "<i>Message généré automatiquement, merci de ne pas y répondre.</i>",
                        )
                    );

                    $output->writeln("Compte de " . $user_fullname . " expire dans" . $days . " jours");
                    
                    $message = new \Swift_Message("Votre compte " . PRODIGE_CATALOG_INTRO . " expire dans {$days} jour" . ($days > 1 ? "s" : ""), 
                                                  $body, 'text/html', 'UTF-8');
                    $message->setFrom(PRO_CATALOGUE_EMAIL_AUTO, PRO_CATALOGUE_NOM_EMAIL_AUTO)->setTo($user_email, $user_fullname);

                    $this->container->get('mailer')->send($message);
                }
            }
        }

        /**
         * @since 4.1.4 gestion de l'expiration des mdp géré par LDAP PPolicy + CAS, ne gérer que l'expiration du compte
         */

        // chercher tous les utilisateurs dont le mot de passe a expiré
        $query = ' select pk_utilisateur, usr_id, usr_nom, usr_prenom, usr_email, usr_password '
            . ' from utilisateur '
            . ' where ( date_expiration_compte IS NOT NULL and date_expiration_compte < now() ) ';
        $users = $conn->fetchAllAssociative($query);

        foreach ($users as $user) {
            $password = bin2hex(random_bytes(10)); // random password 20 car. length
            $encoded = $encoder->encode($password);

            // mettre à jour le mdp dans la base catalogue
            $conn->update('utilisateur',
                array(
                    'usr_password' => $encoded,
                ),
                array('pk_utilisateur' => $user['pk_utilisateur'])
            );

            // mettre à jour le compte sur LDAP
            $ldapOk = false;
            if ($ldap->userExist($user['usr_id'])) {
                $ldapOk = $ldap->changeUserPassword($user['usr_id'], $encoded);
            }
        }

        return Command::SUCCESS;
    }


    /**
     * Get Doctrine connection
     *
     * @param string $connection_name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public")
    {
        $conn = $this->container->get("doctrine")->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);

        return $conn;
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }
}

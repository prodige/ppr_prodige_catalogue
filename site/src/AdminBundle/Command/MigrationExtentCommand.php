<?php

namespace ProdigeCatalogue\AdminBundle\Command;

use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelInterface;

class MigrationExtentCommand extends Command
{
    protected static $defaultName = 'prodige:migration-extent';

    /** @var ParameterBagInterface */
    protected $params;

    /** @var RequestStack */
    protected $requestStack;


    public function __construct(
        ParameterBagInterface $params,
        RequestStack $stack,
        KernelInterface $kernel,
        ContainerInterface $container
    ) {
        parent::__construct();
        $this->params = $params;
        $this->requestStack = $stack;
        $this->kernel = $kernel;
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setName('prodige:migration-extent')
            ->setDescription('Migration des emprises sur les services WMS et WFS');
    }

    /**
     * Migration des emprises de services WxS
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mapPath = $this->params->get("PRODIGE_PATH_DATA") . "cartes/Publication/";
        $conn = $this->container->get("doctrine")->getConnection(BaseController::CONNECTION_PRODIGE);
        $conn->exec('set search_path to public');

        $mapfileWMS = $mapPath . "wms.map";
        $this->setLayersExtent($mapfileWMS, $conn, "wms");

        $mapfileWFS = $mapPath . "wfs.map";
        $this->setLayersExtent($mapfileWFS, $conn, "wfs");

        $arrayFiles = scandir($mapPath);
        foreach ($arrayFiles as $file) {
            $extension = pathinfo($file, PATHINFO_EXTENSION);
            if ($extension == "map") {
                if (substr($file, 0, 4) == "wms_") {
                    $this->setLayersExtent($mapPath . $file, $conn, "wms");
                }
                if (substr($file, 0, 4) == "wfs_") {
                    $this->setLayersExtent($mapPath . $file, $conn, "wfs");
                }
            }
        }
        return Command::SUCCESS;
    }

    protected function setLayersExtent($mapfile, $conn, $type)
    {
        if (file_exists($mapfile)) {
            $oMap=false;
            try{
                $oMap = ms_newMapObj($mapfile);
            }
            catch(\Exception $e){
                echo("invalid mapfile ".$mapfile."\r");
            }
            
            if ($oMap) {
                for ($i = 0; $i < $oMap->numlayers; $i++) {
                    $oLayer = $oMap->getLayer($i);
                    $wms_extent = $oLayer->getMetadata($type . "_extent");
                    if (empty($wms_extent)) {
                        $matches = array();
                        if(preg_match("!from\s+(\w+\.)?\b(\w+)\b!is", $oLayer->data, $matches)){
                            $table = $matches[2];
                            if (!empty($table)) {
                                try{
                                    $extentDB = $conn->fetchAllAssociative("SELECT st_extent(the_geom) FROM public." . $table);
                                    $resultExtentDB = $extentDB[0]["st_extent"];
                                    $resultExtentDB = str_replace("BOX(", "", $resultExtentDB);
                                    $resultExtentDB = str_replace(")", "", $resultExtentDB);
                                    $extent = str_replace(",", " ", $resultExtentDB);
                                    $oLayer->setMetadata($type . "_extent", $extent);
                                }
                                catch(\Exception $e){
                                    echo("missing table ".$table." or missing column the_geom\r");
                                }
                            }
                        }
                        
                    }
                }
                $oMap->save($mapfile);
            }
        }
    }

}

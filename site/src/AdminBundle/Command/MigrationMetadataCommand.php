<?php

namespace ProdigeCatalogue\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Prodige\ProdigeBundle\Controller\BaseController;


use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MigrationMetadataCommand extends ContainerAwareCommand {

    /**
     * Configures the command definition
     */
    protected function configure() {
        $this->setName('prodige:migration_metadata_41')
            ->setDescription('Migration metadata from version 4.0 to 4.1')
           // ->addArgument('service_producteur', InputArgument::REQUIRED, 'identifiant de la rubrique service producteur de la plateforme')
        ;
    }

  
    
    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        
        $catalogue_conn = $this->getCatalogueConnection("public"); 
        
        $query = "select count(*) from public.metadata where istemplate = 'n' and isharvested ='n' "; 
        $nb_metadata = $catalogue_conn->fetchAssociative($query)["count"]; 
        echo "nombre de métadonnée: ".$nb_metadata."\n"; 
                
        if(!empty($nb_metadata) && $nb_metadata > 0 ) {
            
            
            $query = "select id, data, uuid from public.metadata where istemplate = 'n' and isharvested ='n' order by id desc"; 
            $meta = $catalogue_conn->fetchAllAssociative($query); 
            for($i=0; $i< $nb_metadata ; $i++) {
                if(isset($meta[$i])) {
                    $metadata_id = $meta[$i]["id"]; 
                    $metadata_uuid = $meta[$i]["uuid"]; 
                    $metadata_data = $meta[$i]["data"]; 
                    
                    echo $i.": traitement de la métadonnée ".$metadata_uuid; 
                    $host = $this->getContainer()->getParameter("PRO_GEONETWORK_URLBASE")."srv/"; 
                    $metadata_data = str_replace( "local://", $host,$metadata_data );  
                    $metadata_data = str_replace("amp;", "", $metadata_data); 
                    $metadata_data = str_replace( "&", "&amp;",$metadata_data );   
                            
                    $sql = "UPDATE public.metadata SET data =:data WHERE id = :id"; 
                    $catalogue_conn->executeStatement($sql, array("id"  => $metadata_id , "data"  => $metadata_data));       
                    
                    // mise à jour de l'index geonetwork
                    $this->rebuilt_index($metadata_uuid); 
                    echo ":OK\n";
                } else {
                    echo $i.":erreur \n"; 
                }

            }
            $output->writeln("\nMigration effectuée");
            
        }

    }
    
    // appel de l'url
    protected function doCurl($url, $verbose = false, $basicAuthentication = null, $params=array()) {

        $result = ""; 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        
        foreach($params as $key => $value){
            curl_setopt($ch, $key, $value);
        }
        
        if($basicAuthentication !== null) {
            curl_setopt($ch, CURLOPT_USERPWD, implode(":", $basicAuthentication)); #A username and password formatted as "[username]:[password]" to use for the connection.
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);                  
        }
        $out = curl_exec($ch);
        if($out === false) {
           $result = false;
        } else{
           $result = $out; 
        }
        curl_close($ch);        
        
        return $result; 
    }

    
    /* Indexation de la fiche de métadonnées
     * 
     * 
     */
    function rebuilt_index($metadata_uuid) {
        
        $geonetwork_url = $this->getContainer()->getParameter("PRO_GEONETWORK_URLBASE").'srv/api/0.1/records/index?uuids='.$metadata_uuid;
        $container = $this->getContainer();
        
        $user_login = $container->getParameter("phpcli_default_login"); 
        $user_pwd = $container->getParameter("phpcli_default_password"); 
        $basicAuth = array($user_login, $user_pwd); 
        
        $headers = array();
        $headers[] = "Accept: application/json";
        
        $this->doCurl($geonetwork_url, true, $basicAuth, array(CURLOPT_HTTPHEADER=>$headers));
        
    }  

    
    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Get Doctrine connection
     * 
     * @param string $connection_name        	
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public") {
        $conn = $this->getContainer()->get("doctrine")->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);

        return $conn;
    }

    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public") {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public") {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }

}

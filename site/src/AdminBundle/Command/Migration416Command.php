<?php

namespace ProdigeCatalogue\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Services\LdapUtils;
use ProdigeCatalogue\AdminBundle\Services\ThesaurusSerializer;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Migration416Command extends ContainerAwareCommand {

    /**
     * Configures the command definition
     */
    protected function configure() {
        $this->setName('prodige:migration_416')
            ->setDescription('Migration from version 4.1.5 to 4.1.6')
        ;
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $verbosity = $output->getVerbosity();
        $output->setDecorated(true);
        $helper = $this->getHelper('question');

        $container = $this->getContainer();
        
        if(!defined("PRO_USER_INTERNET_USR_ID")){
            define("PRO_USER_INTERNET_USR_ID", $container->getParameter('PRO_USER_INTERNET_USR_ID', 'vinternet'));
        }
        $request = new \Symfony\Component\HttpFoundation\Request();
        $container->get('request_stack')->push($request);
        \Prodige\ProdigeBundle\Services\CurlUtilities::initialize(
                $container->getParameter('phpcli_default_login'),
                $container->getParameter('phpcli_default_password'),
                $container->getParameter('server_ca_cert_path')
        );
        
        // call the prodige.configreader service
        $container->get('prodige.configreader');
        $conn = $CATALOGUE = $this->getCatalogueConnection('public,catalogue');
        
        $ressources = $CATALOGUE->fetchAllAssociative("select uuid, id, data from metadata where isharvested='n' and data like '%(service WFS)%' ");
        
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
           
        $version = "1.0";
        $encoding = "UTF-8";
        foreach($ressources as $ressource){
            $data = $ressource["data"];
            $id = $ressource["id"];
            $uuid = $ressource["uuid"];
            
            $metadata_doc = new \DOMDocument($version, $encoding);
            $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
            $metadata_data = $entete . $data;
            if ( @$metadata_doc->loadXML($metadata_data) !== false ) {
                
                $this->assignIdentifier($metadata_doc, $uuid);
                $new_metadata_data = $metadata_doc->saveXML();
                $new_metadata_data = str_replace($entete, "", $new_metadata_data);
    
                // Prodige4
                $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
                $urlUpdateData = "md.edit.save";
                $formData = array (
                        "id" => $id,
                        "data" => $new_metadata_data 
                );
                // send to gn
                $geonetwork->post($urlUpdateData, $formData);
                $output->writeln("<comment>Migration métadonnée ".$id."</comment>");
        
                
            }
        }
        
                
    }
    /**
     * Assignation d'un identifiant unique à la ressource en fonction de la donnee source
     * @param $doc
     * @param $uuid identifiant uuid
     * @return unknown_type
     */
    protected function assignIdentifier(&$doc, $uuid){
        //global $PRO_GEONETWORK_DIRECTORY;
        $xpath = new \DOMXpath($doc);
        //serach the data
        $identifier = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation/gmd:identifier/gmd:MD_Identifier/gmd:code/gco:CharacterString");

        if($identifier && $identifier->length > 0) {
            $identifier->item(0)->nodeValue = PRO_GEONETWORK_URLBASE."srv/".$uuid;
        } else {
            $CI_Citation = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation/gmd:citedResponsibleParty");
            if($CI_Citation->length>0){
                $new_identifier  = $doc->createElement('gmd:identifier');
                $gmdMD_Identifier  = $doc->createElement('gmd:MD_Identifier');
                $gmdcode  = $doc->createElement('gmd:code');
                $gcoCharacterString  = $doc->createElement('gco:CharacterString');
                $CI_Citation->item(0)->parentNode->insertBefore($new_identifier, $CI_Citation->item(0));
                $new_identifier->appendChild($gmdMD_Identifier);
                $gmdMD_Identifier->appendChild($gmdcode);
                $gmdcode->appendChild($gcoCharacterString);
                $gcoCharacterString->nodeValue = PRO_GEONETWORK_URLBASE."srv/".$uuid;
                ///
            } else {
                //le tag citedResponsibleParty n'existe pas
                $CI_Citation = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation");
                if($CI_Citation->length>0){
                    $new_identifier  = $doc->createElement('gmd:identifier');
                    $gmdMD_Identifier  = $doc->createElement('gmd:MD_Identifier');
                    $gmdcode  = $doc->createElement('gmd:code');
                    $gcoCharacterString  = $doc->createElement('gco:CharacterString');
                    $CI_Citation->item(0)->appendChild($new_identifier);
                    $new_identifier->appendChild($gmdMD_Identifier);
                    $gmdMD_Identifier->appendChild($gmdcode);
                    $gmdcode->appendChild($gcoCharacterString);
                    $gcoCharacterString->nodeValue = PRO_GEONETWORK_URLBASE."srv/".$uuid;
                }
            }
        }
    }
    
    
    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Get Doctrine connection
     * 
     * @param string $connection_name        	
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public") {
        $conn = $this->getContainer()->get("doctrine")->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);

        return $conn;
    }

    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public") {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public") {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }

}

<?php
namespace ProdigeCatalogue\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\Console\Output\OutputInterface;


use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Prodige\ProdigeBundle\Services\LdapUtils;
use Prodige\ProdigeBundle\Common\AlkRequest;

/**
 * Cette commande s'occupe de corriger des liens de métadonnées mal positionnés (réindexation nécessaire)
 */
class Prodige405Command extends ContainerAwareCommand
{

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this->setName('prodige:405')->setDescription('Migration 4.0.4 > 4.0.5');
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // call the prodige.configreader service
        $this->getContainer()->get('prodige.configreader');
        // init config
        $conn = $this->getCatalogueConnection('public');
        
        $query = 'select id, data from metadata where data like \'%gmd:transferOptions%\' and isharvested=\'n\'';
        
        $metadatas = $conn->fetchAllAssociative($query);
        
        foreach ($metadatas as $metadata) {
            $metadata_id = $metadata["id"];
            $data = $metadata["data"];
            $newData = $this->updateData($data);
            if($newData != "") {
                $strUpdate = "update metadata set data=:data where id=:id";
                $conn->executeQuery($strUpdate, array(
                    "data" => $newData,
                    "id" => $metadata_id
                ));
                
            }
            
        }
    }
    /**
     * @abstract met à jour les liens
     * @param data                contenu XML de la métadonnée
     * @return contenu XML de la métadonnée mis à jour, chaîne vide si une erreur est survenue
     */
    protected function updateData($data) {
    
        $newData = "";
        $version  = "1.0";
        $encoding = "UTF-8";
        $entete   = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
    
        $data = str_replace("&", "&amp;", $data);
        $data = $entete.$data;
    
        $doc = new \DOMDocument($version, $encoding);
    
        if(@$doc->loadXML($data) ) {
            
            $xpath = new \DOMXpath($doc);
            $transfertOption = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions");
            if($transfertOption->length > 1) {
                $i=0;
                foreach($transfertOption as $item){
                    //get All onlines and put it in first, then delete all others
                    if($i>0){
                        $onlines = $item->getElementsByTagName("onLine");
                        foreach($onlines as $online){
                            //add all in first
                            $newNodeDesc = $doc->importNode($online, true);
                            $transfertOption->item(0)->appendChild($newNodeDesc);
                        }
                    }
                    $i++;
                }
                
                $transfertOption = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions");
                $i=0;
                foreach($transfertOption as $item){
                    //get All onlines and put it in first, then delete all others
                    if($i>0){
                        //then remove
                        $item->parentNode->removeChild($item);
                    }
                    $i++;
                }
            }
             
        }
        $newData = $doc->saveXML();
        $newData = str_replace($entete, "", $newData);
        return $newData;
    }

    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    /**
     * Get Doctrine connection
     *
     * @param string $connection_name            
     * @param string $schema            
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public")
    {
        $conn = $this->getContainer()
            ->get("doctrine")
            ->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);
        
        return $conn;
    }

    /**
     *
     * @param string $schema            
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema            
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }
}
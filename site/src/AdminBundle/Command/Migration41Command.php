<?php

namespace ProdigeCatalogue\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;


class Migration41Command extends ContainerAwareCommand {

    /**
     * Configures the command definition
     */
    protected function configure() {
        $this->setName('prodige:migration_41')
            ->setDescription('Migration from version 4.0 to 4.1')
           // ->addArgument('service_producteur', InputArgument::REQUIRED, 'identifiant de la rubrique service producteur de la plateforme')
        ;
    }

  
    
    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $verbosity = $output->getVerbosity();
        $output->setDecorated(true);
        $helper = $this->getHelper('question');

        $container = $this->getContainer();
        if(!defined("PRO_USER_INTERNET_USR_ID")){
            define("PRO_USER_INTERNET_USR_ID", $container->getParameter('PRO_USER_INTERNET_USR_ID', 'vinternet'));
        }
        $request = new \Symfony\Component\HttpFoundation\Request();
        $container->get('request_stack')->push($request);
        \Prodige\ProdigeBundle\Services\CurlUtilities::initialize(
                $container->getParameter('phpcli_default_login'),
                $container->getParameter('phpcli_default_password'),
                $container->getParameter('server_ca_cert_path')
        );
        // call the prodige.configreader service
        $container->get('prodige.configreader');
        $conn = $CATALOGUE = $this->getCatalogueConnection('public,catalogue');
        
        $settings = $CATALOGUE->fetchAllAssociative("select prodige_settings_constant, prodige_settings_value from catalogue.prodige_settings");
        foreach($settings as $constant){
            defined($constant["prodige_settings_constant"]) || define($constant["prodige_settings_constant"], $constant["prodige_settings_value"]);
        }
        
        ///////////////////////////////
        $output->writeln("<info>Migration vers la version Prodige 4.1...</info>\n");
        
        $this->updateServicesUrl();
        
        $modelChart = $this->getContainer()->getParameter("PRODIGE_PATH_CATALOGUE")."/src/ProdigeCatalogue/AdminBundle/Resources/models/chart.xml";
        $this->createModel($modelChart);
        $modelNonGeo = $this->getContainer()->getParameter("PRODIGE_PATH_CATALOGUE")."/src/ProdigeCatalogue/AdminBundle/Resources/models/nongeodataset.xml";
        $modelNonGeoId = $this->createModel($modelNonGeo);
        
        $sql = "INSERT INTO  catalogue.prodige_settings (prodige_settings_constant, prodige_settings_title, prodige_settings_value, prodige_settings_desc, prodige_settings_type, prodige_settings_param) VALUES ( 'PRO_NONGEO_METADATA_ID', NULL, :metadata_id, 'Identifiant de la métadonnée de série de données non géographiques modèle', 'textfield', NULL);";
        $conn->executeStatement($sql, array("metadata_id"=>$modelNonGeoId));
        
        ///////////////////////////////
        
        /** STEP 1 : SERVICES WFS AFFECTES AU NIVEAU METADONNEES */
        $output->writeln("<comment>* SERVICES WFS AFFECTES AU NIVEAU METADONNEES (regroupement de toutes les couches WFS dans un seul service WFS par métadonnées)</comment>");
        $request = new \Symfony\Component\HttpFoundation\Request();
        $WxsController = new \ProdigeCatalogue\GeosourceBundle\Controller\LayerAddToWebServiceController();
        $WxsController->setContainer($container);
        
        $namespaces = "ARRAY[ARRAY['gmd', 'http://www.isotc211.org/2005/gmd'], ARRAY['gco','http://www.isotc211.org/2005/gco']]"; 
        $sql_metadata_title = " array_to_string(xpath('/gmd:MD_Metadata/gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString/text()'::text, metadata.data::xml, {$namespaces}), ' ')";
        $sql_metadata_title = " (case when xml_is_well_formed(data::text) then ".$sql_metadata_title." else null::text end) as metadata_title";
        
        $resources = $CATALOGUE->executeQuery(
            "select distinct couche_sdom.uuid, couche_sdom.fmeta_id, couche_donnees.*, ".$sql_metadata_title." "
            . " from couche_sdom"
            . " inner join couche_donnees using (pk_couche_donnees)"
            . " inner join public.metadata on (couche_sdom.fmeta_id=metadata.id::text)"
            . " where couche_donnees.couchd_wfs=1 "
            . " and nullif(couche_donnees.couchd_wfs_uuid, '') is not null"
        ); 
        $resources = $resources->fetchAllAssociative(\PDO::FETCH_GROUP+\PDO::FETCH_ASSOC);
        
        $temp = array();
        // indexation des layers par leur ID
        foreach($resources as $uuid=>$couches_donnees){
            foreach($couches_donnees as $couche_donnees){
                $temp[$uuid][$couche_donnees["pk_couche_donnees"]] = $couche_donnees;
            }
        }
        $resources = $temp;
        
        foreach($resources as $uuid=>$layers){
            if ( count($layers)<2 ) continue;
            $metadata_title = $uuid;
            $wfs_couches = $WxsController->wfsToDatasetAction($uuid, true);
            
            $temp = array();
            $couches_noms = array();
            foreach ($wfs_couches as $pk_couche_donnees=>$wfs_couche){
                // que si la couche de la base existe (de mauvaises valeurs peuvent exister dans les fichiers WFS)
                if ( !isset($layers[$pk_couche_donnees]) ) continue;
                // que si la couche WFS existe (toutes les couches de la base ont une entrée dans la lecture du mapfile mais ne sont pas forcément en WFS)
                if ( !isset($wfs_couche["wxs_title"]) ) continue;
                // cumule des infos de la base et des infos WFS
                $temp[$pk_couche_donnees] = array_merge($resources[$uuid][$pk_couche_donnees], $wfs_couche);
                
                $metadata_title = $temp[$pk_couche_donnees]["metadata_title"] ?: $metadata_title;
                $temp[$pk_couche_donnees]["metadata_title"] = $metadata_title;
                $couches_noms[] = array(
                    "Emplacement " => " ".$temp[$pk_couche_donnees]["couchd_emplacement_stockage"],
                    "Couche " => " ".$temp[$pk_couche_donnees]["couchd_nom"], 
                    "Titre WFS " => " ".$temp[$pk_couche_donnees]["wxs_title"], 
                );
            }
            $wfs_couches = $temp;
            
            $output->write("Migration de la métadonnée {$uuid} \"".$metadata_title."\" : ");
            
            $request->request->replace(array());
            $request->request->set("uuid", $uuid);
            $request->request->set("metadata_title", $metadata_title);
            $request->request->set("wfs_couches", $wfs_couches);
            
            
            $response = $WxsController->datasetToWFSAction($request);
            
            
            $response = @json_decode($response->getContent(), true) ?? array("wfs_couches"=>null);
            if ( !empty($response["wfs_couches"]) ){
                $couchd_wfs_uuid = current($response["wfs_couches"]);
                $couchd_wfs_uuid = $couchd_wfs_uuid["couchd_wfs_uuid"];
                $output->writeln("<info>SUCCES</info>\nToutes les couches WFS sont inscrites dans la métadonnée de service WFS : <info>".$couchd_wfs_uuid."</info>");
                $output->writeln(" * ".implode("\n * ", array_map(function($v){return urldecode(http_build_query($v, null, ", "));}, $couches_noms))."");
            } else {
                $output->writeln("<error>ERREUR : échec de la migration</error>");
            }
            $output->writeln("");
        }
        return;
    }
    
    /**
     * 
     * @param type $modelPathCreate Xml model in Geonetwork
     * @param string path to xml model
     */
    protected function createModel($modelPath){
        
        $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
        $params = array("_csrf"=> "",
                        "metadataType" => "TEMPLATE",
                        "uuidProcessing" => "GENERATEUUID",
                        "transformWith" => "",
                        "publishToAll" => "on",
                        "category" => "",
                        "group" => "");
        $url = 'api/records?'.http_build_query($params);
        
        $data = file_get_contents($modelPath);
        
        $jsonResp = $geonetwork->put($url, $data, false, false, true);
        $jsonObj = json_decode($jsonResp);
        if($jsonObj &&  empty($jsonObj->errors) && $jsonObj->metadataInfos){
            foreach($jsonObj->metadataInfos as $key => $infos){
                return $key;
            }
        }
        return false;
        
    }
    /**
     * Migration des URL de services WxS
     */
    protected function updateServicesUrl(){
        
        $mapfile = PRO_MAPFILE_PATH."wfs.map";
        $oMap = ms_newMapObj($mapfile);
        if ( $oMap ) {
            $oMap->setMetadata("wfs_onlineresource", $this->getContainer()->getParameter("PRODIGE_URL_DATACARTO"). "/wfs");
            $oMap->save($mapfile);
        }
        $mapfile = PRO_MAPFILE_PATH."wms.map";
        $oMap = ms_newMapObj($mapfile);
        if ( $oMap ) {
            $oMap->setMetadata("wms_onlineresource", $this->getContainer()->getParameter("PRODIGE_URL_DATACARTO"). "/wms");
            $oMap->save($mapfile);
        }
    }
    
    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Get Doctrine connection
     * 
     * @param string $connection_name        	
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public") {
        $conn = $this->getContainer()->get("doctrine")->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);

        return $conn;
    }

    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public") {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public") {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }

}

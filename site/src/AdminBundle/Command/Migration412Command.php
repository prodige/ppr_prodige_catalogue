<?php

namespace ProdigeCatalogue\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Services\LdapUtils;
use ProdigeCatalogue\AdminBundle\Services\ThesaurusSerializer;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Migration412Command extends ContainerAwareCommand {

    /**
     * Configures the command definition
     */
    protected function configure() {
        $this->setName('prodige:migration_412')
            ->setDescription('Migration from version 4.1 to 4.2')
        ;
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $verbosity = $output->getVerbosity();
        $output->setDecorated(true);
        $helper = $this->getHelper('question');

        $container = $this->getContainer();
         $container = $this->getContainer();
        if(!defined("PRO_USER_INTERNET_USR_ID")){
            define("PRO_USER_INTERNET_USR_ID", $container->getParameter('PRO_USER_INTERNET_USR_ID', 'vinternet'));
        }
        $request = new \Symfony\Component\HttpFoundation\Request();
        $container->get('request_stack')->push($request);
        \Prodige\ProdigeBundle\Services\CurlUtilities::initialize(
                $container->getParameter('phpcli_default_login'),
                $container->getParameter('phpcli_default_password'),
                $container->getParameter('server_ca_cert_path')
        );
        
        // call the prodige.configreader service
        $container->get('prodige.configreader');
        $conn = $CATALOGUE = $this->getCatalogueConnection('public,catalogue');
        
        $settings = $CATALOGUE->fetchAllAssociative("select prodige_settings_constant, prodige_settings_value from catalogue.prodige_settings");
        foreach($settings as $constant){
            defined($constant["prodige_settings_constant"]) || define($constant["prodige_settings_constant"], $constant["prodige_settings_value"]);
        }
        $version = "1.0";
        $encoding = "UTF-8";
        
        $data = $CATALOGUE->fetchOne("select data from metadata where id=:id", array("id"=>PRO_WMS_METADATA_ID));
        $metadata_doc = new \DOMDocument($version, $encoding);
        $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
        $metadata_data = $entete . $data;
        if ( @$metadata_doc->loadXML($metadata_data) !== false ) {
            $xpath = new \DOMXPath($metadata_doc);
            $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine/" . "gmd:CI_OnlineResource/gmd:linkage/gmd:URL");
            if ( $objXPath->length > 0 ) {
                $objXPath->item(0 )->nodeValue = $this->getContainer()->getParameter("PRODIGE_URL_DATACARTO"). "/wms?request=GetCapabilities" ;
            }
            $new_metadata_data = $metadata_doc->saveXML();
            $new_metadata_data = str_replace($entete, "", $new_metadata_data);
            $new_metadata_data = str_replace($this->getContainer()->getParameter("PRODIGE_URL_FRONTCARTO")."/cgi-bin/mapserv?", $this->getContainer()->getParameter("PRODIGE_URL_DATACARTO"). "/wms?", $new_metadata_data);

            // Prodige4
            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
            $urlUpdateData = "md.edit.save";
            $formData = array (
                    "id" => PRO_WMS_METADATA_ID,
                    "data" => $new_metadata_data 
            );
            // send to geosource
            $geonetwork->post($urlUpdateData, $formData);
            
        }
        
        $data = $CATALOGUE->fetchOne("select data from metadata where id=:id", array("id"=>PRO_WFS_METADATA_ID));
        $metadata_doc = new \DOMDocument($version, $encoding);
        $entete = "<?xml version=\"" . $version . "\" encoding=\"" . $encoding . "\"?>\n";
        $metadata_data = $entete . $data;
        if ( @$metadata_doc->loadXML($metadata_data) !== false ) {
            $xpath = new \DOMXPath($metadata_doc);
            $objXPath = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine/" . "gmd:CI_OnlineResource/gmd:linkage/gmd:URL");
            if ( $objXPath->length > 0 ) {
                $objXPath->item(0 )->nodeValue = $this->getContainer()->getParameter("PRODIGE_URL_DATACARTO"). "/wfs?request=GetCapabilities" ;
            }
            $new_metadata_data = $metadata_doc->saveXML();
            $new_metadata_data = str_replace($entete, "", $new_metadata_data);
            $new_metadata_data = str_replace($this->getContainer()->getParameter("PRODIGE_URL_FRONTCARTO")."/cgi-bin/mapservwfs?", $this->getContainer()->getParameter("PRODIGE_URL_DATACARTO"). "/wfs?", $new_metadata_data);
            
            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
            $urlUpdateData = "md.edit.save";
            $formData = array (
                    "id" => PRO_WFS_METADATA_ID,
                    "data" => $new_metadata_data 
            );
            // send to geosource
            $geonetwork->post($urlUpdateData, $formData);
            
        }
        
    }
    
    
    
    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Get Doctrine connection
     * 
     * @param string $connection_name        	
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public") {
        $conn = $this->getContainer()->get("doctrine")->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);

        return $conn;
    }

    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public") {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public") {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }

}

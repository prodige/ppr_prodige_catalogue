<?php

namespace ProdigeCatalogue\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Services\LdapUtils;
use ProdigeCatalogue\AdminBundle\Services\ThesaurusSerializer;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Migration414Command extends ContainerAwareCommand {

    /**
     * Configures the command definition
     */
    protected function configure() {
        $this->setName('prodige:migration_414')
            ->setDescription('Migration from version 4.3 to 4.1.4')
        ;
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $verbosity = $output->getVerbosity();
        $output->setDecorated(true);
        $helper = $this->getHelper('question');

        $container = $this->getContainer();
        
        if(!defined("PRO_USER_INTERNET_USR_ID")){
            define("PRO_USER_INTERNET_USR_ID", $container->getParameter('PRO_USER_INTERNET_USR_ID', 'vinternet'));
        }
        $request = new \Symfony\Component\HttpFoundation\Request();
        $container->get('request_stack')->push($request);
        \Prodige\ProdigeBundle\Services\CurlUtilities::initialize(
                $container->getParameter('phpcli_default_login'),
                $container->getParameter('phpcli_default_password'),
                $container->getParameter('server_ca_cert_path')
        );
        
        // call the prodige.configreader service
        $container->get('prodige.configreader');
        $conn = $CATALOGUE = $this->getCatalogueConnection('public,catalogue');
        
        $sql = "update metadata set data = replace(data, 'local://fre/xml.keyword.get?', :replace) where  isharvested='n';";
        $conn->executeStatement($sql, array("replace"=> $container->getParameter('PRO_GEONETWORK_URLBASE', ''). "srv/fre/xml.keyword.get?"));
                
    }
    
    
    
    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager() {
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Get Doctrine connection
     * 
     * @param string $connection_name        	
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public") {
        $conn = $this->getContainer()->get("doctrine")->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);

        return $conn;
    }

    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public") {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema        	
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public") {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }

}

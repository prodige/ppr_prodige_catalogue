<?php

namespace ProdigeCatalogue\AdminBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Question\ConfirmationQuestion;
use Prodige\ProdigeBundle\Controller\BaseController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelInterface;

class DispatchWMSCommand extends Command
{
    protected static $defaultName = 'prodige:dispatch_wms';

    /** @var ParameterBagInterface */
    protected $params;

    /** @var RequestStack */
    protected $requestStack;

    /** @var KernelInterface */
    protected $kernel;

    protected $container;

    public function __construct(
        ParameterBagInterface $params,
        RequestStack $stack,
        KernelInterface $kernel,
        ContainerInterface $container
    ) {
        parent::__construct();
        $this->params = $params;
        $this->requestStack = $stack;
        $this->kernel = $kernel;
        $this->container = $container;
    }

    /**
     * Configures the command definition
     */
    protected function configure()
    {
        $this->setDescription(
            'Generation of subdomain WMS services and distribution of WMS resources in these services'
        )
            ->addOption(
                'choose',
                "c",
                InputOption::VALUE_NONE,
                'Propose la liste de tous les sous-domaines avec possibilité de choisir ceux qui seront transformés'
            )
            ->addArgument(
                'sous_domaines',
                InputArgument::IS_ARRAY,
                'Identifiants (textuels) des sous-domaines associés et récepteurs de services WMS'
            );
    }

    /**
     * Executes the command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $verbosity = $output->getVerbosity();
        $output->setDecorated(true);
        $helper = $this->getHelper('question');

        //$container = $this->getContainer();
        if (!defined("PRO_USER_INTERNET_USR_ID")) {
            define("PRO_USER_INTERNET_USR_ID", $this->params->get('PRO_USER_INTERNET_USR_ID', 'vinternet'));
        }
        $request = new \Symfony\Component\HttpFoundation\Request();
        $this->requestStack->push($request);
        dump("1");
        \Prodige\ProdigeBundle\Services\CurlUtilities::initialize(
            $this->params->get('phpcli_default_login'),
            $this->params->get('phpcli_default_password'),
            $$this->params->get('server_ca_cert_path')
        );
        // call the prodige.configreader service
        $this->params->get('prodige.configreader');
        $conn = $CATALOGUE = $this->getCatalogueConnection('public,catalogue');

        $settings = $CATALOGUE->fetchAllAssociative(
            "select prodige_settings_constant, prodige_settings_value from catalogue.prodige_settings"
        );
        foreach ($settings as $constant) {
            defined($constant["prodige_settings_constant"]) || define(
                $constant["prodige_settings_constant"],
                $constant["prodige_settings_value"]
            );
        }
        ///////////////////////////////
        $sous_domaines = array();
        $choose = $input->getOption("choose");
        if ($choose) {
            $input->setInteractive(true);
            $domaines = $CATALOGUE->fetchAllAssociative(
                "select distinct ssdom_id, dom_id, pk_sous_domaine, ssdom_service_wms"
                . ", ('\"'||couche_sdom.dom_nom||'\"') || ' / ' ||  ('\"'||couche_sdom.ssdom_nom||'\"') as domsdom"
                . ", sum(coalesce(couche_donnees.couchd_wms,0)) over(partition by pk_sous_domaine order by dom_id, ssdom_id) as nb_couches"
                . ", array_agg(couche_sdom.couchd_emplacement_stockage) over(partition by pk_sous_domaine order by dom_id, ssdom_id) as couches"
                . " from sous_domaine"
                . " inner join couche_sdom using (pk_sous_domaine)"
                . " inner join domaine on (domaine.pk_domaine=ssdom_fk_domaine)"
                . " inner join couche_donnees using (pk_couche_donnees)"
                . " where couche_donnees.couchd_wms=1 and couchd_wms_sdom=ARRAY[-1]"
                . " order by dom_id, ssdom_id"
            );

            $output->writeln(
                "Sélectionnez les sous-domaines que vous souhaitez convertir en service WMS, en répondant 'y' ou 'n' (quitter les questions par 'q')"
            );
            $old = null;
            foreach ($domaines as $ssdom) {
                if ($ssdom["dom_id"] != $old) {
                    $old !== null && $output->writeln("");
                    $old = $ssdom["dom_id"];
                }
                $ssdom["domsdom"] = html_entity_decode($ssdom["domsdom"]);
                $status = ($ssdom["ssdom_service_wms"] == 1 ? "Migration partielle" : "Non migré");
                $question = new ConfirmationQuestion(
                    html_entity_decode($ssdom["ssdom_id"]) . str_repeat(
                        " ",
                        max(1, 40 - strlen(utf8_decode(html_entity_decode($ssdom["ssdom_id"]))))
                    ) .
                    $ssdom["domsdom"] . str_repeat(" ", max(1, 60 - strlen(utf8_decode($ssdom["domsdom"])))) .
                    str_repeat(" ", max(1, 22 - strlen(utf8_decode($status)))) . ($status) .
                    ($verbosity >= OutputInterface::VERBOSITY_VERBOSE ? "\n" . $ssdom["couches"] . "\n" : "") .
                    str_repeat(
                        " ",
                        max(1, 6 - strlen($ssdom["nb_couches"]))
                    ) . $ssdom["nb_couches"] . " couche(s) WMS : ",
                    false,
                    '/(^y|^q|^1$)/i'
                );
                $normalizer = $question->getNormalizer();

                $question->setNormalizer(function ($answer) use ($normalizer) {
                    if (strtolower($answer) == "q") {
                        return Command::FAILURE;
                    }

                    return $normalizer($answer);
                });

                //            $question->setValidator(function ($answer) {
                //                if ($answer=="q") return $answer;
                //            });

                if ($answer = $helper->ask($input, $output, $question)) {
                    if ($answer === "quit") {
                        break;
                    }
                    $sous_domaines[] = $ssdom["ssdom_id"];
                }
            }
            $output->writeln(str_repeat("-", 100));
            $input->setInteractive(false);
            sleep(1);
        }


        ///////////////////////////////

        $SubDomainesController = new \ProdigeCatalogue\AdminBundle\Controller\SubDomainesController();
        //$SubDomainesController->setContainer($container);
        $SubsDomainsServiceWMS = $this->kernel->locateResource(
            '@AdminBundle/Resources/templates/Administration/SubDomains/SubsDomainsServiceWMS.php'
        );


        $request = new \Symfony\Component\HttpFoundation\Request();
        $WxsController = new \ProdigeCatalogue\GeosourceBundle\Controller\LayerAddToWebServiceController();
        //$WxsController->setContainer($container);

        $sous_domaines = array_unique(array_merge($sous_domaines, $input->getArgument('sous_domaines')));
        $sous_domaines = array_unique(
            array_merge(
                $sous_domaines,
                array_map("html_entity_decode", $sous_domaines),
                array_map("htmlentities", $sous_domaines),
                array_map("utf8_encode", $sous_domaines),
                array_map("utf8_decode", $sous_domaines)
            )
        );
        if (empty($sous_domaines)) {
            $output->writeln("\nAucun sous domaine choisi...\n<question>FIN D'EXECUTION</question>");
            return Command::FAILURE;
        } else {
//            $l = 100;
//            $t = array_merge(array(str_repeat("-", $l), "Sous domaines à migrer : ", str_repeat("-", $l)), $sous_domaines);
//            $t = array_map(function($s)use($l){return "<question>".html_entity_decode($s).str_repeat(" ", $l-strlen(utf8_decode(html_entity_decode($s))))."</question>";}, $t);
//            $output->writeln("\n".implode("\n", $t)."");
        }

        //Récupération de tous les sous-domaines qui existent parmis la liste demandée
        $valides = $CATALOGUE->fetchAllAssociative(
            "select ssdom_id, dom_id, pk_sous_domaine, ssdom_service_wms_uuid"
            . " from sous_domaine"
            . " inner join domaine on (pk_domaine=ssdom_fk_domaine)"
            . " where ssdom_id in (:sous_domaines)",
            compact("sous_domaines"),
            array("sous_domaines" => \Doctrine\DBAL\Connection::PARAM_STR_ARRAY)
        );
        // Récupération de la liste des metadonnées, et leurs couches publiées, pour lesquelles il existe au moins une couche publiée en WMS
        // dans le service WMS Global
        $resources = $CATALOGUE->executeQuery(
            "select distinct couche_sdom.uuid, couche_sdom.fmeta_id, couche_donnees.*, sous_domaine.pk_sous_domaine, sous_domaine.ssdom_id "
            . " from sous_domaine"
            . " inner join couche_sdom using (pk_sous_domaine)"
            . " inner join couche_donnees using (pk_couche_donnees)"
            . " where ssdom_id in (:sous_domaines)"
            . " and couche_donnees.couchd_wms=1 "
            . " and couche_donnees.couchd_wms_sdom @> ARRAY[-1]"
            , compact("sous_domaines")
            , array("sous_domaines" => \Doctrine\DBAL\Connection::PARAM_STR_ARRAY)
        );
        $resources = $resources->fetchAllAssociative(\PDO::FETCH_GROUP + \PDO::FETCH_ASSOC);
        $success_transferts = array();
        $domainesByMeta = array();
        $metaByDomaines = array();

        $temp = array();
        // métadonnées par domaines
        foreach ($resources as $uuid => $couches_donnees) {
            foreach ($couches_donnees as $couche_donnees) {
                $pk_sous_domaine = $couche_donnees["pk_sous_domaine"];
                $metaByDomaines[$pk_sous_domaine][$uuid] = $uuid;
                $domainesByMeta[$uuid][] = $pk_sous_domaine;
                $temp[$uuid][$couche_donnees["pk_couche_donnees"]] = $couche_donnees;
            }
        }
        $resources = $temp;
        //var_dump($sous_domaines, $resources, $domainesByMeta, $metaByDomaines);

        $sous_domaines = array_combine($sous_domaines, $sous_domaines);
        foreach ($valides as $sous_domaine) {
            $ssdom_id = $sous_domaine["ssdom_id"];
            $dom_id = $sous_domaine["dom_id"];
            $pk_sous_domaine = $sous_domaine["pk_sous_domaine"];
            $ssdom_fmeta_uuid = $sous_domaine["ssdom_service_wms_uuid"];

            unset($sous_domaines[$ssdom_id]);

            $output->writeln("");
            $output->writeln(
                "\n<info>" . str_repeat(
                    "-",
                    100
                ) . "\nTRAITEMENT DU SOUS-DOMAINE : " . $dom_id . "/" . $ssdom_id . "\n" . str_repeat(
                    "-",
                    100
                ) . "</info>"
            );

            if (!$ssdom_fmeta_uuid) {
                /** STEP 1 : GENERATION DU SERVICE WMS ASSOCIE AU SOUS-DOMAINE */
                $output->write("<comment>* GENERATION DU SERVICE WMS ASSOCIE AU SOUS-DOMAINE : </comment>");
                //variables "globales" pour l'inclusion du script de création du service WMS
                $domaine_pk = $pk_sous_domaine;
                $domaine_id = $ssdom_id;
                $has_service_wms = 1;
                $controller = $SubDomainesController;
                try {
                    include($SubsDomainsServiceWMS);
                    $CATALOGUE->executeQuery(
                        "update sous_domaine set ssdom_service_wms=1 where pk_sous_domaine=:pk_sous_domaine",
                        $sous_domaine
                    );
                } catch (\Exception $exception) {
                    $output->writeln("<error> ERREUR </error>");
                    $output->writeln("<error> " . $exception->getMessage() . " </error>");
                }
                if (is_null($ssdom_fmeta_uuid)) {
                    $output->writeln("<error> ECHEC </error>");
                    continue;
                }
                $output->writeln("<info> SUCCES </info>");
            }
            /** STEP 2 : TRANSFERT DES DONNEES PUBLIEES EN WMS GLOBAL VERS LE SERVICE WMS DU SOUS-DOMAINE */
            $metadonnees = $metaByDomaines[$pk_sous_domaine] ?? array();

            $output->writeln(
                "<comment>* TRANSFERT DES DONNEES PUBLIEES EN WMS GLOBAL VERS LE SERVICE WMS DU SOUS-DOMAINE : </comment>"
            );
            if (empty($metadonnees)) {
                $output->writeln("<question>SUCCES : aucune métadonnée à transférer </question>");
                continue;
            }

            // pour chaque métadonnées du domaine
            foreach ($metadonnees as $uuid) {
                // lecture des mapfiles WMS pour la métadonnées
                $wms_config = $WxsController->wmsToDatasetAction($uuid, true);

                $wms_on_global = $wms_config["wms_on_global"];
                //ici toutes les couches de la métadonnée
                $wms_couches = $wms_config["wms_couches"];


                // des couches de la metadonnée existent bien dans le WMS global
                if ($wms_on_global) {
                    // pour chaque couche inscrite dans les mapfiles WMS :
                    // - vérifie qu'elle est bien à la fois WMS et dans la base
                    // - cumule les infos de base aux infos WMS
                    $temp = array();
                    $couches_noms = array();
                    foreach ($wms_couches as $pk_couche_donnees => $wms_couche) {
                        // que si la couche de la base existe (de mauvaises valeurs peuvent exister dans les fichiers WMS)
                        if (!isset($resources[$uuid][$pk_couche_donnees])) {
                            continue;
                        }
                        // que si la couche WMS existe (toutes les couches de la base ont une entrée dans la lecture du mapfile mais ne sont pas forcément en WMS)
                        if (!isset($wms_couche["wxs_title"])) {
                            continue;
                        }
                        // cumule des infos de la base et des infos WMS
                        $temp[$pk_couche_donnees] = array_merge($resources[$uuid][$pk_couche_donnees], $wms_couche);
                        $couches_noms[] = array(
                            "Emplacement " => " " . $temp[$pk_couche_donnees]["couchd_emplacement_stockage"],
                            "Couche " => " " . $temp[$pk_couche_donnees]["couchd_nom"],
                            "Titre WMS " => " " . $temp[$pk_couche_donnees]["wxs_title"],
                        );
                    }
                    $wms_couches = $temp;

                    // transfert des données du WMS global au WMS de sousdomaine
                    $response = new \Symfony\Component\HttpFoundation\JsonResponse(array("wms_domaines" => null));
                    if (!empty($wms_couches)) {
                        $request->request->replace(array());
                        $request->request->set("uuid", $uuid);
                        $request->request->set("wms_domaines", array($pk_sous_domaine));
                        $request->request->set("wms_couches", $wms_couches);
                        $response = $WxsController->datasetToWMSAction($request);
                    }

                    $response = @json_decode($response->getContent(), true) ?? array("wms_domaines" => null);
                    if (!empty($response["wms_domaines"])) {
                        $output->writeln("<info>SUCCES : migration de la métadonnée d'UUID {$uuid}</info>");
                        $output->writeln(
                            " * " . implode(
                                "\n * ",
                                array_map(function ($v) {
                                    return urldecode(http_build_query($v, null, ", "));
                                }, $couches_noms)
                            ) . ""
                        );
                    } else {
                        $output->writeln(
                            "<error>ERREUR : pas de migration de la métadonnée d'UUID {$uuid} (aucune couche compatible)</error>"
                        );
                    }
                }//process de transfert

                // la métadonnée n'est pas publiée dans le service WMS global => pas à migrer
                else {
                    $wms_couches = array();
                    $output->writeln(
                        "<error>ERREUR : La métadonnée d'UUID {$uuid} ne contient pas de couches dans le WMS global</error>"
                    );
                }


                // pour toutes les couches marquées WMS de la méta, 
                // si elle ne fait pas partie de l'ensemble conservé (base+WMS) 
                // c'est que le paramétrage de la couche en BD est erroné
                foreach ($resources[$uuid] as $pk_couche_donnees => $couche) {
                    if (isset($wms_couches[$pk_couche_donnees])) {
                        continue;
                    }
                    $CATALOGUE->executeQuery(
                        "update couche_donnees set couchd_wms=0, couchd_wms_sdom=null where pk_couche_donnees=:pk_couche_donnees",
                        array(
                            "pk_couche_donnees" => $pk_couche_donnees,
                        )
                    );
                }

                // La metadonnée est transférée pour ce sous domaine. 
                // Le travail n'est plus à faire pour les autres ssdom de la méta
                foreach ($domainesByMeta[$uuid] as $pkssdom) {
                    unset($metaByDomaines[$pkssdom][$uuid]);
                }
            }
        }

//        // sous-domaines par métadonnées
//        foreach($metaByDomaines as $pk_sous_domaine=>$metadata){
//            foreach($metadata as $uuid) $domainesByMeta[$uuid][] = $pk_sous_domaine;
//        }
//        
//        
//        /** STEP 2 : TRANSFERT DES DONNEES PUBLIEES EN WMS GLOBAL VERS LE SERVICE WMS DU SOUS-DOMAINE */ 
//        foreach($domainesByMeta as $uuid=>$wms_domaines){
//            // lecture des mapfiles WMS pour la métadonnées
//            $wms_couches = $controller->wmsToDatasetAction($uuid, true);
//            $wms_on_global = $wms_couches["wms_on_global"];
//            // la métadonnée n'est pas publiée dans le service WMS global => pas à migrer
//            if ( !$wms_on_global ) continue;
//            
//            $wms_couches = $wms_couches["wms_couches"];
//            
//            $temp = array();
//            $keep_domaines = array();
//            // pour chaque couche inscrite dans les mapfiles WMS
//            foreach ($wms_couches as $pk_couche_donnees=>$wms_couche){
//                // la couche de la base existe (de mauvaises valeurs peuvent exister dans les fichiers WMS)
//                if ( !isset($resources[$uuid][$pk_couche_donnees]) ) continue;
//                // la couche WMS existe (toutes les couches de la base ont une entrée dans la lecture du mapfile mais ne sont pas forcément en WMS)
//                if ( !isset($wms_couche["wxs_title"]) ) continue;
//                // groupe de publication de la couche dans le WMS global
//                $wms_global_group = $wms_couche;
//                // la couche n'est pas publiée dans le WMS global
//                if ( is_null($wms_global_group) ) continue;
//                // la couche n'est pas publiée dans le WMS global sous un des domaines à transférer
//                if ( !in_array($wms_global_group, $wms_domaines) ) continue;
//                
//                $temp[$pk_couche_donnees] = array_merge($resources[$uuid][$pk_couche_donnees], $wms_couche);
//                // conserve les précédentes domaines où la
//                $layer_domaines = json_decode(str_replace(array("{", "}"), array("[", "]"), $resources[$uuid][$pk_couche_donnees]["couchd_wms_sdom"]), true) ?: array();
//                $keep_domaines[] = $wms_global_group;
//                $keep_domaines = array_merge($keep_domaines, $layer_domaines);
//            }
//            $wms_couches = $temp;
//            $wms_domaines = $keep_domaines;
//            $wms_domaines = array_diff($wms_domaines, array(-1, "-1"));
//            if ( !empty($wms_couches) ) {
//
//                $request->request->replace(array());
//                $request->request->set("uuid", $uuid);
//                $request->request->set("wms_domaines", $wms_domaines);
//                $request->request->set("wms_couches", $wms_couches);
//                $response = $controller->datasetToWMSAction($request);
//            }
//            foreach($resources[$uuid] as $pk_couche_donnees=>$couche){
//                break;
//                if ( isset($wms_couches[$pk_couche_donnees]) ) continue;
//                $CATALOGUE->executeQuery("update couche_donnees set couchd_wms=0, couchd_wms_sdom=null where pk_couche_donnees=:pk_couche_donnees", array(
//                    "pk_couche_donnees" => $pk_couche_donnees,
//                    "wms_domaines" => "{".implode(", ", $wms_domaines)."}"
//                ));
//            }
//        }
        if (!empty($sous_domaines)) {
            $output->writeln("");
            $output->writeln(
                "\n<error>Les sous-domaines suivants n'existent pas et n'ont pas été traités : </error>\n<error>- " . implode(
                    "</error>\n<error>- ",
                    $sous_domaines
                ) . "</error>"
            );
        }

        $output->writeln("\n<question>FIN D'EXECUTION</question>");
    }


    /**
     * Returns the default doctrine entity manager.
     *
     * @return \Doctrine\ORM\EntityManager Doctrine entity manager.
     */
    protected function getManager()
    {
        return $this->container->get('doctrine')->getManager();
    }

    /**
     * Get Doctrine connection
     *
     * @param string $connection_name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($connection_name, $schema = "public")
    {
        $conn = $this->container->get("doctrine")->getConnection($connection_name);
        $conn->exec('set search_path to ' . $schema);

        return $conn;
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getProdigeConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_PRODIGE, $schema);
    }

    /**
     *
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getCatalogueConnection($schema = "public")
    {
        return $this->getConnection(BaseController::CONNECTION_CATALOGUE, $schema);
    }

}

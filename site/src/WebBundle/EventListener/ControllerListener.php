<?php

namespace ProdigeCatalogue\WebBundle\EventListener;

use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class ControllerListener implements ContainerAwareInterface {

    use ContainerAwareTrait;

    protected function getDoctrine() {
        return $this->container->get('doctrine');
    }

    protected function getTemplating() {
        return $this->container->get('templating');
    }

    /**
     * Get Doctrine connection
     * @param string $name
     * @param string $schema
     * @return \Doctrine\DBAL\Connection
     */
    protected function getConnection($name, $schema="public") {
        $conn = $this->getDoctrine()->getConnection($name);
        $conn->exec('set search_path to '.$schema);
        return $conn;
    }

    public function onKernelController(FilterControllerEvent $event) {
        if(!$event->isMasterRequest()) {
            return;
        }
    }
}
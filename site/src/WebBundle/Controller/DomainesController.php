<?php

namespace ProdigeCatalogue\WebBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Routing\Annotation\Route; 
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Controller\User;
use Prodige\ProdigeBundle\DAOProxy\DAO;

class DomainesController extends BaseController {

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/get_domaines/{load}/{bAjax}/{fmeta_id}/{bAucunFiltre}", name="catalogue_get_domaines", options={"expose"=true} )
     * #Method({"GET", "POST"})
     * retourne la liste des domaines.sous-domaines
     * @param $bAjax      true : appel AJAX
     *                    false : autre
     * @param $fmeta_id   identifiant d'une métadonnée, permet de récupérer l'information "checked" associé au sous-domaine d'une métadonnée (optionnel)
     * */
    public function getDomainesAction(Request $request, $load, $bAjax=false, $fmeta_id=-1, $bAucunFiltre=false) {

        if($load == 1) {
            if(!$bAjax)
                return "<script language='javascript'>var domaineList = [];</script>";

                $strJson = '[';
                if($bAucunFiltre)
                    $strJson .= '{"text":"Aucun filtre (toutes les donn&eacute;es)","isDomain":true,"id":"-1","leaf":true,"expanded":false,"cls":"folder","children":[]},';

                    $conn = $this->getDoctrine()->getConnection('catalogue');
                    $dao = new DAO($conn, 'catalogue');
                    if($dao) {
                        $bFirstRubrique = true;
                        $glueRub = "\n";
                        //loop on rubrics
                        $rs_rubric = $dao->BuildResultSet("SELECT RUBRIC_ID, RUBRIC_NAME FROM RUBRIC_PARAM ORDER BY RUBRIC_ID");
                        for($rs_rubric->First(); !$rs_rubric->EOF();  $rs_rubric->Next()) {
                            $rubric_id = $rs_rubric->Read(0);
                            $rubric_name = html_entity_decode($rs_rubric->Read(1), ENT_QUOTES, 'UTF-8');
                            $strJson.= $glueRub.'{"text" : "'.$rubric_name.'", "id":"rubric_'.$rubric_id.'", "leaf":false, "expanded":'.($bFirstRubrique || $fmeta_id != -1 ? "true" : "false").', "cls":"folder", "children":[';
                            $bFirstRubrique = false;
                            $rs_dom = $dao->BuildResultSet("SELECT PK_DOMAINE, DOM_NOM" .
                                " FROM DOMAINE" .
                                //" WHERE DOM_RUBRIC=".$rubric_id .
                                " WHERE DOM_RUBRIC=?" .
                                " ORDER BY DOM_NOM", array($rubric_id));
                            $glueDom = "";

                            for($rs_dom->First(); !$rs_dom->EOF() ; $rs_dom->Next()) {
                                $pk_domaine = $rs_dom->Read(0);
                                $nom_domaine = html_entity_decode($rs_dom->Read(1), ENT_QUOTES, 'UTF-8');
                                //$strJson.= $glueDom.'{ "id": "dom_'.$pk_domaine.'", "isDomain" : true, "icon":"'.PRO_CATALOGUE_URLBASE.'images/domaines.gif", "text": "'.$nom_domaine.'", "leaf": false, "expanded":'.( $fmeta_id != -1 ? 'true' : 'false' ).', "cls": "file", "children":[';
                                $strJson.= $glueDom.'{ "id": "dom_'.$pk_domaine.'", "isDomain" : true, "icon":"/images/domaines.gif", "text": "'.$nom_domaine.'", "leaf": false, "expanded":'.( $fmeta_id != -1 ? 'true' : 'false' ).', "cls": "file", "children":[';
                                $rs_sdom = $dao->BuildResultSet("SELECT PK_SOUS_DOMAINE, SSDOM_NOM" .
                                    ( $fmeta_id != -1
                                        ? ", (SELECT COUNT(PK_SOUS_DOMAINE) AS NB" .
                                        " FROM (" .
                                        // récupère les sous-domaines pour les métadonnées de couche
                                        "   SELECT COUCHE_SDOM.PK_SOUS_DOMAINE, COUCHE_SDOM.FMETA_ID FROM COUCHE_SDOM" .
                                        " UNION" .
                                        // récupère les sous-domaines pour les métadonnées de carte
                                        "   SELECT CARTES_SDOM.PK_SOUS_DOMAINE, CARTES_SDOM.FMETA_ID FROM CARTES_SDOM" .
                                        " UNION" .
                                        // récupère les sous-domaines pour les métadonnées de service
                                        "   SELECT SSDCART_FK_SOUS_DOMAINE AS PK_SOUS_DOMAINE, FICHE_METADONNEES.FMETA_ID" .
                                        "   FROM SSDOM_VISUALISE_CARTE" .
                                        "   INNER JOIN CARTE_PROJET ON CARTE_PROJET.PK_CARTE_PROJET=SSDOM_VISUALISE_CARTE.SSDCART_FK_CARTE_PROJET" .
                                        "   INNER JOIN FICHE_METADONNEES ON FICHE_METADONNEES.PK_FICHE_METADONNEES=CARTE_PROJET.CARTP_FK_FICHE_METADONNEES" .
                                        " ) COUCHE_CARTE_SDOM" .
                                        // filtre sur la métadonnée
                                        " WHERE COUCHE_CARTE_SDOM.PK_SOUS_DOMAINE=SOUS_DOMAINE.PK_SOUS_DOMAINE" .
                                        //" AND COUCHE_CARTE_SDOM.FMETA_ID='".$fmeta_id."')"
                                        " AND COUCHE_CARTE_SDOM.FMETA_ID=?)"
                                        : ""
                                        ) .
                                    " FROM SOUS_DOMAINE" .
                                    //" WHERE SSDOM_FK_DOMAINE=".$pk_domaine .
                                    " WHERE SSDOM_FK_DOMAINE=?".
                                    " ORDER BY SSDOM_NOM", array($fmeta_id, $fmeta_id));
                                        $glueSDom = "";
                                        for($rs_sdom->First(); !$rs_sdom->EOF() ; $rs_sdom->Next()) {
                                            $pk_sous_domaine = $rs_sdom->Read(0);
                                            $nom_sous_domaine = html_entity_decode($rs_sdom->Read(1), ENT_QUOTES, 'UTF-8');
                                            $strJson.= $glueSDom.'{ "id": "sdom_'.$pk_sous_domaine.'", "isSubDomain" : true, "rubric" :"'.$rubric_name.'", "domain" :"'.$nom_domaine.'", "text": "'.$nom_sous_domaine.'", "leaf": true,'.( $fmeta_id != -1 ? ' "checked": '.( $rs_sdom->Read(2) > 0 ? 'true' : 'false' ).',' : '' ).'"cls": "file" }';
                                            $glueSDom = ",\n";
                                        }
                                        $strJson .= "]}";//suppression de la virgule

                                        $glueDom = ",\n";
                            }
                            $strJson .= "]}";

                            $glueRub = ",\n";
                        }
                        $strJson .= "]";
                    }
                    if($bAjax) {
                        $response = new Response($strJson);
                        $response->headers->add(array(
                            'Content-type' => 'text/html; charset=UTF-8;'
                        ));
                        return $response;
                    }
                    return "<script language='javascript'>var domaineList = ".$strJson.";</script>";
        }
        //return "<script language='javascript'>var domaineList = [];</script>";
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/get_domaines_for_store/{load}/{q}", name="catalogue_get_domaines_for_store", options={"expose"=true} )
     * #Method({"GET", "POST"})
     * retourne la liste des domaines/sous-domaines pour un store ExtJs
     */

    public function getDomainesForStoreAction(Request $request, $load, $q = "") {

        if($load == 1) {
            $tabDom = array("success" => false, "message" => "", "q" => $q, "rows" => array(), "total" => 0);

            $conn = $this->getDoctrine()->getConnection('catalogue');
            $dao = new DAO($conn, 'catalogue');
            if($dao) {
                $rs_dom = $dao->BuildResultSet("SELECT PK_DOMAINE, DOM_NOM, PK_SOUS_DOMAINE, SSDOM_NOM" .
                                               " FROM DOMAINE DOM LEFT JOIN SOUS_DOMAINE SDOM ON SDOM.SSDOM_FK_DOMAINE=DOM.PK_DOMAINE" .
                                               " ORDER BY DOM_NOM, SSDOM_NOM");
                $pk_domaine_previous = -1;
                for($rs_dom->First(); !$rs_dom->EOF() ; $rs_dom->Next()) {
                    $pk_domaine = $rs_dom->Read(0);
                    $dom_nom = html_entity_decode($rs_dom->Read(1), ENT_QUOTES, 'UTF-8');
                    $pk_sous_domaine = $rs_dom->Read(2);
                    $ssdom_nom = html_entity_decode($rs_dom->Read(3), ENT_QUOTES, 'UTF-8');
                    if ( $pk_domaine != $pk_domaine_previous && ( $q == "" || strstr($dom_nom, $q) ) ) {
                        $tabDom["rows"][] = array("id" => $dom_nom, "value" => $dom_nom);
                        $pk_domaine_previous = $pk_domaine;
                    }
                    if ( $q == "" || strstr($ssdom_nom, $q) ) {
                        $tabDom["rows"][] = array("id" => $ssdom_nom, "value" => "&nbsp;&nbsp;&nbsp;&nbsp;".$ssdom_nom);
                    }
                }
                $tabDom["success"] = true;
            } else {
                $tabDom["message"] = "Impossible de se connecter à la base de données.";
            }
            return new Response(json_encode($tabDom));
        }
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/get_users_sdomaines_admin_list/{load}", name="catalogue_get_users_sdomaines_admin_list", options={"expose"=true} )
     * #Method({"GET", "POST"})
     * retourne la liste des sous-domaines administrables par l'utilisateur
     */
    function getUserSDomainesAdminList(Request $request, $load) {

        if($load == 1) {
            $tabSDom = array();
            //global $AdminPath;
            //require_once($AdminPath."/../include/ClassUser.php");
            $userId = User::GetUserId();

            $conn = $this->getDoctrine()->getConnection('catalogue');
            $dao = new DAO($conn, 'catalogue');
            if ($dao && $userId) {
                $rs_sdom = $dao->BuildResultSet(
                    "SELECT PK_SOUS_DOMAINE, SSDOM_NOM" .
                    " FROM ADMINISTRATEURS_SOUS_DOMAINE" .
                    //" WHERE PK_UTILISATEUR = ".$userId .
                    " WHERE PK_UTILISATEUR = ?".
                    " ORDER BY SSDOM_NOM", array($userId));
                for($rs_sdom->First(); !$rs_sdom->EOF() ; $rs_sdom->Next()) {
                    $pk_sous_domaine = $rs_sdom->Read(0);
                    $nom_sous_domaine = html_entity_decode($rs_sdom->Read(1), ENT_QUOTES, 'UTF-8');
                    $tabSDom[] = array("id" => $pk_sous_domaine, "nom" => $nom_sous_domaine);
                }
            }
            $response = new Response(json_encode($tabSDom));
            $response->headers->add(array(
                'Content-type' => 'text/html; charset=UTF-8;'
            ));
            return $response;
        }
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/catalogue/get_metadata_sous_domaine_list/{load}/{fmeta_id}", name="catalogue_get_metadata_sous_domaine_list", options={"expose"=true} )
     * #Method({"GET", "POST"})
     * retourne la liste des sous-domaines d'une métadonnée
     */
    public function getMetadataSousDomainesList(Request $request, $load, $fmeta_id) {

        if($load == 1) {
            $tabSDom = array();

            $conn = $this->getDoctrine()->getConnection('catalogue');
            $dao = new DAO($conn, 'catalogue');
            if($dao) {
                $rs_sdom = $dao->BuildResultSet(
                    "SELECT SOUS_DOMAINE.PK_SOUS_DOMAINE, SOUS_DOMAINE.SSDOM_NOM" .
                    " FROM SOUS_DOMAINE" .
                    " INNER JOIN (" .
                    // récupère les sous-domaines pour les métadonnées de couche
                    "   SELECT COUCHE_SDOM.PK_SOUS_DOMAINE, COUCHE_SDOM.FMETA_ID FROM COUCHE_SDOM" .
                    " UNION" .
                    // récupère les sous-domaines pour les métadonnées de carte
                    "   SELECT CARTES_SDOM.PK_SOUS_DOMAINE, CARTES_SDOM.FMETA_ID FROM CARTES_SDOM" .
                    " UNION" .
                    // récupère les sous-domaines pour les métadonnées de service
                    "   SELECT SSDCART_FK_SOUS_DOMAINE AS PK_SOUS_DOMAINE, FICHE_METADONNEES.FMETA_ID" .
                    "   FROM SSDOM_VISUALISE_CARTE" .
                    "   INNER JOIN CARTE_PROJET ON CARTE_PROJET.PK_CARTE_PROJET=SSDOM_VISUALISE_CARTE.SSDCART_FK_CARTE_PROJET" .
                    "   INNER JOIN FICHE_METADONNEES ON FICHE_METADONNEES.PK_FICHE_METADONNEES=CARTE_PROJET.CARTP_FK_FICHE_METADONNEES) COUCHE_CARTE_SDOM ON COUCHE_CARTE_SDOM.PK_SOUS_DOMAINE = SOUS_DOMAINE.PK_SOUS_DOMAINE" .
                    //" WHERE COUCHE_CARTE_SDOM.FMETA_ID='".$fmeta_id."'" .
                    " WHERE COUCHE_CARTE_SDOM.FMETA_ID=?" .
                    " ORDER BY SSDOM_NOM", array($fmeta_id));
                for($rs_sdom->First(); !$rs_sdom->EOF() ; $rs_sdom->Next()) {
                    $pk_sous_domaine = $rs_sdom->Read(0);
                    $nom_sous_domaine = html_entity_decode($rs_sdom->Read(1), ENT_QUOTES, 'UTF-8');
                    $tabSDom[] = array("id" => $pk_sous_domaine, "nom" => $nom_sous_domaine);
                }
            }
            $response = new Response(json_encode($tabSDom));
            $response->headers->add(array(
                'Content-type' => 'text/html; charset=UTF-8;'
            ));
            return $response;
        }
    }

}

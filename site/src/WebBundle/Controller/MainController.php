<?php

namespace ProdigeCatalogue\WebBundle\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Prodige\ProdigeBundle\Controller\BaseController;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * @author alkante
 */
class MainController extends BaseController {

    protected $params;

    public function __construct(LoggerInterface $logger, RequestStack $requestStack, ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @Route("/", name="catalogue_web_main", options={"expose"=true})
     * 
     * Affiche la page d'accueil de prodige4
     */
    public function mainAction()
    {
        return $this->redirect($this->container->getParameter('PRO_GEONETWORK_URLBASE'));
    }

    /**
     * @Route("/thumbnails/{file}", name="catalogue_web_thumbnails", options={"expose"=true})
     * 
     * affiche les imagettes
     */
    public function thumbnailsAction(Request $request, $file)
    {
        $fileImg = PRO_PATH_THUMBNAILS."/{$file}";
        $path_parts = pathinfo($fileImg);

        if (file_exists($fileImg) && $path_parts['extension']==='png') {
            return new BinaryFileResponse($fileImg);
        } else {
            return new Response(Response::$statusTexts[Response::HTTP_NOT_FOUND], Response::HTTP_NOT_FOUND);
        }

    }
}

<?php

namespace ProdigeCatalogue\WebBundle\Controller;

use Symfony\Component\Routing\Annotation\Route; 
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Common\AlkRequest;
use Prodige\ProdigeBundle\Services\LdapUtils;

//use Symfony\Component\HttpFoundation\Response;
//use JMS\SecurityExtraBundle\Annotation\Secure;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;


/**
 * @Route("/admin")
 * 
 * @author alkante
 */
class ForgottenPasswordController extends BaseController {

    const ERROR_INVALID               = 1;
    const WARNING_REQUIRED_FIELD      = 2;
    const WARNING_PWD_NOTMATCH        = 3;
    const WARNING_PWD_TOOSHORT        = 4;
    const WARNING_LDAP_USER_NOTEXIST  = 5;
    const WARNING_LDAP_UPDATE_FAIL    = 6;
    const WARNING_USER_NOTEXIST       = 7;
    const WARNING_INVALID_CAPTCHA     = 8;
    const WARNING_ERROR_SENDING_EMAIL = 9;
    
    const PWD_MIN_LENGTH = 6;
    
    private static $ERROR_MSG = array(
        self::ERROR_INVALID               => "Token invalide ou paramètres non reconnus",
        self::WARNING_REQUIRED_FIELD      => "Saisie incorrecte, tous les champs sont obligatoires",
        self::WARNING_PWD_NOTMATCH        => "Les mots de passes ne correspondent pas",
        self::WARNING_PWD_TOOSHORT        => "Le mot de passe doit comporter au minimum 12 caractères, au moins un chiffre, une majuscule, une minuscule et un caractère spécial",
        self::WARNING_LDAP_USER_NOTEXIST  => "Le compte LDAP n'existe pas",
        self::WARNING_LDAP_UPDATE_FAIL    => "Échec de la modification du compte ldap",
        self::WARNING_USER_NOTEXIST       => "Identifiants non reconnus, le compte utilisateur n'existe pas",
        self::WARNING_INVALID_CAPTCHA     => "Le code de sécurité est invalide",
        self::WARNING_ERROR_SENDING_EMAIL => "L'envoi du courriel a échoué, veuillez contacter un administrateur",
    );
    
    /**
     * Nouvelle date d'expiration de mot de passe au format Y-m-d : aujourd'hui + 1 an
     */
    public static function getNewPdwExpireDate()
    {
        // date + 1 year
        return date('Y-m-d',strtotime(date("Y-m-d", time()) . " + 365 days"));
    }
    
    /**
     * Affiche le formulaire de changement de mot de passe
     * chgpwdToken doit être généré par AlkRequest::getEncodeParam("l=".<usr_id>."&p=".<token>);
     * 
     * @Route("/chgpwd/token/{chgpwdToken}", name="catalogue_web_chgpwd")
     */
    public function chgpwdAction(Request $request, $chgpwdToken)
    {   
        $regex =  $this->container->getParameter('PRODIGE_PASSWORD_REGEX');
        $regex = $regex ? $regex :  '/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\w\d\s:])([^\s]){8,16}$/';
          
        $conn = $this->getCatalogueConnection('catalogue');
        
        $tabToken = AlkRequest::readToken($chgpwdToken);
        $usr_id    = $tabToken['l'];
        $usr_token = $tabToken['p'];
        $referer   = $tabToken['ref'];
        
        $params = array(
            'title'       => "Mot de passe oublié",
            'chgpwdToken' => $chgpwdToken,
        );
 
        // erreurs sur le formulaire
        if( $request->query->has('error') && isset(self::$ERROR_MSG[$request->query->get('error')])) {
            $params['warning'] = sprintf(self::$ERROR_MSG[$request->query->get('error')], self::PWD_MIN_LENGTH);
        }
        
        $user = $conn->fetchAllAssociative('select pk_utilisateur, usr_id, usr_description from utilisateur where usr_id=:id and usr_description=:token and (date_expiration_compte is null or date_expiration_compte > now()) limit 1', 
            array('id'=>$usr_id,'token'=>$usr_token)
        );
        if(count($user)>0 ) {
            $user = $params['user'] = $user[0];
            
            // gérer la modification du compte
            if( $request->isMethod('POST') ) {
                $error = false;
                
                // modifier le mot de passe de l'utilisateur en BBD et LDAP
                $identifiant    = $request->request->get('IDENTIFIANT', null);
                $chgMotDePasse2 = $request->request->get('CHGMOTDEPASSE2', null);
                $chgMotDePasse3 = $request->request->get('CHGMOTDEPASSE3', null);

                if( null !== $identifiant && null !== $chgMotDePasse2 && null !== $chgMotDePasse3 ) {
                    if( $chgMotDePasse2 === $chgMotDePasse3 ) {
                        if(  !!preg_match_all( $regex, $chgMotDePasse2) ) {
                            // ok, encoder le mot de passe et maj BDD et LDAP
                            // get the password encoder
                            $encoder = $this->container->get('prodige.passwordencoder');
                            $encoder instanceof \Prodige\ProdigeBundle\Services\PasswordEncoder;

                            $encoded  = $encoder->encode($chgMotDePasse2);

                            $conn->beginTransaction();
                            // mettre à jour le mdp dans la base catalogue
                            $conn->update('utilisateur', 
                                array(
                                    'usr_password'    => $encoded,
                                    'usr_description' => null, // mettre le token à null
                                    'usr_pwdexpire'   => self::getNewPdwExpireDate(),
                                ), 
                                array('pk_utilisateur' => $user['pk_utilisateur'])
                            );

                            // config LDAP
                            $ldap_host     = $this->container->getParameter('ldap_host');
                            $ldap_port     = $this->container->getParameter('ldap_port');
                            $ldap_baseDn   = $this->container->getParameter('ldap_base_dn');
                            $ldap_dnAdmin  = $this->container->getParameter('ldap_search_dn');
                            $ldap_password = $this->container->getParameter('ldap_password');

                            $ldap = LdapUtils::getInstance($ldap_host, $ldap_port, $ldap_dnAdmin, $ldap_password, $ldap_baseDn);
                            // mettre à jour le compte sur LDAP
                            if( $ldap->userExist($user['usr_id']) ) {
                                if( !$ldap->changeUserPassword($user['usr_id'], $encoded)) {
                                    // echec lors de la modification du compte ldap
                                    $conn->rollBack();
                                    $error = self::WARNING_LDAP_UPDATE_FAIL;
                                } else {
                                    // ok, tous les changements ont été enregistrés
                                    $conn->commit();
                                }
                            } else {
                                // le compte ldap $user['usr_id'] n'existe pas
                                $conn->rollBack();
                                $error = self::WARNING_LDAP_USER_NOTEXIST;
                            }
                        } else {
                            // mot de passe trop court
                            $error = self::WARNING_PWD_TOOSHORT;
                        }
                    } else {
                        // les mots de passes ne correspondent pas
                        $error = self::WARNING_PWD_NOTMATCH;
                    }
                } else {
                    // saisie incorrecte, tous les champs sont obligatoires
                    $error = self::WARNING_REQUIRED_FIELD;
                }
                
                if( $error ) {
                    // redirect error
                    return $this->redirectToRoute('catalogue_web_chgpwd', array('chgpwdToken'=>$chgpwdToken, 'error'=>$error));
                } else {
                    return $this->redirectToRoute('catalogue_web_chgpwd_success', array('referer' => $referer));
                }
            
            } // end POST
            
        } else {
            // token invalide ou paramètres non reconnus
            $params['error'] = self::$ERROR_MSG[self::ERROR_INVALID];
        }
        
        return $this->render('WebBundle/Default/chgpwd.html.twig', $params);
    }
    
    /**
     * @Route("/chgpwd/success", name="catalogue_web_chgpwd_success")
     */
    public function chgpwdSuccessAction(Request $request)
    {
        $referer = $request->query->get('referer');
        $params = array(
            'title' => "Mot de passe oublié",
            'URL_BASE' => ($referer!="" ? $referer : $this->container->getParameter('PRO_GEONETWORK_URLBASE')),
            'PRO_INCLUDED' => $this->container->getParameter('PRO_INCLUDED'),
            'PRO_GEONETWORK_URLBASE' => $this->container->getParameter('PRO_GEONETWORK_URLBASE'),
        );
        return $this->render('WebBundle/Default/chgpwd-success.html.twig', $params);
    }
    
    /**
     * Interface de demande de changement de mot de passe
     * 
     * @Route("/chgpwd", name="catalogue_web_chgpwd_ask")
     */
    public function chgpwdAskAction(Request $request)
    {
        $conn = $this->getCatalogueConnection('catalogue');
        
        $params = array(
            'usr_login'   => $request->query->get('login', ''),
            'usr_email'   => $request->query->get('usr_email', ''),
        );
        
        // erreurs sur le formulaire
        if( $request->query->has('error') && isset(self::$ERROR_MSG[$request->query->get('error')])) {
            $params['warning'] = self::$ERROR_MSG[$request->query->get('error')];
        }
        
        if( $request->isMethod('POST') ) {
            $error = false;
                
            // recupérer les input POST
            $usr_login = $request->request->get('usr_login', null);
            $usr_email = $request->request->get('usr_email', null);
            $captcha   = $request->request->get('captcha', null);
            
            if( null!==$usr_login && null!==$usr_email && null!==$captcha ) {
                //if( $captcha == $_SESSION['key'] ) {
                if( $captcha == $request->getSession()->get('key') ) {
                    // vérifier si le compte existe
                    $user = $conn->fetchAllAssociative('select pk_utilisateur, usr_id, usr_email, usr_nom, usr_prenom, usr_description from utilisateur where usr_id ilike :id and usr_email ilike :email and (date_expiration_compte is null or date_expiration_compte > now()) limit 1', 
                        array('id'=>$usr_login,'email'=>$usr_email)
                    );
                    if(count($user)>0 ) {
                        $user = $user[0];
                        
                        $usrToken = base64_encode(time().uniqid());
                        // mettre à jour le token de mdp oublié dans la base catalogue
                        $conn->update('utilisateur', array('usr_description' => $usrToken), array('pk_utilisateur' => $user['pk_utilisateur']));
                        
                        // ok, envoyer le courriel de changement de mdp
                        $mailFrom = PRO_CATALOGUE_EMAIL_AUTO;
                        $nameFrom = PRO_CATALOGUE_NOM_EMAIL_AUTO;
                        $mailTo   = $user['usr_email'];
                        $nameTo   = $user['usr_prenom'].' '.$user['usr_nom'];

                        $strTokenPwd = AlkRequest::getEncodeParam("l=".$user['usr_id']."&p=".$usrToken);
                        $url = $this->container->get('router')->generate('catalogue_web_chgpwd', array('chgpwdToken'=>$strTokenPwd), \Symfony\Component\Routing\RouterInterface::ABSOLUTE_URL);

                        $html = 
                        "Demande de nouveau mot de passe".'<br/><br/>'.
                        "Vous avez sollicité une demande de mot de passe oublié le ".date("d/m/Y")." à ".date("H:i").".<br/>".
                        "Vos nouveaux paramètres de connexion sont les suivants :".
                        "<ul>".
                        '<li>'.sprintf("Identifiant : %s", $user['usr_id']).'</li>'.
                        '<li>'.'<a href="'.$url.'">'.
                                sprintf("Cliquez sur ce lien pour modifier votre mot de passe").'</a>.'.
                        '</li>'.
                        '</ul>'
                        ;

                        $message = new \Swift_Message("Demande de nouveau mot de passe", $html, 'text/html', 'UTF-8');
                        $message->setFrom($mailFrom, $nameFrom)->setTo($mailTo, $nameTo);
                
                        if( ! $this->container->get('mailer')->send($message) ) {
                            // erreur lors de l'envoie du courriel
                            $error = self::WARNING_ERROR_SENDING_EMAIL;
                        }
                        
                    } else {
                        // identifiants non reconnus, le compte utilisateur n'existe pas
                        $error = self::WARNING_USER_NOTEXIST;
                    }
                } else {
                    // le code de sécurité est invalide
                    $error = self::WARNING_INVALID_CAPTCHA;
                }
            } else {
                // saisie incorrecte, tous les champs sont obligatoires
                $error = self::WARNING_REQUIRED_FIELD;
            }
            
            if( $error ) {
                // redirect error
                return $this->redirectToRoute('catalogue_web_chgpwd_ask', array('login'=>$usr_login, 'usr_email'=>$usr_email, 'error'=>$error));
            } else {
                return $this->redirectToRoute('catalogue_web_chgpwd_ask_success');
            }
        }
        
        return $this->render('WebBundle/Default/chgpwd-ask.html.twig', $params);
    }
    
    /**
     * @Route("/chgpwd/asksuccess", name="catalogue_web_chgpwd_ask_success")
     */
    public function chgpwdAskSuccessAction(Request $request)
    {
        $params = array(
            
        );
        return $this->render('WebBundle/Default/chgpwd-ask-success.html.twig', $params);
    }
    
    /**
     * @deprecated désactivé pour des raisons de sécurité
     * @since 4.1.4
     * 
     * Vérifie si un compte utilisateur a expiré ou non
     * 
     * ###Route("/chgpwd/checkaccount/{userId}", name="catalogue_web_chgpwd_checkaccount")
     */
    public function chgpwdCheckAccountAction(Request $request, $userId)
    {
        throw new Exception('deprecated');
        
        // if not already initialized
        $user = new \Prodige\ProdigeBundle\Controller\User();
        $user->setContainer($this->container);
        
        $userDetails = $user->getUserAccountExpirationDetails($userId);
        
        return new JsonResponse($userDetails);
    }
    
}

<?php
namespace ProdigeCatalogue\WebBundle\Common\Ressources;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class Ressources {

    // #################################
    static public $CONNECTION_CONNECTION_FAILED = "Impossible de se connecter à la Base.\n";
    static public $DAO_RESULSTSET_NOT_GOOD = "La requête n'a pu être exécutée.";
    static public $DAO_EXECUTE_ERROR = "Il y a eu une erreur à l'exécution de la requête.";
    // #################################
    static public $GRAPHIC_BG_COLOR ="#FFFFFF";
    static public $GRAPHIC_FG_COLOR ="#000055";

    // #################################
    static public $ACTION_RANGE = 1000;
    static public $ACTION_OPEN_INITIAL = 1000;
    static public $ACTION_OPEN_PROFILES     = 2000;
    static public $ACTION_OPEN_USERS        = 3000;
    static public $ACTION_OPEN_RUBRICS      = 3500;
    static public $ACTION_OPEN_DOMAINS      = 4000;
    static public $ACTION_OPEN_SUBDOMAINS   = 5000;
    static public $ACTION_OPEN_LAYERS       = 7000;
    static public $ACTION_OPEN_MAPS         = 8000;

    static public $ACTION_OPEN_COMPETENCES  = 9000;
    static public $ACTION_OPEN_ZONAGES      = 9500;
    static public $ACTION_OPEN_PERIMETRES   = 9750;
    static public $ACTION_OPEN_GROUPING     = 10000;
    static public $ACTION_OPEN_OBJECTS      = 6;//6ème onglet
    static public $ACTION_OPEN_RUBRICS_AIDE      = 10250;
    
    static public $ACTION_OPEN_GROUPING_PROFILES = 10001;
    static public $ACTION_OPEN_GROUPING_SUBDOMAINS = 10002;

    static public $ACTION_OPEN_OBJECTS_DONNEE     = 11001;
    static public $ACTION_OPEN_OBJECTS_CARTE      = 11002;
    static public $ACTION_OPEN_OBJECTS_METADONNEE = 11003;
    static public $ACTION_OPEN_OBJECTS_MODELE     = 11004;
    static public $ACTION_OPEN_OBJECTS_GROUPS     = 11005;

    static public $BTN_INITIAL      = "Accueil";
    static public $BTN_PROFILES     = "Profils";
    static public $BTN_MEMBERS      = "Membres";
    static public $BTN_ACTIONS      = "Traitements";
    static public $BTN_ADMIN        = "Administre";
    static public $BTN_EDITE         = "Edite";
    static public $BTN_DOMAINS      = "Domaines";
    static public $BTN_SUBDOMAINS   = "Sous-Domaines";
    static public $BTN_OBJECTS      = "Données accessibles";
    static public $BTN_COMPETENCES  = "Compétences";
    static public $BTN_ALERTS       = "Alertes sur édition";
    static public $BTN_ZONAGES      = "Zonages";
    static public $BTN_PERIMETRES   = "Territoires (consultation/téléchargement)";
    static public $BTN_PERIMETRES_EDITION   = "Territoires (Edition)";
    static public $BTN_COMPOSTION = "Composition";
    static public $BTN_LAYERS       = "Couches";
    static public $BTN_MAPS         = "Cartes";
    static public $BTN_RUBRICS      = "Rubriques";
    static public $BTN_IMPORT_DATA  = "Importation";
    static public $BTN_GROUPPROFILS = "Groupes-profil";
    static public $BTN_GROUPSUBDOMS = "Sous-domaines";
    static public $BTN_PROFILS_AUTORISES        = "Profils autorisés";

    static public $BTN_OBJECTS_DONNEE     = "Données";
    static public $BTN_OBJECTS_CARTE      = "Cartes";
    static public $BTN_OBJECTS_METADONNEE = "Métadonnées";
    static public $BTN_OBJECTS_MODELE     = "Modèles";
    static public $BTN_OBJECTS_GROUPS     = "Profils";

    // #################################

    static public $RELATIONLIST_PROFILES_MEMBERS_RELATION = "A pour Utilisateurs";
    static public $RELATIONLIST_PROFILES_MEMBERS_TABLE = "Utilisateurs Disponibles";

    static public $RELATIONLIST_PROFILES_ADMIN_RELATION = "Administre";
    static public $RELATIONLIST_PROFILES_ADMIN_TABLE = "Sous-Domaines Disponibles";

    static public $RELATIONLIST_PROFILES_EDITE_RELATION = "Edite";
    static public $RELATIONLIST_PROFILES_EDITE_TABLE = "Sous-Domaines Disponibles";

    static public $RELATIONLIST_PROFILES_DOM_RELATION = "Accède à";
    static public $RELATIONLIST_PROFILES_DOM_TABLE = "N'accède pas à";

    static public $RELATIONLIST_PROFILES_SSDOM_RELATION = "Accède à";
    static public $RELATIONLIST_PROFILES_SSDOM_TABLE = "N'accède pas à";

    static public $RELATIONLIST_PROFILES_COMPETENCE_RELATION = "Accède à";

    static public $RELATIONLIST_PROFILES_COMPETENCE_TABLE = "N'accède pas à";

    static public $RELATIONLIST_PROFILES_PERIMETRE_RELATION = "Consulte / télécharge sur le territoire";
    static public $RELATIONLIST_PROFILES_PERIMETRE_TABLE = "Ne peut pas consulter / télécharger sur le territoire";

    static public $RELATIONLIST_PROFILES_PERIMETRE_EDITION_RELATION = "Edite sur le territoire";
    static public $RELATIONLIST_PROFILES_PERIMETRE_EDITION_TABLE = "Ne peut pas éditer sur le territoire";

    static public $RELATIONLIST_PROFILES_TRT_RELATION = "Peut utiliser";
    static public $RELATIONLIST_PROFILES_TRT_TABLE = "Traitements Disponibles";

    static public $RELATIONLIST_PROFILES_ACTIONS_RELATION = "Droits activés";
    static public $RELATIONLIST_PROFILES_ACTIONS_TABLE = "Droits Disponibles";

    static public $PROFILES_EMPTY_SELECT = "Veuillez choisir un profil dans la liste.";

    // #################################

    static public $RELATIONLIST_OBJECTS_TRT_RELATION = "Accède à";
    static public $RELATIONLIST_OBJECTS_TRT_TABLE = "Traitements disponibles";

    // #################################

    static public $RELATIONLIST_USERS_PROFILES_RELATION = "A pour Profil";
    static public $RELATIONLIST_USERS_PROFILES_TABLE = "Profils Disponibles";

    static public $USERS_EMPTY_SELECT = "Veuillez choisir un utilisateur dans la liste.";

    // #################################

    static public $RELATIONLIST_DOMAINS_SSDOM_RELATION = "A pour Sous-Domaines";
    static public $RELATIONLIST_DOMAINS_SSDOM_TABLE = "Sous-Domaines Disponibles";

    static public $DOMAINS_EMPTY_SELECT = "Veuillez choisir un domaine dans la liste.";

    // #################################

    static public $RELATIONLIST_ACTIONS_PROFILES_RELATION = "Droit activés sur Profils";
    static public $RELATIONLIST_ACTIONS_PROFILES_TABLE = "Profils Disponibles";

    static public $ACTIONS_EMPTY_SELECT = "Veuillez choisir un traitement dans la liste.";

    // #################################

    static public $RELATIONLIST_LAYERS_SSDOM_RELATION = "Sous-Domaines Accédants";
    static public $RELATIONLIST_LAYERS_SSDOM_TABLE = "Sous-Domaines Disponibles";

    static public $RELATIONLIST_LAYERS_COMPETENCE_RELATION = "Téléchargée par la compétence";
    static public $RELATIONLIST_LAYERS_COMPETENCE_TABLE = "Compétences disponibles";

    static public $RELATIONLIST_LAYERS_ALERTPERIM_RELATION = array("Lors de l'édition du territoire", "Alerter les utilisateurs");
    static public $RELATIONLIST_LAYERS_ALERTPERIM_TABLE = array("Territoires concernés", "Utilisateurs avertis");

    static public $RELATIONLIST_COMPETENCE_LAYERS_RELATION = "Couches téléchargées par la compétence";
    static public $RELATIONLIST_COMPETENCE_LAYERS_TABLE = "Couches disponibles";

    static public $RELATIONLIST_MAPS_COMPETENCE_RELATION = "Téléchargée par la compétence";
    static public $RELATIONLIST_MAPS_COMPETENCE_TABLE = "Compétences disponibles";

    static public $RELATIONLIST_COMPETENCE_MAPS_RELATION = "Cartes téléchargées par la compétence";
    static public $RELATIONLIST_COMPETENCE_MAPS_TABLE = "Cartes disponibles";
    static public $LAYERS_EMPTY_SELECT = "Veuillez choisir une couche dans la liste.";

    // #################################

    static public $RELATIONLIST_MAPS_SSDOM_RELATION = "Sous-Domaines Visualisants";
    static public $RELATIONLIST_MAPS_SSDOM_TABLE = "Sous-Domaines Disponibles";

    static public $MAPS_EMPTY_SELECT = "Veuillez choisir une carte dans la liste.";

    // #################################

    static public $COMPETENCE_EMPTY_SELECT = "Veuillez choisir une compétence dans la liste.";

    // #################################

    static public $ZONAGES_EMPTY_SELECT = "Veuillez choisir un zonage dans la liste.";

    // #################################

    static public $PERIMETRES_EMPTY_SELECT = "Veuillez choisir un territoire dans la liste.";


    static public $RELATIONLIST_SUBDOMAINS_DOM_RELATION = "A pour Domaine";
    static public $RELATIONLIST_SUBDOMAINS_DOM_TABLE = "Domaines Disponibles";
    static public $RELATIONLIST_SUBDOMAINS_LAYERS_RELATION = "A pour Couches";
    static public $RELATIONLIST_SUBDOMAINS_LAYERS_TABLE = "Couches Disponibles";
    static public $RELATIONLIST_SUBDOMAINS_MAPS_RELATION = "A pour Cartes";
    static public $RELATIONLIST_SUBDOMAINS_MAPS_TABLE = "Cartes Disponibles";

    static public $SUBDOMAINS_EMPTY_SELECT = "Veuillez choisir un sous-domaine dans la liste.";
    // #################################
    
    static public $RELATIONLIST_PROFILES_RUBRICS_AIDE_RELATION = "Accède à";
    static public $RELATIONLIST_PROFILES_RUBRICS_AIDE_TABLE = "N'accède pas à";
    
    // #################################
    static public $IFRAME_NAME_MAIN = "AdminDroits";

    // #################################
    static public $PAGE_TITLE_MAIN = "Gestion des Droits Prodige";
    // #################################
    static public $MAIN_SIZE_X = "100%";//640
    static public $MAIN_SIZE_Y = 440;
    static public $MODULE_SIZE_X ="99%";//640
    static public $MODULE_SIZE_Y =420;//428
    static public $LIST_SIZE_X = 150;
    static public $LIST_SIZE_Y = 420;
    static public $SELECT_LIST_SIZE_X = 145;
    static public $SELECT_LIST_SIZE_Y = 400;
    static public $DETAIL_SIZE_X = 700;
    static public $DETAIL_SIZE_Y = 420;
    static public $SUB_DETAILS_SIZE_X ="100%";
    static public $SUB_DETAILS_SIZE_Y =380;
    static public $SUB_OBJECTS_SIZE_X =500;
    static public $SUB_OBJECTS_SIZE_Y =350;
    // #################################
    static public $LIST_ADD_ACTION = 10;
    static public $LIST_MODIFY_ACTION = 20;
    static public $LIST_DELETE_ACTION = 30;

    static public $SELECTLIST_ADD = "Ajouter";
    static public $SELECTLIST_DELETE = "Supprimer";

    static public $NEWROW = -999;
    // #################################
    static public $RELATIONLIST_ADD_TO_RELATION_ACTION = 111;
    static public $RELATIONLIST_REMOVE_FROM_RELATION_ACTION = 222;
    static public $RELATIONLIST_UPDATE_TO_RELATION_ACTION = 333;

    // #################################

    static public $BTN_NAV_INITIAL = "Accueil";
    static public $BTN_NAV_PROFILES = "Profils";
    static public $BTN_NAV_USERS = "Utilisateurs";
    static public $BTN_NAV_RUBRICS = "Rubriques";
    static public $BTN_NAV_DOMAINS = "Domaines";
    static public $BTN_NAV_SUBDOMAINS = "Sous-Domaines";
    //static public $BTN_NAV_ACTIONS = "Traitements";
    static public $BTN_NAV_LAYERS = "Couches";
    static public $BTN_NAV_MAPS = "Cartes";
    static public $BTN_NAV_ZONAGES = "Zonages";
    static public $BTN_NAV_PERIMETRES = "Territoires";
    static public $BTN_NAV_COMPETENCES = "Compétences";
    static public $BTN_NAV_GROUPING = "Regroupements";

    // #################################
    static public $EDITFORM_BTN_VALIDATE = "Valider";
    static public $EDITFORM_BTN_CANCEL = "Annuler";
    static public $EDITFORM_BTN_DELETE = "Supprimer";
    static public $EDITFORM_WAS_DELETE = "Enregistrement supprimé.";
    static public $EDITFORM_DELETE_CONFIRMATION = "Êtes-vous sûr de vouloir supprimer cet enregistrement ?";
    static public $EDITFORM_VALIDATE = 9111;
    static public $EDITFORM_CANCEL = 9222;
    static public $EDITFORM_DELETE = 9333;

    // #################################
    // ########### PROFILES ############
    // #################################
    static public $MODULE_PROFILES_ACCUEIL =
    "<BR><BR><BR>
        <CENTER>
            Gestion des Profils <BR><BR><BR><BR><BR><BR>
            Veuillez choisir un profil dans la liste de gauche.
        </CENTER>";

    // #################################
    // ###########   USERS  ############
    // #################################

    static public function getTabModuleSheet() {
        $TAB_MODULE_SHEETS = array(
            "default" => array(
                array(
                    //"title" => "Ma fiche utilisateur",    "url" => "administration_utilisateur.php"),
                    "title" => "Ma fiche utilisateur",      "url" => "catalogue_administration_administrationUtilisateur"),
                    //array("title" => "Droits",            "url" => "administration_compte_utilisateur.php&Id=".User::GetUserId().""),
                    array("title" => "Droits",              "url" => "catalogue_administration_administrationCompteUtilisateur")
            ),
            self::$ACTION_OPEN_PROFILES => array(
                //array("title" => self::$BTN_INITIAL,    "url" => "Administration/Administration/Profiles/ProfilesAccueil.php"),
                array("title" => self::$BTN_INITIAL,    "url" => "catalogue_administration_profilesAccueil"),
                //array("title" => self::$BTN_MEMBERS,    "url" => "Administration/Administration/Profiles/ProfilesMembers.php"),
                array("title" => self::$BTN_MEMBERS,    "url" => "catalogue_administration_profilesMembers"),
                //array("title" => self::$BTN_ACTIONS,    "url" => "Administration/Administration/Profiles/ProfilesTraitements.php"),
                array("title" => self::$BTN_ACTIONS,    "url" => "catalogue_administration_profilesTraitements"),
                //array("title" => self::$BTN_ADMIN,      "url" => "Administration/Administration/Profiles/ProfilesAdmin.php"),
                array("title" => self::$BTN_ADMIN,      "url" => "catalogue_administration_profilesAdmin"),
                //array("title" => self::$BTN_DOMAINS,    "url" => "Administration/Administration/Profiles/ProfilesDomains.php"),
                array("title" => self::$BTN_DOMAINS,    "url" => "catalogue_administration_profilesDomains"),
                //array("title" => self::$BTN_SUBDOMAINS, "url" => "Administration/Administration/Profiles/ProfilesSSDomains.php"),
                array("title" => self::$BTN_SUBDOMAINS, "url" => "catalogue_administration_profilesSSDomains"),
                //array("title" => self::$BTN_OBJECTS,    "url" => "Administration/Administration/Profiles/ProfilesObjects.php"),
                array("title" => self::$BTN_OBJECTS,    "url" => "catalogue_administration_profilesObjects"),
                //array("title" => self::$BTN_COMPETENCES,"url" => "Administration/Administration/Profiles/ProfilesCompetences.php")
                array("title" => self::$BTN_COMPETENCES,"url" => "catalogue_administration_profilesCompetences")
            ),

            self::$ACTION_OPEN_USERS => array(
                array("title" => self::$BTN_INITIAL,  "url" => "Administration/Administration/Users/UsersAccueil.php"),
                array("title" => self::$BTN_PROFILES, "url" => "Administration/Administration/Users/UsersProfiles.php"),
                array("title" => "Droits",            "url" => "administration_compte_utilisateur.php"),
                array("title" => self::$BTN_PERIMETRES ,"url" => "Administration/Administration/Users/UsersPerimetres.php"),

            ),

            self::$ACTION_OPEN_DOMAINS => array(
                array("title" => self::$BTN_INITIAL,  "url" => "Administration/Administration/Domains/DomainsAccueil.php"),
                array("title" => self::$BTN_SUBDOMAINS,    "url" => "Administration/Administration/Domains/DomainsSubDomains.php"),
            ),

            self::$ACTION_OPEN_SUBDOMAINS => array(
                array("title" => self::$BTN_INITIAL, "url" => "Administration/Administration/SubDomains/SubDomainsAccueil.php"),
                array("title" => self::$BTN_DOMAINS,  "url" => "Administration/Administration/SubDomains/SubDomainsDomains.php"),
            ),

            self::$ACTION_OPEN_LAYERS => array(
                array("title" => self::$BTN_INITIAL,      "url" => "Administration/Administration/Layers/LayersAccueil.php"),
                array("title" => self::$BTN_COMPETENCES,  "url" => "Administration/Administration/Layers/LayersCompetences.php"),
            ),

            self::$ACTION_OPEN_MAPS => array(
                array("title" => self::$BTN_INITIAL,      "url" => "Administration/Administration/Maps/MapsAccueil.php"),
                array("title" => self::$BTN_COMPETENCES,  "url" => "Administration/Administration/Maps/MapsCompetences.php"),
            ),

            self::$ACTION_OPEN_COMPETENCES => array(
                array("title" => self::$BTN_INITIAL,      "url" => "Administration/Administration/Competences/CompetencesAccueil.php"),
                array("title" => self::$BTN_PROFILES,      "url" => "Administration/Administration/Competences/CompetencesProfiles.php"),
                array("title" => self::$BTN_OBJECTS_DONNEE, "url" => "Administration/Administration/Competences/CompetencesLayers.php"),
                array("title" => self::$BTN_OBJECTS_CARTE, "url" => "Administration/Administration/Competences/CompetencesMaps.php"),
            ),

            self::$ACTION_OPEN_ZONAGES => array(
                array("title" => self::$BTN_INITIAL,      "url" => "Administration/Administration/Zonages/ZonagesAccueil.php")
            ),

            self::$ACTION_OPEN_PERIMETRES => array(
                array("title" => self::$BTN_INITIAL,      "url" => "Administration/Administration/Perimetres/PerimetresAccueil.php"),
                array("title" => self::$BTN_COMPOSTION,   "url" => "Administration/Administration/Perimetres/PerimetresComposition.php")
            ),

            self::$ACTION_OPEN_RUBRICS => array(
                array("title" => self::$BTN_INITIAL,      "url" => "Administration/Administration/Rubrics/RubricsAccueil.php"),
            ),

            self::$ACTION_OPEN_GROUPING_PROFILES => array(
                array("title" => self::$BTN_PROFILES,      "url" => "Administration/Administration/Grouping/GroupingGroupesProfil.php"),
            ),

            self::$ACTION_OPEN_GROUPING_SUBDOMAINS => array(
                array("title" => self::$BTN_SUBDOMAINS,      "url" => "Administration/Administration/Grouping/GroupingSubDomains.php"),
            ),
            self::$ACTION_OPEN_RUBRICS_AIDE => array(
                array("title" => self::$BTN_INITIAL,      "url" => "Administration/Administration/RubricsAide/RubricsAideAccueil.php"),
                array("title" => self::$BTN_PROFILS_AUTORISES,      "url" => "Administration/Administration/RubricsAide/RubricsProfils.php"),
            ),

        );

        if(PRO_EDITION=="on"){
            array_push($TAB_MODULE_SHEETS[self::$ACTION_OPEN_USERS], array("title" => self::$BTN_PERIMETRES_EDITION ,"url" => "Administration/Administration/Users/UsersPerimetresEdition.php"));
            array_push($TAB_MODULE_SHEETS[self::$ACTION_OPEN_PROFILES], array("title" => self::$BTN_EDITE,      "url" => "Administration/Administration/Profiles/ProfilesEdite.php"));
            array_push($TAB_MODULE_SHEETS[self::$ACTION_OPEN_LAYERS], array("title" => self::$BTN_ALERTS,       "url" => "Administration/Administration/Layers/LayersAlerts.php"));
        }
        return $TAB_MODULE_SHEETS;
    }

    private function __construct() {
        //
    }

    public static function getVars(Controller $controller) {
        //TODO hismail
        //global $AdminUrl;

        $pageUrl = $controller->generateUrl('catalogue_web_page');
        
        $tabMainMenu = array();
        $strJs = "<script type='text/javascript'>";
        //$strJs .= "var defaultPage = 'droits.php?Action=default&iPart=1';";
        $strJs .= "var defaultPage = '".$pageUrl."?Action=default&iPart=1';";
        //$strJs .= "var emptyPage = 'droits.php?url=Administration/Administration/Main/Empty.php';"; // TODO
        $strJs .= "var emptyPage = '".$pageUrl."?Action=empty&iPart=1';"; // TODO

        //hismail
        if(get_class_vars(self::class)) {
            foreach(get_class_vars(self::class) as $var_name=>$var_value ) {
                if(preg_match("!^ACTION_OPEN_!", $var_name)) {
                    $tabMainMenu[$var_name] = "";
                }
                if(preg_match("!^BTN_NAV_!", $var_name)) {
                    $tabMainMenu[str_replace("BTN_NAV_", "ACTION_OPEN_", $var_name)] = $var_name;
                }
                if(preg_match("!^ACTION_OPEN_!", $var_name)
                    || preg_match("!^BTN_NAV_!", $var_name)
                    || preg_match("!^NEWROW!", $var_name)
                    ) {
                        $strJs .= "\n var ".$var_name." = '".addslashes(str_replace("\n", "\\n", $var_value))."';";
                }
            }
        }

        $strJs .= "\n var tabMainMenu = [];";
        foreach ($tabMainMenu as $menu_id=>$menu_title){
            if ( $menu_title!="" )
                $strJs .= "\n tabMainMenu[".$menu_id."] = ".$menu_title.";";
        }
        $strJs .= "\n var tabPageAdd = [];";
        $tabSheets = Ressources::getTabModuleSheet();
        foreach ($tabSheets as $menu_id=>$tabMenuDesc){
            $strJs .= "\n tabPageAdd['".$menu_id."'] = \"".(array_key_exists("addurl", $tabMenuDesc[0]) ? $tabMenuDesc[0]["addurl"] : $tabMenuDesc[0]["url"])."\";";
        }
        $strJs .= "</script>";
        return $strJs;
    }

}

CREATE SEQUENCE catalogue.seq_rawgraph_couche_config
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE;

CREATE TABLE catalogue.rawgraph_couche_config (
    id integer DEFAULT nextval('catalogue.seq_rawgraph_couche_config'::regclass) NOT NULL,
    titre character varying(255) NOT NULL,
    resume text NOT NULL,
    serie_donnees_uuid character varying(255) NOT NULL,
    couche character varying(255) NOT NULL,
    colonnes text NOT NULL,
    uuid character varying(255) NOT NULL,
    config json
);

ALTER TABLE ONLY catalogue.rawgraph_couche_config
    ADD CONSTRAINT pk_rawgraph_couche_config PRIMARY KEY (id);

<?php

namespace ProdigeCatalogue\GraphBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Prodige\ProdigeBundle\Controller\BaseController;
use Prodige\ProdigeBundle\Services\GeonetworkInterface;
use Symfony\Component\Process\Process;


//use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


/**
 * @Route("/graph")
 */
class DefaultController extends BaseController
{
    /**
     * Retourne les informations du paramétrage d'un graph
     * 
     * @param  $uuid
     * @param  $fields
     * 
     * @return 
     */
    private function getRawgraphConfByUuid($uuid, $fields)
    {
        $values = [];

        $CATALOGUE = $this->getCatalogueConnection(self::CONNECTION_CATALOGUE);

        $sql = "SELECT ".implode(", ", $fields)." FROM rawgraph_couche_config WHERE uuid = :uuid ";
        $values = $CATALOGUE->fetchAllAssociative($sql, ['uuid' => $uuid]);
        if( count($values) == 1 ) { 
            $values = $values[0];
        }

        return $values;
    }

    /**
     * Mise à jour de la métadonnée de service associée
     * 
     * @param  $uuid
     * 
     * @return 
     */
    private function updateMetadata($uuid, $uuidLayer, $tableLayer)
    {
      $CATALOGUE = $this->getCatalogueConnection(self::CONNECTION_CATALOGUE);
      $metadata = $CATALOGUE->fetchAssociative(" SELECT data, id ".
                                         " FROM public.metadata " .
                                         " WHERE uuid=:uuid", array("uuid" => $uuid));
      if($metadata){
        $XMLDATA =  $metadata["data"];
        $metadataId = $metadata["id"];

        $version  = "1.0";
        $encoding = "UTF-8";
        $metadata_doc = new \DOMDocument($version, $encoding);
        $entete = "<?xml version=\"".$version."\" encoding=\"".$encoding."\"?>\n";
        $metadata_data = $entete.$XMLDATA;
        $metadata_doc->loadXML(($metadata_data));

        //suppression du noeud s'il existe
        $xpath = new \DOMXpath($metadata_doc);
        $nodeToDelete = $xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:coupledResource");
        foreach($nodeToDelete as $k => $item){
            $item->parentNode->removeChild($item);
        }

        //suppression du noeud s'il existe
        $xpath2 = new \DOMXpath($metadata_doc);
        $nodeToDelete2 = $xpath2->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/srv:operatesOn");
        foreach($nodeToDelete2 as $k2 => $item2){
            $item2->parentNode->removeChild($item2);
        }

        //suppression du noeud s'il existe
        $distributionInfo = $metadata_doc->getElementsByTagName("distributionInfo");
        if($distributionInfo && $distributionInfo->length>0){
            foreach($distributionInfo as $key => $node){
                $node->parentNode->removeChild($node);
            }
        }

        if($uuidLayer!="" && $tableLayer !=""){

          $strXML = '<gmd:distributionInfo xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:gmx="http://www.isotc211.org/2005/gmx">
                          <gmd:MD_Distribution>
                              <gmd:transferOptions>
                                  <gmd:MD_DigitalTransferOptions>
                                      <gmd:unitsOfDistribution>
                                          <gco:CharacterString>liens associés</gco:CharacterString>
                                      </gmd:unitsOfDistribution>
                                      <gmd:onLine>
                                          <gmd:CI_OnlineResource>
                                              <gmd:linkage>
                                                  <gmd:URL>'.$this->generateUrl('catalogue_rawgraph', array('mode' => 'readOnly'), true).'?uuid=' .$uuid.'</gmd:URL>
                                              </gmd:linkage>
                                              <gmd:protocol>
                                                  <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                                              </gmd:protocol>
                                              <gmd:name>
                                                  <gco:CharacterString>Accès au graphe</gco:CharacterString>
                                              </gmd:name>
                                          </gmd:CI_OnlineResource>
                                      </gmd:onLine>
                                      <gmd:onLine>
                                        <gmd:CI_OnlineResource>
                                           <gmd:linkage>
                                              <gmd:URL>'.$this->generateUrl('catalogue_rawgraph', array('mode' => 'svg'), true).'?uuid=' .$uuid.'</gmd:URL>
                                           </gmd:linkage>
                                           <gmd:protocol>
                                              <gco:CharacterString>WWW:DOWNLOAD-1.0-http--download</gco:CharacterString>
                                           </gmd:protocol>
                                           <gmd:name>
                                              <gmx:MimeFileType type="application/octet-stream">Export SVG du graphe</gmx:MimeFileType>
                                           </gmd:name>
                                           <gmd:function>
                                              <gmd:CI_OnLineFunctionCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_OnLineFunctionCode"
                                                                         codeListValue="download"/>
                                           </gmd:function>
                                        </gmd:CI_OnlineResource>
                                     </gmd:onLine>
                                  </gmd:MD_DigitalTransferOptions>
                              </gmd:transferOptions>
                          </gmd:MD_Distribution>
                     </gmd:distributionInfo>';

          $distInfo = new \DOMDocument;
          $distInfo->loadXML($strXML);
          $nodeToImport = $distInfo->getElementsByTagName("distributionInfo")->item(0);
          $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);

          $transfertOption = $xpath->query("/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions");
          if($transfertOption->length > 0) {
             $transfertOption->item(0)->appendChild($newNodeDesc);
          }else{
            $racine = $metadata_doc->getElementsByTagName("MD_Metadata");
            if($racine){
              $dataQualityInfo = $racine->item(0)->getElementsByTagName("dataQualityInfo");
              if($dataQualityInfo && $dataQualityInfo->length > 0) {
                  $racine->item(0)->insertBefore($newNodeDesc, $dataQualityInfo->item(0));
              } else {
                  $racine->item(0)->appendChild($newNodeDesc);
              }
            }
          }

          $strXml = '<srv:containsOperations  xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">
              <srv:SV_OperationMetadata>
                  <srv:operationName>
                    <gco:CharacterString>Accès au graphe</gco:CharacterString>
                  </srv:operationName>
                  <srv:DCP>
                    <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                  </srv:DCP>
                  <srv:connectPoint>
                      <gmd:CI_OnlineResource>
                          <gmd:linkage>
                            <gmd:URL>'. $this->generateUrl('catalogue_rawgraph', array('mode' => 'readOnly'), true).'?uuid=' .$uuid.'</gmd:URL>
                          </gmd:linkage>
                          <gmd:protocol>
                            <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                          </gmd:protocol>
                      </gmd:CI_OnlineResource>
                  </srv:connectPoint>
              </srv:SV_OperationMetadata>
            </srv:containsOperations>';  
          
          $tagRepport = new \DOMDocument();
          $tagRepport->loadXML($strXml);

          $nodeToImport = $tagRepport->getElementsByTagName("containsOperations" )->item(0);

          $coupledResource = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
          $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/*");
          if ( $objXPath->length > 0 ) {
              $element = $objXPath->item(0);
          }
          if ( $coupledResource && $coupledResource->length > 0 ) {
              $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
              $coupledResource->item(0 )->insertBefore($newNodeDesc, $element);
          }

          $strXml = '<srv:containsOperations  xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco">
              <srv:SV_OperationMetadata>
                  <srv:operationName>
                    <gco:CharacterString>Export SVG du graphe</gco:CharacterString>
                  </srv:operationName>
                  <srv:DCP>
                    <srv:DCPList codeList="http://www.isotc211.org/2005/iso19119/resources/Codelist/gmxCodelists.xml#DCPList" codeListValue="WebServices"/>
                  </srv:DCP>
                  <srv:connectPoint>
                      <gmd:CI_OnlineResource>
                          <gmd:linkage>
                            <gmd:URL>'.$this->generateUrl('catalogue_rawgraph', array('mode' => 'svg'), true).'?uuid=' .$uuid.'</gmd:URL>
                          </gmd:linkage>
                          <gmd:protocol>
                            <gco:CharacterString>WWW:LINK-1.0-http--link</gco:CharacterString>
                          </gmd:protocol>
                      </gmd:CI_OnlineResource>
                  </srv:connectPoint>
              </srv:SV_OperationMetadata>
            </srv:containsOperations>';  
          
          $tagRepport = new \DOMDocument();
          $tagRepport->loadXML($strXml);

          $nodeToImport = $tagRepport->getElementsByTagName("containsOperations" )->item(0);

          $coupledResource = $metadata_doc->getElementsByTagName("SV_ServiceIdentification");
          $objXPath = @$xpath->query("/gmd:MD_Metadata/gmd:identificationInfo/srv:SV_ServiceIdentification/*");
          if ( $objXPath->length > 0 ) {
              $element = $objXPath->item(0);
          }
          if ( $coupledResource && $coupledResource->length > 0 ) {
              $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);
              $coupledResource->item(0 )->insertBefore($newNodeDesc, $element);
          }

          $IdentInfo = $metadata_doc->getElementsByTagName("SV_ServiceIdentification")->item(0);
          $strXML ='<srv:coupledResource xmlns:srv="http://www.isotc211.org/2005/srv" xmlns:gco="http://www.isotc211.org/2005/gco" xmlns:gmd="http://www.isotc211.org/2005/gmd">
                       <srv:SV_CoupledResource>
                        <srv:operationName>
                           <gco:CharacterString>GetChart</gco:CharacterString>
                        </srv:operationName>
                        <srv:identifier>
                           <gco:CharacterString>'.$uuidLayer.'</gco:CharacterString>
                        </srv:identifier>
                        <gco:ScopedName>'.$tableLayer.'</gco:ScopedName>
                     </srv:SV_CoupledResource>
                   </srv:coupledResource>';
          $addXmlCarte = new \DOMDocument;
          $addXmlCarte->loadXML($strXML);
          $nodeToImport = $addXmlCarte->getElementsByTagName("coupledResource")->item(0);
          $newNodeDesc = $metadata_doc->importNode($nodeToImport, true);

          if($metadata_doc->getElementsByTagName("coupledResource") && $metadata_doc->getElementsByTagName("coupledResource")->length>0){
             $firstCoupledResource = $metadata_doc->getElementsByTagName("coupledResource")->item(0);
             $firstCoupledResource->parentNode->insertBefore($newNodeDesc, $firstCoupledResource);
          }else{
             $IdentInfo->appendChild($newNodeDesc);
          }

          $strXML2 = '<srv:operatesOn xmlns:xlink="http://www.w3.org/1999/xlink"  xmlns:srv="http://www.isotc211.org/2005/srv" 
                    uuidref="'.$uuidLayer.'" xlink:href="'.rtrim(PRO_GEONETWORK_URLBASE, "/").'/srv/'.$uuidLayer.'"/>';
          $addXmlCarte2 = new \DOMDocument;

          $addXmlCarte2->loadXML($strXML2);
          $nodeToImport2 = $addXmlCarte2->getElementsByTagName("operatesOn")->item(0);
          $newNodeDesc2 = $metadata_doc->importNode($nodeToImport2, true);

          if($metadata_doc->getElementsByTagName("operatesOn") && $metadata_doc->getElementsByTagName("operatesOn")->length>0){
              $firstOperatesOn = $metadata_doc->getElementsByTagName("operatesOn")->item(0);
              $firstOperatesOn->parentNode->insertBefore($newNodeDesc2, $firstOperatesOn);
          }else{
              $IdentInfo->appendChild($newNodeDesc2);
          }
        }

        $new_metadata_data = $metadata_doc->saveXML();
        $new_metadata_data = str_replace($entete, "", $new_metadata_data);
        if($new_metadata_data != "") {
            $urlUpdateData = "md.edit.save";
            $formData = array(
                "id"=> $metadataId,
                "data" => $new_metadata_data
            );
            //send to geosource
            $geonetwork = new GeonetworkInterface(PRO_GEONETWORK_URLBASE, 'srv/fre/');
            $geonetwork->post($urlUpdateData, $formData);
        }
      }
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/param/{uuid}", name="catalogue_graph_param", options={"expose"=true})
     */
    public function paramGraphAction(Request $request, $uuid)
    {
        $values         = $this->getRawgraphConfByUuid($uuid, ['titre', 'resume', 'serie_donnees_uuid', 'couche', 'colonnes']);
        $values['uuid'] = $uuid;

        return $this->render('GraphBundle/Default/config_graph.html.twig', ['values' => $values]);
    }

    /**
     *
     * @Route("/getRawgraphConf/{uuid}", name="catalogue_graph_getRawgraphConf", options={"expose"=true})
     */
    public function getRawgraphConfAction(Request $request, $uuid) 
    {
        $values = $this->getRawgraphConfByUuid($uuid, ['titre', 'resume', 'serie_donnees_uuid', 'couche', 'colonnes', 'config']);
        if( count($values) ) {
            return new JsonResponse($values);
        }

        return new Response('Erreur', 500);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/getLayerFields/{table_name}", name="catalogue_graph_getLayerFields", options={"expose"=true})
     */
    public function getLayerFieldsAction(Request $request, $table_name) 
    {
        $PRODIGE = $this->getProdigeConnection(self::CONNECTION_PRODIGE);

        $sql = "SELECT column_name
                FROM information_schema.columns
                WHERE table_name = :table_name
                AND column_name != 'the_geom' ";
        $results = $PRODIGE->fetchAllAssociative($sql, ['table_name' => $table_name]);

        return new JsonResponse($results);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/submitForm/{uuid}", name="catalogue_graph_submitForm", options={"expose"=true})
     */
    public function submitFormAction(Request $request, $uuid) 
    {
        $CATALOGUE = $this->getCatalogueConnection(self::CONNECTION_CATALOGUE);

        extract($request->request->all());
        if( isset($titre) && isset($resume) && isset($serie_donnees_uuid) && isset($couche) && isset($colonnes) ) {
            $coucheConfigFields = array("titre", "resume", "serie_donnees_uuid", "couche", "colonnes");
            $colonnes           = implode(', ', $colonnes);
            $values             = $this->getRawgraphConfByUuid($uuid, ['uuid']);
            if( count($values) ) {
                $set = array_combine($coucheConfigFields, $coucheConfigFields);
                $set = str_replace("=", "=:", http_build_query($set, null, ", "));
                $sql = "UPDATE rawgraph_couche_config SET " . $set . " WHERE uuid = :uuid ";
                $CATALOGUE->executeQuery($sql, array_merge(compact($coucheConfigFields), ['uuid' => $uuid]));
            }
            else {
                $coucheConfigFields[] = 'uuid';
                $sql = "INSERT INTO rawgraph_couche_config (".implode(", ", $coucheConfigFields).") VALUES (:".implode(", :", $coucheConfigFields).") ";
                $CATALOGUE->executeQuery($sql, compact($coucheConfigFields));
            }

            $session = $request->getSession();
            $session->set('submit', 'submit');

            return new Response('Success');
        }

        return new Response('Error', 500);
    }

    /**
     * 
     * @Route("/{mode}", name="catalogue_rawgraph", requirements={"mode":"update|readOnly|svg"}, options={"expose"=true})
     */
    public function indexAction(Request $request, $mode)
    {
      // vérification des droits sur un des domaines de la donnée
      $uuid = $request->query->get('uuid', NULL);

      // verify CMS rights on metadata object (map, dataset...)
      $serv = $this->container->get('prodige.verifyrights'); 
      $serv instanceof \Prodige\ProdigeBundle\Controller\VerifyRightsController;
      $req = new \Symfony\Component\HttpFoundation\Request(array(
          "uuid"          => $uuid,
          "TRAITEMENTS" => "CMS|NAVIGATION",
          "OBJET_TYPE"  => "service", 
          "OBJET_STYPE" => "chart",
      ));
      $response = $serv->verifyRightsAction($req);
           
      if( ! $response instanceof \Symfony\Component\HttpFoundation\Response ) {
        throw new AuthenticationException('Erreur lors de la vérification des droits : format de réponse erroné');
      }
      $rights = json_decode($response->getContent(), true);
      $bAllow = false;
      if($mode =="readOnly" || $mode =="svg"){
        $bAllow = $rights["NAVIGATION"];
      }else{
        $bAllow = $rights["CMS"];
      }
      if( ! $bAllow){
        \Prodige\ProdigeBundle\Common\SecurityExceptions::throwAccessDeniedException();
      }else{
//        /* vérifier si la metadata possède une entrée dans la table rawgraph_couche_config */
//        $values = $this->getRawgraphConfByUuid($uuid, ['uuid']);
//        if( !count($values) ) {
//            return $this->redirectToRoute('catalogue_graph_param', ['uuid' => $uuid]);
//        }

            $session = $request->getSession();
            if( $mode == 'update' ) {
                if( !$session->has('submit') ) {
                    return $this->redirectToRoute('catalogue_graph_param', ['uuid' => $uuid]);
                }
                $session->remove('submit');
            }

            return $this->render('GraphBundle/Default/index.html.twig' );
      }
    }

    /**
     *
     * @Route("/getLayerData/{uuid}", name="catalogue_rawgraph_getLayerData", options={"expose"=true})
     */
    public function getLayerDataAction(Request $request, $uuid) 
    {
        $CATALOGUE = $this->getCatalogueConnection(self::CONNECTION_CATALOGUE);
        $PRODIGE   = $this->getProdigeConnection();

        $values = $this->getRawgraphConfByUuid($uuid, [ 'couche', 'colonnes']);
        if( count($values) ) {
            $couche   = $values['couche'];
            $colonnes = $values['colonnes'];
            $TabColonnes = explode(", ", $colonnes);
            $colonnes = '\"'.implode('\",\"', $TabColonnes).'\"';
            $cmd      = 'PGPASSWORD='.$this->container->getParameter('prodige_password').' psql -h '.$this->container->getParameter('prodige_host').' -U '.$this->container->getParameter('prodige_user').' -d '.$this->container->getParameter('prodige_name').
                        ' -c "Copy (SELECT '.( !empty($colonnes) ? $colonnes : '*' ).' FROM '.$couche.') To STDOUT With CSV NULL AS \'null\' HEADER DELIMITER \',\';" ';
            $process  = Process::fromShellCommandline($cmd);
            
            $process->run();
            if( !empty($process->getOutput()) && empty($process->getErrorOutput()) ) {
                return new Response($process->getOutput());
            }
        }

        return new Response('Error', 500);
    }

    /**
     * @IsGranted("ROLE_USER")
     * @Route("/saveConfig/{uuid}", name="catalogue_graph_saveConfig", options={"expose"=true})
     */
    public function saveConfigAction(Request $request, $uuid) 
    {
        $CATALOGUE = $this->getCatalogueConnection(self::CONNECTION_CATALOGUE);

        $config = $request->request->get('config', null);
        if( $config ) {
            $sql = "UPDATE rawgraph_couche_config set config = :config WHERE uuid = :uuid";
            $CATALOGUE->executeQuery($sql, ['config' => json_encode($config), 'uuid' => $uuid]);
            $infoGraph = $this->getRawgraphConfByUuid($uuid, ['serie_donnees_uuid', 'couche']);
            $this->updateMetadata($uuid, $infoGraph["serie_donnees_uuid"], $infoGraph["couche"]);

            return new Response('Success');
        }

        return new Response('Error', 500);
    }

    /**
     * remplacement de caractère spéciaux
     * 
     * @param type $string
     * @return type
     */
    private function replace_accents($string) { 
        return strtolower(preg_replace('/\s+/', '', str_replace( 
                array('à','á','â','ã','ä', 'ç', 'è','é','ê','ë', 'ì','í','î','ï', 'ñ', 'ò','ó','ô','õ','ö', 'ù','ú','û','ü', 'ý','ÿ', 'À','Á','Â','Ã','Ä', 'Ç', 'È','É','Ê','Ë', 'Ì','Í','Î','Ï', 'Ñ', 'Ò','Ó','Ô','Õ','Ö', 'Ù','Ú','Û','Ü', 'Ý'), 
                array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i','i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A','A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U','U','U', 'Y'), 
                $string)));
    }

    /**
     * @Route("/svg/getCode", name="catalogue_graph_getSvgCode", options={"expose"=true})
     */
    public function getCodeAction(Request $request) 
    {
        $response = new Response();

        $svgCode = $request->request->get('svgCode', NULL);
        $titre   = $request->request->get('titre', NULL);
        if( $svgCode && $titre ) {
            $response->headers->set('Content-Type', 'image/svg+xml');
            $response->headers->set('Content-Disposition', 'attachment; filename="'.$this->replace_accents($titre).'.svg";');
            $response->setContent($svgCode);
        }
        else {
            $response->setContent("Absence de contenu svg");
        }

        return $response;        
    }

}

#!/bin/bash
#backup 
cp /var/www/html/data/logs/Telechargements.csv /var/www/html/data/logs/Telechargements.csv.backup_sed
cp /var/www/html/data/logs/ConsultationMetadonnees.csv /var/www/html/data/logs/ConsultationMetadonnees.csv.backup_sed
cp /var/www/html/data/logs/ConsultationCarte.csv /var/www/html/data/logs/ConsultationCarte.csv.backup_sed
cp /var/www/html/data/logs/Connexions.csv /var/www/html/data/logs/Connexions.csv.backup_sed

# fix format date
sed  -i 's/^\(....-..-..\)T\(..:..:..\)[^\+]*+02:00;/\1 \2;/' /var/www/html/data/logs/Telechargements.csv
sed  -i 's/^\(....-..-..\)T\(..:..:..\)[^\+]*+01:00;/\1 \2;/' /var/www/html/data/logs/Telechargements.csv

sed  -i 's/^\(....-..-..\)T\(..:..:..\)[^\+]*+02:00;/\1 \2;/' /var/www/html/data/logs/ConsultationMetadonnees.csv
sed  -i 's/^\(....-..-..\)T\(..:..:..\)[^\+]*+01:00;/\1 \2;/' /var/www/html/data/logs/ConsultationMetadonnees.csv

sed  -i 's/^\(....-..-..\)T\(..:..:..\)[^\+]*+02:00;/\1 \2;/' /var/www/html/data/logs/ConsultationCarte.csv
sed  -i 's/^\(....-..-..\)T\(..:..:..\)[^\+]*+01:00;/\1 \2;/' /var/www/html/data/logs/ConsultationCarte.csv

sed  -i 's/^\(....-..-..\)T\(..:..:..\)[^\+]*+02:00;/\1 \2;/' /var/www/html/data/logs/Connexions.csv
sed  -i 's/^\(....-..-..\)T\(..:..:..\)[^\+]*+01:00;/\1 \2;/' /var/www/html/data/logs/Connexions.csv

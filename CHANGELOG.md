# Changelog

All notable changes to [ppr_prodige_catalogue](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue) project will be documented in this file.

## [4.4.46](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.45...4.4.46) - 2024-10-17

## [4.4.45](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.44...4.4.45) - 2024-07-24

## [4.4.44](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.43...4.4.44) - 2024-07-24

## [4.4.43](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.42...4.4.43) - 2024-07-16

## [4.4.42](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.41...4.4.42) - 2024-07-01

## [4.4.41](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.40...4.4.41) - 2024-05-28

### Fix

- API random problem serving data

## [4.4.40](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.39...4.4.40) - 2024-05-22

## [4.4.39](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.38...4.4.39) - 2024-05-21

### Fix

- API sort_by bug fix

## [4.4.38](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.37...4.4.38) - 2024-04-19

## [4.4.38](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.36...4.4.38) - 2024-04-19

## [4.4.36](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.35...4.4.36) - 2024-04-19

### Changed

- add full text search in API

## [4.4.35](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.34...4.4.35) - 2024-04-19

## [4.4.34](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.33...4.4.34) - 2024-03-19

## [4.4.33](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.32...4.4.33) - 2024-02-15

## [4.4.32](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.31...4.4.32) - 2024-02-02

## [4.4.31](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.30...4.4.31) - 2024-02-02

## [4.4.30](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.29...4.4.30) - 2024-02-01

### Fix

- date format in logs

## [4.4.29](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.28...4.4.29) - 2024-01-12

## [4.4.28](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.27...4.4.28) - 2024-01-12

## [4.4.27](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.26...4.4.27) - 2024-01-05

## [5.0.24](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.26...5.0.24) - 2024-01-05

## [4.4.26](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.25...4.4.26) - 2023-12-14

## [4.4.25](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.24...4.4.25) - 2023-11-29

## [4.4.24](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.23...4.4.24) - 2023-11-29

### Changed

- add timestamp field in PRODIGE API

## [4.4.23](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.22...4.4.23) - 2023-11-02

## [4.4.22](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.21...4.4.22) - 2023-09-28

### Fix

- data view without default representation

## [4.4.21](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.20...4.4.21) - 2023-09-21

### Fix

- layer view without default representation

## [4.4.20](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.19...4.4.20) - 2023-09-15

### Fix

- increase memory limit in API

## [4.4.19](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.18...4.4.19) - 2023-09-04

## [4.4.18](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.17...4.4.18) - 2023-09-04

## [4.4.17](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.16...4.4.17) - 2023-08-25

## [4.4.16](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.15...4.4.16) - 2023-08-22

## [4.4.15](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.14...4.4.15) - 2023-06-15

### Fix

- urbadata module general fix

## [4.4.14](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.13...4.4.14) - 2023-06-14

### Changed

- API : Add srs parameter in API to retrieve data in wanted srs

### Fix

- align data.gouv API on last_update parameter

## [4.4.13](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.12...4.4.13) - 2023-06-13

## [4.4.12](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.11...4.4.12) - 2023-05-16

## [4.4.11](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.10...4.4.11) - 2023-05-15

## [4.4.10](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.9...4.4.10) - 2023-05-04

## [4.4.9](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.8...4.4.9) - 2023-04-27

## [4.4.8](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.7...4.4.8) - 2023-04-06

## [4.4.7](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.6...4.4.7) - 2023-04-06

## [4.4.6](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.5...4.4.6) - 2023-04-06

## [4.4.5](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.4...4.4.5) - 2023-03-30

## [4.4.4](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.3...4.4.4) - 2023-03-29

## [4.4.3](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.2...4.4.3) - 2023-03-17

## [4.4.2](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.1...4.4.2) - 2023-03-17

## [4.4.1](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc49...4.4.1) - 2023-03-17

## [4.4.0_rc49](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc48...4.4.0_rc49) - 2023-02-10

## [4.4.0_rc48](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc47...4.4.0_rc48) - 2023-02-09

## [4.4.0_rc47](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc46...4.4.0_rc47) - 2023-02-06

## [4.4.0_rc46](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc45...4.4.0_rc46) - 2023-02-06

## [4.4.0_rc45](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc44...4.4.0_rc45) - 2023-02-06

## [4.4.0_rc44](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc43...4.4.0_rc44) - 2023-02-03

## [4.4.0_rc43](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc42...4.4.0_rc43) - 2023-02-03

## [4.4.0_rc42](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc41...4.4.0_rc42) - 2023-02-03

## [4.4.0_rc41](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc40...4.4.0_rc41) - 2023-01-18

## [4.4.0_rc40](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc39...4.4.0_rc40) - 2022-12-19

## [4.4.0_rc39](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc38...4.4.0_rc39) - 2022-12-16

## [4.4.0_rc38](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc37...4.4.0_rc38) - 2022-12-12

## [4.4.0_rc37](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc36...4.4.0_rc37) - 2022-12-08

## [4.4.0_rc36](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc35...4.4.0_rc36) - 2022-12-08

## [4.4.0_rc35](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc34...4.4.0_rc35) - 2022-12-08

## [4.4.0_rc34](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc33...4.4.0_rc34) - 2022-12-08

## [4.4.0_rc33](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc32...4.4.0_rc33) - 2022-12-07

## [4.4.0_rc32](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc31...4.4.0_rc32) - 2022-11-10

## [4.4.0_rc31](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc30...4.4.0_rc31) - 2022-11-09

## [4.4.0_rc30](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc29...4.4.0_rc30) - 2022-11-09

## [4.4.0_rc29](https://gitlab.adullact.net/prodige/ppr_prodige_catalogue/compare/4.4.0_rc28...4.4.0_rc29) - 2022-11-09


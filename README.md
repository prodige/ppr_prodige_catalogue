# Module PRODIGE catalogue

Module noyau de PRODIGE intégrant le back-office de gestion d'utilisateurs et de droits et les relations avec geonetwork. 

### configuration

Modifier le fichier site/app/config/global_parameters.yml

### Installation

[documentation d'insatllation en développement](cicd/dev/README.md).

## Accès à l'application

- [Accès au module](https://catalogue.prodige.internal).

#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
. $SCRIPTDIR/env.sh


## Prompt https://nexus.alkante.com login/password if NEXUS_USR and NEXUS_PSW environnement variable are not defined
if [ -z "${NEXUS_USR}" ]; then
  echo -n "Enter nexus.alkante.com login: "
  read NEXUS_USR
fi
if [ -z "${NEXUS_PSW}" ]; then
  echo -n "password: "
  stty -echo
  IFS= read -r NEXUS_PSW
  stty echo
  printf '\n'
fi
[ -z "${REPO_COMPOSER_DOMAIN}" ] && REPO_COMPOSER_DOMAIN='nexus.alkante.com'

echo -n "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > $SCRIPTDIR/docker-build/docker-compose.env;

# Create dockers
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml build
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml up -d
sleep 2

# Exec
docker exec --user www-data -w /var/www/html/site ppr_prodige_catalogue_build_web /bin/bash -c " \
  set -e; \
  echo '{
    \"http-basic\": {
      \""$REPO_COMPOSER_DOMAIN"\": {
			  \"username\": \""$NEXUS_USR"\",
			  \"password\": \""$NEXUS_PSW"\"
      }
    }
  }' > auth.json; \
  COMPOSER_MEMORY_LIMIT=5G composer install --no-interaction --no-progress; \
  rm auth.json"

# Unzip vendor
docker exec --user www-data -w /var/www/html/site ppr_prodige_catalogue_build_web /bin/bash -c " \
  set -e; \
  for lib in public/Scripts/*.zip; do \
    unzip -oq \$lib -d public/Scripts ;\
    rm -f \$lib ;\
  done;\
  for lib in  public/vendor/*.zip; do \
    unzip -oq \$lib -d public/vendor;\
    rm -f \$lib;\
  done;\
  php bin/console assets:install public"

# Stop dockers
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml down

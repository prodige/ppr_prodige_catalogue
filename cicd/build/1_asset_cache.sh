#!/bin/sh
set -e

# Source env file
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`
. $SCRIPTDIR/env.sh

echo -n "LOCAL_USER_ID=$(id -u)\nLOCAL_GROUP_ID=$(id -g)" > $SCRIPTDIR/docker-build/docker-compose.env;

# Create dockers
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml build
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml up -d

# Exec
docker exec --user www-data -w /var/www/html/site ppr_prodige_catalogue_build_web /bin/bash -c " \
  set -e; \
  php ./bin/console assets:install web; \
  php ./bin/console cache:clear --env dev; \
  php ./bin/console cache:clear --env prod;"

# Stop dockers
docker-compose -f $SCRIPTDIR/docker-build/docker-compose.yml down
